PROCEDURE GrabarComp_renPago
  SELECT comp_ren
  APPEND BLANK
  REPLACE	comp_ren.grupo_comp WITH AuxGrupoCompWeb,;
		comp_ren.nro_comp   WITH AuxNroComp,;
		comp_ren.reng_comp  WITH AuxCantReng,;
		comp_ren.tipo_imp   WITH P_ArchMovSelec.tipoimponible,;
		comp_ren.nro_imp    WITH P_ArchMovSelec.nroimponible,;
		comp_ren.recurso    WITH P_ArchMovSelec.recurso,;
		comp_ren.anio       WITH P_ArchMovSelec.anio,;
		comp_ren.cuota      WITH P_ArchMovSelec.cuota,;
		comp_ren.nro_mov    WITH P_ArchMovSelec.nromov,;
		comp_ren.conc_cc    WITH rec_cc.conc_cc,;
		comp_ren.nro_plan   WITH rec_cc.nro_plan,;
		comp_ren.imp_reng   WITH m.imp_ori,;
		comp_ren.actualiza1 WITH m.actualiz,;
		comp_ren.intereses1 WITH m.interes,;
		comp_ren.imp1_iva1  WITH 0,;
		comp_ren.imp1_iva2  WITH 0,;
		comp_ren.actualiza2 WITH 0,;
		comp_ren.intereses2 WITH 0,;
		comp_ren.imp2_iva1  WITH 0,;
		comp_ren.imp2_iva2  WITH 0,;
		comp_ren.actualiza3 WITH 0,;
		comp_ren.intereses3 WITH 0,;
		comp_ren.imp3_iva1  WITH 0,;
		comp_ren.imp3_iva2  WITH 0,;
		comp_ren.debita_cc  WITH 'S',;
		comp_ren.venc_conc  WITH 0
ENDPROC