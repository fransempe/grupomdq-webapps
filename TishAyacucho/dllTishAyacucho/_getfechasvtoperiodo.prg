 
* Description:
PROCEDURE _getFechasVtoPeriodo(pResource as String, p_numYear as Number, p_numCuota as Number) as String
	PRIVATE strXML_aux as String

	=UseT('recur\fec_vtos')
	SELECT fec_vtos
	SET ORDER TO PRIMARIO
	
	strXML_aux = ''
	IF (SEEK(ALLTRIM(pResource) + STR(p_numYear, 4, 0) + STR(p_numCuota, 4, 0), 'fec_vtos')) THEN	
		strXML_aux = fec_vtos.fv_cuota + '|' + fec_vtos.fv_presentacion
	ENDIF
 	 
	RETURN ALLTRIM(strXML_aux)
ENDPROC