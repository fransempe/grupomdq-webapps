
* _ObtainListTrades
* This function returns the list of trades
PROCEDURE _ObtainListTrades(pTrades as String) as String
	PRIVATE mIndex as String
	PRIVATE mResult as String
	PRIVATE mResult_XML as String
	PRIVATE mNumberTrade as Number
	PRIVATE mNumberTrade_AUX as String
	PRIVATE i as Integer



	* I open the table comercio
	IF (!USED('comercio')) THEN
		=UseT('recur\comercio')
	ENDIF
	SELECT comercio 
	SET ORDER TO Primario
	 



	mNumberTrade_AUX = ''
	mResult = ''
	FOR i = 1 to LEN(ALLTRIM(pTrades)) 
		
		
		IF (ALLTRIM(SUBSTR(pTrades, i, 1)) <> '|') THEN	
			mNumberTrade_AUX = mNumberTrade_AUX + ALLTRIM(SUBSTR(pTrades, i, 1))		
		ELSE	
		
		
		
			mNumberTrade = VAL(pTrades)
		
			* I create the index	    					   
			mNumberTrade = VAL(mNumberTrade_AUX)
			mIndex = STR(mNumberTrade, 10, 0)


			* I seek the trade		
			IF SEEK(mIndex, 'comercio') THEN	
			
				
				mResult = mResult + '<TRADE>' + _S_
				mResult = mResult + '<TRADE_NUMBER>' + ALLTRIM(STR(comercio.Nro_com)) + '</TRADE_NUMBER>' + _S_
				mResult = mResult + '<TRADE_NAME>' + ALLTRIM(comercio.Nomb_com) + '</TRADE_NAME>' +_S_


				
				m.cuit1 = 0
				m.cuit2 = 0
				=FContVin('C', comercio.Nro_com,.F.,.F.,@m.cuit1,@m.cuit2)
				
				
				IF (m.cuit1 > 0) THEN
					IF (SEEK(STR(m.cuit1,10,0),'contrib')) THEN
						mResult = mResult + '<CONTRIBUYENTE_NOMBRE>' + ALLTRIM(contrib.Nomb_cont) + '</CONTRIBUYENTE_NOMBRE>' + _S_							
					ENDIF
				ELSE
					mResult = mResult + '<CONTRIBUYENTE_NOMBRE>Contribuyente sin asignar</CONTRIBUYENTE_NOMBRE>' + _S_							
				ENDIF
				
				
				
				mResult = mResult + '</TRADE>' + _S_
				
			ENDIF		

			mNumberTrade_AUX = ''
		ENDIF
				
    ENDFOR      

	mResult_XML = '<?xml version="1.0" encoding="UTF-8"?>' + _S_				
	mResult = mResult_XML + _S_ + '<DATA_TRADES>' + _S_ + ALLTRIM(mResult) + '</DATA_TRADES>'
	RETURN ALLTRIM(mResult)	
ENDPROC