PROCEDURE Add_DDJJ (p_strDatos_XML)
	PRIVATE strERROR as String
	PRIVATE mIndex as String
	PRIVATE numCountDDJJ as Number
	PRIVATE strTipoDDJJ as String
	PRIVATE strRubro as String


	
	* Leo el xml y creo el cursor
	strERROR = 'OK'
	XMLTOCURSOR(p_strDatos_XML, 'Cursor_DDJJ')
	SELECT Cursor_DDJJ
	IF UPPER(ALIAS()) # UPPER('Cursor_DDJJ')
		strERROR = 'Imposible generar cursor de datos.'
	ENDIF


	GO TOP IN Cursor_DDJJ
	* Debido a problemas de conversi�n con la funci�n XMLTOCURSOR el primer registro
	* se utiliza como m�scara de campos y, por la tanto, se omite.
	SKIP IN Cursor_DDJJ


	
	* Abro la la tabla de declaraciones juradas
	IF (!USED('com_ddjj')) THEN
 		=UseT('recur\com_ddjj')
 	ENDIF
		
	
	* Abro la tabla de historial de declaraciones juradas, esto es para el caso de las rectificativas
	IF (!USED('com_dj_d')) THEN
 		=UseT('recur\com_dj_d')
 	ENDIF
 	
 	
 	* Abro la la tabla de comercios
	IF (!USED('comercio')) THEN
 		=UseT('recur\comercio')
 	ENDIF




	* Antes de grabar las declaraciones juradas y los rubros seleccionados limpio todos los 
	* rubros para este comercio. Esto lo hago porque puede ser que en la declaraci�n jurada anterior halla seleccionado m�s
	* rubros que en esta declaraci�n jurada y para que no queden guardados rubros de declaraciones anteriores los borro todos	
	SELECT comercio
	SET ORDER TO PRIMARIO IN comercio
	IF (SEEK(STR(Cursor_DDJJ.Nro_com, 10, 0), 'comercio')) THEN			
		REPLACE comercio.Rubro1 	WITH 0, ;
				comercio.Rubro2 	WITH 0, ;
				comercio.Rubro3 	WITH 0, ;
				comercio.Rubro4 	WITH 0, ;	
				comercio.Rubro5 	WITH 0, ;
				comercio.Rubro6 	WITH 0, ;
				comercio.Rubro7 	WITH 0, ;
				comercio.Rubro8 	WITH 0, ;
				comercio.Rubro9 	WITH 0, ;
				comercio.Rubro10 	WITH 0
	ENDIF				






	* Recorro el cursor porque queda registro corresponde a un rubro distinto de la misma 
	* declaraci�n jurada.
	numCountDDJJ = 0
	strTipoDDJJ = 'O'
	SELECT Cursor_DDJJ
	DO WHILE !EOF('Cursor_DDJJ')
	
		
		* IMPORTANTE: Las declaraciones juradas las busco por rubro
		strIndex = STR(Cursor_DDJJ.Nro_com, 10, 0) + STR(Cursor_DDJJ.Anio, 4, 0) + STR(Cursor_DDJJ.Cuota, 3, 0) + STR(Cursor_DDJJ.Rubro_C, 7, 0)
		SELECT com_ddjj
		SET ORDER TO PRIMARIO IN com_ddjj		
		IF SEEK(strIndex, 'com_ddjj') THEN			
		
			IF (ALLTRIM(com_ddjj.ORIG_RECT) = 'O') THEN
				strTipoDDJJ = 'R'
				numCountDDJJ = 1
			ELSE
				strTipoDDJJ = 'R'
				numCountDDJJ = (com_ddjj.NRO_RECT + 1)								
			ENDIF
		ELSE		
			APPEND BLANK 					
		ENDIF
		
		REPLACE com_ddjj.NRO_COM 		WITH Cursor_DDJJ.NRO_COM, ;
				com_ddjj.ANIO 			WITH Cursor_DDJJ.ANIO, ;
				com_ddjj.CUOTA 			WITH Cursor_DDJJ.CUOTA, ;
				com_ddjj.NRO_TITULP		WITH 0, ;
				com_ddjj.NRO_EMPLP 		WITH Cursor_DDJJ.NRO_EMPLP, ;
				com_ddjj.MONT_FACTP 	WITH Cursor_DDJJ.MONT_FACTP, ;
				com_ddjj.MONT_GANP 		WITH Cursor_DDJJ.MONT_GANP, ;
				com_ddjj.MONT_GASTP 	WITH Cursor_DDJJ.MONT_GASTP, ;
				com_ddjj.MONT_SUELP 	WITH Cursor_DDJJ.MONT_SUELP, ;
				com_ddjj.PRES_DDJJP 	WITH Cursor_DDJJ.PRES_DDJJP, ;
				com_ddjj.FECH_DDJJP 	WITH DATE(), ;
				com_ddjj.MONT_DDJJP		WITH Cursor_DDJJ.MONT_DDJJP, ;
				com_ddjj.RUBRO_C 		WITH Cursor_DDJJ.RUBRO_C
				*com_ddjj.ORIG_RECT 	WITH strTipoDDJJ
				*com_ddjj.NRO_RECT 		WITH numCountDDJJ


		
		
		
		
		* Guardo la declaraci�n en la tabla de historial de declaraciones juradas
		SELECT com_dj_d
		APPEND BLANK 					
		REPLACE com_dj_d.NRO_COM 		WITH Cursor_DDJJ.NRO_COM, ;
				com_dj_d.ANIO 			WITH Cursor_DDJJ.ANIO, ;
				com_dj_d.CUOTA 			WITH Cursor_DDJJ.CUOTA, ;
				com_dj_d.NRO_EMPLP 		WITH Cursor_DDJJ.NRO_EMPLP, ;
				com_dj_d.MONT_FACTP 	WITH Cursor_DDJJ.MONT_FACTP, ;
				com_dj_d.MONT_GANP 		WITH Cursor_DDJJ.MONT_GANP, ;
				com_dj_d.MONT_GASTP 	WITH Cursor_DDJJ.MONT_GASTP, ;
				com_dj_d.MONT_SUELP 	WITH Cursor_DDJJ.MONT_SUELP, ;
				com_dj_d.PRES_DDJJP 	WITH Cursor_DDJJ.PRES_DDJJP, ;
				com_dj_d.FECH_DDJJP     WITH DATE(), ;
				com_dj_d.MONT_DDJJP		WITH Cursor_DDJJ.MONT_DDJJP, ;
				com_dj_d.RUBRO_C 		WITH Cursor_DDJJ.RUBRO_C
				*com_dj_d.ORIG_RECT 	WITH strTipoDDJJ
				*com_dj_d.NRO_RECT 		WITH numCountDDJJ
				
				
				
		* Actualizo el rubro				
		SELECT comercio
		SET ORDER TO PRIMARIO IN comercio
		IF (SEEK(STR(Cursor_DDJJ.Nro_com, 10, 0), 'comercio')) THEN			
				
			strRubro = 'comercio.Rubro' + ALLTRIM(STR(Cursor_DDJJ.Rubro_numero))
			REPLACE &strRubro WITH Cursor_DDJJ.Rubro_C				
		
		ENDIF				
				
				
				
				
		SELECT Cursor_DDJJ
		SKIP IN Cursor_DDJJ
	ENDDO

 	* Devuelvo el n�mero de la declaraci�n jurada	
	RETURN numCountDDJJ 
ENDPROC