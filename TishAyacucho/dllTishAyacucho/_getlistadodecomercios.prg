 
PROCEDURE _getListadoDeComercios(p_strComercios as String) as String
	PRIVATE strIndex as String	
	PRIVATE numNroComercio_aux as String
	PRIVATE i as Integer
	PRIVATE strXML as String


	IF (!USED('comercio')) THEN
		=UseT('recur\comercio')
	ENDIF
	SELECT comercio 
	SET ORDER TO Primario
	 

	strXML = ''
	strXML = strXML + '<?xml version="1.0" encoding="utf-8"?>' + _S_
	strXML = strXML + '<ArrayOfComercio xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">' + _S_

	numNroComercio_aux = ''
	FOR i = 1 to LEN(ALLTRIM(p_strComercios)) 
				
		IF (ALLTRIM(SUBSTR(p_strComercios , i, 1)) <> '|') THEN	
			numNroComercio_aux = numNroComercio_aux + ALLTRIM(SUBSTR(p_strComercios, i, 1))		
		ELSE	
		
			* Busco el comercio
			strIndex = STR(VAL(numNroComercio_aux), 10, 0)
			IF SEEK(strIndex, 'comercio') THEN						
				strXML = strXML + '<comercio>' 					+ _S_
				strXML = strXML + '<numero>' 					+ ALLTRIM(STR(comercio.Nro_com)) 	+ '</numero>' + _S_
				strXML = strXML + '<razonSocial>' 				+ ALLTRIM(comercio.Nomb_com) 		+ '</razonSocial>' +_S_


				* Obtengo el cuit asociado al comercio				
				m.cuit1 = 0
				m.cuit2 = 0
				=FContVin('C', comercio.Nro_com,.F.,.F.,@m.cuit1,@m.cuit2)							
				IF (m.cuit1 > 0) THEN
					IF (SEEK(STR(m.cuit1,10,0), 'contrib')) THEN
						strXML = strXML +  '<contribuyente>'	+ ALLTRIM(contrib.Nomb_cont)		+ '</contribuyente>' + _S_								
					ENDIF					
				ELSE
					strXML = strXML +  '<contribuyente>' 	+ 'Contribuyente sin asignar'		+ '</contribuyente>' + _S_			
				ENDIF	
														
				strXML = strXML + '</comercio>' + _S_				
			ENDIF		

			numNroComercio_aux = ''
		ENDIF				
    ENDFOR      
    
    
    strXML = strXML + '</ArrayOfComercio>' + _S_			
	RETURN ALLTRIM(strXML)      
ENDPROC