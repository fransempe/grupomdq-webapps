
*_ObtenerRecursoTISH:
* this function returns the number of recurso related to TISH 
PROCEDURE _ObtenerRecursoTISH()	as String
	PRIVATE mResult as String
	PRIVATE mIndex as String
	

	=UseT('asa\config')
	SELECT config
	SET ORDER TO Primario IN config
	 
	mResult = ''	
	mIndex = PADR('RECUR', 8, ' ') + 'RECURSO_TISH'
	IF SEEK(mIndex, 'config')
		mResult = ALLTRIM(config.cont_par)
	ENDIF
			
			
	RETURN ALLTRIM(mResult)
ENDPROC