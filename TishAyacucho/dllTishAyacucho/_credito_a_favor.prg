PROCEDURE _Credito_a_Favor(pComercio as Number) as Number
	PRIVATE _Credito as Number
	
	_Credito = 0

	SET ORDER TO primario IN rec_cc

	SELECT comprob
	SET ORDER TO imponib
	SET KEY TO 'C' + STR(pComercio ,10,0) IN comprob
	GO TOP IN comprob
	DO WHILE NOT EOF('comprob')

		* Omitir ciertos tipos de comprobantes
		IF comprob.tipo_comp # 'DE' AND comprob.tipo_comp # 'PP' AND comprob.tipo_comp # 'ND'
			SKIP IN comprob
			LOOP
		ENDIF
		
		
		* Surgi� un caso de una ND no vencida que ten�a 2 renglones (movimientos) y sobre uno de los movimientos
        * hab�an hecho un plan de pago por lo que el movimiento qued� EN PLAN.
        * Como la ND no estaba vencida se cargaba en la Web y se pod�a imprimir y se lleg� a pagar un movimiento
        * EN PLAN.
        * Asi que ahora si alguno de los movimientos del comprob. est� en plan directamente no se carga el
        * comprobante en pantalla.
		* Tampoco se carga si alguno de los movimientos ya est� cancelado.
        IF CompTieneMovEnPlanOCanc()
          SKIP IN comprob
          LOOP
        ENDIF
                
        
        * Si es un comprobante normal y es negativo sumo para saber si tiene credito a favor
		IF comprob.est_comp = 'NO'
	        IF comprob.tot_comp < 0 THEN        
		        _Credito = (_Credito + comprob.tot_comp)
	        ENDIF
		ENDIF
			
		
		SKIP IN comprob
	ENDDO
	
	
	SET KEY TO '' IN comprob
	RETURN _Credito
ENDPROC


********************************************************************
FUNCTION CompTieneMovEnPlanOCanc
********************************************************************
PRIVATE retorno
retorno = .F.

SET KEY TO STR(comprob.grupo_comp,2,0) + STR(comprob.nro_comp,9,0) IN comp_ren
GO TOP IN comp_ren
DO WHILE NOT EOF('comp_ren') AND !retorno
  IF comp_ren.debita_cc = 'S' AND nro_mov > 0
    IF SEEK(comp_ren.recurso + comp_ren.tipo_imp + STR(comp_ren.nro_imp,10) + STR(comp_ren.anio,4) +;
          STR(comp_ren.cuota,3) + STR(comp_ren.nro_mov,3), 'rec_cc')
      IF rec_cc.est_mov = 'EP'
        retorno = .T.
      ENDIF
    ELSE
      IF SEEK(comp_ren.recurso + comp_ren.tipo_imp + STR(comp_ren.nro_imp,10) + STR(comp_ren.anio,4) +;
          STR(comp_ren.cuota,3) + STR(comp_ren.nro_mov,3), 'rec_ccc')
        retorno = .T.
      ENDIF
    ENDIF  
  ENDIF
  
  SKIP IN comp_ren
ENDDO

RETURN retorno
ENDFUNC





