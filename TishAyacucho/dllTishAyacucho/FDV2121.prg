*************************************************
* FUNCTION FDV2121
*************************************************
* Autor : Rodrigo
* Dise�o: Pachu
* 
* Fecha :	31/03/95
* 
* Funcionamiento:	Calcula el digito verifiacdor segun algoritmo 2121
* 
* Par�metros:			NRO_FORMAT	(C)	:	Numero formateado
* 
* Modificaciones:
* 
PARAMETERS m.nro_format
=VerNuPar ('FDV2121', PARAMETERS()	, 1 )
=VerTiPar ('FDV2121', 'M.NRO_FORMAT','C')

PRIVATE digver, long_nro, posic, prox_num, digito
long_nro	=	LEN(m.nro_format)
posic			=	1
prox_num	=	2
digver		=	0

DO WHILE posic <= long_nro

	digito = ASC(SUBSTR(m.nro_format, posic, 1)) - 48

	IF digito > 0 AND digito <= 9

		IF prox_num = 2
			aux			=	2	*	digito
			digver	=	digver + aux
			aux			=	INT(aux / 10)
			digver	=	digver + aux
		ELSE
			digver	=	digver + digito
		ENDIF

		DO CASE
		CASE prox_num	=	1
			prox_num = 2
		CASE prox_num	=	2
			prox_num = 1
		ENDCASE

	ENDIF
	posic	=	posic	+	1
ENDDO
digver	=	digver % 10

RETURN digver
