
* _ObtainList_DateExpiry:
* Description:
PROCEDURE  _ObtainList_DateExpiry(pResource as String, pYear as Integer) as String
	PRIVATE mXML_AUX as String


	* I open the table fec_vtos
	=UseT('recur\fec_vtos')
	SELECT fec_vtos


	* I obtain 
	mXML_AUX = '<PERIODOS>' + _S_
	SET ORDER TO 1
	SET KEY TO ALLTRIM(pResource) + STR(pYear, 4, 0)
	GO TOP IN fec_vtos
	DO WHILE NOT EOF('fec_vtos')		

		mXML_AUX = mXML_AUX + '<PERIODO>'
		mXML_AUX = mXML_AUX + '<RECURSO>' + ALLTRIM(fec_vtos.Recurso) + '</RECURSO>'  
		mXML_AUX = mXML_AUX + '<ANIO>' + ALLTRIM(STR(fec_vtos.Anio)) + '</ANIO>' + _S_
		mXML_AUX = mXML_AUX + '<CUOTA>' + ALLTRIM(STR(fec_vtos.Cuota)) + '</CUOTA>' + _S_
		mXML_AUX = mXML_AUX + '<FV_CUOTA1>' + ALLTRIM(DTOC(fec_vtos.Fv1_Cuota)) + '</FV_CUOTA1>' + _S_				
		mXML_AUX = mXML_AUX + '<FV_CUOTA2>' + ALLTRIM(DTOC(fec_vtos.Fv2_Cuota)) + '</FV_CUOTA2>' + _S_				
		mXML_AUX = mXML_AUX + '<FV_P_DDJJ>' + ALLTRIM(DTOC(fec_vtos.Fv_p_ddjj)) + '</FV_P_DDJJ>' + _S_			
		mXML_AUX = mXML_AUX + '</PERIODO>'
 
	 
		SKIP IN fec_vtos
	ENDDO
	SET KEY TO
	mXML_AUX = mXML_AUX + '</PERIODOS>' + _S_		
	
 	 
	RETURN ALLTRIM(mXML_AUX)
ENDPROC