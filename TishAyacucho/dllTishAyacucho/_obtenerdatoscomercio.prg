PROCEDURE _ObtenerDatosComercio(pNroComercio as Number, pTipoImponible as String) as String
	PRIVATE mDatos as String
	PRIVATE mUbicacion as String


	* I open the table comercio
	IF (!USED('comercio')) THEN
		=UseT('recur\comercio')
	ENDIF
	SELECT comercio 
	SET ORDER TO 1
	SCATTER MEMVAR BLANK
	
	
	*Obtengo los datos del comercio
	mDatos = ''
	IF (SEEK(STR(pNroComercio,10 , 0)))
		mDatos = '<COMERCIO_NUMERO>' + ALLTRIM(STR(comercio.Nro_com)) + '</COMERCIO_NUMERO>' + _S_
		mDatos = mDatos + '<COMERCIO_RAZONSOCIAL>' + ALLTRIM(comercio.Nomb_com) + '</COMERCIO_RAZONSOCIAL>' + _S_
		mDatos = mDatos + '<COMERCIO_NOMBREFANTASIA>' + ALLTRIM(comercio.Nomb_fan) + '</COMERCIO_NOMBREFANTASIA>' + _S_				
		mDatos = mDatos + '<COMERCIO_NOMBRE>' + ALLTRIM(comercio.Nomb_com) + '</COMERCIO_NOMBRE>' + _S_
		
		
		mDatos = mDatos + '<COMERCIO_TELEFONO>' + ALLTRIM(comercio.Tel1_com) + '</COMERCIO_TELEFONO>' + _S_				
		mDatos = mDatos + '<COMERCIO_EMAIL>' + ALLTRIM(comercio.Email) + '</COMERCIO_EMAIL>' + _S_				
		mDatos = mDatos + '<COMERCIO_RESPONSABLE>' + ALLTRIM(comercio.Responsab) + '</COMERCIO_RESPONSABLE>' + _S_				
		mDatos = mDatos + '<COMERCIO_CARGO>' + ALLTRIM(comercio.Cargo) + '</COMERCIO_CARGO>' + _S_				

		
		mUbicacion = ''											
		IF (!EMPTY(comercio.Nomb_calle)) THEN
			mUbicacion = mUbicacion + 'Calle ' + ALLTRIM(comercio.Nomb_calle)
		ENDIF
		
		IF (!EMPTY(comercio.Puerta)) THEN
			mUbicacion = mUbicacion + ', Puerta ' + ALLTRIM(comercio.Puerta)
		ENDIF
		
		IF (!EMPTY(comercio.Piso)) THEN
			mUbicacion = mUbicacion + ', Piso ' + ALLTRIM(comercio.Piso)
		ENDIF
						
		IF (!EMPTY(comercio.Depto)) THEN
			mUbicacion = mUbicacion + ', Depto. ' + ALLTRIM(comercio.Depto)
		ENDIF
				
		
		mDatos = mDatos + '<COMERCIO_UBICACION>' + ALLTRIM(mUbicacion) + '</COMERCIO_UBICACION>' + _S_
		mDatos = mDatos + '<COMERCIO_LOCALIDAD>' + ALLTRIM(comercio.Nomb_loc) + ' (' + ALLTRIM(comercio.Cod_post) + ')' +'</COMERCIO_LOCALIDAD>' + _S_
		
		
		* Le aviso al usuario si el comercio esta dado de baja
		IF !(EMPTY(comercio.fec_baja))
			mDatos = mDatos + '<COMERCIO_ESTADO>BAJA</COMERCIO_ESTADO>'
		ELSE
			mDatos = mDatos + '<COMERCIO_ESTADO>OK</COMERCIO_ESTADO>'
		ENDIF				
	

	
	
		* I open the table contrib
		IF (!USED('contrib')) THEN
			=UseT('recur\contrib')
		ENDIF
		SELECT contrib
		SET ORDER TO 1
		SCATTER MEMVAR BLANK
	

		m.cuit1 = 0
		m.cuit2 = 0
		=FContVin(pTipoImponible, pNroComercio,.F.,.F.,@m.cuit1,@m.cuit2)
		
		* Si el imponible es un contribuyente y no tiene ninguna entrada en VINC_IMP ==> ese mismo
		*	contribuyente se toma c�mo titular titular.							
		IF m.cuit1 <= 0 AND pTipoImponible = 'N'
			m.cuit1 = pNroComercio
		ENDIF
		
		IF m.cuit1>0
			IF SEEK(STR(m.cuit1,10,0),'contrib')									
				mDatos = mDatos + '<CONTRIBUYENTE_NOMBRE>' + ALLTRIM(contrib.Nomb_cont) + '</CONTRIBUYENTE_NOMBRE>' + _S_							
				mDatos = mDatos + '<CONTRIBUYENTE_CUIT>' + ALLTRIM(STR(contrib.Cuit)) + '</CONTRIBUYENTE_CUIT>' + _S_			
			ENDIF
		ENDIF
	
		
	ELSE	
		mDatos = mDatos + '<COMERCIO_ESTADO>INEXISTENTE</COMERCIO_ESTADO>'
	ENDIF
	
	
	mResultado_XML = '<?xml version="1.0" encoding="UTF-8"?>' + _S_
	mResultado_XML = '<COMERCIO>' + _S_ + mDatos + '</COMERCIO>'
	
	
	RETURN ALLTRIM(mResultado_XML)
		 
ENDPROC