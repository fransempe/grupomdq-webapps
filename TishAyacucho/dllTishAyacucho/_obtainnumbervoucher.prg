
* _ObtainNumberVoucher:
* This function returns the group of the voucher and the number of the voucher 
PROCEDURE _ObtainNumberVoucher(pRecurso as String, pNumberComercio as Integer, pYear as Integer, pQuota as Integer) as String
PRIVATE mIndex as String
PRIVATE mResult as String


	* I open the table com_ddjj
	IF (!USED('rec_cc')) THEN
		=UseT('recur\rec_cc')
	ENDIF
	SELECT rec_cc
	SCATTER MEMVAR BLANK


	* I create the index
	mIndex = ALLTRIM(pRecurso) + 'C' + STR(pNumberComercio, 10, 0) + STR(pYear, 4, 0) + STR(pQuota, 3, 0)

			
	* I seek the voucher
	mResult = ''
	IF SEEK(mIndex, 'rec_cc') THEN	
		mResult = ALLTRIM(STR(rec_cc.Grupo_Comp)) + '|' + ALLTRIM(STR(rec_cc.Nro_Comp))
	ENDIF		
	 
	 
	RETURN ALLTRIM(mResult)
ENDPROC