

* _ObtenerListadoREC_CCC:
* I obtain the paid of the comercio
PROCEDURE  _ObtenerListadoREC_CCC(pRecurso as String, pNumberComercio as Number) as String
	PRIVATE mXML_AUX as String
	PRIVATE mYearBegin as Number
	PRIVATE mYearEnd as Number
	
	
	mYearBegin = (YEAR(DATE()) -100)
	mYearEnd = YEAR(DATE())

	

	* I open the table rec_ccc
	=UseT('recur\rec_ccc')
	SELECT rec_ccc
	SCATTER MEMVAR BLANK

	

	* I obtain the paid of the comercio
	mXML_AUX = '<REC_CCC>' + _S_
	SET  KEY TO RANGE  pRecurso + 'C' + STR(pNumberComercio, 10, 0) + STR(mYearBegin, 4, 0), pRecurso + 'C' + STR(pNumberComercio, 10, 0) + STR(mYearEnd, 4, 0) 

	
	GO TOP IN rec_ccc
	DO WHILE NOT EOF('rec_ccc')		
	
		mXML_AUX = mXML_AUX + '<RENGLONES_REC_CCC>'	+ _S_ 
		mXML_AUX = mXML_AUX + '<DDV_NROMOV>' + ALLTRIM(STR(rec_ccc.nro_mov)) + '</DDV_NROMOV>' + _S_ 
		mXML_AUX = mXML_AUX + '<DDV_REC>' + ALLTRIM(pRecurso) + '</DDV_REC>' + _S_
		mXML_AUX = mXML_AUX + '<DDV_ANIO>' + ALLTRIM(STR(rec_ccc.anio)) + '</DDV_ANIO>' + _S_ 
		mXML_AUX = mXML_AUX + '<DDV_CUOTA>' + ALLTRIM(STR(rec_ccc.cuota)) + '</DDV_CUOTA>' + _S_ 
		mXML_AUX = mXML_AUX + '<DDV_CONCEPTO>' + ALLTRIM(rec_ccc.conc_cc) + '</DDV_CONCEPTO>' + _S_ 
		mXML_AUX = mXML_AUX + '<DDV_DEPLAN />' + _S_ 
		mXML_AUX = mXML_AUX + '<DDV_FECHAVENC>' + ALLTRIM(DTOC(rec_ccc.fecven_mov)) + '</DDV_FECHAVENC>' + _S_ 
		mXML_AUX = mXML_AUX + '<DDV_CONDESPECIAL />' + _S_
		mXML_AUX = mXML_AUX + '<DDV_IMPORTEORIGEN>' + ALLTRIM(rec_ccc.imp_mov) + '</DDV_IMPORTEORIGEN>' + _S_
		mXML_AUX = mXML_AUX + '<DDV_IMPORTERECARGO>0.00</DDV_IMPORTERECARGO>' + _S_ 
		mXML_AUX = mXML_AUX + '<DDV_IMPORTETOTAL>' + ALLTRIM(rec_ccc.imp_mov) + '</DDV_IMPORTETOTAL>' + _S_ 
		mXML_AUX = mXML_AUX + '</RENGLONES_REC_CCC>'
	
		 
		SKIP IN rec_ccc
	ENDDO
	SET KEY TO
	mXML_AUX = mXML_AUX + '</REC_CCC>' + _S_		
	
 	 
	RETURN ALLTRIM(mXML_AUX)
ENDPROC