
*	Call_PRG_Excluded: 
*	This function calls or uses prg excluded
PROCEDURE Call_PRG_Excluded(pNamePRG as String, pNameFunction as String) as String
	PRIVATE mResult_XML as String
	PRIVATE mPath_Function as String
	PRIVATE mOld_Path_Function as String
	

		
		* I obtain the path of the function and I seteo the new path 
		mPath_Function = "'" + ALLTRIM(_SISTEMA) + ALLTRIM(m.sistema) + "\" + ALLTRIM(m.modulo) + "\TISH\'"
		mOld_Path_Function = SET("Path")
		SET PATH TO &mPath_Function 


		* I check what exist the car
		* If exist the use			
		mResult_XML = ''
		IF FILE(ALLTRIM(pNamePRG) + '.FXP')				
			pNameFunction = ALLTRIM(pNameFunction) + '()'
			mResult_XML = &pNameFunction
		ENDIF
		
		
		* Seteo the path previous
		SET PATH TO &mOld_Path_Function 
		
	RETURN ALLTRIM(mResult_XML)

ENDPROC