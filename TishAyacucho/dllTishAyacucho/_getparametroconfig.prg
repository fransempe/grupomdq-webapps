***********************************************************************************
*	M�todo _getParametroConfig:  
*	Est� m�todo busca en la tabla asa/config recibiendo como par�metros los valores
* 	Sistema y Nomb_Par respectivamente.
***********************************************************************************
PROCEDURE _getParametroConfig (p_strSistema as String, p_strParametro as String) as String
	PRIVATE strIndice as String
	PRIVATE strValor as String


	strValor = ''
	IF (!USED('config')) THEN
		=UseT('asa\config')
	ENDIF
	SELECT config
	
	SET ORDER TO PRIMARIO IN config
	strIndice = PADR(UPPER(ALLTRIM(p_strSistema)), 8, ' ') + UPPER(ALLTRIM(p_strParametro)) 
	IF (SEEK(strIndice, 'config')) THEN
		strValor = ALLTRIM(config.Cont_Par)
	ENDIF
	
	RETURN ALLTRIM(strValor)
ENDPROC
***********************************************************************************	