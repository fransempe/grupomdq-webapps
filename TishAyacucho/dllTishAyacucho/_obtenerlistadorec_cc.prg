
* _ObtenerListadoREC_CC:
* I obtain the debt of the comercio
PROCEDURE  _ObtenerListadoREC_CC(pRecurso as String, pNumberComercio as Number, pYearBegin as Number, pYearEnd as Number) as String
	PRIVATE mXML_AUX as String


	* I open the table rec_cc
	=UseT('recur\rec_cc')
	SELECT rec_cc
	SCATTER MEMVAR BLANK


	* I open the table comp_ren
	=UseT('recur\comp_ren')
	SELECT comp_ren
	SCATTER MEMVAR BLANK



	* I obtain the debt of the comercio
	mXML_AUX = '<REC_CC>' + _S_
	SELECT rec_cc
	SCATTER MEMVAR BLANK
	SET  KEY TO RANGE  pRecurso + 'C' + STR(pNumberComercio, 10, 0) + STR(pYearBegin, 4, 0), pRecurso + 'C' + STR(pNumberComercio, 10, 0) + STR(pYearEnd, 4, 0) 
	GO TOP IN rec_cc
	DO WHILE NOT EOF('rec_cc')		


		mXML_AUX = mXML_AUX + '<RENGLONES_REC_CC>'
		mXML_AUX = mXML_AUX + '<ANIO>' + ALLTRIM(STR(rec_cc.anio)) + '</ANIO>'  
		mXML_AUX = mXML_AUX + '<PERIODO>' + ALLTRIM(STR(rec_cc.cuota)) + '</PERIODO>' + _S_
		mXML_AUX = mXML_AUX + '<NRO_MOV>' + ALLTRIM(STR(rec_cc.nro_mov)) + '</NRO_MOV>' + _S_
		mXML_AUX = mXML_AUX + '<CONCEPTO>' + ALLTRIM(rec_cc.conc_cc) + '</CONCEPTO>' + _S_				
		mXML_AUX = mXML_AUX + '<FECHA_VTO>' + ALLTRIM(DTOC(rec_cc.fecven_mov)) + '</FECHA_VTO>' + _S_
		mXML_AUX = mXML_AUX + '<IMPORTE>' + ALLTRIM(rec_cc.imp_mov) + '</IMPORTE>' + _S_
	
		
		IF (rec_cc.fecven_mov >= DATE()) THEN
			IF (rec_cc.grupo_comp > 0) AND (rec_cc.nro_comp > 0) THEN
									
				IF (SEEK (STR(rec_cc.grupo_comp, 2, 0) + STR(rec_cc.nro_comp, 9, 0), 'comp_ren')) THEN			
								
					IF (comp_ren.Recurso = pRecurso) AND (comp_ren.Tipo_imp = 'C') AND (comp_ren.Nro_imp = pNumberComercio) ;
						AND (comp_ren.Anio = rec_cc.Anio) AND (comp_ren.Cuota = rec_cc.Cuota) THEN
						
												
 						mXML_AUX = mXML_AUX + '<NRO_COMPROBANTE>' + ALLTRIM(STR(comp_ren.Grupo_comp)) + '|' + ALLTRIM(STR(comp_ren.Nro_comp)) + '</NRO_COMPROBANTE>' + _S_
						mXML_AUX = mXML_AUX + '<ESTADO>' + 'Adeudado' + '</ESTADO>' + _S_				
					ELSE
						mXML_AUX = mXML_AUX + '<NRO_COMPROBANTE></NRO_COMPROBANTE>' + _S_				
						mXML_AUX = mXML_AUX + '<ESTADO>' + 'No disponib.' + '</ESTADO>' + _S_												     					
					endif
					
				ELSE
					mXML_AUX = mXML_AUX + '<NRO_COMPROBANTE></NRO_COMPROBANTE>' + _S_
					mXML_AUX = mXML_AUX + '<ESTADO>' + 'No disponib.' + '</ESTADO>' + _S_				
				ENDIF	
			ELSE
				mXML_AUX = mXML_AUX + '<NRO_COMPROBANTE></NRO_COMPROBANTE>' + _S_
				mXML_AUX = mXML_AUX + '<ESTADO>' + 'Adeudado' + '</ESTADO>' + _S_				
			ENDIF
			
					
		ELSE
			mXML_AUX = mXML_AUX + '<NRO_COMPROBANTE></NRO_COMPROBANTE>' + _S_
			mXML_AUX = mXML_AUX + '<ESTADO>' + 'Adeudado' + '</ESTADO>' + _S_				
		ENDIF
		

		
	
		
		mXML_AUX = mXML_AUX + '</RENGLONES_REC_CC>'
 

	 
		SKIP IN rec_cc
	ENDDO
	SET KEY TO
	mXML_AUX = mXML_AUX + '</REC_CC>' + _S_		
	
 	 
	RETURN ALLTRIM(mXML_AUX)
ENDPROC