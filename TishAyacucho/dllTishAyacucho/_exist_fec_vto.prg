
* _Exist_Fec_Vto:
* Description:
PROCEDURE _Exist_Fec_Vto(pResource as String, pYear as Number, pQuota as Number) as Boolean
PRIVATE mResult as Boolean



	* I open the table rec_cc
	IF (!USED('rec_cc')) THEN
		=UseT('recur\rec_cc')
	ENDIF
	SELECT rec_cc
	SCATTER MEMVAR BLANK


	mResult = .T.
	LOCATE FOR RECURSO = pResource AND TIPO_IMP = 'C' AND ANIO = pYear AND CUOTA = pQuota
	IF (FOUND()) THEN
		mResult = .T.
	ELSE
	
	
		* I open the table rec_ccc
		IF (!USED('rec_ccc')) THEN
			=UseT('recur\rec_ccc')
		ENDIF
		SELECT rec_ccc
		SCATTER MEMVAR BLANK
		
		LOCATE FOR RECURSO = pResource AND TIPO_IMP = 'C' AND ANIO = pYear AND CUOTA = pQuota
		IF (FOUND()) THEN
			mResult = .T.
		ELSE
			mResult = .F.
		ENDIF
	ENDIF
	 
	 
	RETURN mResult
ENDPROC