
PROCEDURE _getRubrosByComercio(p_strNroComercio as String) as String
	PRIVATE strXML as String
	PRIVATE i as Number
	PRIVATE strRubro_aux as String
	PRIVATE numRubro_C as Number


	IF (!USED('comercio')) THEN
		=UseT('recur\comercio')
	ENDIF

	IF (!USED('rubs_com')) THEN
		=UseT('recur\rubs_com')
	ENDIF


	strXML = ''
	strXML = strXML + '<?xml version="1.0" encoding="utf-8"?>' + _S_
	strXML = strXML + '<ArrayOfRubro xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">' + _S_


	SELECT comercio
	SET ORDER TO Primario IN comercio
	IF (SEEK(STR(VAL(ALLTRIM(p_strNroComercio)),10 , 0), 'comercio')) THEN
	
		FOR i = 1 TO 10
		
			strRubro_aux = 'comercio.Rubro' + ALLTRIM(STR(i))
			IF (!EMPTY(&strRubro_aux)) THEN
				numRubro_C = &strRubro_aux
							
				SELECT rubs_com
				SET ORDER TO Primario IN rubs_com
				IF (SEEK(STR(numRubro_C, 7, 0), 'rubs_com')) THEN
				
					strXML = strXML +  '<rubro>'				+ _S_
					strXML = strXML +  '<codigo>' 				+ ALLTRIM(STR(rubs_com.rubro_c))	+ '</codigo>' + _S_			
					strXML = strXML +  '<descripcion>' 			+ ALLTRIM(rubs_com.dsc_rubc)		+ '</descripcion>' + _S_			
					strXML = strXML +  '<alicuotaActual>' 		+ ALLTRIM(STR(rubs_com.porc_iddjj))		+ '</alicuotaActual>' + _S_								
					strXML = strXML +  '<alicuotaAnterior>' 	+ ALLTRIM(STR(rubs_com.porc_ant))		+ '</alicuotaAnterior>' + _S_								
					strXML = strXML +  '</rubro>' + _S_		
				
				ENDIF
			ENDIF
		
			LOOP 
		ENDFOR
	ENDIF
	
	 
			 	
	strXML = strXML +  '</ArrayOfRubro >' + _S_			
	RETURN ALLTRIM(strXML) 
ENDPROC