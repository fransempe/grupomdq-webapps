
* _ObtainDetailDDJJ:
* This function returns the detail of a DDJJ
PROCEDURE _ObtainDetailDDJJ(pNumberComercio as Number, pYear as Integer, pQuota as Integer) as String				
	LOCAL oErr AS Exception 
	PRIVATE mIndex as String
	PRIVATE mXML_AUX as String
	PRIVATE mNameRubro as String	 



	TRY
	
		* I open the table com_ddjj
		=UseT('recur\com_ddjj')
		SELECT com_ddjj
		SCATTER MEMVAR BLANK

		
		* I create the index
		mIndex = STR(pNumberComercio, 10, 0) + STR(pYear, 4, 0) + STR(pQuota, 3, 0)

	
		
		* I obtain the detail of the DDJJ
		mXML_AUX = '<DETAIL_DDJJ>' + _S_
		IF SEEK(mIndex, 'com_ddjj') THEN	

			DO WHILE NOT EOF('com_ddjj') AND (com_ddjj.nro_com = pNumberComercio) AND (com_ddjj.anio = pYear) AND (com_ddjj.cuota = pQuota)
			
			
				* I obtain the name of the rubro				
				mNameRubro = ''
				mNameRubro = _ObtainNameRubro(com_ddjj.rubro_c)
				mNameRubro = '(' + ALLTRIM(STR(com_ddjj.rubro_c)) + ') ' + ALLTRIM(mNameRubro)

				*I create the row
				mXML_AUX = mXML_AUX + '<RENGLONES_DDJJ>'				
				mXML_AUX = mXML_AUX + '<RUBRO>' + ALLTRIM(mNameRubro) + '</RUBRO>'  
				mXML_AUX = mXML_AUX + '<EMPLEADOS>' + ALLTRIM(STR(com_ddjj.nro_emplp)) + '</EMPLEADOS>' + _S_
				mXML_AUX = mXML_AUX + '<MONTO>' + ALLTRIM(STR(com_ddjj.mont_ddjjp, 13, 2)) + '</MONTO>' + _S_				
				mXML_AUX = mXML_AUX + '<FECHA_PRESENTACION_DDJJ>' + ALLTRIM(DTOC(com_ddjj.fech_ddjjp)) + '</FECHA_PRESENTACION_DDJJ>' + _S_				
				mXML_AUX = mXML_AUX + '</RENGLONES_DDJJ>'
				
				SKIP IN com_ddjj
			ENDDO
			
						
		ENDIF		
		mXML_AUX = mXML_AUX + '</DETAIL_DDJJ>' + _S_	
	
	
	CATCH TO oErr
	   mXML_AUX = '<ERROR>' + ALLTRIM(oErr.Message) + '</ERROR>'
	FINALLY
	   	*CLEAR EVENTS
	ENDTRY
 
 
	RETURN ALLTRIM(mXML_AUX)
ENDPROC