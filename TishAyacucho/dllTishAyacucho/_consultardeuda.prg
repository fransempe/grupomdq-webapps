PROCEDURE _ConsultarDeuda (AuxArchDeud as string, TipoImp as String, NroImp as double, ambiente as Logical) as String
	EXTERNAL ARRAY vec_int
	PRIVATE AuxTodoOK, AuxNoImpr_SiNoDDJJ, AuxComDDJJParam
	AuxNoImpr_SiNoDDJJ = .F.
	AuxComDDJJParam = ''
		
			
	SET ORDER TO primario IN rec_cc
	AuxTodoOK = ambiente
	IF TYPE('TipoImp') = 'C' AND AuxTodoOK
		P_TipoImponible = SUBSTR(TipoImp,1,1)
		IF TYPE('NroImp') = 'N'
			IF NroImp <= 9999999999
				P_NroImponible = NroImp
			ELSE
				AuxTodoOK = .F.
				M_Mensaje = 'N�mero de imponible no v�lido. Debe ser menor que 9.999.999.999.'
			ENDIF
		ELSE
			AuxTodoOK = .F.
			M_Mensaje = 'Error de transferencia de datos en el N�mero de Imponible.'
		ENDIF
	ELSE
		AuxTodoOK = .F.
		M_Mensaje = 'Error de transferencia de datos en el Tipo de Imponible.'
	ENDIF
	

	IF AuxTodoOK	
		PRIVATE AuxOrigPlanCCE, AuxRecPlanCCE, AuxTotPlanCCE
		PRIVATE AuxOrigPlanSCE, AuxRecPlanSCE, AuxTotPlanSCE
		PRIVATE AuxOrigNorCCE, AuxRecNorCCE, AuxTotNorCCE				
		PRIVATE AuxOrigNorSCE, AuxRecNorSCE, AuxTotNorSCE
		AuxOrigPlanCCE = 0
		AuxRecPlanCCE = 0
		AuxTotPlanCCE = 0
		AuxOrigPlanSCE = 0
		AuxRecPlanSCE = 0
		AuxTotPlanSCE = 0
		AuxOrigNorCCE = 0
		AuxRecNorCCE = 0
		AuxTotNorCCE = 0		
		AuxOrigNorSCE = 0
		AuxRecNorSCE = 0
		AuxTotNorSCE = 0
	ENDIF

	* En Ayacucho no se debe poder imprimir desde la web las cuotas de Seguridad
	*	e Higiene para las cuales no se ha presentado previamente la DDJJ. Se utiliza
	*	el siguiente par�metro para indicar este comportamiento.
	IF AuxTodoOK
		AuxComDDJJParam = Get_Para ('RECUR','NOIMPR_SINODDJJ')
		IF UPPER(ALLTRIM(AuxComDDJJParam)) == 'S' OR UPPER(ALLTRIM(AuxComDDJJParam)) == 'SI'
			AuxNoImpr_SiNoDDJJ = .T.
		ENDIF			
	ENDIF
	
	* Buscar y agregar al cursor CompAVenc los comprobantes a�n no vencidos que est�n
	*	sin cancelar
	IF AuxTodoOK
		_CreditoCtaCte = 0
		AuxTodoOK = CargarCompAVenc()
	ENDIF
	
	* Buscar y agregar al cursor CuotasVenc los movimientos de cuenta corriente sin
	*	cancelar ya vencidos que no est�n incluidos en ninguno de los comprobantes
	*	por vencer
	IF AuxTodoOK
		AuxTodoOK = CargarCuotasVenc()
	ENDIF
	
	IF AuxTodoOK
		AuxTodoOK = AgregarDatosCuenta(@AuxArchDeud)
		IF AuxTodoOK
			IF (RECCOUNT('CuotasVenc') = 0) AND (RECCOUNT('CompAVenc') = 0)
				M_Mensaje = 'La cuenta no registra deuda al ' + DTOC(DATE()) + '.'
			ELSE
				AgregarCuotasVenc(@AuxArchDeud)
				AgregarProxCompAVenc(@AuxArchDeud)
				AgregarImporteCreditoCtaCte(@AuxArchDeud)
				*AgregarOtrosCompAVenc(@AuxArchDeud)
				AgregarReferencias(@AuxArchDeud)				
			ENDIF
		ENDIF
	ENDIF

	RETURN AuxTodoOK
ENDPROC
