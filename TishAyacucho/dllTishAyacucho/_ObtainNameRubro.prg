
* _ObtainNameRubro:
* This function returns the name of a Rubro
PROCEDURE _ObtainNameRubro(pNumberRubro as Number) as String				
	LOCAL oErr AS Exception 
	PRIVATE mIndex as String
	PRIVATE mNameRubro as String
	

	TRY
	
		* I open the table rubs_com
		=UseT('recur\rubs_com')
		SELECT rubs_com
		SCATTER MEMVAR BLANK

		
		* I create the index
		mIndex = STR(pNumberRubro, 7, 0) 

				
		* I obtain the name of the Rubro
		mNameRubro = ''
		IF SEEK(mIndex, 'rubs_com') THEN	
			mNameRubro = ALLTRIM(rubs_com.dsc_rubc)
		ENDIF		

	
	
	CATCH TO oErr
	   mNameRubro = ALLTRIM(oErr.Message)
	FINALLY
	   	*CLEAR EVENTS
	ENDTRY
 
 
	RETURN ALLTRIM(mNameRubro)
ENDPROC