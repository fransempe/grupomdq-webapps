﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace entidades
{
    [XmlRootAttribute("rubro", Namespace = "", IsNullable = false)]
    public class rubro {
        private string strCodigo;
        private string strDescripcion;
        private double strAlicuotaActual;
        private string strAlicuota2018;
        private string strAlicuota2019;

        public string codigo {
            get { return this.strCodigo.Trim(); }
            set { this.strCodigo = value.Trim(); }
        }

        public string descripcion {
            get { return this.strDescripcion.Trim(); }
            set { this.strDescripcion = value.Trim(); }
        }

        public double alicuotaActual {
            get { return this.strAlicuotaActual; }
            set { this.strAlicuotaActual = value; }
        }

        public string alicuota2018
        {
            get { return this.strAlicuota2018.Trim(); }
            set { this.strAlicuota2018 = value.Trim(); }
        }

        public string alicuota2019
        {
            get { return this.strAlicuota2019.Trim(); }
            set { this.strAlicuota2019 = value.Trim(); }
        }


        public rubro() {
            this.strCodigo = "";
            this.strDescripcion = "";
            this.alicuotaActual = 0;
            this.alicuota2018 = "";
            this.alicuota2019 = "";
        }

    }
}
