﻿

Imports iTextSharp.text
Imports iTextSharp.text.pdf


Public Class clsPDFComprobanteDDJJ

#Region "Variables"

    Private Structure SColumnas
        Dim Rubro_Code As Integer
        Dim Rubro_Description As Integer
        Dim Rubro_Employees As Integer
        Dim Rubro_Amount As Integer
        Dim Rubro_Aliquot As Integer


        Dim Comprobante_Recurso As Integer
        Dim Comprobante_Anio As Integer
        Dim Comprobante_Cuota As Integer
        Dim Comprobante_Concepto As Integer
        Dim Comprobante_ImporteOrigen As Integer
        Dim Comprobante_ImporteRecargo As Integer
        Dim Comprobante_ImporteTotal As Integer

    End Structure

    Private dsDatosConstancia As DataSet
    Private dsDatosComprobante As DataSet
    Private mNombreMunicipalidad As String
    Private mRutaFisica As String
    Private mVistaDatos As DataView


    'Variables para Generar el LOGO, SOLO PARA RAFAM
    Private mTipoConexion As String
    Private mOrganismo As String
    Private mNombreOrganismo As String


    Private mCodigoMunicipalidad As String

    Private mPeriod_DDJJ As String

    Private mIsRectificativa As Boolean
    Private mIsNew_DDJJ As Boolean
    Private mAdd_AND_Voucher As Boolean


    Private dsDatosMunicipalidad As DataSet



    Private lstRubros As New List(Of entidades.rubro)

#End Region

#Region "Propertys"

    Public Property DatosConstancia() As DataSet
        Get
            Return Me.dsDatosConstancia
        End Get
        Set(ByVal value As DataSet)
            Me.dsDatosConstancia = value
        End Set
    End Property


    Public Property DatosComprobante() As DataSet
        Get
            Return Me.dsDatosComprobante
        End Get
        Set(ByVal value As DataSet)
            Me.dsDatosComprobante = value
        End Set
    End Property


    Public WriteOnly Property NombreMunicipalidad() As String
        Set(ByVal value As String)
            mNombreMunicipalidad = value.Trim
        End Set
    End Property

    Public WriteOnly Property RutaFisica() As String
        Set(ByVal value As String)
            mRutaFisica = value.Trim
        End Set
    End Property

    Public WriteOnly Property TipoConexion() As String
        Set(ByVal value As String)
            mTipoConexion = value
        End Set
    End Property

    Public WriteOnly Property Organismo() As String
        Set(ByVal value As String)
            mOrganismo = value
        End Set
    End Property


    Public WriteOnly Property NombreOrganismo() As String
        Set(ByVal value As String)
            mNombreOrganismo = value
        End Set
    End Property


    Public WriteOnly Property CodigoMunicipalidad() As String
        Set(ByVal value As String)
            mCodigoMunicipalidad = value
        End Set
    End Property


    Public WriteOnly Property Period_DDJJ() As String
        Set(ByVal value As String)
            Me.mPeriod_DDJJ = value.ToString.Trim
        End Set
    End Property

    Public WriteOnly Property IsRectificativa() As Boolean
        Set(ByVal value As Boolean)
            Me.mIsRectificativa = value
        End Set
    End Property

    Public WriteOnly Property IsNew_DDJJ() As Boolean
        Set(ByVal value As Boolean)
            Me.mIsNew_DDJJ = value
        End Set
    End Property


    Public WriteOnly Property Add_AND_Voucher() As Boolean
        Set(ByVal value As Boolean)
            Me.mAdd_AND_Voucher = value
        End Set
    End Property


    Public WriteOnly Property DatosMunicipalidad() As DataSet
        Set(ByVal value As DataSet)
            Me.dsDatosMunicipalidad = value
        End Set
    End Property

    Public WriteOnly Property Rubros() As List(Of entidades.rubro)        
        Set(ByVal value As List(Of entidades.rubro))
            Me.lstRubros = value
        End Set
    End Property



#End Region


#Region "Constructor"

    Public Sub New()


        Me.dsDatosConstancia = Nothing
        Me.dsDatosComprobante = Nothing
        Me.mNombreMunicipalidad = ""
        Me.mRutaFisica = ""
        Me.mVistaDatos = Nothing


        'Variables para Generar el LOGO, SOLO PARA RAFAM
        Me.mTipoConexion = ""
        Me.mOrganismo = ""
        Me.mNombreOrganismo = ""


        Me.mCodigoMunicipalidad = ""
        Me.Period_DDJJ = ""


        Me.mIsNew_DDJJ = False
        Me.mAdd_AND_Voucher = False


    End Sub



    Protected Overrides Sub Finalize()

        Me.dsDatosConstancia = Nothing
        Me.dsDatosComprobante = Nothing
        Me.mNombreMunicipalidad = ""
        Me.mRutaFisica = ""
        Me.mVistaDatos = Nothing


        'Variables para Generar el LOGO, SOLO PARA RAFAM
        Me.mTipoConexion = ""
        Me.mOrganismo = ""
        Me.mNombreOrganismo = ""


        Me.mCodigoMunicipalidad = ""
        Me.Period_DDJJ = ""

        Me.mIsNew_DDJJ = False
        Me.mAdd_AND_Voucher = False


        MyBase.Finalize()
    End Sub

#End Region




    Public Sub CrearPDF()
        Dim mDocumentoPDF As Document = New Document(iTextSharp.text.PageSize.A4, 15, 15, 50, 50)
        Dim writer As PdfWriter = PdfWriter.GetInstance(mDocumentoPDF, New System.IO.FileStream(mRutaFisica.ToString, System.IO.FileMode.Create))


        writer.ViewerPreferences = PdfWriter.PageLayoutSinglePage
        mDocumentoPDF.Open()


        If (Me.mIsNew_DDJJ) Then
            Call Me.Constancia_CrearEncabezadoLogo(mDocumentoPDF, writer)
            Call Me.Constancia_CrearEncabezado(mDocumentoPDF, writer)
            Call Me.Constancia_TablaMovimientos(mDocumentoPDF, writer, 0)

            mDocumentoPDF.NewPage()
        End If



        If (Me.mAdd_AND_Voucher) Then
            If (dsDatosComprobante.Tables("comprobante").Rows(0).Item("IMPORTE_TOTAL_1").ToString.IndexOf("-") = -1) Then
                For i = 0 To dsDatosComprobante.Tables("renglones").Rows.Count - 1
                    Call Me.CrearEncabezadoLogo(mDocumentoPDF, writer)
                    Call Me.CrearEncabezado(mDocumentoPDF, writer)
                    Call Me.TablaMovimientos(mDocumentoPDF, writer, 0)
                    Call Me.Talon(mDocumentoPDF, writer, 0)
                    Call Me.CrearBarCode(mDocumentoPDF, writer, 0)

                    mDocumentoPDF.NewPage()
                Next
            End If
        End If


        mDocumentoPDF.Close()
    End Sub

#Region "Crear PDF"


#Region "Constancia de Presentacion de DDJJ"

    Private Sub Constancia_CrearEncabezadoLogo(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mLogo As Image
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte

        Dim mNombreLogo As String
        Dim mLogoHeight As Single
        Dim mLogoWidth As Single
        Dim mLogoCoordenadaX As Single
        Dim mLogoCoordenadaY As Single
        Dim mLogo_AUX As Byte()



        'Obtengo el Nombre del Logo segun la CONEXION y Seteo propiedades
        If (mTipoConexion.ToString = "RAFAM") Then
            mNombreLogo = "logo_rafam.jpg"
            mLogoHeight = 60
            mLogoWidth = 60
            mLogoCoordenadaX = 50
            mLogoCoordenadaY = 745
        Else

            'Logo
            mLogo_AUX = clsTools.ObtenerLogo()
            mNombreLogo = "logo.jpg"
            'ZARATE
            mLogoHeight = 81
            mLogoWidth = 100
            mLogoCoordenadaX = 20
            mLogoCoordenadaY = 720

            'AYACUCHO
            'mLogoHeight = 50
            'mLogoWidth = 100
            'mLogoCoordenadaX = 20
            'mLogoCoordenadaY = 750

        End If



        'Agrego el Logo a PDF
        Try

            If (mTipoConexion.ToString = "RAFAM") Then
                mLogo = iTextSharp.text.Image.GetInstance(mNombreLogo.ToString)
            Else
                mLogo = iTextSharp.text.Image.GetInstance(mLogo_AUX)
            End If

            mLogo.SetAbsolutePosition(mLogoCoordenadaX, mLogoCoordenadaY)
            mLogo.ScaleAbsolute(mLogoWidth, mLogoHeight)
            mDocumentoPDF.Add(mLogo)
        Catch ex As Exception
        End Try



        'Nombre de La Municipalidad
        If (mTipoConexion.ToString = "RAFAM") Then
            cb = writer.DirectContent
            cb.BeginText()
            mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
            cb.SetFontAndSize(mFuente, 10)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mOrganismo.ToString, 105, 775, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mNombreOrganismo.ToString, 105, 765, 0)
            cb.EndText()
        End If







        'TITULO de la IMPRESION
        cb = writer.DirectContent
        cb.BeginText()
        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont


        cb.SetFontAndSize(mFuente, 12)
        If Not (Me.mIsRectificativa) Then
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Original", 530, 715, 0)
        Else
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Rectificativa", 515, 715, 0)
        End If


        cb.SetFontAndSize(mFuente, 15)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Constancia de presentación de la declaración jurada", 150, (775 - 30), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "de la Tasa a la Actividad Económica ", 176, (760 - 30), 0)
        cb.EndText()
    End Sub

    Private Sub Constancia_CrearEncabezado(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim mFuenteNegrita As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mValorY As Integer

        cb = writer.DirectContent

        'Rectangulo Contenedora 
        'cb.Rectangle( 15, 690, 565, 50)
        cb.Rectangle(15, 645, 565, 65)




        mFuente = FontFactory.GetFont(FontFactory.TIMES_ITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        mFuenteNegrita = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 10)


        cb.BeginText()




        'mValorY = 725
        mValorY = 725 - 30
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Titular: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosConstancia.Tables("COMERCIO").Rows(0).Item("CONTRIBUYENTE_NOMBRE").ToString.Trim, 60, mValorY, 0)

        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Nro. de Cuenta: ", 300, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosConstancia.Tables("COMERCIO").Rows(0).Item("COMERCIO_NUMERO").ToString.Trim, 370, mValorY, 0)







        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Razón Social: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosConstancia.Tables("COMERCIO").Rows(0).Item("COMERCIO_RAZONSOCIAL").ToString.Trim, 80, mValorY, 0)
        cb.SetFontAndSize(mFuente, 10)


        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Cuit: ", 300, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosConstancia.Tables("COMERCIO").Rows(0).Item("CONTRIBUYENTE_CUIT").ToString.Trim, 325, mValorY, 0)




        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Nombre de fantasía: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosConstancia.Tables("COMERCIO").Rows(0).Item("COMERCIO_NOMBREFANTASIA").ToString.Trim, 110, mValorY, 0)
        cb.SetFontAndSize(mFuente, 10)


        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Ubicación: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosConstancia.Tables("COMERCIO").Rows(0).Item("COMERCIO_UBICACION").ToString.Trim, 70, mValorY, 0)

        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Localidad: ", 300, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosConstancia.Tables("COMERCIO").Rows(0).Item("COMERCIO_LOCALIDAD").ToString.Trim, 350, mValorY, 0)


        cb.EndText()

    End Sub

    Private Sub Constancia_TablaMovimientos(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter, ByVal mIdComprobante As Integer)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mColumnas As SColumnas
        Dim mRenglon As Integer
        Dim mAmountTotal As Double
        Dim mAmountTotal_AUX As String
        Dim strImporteRenglon_aux As String

        cb = writer.DirectContent


        'Rectangulo Contenedora 
        'cb.Rectangle(15, 400, 565, 280)
        cb.Rectangle(15, (400 - 45), 565, 280)


        'Linea de Encabezado
        cb.SetLineWidth(1)
        'cb.MoveTo(15, 660)
        'cb.LineTo(580, 660)

        cb.MoveTo(15, (660 - 45))
        cb.LineTo(580, (660 - 45))


        'Linea de Encabezado 2
        'cb.MoveTo(15, 630)
        'cb.LineTo(580, 630)
        cb.MoveTo(15, (630 - 45))
        cb.LineTo(580, (630 - 45))



        'Linea de Encabezado 2 (actividades)
        cb.MoveTo(15, (645 - 45))
        cb.LineTo(410, (645 - 45))


        'Line de Vertical
        cb.SetLineWidth(1)
        cb.MoveTo(400, (400 - 45))
        cb.LineTo(400, (420 - 45))


        'Line de Vertical actividades
        cb.SetLineWidth(1)
        cb.MoveTo(410, (630 - 45))
        cb.LineTo(410, (660 - 45))



        'Linea superior
        cb.MoveTo(400, (420 - 45))
        cb.LineTo(580, (420 - 45))


        ''Linea Media
        'cb.MoveTo(300, 220)
        'cb.LineTo(580, 220)
        cb.Stroke()




        mColumnas.Rubro_Code = 35
        mColumnas.Rubro_Description = 60
        mColumnas.Rubro_Employees = 450
        mColumnas.Rubro_Amount = 500
        mColumnas.Rubro_Aliquot = 550



        cb.BeginText()
        Dim fuente As iTextSharp.text.pdf.BaseFont


        'mRenglon = (667 - 30)
        mRenglon = (667 - 45)



        'Filtro el DataSet
        mVistaDatos = New DataView(dsDatosConstancia.Tables("comprobante"))
        mVistaDatos.RowFilter = "Comprobante_Id = " & mIdComprobante.ToString


        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 12)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Período:", 80, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Me.mPeriod_DDJJ.ToString.Trim, 110, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Fecha de Presentación:", 405, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, Format(Now, "dd/MM/yyyy").ToString.Trim, 500, mRenglon, 0)


        'Actividades
        mRenglon = mRenglon - 18
        fuente = FontFactory.GetFont(FontFactory.TIMES_BOLD, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "ACTIVIDADES", (mColumnas.Rubro_Description + 150), mRenglon, 0)




        'mRenglon = mRenglon - 17
        mRenglon = mRenglon - 15
        fuente = FontFactory.GetFont(FontFactory.TIMES_BOLD, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Código", mColumnas.Rubro_Code, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Descripción", mColumnas.Rubro_Description + 140, mRenglon, 0)

        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Cantidad", mColumnas.Rubro_Employees, (mRenglon + 18), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "de", mColumnas.Rubro_Employees, (mRenglon + 9), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Empleados", mColumnas.Rubro_Employees, mRenglon, 0)

        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Monto", mColumnas.Rubro_Amount, (mRenglon + 10), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Declarado", mColumnas.Rubro_Amount, mRenglon, 0)

        'cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Alicuota (‰)", mColumnas.Rubro_Aliquot, mRenglon, 0)


        Dim i As Integer
        'mRenglon = mRenglon - 20
        mRenglon = mRenglon - 20
        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(fuente, 10)





        'Filtro el DataSet
        mVistaDatos = New DataView(dsDatosConstancia.Tables("RUBROS"))

        mAmountTotal = 0
        mAmountTotal_AUX = "0"
        If (mVistaDatos.Count > 0) Then
            For i = 0 To mVistaDatos.Count - 1
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(i).Row("RUBRO_CODE").ToString.Trim, mColumnas.Rubro_Code, mRenglon, 0)

                'Corto la descripción del rubro
                If (mVistaDatos.Item(i).Row("RUBRO_DESCRIPTION").ToString.Trim.Length > 60) Then
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mVistaDatos.Item(i).Row("RUBRO_DESCRIPTION").ToString.Trim.Substring(1, 60) & ".", mColumnas.Rubro_Description, mRenglon, 0)
                Else
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mVistaDatos.Item(i).Row("RUBRO_DESCRIPTION").ToString.Trim, mColumnas.Rubro_Description, mRenglon, 0)
                End If
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(i).Row("RUBRO_EMPLOYEES").ToString.Trim, mColumnas.Rubro_Employees, mRenglon, 0)


                strImporteRenglon_aux = mVistaDatos.Item(i).Row("RUBRO_AMOUNT").ToString.Trim
                strImporteRenglon_aux = strImporteRenglon_aux.Replace(".", ",")
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, Format(CDbl(strImporteRenglon_aux), "N2"), mColumnas.Rubro_Amount, mRenglon, 0)


                Dim alicuota As String = "0"
                Dim anio As String = Me.mPeriod_DDJJ.ToString.Trim.Substring(0, Me.mPeriod_DDJJ.ToString.Trim.Length - 2)
                Dim rubro As String = mVistaDatos.Item(i).Row("RUBRO_CODE").ToString.Trim

                'alicuota = Me.getAlicuota(anio, rubro)
                'cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, alicuota.Trim(), mColumnas.Rubro_Aliquot, mRenglon, 0)

                mAmountTotal_AUX = mVistaDatos.Item(i).Row("RUBRO_AMOUNT").ToString.Trim
                mAmountTotal_AUX = mAmountTotal_AUX.Replace(".", ",")
                mAmountTotal = mAmountTotal + CDbl(mAmountTotal_AUX.Trim)

                mRenglon = mRenglon - 15
            Next
        End If



        'Total
        fuente = FontFactory.GetFont(FontFactory.TIMES_BOLD, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 12)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "TOTAL: " & Format(mAmountTotal, "N2"), mColumnas.Rubro_Amount, (405 - 45), 0)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "TOTAL: " & mAmountTotal, mColumnas.Rubro_Amount, (405 - 45), 0)


        cb.EndText()

    End Sub

#End Region



#Region "Comprobante de Pago"

    Private Sub CrearEncabezadoLogo(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mLogo As Image
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte

        Dim mNombreLogo As String
        Dim mLogoHeight As Single
        Dim mLogoWidth As Single
        Dim mLogo_AUX As Byte()
        Dim mLogoCoordenadaX As Single
        Dim mLogoCoordenadaY As Single




        'Obtengo el Nombre del Logo segun la CONEXION y Seteo propiedades
        If (mTipoConexion.ToString = "RAFAM") Then
            mNombreLogo = "logo_rafam.jpg"
            mLogoHeight = 60
            mLogoWidth = 60
            mLogoCoordenadaX = 50
            mLogoCoordenadaY = 745


        Else
            'Logo
            mLogo_AUX = clsTools.ObtenerLogo()
            mNombreLogo = "logo.jpg"
            mLogoHeight = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoHeight").ToString.Trim)
            mLogoWidth = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoWidth").ToString.Trim)
            mLogoCoordenadaX = CSng(System.Configuration.ConfigurationManager.AppSettings("Comprobante_LogoCoordenadaX").ToString.Trim)
            mLogoCoordenadaY = CSng(System.Configuration.ConfigurationManager.AppSettings("Comprobante_LogoCoordenadaY").ToString.Trim)

        End If



        'Agrego el Logo a PDF
        Try
            If (mTipoConexion.ToString = "RAFAM") Then
                mLogo = iTextSharp.text.Image.GetInstance(mNombreLogo.ToString)
            Else
                mLogo = iTextSharp.text.Image.GetInstance(mLogo_AUX)
            End If
            mLogo.SetAbsolutePosition(mLogoCoordenadaX, mLogoCoordenadaY)
            mLogo.ScaleAbsolute(mLogoWidth, mLogoHeight)
            mDocumentoPDF.Add(mLogo)
        Catch ex As Exception
        End Try



        'Nombre de La Municipalidad
        If (mTipoConexion.ToString = "RAFAM") Then
            cb = writer.DirectContent
            cb.BeginText()
            mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
            cb.SetFontAndSize(mFuente, 10)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mOrganismo.ToString, 105, 775, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mNombreOrganismo.ToString, 105, 765, 0)
            cb.EndText()
        End If







        'TITULO de la IMPRESION
        cb = writer.DirectContent
        cb.BeginText()


        'Datos de la municipalidad
        If (dsDatosMunicipalidad IsNot Nothing) Then


            'Pie del logo
            mFuente = FontFactory.GetFont(FontFactory.TIMES, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
            cb.SetFontAndSize(mFuente, 8)


            Me.mNombreMunicipalidad = ""
            Me.mNombreMunicipalidad = clsTools.ObtenerNombreMunicipalidad()
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Municipalidad de " & Me.mNombreMunicipalidad.ToString.Trim, 75, 755, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Pcia. de Buenos Aires", 75, 745, 0)


            mFuente = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
            cb.SetFontAndSize(mFuente, 12)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Municipalidad de " & Me.mNombreMunicipalidad.ToString.Trim, 580, 790, 0)


            mFuente = FontFactory.GetFont(FontFactory.TIMES, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
            cb.SetFontAndSize(mFuente, 8)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_DIRECCION").ToString.Trim, 580, 775, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_TELEFONO").ToString.Trim, 580, 765, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_MAIL").ToString.Trim, 580, 755, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_WEB").ToString.Trim, 580, 745, 0)
        End If


        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 17)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Tasa a la Actividad Económica", 150, 760, 0)
        cb.EndText()
    End Sub

    Private Sub CrearEncabezado(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim mFuenteNegrita As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mValorY As Integer

        cb = writer.DirectContent

        'Rectangulo Contenedora 
        cb.Rectangle(15, 690, 565, 50)




        mFuente = FontFactory.GetFont(FontFactory.TIMES_ITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        mFuenteNegrita = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 10)


        cb.BeginText()




        mValorY = 725
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Titular: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosComprobante.Tables("imponible").Rows(0).Item("TITULAR").ToString, 70, mValorY, 0)
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Tipo de Cuenta: ", 430, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, ObtenerTipoCuenta(dsDatosComprobante.Tables("imponible").Rows(0).Item("TIPOIMPONIBLE").ToString.Trim), 500, mValorY, 0)


        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Domicilio: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosComprobante.Tables("imponible").Rows(0).Item("DOMICILIO").ToString, 70, mValorY, 0)
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Nro. de Cuenta: ", 430, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosComprobante.Tables("imponible").Rows(0).Item("NROIMPONIBLE").ToString, 500, mValorY, 0)


        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Localidad: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosComprobante.Tables("imponible").Rows(0).Item("LOCALIDAD").ToString, 70, mValorY, 0)


        cb.EndText()

    End Sub

    Private Sub TablaMovimientos(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter, ByVal mIdComprobante As Integer)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mColumnas As SColumnas
        Dim mRenglon As Integer


        cb = writer.DirectContent


        'Rectangulo Contenedora 
        cb.Rectangle(15, 200, 565, 480)


        'Linea de Encabezado
        cb.SetLineWidth(1)
        cb.MoveTo(15, 660)
        cb.LineTo(580, 660)


        'Linea de Encabezado 2
        cb.MoveTo(15, 645)
        cb.LineTo(580, 645)



        'Line de Vertical
        cb.SetLineWidth(1)
        cb.MoveTo(300, 200)
        cb.LineTo(300, 240)



        'Linea superior
        cb.MoveTo(300, 240)
        cb.LineTo(580, 240)


        'Linea Media
        cb.MoveTo(300, 220)
        cb.LineTo(580, 220)
        cb.Stroke()




        mColumnas.Comprobante_Recurso = 35
        mColumnas.Comprobante_Anio = 70
        mColumnas.Comprobante_Cuota = 105
        mColumnas.Comprobante_Concepto = 140
        mColumnas.Comprobante_ImporteOrigen = 400
        mColumnas.Comprobante_ImporteRecargo = 490
        mColumnas.Comprobante_ImporteTotal = 570


        cb.BeginText()
        Dim fuente As iTextSharp.text.pdf.BaseFont


        mRenglon = 667


        'Filtro el DataSet
        mVistaDatos = New DataView(dsDatosComprobante.Tables("comprobante"))
        mVistaDatos.RowFilter = "Comprobante_Id = " & mIdComprobante.ToString


        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Nº Comprobante:", 80, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("NROCOMP").ToString, 150, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Fecha de Emisión:", 440, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, dsDatosComprobante.Tables("comprobante").Rows(0).Item("FECHA_EMISION").ToString, 500, mRenglon, 0)


        mRenglon = mRenglon - 17
        fuente = FontFactory.GetFont(FontFactory.TIMES_BOLD, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Recurso", mColumnas.Comprobante_Recurso, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Año", mColumnas.Comprobante_Anio, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Cuota", mColumnas.Comprobante_Cuota, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Concepto", mColumnas.Comprobante_Concepto, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Importe Origen", mColumnas.Comprobante_ImporteOrigen, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Importe Recargo", mColumnas.Comprobante_ImporteRecargo, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Importe Total", mColumnas.Comprobante_ImporteTotal, mRenglon, 0)




        Dim i As Integer
        mRenglon = mRenglon - 20
        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(fuente, 8)





        'Filtro el DataSet
        mVistaDatos = New DataView(dsDatosComprobante.Tables("renglon"))
        mVistaDatos.RowFilter = "Renglones_Id = " & mIdComprobante.ToString



        If (mVistaDatos.Count > 0) Then
            For i = 0 To mVistaDatos.Count - 1
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(i).Row("RECURSO").ToString, mColumnas.Comprobante_Recurso, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(i).Row("ANIO").ToString, mColumnas.Comprobante_Anio, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(i).Row("CUOTA").ToString, mColumnas.Comprobante_Cuota, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mVistaDatos.Item(i).Row("CONCEPTO").ToString, mColumnas.Comprobante_Concepto, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, mVistaDatos.Item(i).Row("IMPORIGENRENG").ToString, mColumnas.Comprobante_ImporteOrigen, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, mVistaDatos.Item(i).Row("IMPRECARGOSRENG").ToString, mColumnas.Comprobante_ImporteRecargo, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, mVistaDatos.Item(i).Row("IMPTOTALRENG").ToString, mColumnas.Comprobante_ImporteTotal, mRenglon, 0)

                mRenglon = mRenglon - 15
            Next
        End If




        'Filtro el DataSet
        mVistaDatos = New DataView(dsDatosComprobante.Tables("comprobante"))
        mVistaDatos.RowFilter = "Comprobante_Id = " & mIdComprobante.ToString



        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "VENCIMIENTO", 400, 225, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_1").ToString, 400, 203, 0)


        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "TOTAL", 560, 225, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_1").ToString, 560, 203, 0)



        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(mFuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Contribuyente", 555, 190, 0)
        cb.EndText()


    End Sub

    Private Sub Talon(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter, ByVal mIdComprobante As Integer)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim mFuenteNegrita As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte

        Dim mRows As Integer = 25


        cb = writer.DirectContent



        'Rectangulo Contenedora 
        cb.Rectangle(15, 25, 300, 145)

        'Rectangulo Fecha Vencimiento
        cb.Rectangle(20, 130, 290, 20)


        'Rectangulos Importes
        cb.Rectangle(20, 70, 290, 53)


        'Primer linea de Cuadro de Importes
        cb.SetLineWidth(1)
        cb.MoveTo(20, 108)
        cb.LineTo(310, 108)


        'Segundo linea de Cuadro de Importes
        cb.SetLineWidth(1)
        cb.MoveTo(20, 89)
        cb.LineTo(310, 89)
        cb.Stroke()








        cb.BeginText()

        'Nombre de la Municipalidad 
        mFuente = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont

        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Municipalidad de " & mNombreMunicipalidad.ToString, 15, (160 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Comprobante de Pago", 15, (148 + mRows), 0)



        '------ Datos Rectangulo Talon --------
        'Asigno fuente
        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        mFuenteNegrita = FontFactory.GetFont(FontFactory.TIMES_BOLD, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(mFuente, 8)



        'Filtro el DataSet
        mVistaDatos = New DataView(dsDatosComprobante.Tables("comprobante"))
        mVistaDatos.RowFilter = "Comprobante_Id = " & mIdComprobante.ToString



        'Imprimo Datos
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Nº Comprobante:", 120, (132 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("NROCOMP").ToString, 185, (132 + mRows), 0)


        cb.SetFontAndSize(mFuenteNegrita, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "1º Vencimiento:", 55, (113 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_1").ToString, 108, (112 + mRows), 0)

        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "2º Vencimiento:", 235, (113 + mRows), 0)
        If (mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString.Trim = "") Then
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "--------", 283, (113 + mRows), 0)
        Else
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString, 283, (113 + mRows), 0)
        End If



        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Vencimiento", 50, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Origen", 120, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Recargos", 210, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Total", 290, (88 + mRows), 0)

        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "1º", 50, (73 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_ORIGEN1").ToString, 120, (73 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_RECARGO_1").ToString, 210, (73 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_1").ToString, 290, (73 + mRows), 0)


        'Segundo vencimiento
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "2º", 50, (53 + mRows), 0)
        If (mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString.Trim = "") Then
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "------", 120, (53 + mRows), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "------", 210, (53 + mRows), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "------", 290, (53 + mRows), 0)
        Else
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_ORIGEN2").ToString, 120, (53 + mRows), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_RECARGO_2").ToString, 210, (53 + mRows), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_2").ToString, 290, (53 + mRows), 0)
        End If



        cb.SetFontAndSize(mFuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Banco", 305, 15, 0)
        cb.EndText()








        '----------------- CUADRO MUNICIPALIDAD -----------
        'Rectangulo Contenedora 
        cb.Rectangle(320, 25, 260, 145)



        'Rectangulo Fecha Vencimiento
        cb.Rectangle(325, 130, 250, 20)



        'Rectangulos Importes
        cb.Rectangle(325, 70, 250, 53)



        'Primera Line de Cuadro de Importes
        cb.SetLineWidth(1)
        cb.MoveTo(325, 108)
        cb.LineTo(575, 108)


        'Segundo linea de Cuadro de Importes
        cb.SetLineWidth(1)
        cb.MoveTo(325, 89)
        cb.LineTo(575, 89)
        cb.Stroke()






        cb.BeginText()

        'Nombre de la Municipalidad 
        mFuente = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Municipalidad de " & mNombreMunicipalidad.ToString, 320, (160 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Comprobante de Pago", 320, (148 + mRows), 0)



        '------ Datos Rectangulo Talon --------
        'Asigno fuente
        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(mFuente, 8)


        'Imprimo Datos
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Nº Comprobante:", 410, (132 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("NROCOMP").ToString, 475, (132 + mRows), 0)
        cb.SetFontAndSize(mFuenteNegrita, 8)

        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "1º Vencimiento:", 360, (113 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_1").ToString, 410, (113 + mRows), 0)

        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "2º Vencimiento:", 500, (113 + mRows), 0)
        If (mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString.Trim = "") Then
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "--------", 550, (113 + mRows), 0)
        Else
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString, 550, (113 + mRows), 0)
        End If



        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Vencimiento", 355, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Origen", 420, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Recargos", 480, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Total", 550, (88 + mRows), 0)


        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "1º", 355, (73 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_ORIGEN1").ToString, 420, (73 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_RECARGO_1").ToString, 480, (73 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_1").ToString, 550, (73 + mRows), 0)


        'Segundo vencimiento
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "2º", 355, (53 + mRows), 0)
        If (mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString.Trim = "") Then
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "------", 420, (53 + mRows), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "------", 480, (53 + mRows), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "------", 550, (53 + mRows), 0)
        Else
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_ORIGEN2").ToString, 420, (53 + mRows), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_RECARGO_2").ToString, 480, (53 + mRows), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_2").ToString, 550, (53 + mRows), 0)
        End If




        cb.SetFontAndSize(mFuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Municipalidad", 555, 15, 0)
        cb.EndText()



    End Sub

    Private Sub CrearBarCode(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter, ByVal mIdComprobante As Integer)
        Dim mCelda As New iTextSharp.text.Cell
        Dim mTabla As New Table(2)
        Dim mCode39 As String
        Dim mCode39CoordenadaX As Single


        Dim cb As PdfContentByte
        cb = writer.DirectContent


        'Filtro el DataSet
        mVistaDatos = New DataView(dsDatosComprobante.Tables("comprobante"))
        mVistaDatos.RowFilter = "Comprobante_Id = " & mIdComprobante.ToString


        'Interlib 2of5
        Dim code25 As BarcodeInter25 = New BarcodeInter25
        code25.StartStopText = False
        code25.GenerateChecksum = False
        code25.Code = mVistaDatos.Item(0).Row("stringcodbarra").ToString
        Dim image25 As Image
        image25 = code25.CreateImageWithBarcode(cb, Nothing, Nothing)
        image25.SetAbsolutePosition(22, 30)
        mDocumentoPDF.Add(image25)


        'Code 39
        Dim code39 As New Barcode39
        Dim image39 As Image


        'Genero el Codigo de Barras para la Municipalidad segun la CONEXION
        If (mTipoConexion.ToString = "RAFAM") Then
            mCode39 = mCodigoMunicipalidad.ToString.Trim & _
                      mVistaDatos.Item(0).Row("NROCOMP").ToString.Substring(0, 3) & _
                      mVistaDatos.Item(0).Row("NROCOMP").ToString.Substring(4, 12) & _
                      mVistaDatos.Item(0).Row("NROCOMP").ToString.Substring(17, 1)

            'Codigo Verificador del BARCODE39
            mCode39 = mCode39 & "3"
            mCode39CoordenadaX = 335
        Else
            mCode39 = mVistaDatos.Item(0).Row("stringcodbarra").ToString.Substring((mVistaDatos.Item(0).Row("stringcodbarra").ToString.Length - 14))
            mCode39CoordenadaX = 370
        End If




        code39.Code = mCode39.ToString
        code39.StartStopText = False
        cb = writer.DirectContent
        image39 = code39.CreateImageWithBarcode(cb, Nothing, Nothing)
        image39.SetAbsolutePosition(mCode39CoordenadaX, 30)
        mDocumentoPDF.Add(image39)
    End Sub
#End Region

#End Region

    Private Function ObtenerTipoCuenta(ByVal mTipoCuenta As String) As String
        Select Case mTipoCuenta
            Case "I" : Return "INMUEBLE"
            Case "C" : Return "COMERCIO"
            Case "E", "O" : Return "CEMENTERIO"
            Case "V", "R" : Return "VEHICULOS"
            Case Else : Return ""
        End Select
    End Function




    Private Function getAlicuota(ByVal p_anio As String, ByVal p_CodigoRubro As String) As String

        Dim r As entidades.rubro = Nothing
        r = Me.lstRubros.Find(Function(x) x.codigo.Contains(p_CodigoRubro))
        If (r IsNot Nothing) Then

            If (p_anio = "2014") Then
                Return r.alicuotaActual
            Else
                Return r.alicuota2018
            End If
        Else
            Return ""
        End If
    End Function


End Class

