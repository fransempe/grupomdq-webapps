﻿Option Explicit On
Option Strict On


'Web Services
Imports web_tish.ws_tish.Service1

Partial Public Class webfrmlistadoctacte
    Inherits System.Web.UI.Page

#Region "Variables"

    Private Structure SLogo
        Dim LogoOrganismo As Byte()
        Dim RenglonOrganismo As String
        Dim RenglonNombreOrganismo As String
    End Structure

    Private mWS As web_tish.ws_tish.Service1
    Private mPDF As clsPDFListadoCTACTE
    Private mLogo As SLogo

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dsDataComercio As DataSet
        Dim dsDataRows As DataSet
        Dim dtDataRows_AUX As DataTable
        Dim mRutaFisica As String
        Dim mNombreArchivoComprobante As String
        Dim mArchivo As String
        Dim mCodigoMunicipalidad As String
        Dim dtDataComercio_AUX As DataTable
        Dim dsCtaCte As DataSet


        Try

            '  If Not (IsPostBack) Then

            dsCtaCte = New DataSet()
            dsCtaCte = Me.GetCtaCte()
            'If (Session("LIST_CTACTE") IsNot Nothing) Then
            If (dsCtaCte IsNot Nothing) Then


                'I Update the dataset
                dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))


                'I obtain the data of the comercio
                dsDataComercio = New DataSet
                dsDataComercio.Tables.Add(dtDataComercio_AUX)


                'I obtain the list of cta cte
                dsDataRows = New DataSet
                dtDataRows_AUX = New DataTable
                'dtDataRows_AUX = CType(Session("LIST_CTACTE"), DataTable).Copy
                dtDataRows_AUX = dsCtaCte.Tables(0).Copy
                dsDataRows.Tables.Add(dtDataRows_AUX)





                'Creo Directorio y Archivo en Disco
                mRutaFisica = Request.ServerVariables("APPL_PHYSICAL_PATH").ToString & "comprobantespdf"
                If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString)) Then
                    My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString)
                End If
                mNombreArchivoComprobante = dtDataComercio_AUX.Rows(0).Item("COMERCIO_NUMERO").ToString.Trim & "listadoctacte" & Format(Now, "HHmmss")
                mNombreArchivoComprobante = "ddjj" & Format(Now, "HHmmss")
                mArchivo = mRutaFisica & "\" & mNombreArchivoComprobante & ".pdf"


                'Creo y Seteo el Nombre del Logo dependiendo de la Conexion
                mCodigoMunicipalidad = ""
                'If (Application("tipo_conexion").ToString = "RAFAM") Then
                '    Call GenerarLogo()
                '    mCodigoMunicipalidad = ObtenerCodigoMunicipalidad()
                'End If





                'Genero el PDF
                mPDF = New clsPDFListadoCTACTE

                'mPDF.TipoConexion  = Application("tipo_conexion").ToString


                'Estos Datos son para agregar al LOGO si esta Conectado a RAFAM
                'If (Application("tipo_conexion").ToString = "RAFAM") Then
                '    mPDF.Organismo = mLogo.RenglonOrganismo.ToString
                '    mPDF.NombreOrganismo = mLogo.RenglonNombreOrganismo.ToString
                'End If




                mPDF.RutaFisica = mArchivo.ToString


                mPDF.DatosMunicipalidad = Me.ObtenerDatosMunicipalidad()
                mPDF.DataComercio = dsDataComercio
                mPDF.DataRows = dsDataRows
                'mPDF.DatosContribuyente = ""
                mPDF.CrearPDF()




                'Muestro el Comprobante por pantalla
                Response.Expires = 0
                Response.Buffer = True
                Response.Clear()
                Response.ContentType = "application/pdf"
                Response.TransmitFile(mArchivo.ToString)
                Response.Flush()



            Else
                Call LimpiarSession()
            End If

        Catch ex As Exception
            Response.Redirect("../webfrmerror.aspx", False)
        End Try
    End Sub


#Region "procedimientos y Funciones"



    Private Sub LimpiarSession()
        Session.RemoveAll()
        Session.Clear()
        Session.Abandon()
        Response.Redirect("../../webfrmindex.aspx", False)
    End Sub

    Private Function ObtenerTipoCuenta(ByVal mTipoCuenta As String) As String
        Select Case mTipoCuenta
            Case "I" : Return "INMUEBLE"
            Case "C" : Return "COMERCIO"
            Case "E", "O" : Return "CEMENTERIO"
            Case "V" : Return "VEHICULOS"
            Case Else : Return ""
        End Select
    End Function

#End Region


    Private Function ObtenerDatosMunicipalidad() As DataSet
        Dim dsDatos As DataSet
        Dim mXML As String

        Try

            'Obtengo los datos de la municipalidad
            mWS = New web_tish.ws_tish.Service1
            mXML = mWS.ObtainMunicipalidadData()
            mWS = Nothing


            dsDatos = New DataSet
            dsDatos = ClsTools.ObtainDataXML(mXML.ToString.Trim)

            Return dsDatos
        Catch ex As Exception
            'mLog = New Clslog
            'mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            'mLog = Nothing
            Response.Redirect("../webfrmerror.aspx", False)
            Return Nothing
        End Try

    End Function


    Private Function GetCtaCte() As DataSet
        Dim dsData As DataSet
        Dim dtDataComercio_AUX As DataTable


        'I Update the dataset
        dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))

        dsData = New DataSet
        dsData = ObtainListCtaCte(CLng(dtDataComercio_AUX.Rows(0).Item("COMERCIO_NUMERO").ToString.Trim), False)

        
        Return dsData
    End Function


    Private Function ObtainListCtaCte(ByVal pNumberComercio As Long, ByVal pListComplete As Boolean) As DataSet
        Dim mXML_AUX As String
        Dim dsListCtaCte As DataSet


        Try

            'I look for the information of the comercio
            mWS = New ws_tish.Service1
            mXML_AUX = mWS.ObtainCTACTE(pNumberComercio)
            mWS = Nothing


            'I read the XML
            dsListCtaCte = New DataSet
            dsListCtaCte = clsTools.ObtainDataXML(mXML_AUX.ToString.Trim)


            'If the dsListDDJJ is not empty I assign information
            If (clsTools.HasData(dsListCtaCte)) Then                
                dsListCtaCte = CreateDataSet(dsListCtaCte, pListComplete)
            End If



        Catch ex As Exception
            dsListCtaCte = Nothing
        Finally
            mWS = Nothing
        End Try


        Return dsListCtaCte
    End Function

    Private Function CreateDataSet(ByVal dsCtaCte As DataSet, ByVal pListComplete As Boolean) As DataSet
        Dim dsDatos As DataSet
        Dim dtDatos As DataTable
        Dim mFila As DataRow
        Dim i As Integer

        Dim dvListCTACTE As DataView
        Dim mPrimaryKey(2) As DataColumn
        Dim mPrimaryKey_Data(2) As Object
        Dim mIndexRow As Integer




        Try


            'Create the columns
            dtDatos = New DataTable("LIST_CTACTE")
            mFila = dtDatos.NewRow
            dtDatos.Columns.Add("DDV_NROMOV", System.Type.GetType("System.Int32"))
            dtDatos.Columns.Add("DDV_REC", System.Type.GetType("System.String"))
            dtDatos.Columns.Add("DDV_ANIO", System.Type.GetType("System.Int32"))
            dtDatos.Columns.Add("DDV_CUOTA", System.Type.GetType("System.Int32"))
            dtDatos.Columns.Add("DDV_CONCEPTO")
            dtDatos.Columns.Add("DDV_FECHAVENC")
            dtDatos.Columns.Add("DDV_CONDESPECIAL")
            dtDatos.Columns.Add("DDV_IMPORTEORIGEN")
            dtDatos.Columns.Add("DDV_IMPORTERECARGO")
            dtDatos.Columns.Add("DDV_IMPORTETOTAL")
            dtDatos.Columns.Add("ESTADO")



            'I fill the dataset whit the rec_cc
            If (dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA") IsNot Nothing) Then
                For i = 0 To dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows.Count - 1
                    mFila = dtDatos.NewRow
                    mFila("DDV_NROMOV") = CInt(dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_NROMOV").ToString.Trim)
                    mFila("DDV_REC") = dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_REC").ToString.Trim
                    mFila("DDV_ANIO") = CInt(dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_ANIO").ToString.Trim)
                    mFila("DDV_CUOTA") = dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_CUOTA").ToString.Trim
                    mFila("DDV_CONCEPTO") = dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_CONCEPTO").ToString.Trim
                    mFila("DDV_FECHAVENC") = dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_FECHAVENC").ToString.Trim
                    mFila("DDV_CONDESPECIAL") = dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_CONDESPECIAL").ToString.Trim
                    mFila("DDV_IMPORTEORIGEN") = dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_IMPORTEORIGEN").ToString.Trim
                    mFila("DDV_IMPORTERECARGO") = dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_IMPORTERECARGO").ToString.Trim
                    mFila("DDV_IMPORTETOTAL") = dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_IMPORTETOTAL").ToString.Trim
                    mFila("ESTADO") = "Adeudado"
                    dtDatos.Rows.Add(mFila)
                Next
            End If





            dsDatos = New DataSet
            dsDatos.Tables.Add(dtDatos)




            'I create the index
            mPrimaryKey(0) = dsDatos.Tables("LIST_CTACTE").Columns("DDV_ANIO")
            mPrimaryKey(1) = dsDatos.Tables("LIST_CTACTE").Columns("DDV_CUOTA")
            mPrimaryKey(2) = dsDatos.Tables("LIST_CTACTE").Columns("DDV_NROMOV")
            dsDatos.Tables("LIST_CTACTE").PrimaryKey = mPrimaryKey

            'I seteo the order
            dsDatos.Tables(0).DefaultView.Sort = "DDV_ANIO, DDV_CUOTA, DDV_NROMOV"




            'I add the mov of her rec_cc And updated the new mov
            If (dsCtaCte.Tables("RENGLONES_REC_CCC") IsNot Nothing) Then
                For i = 0 To dsCtaCte.Tables("RENGLONES_REC_CCC").Rows.Count - 1

                    'I seteo the index
                    mPrimaryKey_Data(0) = CInt(dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_ANIO").ToString.Trim)
                    mPrimaryKey_Data(1) = CInt(dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_CUOTA").ToString.Trim)
                    mPrimaryKey_Data(2) = CInt(dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_NROMOV").ToString.Trim)


                    'I search for the index
                    mIndexRow = dsDatos.Tables("LIST_CTACTE").DefaultView.Find(mPrimaryKey_Data)


                    If (mIndexRow = -1) Then

                        mFila = dtDatos.NewRow
                        mFila("DDV_NROMOV") = CInt(dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_NROMOV").ToString.Trim)
                        mFila("DDV_REC") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_REC").ToString.Trim
                        mFila("DDV_ANIO") = CInt(dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_ANIO").ToString.Trim)
                        mFila("DDV_CUOTA") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_CUOTA").ToString.Trim
                        mFila("DDV_CONCEPTO") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_CONCEPTO").ToString.Trim
                        mFila("DDV_FECHAVENC") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_FECHAVENC").ToString.Trim
                        mFila("DDV_CONDESPECIAL") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_CONDESPECIAL").ToString.Trim
                        mFila("DDV_IMPORTEORIGEN") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTEORIGEN").ToString.Trim
                        mFila("DDV_IMPORTERECARGO") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTERECARGO").ToString.Trim
                        mFila("DDV_IMPORTETOTAL") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTETOTAL").ToString.Trim
                        mFila("ESTADO") = "Pagado"

                        'I add the row
                        dsDatos.Tables("LIST_CTACTE").Rows.Add(mFila)
                    Else



                        'dsDatos.Tables("LIST_DDJJ").Rows(mIndexRow).Item("DDV_IMPORTEORIGEN") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTEORIGEN").ToString.Trim
                        'dsDatos.Tables("LIST_DDJJ").Rows(mIndexRow).Item("DDV_IMPORTERECARGO") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTERECARGO").ToString.Trim
                        'dsDatos.Tables("LIST_DDJJ").Rows(mIndexRow).Item("DDV_IMPORTETOTAL") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTETOTAL").ToString.Trim
                        'dsDatos.Tables("LIST_DDJJ").Rows(mIndexRow).Item("NRO_COMPROBANTE") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("NRO_COMPROBANTE").ToString.Trim

                        dsDatos.Tables("LIST_CTACTE").Rows(mIndexRow).Item("DDV_IMPORTEORIGEN") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTEORIGEN").ToString.Trim
                        dsDatos.Tables("LIST_CTACTE").Rows(mIndexRow).Item("DDV_IMPORTERECARGO") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTERECARGO").ToString.Trim
                        dsDatos.Tables("LIST_CTACTE").Rows(mIndexRow).Item("DDV_IMPORTETOTAL") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTETOTAL").ToString.Trim
                        dsDatos.Tables("LIST_CTACTE").Rows(mIndexRow).Item("NRO_COMPROBANTE") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("NRO_COMPROBANTE").ToString.Trim

                    End If
                Next
            End If


            'I seteo the order
            'dsDatos.Tables("LIST_CTACTE").DefaultView.Sort = "DDV_ANIO ASC, DDV_CUOTA ASC"
            dsDatos.Tables("LIST_CTACTE").DefaultView.Sort = "DDV_ANIO, DDV_CUOTA, DDV_NROMOV"




            If Not (pListComplete) Then
                dvListCTACTE = New DataView()
                dvListCTACTE.Table = dsDatos.Tables("LIST_CTACTE")


                dvListCTACTE.RowFilter = "DDV_ANIO >= " & (Now.Year - 2).ToString.Trim & " AND DDV_ANIO <= " & Now.Year
                dsDatos.Tables.Clear()
                dsDatos.Tables.Add(dvListCTACTE.ToTable)

                'I seteo the order
                dsDatos.Tables("LIST_CTACTE").DefaultView.Sort = "DDV_ANIO ASC, DDV_CUOTA ASC, DDV_NROMOV"
            End If


            Return dsDatos
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

End Class