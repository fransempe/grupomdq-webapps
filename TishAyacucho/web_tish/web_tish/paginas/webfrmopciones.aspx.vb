﻿Option Explicit On
Option Strict On


Partial Public Class webfrmopciones
    Inherits System.Web.UI.Page


#Region "Variables"
    Private mWS As ws_tish.Service1

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not (IsPostBack) Then

            Call Me.SloganMuniicpalidad()
            Call Me.ValidateSession()
            Call Me.LoadFooter()

            'I load the comercio' data
            Call Me.LoadDataComercio()

        End If


    End Sub


#Region "functions"

    Private Sub SloganMuniicpalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema SAE."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema SAE."
        End If

    End Sub



    'ValidateSession:
    'This function validate the data of session
    Private Function ValidateSession() As Boolean
        Dim mLogin_OK As Boolean


        'flag Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'name user
        If (mLogin_OK) Then
            If (Session("contribuyente_nombre") IsNot Nothing) Then
                If (Session("contribuyente_nombre").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If




        If (mLogin_OK) Then
            'lblcontribuyente_nombre.Text = Session("data_trade").rows(0).item("CONTRIBUYENTE_CUIT").ToString.Trim()
        Else
            Response.Redirect("webfrmlogin.aspx", False)
        End If

    End Function


    'LoadFooter:
    'This function loads the information of the municipality in the foot of the page
    Private Sub LoadFooter()

        'name Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telephone Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If

        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub


    'LoadDataComercio
    'This function loads the data of the Comercio
    Private Sub LoadDataComercio()
        Dim mDataTrade_XML As String
        Dim dsDataTrade As DataSet


        Try


            mDataTrade_XML = ""
            mWS = New ws_tish.Service1
            mDataTrade_XML = mWS.ObtenerDatosComercio(Session("trade_number").ToString.Trim)
            mWS = Nothing



            'I read the XML
            dsDataTrade = New DataSet
            dsDataTrade = clsTools.ObtainDataXML(mDataTrade_XML.ToString.Trim)
            Session("data_trade") = dsDataTrade.Tables(0)


            'I validate that the this loaded datatable and I assign the data
            If (dsDataTrade IsNot Nothing) Then
                lblcomercio_numero.Text = dsDataTrade.Tables(0).Rows(0).Item("COMERCIO_NUMERO").ToString.Trim
                lblcomercio_razonsocial.Text = dsDataTrade.Tables(0).Rows(0).Item("COMERCIO_RAZONSOCIAL").ToString.Trim
                lblcomercio_nombre_fantasia.Text = dsDataTrade.Tables(0).Rows(0).Item("COMERCIO_NOMBREFANTASIA").ToString.Trim


                lblcomercio_telefono.Text = dsDataTrade.Tables(0).Rows(0).Item("COMERCIO_TELEFONO").ToString.Trim
                lblcomercio_email.Text = dsDataTrade.Tables(0).Rows(0).Item("COMERCIO_EMAIL").ToString.Trim
                lblcomercio_responsable.Text = dsDataTrade.Tables(0).Rows(0).Item("COMERCIO_RESPONSABLE").ToString.Trim
                lblcomercio_cargo.Text = dsDataTrade.Tables(0).Rows(0).Item("COMERCIO_CARGO").ToString.Trim


                lblcomercio_ubicacion.Text = dsDataTrade.Tables(0).Rows(0).Item("COMERCIO_UBICACION").ToString.Trim
                lblcomercio_localidad.Text = dsDataTrade.Tables(0).Rows(0).Item("COMERCIO_LOCALIDAD").ToString.Trim
                lblcontribuyente_nombre.Text = dsDataTrade.Tables(0).Rows(0).Item("CONTRIBUYENTE_NOMBRE").ToString.Trim

            End If


        Catch ex As Exception
        Finally

        End Try

    End Sub
#End Region

End Class