﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmresultados_genericos.aspx.vb" Inherits="web_tish.webfrmresultados_genericos" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
             
        
        
        <!-- Seteo the resources -->
        <script src="../js/funciones.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../css/estilos_listado.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_botones.css" type="text/css" />        
        <%           
            'Seteo el css que voy a utilizar
            Dim mPath_css As String                
            mPath_css = ""
            mPath_css = System.Configuration.ConfigurationManager.AppSettings("css").ToString.Trim()
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_listado.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_genericos.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_botones.css' type='text/css' />")
        %>                   
        
        
        
        <!-- ajax -->       
        <script type="text/javascript" language="javascript" src="../js/jquery1-2-6-min.js"></script>
        <script type="text/javascript" language="javascript">
        
            /* Carga del logo de la municipalidad */
            function ajax_load_logo(){   
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_logo.ashx",                                        
                    success: see_response_logo
                });
            }
            
            
            function see_response_logo(html){                   
                if (html != '') {
                    var _control = document.getElementById('div_header');                   
                    _control.innerHTML = html;                   
                    return false;
                }
                
            return true;    
            }          
        </script>      
        
        
        
        <!-- recursos para crear la ventana modal -->
	    <link rel="stylesheet" href="../modal/Assets/LightFace.css" />	
	    <script src="../modal/mootools.js" type="text/javascript"></script>
	    <script src="../modal/mootools-more-drag.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.IFrame.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Image.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Request.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Static.js" type="text/javascript"></script>
        

        <!-- Logout -->
        <script type="text/javascript">
        	function logout() {	 	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema SAE :: Cerrar sesión', 
				    width: 250,
				    height: 50,
	  			    content: '<div align="left">Usted esta a punto de cerrar su sesión.\n¿Desea continuar?</div>',
	 			    buttons: [		
	 			        {
						    title: 'Aceptar',
						    event: function() { this.close(); window.location = "webfrmlogin.aspx"; }
					    },
					    {
						    title: 'Cerrar',
						    event: function() { this.close();}
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }    
	 	    
	 	    
            function change_trade() {	 	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema SAE :: Cambiar comercio', 
				    width: 300,
				    height: 50,
	  			    content: 'Usted esta a punto de cambiar de comercio\n¿Desea continuar?',
	 			    buttons: [		
	 			        {
						    title: 'Aceptar',
						    event: function() { this.close();window.location= "webfrmseleccionar_comercio.aspx";}
					    },
					    {
						    title: 'Cerrar',
						    event: function() { this.close();}
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }        
    
        </script>

        
        
     
        
        
        <title>Sistema SAE :: Resultado</title>
        <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
    	
        
        
        
        <script type="text/javascript">

            function validar_datos() {

                /* -- Nro de Comercio -- */
                //Cadena Vacia
                if (control_vacio(window.document.getElementById('txtnro_comercio'),'Número de comercio:\nDebe ingresar un número de comercio.')) {
                   return false;
                }
                
                //Cadena Valida
                if (!validar_cadena_solo_numeros(window.document.getElementById('txtnro_comercio'),'Número de comercio')) {          
                    return false;
                }
                
            return true;
            }
            
    
    
            /* Valido que la clave exista y le pregunto al usuario si quiere remplazar la clave*/
            function generar_clave() {
            
                /* -- Nro de Comercio -- */
                //Cadena Vacia
                if (control_vacio(window.document.getElementById('txtnro_comercio'),'Número de comercio:\nDebe ingresar un número de comercio.')) {
                   return false;
                }
                
                //Cadena Valida
                if (!validar_cadena_solo_numeros(window.document.getElementById('txtnro_comercio'),'Número de comercio')) {          
                    return false;
                }
                
                
                /* -- Cuit del contribuyente -- */
                //Cadena Vacia
                if (control_vacio(window.document.getElementById('txtcontribuyente_cuit'),'Cuit contribuyente:\nDebe ingresar el cuit del contribuyente.')) {
                   return false;
                }
                
                //Cadena Valida
                if (!validar_cadena_solo_numeros(window.document.getElementById('txtcontribuyente_cuit'),'Cuit contribuyente')) {          
                    return false;
                }
            
            
            
                // Validar comercio cargado
                if (window.document.getElementById('lblcomercio_nombre').innerHTML === '') {          
                    alert('Generacion de clave:\nDebe seleccionar un comercio.');
                    return false;
                }
            
                
                           
                
                return imprimir_clave();
                
            
            return true;
            }
            
    
    
    /* I warn him the user who for the trade selecciondo already exists a key */    
    function existing_key() {
        if (window.document.getElementById('lblestado_clave').innerHTML == 'CLAVE EXISTENTE.') {
            return confirm ('El comercio seleccionado ya contiene un clave para el ingreso Web.\n'
                            + '¿Desea remplazarla de todas formas?')
        }
    }
    
    
    
    /* Valido que clave exista y le recomiendo que use la ultima version de Acrobat */    
    function imprimir_clave() {
        var existing_key_aux = false;
        
        
        /* I obtain the value of the function */   
        existing_key_aux = existing_key();
       
       
        if (existing_key_aux) {
                     
            if (window.document.getElementById('lblestado_clave').innerHTML != 'CLAVE INEXISTENTE.') {
                return confirm("Usted está a punto de generar una constancia de clave.\n"
                               + "Para el correcto funcionamiento se recomienda descargar la última versión de Adobe Reader.\n"
                               + "¿Desea continuar?")
            }
        }
        
    return existing_key_aux;
    }
    
    
    
    
    
    /* Esta funcion genera el mensaje al usuario de que esta a punto de cerrar la session de usuario*/
    function cerrar_session() {
        return confirm("Usted esta a punto de cerrar su sesión\n¿Desea continuar?")              
    }
    
    
    
    /* Esta Funcion ASIGNA ESTILO a un BOTON */          
    function efecto_over(boton){
        boton.className = "boton_gris"; 	        
	}
	    

    /* Esta Funcion ASIGNA ESTILO a un BOTON */          
  	function efecto_out(boton){
        boton.className = "boton_verde";    
	}


 
    
    
    
    


    

</script>
        
        
    </head>

    <body>
    
        
        <!-- wrap starts here -->	
        <div id="wrap">
                     
		    <div id="header">
		          
		        <!-- div container font -->
                <div id="container_font">			        
		        <div  style="width:5%; float:left;  padding:0px 5px 0px 5px;">
                    <a href="#" title="Usar fuente menor" class="link_font" onclick="assign_font('-');">A-</a> 
                </div>
                
                <div  style="width:5%; float:left;">
		            <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 
                </div>		            
                
		        <div  style="width:5%; float:left;">
		            <a href="#" title="Usar fuente mayor"class="link_font" onclick="assign_font('+');">A+</a> 
		        </div>	            
            </div>
                    
		    
		    <div id="div_header"  class="div_image_logo">                
                <img  class="image_logo" alt="" src="../imagenes/topiz.jpg"  />
            </div>  
		          
		                
			<h1 id="logo-text">Tasa por Servicios a la Actividad Económica</h1>			
			<h2 id="slogan"><asp:Label ID="lblslogan_municipalidad" runat="server" Text=""></asp:Label></h2>	
					
						
			
		</div>
	  
            <!-- content-wrap starts here -->
            <div id="content-wrap">
    	  
    	  
                 <!-- div container right -->
                <div id="div_optiones"  class="menu_options">
                
                    <div id="div_comercio" style=" padding-top:20px;">
                        <div align="center">
                            <table  style="width:95%">
                                <tr>
                                    <td  rowspan="2"><img id="user" alt="" src="../imagenes/user.png"  style=" width:50px; height:60px; border:none"/></td>
                                    <td><h1 align ="left" style="color:Black">Comercio</h1></td>
                                </tr>
                                <tr>                                
                                    <td id="td_numero" runat="server">
                                        NUMERO: <asp:Label ID="lblcomercio_user" runat="server" Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>
                                
                                
                            </table>
                        </div>
                       
                        
                        
                        
                        
                    </div>
                    
                    
                    <h1 id = "h1_accesos" align ="left" style="color:Black" runat="server">Accesos</h1>                    			
	                <ul id="ul_accesos" class="sidemenu" runat="server">               
                        <li><a href="webfrmdeclaracionesjuradas.aspx">Ver listado de DDJJ</a></li>
                        <li><a href="webfrmctacte.aspx">Ver cuenta corriente</a></li>
                        <li><a href="webfrmopciones.aspx">Datos del comercio</a></li>				                
                        <li><a href="#" onclick="javascript:change_trade();">Cambiar comercio</a></li>			                        
                    </ul>	
	                
	                <h1 id="h1_usuario_web" align ="left" style="color:Black" runat="server">Usuario Web</h1>				
                    <ul id="ul_usuario_web" class="sidemenu" runat="server">
	                    <li><a href="webfrmdatos_representante.aspx">Mis datos</a></li>				                
		                <li><a href="webfrmcambiar_clave.aspx">Cambiar clave</a></li>	
		                <li><a href="#" onclick="javascript:logout();">Cerrar sesión</a></li>
	                </ul>	
                </div>
    	    
                <!-- div container page title -->
                <div id="div_title" style="background-color:Black;" align ="left" >
                    <strong class="titulo_page">Sistema SAE :: Resultado</strong>
                </div>
    	  
  		        <div id="main"> 
    			
                    <p>
			            <strong>Sistema SAE :: Resultado</strong>
			            <asp:Label ID="lbltitle" runat="server" Text="Usted tiene la opción de imprimir o guardar su comprobante de pago."></asp:Label>
			        </p>  

    		
                    
                    <form id="formulario" runat="server">
                        
                        
                       
                    
                    
                   
                    
                    
                 
                        
                        
                      

                    
                        <div id="div_container_trade" align="center" style="padding-bottom:30px;"  runat="server">
                    
            		        <script type="text/javascript" src="../efecto_ajax/jsUpdateProgress.js"></script>		
	                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	                      
            		     
            		     
            		            <!-- message for the user -->
                                
                        
                                <!-- div add trade -->
                                <div id="add_trade" align="center" style="width:90%">
                                
                                         
                                         
                                         
                                         
                                         
                                    <div id="div_list_trades" style="padding-top:20px; width:100%;">
                                        <table id="table_messenger_pdf" border="5" align="center" style="width:100%;  height:236px" runat="server">
                                            <tr>
                                                <td style= "padding-top:10px;">
                                                    <table id="table_period" align="center" style="width:100%;">
                                                        <tr align="center" style="font-size:medium">
                                                            <td colspan="2">
                                                                <div id="mensaje">
                                                                    <asp:Label ID="lblmessenger_pdf" runat="server" Text=""  CssClass="titulo_2"></asp:Label>
                                                                </div>
                                                            </td>
                                                            
                                                        <tr>
                                                            <td colspan="2">
                                                                <p>Desde acá usted podrá descargar su comprobante.</p>
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td align="right" style="width:55%;">
                                                                <p><strong>DESCARGAR COMPROBANTE:</strong></p>
                                                            </td>
                                                            <td align="left" style="width:45%;">
                                                                
                                                                <a id="link_pdf" href="#" runat="server"><img alt="Constancia" src="../imagenes/pdf.jpg" width="42"  height="55"></a>
                                                                
                                                            </td>
                                                        </tr>
                                                            
                                                        </tr>
                                                    </table>
                                                
                                                                   		                            
            		                            		                            
		                                            <div  align="center" style="padding-top:15px;"> 
		                                                <table>
		                                                
		                                                    <tr>
		                                                        <td>
                                                                    <asp:Label ID="lblmessage_mascomprobantes" runat="server" Text="" Visible="false"></asp:Label>
		                                                        </td>
		                                                    </tr>	
		                                                
		                                                    <tr>
		                                                        <td>
		                                                            <asp:LinkButton ID="btnback_messenger_pdf" runat="server" CssClass="link">Volver</asp:LinkButton>		                                                         
		                                                        </td>
		                                                    </tr>		                                        
		                                                </table>	
                                                    </div>	
                                                </td>
                                            </tr>
                                        </table>                                                                    
                                        
                                        
                                        <table id="table_messenger" border="5" align="center" style="width:100%; height:236px;"   runat="server">
                                            <tr>
                                                <td style= "padding-top:10px;">
                                                    <table id="table1" align="center" style="width:100%;">
                                                        <tr align="center" style="font-size:medium">
                                                            <td colspan="2">
                                                                <div id="Div1">
                                                                    <asp:Label ID="lblmessenger" runat="server" Text=""  CssClass="titulo_2"></asp:Label>
                                                                </div>
                                                            </td>
                                                  
                                                        </tr>
                                                  
                                                        
                                                      
                                                            
                                                         
                                                    </table>
                                                
                                                                   		                            
            		                            		                            
		                                            <div  align="center" style="padding-top:15px;"> 
		                                                <table>
		                                                    <tr>
		                                                        <td>
		                                                            <asp:LinkButton ID="btnback_messenger" runat="server" CssClass="link">Volver</asp:LinkButton>		                                                         		                                                        
                                                                </td>
                                                                
		                                                    </tr>		                                        
		                                                </table>	
                                                    </div>	
                                                </td>
                                            </tr>
                                        </table>                                                                    
                                    </div>
                                        
                                        
                                        
                                     
                                     
                                </div>
                              	
                              
                        
                        
                          
	                    </div>
	                    
	                    
                       
	                </form>
    				
      								
  		        </div> 	
    
	        </div>
		
		
		    <!-- div data footer -->
		    <div id="footer">	
		       <div id="div_pie_municipalidad" 
                    style="width:70%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		            Municipalidad de
		                <strong> 
		                    <asp:Label ID="lblnombre_municipalidad" runat="server" Text="NOMBRE_MUNICIPIO"></asp:Label> 
                        </strong> 
                        
				    <br />
				    Teléfono: 
				        <strong> 
                            <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                        </strong> 
                     | Email: 
				        <strong> 
                            <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                        </strong> 
                    
			    </div>
    		   
		       <div id="div_pie_grupomdq"  class="footer_grupomdq">
				    Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ S.A</a></strong>  
				    <br />
				    Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				    <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
                   <asp:Label ID="lblversion" runat="server" Text=""></asp:Label>
		       </div>
		    </div>	
		    
        </div>	



    </body>
</html>
