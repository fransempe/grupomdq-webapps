﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmdeclaracionesjuradas.aspx.vb" Inherits="web_tish.webfrmdeclaracionesjuradas" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

    <head>
    
        <!-- Sseteo the resources -->
        <script src="../js/funciones.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../css/estilos_listado.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_botones.css" type="text/css" />
        <%           
            'Seteo el css que voy a utilizar
            Dim mPath_css As String                
            mPath_css = ""
            mPath_css = System.Configuration.ConfigurationManager.AppSettings("css").ToString.Trim()
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_listado.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_genericos.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_botones.css' type='text/css' />")
        %>        
        
        
        
        
        
        <!-- recursos para crear la ventana modal -->
	    <link rel="stylesheet" href="../modal/Assets/LightFace.css" />	
	    <script src="../modal/mootools.js" type="text/javascript"></script>
	    <script src="../modal/mootools-more-drag.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.IFrame.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Image.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Request.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Static.js" type="text/javascript"></script>
	
	    
        <!-- Creo la ventana modal -->
        <script type="text/javascript">    	
	 	    function modal_window(phtml) {	 	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema SAE :: Detalle de declaración jurada', 
				    width: 600,
				    height: 200,
	  			    content: phtml,
	 			    buttons: [					
					    {
						    title: 'Cerrar',
						    event: function() { this.close(); }
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }        
	 	    
	 	    
	 	     function print() {	 	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema SAE :: Reimpresión', 
				    width: 350,
				    height: 20,
	  			    content: 'La reimpresión de las DDJJ se encuentra en desarrollo.',
	 			    buttons: [					
					    {
						    title: 'Cerrar',
						    event: function() { this.close(); }
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }        
	 	    
	 	    
	 	    
	 	    function logout() {	 	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema SAE :: Cerrar sesión', 
				    width: 250,
				    height: 50,
	  			    content: '<div align="left">Usted esta a punto de cerrar su sesión.\n¿Desea continuar?</div>',
	 			    buttons: [		
	 			        {
						    title: 'Aceptar',
						    event: function() { this.close(); window.location = "webfrmlogin.aspx"; }
					    },
					    {
						    title: 'Cerrar',
						    event: function() { this.close();}
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }        
	 	    
	 	    
	 	    function change_trade() {	 	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema SAE :: Cambiar comercio', 
				    width: 300,
				    height: 50,
	  			    content: '<div align="left">Usted esta a punto de cambiar de comercio\n¿Desea continuar?</div>',
	 			    buttons: [		
	 			        {
						    title: 'Aceptar',
						    event: function() { this.close();window.location= "webfrmseleccionar_comercio.aspx";}
					    },
					    {
						    title: 'Cerrar',
						    event: function() { this.close();}
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }        


        </script>
        
        
        
        
               
               
        <!-- ajax -->       
        <script type="text/javascript" language="javascript" src="../js/jquery1-2-6-min.js"></script>
        <script type="text/javascript" language="javascript">
        
            /* Carga del logo de la municipalidad */
            function ajax_load_logo(){   
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_logo.ashx",                                        
                    success: see_response_logo
                });
            }
            
            
            function see_response_logo(html){                   
                if (html != '') {
                    var _control = document.getElementById('div_header');                   
                    _control.innerHTML = html;                   
                    return false;
                }
                
            return true;    
            }          
            
            
            
            /* Obtengo los datos del detalle */
            function ajax_load_ddjj(){             
                var trade_number;
                trade_number = document.getElementById('lblcomercio_user').innerHTML;
                  
                	_html = '<div id="loading" align="center">'+
            	            '<table border= "0">' +
	                            '<tr>' +
	                                '<td align="center" style="width:10%;"><img id="img_load" alt="" src="../imagenes/loading.gif" /></td>' +
	                                '<td align="left" style="width:90%;">Cargando declaraciones juradas ......</td>' +
	                            '</tr>' +
	                        '</table>' +
	                    '</div>';              		          
                window.document.getElementById('div_ddjj').innerHTML = _html;
                
                                                            
                _comercio = window.document.getElementById('lblcomercio_user').innerHTML;                
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_ddjj.ashx",                                                            
                    data:"action=get&comercio=" + _comercio,
                    success: see_response_load_ddjj
                });
            }            
            
            

            function see_response_load_ddjj(html){                   
                if (html != '') {
                    window.document.getElementById('div_ddjj').innerHTML = html;                                             
                    return false;
                }                
            return true;    
            }                      
            
            
            
            
            
            
            
            /* Obtengo los datos del detalle */
            function ajax_view_detail(pyear, pquota, ptype){             
                var trade_number;
                trade_number = document.getElementById('lblcomercio_user').innerHTML;
                  
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_detail_ddjj.ashx",   
                    data:"year=" + pyear + "&" + "quota=" + pquota + "&" + "type=" + ptype + "&" + "trade_number=" + trade_number,
                    success: see_response_detail
                });
            }
            

            function see_response_detail(html){                   
                if (html != '') {                    
                    modal_window(html);  
                    return false;
                }
                
            return true;    
            }          
            
        </script>               
               
               
        <!-- funcion doPostBack y quienes las usan -->
        <script type="text/javascript" language="javascript">
            
            function declaration_new(){ __doPostBack('new', ''); }
            function rectification(p_period){ __doPostBack('rectification', p_period); }
        
            function __doPostBack(eventTarget, eventArgument) {
                var form = document.forms["formulario"];
                
                form.__EVENTTARGET.value = eventTarget.split("$").join(":");
                form.__EVENTARGUMENT.value = eventArgument;
                form.submit();
            }
        </script>
               
   
        
        
        <!-- Estas funciones le dan efecto a la grilla cuando paso el mouse por arriba -->
        <script type="text/javascript">
        
            /* Esta Funcion Asigna Efectos a las Grillas */
            function efecto_grilla(id_grilla) {
              var grilla=document.getElementById(id_grilla);
              if (grilla != null) {
                for (i=0; fila=grilla.getElementsByTagName('td')[i]; i++) {
                    fila.onmouseover = function() {iluminar(this,true)}
                    fila.onmouseout = function() {iluminar(this,false)} 
                }     
              }
            }


            /* Esta Funcion Asigna el EFECTO de CAMBIAR DE COLOR */
            function iluminar(obj,valor) {
                var fila = obj.parentNode;
                for (i=0; filas = fila.getElementsByTagName('td')[i]; i++)
                    filas.style.background = (valor) ? 'gray' : '';     
            }
        </script>
        
        
          <!-- funciones varias -->
        <script type="text/javascript" language="javascript">
        
            function list_ddjj() {
                loading('Generando listado de declaraciones juradas');
                window.location= "listados/webfrmlistadoddjj.aspx";
            }


        function loading(p_message) {
            var _html;
        
        	_html = '<div id="loading" align="center">'+
        	            '<table border= "0">' +
                            '<tr>' +
                                '<td align="center" style="width:10%;"><img id="img_load" alt="" src="../imagenes/loading.gif" /></td>' +
                                '<td align="left" style="width:90%;">' + p_message + ' ....' +'</td>' +
                            '</tr>' +
                        '</table>' +
	                '</div>';          
             window.document.getElementById('div_contenedor_botones').innerHTML = _html;
        }
        
        </script>
        
      
        
        <title>Sistema SAE :: Listado de declaraciones juradas</title>
        <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
    	
    </head>


    <body onload="javascript:ajax_load_ddjj();">
    
        
        <!-- div container of the page -->	
        <div id="wrap">
         
            <!-- div header -->	
		    <div id="header">	
		    
		       <!-- div container font -->
                <div id="container_font">			        
		            <div  style="width:5%; float:left;  padding:0px 5px 0px 5px;">
                        <a href="#" title="Usar fuente menor"  class="link_font" onclick="assign_font('-');">A-</a> 
                    </div>
                    
                    <div  style="width:5%; float:left;">
		                <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 
                    </div>		            
                    
		            <div  style="width:5%; float:left;">
		                <a href="#" title="Usar fuente mayor" class="link_font" onclick="assign_font('+');">A+</a> 
		            </div>	            
                </div>
		    
		    
		        <div id="div_header"  class="div_image_logo">                
                    <img  class="image_logo" alt="" src="../imagenes/topiz.jpg"  />
                </div>         
		    	 	    
		    	<h1 id="logo-text">Tasa por Servicios a la Actividad Económica</h1>			
			    <h2 id="slogan">
                    <asp:Label ID="lblslogan_municipalidad" runat="server" Text=""></asp:Label></h2>				
		    </div>
		    
		    
	  
	        <!-- div container -->	
	        <div id="content-wrap">
	  
	  
    	        <!-- div container page title -->
                <div id="div_title" style="background-color:Black;" align ="left" >
                    <strong class="titulo_page">Sistema SAE :: Listado de declaraciones juradas</strong>
                </div>
	  
	  
	  
	            <!-- div container right -->
                <div id="div_optiones" style="width:24%;  height:100%; float:right;">
                
                    <div id="div_comercio" style=" padding-top:20px;">
                        <div align="center">
                            <table style="width:95%">
                                <tr>
                                    <td  rowspan="2">
                                        <img  id="user"  alt="" src="../imagenes/user.png"/>
                                    </td>
                                    <td><h1 align ="left" style="color:Black">Comercio</h1></td>
                                </tr>
                                <tr>                                
                                    <td>
                                        NUMERO: <asp:Label ID="lblcomercio_user" runat="server" Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>
                                
                                
                            </table>
                        </div>
                       
                        
                        
                        
                        
                    </div>
                    
                    
                    <h1 align ="left" style="color:Black">Accesos</h1>                    			
	                <ul class="sidemenu">               
                        <li><a href="webfrmdeclaracionesjuradas.aspx">Ver listado de DDJJ</a></li>
                        <li><a href="webfrmctacte.aspx">Ver cuenta corriente</a></li>
                        <li><a href="webfrmopciones.aspx">Datos del comercio</a></li>				                
                        <li><a href="#" onclick="javascript:change_trade();">Cambiar comercio</a></li>			                        
                    </ul>	
	                
	                <h1 align ="left" style="color:Black">Usuario Web</h1>				
                    <ul class="sidemenu">
	                    <li><a href="webfrmdatos_representante.aspx">Mis datos</a></li>				                
		                <li><a href="webfrmcambiar_clave.aspx">Cambiar clave</a></li>	
		                <li><a href="#" onclick="javascript:return logout();">Cerrar sesión</a></li>		                
	                </ul>	
                </div>
	    
	  
	  
	  
	  
	  
	             <!-- div container left -->	
	  		    <div id="main"> 
					 				                       
                    <!-- form -->	
                    <form id="formulario" runat="server">
                    
                        <!-- Controles para utilizar la funcion doPostBack sin controles ASP -->
                        <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
                        <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
                   

                                             
                        <div style="padding-bottom:10px;">
                            <strong>ATENCIÓN:</strong>
                            Usted se encuentra viendo las DDJJ correspondientes a los últimos dos años.                            
                        </div>
            
	                        		                            
	                            
                        <!-- div container of the table movements -->
                        <div id="div_ddjj">
                        
                                                                                                        
                        </div>   
                
                        
		            </form>	
		            	         
		        </div>
  		    </div> 	
			  
			  
			<!-- div footer -->	
	  		<div id="footer">
		
		        <!-- data of the municipio -->
		        <div id="div_pie_municipalidad" style="width:70%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		            Municipalidad de
		                        <strong> 
		                <asp:Label ID="lblnombre_municipalidad" runat="server" Text="NOMBRE_MUNICIPIO"></asp:Label> 
                    </strong> 
                    
				    <br />
				    Teléfono: 
				    <strong> 
                        <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                    </strong> 
                    | Email: 
				    <strong> 
                        <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                    </strong>                 
			    </div>
		   
		   
		        <!-- data of gmdq-->
		        <div id="div_pie_grupomdq" class="footer_grupomdq">
				    Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ S.A</a></strong>  
				    <br />
				    Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				    <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
                    <asp:Label ID="lblversion" runat="server" Text=""></asp:Label>
		        </div>
			
		    </div>	
		
		</div>
		
    </body>
</html>
