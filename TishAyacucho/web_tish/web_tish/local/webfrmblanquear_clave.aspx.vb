﻿Option Explicit On
Option Strict On

Partial Public Class webfrmblanquear_clave
    Inherits System.Web.UI.Page

#Region "Variables"
    Private mWS As ws_tish.Service1
    Private mXML As String
#End Region



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim mEvent As String


        If Not (IsPostBack) Then


            Me.btnload_manager.Attributes.Add("onclick", "javascript:return load_manager();")

            Me.btnclear_key.Attributes.Add("onclick", "javascript:return activate_user();")



            Me.txtcuit.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtcuit.Attributes.Add("onblur", "javascript:event_focus(this);")


            Me.txtcuit.Attributes.Add("onkeydown", "javascript:return load_manager_key(this, event);")

            Call Me.SloganMuniicpalidad()
            Call Me.ValidarSession()
            Call Me.CargarPie()

            Me.txtcuit.Focus()
        Else

            'mEvent = Request.Params.Get("__EVENTTARGET")
            'If (mEvent IsNot Nothing) Then
            '    If (mEvent.ToString.Trim = "clear_key") Then
            '        Call ClearKey()
            '    End If
            'End If


            End If
    End Sub

    Private Sub SloganMuniicpalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema SAE."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema SAE."
        End If

    End Sub


    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If


        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub


    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("user_name") IsNot Nothing) Then
                If (Session("user_name").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        If (mLogin_OK) Then
            lblname_user.Text = Session("user_name").ToString.Trim
        Else
            Response.Redirect("webfrmlogin_rafam.aspx", False)
        End If

    End Function



    'Protected Sub btn_load_manager_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_load_manager.Click
    '    Call ObtainTrade()
    'End Sub

    'Private Sub ObtainTrade()
    '    Dim dsDataManager As DataSet


    '    Try

    '        'I obtain the data of the trade
    '        mWS = New ws_tish.Service1
    '        dsDataManager = mWS.ObtainDataManager(txtcuit.Text.Trim)
    '        mWS = Nothing


    '        'I assign the data
    '        lblmessenger.Visible = False
    '        If (dsDataManager IsNot Nothing) Then

    '            If (dsDataManager.Tables("Manager").Rows(0).Item("FECHA_BAJA").ToString.Trim <> "") Then

    '                lblmessenger.Text = "El usuario Web seleccionado se encuentra en estado de baja." & vbCr & _
    '                                    "Para realizar esta operación usted deberá primero dirigirse a la opción [Activar usuario Web] " & vbCr & _
    '                                    "que se encuentra en el menú principal."
    '                lblmessenger.Visible = True
    '            Else


    '                'lblreason_social.Text = dsDataManager.Tables("Manager").Rows(0).Item("RAZON_SOCIAL").ToString.Trim
    '                'lblcontac.Text = dsDataManager.Tables("Manager").Rows(0).Item("CONTACTO").ToString.Trim
    '                'lbladdress.Text = dsDataManager.Tables("Manager").Rows(0).Item("DIRECCION").ToString.Trim
    '                'lbltelephone.Text = dsDataManager.Tables("Manager").Rows(0).Item("TELEFONO").ToString.Trim
    '                'lblemail.Text = dsDataManager.Tables("Manager").Rows(0).Item("EMAIL").ToString.Trim
    '            End If
    '        End If


    '    Catch ex As Exception

    '    End Try

    'End Sub


    Private Sub ClearKey()
        Dim dsDataManager As DataSet
        Dim mDataManager_AUX As String
        Dim mNewKey As String

        Try



            'I clear the existing key and believe a new key
            mNewKey = ""
            mWS = New ws_tish.Service1
            mNewKey = mWS.ClearKey(txtcuit.Text.Trim)
            mWS = Nothing


            mDataManager_AUX = ""
            If (mNewKey.ToString.Trim <> "") Then


                'I obtain the data of the manager
                mWS = New ws_tish.Service1
                dsDataManager = mWS.ObtainDataManager(txtcuit.Text.Trim)
                mWS = Nothing


                If (dsDataManager IsNot Nothing) Then
                    mDataManager_AUX = mNewKey.ToString.Trim
                    mDataManager_AUX = mDataManager_AUX & "|" & txtcuit.Text.Trim
                    mDataManager_AUX = mDataManager_AUX & "|" & dsDataManager.Tables("Manager").Rows(0).Item("RAZON_SOCIAL").ToString.Trim
                    mDataManager_AUX = mDataManager_AUX & "|" & dsDataManager.Tables("Manager").Rows(0).Item("CONTACTO").ToString.Trim
                    mDataManager_AUX = mDataManager_AUX & "|" & dsDataManager.Tables("Manager").Rows(0).Item("DIRECCION").ToString.Trim
                    mDataManager_AUX = mDataManager_AUX & "|" & dsDataManager.Tables("Manager").Rows(0).Item("TELEFONO").ToString.Trim
                    mDataManager_AUX = mDataManager_AUX & "|" & dsDataManager.Tables("Manager").Rows(0).Item("EMAIL").ToString.Trim



                    Session("data_manager") = mDataManager_AUX.ToString.Trim
                    Response.Redirect("webfrmimprimir_clave.aspx", False)
                End If

            End If



        Catch ex As Exception

        End Try

    End Sub


  
    Protected Sub btnclear_key_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnclear_key.Click
        Call Me.ClearKey()
    End Sub

End Class