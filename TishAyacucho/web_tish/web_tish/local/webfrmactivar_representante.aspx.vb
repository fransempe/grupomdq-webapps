﻿Option Explicit On
Option Strict On

Partial Public Class webfrmactivar_representante
    Inherits System.Web.UI.Page


#Region "Variables"
    Private mWS As ws_tish.Service1
    Private mXML As String
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (IsPostBack) Then


            Me.txtcuit.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtcuit.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtcuit.Attributes.Add("onkeydown", "javascript:return load_manager_key(this, event);")
            Me.btnup_user.Attributes.Add("onclick", "javascript:return activate_user();")

            Call Me.SloganMuniicpalidad()
            Call Me.ValidarSession()
            Call Me.CargarPie()

            Me.txtcuit.Focus()

        End If
    End Sub

    Private Sub SloganMuniicpalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema SAE."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema SAE."
        End If

    End Sub


    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If

        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub


    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("nombre_usuario") IsNot Nothing) Then
                If (Session("nombre_usuario").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        If (mLogin_OK) Then
            lblname_user.Text = Session("user_name").ToString.Trim
        Else
            Response.Redirect("webfrmlogin_rafam.aspx", False)
        End If

    End Function


    'Protected Sub btn_load_manager_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_load_manager.Click
    '    Call ObtainTrade()
    'End Sub

    Private Sub ObtainTrade()
        Dim dsDataManager As DataSet


        Try

            'I obtain the data of the trade
            mWS = New ws_tish.Service1
            dsDataManager = mWS.ObtainDataManager(txtcuit.Text.Trim)
            mWS = Nothing


            'I assign the data
            If (dsDataManager IsNot Nothing) Then
                'lblreason_social.Text = dsDataManager.Tables("Manager").Rows(0).Item("RAZON_SOCIAL").ToString.Trim
                'lblcontac.Text = dsDataManager.Tables("Manager").Rows(0).Item("CONTACTO").ToString.Trim
                'lbladdress.Text = dsDataManager.Tables("Manager").Rows(0).Item("DIRECCION").ToString.Trim
                'lbltelephone.Text = dsDataManager.Tables("Manager").Rows(0).Item("TELEFONO").ToString.Trim
                'lblemail.Text = dsDataManager.Tables("Manager").Rows(0).Item("EMAIL").ToString.Trim
            End If


        Catch ex As Exception

        End Try

    End Sub

    'Protected Sub btn_up_user_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_up_user.Click
    '    If (ValidateData()) Then
    '        Call UpUsers()
    '    End If
    'End Sub

    Private Function ValidateData() As Boolean

        If (txtcuit.Text.Trim = "") Then
            Return False
        End If

        Return True
    End Function

    Private Sub UpUsers()

        Try

            mWS = New ws_tish.Service1
            mXML = mWS.UpUsers(txtcuit.Text.Trim)
            mWS = Nothing

            Session("type_messenger") = "operation"
            Response.Redirect("webfrmresultado.aspx", False)

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnup_user_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnup_user.Click
        If (ValidateData()) Then
            Call Me.UpUsers()
        End If
    End Sub
End Class