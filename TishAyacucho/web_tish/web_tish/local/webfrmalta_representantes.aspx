﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmalta_representantes.aspx.vb" Inherits="web_tish.webfrmalta_representantes" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
    
        
        <!-- Seteo the resources -->
        <script src="../js/funciones.js" type="text/javascript"></script>        
        <link rel="stylesheet" href="../css/estilos_listado.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_botones.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_carteles.css" type="text/css" />
        <%           
            'Seteo el css que voy a utilizar
            Dim mPath_css As String
            mPath_css = ""
            mPath_css = System.Configuration.ConfigurationManager.AppSettings("css").ToString.Trim()
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_listado.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_genericos.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_botones.css' type='text/css' />")
        %>
        
        
       
        
        <!-- mascara CUIT -->
        <script type="text/javascript" src="../js/jquery.js"></script>
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" language="javascript"  src="../js/jquery.maskedinput.js"></script>
        <script type="text/javascript">
            jQuery(function($){
                $("#txtcuit").mask("99-99999999-9"); 
            });
        </script>
        
        
        
        <!-- ajax -->       
        <script type="text/javascript" language="javascript" src="../js/jquery1-2-6-min.js"></script>
        <script type="text/javascript" language="javascript">
        
            /* Carga del logo de la municipalidad */
            function ajax_load_logo(){   
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_logo.ashx",                                                            
                    success: see_response_logo
                });
            }
            
            
            function see_response_logo(html){                   
                if (html != '') {
                    var _control = document.getElementById('div_header');                   
                    _control.innerHTML = html;                   
                    return false;
                }
                
            return true;    
            }          
        </script>
        
        
        
        <!-- Esta funcion da el efecto de cargando... miestras va al server -->
        <link href="../efecto_ajax/cssUpdateProgress.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript">
	        var ModalProgress = '<%= ModalProgress.ClientID %>';         
        </script>
        
        
        <title>Sistema SAE :: Alta de usuarios Web</title>
        <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
    	
        
        
                
        
        
        <script type="text/javascript">

            function to_validate_data() {               

                /*--------------- CUIT ----------------*/
                /* control empty */
                if (control_vacio(window.document.getElementById('txtcuit'),'CUIT:\nDebe ingresar un número de cuit.')) {
                   return false;
                }
                
                
                /* cuit validate */
                /*
                var _cuit;
                _cuit = window.document.getElementById('txtcuit').value;
                
                alert(_cuit.toString());
                if (!validar_cuit(window.document.getElementById('txtcuit'),'CUIT')) {          
                    return false;
                }
                */

                /*---------- REAZON SOCIAL ------------*/
                /* control empty */
                if (control_vacio(window.document.getElementById('txtreason_social'),'Razon social:\nDebe ingresar la razon social.')) {
                   return false;
                }
                
                /* reazon social validate */
                if (!validar_cadena(window.document.getElementById('txtreason_social'),'Razon social')) {          
                    return false;
                }


                /*------------- CONTACT ---------------*/
                /* control empty */
                if (control_vacio(window.document.getElementById('txtcontact'),'Contacto:\nDebe ingresar un contacto.')) {
                   return false;
                }
                
                /* contact validate */
                if (!validar_cadena(window.document.getElementById('txtcontact'),'Contacto')) {          
                    return false;
                }


                /*------------- ADDRESS --------------*/
                /* control empty */
                if (control_vacio(window.document.getElementById('txtaddress'),'Direccion:\nDebe ingresar un direccion.')) {
                   return false;
                }
                
                /* address validate */
                if (!validar_cadena(window.document.getElementById('txtaddress'),'Direccion')) {          
                    return false;
                }


                /*------------ TELEPHONE -------------*/
                /* control empty */
                if (control_vacio(window.document.getElementById('txttelephone'),'Telefono:\nDebe ingresar un telefono.')) {
                   return false;
                }
                
                /* address validate */
                if (!validar_cadena_telefono(window.document.getElementById('txttelephone'),'Telefono')) {          
                    return false;
                }


                /*-------------- EMAIL ---------------*/
                /* control empty */
                if (control_vacio(window.document.getElementById('txtemail'),'E-mail:\nDebe ingresar un email.')) {
                   return false;
                }
                
                /* email validate */
                if (!validate_email(window.document.getElementById('txtemail'))) {          
                    return false;
                }
                                
                
                
                
                
            return true;
            }
            
       

</script>
    
            
     
       

<script type="text/javascript" language="javascript">
    
    
    
    /* Esta funcion genera el mensaje al usuario de que esta a punto de cerrar la session de usuario*/
    function cerrar_session() {
        return confirm("Usted esta a punto de cerrar su sesión\n¿Desea continuar?")              
    }
    
    
   
    /* Esta funcion elimina los comercios asociados al contribuyente */          
    function delete_trade(pos){   
        var respuesta;
        
        respuesta = confirm('Usted esta a punto de eliminar el comercio seleccionado\n¿Desea continuar?');
        if (respuesta) {
            __doPostBack("delete_trade", pos);
        }
	}


 
    
    
    function to_validate_load_trade () {
    
        if (control_vacio(window.document.getElementById('txtnumber_trade'),'Número de comercio:\nDebe ingresar un número de comercio.')) {
            return false;   
        }
    
    
        if (!validar_cadena_solo_numeros(window.document.getElementById('txtnumber_trade'),'Número de comercio')) {
            return false;   
        }
        
    return true;
    }



    function to_validate_add_trade() {   
        if (window.document.getElementById('lblreason_social').innerHTML == '') {            
            alert('Debe seleccionar un comercio a agregar.');
            return false;            
        }
        
    return true;
    }
    



    

</script>
        
        
    </head>

    <body>
    
        
        <!-- wrap starts here -->	
        <div id="wrap">
                     
    		
                    
                       
		    <div id="header">
		          
		        <!-- div container font -->
                <div id="container_font">			        
		        <div class="div_font" style="padding:0px 5px 0px 5px;">
                    <a href="#" title="Usar fuente menor" class="link_font" onclick="assign_font('-');">A-</a> 
                </div>
                
                <div class="div_font">
		            <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 
                </div>		            
                
		        <div class="div_font">
		            <a href="#" title="Usar fuente mayor"  class="link_font" onclick="assign_font('+');">A+</a> 
		        </div>	            
            </div>
            
                    
		    <div id="div_header"  class="div_image_logo">                
 	             <img  class="image_logo" alt="" src="../imagenes/topiz.jpg"  />
            </div>   
		                
			<h1 id="logo-text">Tasa por Servicios a la Actividad Económica</h1>			
			<h2 id="slogan"><asp:Label ID="lblslogan_municipalidad" runat="server" Text=""></asp:Label></h2>	
					
						
			
		</div>
	  
            <!-- content-wrap starts here -->
            <div id="content-wrap">
            
            
                <!-- div container page title -->
                <div id="div_title" style="background-color:Black;" align ="left" >
                    <strong class="titulo_page">Sistema SAE :: Alta de usuarios Web</strong>
                </div>

    	  
    	  
                <!-- div container right -->
                <div id="div_optiones" class="menu_options">
                    
                    <div id="div_comercio" style=" padding-top:20px;">
                            <div align="center">
                                <table style="width:95%">
                                    <tr>
                                        <td  rowspan="2"><img id="user" alt="" src="../imagenes/user.png"  style=" width:60px; height:60px; border:none"/></td>
                                        <td><p  class="titulo_usuario"align ="left" style="color:Black; height: 25px;">Bienvenido</p></td>
                                    </tr>
                                    <tr>                                
                                        <td>
                                            Usuario: <asp:Label ID="lblnombre_usuario" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        
                    <h1 align ="left" style="color:Black">Opciones</h1>                    			
                    <ul class="sidemenu">                                       	                    
                        <li><a href="webfrmopciones_rafam.aspx" >Volver a opciones</a></li>	                					
                        <li><a href="../webfrmindex.aspx" onclick="javascript:return cerrar_session();">Cerrar sesión</a></li>	                					
                    </ul>	
                </div>
    	    
    	  
    	  
  		        <div id="main"> 
    		    
                    <form id="formulario" runat="server">
                    
                    <div id="div_container_trade_ajax" align="center" style="padding-bottom:30px;">
                    
                            <script type="text/javascript" src="../efecto_ajax/jsUpdateProgress.js"></script>		
	                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	                        
	                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
	                            <ContentTemplate >
                    
                    
                                    <div  align="left"  id="div_link_manager"  runat="server"> 
                                        <strong class="titulo_3">Usuario Web</strong>  - 
                                        <asp:LinkButton ID="link_manager" runat="server">[Ocultar]</asp:LinkButton>                               
                                        <hr class="linea"/>
                                    </div>	
                        
                        
                                    <div id="div_menssage_userweb" class="Globo GlbRed" style="width:70%;" runat="server" visible="false">
                                        <asp:Label ID="lblmessage_user_web" runat="server" Text="" Visible="False"></asp:Label>
                                    </div>
                        
                    
                                    <!-- div data manager-->
                                    <div id="div_manager" align="center" style="width:100%; padding-bottom:10px;" runat="server">
                                <table id="table_data_manager" border="0" style="width: 70%; height: 136px; background-color: #FFFFFF; color: #000000;" align="center">
                                
                                    <tr>
                                        <td class="columna_titulo_left">CUIT:</td>
                                        <td class="columna_dato_left">
                                            <asp:TextBox ID="txtcuit" runat="server" CssClass="textbox" MaxLength="11" Width="98%" ></asp:TextBox>
                                        </td>
                                    </tr> 
                                                
                                    <tr>
                                        <td class="columna_titulo_left">Razón social:</td>                        
                                        <td class="columna_dato_left">
                                            <asp:TextBox ID="txtreason_social" runat="server" CssClass="textbox" 
                                                MaxLength="50" Width="98%"></asp:TextBox>
                                        </td>                                                                                                   
                                    </tr>
                                    
                                    <tr>
                                        <td class="columna_titulo_left">Persona de contacto:</td>                        
                                        <td class="columna_dato_left">
                                            <asp:TextBox ID="txtcontact" runat="server" CssClass="textbox" MaxLength="50" 
                                                Width="98%"></asp:TextBox>
                                        </td>                                                                                                   
                                    </tr>
                                    
                                    <tr>
                                        <td class="columna_titulo_left">Dirección:</td>                        
                                        <td class="columna_dato_left">
                                            <asp:TextBox ID="txtaddress" runat="server" CssClass="textbox" MaxLength="50" 
                                                Width="98%"></asp:TextBox>
                                        </td>                                                                                                   
                                    </tr>
                                    
                                    <tr>
                                        <td class="columna_titulo_left">Teléfono:</td>                        
                                        <td class="columna_dato_left">
                                            <asp:TextBox ID="txttelephone" runat="server" CssClass="textbox" MaxLength="20" 
                                                Width="98%"></asp:TextBox>
                                        </td>                                                                                                   
                                    </tr>
                                    
                                    <tr>
                                        <td class="columna_titulo_left">e-mail</td>                        
                                        <td class="columna_dato_left">
                                            <asp:TextBox ID="txtemail" runat="server" CssClass="textbox" MaxLength="50" 
                                                Width="98%"></asp:TextBox>
                                        </td>                                                                                                   
                                    </tr>                        
                                    
                                
                                </table>   
                            
                            </div>  
                
                    

                    
                        
                    
            		       
            		     
            		     
            		                <div id="div_link_add_trade"  align="left"  runat="server"> 
                                        <strong class="titulo_3">Agregar Comercios</strong>  - 
                                        <asp:LinkButton ID="link_add_trade" runat="server">[Ver]</asp:LinkButton>                                        
                                        <hr class="linea"/>
                                    </div>	
            		     
            		     
            		                <div id="div_container_trade" align="center" style="padding-bottom:30px;"  runat="server">
            		             
            		                    <!-- message for the user -->
                                        <div id="div_mensaje" class="Globo GlbRed" style="width:80%;" runat="server" visible="false">
                                            <asp:Label ID="lblmensaje" runat="server" Text="" Visible="False"></asp:Label>
                                        </div>
                                       
                                
                                        <!-- div new user -->
                                        <div id="add_user" align="center" style="width:100%">
                                        
                                            <div id="div_table_trade" style="width:100%">
                                                <table id="table_trade" border="0" style="width: 100%; background-color: #FFFFFF; color: #000000;" align="center">                                        
                                                    <tr>   
                                                        <td>            		                                                                       
                                                            <table id="table_select_trade" align="center"  border="0" style="width:100%;" runat="server">                                                                                                
                                                            
                                                                <tr>
                                                                
                                                                    <td   align="center" style="width:25%">Nro. Comercio:</td>
                                                            
                                                                    <td  class="columna_dato_left" style="width:25%">
                                                                        <asp:TextBox ID="txtnumber_trade" runat="server" CssClass="textbox" MaxLength="10" Width="100%"></asp:TextBox>
                                                                    </td>
                                                            
                                                                    <td  class="columna_dato_left" style="width:25%">                                                            
                                                                        <asp:ImageButton ID="btn_load_trade" runat="server" ImageUrl="~/imagenes/load.gif" CssClass="boton_load" />                                                            
                                                                    </td>
                                                                    
                                                                </tr>
                                                            </table>
                                                            
                                                            
                                                            <table id="table_data_trade" align="center"  border="0" style="width:100%;" runat="server">
                                                                <tr>
                                                                    <td class="columna_dato_left" style="width:10%">Razon Social:</td>                                        
                                                                    <td class="columna_dato_left" style="width:90%" align="left">                                                        
                                                                        <asp:Label ID="lblreason_social" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                
                                                                <tr>
                                                                    <td class="columna_dato_left" style="width:10%">Contribuyente:</td>                                        
                                                                    <td class="columna_dato_left" style="width:90%" align="left">                                                        
                                                                        <asp:Label ID="lblcontribuyente" runat="server" Text=""></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                
                                                                <tr align="right">
                                                                    <td  colspan="2" align="right" style="width:100%;">
                                                                        <asp:Button ID="btnadd_trade" runat="server"                                                          
                                                                            Text="Agregar" BorderStyle="None" 
                                                                            CssClass="boton" Height="30px" Width="70px"                                                                                                                                                         
                                                                            ToolTip="Agregar comercio"
                                                                        />                                                                                                                             
                                                                                                                          
                                                            
                                                                        <asp:Button ID="btncancel" runat="server"                                                          
                                                                            Text="Cancelar" BorderStyle="None" 
                                                                            CssClass="boton" Height="30px" Width="70px"              
                                                                            ToolTip="Cancelar."
                                                                        />
                                                                    </td>                                                            
                                                                </tr>
                                                                        
                                                            </table>                                        
                                                        </td>
                                                    </tr>                                        
                                                </table>
                                            </div>         
                                                 
                                                 
                                                 
                                                 
                                            <div id="div_list_trades" style="padding-top:20px; width:100%;">
                                                <table id="table1" border="0" style="width: 100%; background-color: #FFFFFF; color: #000000;" align="center">                                                    		            
                                                    <tr>   
                                                        <td>
                        		                            
                                                            <!-- Div contenedor de la tabla de movimientos -->
                                                            <div id="div_tabla_movimientos" runat="server" >                                    
                                		                         		                
                                                        
                                                        
                                                                <asp:GridView ID="gvlist_trade" runat="server" AllowSorting="True" 
                                                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" 
                                                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="1" EnableTheming="True" 
                                                                    ForeColor="Black" GridLines="Vertical" PageSize="5" Width="100%">
                                                                    <FooterStyle BackColor="#CCCCCC" />
                                                                    <RowStyle ForeColor="Black" />
                                                                    <Columns>
                                                                        <asp:BoundField DataField="COMERCIO_NUMERO" HeaderStyle-Width="55px" 
                                                                            HeaderText="Número">
                                                                            <HeaderStyle HorizontalAlign="Center" Width="55px" />
                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="15%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="COMERCIO_RAZONSOCIAL" HeaderText="Razon social">
                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="40%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="CONTRIBUYENTE_NOMBRE" HeaderText="Contribuyente">
                                                                            <FooterStyle HorizontalAlign="Center" />
                                                                            <HeaderStyle HorizontalAlign="Left" />
                                                                            <ItemStyle HorizontalAlign="Left" Width="40%" />
                                                                        </asp:BoundField>
                                                                        
                                                                        
                                                                        <asp:templatefield HeaderText="" ShowHeader="False"  ControlStyle-Font-Bold="true" ItemStyle-Width="5px" HeaderStyle-BackColor="Black" >
                                                            <HeaderTemplate>Eliminar</HeaderTemplate>
                                                            
                                                            <ItemTemplate> 
                                                                <asp:Image ID="image1"  runat="server"  ImageUrl="~/imagenes/delete.gif" Width="16px" Height="16px"/>
                                                            </ItemTemplate>
                                                                 
                                                            
                                                            <ControlStyle CssClass="columna_eliminar" />
                                                                 
                                                            
                                                            <HeaderStyle BackColor="Black" VerticalAlign="NotSet" Wrap="True" 
                                                                HorizontalAlign="Center"  Width="5%"/>
                                                            <ItemStyle Wrap="True" Width="5%" HorizontalAlign="Center"  />                                                             
                                                        </asp:templatefield>
                                                                        
                                                                        
                                                                    </Columns>
                                                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                                                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                                                                    <AlternatingRowStyle BackColor="#CCCCCC" />
                                                                </asp:GridView>
                                                             
                                                            
                                                            </div>   
                                                             
                                                        </td>                                
                                                    </tr>                                                     
                                                </table>                                                                          
                                            </div>
                                                
                                                
                                      
                                             
                                        </div>
                              	
                                    </div>      
                                    
                                  <div align="right">
                                        <asp:Button ID="btnagree" runat="server"                                                          
                                            Text="Guardar" BorderStyle="None" 
                                            CssClass="boton" Height="35px" Width="100px"                                                     
                                            ToolTip="Guardar"                                                           
                                        />                                        
                                    </div>
                                             
                        
	                            </ContentTemplate >		
                            </asp:UpdatePanel>
                        
                            <!-- panel ajax-->
                            <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
                                <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server">
                                    <ProgressTemplate>
	                                    <div style="position: relative; text-align: center; top: 3px; left: 1554px; height: 17px;">
		                                    <img src="../efecto_ajax/loading.gif" style="vertical-align: middle" alt="Processing" width="16px" height="16px"/>
		                                    Cargando ...
	                                    </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </asp:Panel>
                            <ajaxToolkit:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress" BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
                
                        
	                    </div>
	                    
	                </form>    
                       
  		        </div> 	
    
	        </div>
		
		
		    <!-- div data footer -->
		    <div id="footer">	
		       <div id="div_pie_municipalidad" 
                    style="width:70%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		            Municipalidad de
		                <strong> 
		                    <asp:Label ID="lblnombre_municipalidad" runat="server" Text="NOMBRE_MUNICIPIO"></asp:Label> 
                        </strong> 
                        
				    <br />
				    Teléfono: 
				        <strong> 
                            <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                        </strong> 
                     | Email: 
				        <strong> 
                            <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                        </strong> 
                    
			    </div>
    		   
		       <div id="div_pie_grupomdq"  class="footer_grupomdq">
				    Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ S.A</a></strong>  
				    <br />
				    Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				    <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
                   <asp:Label ID="lblversion" runat="server" Text=""></asp:Label>
		       </div>
		    </div>	
		    
	                    
                       
	                </form>
    				
      								
        </div>	



    </body>
</html>
