﻿Option Explicit On
Option Strict On

Partial Public Class webfrmgenerar_clave
    Inherits System.Web.UI.Page

#Region "Variables"
    Private mWS As ws_tish.Service1
    Private mXML As String

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not (IsPostBack) Then

            Call Me.SloganMunicipalidad()
            Call Me.ValidarSession()
            Call Me.CargarPie()


            'I assign the events javascript
            'btncargar.Attributes.Add("onclick", "javascript:return validar_datos();")
            'btngenerar_clave.Attributes.Add("onclick", "javascript:return generar_clave();")

            Me.txtnro_comercio.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtnro_comercio.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtcontribuyente_cuit.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtcontribuyente_cuit.Attributes.Add("onblur", "javascript:event_focus(this);")



            'Cargar el Proveedor seleccionado
            If (Session("codigo_proveedor") IsNot Nothing) Then
                Me.txtnro_comercio.Text = Session("codigo_proveedor").ToString.Trim
                Call Me.CargarComercio(CLng(Me.txtnro_comercio.Text.Trim))
            End If


            Me.txtnro_comercio.Focus()
        Else
            'Call LimpiarControles()
        End If
    End Sub

    Protected Sub btncargar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btncargar.Click
        Call CargarComercio(CLng(txtnro_comercio.Text.Trim))
    End Sub


    Protected Sub btnbuscar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnbuscar.Click
        Response.Redirect("webfrmbuscar_comercio.aspx", False)
    End Sub

    Private Sub SloganMunicipalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema SAE."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema SAE."
        End If

    End Sub


    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("nombre_usuario") IsNot Nothing) Then
                If (Session("nombre_usuario").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        If (mLogin_OK) Then
            lblnombre_usuario.Text = Session("nombre_usuario").ToString.Trim
        Else
            Response.Redirect("webfrmlogin_rafam.aspx", False)
        End If

    End Function

    Private Sub CargarComercio(ByVal pNroComercio As Long)
        Dim dsDatos As DataSet
        Dim mDatos_XML As String
        Dim mCUIT_Comercio As String


        Try

            txtcontribuyente_cuit.Text = ""



            'Obtengo los datos
            mWS = New ws_tish.Service1
            mDatos_XML = mWS.ObtenerDatosComercio(pNroComercio.ToString.Trim)
            mWS = Nothing


            dsDatos = New DataSet
            dsDatos = clsTools.ObtainDataXML(mDatos_XML.ToString)


            lblmensaje.Visible = False
            If (clsTools.HasData(dsDatos)) Then


                'Si el estado es OK asigno los datos del comercio
                If (dsDatos.Tables(0).Rows(0).Item("COMERCIO_ESTADO").ToString.Trim = "OK") Then


                    'Asigno los datos
                    lblcomercio_nombre.Text = dsDatos.Tables(0).Rows(0).Item("COMERCIO_NOMBRE").ToString.Trim
                    lblcomercio_nombrefantasia.Text = dsDatos.Tables(0).Rows(0).Item("COMERCIO_NOMBREFANTASIA").ToString.Trim
                    lblcomercio_domicilio.Text = dsDatos.Tables(0).Rows(0).Item("COMERCIO_UBICACION").ToString.Trim
                    lblcontribuyente_nombre.Text = dsDatos.Tables(0).Rows(0).Item("CONTRIBUYENTE_NOMBRE").ToString.Trim



                    'Le aviso al usuario si el comercio ya tiene una clave asignada
                    mCUIT_Comercio = ""
                    mCUIT_Comercio = ClaveExistente(pNroComercio)
                    If (mCUIT_Comercio.ToString.Trim <> "") Then
                        lblestado_clave.Text = "CLAVE EXISTENTE."
                        txtcontribuyente_cuit.Text = mCUIT_Comercio.ToString.Trim
                    Else
                        lblestado_clave.Text = "CLAVE INEXISTENTE."
                    End If

                    Session("comercio_clave") = dsDatos.Tables(0).Rows(0).Item("CLAVE").ToString.Trim
                    txtcontribuyente_cuit.Focus()
                Else

                    If (dsDatos.Tables(0).Rows(0).Item("COMERCIO_ESTADO").ToString.Trim = "BAJA") Then
                        lblmensaje.Text = "Este comercio ha sido dado de baja."
                    Else
                        lblmensaje.Text = "Comercio inexistente."
                    End If
                    lblmensaje.Visible = True


                    Call LimpiarControles()
                End If

            Else
                Call LimpiarControles()
            End If



        Catch ex As Exception
            mWS = Nothing
        End Try
    End Sub

    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If

        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub

    Private Sub btngenerar_clave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btngenerar_clave.Click


        'Creo la clave para el proveedor WEB
        Session("comercio_clave") = CrearClave()


        'Variables para la Impresion
        If (Session("comercio_clave").ToString.Trim <> "") Then
            Session("comercio_numero") = txtnro_comercio.Text.Trim
            Session("comercio_nombre") = lblcomercio_nombre.Text.Trim
            Session("comercio_nombrefantasia") = lblcomercio_nombrefantasia.Text.Trim
            Session("contribuyente_cuit") = txtcontribuyente_cuit.text.trim
            Session("contribuyente_nombre") = lblcontribuyente_nombre.Text.Trim



            Response.Redirect("webfrmimprimir_clave.aspx", False)
        End If

    End Sub



    Private Function CrearClave() As String
        Dim mClave_AUX As String

        Try

            'Creo el comercio Web con su clave
            mClave_AUX = ""
            mWS = New ws_tish.Service1
            'mClave_AUX = mWS.CrearComercioWeb(CLng(txtnro_comercio.Text.Trim), txtcontribuyente_cuit.Text.Trim)
            mWS = Nothing

        Catch ex As Exception
            mWS = Nothing
            mClave_AUX = ""
        End Try


        Return mClave_AUX.ToString.Trim
    End Function


    Private Function ClaveExistente(ByVal pNroComercio As Long) As String
        Dim mCUIT As String

        Try

            'busco si el comercio ya tiene una clave asignada
            mCUIT = ""
            mWS = New ws_tish.Service1
            mCUIT = mWS.ExisteComercio(pNroComercio)
            mWS = Nothing

        Catch ex As Exception
            mWS = Nothing
            mCUIT = ""
        End Try


        Return mCUIT.ToString.Trim
    End Function


    Private Sub LimpiarControles()
        lblcomercio_nombre.Text = ""
        lblcomercio_nombrefantasia.Text = ""
        lblcomercio_domicilio.Text = ""
        txtcontribuyente_cuit.Text = ""
        lblcontribuyente_nombre.Text = ""
        lblestado_clave.Text = ""
        Session("comercio_clave") = ""
    End Sub
End Class