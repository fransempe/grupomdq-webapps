﻿Public Partial Class webfrmopciones_rafam
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (IsPostBack) Then
            Call Me.SloganMunicipalidad()
            Call Me.ValidarSession()
            Call Me.CargarPie()
        End If

    End Sub


    Private Sub SloganMunicipalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema SAE."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema SAE."
        End If

    End Sub

    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("user_name") IsNot Nothing) Then
                If (Session("user_name").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        If (mLogin_OK) Then
            lblname_user.Text = "Bienvenido usuario " & Session("user_name").ToString.Trim
        Else
            Response.Redirect("webfrmlogin_rafam.aspx", False)
        End If


    End Function

    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If

        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub


    Protected Sub link_new_manager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_new_manager.Click        
        Response.Redirect("webfrmalta_representantes.aspx", False)
    End Sub

    Protected Sub link_edit_manager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_edit_manager.Click        
        Response.Redirect("webfrmeditar_representante.aspx", False)
    End Sub

    
    Protected Sub link_clear_key_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_clear_key.Click
        Response.Redirect("webfrmblanquear_clave.aspx", False)
    End Sub


    Protected Sub link_down_user_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_down_user.Click
        Response.Redirect("webfrmbaja_representante.aspx", False)
    End Sub

    Protected Sub link_up_user_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_up_user.Click
        Response.Redirect("webfrmactivar_representante.aspx", False)
    End Sub

    
    Protected Sub link_abm_date_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_abm_date.Click
        Response.Redirect("webfrmexpiration_periods_list.aspx", False)
    End Sub
End Class