﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmactivar_representante.aspx.vb" Inherits="web_tish.webfrmactivar_representante" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Sistema SAE :: Activación de usuarios Web</title>
        <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
    	
        

        <!-- Seteo the resources -->
        <script src="../js/funciones.js" type="text/javascript"></script>
        <script src="../js/jquery.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../css/estilos_listado.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_botones.css" type="text/css" />   
        <link rel="stylesheet" href="../css/estilos_carteles.css" type="text/css" />    
        <%           
            'Seteo el css que voy a utilizar
            Dim mPath_css As String                
            mPath_css = ""
            mPath_css = System.Configuration.ConfigurationManager.AppSettings("css").ToString.Trim()
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_listado.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_genericos.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_botones.css' type='text/css' />")            
        %> 



        <!-- mascara CUIT -->
        <script type="text/javascript" src="../js/jquery.js"></script>
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" language="javascript"  src="../js/jquery.maskedinput.js"></script>
        <script type="text/javascript">
            jQuery(function($){
                $("#txtcuit").mask("99-99999999-9"); 
            });
        </script>



                           
        <!-- ajax -->       
        <script type="text/javascript" language="javascript" src="../js/jquery1-2-6-min.js"></script>
        <script type="text/javascript" language="javascript">
        
            jQuery.noConflict();
            function ajax_load_manager(p_cuit){                                              
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_load_manager.ashx",                                        
                    data:"action=activate&cuit=" + p_cuit, 
                    success: see_response
                });
            }
            
            
            function see_response(html){                   
                if (html != '') {
                    var _control = document.getElementById('div_data_manager');
                    _control.style.display = 'none';
                    _control.innerHTML = html;
                    show_div();
                    return false;
                }
                
            return true;    
            }          
            
            
            
	        function show_div() {	
	            var _control;

		        jQuery("#div_data_manager").slideToggle();		
		        
		        _control = document.getElementById('div_button');
		        if (!control_exists(document.getElementById('lblmessenger'))) {		            
		            _control.style.display = 'block';            		        
		        } else {
		            _control.style.display = 'none';            
		        }		                 
	        }
	        
	        
	        /* Carga del logo de la municipalidad */
            function ajax_load_logo(){   
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_logo.ashx",                                                            
                    success: see_response_logo
                });
            }
            
            
            function see_response_logo(html){                   
                if (html != '') {
                    var _control = document.getElementById('div_header');                   
                    _control.innerHTML = html;                   
                    return false;
                }
                
            return true;    
            }        
        </script>
        
        
        
        <script type="text/javascript">
            var _control;
            var _codehtml;
        
        
            function load_manager() {  
            
                if (validate_data()) {
                
                    _codehtml = '<table border="0" width="100%" align="center">' +
                                    '<tr>' + 
                                        '<td align="right" style="width:35%;"><img alt="" src="../imagenes/loading.gif"/ width="24px" height="24px" /></td>' + 
                                        '<td align="left" style="width:75%;"><p>Cargando datos ...</p></td>' +                                      
                                    '</tr>' +
                                '</table>';


                
                    _control = document.getElementById('div_data_manager');                
                    _control.innerHTML = _codehtml;
                              
                    ajax_load_manager(window.document.getElementById('txtcuit').value);
                
                }
            return false;
            }
        
        
        
             function validate_data() {

                    //Cadena Vacia
                    if (control_vacio(window.document.getElementById('txtcuit'),'Número de comercio:\nDebe ingresar un número de comercio.')) {
                       return false;
                    }
                    
                    /*
                    //Cadena Valida
                    if (!validar_cadena_solo_numeros(window.document.getElementById('txtcuit'),'Número de comercio')) {          
                        return false;
                    }
                    */
                return true;
                }
                
                
                
            function activate_user() {
                var _messenger;
                
                _messenger = false;
                _messenger = confirm('Usted esta a punto de activar a este usuario.\n ¿Desea continuar?');    
                if (_messenger) {
                
                    _codehtml = '<table border="0" width="100%" align="center">' +
                                    '<tr>' + 
                                        '<td align="right" style="width:35%;"><img alt="" src="../imagenes/loading.gif"/ width="24px" height="24px" /></td>' + 
                                        '<td align="left" style="width:75%;"><p>Habilitando usuario ...</p></td>' +                                      
                                    '</tr>' +
                                '</table>';
                
                     
                    _control = document.getElementById('div_data_manager');                
                    _control.innerHTML = _codehtml;
                    
               
                
                    __doPostBack("btnup_user",'')               
                   
                    return true;
                }
                
            return false;
            }
            
                
        </script>
        
                
        <!-- functions -->
        <script type="text/javascript">

            function to_validate_data() {

                /* -- Nro de Comercio -- */
                //Cadena Vacia
                if (control_vacio(window.document.getElementById('txtcuit'),'CUIT:\nDebe ingresar un número de cuit.')) {
                   return false;
                }
                
                //Cadena Valida
                if (!validar_cuit(window.document.getElementById('txtcuit'),'Número de cuit')) {          
                    return false;
                }
                
                //load user
                if (window.document.getElementById('lblreason_social').innerHTML == '') {
                    alert('Debe cargar al usuario Web');
                    return false;                
                }               
                
                //messenger user
                var _messenger = false;
                _messenger = confirm('Usted esta a punto de activar este usuario Web.\n¿Desea continuar?');
                if (!_messenger) {
                    return false;
                }
                
                
            return true;
            }
            
    
    
       
            function to_validate_load_manager() {
                             
                //control empty
                if (control_vacio(window.document.getElementById('txtcuit'),'CUIT:\nDebe ingresar un número de cuit.')) {
                   return false;
                }
                
                //string validate
                if (!validar_cuit(window.document.getElementById('txtcuit'),'CUIT')) {          
                    return false;
                }
            
                return true;
            }
            
    
    
    
    
    
            /* Esta funcion genera el mensaje al usuario de que esta a punto de cerrar la session de usuario*/
            function cerrar_session() {
                return confirm("Usted esta a punto de cerrar su sesión\n¿Desea continuar?")              
            }
    
    
    
            /* Esta Funcion ASIGNA ESTILO a un BOTON */          
            function efecto_over(boton){
                boton.className = "boton_gris"; 	        
	        }
        	    

            /* Esta Funcion ASIGNA ESTILO a un BOTON */          
  	        function efecto_out(boton){
                boton.className = "boton_verde";    
	        }


            function load_manager_key(val, ev){

                if (window.event) {
                    var key = window.event.keyCode;
                } else {
                    var key = ev.which;
                }
                
             
                if (key == 13 || key == 9){    
                   return load_manager(); 
                }
            }



 
</script>


        <!-- recursos para crear la ventana modal -->
	    <link rel="stylesheet" href="../modal/Assets/LightFace.css" />	
	    <script src="../modal/mootools.js" type="text/javascript"></script>
	    <script src="../modal/mootools-more-drag.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.IFrame.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Image.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Request.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Static.js" type="text/javascript"></script>
        

        <!-- Logout -->
        <script type="text/javascript">
        	function logout() {	 	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema SAE :: Cerrar sesión', 
				    width: 250,
				    height: 50,
	  			    content: '<div align="left">Usted esta a punto de cerrar su sesión.\n¿Desea continuar?</div>',
	 			    buttons: [		
	 			        {
						    title: 'Aceptar',
						    event: function() { this.close();window.location= "../webfrmindex.aspx";}
					    },
					    {
						    title: 'Cerrar',
						    event: function() { this.close();}
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }        
        </script>

                    
    </head>


    <body>
    
        
        <!-- wrap starts here -->	
        <div id="wrap">
                     
		    <div id="header">
		          
		        <!-- div container font -->
                <div id="container_font">			        
		        <div class="div_font" style="padding:0px 5px 0px 5px;">
                    <a href="#" title="Usar fuente menor" class="link_font" onclick="assign_font('-');">A-</a> 
                </div>
                
                <div class="div_font">
		            <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 
                </div>		            
                
		        <div class="div_font">
		            <a href="#" title="Usar fuente mayor" class="link_font" onclick="assign_font('+');">A+</a> 
		        </div>	            
            </div>
            
                    
		    <div id="div_header"  class="div_image_logo">                
 	             <img  class="image_logo" alt="" src="../imagenes/topiz.jpg"  />
            </div>   
                    
		          
		    <!-- title header-->            
			<h1 id="logo-text">Tasa por Servicios a la Actividad Económica</h1>			
			<h2 id="slogan">
                <asp:Label ID="lblslogan_municipalidad" runat="server" Text=""></asp:Label></h2>	
		
		</div>
	  
	  
            <!-- content-wrap starts here -->
            <div id="content-wrap">
    	  
    	        
    	        <!-- div container page title -->
                <div id="div_title" style="background-color:Black;" align ="left" >
                    <strong class="titulo_page">Sistema SAE :: Activación de usuarios Web </strong>
                </div>
    	  
    	  
    	  
                <!-- div container right -->
                <div id="div_optiones" class="menu_options">
                    
                    <div id="div_user" style=" padding-top:20px;">
                            <div align="center">
                                <table style="width:95%">
                                    <tr>
                                        <td  rowspan="2"><img id="user" alt="" src="../imagenes/user.png"  style=" width:60px; height:60px; border:none"/></td>
                                        <td><p  class="titulo_usuario"align ="left" style="color:Black; height: 25px;">Bienvenido</p></td>
                                    </tr>
                                    <tr>                                
                                        <td>
                                            Usuario: <asp:Label ID="lblname_user" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        
                    <h1 align ="left" style="color:Black">Opciones</h1>                    			
                    <ul class="sidemenu">                                       	                    
                        <li><a href="webfrmopciones_rafam.aspx" >Volver a opciones</a></li>	                					
                        <li><a href="#" onclick="javascript:logout();">Cerrar sesión</a></li>	                					
                    </ul>
                    	
                    	
                </div>
                

    	  
    	  
  		        <div id="main"> 
    			
                    
                    
                    <!-- form of the page -->
                    <form id="formulario" runat="server">
                    
             
                        <!-- div container -->
                        <div id="div_container" align="center" style="padding-bottom:30px;"  runat="server">
                        
                                <!-- div add trade -->
                                <div id="add_trade" align="center" style="width:90%">

                                    
                                    <div id="div_messenger">
                                    </div>
                                                                        
                                    <table id="table_container" border="0" align="center" style="width:100%;">
                                        <tr>
                                            <td style= "padding-top:10px;" align="center">
                                               <table id="table_data_manager" border="0" cellspacing="10" style="border-style: double; width: 90%; height: 220px; background-color: #FFFFFF; color: #000000;" align="center">
                        
                                                    <tr>
                                                        <td class="column_data1" align="right">CUIT:</td>
                                                        <td class="column_data2">
                                                            <asp:TextBox ID="txtcuit" runat="server" CssClass="textbox" MaxLength="11" Width="80%" ></asp:TextBox>                                        
                                                        </td>
                                                        <td>
                                                            <img alt="" src="../imagenes/load.gif" class="boton_load" onclick="javascript:return load_manager();" />                                         
                                                        </td>
                                                    </tr> 
                                                 
                                                   <tr>
                                                        <td id ="td_data_manager" colspan="3" align="center">  
                                                            <div id="div_data_manager"  style="display:none;" align="center">
                                                            
                                                            </div>                                                     
                                                        </td>
                                                   </tr>                                     
                                                </table>
        		                            		 
        		                            		 
        		                            	<!-- div container buttons -->   
        		                                <div id="div_button" align="center" style="padding-top:15px; display:none;">
                                                    <table id="table_buttons">
                                                        <tr>
                                                            <td align="center">
                                                                <asp:ImageButton ID="btnup_user" runat="server" Height="32px" ImageUrl="~/imagenes/user.png" Width="32px" />                                                                                                                                                                                    
                                                            </td>
                                                            <td><strong>Activar usuario</strong></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>                                                                      
                             
                                </div>
                              	
                              
                     
	                    </div>
	                    
	                    
                       
	                </form>
    				
      								
  		        </div> 	
    
	        </div>
		
		
		    <!-- div data footer -->
		    <div id="footer">	
		       <div id="div_pie_municipalidad" 
                    style="width:70%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		            Municipalidad de
		                <strong> 
		                    <asp:Label ID="lblnombre_municipalidad" runat="server" Text="NOMBRE_MUNICIPIO"></asp:Label> 
                        </strong> 
                        
				    <br />
				    Teléfono: 
				        <strong> 
                            <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                        </strong> 
                     | Email: 
				        <strong> 
                            <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                        </strong> 
                    
			    </div>
    		   
		       <div id="div_pie_grupomdq" class="footer_grupomdq">
				    Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ S.A</a></strong>  
				    <br />
				    Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				    <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
                   <asp:Label ID="lblversion" runat="server" Text=""></asp:Label>
		       </div>
		    </div>	
		    
        </div>	



    </body>
    
</html>
