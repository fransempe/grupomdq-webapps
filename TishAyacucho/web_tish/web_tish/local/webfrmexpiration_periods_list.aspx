﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmexpiration_periods_list.aspx.vb" Inherits="web_tish.webfrmexpiration_periods_list" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
              
        
    <style type="text/css">
	* {
		margin: 0;
		padding: 0;
	}
	
		
	/* base semi-transparente */
    .overlay{
        display: none;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 720px;
        background: #000;
        z-index:1001;
		opacity:.75;
        -moz-opacity: 0.75;
        filter: alpha(opacity=75);
    }
	
    /* estilo para lo q este dentro de la ventana modal */
    .modal {
        display: none;
        position: absolute;
        /*
        top: 25%;
        left: 25%;
        width: 50%;
        height: 50%;
        */
        
        left: 50%;
        top: 50%;
        width: 500px;
        height: 240px;
        margin-top: -120px;
        margin-left: -250px;
        
        padding: 0px;
        background: #fff;
		color: #333;
        z-index:1002;
        overflow: auto;        
    }
    
    
    .modal_column_title{ width:50%; text-align:right;}
	.modal_column_data{ width:50%; text-align:left;}
	
    
    .modal_boton {
	    height: 30px;
	    width:90px;
	    background-color:#5b6951;
	    line-height: 35px;
    	
	    margin: 2px 5px 2px 5px;
	    color: #fff;
	    font-size: 1em;
	    font-weight:bold;
	    text-decoration: none;
	    cursor:pointer;
	
    }

    .modal_boton:hover  {
	    color:white;
	    background-color:Gray;	
    }
    
    
    .modal_div_header {
    	background-color:#5b6951;
    	color:White;
    	padding-top:5px;
    	padding-bottom:5px;
    }
    
    
    .modal_div_header_h2 {
    	font-family: 'Trebuchet MS', Arial, sans-serif;
	    font-weight: bold;
	    font-size: 1.3em;
	    text-transform: uppercase;
    }
    
    
    .modal_div_resource {    	
    	padding-top:20px;
    	width:80%;
    }
    
       

    
    
    </style>
        
        
        <!-- Seteo the resources -->
        <script src="../js/funciones.js" type="text/javascript"></script>       
        <link rel="stylesheet" href="../css/estilos_listado.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_botones.css" type="text/css" />        
        <link rel="stylesheet" href="../css/estilos_carteles.css" type="text/css" />
        <%           
            'Seteo el css que voy a utilizar
            Dim mPath_css As String                
            mPath_css = ""
            mPath_css = System.Configuration.ConfigurationManager.AppSettings("css").ToString.Trim()
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_listado.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_genericos.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_botones.css' type='text/css' />")            
        %> 
        
        
        
        
        
    
        <!-- recursos para crear la ventana modal -->
        <link rel="stylesheet" href="../modal/Assets/LightFace.css" />	
        <script src="../modal/mootools.js" type="text/javascript"></script>
        <script src="../modal/mootools-more-drag.js" type="text/javascript"></script>
        <script src="../modal/Source/LightFace.js" type="text/javascript"></script>
        <script src="../modal/Source/LightFace.IFrame.js" type="text/javascript"></script>
        <script src="../modal/Source/LightFace.Image.js" type="text/javascript"></script>
        <script src="../modal/Source/LightFace.Request.js" type="text/javascript"></script>
        <script src="../modal/Source/LightFace.Static.js" type="text/javascript"></script>
    	
    	    
        
        <script type="text/javascript">    	
 	        function modal_window(phtml) {
  		        box = new LightFace({ 
 			        title: 'Sistema SAE ::', 
			        width: 500,
			        height: 200,
  			        content: phtml,
 			        buttons: [ 			           
				        {
					        title: 'Cerrar',
					        event: function() { this.close(); fill_grid(); }
				        }
			        ]
 		        });
 		        box.open();		
 	        }
        
            function fill_grid() {                
                ajax_load_periods(true);
            }
            
            
           
        	function logout() {	 	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema SAE :: Cerrar sesión', 
				    width: 250,
				    height: 50,
	  			    content: '<div align="left">Usted esta a punto de cerrar su sesión.\n¿Desea continuar?</div>',
	 			    buttons: [		
	 			        {
						    title: 'Aceptar',
						    event: function() { this.close();window.location= "../webfrmindex.aspx";}
					    },
					    {
						    title: 'Cerrar',
						    event: function() { this.close();}
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }        
            </script>
           
         
  
  
        <!-- ajax -->       
        <script type="text/javascript" language="javascript" src="../js/jquery1-2-6-min.js"></script>
        <script type="text/javascript" language="javascript">
        
            /* Carga del logo de la municipalidad */
            function ajax_load_logo(){   
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_logo.ashx",                                                            
                    success: see_response_logo
                });
            }            
            
            function see_response_logo(html){                   
                if (html != '') {
                    var _control = document.getElementById('div_header');                   
                    _control.innerHTML = html;                   
                    return false;
                }
                
            return true;    
            }          
            
            
            
            
            /* Cargar periodos */
            function ajax_load_periods(p_popup){  
                var _index;
                var _year;                
                var _html;
                
                
                if (p_popup) {
                    close_popup();
                }
                
                 
            		          
            	_html = '<div id="loading" align="center">'+
            	            '<table border= "0">' +
	                            '<tr>' +
	                                '<td align="right" style="width:50%;"><img id="img_load" alt="" src="../imagenes/loading.gif" /></td>' +
	                                '<td align="left" style="width:50%;">Cargando datos ......</td>' +
	                            '</tr>' +
	                        '</table>' +
	                    '</div>';              		          
                window.document.getElementById('div_container_period_dinamic').innerHTML = _html;
                document.getElementById('div_add_periods').style.display = 'none';


                control_combo = window.document.getElementById('cbperiodos');
                _index = control_combo.selectedIndex;
                _year = control_combo.options[_index].value;
                
                               
                 
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_load_periods.ashx",                                                            
                    data:"year=" + _year,
                    success: see_response_load_periods
                });
            }            
            
            function see_response_load_periods(html){                   
                if (html != '') {
                    window.document.getElementById('div_container_period_dinamic').innerHTML = html;                         
                    document.getElementById('div_add_periods').style.display = 'block';                          
                    return false;
                }
                
            return true;    
            }          



            /* Creo el popup para agregar periodos */
            function period_add(){                                                            
                var _index;
                var _year;

                control_combo = window.document.getElementById('cbperiodos');
                _index = control_combo.selectedIndex;
                _year = control_combo.options[_index].value;
            
                window.document.getElementById('light').style.display='block';
                window.document.getElementById('fade').style.display='block';            
                window.document.getElementById('div_container_period_add').style.display='block';                            
                                
                window.document.getElementById('txtyear').value = _year;
                window.document.getElementById('txtyear').focus();
                return true;    
            }          
            
            
            
            /* Carga modal */
	        function ajax_period_add() {   
		        var _year;
		        var _quota;
		        var _date_presentation;
		        var _date_quota1;
		        var _date_quota2;


                if (validate_date_add()) {

		            /* Asigno los valores cargados */
		            _year = window.document.getElementById('txtyear').value;
		            _quota = window.document.getElementById('txtquota').value;
		            _date_quota1 = window.document.getElementById('txtdate_quota1').value;
//		            _date_quota2 = window.document.getElementById('txtdate_quota2').value;
		            _date_presentation = window.document.getElementById('txtdate_presentation').value;


		            jQuery.ajax({
			            type:"POST", 
			            url:"../controladores/handler_add_period.ashx",
			            data: "action=save&year=" + _year + "&" + "quota=" + _quota + "&" + "date_quota1=" + _date_quota1 + "&" +"date_quota2=" + _date_quota2 + "&" + "date_presentation=" + _date_presentation,
			            success: see_response_period_add
		            });
		        }
	        }
		
	        function see_response_period_add(html){                   
        		var _message;
        		        		       	
        	        		        		       		
        		_message = 'El período se ha agregado exitosamente';
        		if (html != 'OK') {
        		    _message = 'El período no se ha podido agregar';
        		}
        		
		        
	            html = '<div class="modal_div_header">' + 
		                    '<strong class="modal_div_header_h2">Nuevo período de vencimiento</strong>' + 
	                   '</div>' +	
	                   '<table id="table" border="0" style="width: 60%; background-color: #FFFFFF; color: #000000;" align="center">' +
		                   '<tr>' +
				                '<td align="center">' +
					                '<div><h3>' + _message + '</h3></div>' +
				                '</td>' +
			                '</tr>' +                  				
			                '<tr>' +
				                '<td align="center">' +
					                '<a id="link_close_popup_add" href="#" onClick="javascript:ajax_load_periods(true);">Cerrar ventana</a>' +
				                '</td>' +
			                '</tr>' +
		                '</table>'
		        
	            window.document.getElementById('div_container_period_add').style.display = 'none';
	            window.document.getElementById('div_container_period_message').style.display = 'block';		            
		        window.document.getElementById('div_container_period_message').innerHTML = html;                               
		        window.document.getElementById('link_close_popup_add').focus();
		        
	        return true;    
	        }          
            
            
            
            
            
            /*  ----------- EDICION -------------*/
            /* Creo el popup para agregar periodos */
            function period_edit(p_year, p_quota, p_date_quota1, p_date_quota2, p_date_presentation){                           
                                                     
                window.document.getElementById('lbledit_year').innerHTML = p_year;
                window.document.getElementById('lbledit_year').style.textAlign = 'center';                
                window.document.getElementById('lbledit_quota').innerHTML = p_quota;
                window.document.getElementById('lbledit_quota').style.textAlign = 'center';
                window.document.getElementById('txtedit_date_quota1').value = p_date_quota1;
                /*window.document.getElementById('txtedit_date_quota2').value = p_date_quota2;*/
                window.document.getElementById('txtedit_date_presentation').value = p_date_presentation;
                
                                
                                                 
                window.document.getElementById('light').style.display='block';
                window.document.getElementById('fade').style.display='block';            
                window.document.getElementById('div_container_period_edit').style.display='block';
                window.document.getElementById('txtedit_date_presentation').focus();
                window.document.getElementById('txtedit_date_presentation').select();
                return true;    
            }          



            /* Creo el popup para editar un periodo */
            function ajax_period_edit(){   
                var _year;
		        var _quota;
		        var _date_presentation;
		        var _date_quota1;
		        var _date_quota2;


		        /* Asigno los valores cargados */
		        _year = window.document.getElementById('lbledit_year').innerHTML;
		        _quota = window.document.getElementById('lbledit_quota').innerHTML;
		        _date_quota1 = window.document.getElementById('txtedit_date_quota1').value;
		        /*_date_quota1 = window.document.getElementById('txtedit_date_quota1').value;*/
		        _date_presentation = window.document.getElementById('txtedit_date_presentation').value;
            
            
                        
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_period_edit.ashx",
                    data: "action=save&year=" + _year + "&" + "quota=" + _quota + "&" + "date_quota1=" + _date_quota1 + "&" + "date_quota2=" + _date_quota2 + "&" + "date_presentation=" + _date_presentation,
                    success: see_response_period_edit
                });
            }
            
            
            function see_response_period_edit(html){                                   
                var _message;
        		
        		_message = 'El período se ha modificado exitosamente';
        		if (html != 'OK') {
        		    _message = 'El período seleccionado no se ha podido modificar';
        		}
        		

	            html = '<div class="modal_div_header">' + 
		                    '<strong class="modal_div_header_h2">Edición de períodos de vencimiento</strong>' + 
	                   '</div>' +		            
	                   '<table id="table" border="0" style="width: 60%; background-color: #FFFFFF; color: #000000; padding-top:40px;" align="center">' +
		                   '<tr>' +
				                '<td align="center">' +
					                '<div><h3>' + _message + '</h3></div>' +
				                '</td>' +
			                '</tr>' +                  				
			                '<tr>' +
				                '<td align="center">' +
					                '<a id="link_close_popup_edit" href="#" onClick="javascript:ajax_load_periods(true);">Cerrar ventana</a>' +
				                '</td>' +
			                '</tr>' +
		                '</table>'
		        
		        
	            window.document.getElementById('div_container_period_edit').style.display = 'none';
	            window.document.getElementById('div_container_period_message').style.display = 'block';		            
		        window.document.getElementById('div_container_period_message').innerHTML = html;                               
		        window.document.getElementById('link_close_popup_edit').focus();
			     	
	        return true;  
	        }  



            /* Creo el popup para eliminar  */
            function period_delete(p_year, p_quota){               
            	box = new LightFace({ 
	 			title: 'Eliminar período', 
				width: 250,
				height: 50,
	  			content: 'La eliminación de períodos de vencimiento se encuentra en desarrollo.',
	 			buttons: [
			        {
						title: 'Aceptar',
						event: function() { this.close(); }
					}
				]
	 		});
	 		box.open();		
          
            }
        </script>
        
        
        <!--funciones de validacion de datos -->
        <script type="text/javascript" language="javascript">
            function validate_date_add() {
                                    
                if (control_vacio(window.document.getElementById('txtyear'),'Año:\nDebe ingresar un año')) {
                    return false;
                }
                
                if (control_vacio(window.document.getElementById('txtquota'),'Cuota:\nDebe ingresar una cuota')) {
                    return false;
                }
                
                if (control_vacio(window.document.getElementById('txtdate_presentation'),'Fecha Vto. de presentación:\nDebe ingresar una fecha')) {                    
                    return false;
                }    
                
                if (control_vacio(window.document.getElementById('txtdate_quota'),'Fecha Vto. de cuota:\nDebe ingresar una fecha')) {                    
                    return false;
                }
                
            return true
            }            
     
        </script>
        
   
        
        <title>Sistema SAE :: Listado de períodos de vencimiento</title>
        <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
    
        
        <!-- Controls Mask -->
        <script type="text/javascript" src="../js/jquery.js"></script>
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" language="javascript"  src="../js/jquery.maskedinput.js"></script>
        <script type="text/javascript">    
        
        
        jQuery(function($){
            $("#txtyear").mask("9999"); 
            $("#txtquota").mask("99");
            $("#txtdate_quota1").mask("99/99/9999");
            $("#txtdate_quota2").mask("99/99/9999"); 
		    $("#txtdate_presentation").mask("99/99/9999");


		    $("#txtedit_date_quota1").mask("99/99/9999");
		    $("#txtedit_date_quota2").mask("99/99/9999"); 
		    $("#txtedit_date_presentation").mask("99/99/9999"); 		    
	    });
        

     
 
 
 
    function loading_key(val, ev){
        if (window.event) {
            var key = window.event.keyCode;
        } else {
            var key = ev.which;
        }
        
     
        if (key == 13 || key == 9){    
           return loading(); 
        }
    }
 
 

    function load_periods() {
        window.document.getElementById('img_loading').style.display = 'block';
        window.document.getElementById('btnload').style.display = 'none';
        
        __doPostBack("load_manager", '');
        return true;
    }
   
   
   
    function close_popup() {
		window.document.getElementById('light').style.display='none';
		window.document.getElementById('fade').style.display='none';	
		window.document.getElementById('div_container_period_message').style.display='none';	
		
		if (window.document.getElementById('div_container_period_add').style.display = 'block') {
		    window.document.getElementById('txtyear').value = '';	
		    window.document.getElementById('txtquota').value = '';
		    window.document.getElementById('txtdate_quota1').value = '';
		    /*window.document.getElementById('txtdate_quota2').value = '';	*/
		    window.document.getElementById('txtdate_presentation').value = '';	
		    window.document.getElementById('div_container_period_add').style.display = 'none';	
		}
		
		
        if (window.document.getElementById('div_container_period_edit').style.display = 'block') {
		    window.document.getElementById('lbledit_year').innerHTML = '';	
		    window.document.getElementById('lbledit_quota').innerHTML = '';
		    window.document.getElementById('txtedit_date_quota1').value = '';
		   /* window.document.getElementById('txtedit_date_quota2').value = '';	*/
		    window.document.getElementById('txtedit_date_presentation').value = '';			    
		    window.document.getElementById('div_container_period_edit').style.display = 'none';	
		}
	}
	
   
      

    function key_up_txt(e, p_control) {                    
        pressed_key = (document.all) ? e.keyCode : e.which;
        if (pressed_key == 13){
            document.getElementById(p_control).focus();    
        }               
    }
        
     

</script>
        
        
    </head>

    <body onload="javascript:ajax_load_periods(false);">
    
        
        <!-- wrap starts here -->	
        <div id="wrap">
                     
		    <div id="header">
		          
		        <!-- div container font -->
                <div id="container_font">			        
		        <div class="div_font" style="padding:0px 5px 0px 5px;">
                    <a href="#" title="Usar fuente menor" class="link_font" onclick="assign_font('-');">A-</a> 
                </div>
                
                <div class="div_font">
		            <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 
                </div>		            
                
		        <div class="div_font">
		            <a href="#" title="Usar fuente mayor" class="link_font" onclick="assign_font('+');">A+</a> 
		        </div>	            
            </div>
                    
		    
	        <div id="div_header"  class="div_image_logo">                
 	            <img  class="image_logo" alt="" src="../imagenes/topiz.jpg"  />
            </div>   
		                
			<h1 id="logo-text">Tasa por Servicios a la Actividad Económica</h1>			
			<h2 id="slogan"><asp:Label ID="lblslogan_municipalidad" runat="server" Text=""></asp:Label></h2>	
						
			
		</div>
	  
            <!-- content-wrap starts here -->
            <div id="content-wrap">
            
                <!-- div container page title -->
                <div id="div_title" style="background-color:Black;" align ="left" >
                    <strong class="titulo_page">Sistema SAE :: Listado de períodos de vencimiento</strong>
                </div>
    	  
    	  
                <!-- div container right -->
                <div id="div_optiones" class="menu_options">
                    
                    <div id="div_comercio" style=" padding-top:20px;">
                            <div align="center">
                                <table  style="width:95%">
                                    <tr>
                                        <td  rowspan="2"><img id="user" alt="" src="../imagenes/user.png"  style=" width:60px; height:60px; border:none" onclick="return user_onclick()" /></td>
                                        <td><p  class="titulo_usuario"align ="left" style="color:Black; height: 25px;">Bienvenido</p></td>
                                    </tr>
                                    <tr>                                
                                        <td>
                                            Usuario: <asp:Label ID="lblnombre_usuario" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        
                    <h1 align ="left" style="color:Black">Opciones</h1>                    
                    <ul class="sidemenu">                                       	                    
                        <li><a href="webfrmopciones_rafam.aspx" >Volver a opciones</a></li>	                					
                        <li><a href="#" onclick="javascript:logout();">Cerrar sesión</a></li>	                					
                    </ul>	
                </div>
    	    
    	  
    	  
  		        <div id="main"> 
    			
                    
                    <form id="formulario" runat="server">
                        
                        
                        <div id="div_container" align="center" style="padding-bottom:30px; height:100%;" runat="server">
                    
            
    		                <!-- message for the user -->
                            <div id="div_messenger" class="Globo GlbRed" style="width:70%;" runat="server" visible="false">
                                <asp:Label ID="lblmessenger" runat="server" Text="" Visible="False"></asp:Label>
                            </div>
                               
                    
                            <!-- div data manager-->
                            <div id="div_selected_periods" align="center" style="width:100%; padding-bottom:0px;" runat="server">
                                <table id="table_data_manager" border="0" style="width: 60%; background-color: #FFFFFF; color: #000000;" align="center"> 
                                    <tr>
                                        <td class="columna_titulo_right" style="width:50%;">Seleccione un período:</td>
                                        <td class="columna_dato_left" style="width:30%;" align="center">                                        
                                            <asp:DropDownList ID="cbperiodos" Width="100%" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td id="td_load" align="left">
                                            <img id="img_loading" alt="" src="../imagenes/loading.gif"  style="display:none;"/>                                         
                                            <img id="btnload" alt="" src="../imagenes/load.gif" class="boton_load" onclick="javascript:return ajax_load_periods(false);" />                                         
                                        </td>                                                                                         
                                    </tr>                                 
                                
                                </table>   
                        
                            </div>  
                            <hr class="linea"/>
                    
                    
                       
                        
                        
                            <div id="div_container_periods" align="center" style="padding-bottom:0px; height:400px;">                		     
                                <div id="div_container_period_dinamic">
            		            
	                            </div>
	                            
	                            
	                            <!-- message for the user -->
                                <div id="div_add_periods" style="display:none;">
                                    <a href="#" class="" onclick="javascript:period_add();"> <strong>+</strong> Agregar nuevo período</a>
                                    <br />
                                </div>
	                        </div>
	                        
                      

	                    
	                    </div>
	                    
	                    
                
	                    
	                    
                       
	                </form>
    				
      								
  		        </div> 	
    
	        </div>
		
		
		    <!-- div data footer -->
		    
		    <div id="footer">	
		       <div id="div_pie_municipalidad" 
                    style="width:70%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		            Municipalidad de
		                <strong> 
		                    <asp:Label ID="lblnombre_municipalidad" runat="server" Text="NOMBRE_MUNICIPIO"></asp:Label> 
                        </strong> 
                        
				    <br />
				    Teléfono: 
				        <strong> 
                            <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                        </strong> 
                     | Email: 
				        <strong> 
                            <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                        </strong> 
                    
			    </div>
    		   
		       <div id="div_pie_grupomdq"  class="footer_grupomdq">
				    Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ S.A</a></strong>  
				    <br />
				    Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				    <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
                   <asp:Label ID="lblversion" runat="server" Text=""></asp:Label>
		       </div>
		    </div>	
		    
        </div>	




    <!-- base semi-transparente -->
    <div id="fade" class="overlay" ></div>
    <!-- fin base semi-transparente -->

 
    <!-- ventana modal -->  
	<div id="light" class="modal">
    	<div id="div_container_popup" style="padding-top:0px; width:100%;" align="center" >
    	
    	    <div id="div_container_period_message"  style="display:none;">
    	    
    	    </div>
    	
    	    <div id="div_container_period_add" style="display:none;">
    	    
    	         <div class="modal_div_header">	
			        <strong class="modal_div_header_h2">Nuevo período de vencimiento</strong>
		        </div>				   
    	    
    	    	        
		        <div class="modal_div_resource" align="center">	
			        <strong>
			            Recurso involucrado: <asp:Label ID="lbldescription_resource_tish_add" runat="server" Text=""></asp:Label>
			        </strong>
			        <hr class="linea"/>
		        </div>							   
					   
					       
		        <table id="table" border="0" style="width: 50%; background-color: #FFFFFF; color: #000000;" align="center">
			
				    <tr>
					    <td class="modal_column_title" align="right">A&ntilde;o:</td>
					    <td class="modal_column_data">
						    <input id ="txtyear" type="text" class="textbox_onblur" maxlength="4" 
							    onblur="javascript:event_focus(this);" 
							    onfocus="javascript:event_focus(this);" 
	   					        onkeyup="javascript:key_up_txt(event, 'txtquota');" />
					    </td>
				    </tr>
				    <tr>
					    <td class="modal_column_title" align="right">Cuota:</td>
					    <td class="modal_column_data">
						    <input id ="txtquota" type="text"  class="textbox_onblur"  maxlength="2"
							    onblur="javascript:event_focus(this);" 
							    onfocus="javascript:event_focus(this);"
							    onkeyup="javascript:key_up_txt(event, 'txtdate_presentation');" />
					    </td>
				    </tr>
                    <tr>
					    <td class="modal_column_title" align="right">Fecha presentación:</td>
					    <td class="modal_column_data">
						    <input id ="txtdate_presentation" type="text" class="textbox_onblur" 
							    onblur="javascript:event_focus(this);" 
							    onfocus="javascript:event_focus(this);"
							    onkeyup="javascript:key_up_txt(event, 'txtdate_quota1');" />
					    </td>
				    </tr>
                    <tr>
					    <td class="modal_column_title" align="right">Fecha Vto. Pago:</td>
					    <td class="modal_column_data">
						    <input id ="txtdate_quota1" type="text" class="textbox_onblur" 
							    onblur="javascript:event_focus(this);" 
							    onfocus="javascript:event_focus(this);"
							    onkeyup="javascript:key_up_txt(event, 'txtdate_quota2');" />
					    </td>
				    </tr>
                   <%-- <tr>
					    <td class="modal_column_title" align="right">Fecha Vto. 2 cuota:</td>
					    <td class="modal_column_data">
						    <input id ="txtdate_quota2" type="text" class="textbox_onblur" 
							    onblur="javascript:event_focus(this);" 
							    onfocus="javascript:event_focus(this);"
							    onkeyup="javascript:key_up_txt(event, 'btnsave');" />
					    </td>
				    </tr>--%>
				    <tr>
					    <td  colspan="2" align="center">
						    <div style="width:100%" align="center">
							    <input id ="btnsave" type="button" value="Agregar"   class="modal_boton" onclick="javascript:ajax_period_add();" />
							    <input id ="btncancel" type="button" value="Cancelar"   class="modal_boton" onclick="javascript:close_popup();" />
						    </div>                
					    </td>
				    </tr> 
			    </table>
			</div>
			    
			    
			    
			
			<div id="div_container_period_edit"  style="display:none;">
			
			    <div class="modal_div_header">	
			        <strong class="modal_div_header_h2">Edición de períodos de vencimiento</strong>
		        </div>					   
			
    	        <div class="modal_div_resource" align="center">	
			        <strong>
			            Recurso involucrado: <asp:Label ID="lbldescription_resource_tish_edit" runat="server" Text=""></asp:Label>
			        </strong>
			        <hr class="linea"/>
		        </div>		
		        			   
					   
					       
		        <table id="table_period_edit" border="0" style="width: 50%; background-color: #FFFFFF; color: #000000;" align="center">
			
				    <tr>
					    <td class="modal_column_title" align="right">A&ntilde;o:</td>
					    <td id="lbledit_year" class="modal_column_data"></td>
				    </tr>
				    <tr>
					    <td class="modal_column_title" align="right">Cuota:</td>
					    <td id = "lbledit_quota" class="modal_column_data"></td>
				    </tr>               
				    <tr>
					    <td class="modal_column_title" align="right">Fecha presentación:</td>
					    <td class="modal_column_data">
						    <input id ="txtedit_date_presentation" type="text" class="textbox_onblur" 
							    onblur="javascript:event_focus(this);" 
							    onfocus="javascript:event_focus(this);"
							    onkeyup="javascript:key_up_txt(event, 'txtedit_date_quota1');" />
					    </td>
				    </tr>
				    <tr>
					    <td class="modal_column_title" align="right">Fecha Vto. Pago:</td>
					    <td class="modal_column_data">
						    <input id ="txtedit_date_quota1" type="text" class="textbox_onblur" 
							    onblur="javascript:event_focus(this);" 
							    onfocus="javascript:event_focus(this);"
							    onkeyup="javascript:key_up_txt(event, 'txtedit_date_quota2');" />
					    </td>
				    </tr>
                   <%-- <tr>
					    <td class="modal_column_title" align="right">Fecha Vto. 2 cuota:</td>
					    <td class="modal_column_data">
						    <input id ="txtedit_date_quota2" type="text" class="textbox_onblur" 
							    onblur="javascript:event_focus(this);" 
							    onfocus="javascript:event_focus(this);"
							    onkeyup="javascript:key_up_txt(event, 'btnedit_save');" />
					    </td>
				    </tr>--%>
				    <tr>
					    <td  colspan="2" align="center">
						    <div style="width:100%" align="center">
							    <input id ="btnedit_save" type="button" value="Aceptar"   class="modal_boton" onclick="javascript:ajax_period_edit();" />
							    <input id ="btnedit_cancel" type="button" value="Cancelar"   class="modal_boton" onclick="javascript:close_popup();" />
						    </div>                
					    </td>
				    </tr> 
			    </table>
			</div>
			    
			    
    	</div>
    </div>
    <!-- fin ventana modal -->



    </body>
</html>
