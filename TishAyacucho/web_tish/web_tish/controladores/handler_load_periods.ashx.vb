﻿Option Explicit On
Option Strict On

Imports System.Web
Imports System.Web.Services


Public Class handler_load_periods
    Implements System.Web.IHttpHandler

#Region "Variables"
    Private mWS As ws_tish.Service1
#End Region


    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim mYear As String
        
        Dim mHTML As String
        Dim mResponse As String


        Try

            context.Response.ContentType = "text/plain"


            'I obtain the variables
            mYear = context.Request.Form("year")
            


            mHTML = ""
            mHTML = Me.CreateHTML(CInt(mYear))


            'create the response
            mResponse = ""
            mResponse = mHTML.ToString.Trim


        Catch ex As Exception
            mResponse = "<span id='lblmessenger' style='color: Red; font-weight: bold;'>" & _
                            "Error al intentar recuperar los datos." & _
                        "</span>"
        End Try

        context.Response.Write(mResponse.ToString.Trim)
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Private Function CreateHTML(ByVal pYear As Integer) As String
        Dim dsData As DataSet
        Dim mHTML_Table_Header As String
        Dim mHTML_Table_Body As String
        Dim mHTML As String


        Try

            mHTML = ""



            'Cabecera de la tabla
            mHTML_Table_Header = "<thead>" & _
                                        "<tr>" & _
                                            "<th><h6>Recurso</h6></th>" & _
                                            "<th><h6>Año</h6></th>" & _
                                            "<th><h6>Cuota</h6></th>" & _
                                            "<th><h6>Fecha Vto. present.</h6></th>" & _
                                            "<th><h6>Fecha Vto. Pago</h6></th>" & _
                                            "<th><h6>Editar</h6></th>" & _
                                            "<th><h6>Eliminar</h6></th>" & _
                                        "</tr>" & _
                                     "</thead>"
            '"<th><h6>Fecha Vto. 2 cuota</h6></th>" & _


            'I obtain the data of the trade
            dsData = Me.ObtainList_DateExpiry(pYear)


            'I assign the data        
            If (clsTools.HasData(dsData)) Then

                'Creo los renglones de la tabla
                mHTML_Table_Body = ""
                For i = 0 To dsData.Tables(0).Rows.Count - 1
                    mHTML_Table_Body = mHTML_Table_Body & "<tr>" & _
                                                            "<td align='center'>" & dsData.Tables(0).Rows(i).Item("RECURSO").ToString.Trim & "</td>" & _
                                                            "<td align='center'>" & dsData.Tables(0).Rows(i).Item("ANIO").ToString.Trim & "</td>" & _
                                                            "<td align='center'>" & dsData.Tables(0).Rows(i).Item("CUOTA").ToString.Trim & "</td>" & _
                                                            "<td align='center'>" & dsData.Tables(0).Rows(i).Item("FV_P_DDJJ").ToString.Trim & "</td>" & _
                                                            "<td align='center'>" & dsData.Tables(0).Rows(i).Item("FV_CUOTA1").ToString.Trim & "</td>" & _
                                                            "<td align='center'> " & _
                                                                "<img src='../imagenes/editar.png' alt=''  title='Editar período' style='cursor:pointer;' onclick='javascript:period_edit(" & dsData.Tables(0).Rows(i).Item("ANIO").ToString.Trim & ", " & dsData.Tables(0).Rows(i).Item("CUOTA").ToString.Trim & ", """ & dsData.Tables(0).Rows(i).Item("FV_CUOTA1").ToString.Trim & """, """ & dsData.Tables(0).Rows(i).Item("FV_CUOTA2").ToString.Trim & """ , """ & dsData.Tables(0).Rows(i).Item("FV_P_DDJJ").ToString.Trim & """);'/>" & _
                                                            "</td>" & _
                                                            "<td align='center'> " & _
                                                                "<img src='../imagenes/delete.gif' alt=''  title='Eliminar período' style='cursor:pointer;' onclick='javascript:period_delete(" & dsData.Tables(0).Rows(i).Item("ANIO").ToString.Trim & ", " & dsData.Tables(0).Rows(i).Item("CUOTA").ToString.Trim & ");'/>" & _
                                                            "</td>" & _
                                                            "</tr>" & _
                                                          "<tr>"
                    '"<td align='center'>" & dsData.Tables(0).Rows(i).Item("FV_CUOTA2").ToString.Trim & "</td>" & _
                Next


            Else
                mHTML_Table_Body = "<tbody>" & _
                                        "<tr>" & _
                                            "<td></td>" & _
                                            "<td></td>" & _
                                            "<td></td>" & _
                                            "<td></td>" & _
                                            "<td></td>" & _
                                            "<td></td>" & _
                                            "<td></td>" & _
                                            "</tr>" & _
                                       "<tr>" & _
                                    "</tbody>"
                '"<td></td>" & _
            End If




            mHTML = "<table cellpadding='0' cellspacing='0' border='0' id='table_periods' class='table_periods'>" & _
                        mHTML_Table_Header.Trim & vbCr & _
                        "<tbody>" & mHTML_Table_Body.Trim & "</tbody>" & _
                    "</table>"


        Catch ex As Exception
            mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                        "<p id='lblmessenger' style='color: Red; font-weight: inherit;'> " & _
                            "Error al intentar obtener los datos." & _
                        "</p>" & _
                    "</div>"
        End Try


        Return mHTML.ToString.Trim
    End Function

    Private Function ObtainList_DateExpiry(ByVal pYear As Integer) As DataSet
        Dim mXMLData As String
        Dim dsData As DataSet

        Try

            'I obtain the data of the trade
            Me.mWS = New ws_tish.Service1
            mXMLData = Me.mWS.ObtainList_DateExpiry(pYear)
            Me.mWS = Nothing


            dsData = New DataSet
            dsData = clsTools.ObtainDataXML(mXMLData.Trim)

        Catch ex As Exception
            dsData = Nothing
        End Try

        Return dsData
    End Function

End Class