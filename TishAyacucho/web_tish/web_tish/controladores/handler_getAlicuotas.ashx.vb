﻿Imports System.Web
Imports System.Web.Services

Public Class handler_getAlicuotas
    Implements System.Web.IHttpHandler

#Region "Variables"
    Private mWS As ws_tish.Service1
#End Region



    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim strNroComercio As String = ""
        Dim strPeriodo As String = ""
        Dim strResponse As String = ""

        Try
            context.Response.ContentType = "text/plain"

            strNroComercio = CLng(context.Request.Form("comercio"))
            strPeriodo = context.Request.Form("period")

            strResponse = Me.getAlicuotas(strNroComercio.Trim, strPeriodo.Trim)
        Catch ex As Exception
            strResponse = ""
        End Try

        context.Response.Write(strResponse.Trim)
    End Sub

    Private Function getAlicuotas(ByVal p_strComercio As String, ByVal strPeriodo As String) As String
        Dim rubro As New List(Of entidades.rubro)
        Dim strXML As String = ""
        Dim strAlicuotas As String = ""


        Try
            Me.mWS = New ws_tish.Service1
            strXML = Me.mWS.getRubrosByComercio(p_strComercio)
            Me.mWS = Nothing


            rubro = entidades.serializacion.deserializarXML(Of List(Of entidades.rubro))(strXML.Trim)
            If (rubro IsNot Nothing) Then

                For Each rubro_aux As entidades.rubro In rubro
                    If strPeriodo = "2018" Then
                        strAlicuotas += rubro_aux.alicuota2018 & "|"
                    ElseIf strPeriodo = "2019" Then
                        strAlicuotas += rubro_aux.alicuota2019 & "|"
                    Else
                        strAlicuotas += rubro_aux.alicuotaActual & "|"
                    End If
                Next
            End If

        Catch ex As Exception
            strAlicuotas = ""
        End Try

        Return strAlicuotas.Trim
    End Function


    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class