﻿Option Explicit On
Option Strict On

Imports System.Web
Imports System.Web.Services

Public Class handler_new_manager
    Implements System.Web.IHttpHandler

#Region "Variables"
    Private mWS As ws_tish.Service1
#End Region

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim mAction As String
        Dim mNumberTrade As String
        Dim mHTML As String
        Dim mResponse As String


        Try

            context.Response.ContentType = "text/plain"

            'I obtain the variables
            mAction = context.Request.Form("action")
            mNumberTrade = context.Request.Form("number_trade")


            'check the login
            mHTML = ""
            Select Case mAction.ToString.Trim
                Case "load"
                    mHTML = BelieveHTMLTrade(mNumberTrade.ToString.Trim)
                Case "add"
                    'mHTML = BelieveHTMLManagerDown(mNumberTrade.ToString.Trim)
                
            End Select


            'believe the response
            mResponse = ""
            mResponse = mHTML.ToString.Trim


        Catch ex As Exception
            mResponse = "<span id='lblmessenger' style='color: Red; font-weight: bold;'>" & _
                            "Error al intentar recuperar los datos." & _
                        "</span>"
        End Try

        context.Response.Write(mResponse.ToString.Trim)
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property


    Private Function BelieveHTMLTrade(ByVal pNumberTrade As String) As String
        Dim dsDataTrade As DataSet
        Dim mReason_Social As String
        Dim mEmail As String
        Dim mHTML As String


        Try

            mHTML = ""


            'I obtain the data of the trade
            dsDataTrade = ObtainDataTrade(pNumberTrade)


            'I assign the data
            'lblmessenger.Visible = False
            If (clsTools.HasData(dsDataTrade)) Then

                If (dsDataTrade.Tables("Manager").Rows(0).Item("FECHA_BAJA").ToString.Trim <> "") Then


                    mHTML = "<p id='lblmessenger' style='color: Red; font-weight: inherit;'>" & _
                                "El Representante/Titular seleccionado se encuentra en estado de baja. </br>" & _
                            "</p>" & _
                            "<p id='lblmessenger1' style='color: black; font-weight: inherit;'>" & _
                                "Para realizar esta operación usted deberá primero dirigirse a la opción  <a href='webfrmactivar_representante.aspx'>[Activar Representante/Titular]</a>" & _
                            "</p>"
                Else
                    mReason_Social = dsDataTrade.Tables("Manager").Rows(0).Item("RAZON_SOCIAL").ToString.Trim

                    'mEmail = dsDataManager.Tables("Manager").Rows(0).Item("EMAIL").ToString.Trim

                    mHTML = "<strong>Datos del Representante/Titular:</strong> " & _
                            "<hr  style=' color:#009900;' width=100%' /> " & _
                            "<table border='0' style='border-bottom-color:Green; width:100%;' visible='false'; > " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>Razon social:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mReason_Social.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right' style='width:20%'>Razon Social:</td> " & _
                                   " <td class='columna_dato_left' style='width:80%' align='left' colspan='3'> " & _
                                        " <asp:Label ID='lblreason_social' runat='server' Text=''></asp:Label> " & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right' style='width:20%'>Contribuyente:</td> " & _
                                   " <td class='columna_dato_left' style='width:80%' align='left'  colspan='3'>" & _
                                        "HOLA contribuyente " & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                    " <td  colspan='3' style='width:20%'> " & _
                                        " <input id='Button1' type='button' value='button'  class='boton_verde'  style='width:80px;'/> " & _
                                    " </td> " & _
                               " </tr> " & _
                            " </table> "

                End If
            Else
                mHTML = "<p id='lblmessenger' style='color: Red; font-weight: inherit;'> " & _
                            "Representante / Titular inexistente." & _
                        "</p>"

            End If


        Catch ex As Exception
            mHTML = "<p id='lblmessenger' style='color: Red; font-weight: inherit;'> " & _
                        "Error al intentar obtener los datos." & _
                    "</p>"
        End Try


        Return mHTML.ToString.Trim
    End Function


    Private Function ObtainDataTrade(ByVal pNumberTrade As String) As DataSet
        Dim dsDataTrade_AUX As DataSet
        Dim mXML As String

        Try

            'I obtain the data of the trade
            mWS = New ws_tish.Service1
            mXML = mWS.ObtenerDatosComercio(pNumberTrade.ToString.Trim)
            mWS = Nothing


            'I believe the XML on the disc to read it
            dsDataTrade_AUX = New DataSet
            dsDataTrade_AUX = clsTools.ObtainDataXML(mXML.ToString.Trim)



        Catch ex As Exception
            dsDataTrade_AUX = Nothing
        End Try

        Return dsDataTrade_AUX
    End Function
End Class