﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general sobre un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie estos atributos para modificar la información
' asociada con un ensamblado.

' Revisar los valores de los atributos del ensamblado

<Assembly: AssemblyTitle("Interface_ws_tish_rafam")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Windows uE")> 
<Assembly: AssemblyProduct("Interface_ws_tish_rafam")> 
<Assembly: AssemblyCopyright("Copyright © Windows uE 2010")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'El siguiente GUID sirve como identificador de typelib si este proyecto se expone a COM
<Assembly: Guid("260c2ea8-e4f6-470d-a676-506f52130b4c")> 

' La información de versión de un ensamblado consta de los cuatro valores siguientes:
'
'      Versión principal
'      Versión secundaria 
'      Número de versión de compilación
'      Revisión
'
' Puede especificar todos los valores o establecer como predeterminados los números de versión de compilación y de revisión 
' mediante el asterisco ('*'), como se muestra a continuación:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
