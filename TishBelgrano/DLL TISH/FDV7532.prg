*************************************************
* FUNCTION FDV7532
*************************************************
* Autor : Rodrigo
* Dise�o: Pachu
* 
* Fecha :	31/03/95
* 
* Funcionamiento:	Calcula el digito verifiacdor segun algoritmo 7532
* 
* Par�metros:			NRO_FORMAT	(C)	:	Numero formateado
* 
* Modificaciones:
* 
PARAMETERS m.nro_format
=VerNuPar ('FDV7532', PARAMETERS()	, 1 )
=VerTiPar ('FDV7532', 'M.NRO_FORMAT','C')

PRIVATE digver, long_nro, posic, prox_num, digito
long_nro	=	LEN(m.nro_format)
posic			=	1
prox_num	=	7
digver		=	0

DO WHILE posic <= long_nro

	digito = (ASC(SUBSTR(m.nro_format, posic, 1)) - 48)

	IF digito > 0 AND digito <= 9

		digver = digver	+ prox_num * digito

		DO CASE
		CASE prox_num	=	7
			prox_num = 5
		CASE prox_num	=	5
			prox_num = 3
		CASE prox_num	=	3
			prox_num = 2
		CASE prox_num	=	2
			prox_num = 7
		ENDCASE

	ENDIF
	
	posic	=	posic	+	1

ENDDO
digver	=	digver % 10

RETURN digver
