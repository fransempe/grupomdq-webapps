	***********************************************************************************
	*	EmitirCompPagoNoVencGeneric
	***********************************************************************************
	PROCEDURE EmitirCompPagoNoVencGeneric(XMLGrupoNroComp as String) as String
		PRIVATE retorno
		
		AuxArchCompNoVenc = '<?xml version="1.0" encoding="UTF-8"?>' + _S_
		AuxArchCompNoVenc = AuxArchCompNoVenc + '<VFPDATA>' + _S_
		
		IF AmbienteOK
			EfimuniWeb()
		
			* BEGIN Apertura de tablas
			=UseT('recur\concs_cc')
			SELECT concs_cc
			SCATTER MEMVAR BLANK
			=UseT('recur\rec_cc')
			SELECT rec_cc
			SCATTER MEMVAR BLANK
			=UseT('recur\rec_fec')
			SELECT rec_fec
			SCATTER MEMVAR BLANK
			=UseT('recur\comprob')
			SELECT comprob
			SCATTER MEMVAR BLANK
			=UseT('recur\comp_ren')
			SELECT comp_ren
			SCATTER MEMVAR BLANK
			=UseT('recur\pla_imp')
			SELECT pla_imp
			SCATTER MEMVAR BLANK
			
			
			
			
			
			_EmitirCompPagoNoVenc (@AuxArchCompNoVenc, XMLGrupoNroComp, .T.)
			AgregarTelefonoYMail(@AuxArchCompNoVenc)
		ENDIF
		
		AgregarMensaje (@AuxArchCompNoVenc,'ERROR','MENSERROR')
		
		AgregarTiempo (@AuxArchCompNoVenc)
		
		AuxArchCompNoVenc = AuxArchCompNoVenc + '</VFPDATA>' + _S_

		RETURN STRCONV(AuxArchCompNoVenc,_STRCONV_)
	ENDPROC		
	***********************************************************************************