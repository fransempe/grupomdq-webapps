
* _Delete_Fec_Vto:
* Description:
PROCEDURE _Delete_Fec_Vto (pResource as String, pYear as Number, pQuota as Number) as String
	PRIVATE mResult as String
	PRIVATE mIndex as String


	* I open the table fec_vtos
	IF (!USED('fec_vtos')) THEN
		=UseT('recur\fec_vtos')
	ENDIF
	SELECT fec_vtos
	SCATTER MEMVAR BLANK


	mIndex = ALLTRIM(pResource) + STR(pYear, 4, 0) + STR(pQuota, 3, 0)
	SET ORDER TO PRIMARIO	
	IF (SEEK(mIndex)) THEN
		IF (ALIAS() = 'fec_vtos') THEN		
			DELETE
			mResult = 'OK'
		ELSE
			mResult = 'Alias incorrecto para realizar la eliminacion'
		ENDIF
	ELSE
		mResult = 'Per�odo no encontrado'
	ENDIF
	 
	 
	RETURN ALLTRIM(mResult)
ENDPROC