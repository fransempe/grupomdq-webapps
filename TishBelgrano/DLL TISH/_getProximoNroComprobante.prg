
**************************************************************************************
*	M�todo _getProximoNroComprobante : 
*	Este m�todo devuelve el pr�ximo n�mero de comprobante junto el su respectivo
*   d�gito verificador
**************************************************************************************
PROCEDURE _getProximoNroComprobante (p_numGrupoComprobante as Number, ;
									 p_strTipoImponible as String, p_numNroImponible as Number, ;
									 numProximoNumero as Number, numDigitoVerificador as Number)
	
	IF (!USED('numerad')) THEN
		=UseT('recur\numerad')
	ENDIF
	SELECT numerad

	IF (!USED('comprob')) THEN
		=UseT('recur\comprob')
	ENDIF
	SELECT comprob
								

	numProximoNumero = 0
	numDigitoVerificador = 0 				
	mAuxTipoInc = ''
	
	
	* Obtengo el pr�ximo n�mero de comprobante
	IF (NOT FPrxNum('COMPR', p_numGrupoComprobante, .T., .T., .F., .F., @numProximoNumero, @mAuxTipoInc)) THEN
		numProximoNumero = -1
	ENDIF

	* Obtengo el d�gito verificador
	IF (numProximoNumero <> -1) THEN								
		numDigitoVerificador = FDIGCOM(p_numGrupoComprobante, numProximoNumero, ALLTRIM(p_strTipoImponible), p_numNroImponible)				
	ENDIF			

		
ENDPROC