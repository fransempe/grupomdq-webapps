PROCEDURE _New_DDJJ (pRecurso as String, pCuit as String,;
					 pNumberComercio as Integer,;
					 pYear as Integer, pQuota as Integer,;
					 pDatos_XML as String) as String
					 
					 
	PRIVATE mResult as String
	PRIVATE mERROR as String
	PRIVATE mCountDDJJ as Number
	
	PRIVATE mNumberComercio as Integer
	
	PRIVATE mGroupVoucherTISH as Integer	
	PRIVATE datFechaVTO1 as DATE()	
	PRIVATE datFechaVTO2 as DATE()	
	PRIVATE datFechaVTOP as DATE()

	
	* Variables para usar el PRG externo
	PRIVATE mResult_XML as String
	PRIVATE mPath_Function as String
	PRIVATE mOld_Path_Function as String

	PRIVATE mIndex as String
		
	PRIVATE mXML as String
	
	PRIVATE boolContribuyenteAlDia as Boolean
	PRIVATE boolDesglosaDescuento as Boolean
	PRIVATE strConceptoImporteBruto as String
	PRIVATE strConceptoImporteDescuento as String
	PRIVATE dblImporteBruto as Double
	PRIVATE dblImporteDescuento as Double
	PRIVATE dblPorcentajeDescuento as Double

	PRIVATE numProximoNumero as Number
	PRIVATE numDigitoVerificador as Number
	
	PRIVATE numNroMovBruto as Number
	PRIVATe numNroMovDescuento as Number
	
	
	* Abro las tablas que voy a necesitar
	IF (!USED('comercio')) THEN
		=UseT('recur\comercio')
	ENDIF
	IF !USED ('com_ddjj')
		=UseT ('recur\com_ddjj')
	ENDIF
	IF !USED ('rubs_com')
		=UseT ('recur\rubs_com')
	ENDIF
	IF !USED ('rec_imp')
		=UseT ('recur\rec_imp')
	ENDIF
	IF !USED ('conc_imp')
		=UseT ('recur\conc_imp')
	ENDIF	
	IF !USED ('concsdev')
		=UseT ('recur\concsdev')
	ENDIF	
	IF !USED ('rec_cc')
		=UseT ('recur\rec_cc')
	ENDIF
	
					
	* Inicializo las variables
	mERROR 						= 'OK'
	boolContribuyenteAlDia  	= .F.
	boolDesglosaDescuento 		= .T.
	strConceptoImporteBruto 	= ''
	strConceptoImporteDescuento = ''
	dblImporteBruto 			= 0
	dblImporteDescuento 		= 0
	dblPorcentajeDescuento 		= 0
	mGroupVoucherTISH 			= 0
	numProximoNumero 			= 0
	numDigitoVerificador 		= 0
	numNroMovBruto 				= 0
	numNroMovDescuento 			= 0
	
	
	* Seteo los valores de par�metros de configuraci�n
	mGroupVoucherTISH 				= VAL(_getParametroConfig ('RECUR', 'GRUPO_COMP_TISH'))
	strConceptoImporteBruto  		= _getParametroConfig ('RECUR', 'WSH_CONC_BRUTO')
	strConceptoImporteDescuento  	= _getParametroConfig ('RECUR', 'WSH_CONC_DTO')
	dblPorcentajeDescuento  		= VAL(_getParametroConfig ('RECUR', 'WSH_DESCUENTO'))
	

	
	* Obtengo las fechas de vencimiento correspondiente a per�odo
	datFechaVTO1 = {}
	datFechaVTO2 = {}
	datFechaVTOP = {}
	_getFechasVencimiento(ALLTRIM(pRecurso), pYear, pQuota, @datFechaVTO1, @datFechaVTO2, @datFechaVTOP)	

					


	************** NUEVA DECLARACI�N JURADA ****************										
	* Agrego una nueva declaraci�n jurada
	Add_DDJJ(pDatos_XML)						 



	************** CALCULO IMPORTE BRUTO A PAGAR ****************										
	* Asigno estas variables p�blicas que van a ser usadas desde el prg fdevsh externo
	m.anio  = pYear
	m.cuota = pQuota							

	* Obtengo el path de los prg externos y lo seteo en FOX
	mPath_Function = "'" + ALLTRIM(_SISTEMA) + ALLTRIM(m.sistema) + "\" + ALLTRIM(m.modulo) + "\TISH\'"
	mOld_Path_Function = SET("Path")
	SET PATH TO &mPath_Function 
	
	* Obtengo el importe bruto a pagar
	dblImporteBruto = 0				
	pNameFunction = 'fdevsh'
	pNameFunction = ALLTRIM(pNameFunction) + '(' + STR(pNumberComercio) + ')'
	dblImporteBruto= &pNameFunction
							
	* Seteo el path de FOX como estaban antes
	SET PATH TO &mOld_Path_Function 


	
										
	************** DESCUENTO ****************										
	* Reviso si el contribuyente esta al d�a. 
	boolContribuyenteAlDia = .F.										
	boolContribuyenteAlDia = ImpAlDia(ALLTRIM(pRecurso), 'C', pNumberComercio)		
	
	
								
			
	* Si el contribuyente esta al d�a y existe un concepto de descuento, aplico el descuento
	boolDesglosaDescuento = .F.
	IF (boolContribuyenteAlDia) AND (!EMPTY(strConceptoImporteDescuento)) THEN
						
		* Calculo el descuento
		dblImporteDescuento = (dblImporteBruto * (dblPorcentajeDescuento/100)) 

		* Si los conceptos (WSH_CONC_BRUTO y WSH_CONC_DTO) son distintos significa que debo desglozar el importe en dos renglones.
		* En el caso de que sean iguales, significa que tengo que restarle el descuento al monto bruto y registrarlo en un �nico rengl�n
		IF (ALLTRIM(strConceptoImporteBruto) = ALLTRIM(strConceptoImporteDescuento)) THEN												
			boolDesglosaDescuento = .F.
			dblImporteBruto = dblImporteBruto - dblImporteDescuento					 
	 	ELSE
	 		boolDesglosaDescuento = .T.					 	
	 	ENDIF										
	ENDIF
																			
	 
	 
	************** OBTENGO N�MERO DE COMPROBANTE ****************										
	* Obtengo el n�mero y el d�gito verificador para el comprobante que voy a agregar
	_getProximoNroComprobante (mGroupVoucherTISH, 'C', pNumberComercio, @numProximoNumero, @numDigitoVerificador)					

			
												
	************** CUENTA CORRIENTE ****************																							
	* Agrego uno o dos movimiento en la cuenta corriente adeudada y obtengo los n�meros movimientos 
	numNroMovBruto = 0
	numNroMovDescuento = 0
	IF (mERROR = 'OK') THEN								
		mERROR = Add_CTACTE (pRecurso, pNumberComercio, pYear, pQuota, ;
							 datFechaVTO1, boolDesglosaDescuento, ;
							 ALLTRIM(strConceptoImporteBruto), ALLTRIM(strConceptoImporteDescuento), ;									 
							 dblImporteBruto, dblImporteDescuento, ;									 
							 mGroupVoucherTISH, numProximoNumero, ;
							 @numNroMovBruto, @numNroMovDescuento)
	ENDIF
			
			
			
	************** COMPROBANTE ****************																	
	* Grabo el nuevo comprobante
	IF (mERROR = 'OK') THEN																			
		mERROR = Add_Voucher (mGroupVoucherTISH, numProximoNumero, numDigitoVerificador, pNumberComercio, ; 
							  datFechaVTO1, datFechaVTO2, ;
							  boolDesglosaDescuento, ;
							  ALLTRIM(strConceptoImporteBruto), ALLTRIM(strConceptoImporteDescuento), ;									 
							  dblImporteBruto, dblImporteDescuento, ;									 
							  numNroMovBruto, @numNroMovDescuento, ;									  
							  pCuit, pRecurso, pYear, pQuota)				
	ENDIF

	
 
	************** EMISI�N DEL COMPROBANTE ****************										
	* Genero un xml con el formato requerido para la emisi�n del comprobante
	mXML = '<VFPDATA>' + _S_ + ;
			   '<DETALLECOMPROBSELECCIONADOS>' + _S_ + ;
     		   	   '<TIPOIMPONIBLE></TIPOIMPONIBLE>' + _S_ + ;
	 			   '<NROIMPONIBLE></NROIMPONIBLE>' + _S_ + ;
				   '<GRUPOCOMP></GRUPOCOMP>' + _S_ + ;
				   '<NROCOMP></NROCOMP>' + _S_ + ;
		   	   '</DETALLECOMPROBSELECCIONADOS>' + _S_ + ;
			   '<DETALLECOMPROBSELECCIONADOS>' + _S_ + ;
	 		   	   '<TIPOIMPONIBLE>' + 'C' + '</TIPOIMPONIBLE>' + _S_ + ;
				   '<NROIMPONIBLE>' + ALLTRIM(STR(pNumberComercio)) + '</NROIMPONIBLE>' + _S_ + ;
				   '<GRUPOCOMP>' + ALLTRIM(STR(mGroupVoucherTISH)) + '</GRUPOCOMP>' + _S_ + ;
				   '<NROCOMP>' + ALLTRIM(STR(numProximoNumero)) + '</NROCOMP>' + _S_ + ;
			   '</DETALLECOMPROBSELECCIONADOS>' + _S_ + ;
		   '</VFPDATA>'


	mERROR = EmitirCompPagoNoVencGeneric(ALLTRIM(mXML))
	mResult = ALLTRIM(mERROR)
		
	RETURN ALLTRIM(mResult)	
ENDPROC






 



* ImporteRectificativa: Obtengo el total para la rectificativa
*******************************************************************************************************************************************************
FUNCTION ImporteRectificativa(pIndex as String, pAmount as Double) as Double
PRIVATE mTotal_REC_CC_AUX as Double
PRIVATE mTotal_REC_CCC_AUX as Double
PRIVATE mTotal_CTACTE as Double
PRIVATE mAmount_AUX as Double


	* Obtengo los totales de la cta. cte.
	mTotal_REC_CC_AUX  = TotalRECC(pIndex)
	mTotal_REC_CCC_AUX  = TotalRECCC(pIndex)


	
	mTotal_CTACTE = 0
	mTotal_CTACTE = mTotal_REC_CC_AUX - mTotal_REC_CCC_AUX 

	IF (mTotal_CTACTE < 0) THEN
		mTotal_CTACTE = (mTotal_CTACTE * -1)
	ENDIF



	mAmount_AUX = 0
	mAmount_AUX = pAmount - mTotal_CTACTE
	
	
	RETURN mAmount_AUX
ENDFUNC
*******************************************************************************************************************************************************
*******************************************************************************************************************************************************




* TotalRECC: Obtengo el total de la REC_CC	
******************************************************************************************************************************************************** 
FUNCTION TotalRECC(pIndex as String) as Double
PRIVATE mTotal_REC_CC as Double

	IF (!USED('rec_cc')) THEN
		=UseT('recur\rec_cc')
	ENDIF
	SELECT rec_cc
	SCATTER MEMVAR BLANK

	SET ORDER TO 1	
	SET KEY TO ALLTRIM(pIndex)
	GO TOP IN rec_cc

	mTotal_REC_CC = 0
	DO WHILE NOT EOF('rec_cc')		

		IF (rec_cc.Est_mov = 'NO') THEN
			mTotal_REC_CC = mTotal_REC_CC + VAL(rec_cc.Imp_mov)
		ENDIF 
		 
		SKIP IN rec_cc
	ENDDO
	SET KEY TO

	RETURN mTotal_REC_CC
ENDFUNC
*******************************************************************************************************************************************************
*******************************************************************************************************************************************************



* TotalRECCC: Obtengo el total de la REC_CCC	
*******************************************************************************************************************************************************
FUNCTION TotalRECCC(pIndex as String) as Double
PRIVATE mTotal_REC_CCC as Double


	IF (!USED('rec_ccc')) THEN
		=UseT('recur\rec_ccc')
	ENDIF
	SELECT rec_ccc
	SCATTER MEMVAR BLANK

	SET ORDER TO 1	
	SET KEY TO ALLTRIM(pIndex)
	GO TOP IN rec_ccc

	mTotal_REC_CCC = 0
	DO WHILE NOT EOF('rec_ccc')		

		IF (rec_ccc.Est_mov = 'NO') THEN
			mTotal_REC_CCC = mTotal_REC_CCC + VAL(rec_ccc.Imp_mov)
		ENDIF 
		 
		SKIP IN rec_ccc
	ENDDO
	SET KEY TO

	RETURN mTotal_REC_CCC
ENDFUNC
*******************************************************************************************************************************************************

