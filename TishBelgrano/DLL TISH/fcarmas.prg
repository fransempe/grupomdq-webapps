*************************************************
* FUNCTION FCarMas
*************************************************
* Autor  : Gabriel Zubieta
* Dise�o : Fernando
* Fecha  : 07/02/95
* 
* Funcionamiento:
* Carga en memoria las distintas mascaras para los diferentes 
* tipos de imponibles. En el programa que llama a esta fcion se deben 
* definir los arreglos correspondientes
* 
* 
* Par�metros:
* no tiene
* 
* Modificaciones:
* Gabriel 09/02/95 Agregue actualizaci�n de dig_imp
* Elio    14-02-95 Agregue actualizaci�n de diagrama
* Gabriel 15/03/95 Agregue 2 vectores, IDENT_EXP y UBIC_EXP 
*
PRIVATE retorno, m.elem, onerro_aux
DIMENSI masc_imp[26], dig_imp[26], tab_imp[26], ident_imp[26], desc_tab[26]
DIMENSI ident_exp[26], ubic_exp[26], indi_ide[26], indi_ubi[26]
* Las Lineas anteriores deben ser definidas en el programa que llama a esta fcion
EXTERNAL ARRAY masc_imp, dig_imp, tab_imp, ident_imp, desc_tab
EXTERNAL ARRAY ident_exp, ubic_exp, indi_ide, indi_ubi

retorno = .T.
=UseT ('recur\tips_imp')
STORE ' ' TO masc_imp, dig_imp, tab_imp, ident_imp, desc_tab, ident_exp, ubic_exp, indi_ide, indi_ubi

GO TOP IN tips_imp
DO WHILE !EOF('tips_imp')
	IF ( tips_imp.tipo_imp >= 'A' AND tips_imp.tipo_imp <= 'Z' )
		m.elem = ASC( tips_imp.tipo_imp ) - 64
		masc_imp [ m.elem ]  = ALLT( tips_imp.masc_timp )
		dig_imp  [ m.elem ]  =       tips_imp.usa_digver
		tab_imp  [ m.elem ]  = ALLT( tips_imp.tab_maes  )
		ident_imp[ m.elem ]  = ALLT( tips_imp.exp_nro   )
		desc_tab [ m.elem ]  =       tips_imp.desc_timp
		ident_exp[ m.elem ]  =       tips_imp.exp_ident
		ubic_exp [ m.elem ]  =       tips_imp.exp_ubic
		indi_ide [ m.elem ]  =			 tips_imp.ind_ident
		indi_ubi [ m.elem ]  =       tips_imp.ind_ubic
	ELSE
		=msg("El Tipo de Imponible " + tips_imp.tipo_imp + " no es v�lido ")
	ENDIF
	SKIP IN tips_imp
ENDDO

RETURN retorno

