
DEFINE CLASS tishweb as Session olepublic

	***********************************************************************************
	*	Constructor:
	*	Esta funcion setea parametros de FOX y lee el archivo tish.ini donde se
	*	se encuentran los path de recursos y de la base de datos
	***********************************************************************************
	PROCEDURE Init as Boolean
		PUBLIC AmbienteOK, TimeStart
		
		TimeStart = SECONDS()
		AmbienteOK = AmbienteWeb()
		
		RETURN .T.
	ENDPROC
	***********************************************************************************
	
	
	
	***********************************************************************************
	*	Crear todo:
	*	Cierra y borra la memoria 
	*	lee el archivo tish.ini donde se encuentran los path de recursos y de la base de datos
	***********************************************************************************
	PROCEDURE CrearTodo as Boolean
		BorrarTodo()
		
		AmbienteOK = AmbienteWeb()
		
		RETURN AmbienteOK
	ENDPROC
	***********************************************************************************
	
	
	
	***********************************************************************************
	*	ObtenerDatosContribuyente: 
	*	Esta funcion devuelve en base a un n�mero de contribuyente sus datos
	***********************************************************************************
	PROCEDURE Login (pCuit as String, pClave as String) as String
		PRIVATE mDatos as String
 
	 	* Seteo parametros de fox y leo donde estan los recursos tanto de sistema como de base de datos 
		IF AmbienteOK
			EfimuniWeb()
							
				 				
			* Abro las tablas que voy a utilizar
			IF (!USED('contrib')) THEN
				=UseT('recur\contrib')
			ENDIF
			SELECT contrib
		 	
		 	
		 	mDatos = 'antes'			
			IF (SEEK(pCuit, 'contrib')) THEN
				mDatos = ALLTRIM(contrib.nomb_cont)
			ENDIF
		ENDIF
		
		
		AuxArchDeud = mDatos 
		RETURN AuxArchDeud
		
	ENDPROC
	***********************************************************************************
	
	
	
	***********************************************************************************
	*	ObtenerDatosMunicipalidad: 
	*	Esta funcion devuelve los datos del municipio que van a ir al pie de la web
	***********************************************************************************
	PROCEDURE ObtenerDatosMunicipalidad() as String
		PRIVATE mNombre_Municipio as String
		PRIVATE mTelefono_Municipio as String
		PRIVATE mEmail_Municipio as String
		PRIVATE mIndice as String
		PRIVATE mResultado as String
		
 
	 	 
	 	 
	 	* Seteo parametros de fox y leo donde estan los recursos tanto de sistema como de base de datos 
	 	mResultados = ''
		IF (AmbienteOK)					 					 
			EfimuniWeb()
							
				 				
			* Abro las tablas que voy a utilizar
			IF (!USED('config')) THEN
				=UseT('asa\config')
			ENDIF
			SELECT config

			 		 	
		 	* Obtengo el Nombre del Municipio
		 	mNombre_Municipio = ''
		 	mIndice = PADR('ASA',8,' ') + 'PARTIDO'		 	
			IF (SEEK(mIndice, 'config')) THEN
				mNombre_Municipio = ALLTRIM(config.cont_par)
			ENDIF
		 	
		 	
		 	* Obtengo el Telefono del Municipio
			mTelefono_Municipio = ''			
		 	mIndice = PADR('ASA',8,' ') + 'NROTELRECWEB'
			IF (SEEK(mIndice, 'config')) THEN
				mTelefono_Municipio = ALLTRIM(config.cont_par)
			ENDIF


			* Obtengo el Email del Municipio
		 	mEmail_Municipio = ''	
		 	mIndice = PADR('ASA',8,' ') + 'DIRMAILRECWEB'
			IF (SEEK(mIndice, 'config')) THEN
				mEmail_Municipio = ALLTRIM(config.cont_par)
			ENDIF

		
			* Genero la cadena a devolver
			mResultado = mNombre_Municipio + '|' +  mTelefono_Municipio + '|' + mEmail_Municipio 
		ENDIF
		
		
		 
		RETURN mResultado 		
	ENDPROC
	***********************************************************************************	
	
	
	
	***********************************************************************************
	*	ObtenerDatosComercio: 
	*	Esta funcion devuelve los datos del comercio y del contribuyente
	***********************************************************************************
	PROCEDURE ObtenerDatosComercio(pNroComercio as Number) as String
		PRIVATE mResult_XML as String


		mResult_XML = ''
		IF AmbienteOK
			EfimuniWeb()
									 
			IF NOT (EMPTY(pNroComercio))
				mResult_XML = _ObtenerDatosComercio(pNroComercio, 'C')
			ENDIF					
		ENDIF	
		
		
		RETURN ALLTRIM(mResult_XML)
	ENDPROC
	***********************************************************************************	
	
	
	
	***********************************************************************************
	*	BuscarComercio: 
	*	Esta funcion devuelve los comercios obtenidos en la busqueda, la busqueda
	*	puede ser por CODIGO, CUIT, NOMBRE_COMERCIO, NOMBRE_FANTASIA
	***********************************************************************************
	PROCEDURE BuscarComercio(pDato as String, pTipoBusqueda as String) as String
		PRIVATE mResultado_XML as String
										
		mResultado_XML = ''
		IF (AmbienteOK) THEN														 
			EfimuniWeb()								 

			mResultado_XML = _BuscarComercio(pDato, pTipoBusqueda)
		ENDIF	
		
				
		RETURN ALLTRIM(mResultado_XML)
	ENDPROC
	***********************************************************************************	
	
	

	
	
	
	***********************************************************************************
	*	ObtainCTACTE: 
	*	This function returns a list of the DDJJ
	***********************************************************************************
	PROCEDURE ObtainCTACTE(pNumberComercio as Number) as String
		PRIVATE mResult_XML as String
		PRIVATE mRecurso as String

		
		* I check then path of the resources
		IF (AmbienteOK) THEN
			EfimuniWeb()
			
									 
			* I obtain the number of the recurso related to TISH and returns a list of the DDJJ
			IF NOT (EMPTY(pNumberComercio))		
				mRecurso = _ObtenerRecursoTISH()				
				mResult_XML = _ObtainCTACTE(mRecurso, pNumberComercio)				
			ENDIF					
		ENDIF	
		
		
		RETURN ALLTRIM(mResult_XML)
	ENDPROC
	***********************************************************************************	
	
	

	
	
	
	***********************************************************************************
	*	getListadoDDJJByComercio: 
	*	Este m�todo devuelve el listado de declaraciones juradas de los �ltimos dos
	* 	anio para un comercio
	***********************************************************************************
	PROCEDURE getListadoDDJJByComercio(p_numNroComercio as Number) as String		
		EfimuniWeb()
		RETURN _getListadoDDJJByComercio(p_numNroComercio)				
	ENDPROC
	***********************************************************************************	
		
	

	
	
	
	***********************************************************************************
	*	DetailDDJJ: 
	*	This function returns the detail of only a DDJJ
	***********************************************************************************
	PROCEDURE ObtainDetailDDJJ(pNumberComercio as Number, pYear as Number, pQuota as Number) as String
		PRIVATE mResult_XML as String

		
		* I check then path of the resources
		IF (AmbienteOK) THEN
			EfimuniWeb()
			
									 
			* I obtain the detail of only a DDJJ
			mResult_XML = _ObtainDetailDDJJ(pNumberComercio, pYear, pQuota)				
		ENDIF	
				
		
		RETURN ALLTRIM(mResult_XML)
	ENDPROC
	***********************************************************************************	
			
	

	
	
	
	***********************************************************************************
	*	ObtainRubros: 
	*	This function returns the detail of only a DDJJ
	***********************************************************************************
	PROCEDURE ObtainRubros(pNumberComercio as Number) as String
		PRIVATE mResult_XML as String


		IF (AmbienteOK) THEN
			EfimuniWeb()
									 
			* I obtain the Rubros of the comercio
			mResult_XML = _ObtainRubros(pNumberComercio)				
		ENDIF	
		
		RETURN ALLTRIM(mResult_XML)
	ENDPROC
	***********************************************************************************	
			
	

	
	
	
	***********************************************************************************
	*	ObtainNumberRecursoTISH: 
	*	This function returns the detail of only a DDJJ
	***********************************************************************************
	PROCEDURE ObtainNumberRecursoTISH() as String
		PRIVATE mResult_XML as String

		IF (AmbienteOK) THEN
			EfimuniWeb()
											 			
			mResult_XML = _ObtenerRecursoTISH()			
		ENDIF	
		
		RETURN ALLTRIM(mResult_XML)
	ENDPROC
	***********************************************************************************	
		
	

	
	
	
	***********************************************************************************
	*	ObtainPeriodsTISH: 
	*	This function returns the periods that the municipalidad was using to receive the tish
	***********************************************************************************
	PROCEDURE ObtainPeriodsTISH() as String
		PRIVATE mResult_XML as String
		PRIVATE mNameFunction as String
		
		mResult_XML = ''
		IF (AmbienteOK) THEN
			EfimuniWeb()
			
			mResult_XML = Call_PRG_Excluded('_ObtainPeriodsTISH', '_ObtainPeriodsTISH')
		ENDIF	
						
		RETURN ALLTRIM(mResult_XML)
	ENDPROC	
	***********************************************************************************	
	***********************************************************************************	
		
	

	
	
	
	***********************************************************************************
	*	Exist_DDJJ: 
	*	This function says to us if the ddjj exists
	***********************************************************************************
	PROCEDURE Exist_DDJJ(pNumberComercio as Integer, pYear as Integer, pQuota as Integer) as Boolean
		PRIVATE mResult as Boolean
			
		IF (AmbienteOK) THEN
			EfimuniWeb()
						
			mResult = .F.
			mResult = _Exist_DDJJ(pNumberComercio, pYear, pQuota)						
		ENDIF					
		
		RETURN mResult
	ENDPROC
	***********************************************************************************		
		
	

	
	
	
	***********************************************************************************
	*	AddDDJJ: 
	*	This function returns the periods that the municipalidad was using to receive the tish
	***********************************************************************************
	PROCEDURE AddDDJJ(pRecurso as String, pCuit as String,;
					  pNumberComercio as Integer,;
					  pYear as Integer, pQuota as Integer,;
					  pDatos_DDJJ as String) as String
					  					  
		PRIVATE mResult_XML as String
		
		IF (AmbienteOK) THEN
			EfimuniWeb()
					
			mResult_XML = ''
			mResult_XML = _New_DDJJ(pRecurso, pCuit,pNumberComercio, pYear, pQuota, pDatos_DDJJ)				
		ENDIF	
						
		RETURN ALLTRIM(mResult_XML)
	ENDPROC
	***********************************************************************************		
	***********************************************************************************		
	
	
	
	***********************************************************************************
	*	ObtainList_DateExpiry: 
	*	Description: 
	***********************************************************************************
	PROCEDURE ObtainList_DateExpiry(pYear as Integer) as String			  					  
		PRIVATE mResult_XML as String
		PRIVATE mResourceTISH as String
		 	

		mResult_XML = ''
		IF (AmbienteOK) THEN
					
			* Seteo of variables					 
			EfimuniWeb()
			
			mResourceTISH  = ''
			mResourceTISH  = _ObtenerRecursoTISH()						
			mResult_XML = _ObtainList_DateExpiry(ALLTRIM(mResourceTISH), pYear)			
		ENDIF	
				
		
		RETURN ALLTRIM(mResult_XML)
	ENDPROC
	***********************************************************************************		
	***********************************************************************************		

	
	
	***********************************************************************************
	*	Add_Date_Expiry: 
	*	Description: 
	***********************************************************************************
	PROCEDURE Add_Date_Expiry (pYear as Integer, pQuota as Integer, pDate_Quota1 as String, pDate_Quota2 as String, pDate_p_ddjj as String) as String			  					  
		PRIVATE mResource as String
		PRIVATE mResult_XML as String
		 	
		mResult_XML = ''
		IF (AmbienteOK) THEN
			EfimuniWeb()
			
			mResource = _ObtenerRecursoTISH()								
			mResult_XML = _ADD_FEC_VTOS(ALLTRIM(mResource), pYear, pQuota, ALLTRIM(pDate_Quota1), ALLTRIM(pDate_Quota2), ALLTRIM(pDate_p_ddjj))
		ENDIF			
		
		RETURN ALLTRIM(mResult_XML)
	ENDPROC
	***********************************************************************************		
	***********************************************************************************		
	
	
	
	
	***********************************************************************************
	*	ObtainNumberVoucher
	*	This function returns the group of the voucher and the number of the voucher 
	***********************************************************************************
	PROCEDURE ObtainNumberVoucher(pRecurso as String, pNumberComercio as Integer, pYear as Integer, pQuota as Integer) as String
		PRIVATE mResult as String
	 
		mResult = ''
		mResult = _ObtainNumberVoucher(pRecurso, pNumberComercio, pYear, pQuota)	 
		
		RETURN ALLTRIM(mResult)		
	ENDPROC
	***********************************************************************************

	
	
	
	***********************************************************************************
	*	ConsultarDeuda
	***********************************************************************************
	PROCEDURE ConsultarDeuda (TipoImp as String, NroImp as double) as String
		PRIVATE retorno
		PRIVATE mResult_XML  as String
		PRIVATE mRecurso as String
	 

		*PRIVATE nNroImp
		
		
		AuxArchDeud = '<?xml version="1.0" encoding="UTF-8"?>' + _S_
		AuxArchDeud = AuxArchDeud + '<VFPDATA>' + _S_
		
			  
	 	 
		IF AmbienteOK
		
		
			*RETURN "juanito"
			EfimuniWeb()
							
					
			* BEGIN Apertura de tablas
			=UseT('recur\pla_imp')
			SELECT pla_imp
			SCATTER MEMVAR BLANK
			=UseT('recur\codifics')
			SELECT codifics
			SCATTER MEMVAR BLANK
			=UseT('recur\concs_cc')
			SELECT concs_cc
			SCATTER MEMVAR BLANK
			=UseT('recur\rec_cc')
			SELECT rec_cc
			SCATTER MEMVAR BLANK
			=UseT('recur\rec_ccc')
			SELECT rec_ccc
			SCATTER MEMVAR BLANK
			=UseT('recur\rec_fec')
			SELECT rec_fec
			SCATTER MEMVAR BLANK
			=UseT('recur\comprob')
			SELECT comprob
			SCATTER MEMVAR BLANK
			=UseT('recur\comp_ren')
			SELECT comp_ren
			SCATTER MEMVAR BLANK
			=UseT('recur\com_ddjj')
			SELECT comp_ren
			SCATTER MEMVAR BLANK
			* END Apertura de tablas
			
			
			
			* Creaci�n de cursores
			cre_cursores()
			

	
		
			_ConsultarDeuda (@AuxArchDeud, TipoImp, NroImp, AmbienteOK)
			AgregarTelefonoYMail(@AuxArchDeud)
		ENDIF
		
		
		
		AuxArchDeud = AuxArchDeud + Indent(1) + '<NOTAS>' + _S_
		AgregarMensaje (@AuxArchDeud,'REGISTRO_NOTAS','MENSAJE')
		AuxArchDeud = AuxArchDeud + Indent(1) + '</NOTAS>' + _S_
		
		AgregarTiempo (@AuxArchDeud)
		
		
		
		mRecurso = _ObtenerRecursoTISH()	
		mResult_XML = _ObtenerListadoREC_CCC(mRecurso, NroImp)
		AuxArchDeud = AuxArchDeud + mResult_XML
		
		
		
		AuxArchDeud = AuxArchDeud + '</VFPDATA>' + _S_
		
		RETURN STRCONV(AuxArchDeud,_STRCONV_)
		
	ENDPROC
	***********************************************************************************

	

	
	***********************************************************************************
	*	EmitirCompPagoNoVenc
	***********************************************************************************
	PROCEDURE EmitirCompPagoNoVenc (XMLGrupoNroComp as String) as String
		PRIVATE retorno
		PRIVATE AuxArchCompNoVenc as String
		

		AuxArchCompNoVenc = ''
		AuxArchCompNoVenc = EmitirCompPagoNoVencGeneric(XMLGrupoNroComp) 
		

		RETURN STRCONV(AuxArchCompNoVenc,_STRCONV_)
	ENDPROC		
	***********************************************************************************
	
	
	
	***********************************************************************************
	*	EmitirCompPagoDeudaVenc
	***********************************************************************************
	PROCEDURE EmitircompPagoDeudaVenc (XMLDeudaVenc as String) as String	
		AuxArchComp = '<?xml version="1.0" encoding="UTF-8"?>' + _S_
		AuxArchComp = AuxArchComp + '<VFPDATA>' + _S_
		
		
		IF (AmbienteOK) THEN
			EfimuniWeb()

			IF (!USED('numerad')) THEN			
				=UseT('recur\numerad')		
			ENDIF
			IF (!USED('comprob')) THEN			
				=UseT('recur\comprob')
			ENDIF			
			IF (!USED('comp_ren')) THEN			
				=UseT('recur\comp_ren')
			ENDIF			
			IF (!USED('rec_cc')) THEN			
				=UseT('recur\rec_cc')
			ENDIF
			IF (!USED('rec_fec')) THEN			
				=UseT('recur\rec_fec')
			ENDIF			
			IF (!USED('concs_cc')) THEN			
				=UseT('recur\concs_cc')
			ENDIF			
			IF (!USED('pla_imp')) THEN			
				=UseT('recur\pla_imp')
			ENDIF			

						 			 
			_EmitircompPagoDeudaVenc (@AuxArchComp, XMLDeudaVenc, AmbienteOK)
			AgregarTelefonoYMail (@AuxArchComp)
		ENDIF
		
		AgregarMensaje (@AuxArchComp,'ERROR','MENSERROR')

		AgregarTiempo (@AuxArchComp)
		
		AuxArchComp = AuxArchComp + '</VFPDATA>' + _S_

		RETURN STRCONV(AuxArchComp,_STRCONV_)
	ENDPROC
	***********************************************************************************
	
	
	
	***********************************************************************************
	*	getListadoDeComercios
	* 	Este m�todo devuelve un listado de comercios con infomaci�n b�sica
	***********************************************************************************
	PROCEDURE getListadoDeComercios(p_strComercios as String) as String
		EfimuniWeb()
		RETURN _getListadoDeComercios(ALLTRIM(p_strComercios))		
	ENDPROC
	***********************************************************************************
	
	
	
	
	***********************************************************************************
	*	_ObtainListTrades
	* 	Este m�todo devuelve un listado de comercios con infomaci�n b�sica
	***********************************************************************************
	PROCEDURE obtainListTrades(p_strComercios as String) as String
		EfimuniWeb()
		RETURN _ObtainListTrades(ALLTRIM(p_strComercios))		
	ENDPROC
	***********************************************************************************
	
	
	
	
	
	***********************************************************************************
	*	ObtainDateOfExpiry
	* 	This function returns periods with date of expiry bigger than today
	***********************************************************************************
	PROCEDURE ObtainDateOfExpiry() as String
		PRIVATE mResult as String
		EfimuniWeb()
			 
		mResult = ''
		mResult = _ObtainDateOfExpiry()
	 		
		RETURN ALLTRIM(mResult)		
	ENDPROC
	***********************************************************************************
	
	
	
	
	
	***********************************************************************************
	*	Edit_Fec_Vtos
	* 	Description:
	***********************************************************************************
	PROCEDURE Edit_Fec_Vtos(pYear as Number, pQuota as Number, pDate_Quota1 as String, pDate_Quota2 as String, pDate_p_ddjj as String) as String
		PRIVATE mResult as String
		PRIVATE mResource as String

		EfimuniWeb()
		
		mResource = ''	 		
		mResult = ''
		mResource = _ObtenerRecursoTISH()								
		mResult = _Edit_Fec_Vtos(ALLTRIM(mResource), pYear, pQuota, ALLTRIM(pDate_Quota1), ALLTRIM(pDate_Quota2), ALLTRIM(pDate_p_ddjj))			

		RETURN ALLTRIM(mResult)
	ENDPROC
	***********************************************************************************
				
	
	
	
	
	***********************************************************************************
	*	Fec_Vto_CTACTE_Exist
	* 	Description:
	***********************************************************************************			  
	PROCEDURE Fec_Vto_CTACTE_Exist(pYear as Integer, pQuota as Integer) as String
		PRIVATE mResult as String
		PRIVATE mResource as String

	 	EfimuniWeb()
		
		
		mResult = ''
		mResource = ''	 
		mResource = _ObtenerRecursoTISH()								

		IF (_Exist_Fec_Vto(ALLTRIM(mResource), pYear, pQuota)) THEN
			mResult = 'SI'
		ELSE		
			mResult = 'NO'
	 	ENDIF
	 	
		RETURN ALLTRIM(mResult)
	ENDPROC
	***********************************************************************************

	
	
	
	
	***********************************************************************************
	*	Delete_Fec_Vtos
	* 	Description:
	***********************************************************************************			  
	PROCEDURE Delete_Fec_Vto(pYear as Integer, pQuota as Integer) as String
		PRIVATE mResult as String
		PRIVATE mResource as String
		 			 
		EfimuniWeb()	
		
		mResult = ''
		mResource = ''	 
		mResource = _ObtenerRecursoTISH()								

		IF (_Exist_Fec_Vto(ALLTRIM(mResource), pYear, pQuota)) THEN
			mResult = 'Per�odo existente en Cta. Cte.'
		ELSE		
			mResult = _Delete_Fec_Vto(ALLTRIM(mResource), pYear, pQuota)					
	 	ENDIF
	 	
		RETURN ALLTRIM(mResult)
	ENDPROC
	***********************************************************************************
	
	
	
	
	**********************************************************************************
	*	Obtengo la descripcion de recurso
	***********************************************************************************
	PROCEDURE ObtainDescriptionResourceTISH() as String
		PRIVATE mXML as String 
		mXML = _ObtainDescriptionResourceTISH()
		RETURN ALLTRIM(mXML)
	ENDPROC
	**********************************************************************************
	
	
	
	**********************************************************************************
	*	Obtengo los datos del municipio
	***********************************************************************************
	PROCEDURE ObtainMunicipalidadData() as String
		PRIVATE mXML as String 
		mXML = _ObtainMunicipalidadData()
		RETURN TRIM(mXML)
	ENDPROC
	***********************************************************************************		
	***********************************************************************************		


	
	
	
	***********************************************************************************
	* Verifico que existan y esten definidos todos los parametros antes de iniciar 
	* cualquier operacion en el sistema
	***********************************************************************************
	PROCEDURE VerificarParametros(pVerParametros as Boolean) as String
		PRIVATE mXML as String
		
		EfimuniWeb()
		mXML = _VerificarParametros(pVerParametros)
		
		RETURN ALLTRIM(mXML)	
	ENDPROC
	***********************************************************************************
	***********************************************************************************




	***********************************************************************************
	* Verifico que existan y esten definidos todos los parametros antes de iniciar 
	* cualquier operacion en el sistema
	***********************************************************************************
	PROCEDURE ObtenerFechaActuaWeb() as String		
		PRIVATE mXMLDate as Date
		
		EfimuniWeb()
		mXMLDate = _ObtenerFechaActuaWeb()				
		
		RETURN ALLTRIM(DTOC(mXMLDate))
	ENDPROC
	***********************************************************************************
	***********************************************************************************




	***********************************************************************************
	* Obtengo el articulo que utiliza la municipalidad para esta tasa
	***********************************************************************************
	PROCEDURE getArticle() as String		
		PRIVATE _Article as String
		
		EfimuniWeb()
		_Article = _getArticle()
		
		RETURN ALLTRIM(_Article)
	ENDPROC
	***********************************************************************************
	***********************************************************************************		




	***********************************************************************************
	* Checkeo si usa el valor cantidad de empleados en la DDJJ
	***********************************************************************************
	PROCEDURE getFechaVtoPeriodo(p_numYear as Number, p_numCuota as Number) as String		
		PRIVATE strRecursoTISH as String
		
		EfimuniWeb()
		strRecursoTISH = _ObtenerRecursoTISH()	
		
		RETURN _getFechasVtoPeriodo(ALLTRIM(strRecursoTISH), p_numYear, p_numCuota) 
		
	ENDPROC
	***********************************************************************************
	***********************************************************************************		








	***********************************************************************************
	* Checkeo si usa el valor cantidad de empleados en la DDJJ
	***********************************************************************************
	PROCEDURE getEmployeesUsing() as String		
		PRIVATE _Using as String
		
		EfimuniWeb()
		_Using = _getEmployeesUsing()
		
		RETURN ALLTRIM(_Using)
	ENDPROC
	***********************************************************************************
	***********************************************************************************		
	
	
	
	
	***********************************************************************************
	* Listado de rubros 
	***********************************************************************************
	PROCEDURE getRubros() as String
		EfimuniWeb()
		RETURN _getRubros()
	ENDPROC
	***********************************************************************************
	***********************************************************************************		
	



	***********************************************************************************
	* Devuelve los rubros asociados a un comercio
	***********************************************************************************
	PROCEDURE getRubrosByComercio(p_strNroComercio as String) as String
		EfimuniWeb()
		RETURN _getRubrosByComercio(ALLTRIM(p_strNroComercio))
	ENDPROC
	***********************************************************************************
	***********************************************************************************		
 
	***********************************************************************************
	* Devuelve los datos de un contribuyente a partir de su cuit
	***********************************************************************************
	***********************************************************************************
	PROCEDURE getDatosContribuyentebyCuit(pNroCuit as String) as String
		PRIVATE mResult_XML as String


		mResult_XML = ''
		IF AmbienteOK
			EfimuniWeb()
									 
			IF NOT (EMPTY(pNroCuit))
				mResult_XML = _getDatosContribuyenteByCuit(ALLTRIM(pNroCuit))
			ENDIF					
		ENDIF	
		
		
		RETURN ALLTRIM(mResult_XML)
	ENDPROC
	***********************************************************************************	
	
	
	***********************************************************************************
	*	Limpiar memoria:
	*	Cierra y borra la memoria 
	***********************************************************************************
	PROCEDURE BorrarTodo
		CLOSE ALL
		CLEAR MEMORY
	ENDPROC
	***********************************************************************************
	
	
	
	***********************************************************************************
	*	Destructor Impl�cito:
	*	Cierra y borra la memoria 
	***********************************************************************************
	PROCEDURE Destroy
		CLOSE ALL
		CLEAR MEMORY
	ENDPROC
	***********************************************************************************
	
ENDDEFINE