*************************************************
* FUNCTION FINTERE
*************************************************
* Autor : Leo
* Dise�o: Leo
* 
* Fecha : 29/12/97
* 
* Funcionamiento: calcula el interes
* 
*	
*	SQLMODI01
*		Usuario							: Gualeguay
*		Fecha								: 11/01/2000
*		Quien modific�			: Carlos
*		Detalle problema		: Cuando se calculaba inter�s sobre una cuota
*													donde el a�o y mes del vencimiento eran iguales
*													al de actualizaci�n, el inter�s se calculaba
*													mal.
*													
*		Detalle correcci�n	: Antes solo multiplicaba el inter�s mensual por
*													el importe origen. Ahora adem�s, lo divide por
*													3000 (ser�a dividido 30 d�as y divido 100).
*													
************************************************************************
PARAMETERS importe, indice, fechaini, fechafin
PRIVATE coefic, anio, mes, dia, dia_o, mes_o, anio_o, dias_m, anio_ant, mes_ant

IF !USED ('INDICES')
	=USET ('RECUR\INDICES')
ENDIF

coefic = 0

anio_o = YEAR  (m.fechaini)
mes_o  = MONTH (m.fechaini)
dia_o  = DAY   (m.fechaini)
IF SEEK (m.indice + STR (m.anio_o,4,0) + STR(m.mes_o,2,0), 'indices') 
	anio   = YEAR  (m.fechafin)
	mes    = MONTH (m.fechafin)
	dia    = DAY   (m.fechafin)
	IF m.anio_o = m.anio AND m.mes_o = m.mes
		m.coefic = indices.porc_ind * (m.dia - m.dia_o) / 3000
	ELSE
		coefic = - indices.val_ind
		IF indices.porc_ind <> 0
			m.dias_m =	DAY (GOMONTH (CTOD ('01/' + STR (m.mes_o, 2) + '/' + ;
  	                       STR (m.anio_o, 4)), 1) - 1)
			m.coefic = m.coefic + indices.porc_ind * (m.dias_m - m.dia_o) / 3000
		ENDIF

		IF m.mes > 1
			anio_ant = m.anio
			mes_ant  = m.mes - 1
		ELSE
			anio_ant = m.anio - 1
			mes_ant  = 12
		ENDIF

		IF SEEK (m.indice + STR (m.anio_ant,4,0) + STR(m.mes_ant,2,0),'indices')
			m.coefic = indices.val_ind + m.coefic

			IF SEEK (m.indice + STR (m.anio,4,0) + STR(m.mes,2,0), 'indices')
				IF indices.porc_ind <> 0
					m.coefic = m.coefic + indices.porc_ind * m.dia / 3000
				ENDIF
			ELSE
				mensaje = 'No est�n cargados los valores para el �ndice de inter�s ' +;
					m.indice + ' para el mes ' + ALLT(STR(m.mes,2,0)) + ' del a�o ' +;
					STR(m.anio,4,0) + '. Verifique!'
				=FINAL(15, mensaje)
			ENDIF
		ELSE
			mensaje = 'No est�n cargados los valores para el �ndice de inter�s ' +;
				m.indice + ' para el mes ' + ALLT(STR(m.mes_ant,2,0)) + ' del a�o ' +;
				STR(m.anio_ant,4,0) + '. Verifique!'
			=FINAL(15, mensaje)
		ENDIF
	ENDIF
ELSE
	mensaje = 'No est�n cargados los valores para el �ndice de inter�s ' +;
		m.indice + ' para el mes ' + ALLT(STR(m.mes_o,2,0)) + ' del a�o ' +;
		STR(m.anio_o,4,0) + '. Verifique!'
	=FINAL(15, mensaje)
ENDIF

IF m.coefic < 0
	m.coefic = 0
ENDIF

RETURN ROUND (m.importe * m.coefic, 2)
