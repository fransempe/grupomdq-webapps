PROCEDURE AgregarProxCompAVenc(AuxArchDeud as String)
	PRIVATE AuxPrimeraVez
	
	AuxPrimeraVez = .T.
	
	GO TOP IN CompAVenc
	SELECT CompAVenc
	DO WHILE NOT EOF('CompAVenc')
		IF CompAVenc.fecha_venc <= DATE() + 60
			IF AuxPrimeraVez
				AuxArchDeud = AuxArchDeud + Indent(1) + '<DETALLEPROXCOMPAVENC>' + _S_
				AuxPrimeraVez = .F.
			ENDIF
			AuxArchDeud = AuxArchDeud + Indent(2) + '<REGISTRO_DETALLEPROXCOMPAVENC>' + _S_
			
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DPC_COMPROBANTE>' + PADL(ALLTRIM(STR(grupo_comp,2,0)),2,'0') ;
				+ '/' + PADL(ALLTRIM(STR(nro_comp,9,0)),9,'0') + '/' + ;
				PADL(ALLTRIM(STR(dv_comp,2,0)),2,'0') + '</DPC_COMPROBANTE>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DPC_DETALLE>' + ALLTRIM(detalle_comp) + '</DPC_DETALLE>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DPC_VENC>' + ALLTRIM(STR(venc,1,0)) + '</DPC_VENC>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DPC_FECHAVENC>' + DTOC(fecha_venc) + '</DPC_FECHAVENC>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DPC_IMPORTEORIGEN>' + ALLTRIM(STR(tot_ori,15,2)) + '</DPC_IMPORTEORIGEN>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DPC_IMPORTERECARGO>' + ALLTRIM(STR(tot_actint,15,2)) + '</DPC_IMPORTERECARGO>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DPC_IMPORTETOTAL>' + ALLTRIM(STR(tot_comp,15,2)) + '</DPC_IMPORTETOTAL>' + _S_
						
			AuxArchDeud = AuxArchDeud + Indent(2) + '</REGISTRO_DETALLEPROXCOMPAVENC>' + _S_
		ENDIF
		SKIP IN CompAVenc
	ENDDO
	IF NOT AuxPrimeraVez
		AuxArchDeud = AuxArchDeud + Indent(1) + '</DETALLEPROXCOMPAVENC>' + _S_
	ELSE
		AuxArchDeud = AuxArchDeud + Indent(1) + '<DETALLEPROXCOMPAVENC>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(1) + '</DETALLEPROXCOMPAVENC>' + _S_
	ENDIF
ENDPROC
