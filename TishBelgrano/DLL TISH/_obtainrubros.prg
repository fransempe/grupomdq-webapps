
* _ObtainRubros:
* This function returns the rubros of the comercio
PROCEDURE _ObtainRubros(pNumberComercio as Number) as String				
	LOCAL oErr AS Exception 
	PRIVATE mIndex as String
	PRIVATE mXML_AUX as String


	TRY
	
		* I open the table comercio
		=UseT('recur\comercio')
		SELECT comercio
		SCATTER MEMVAR BLANK

		* I open the table rubs_com
		=UseT('recur\rubs_com')
		SELECT rubs_com
		SCATTER MEMVAR BLANK


		
		* I create the index
		mIndex = STR(pNumberComercio, 10, 0)

	
		
		* I obtain the detail of the DDJJ
		mXML_AUX = '<RUBROS>' + _S_
		IF SEEK(mIndex, 'comercio') THEN	
		
			
			*-------------------------------- RUBRO1 -------------------------------- 
			IF (comercio.rubro1 > 0) THEN
							
				* I create the index for search the rubro
				mIndex = STR(comercio.rubro1, 7, 0)
				IF SEEK(mIndex, 'rubs_com') THEN
		

					* I create the row with the data of the rubro
					mXML_AUX = mXML_AUX + '<LINES_RUBRO>'				
					mXML_AUX = mXML_AUX + '<RUBRO_CODE>' + ALLTRIM(STR(rubs_com.rubro_c)) + '</RUBRO_CODE>'  
					mXML_AUX = mXML_AUX + '<RUBRO_DESCRIPTION>' + ALLTRIM(rubs_com.dsc_rubc) + '</RUBRO_DESCRIPTION>' + _S_
					mXML_AUX = mXML_AUX + '<RUBRO_ALIQUOT>' + ALLTRIM(STR(rubs_com.porc_iddjj)) + '</RUBRO_ALIQUOT>' + _S_				
					
					mXML_AUX = mXML_AUX + '<EMPLOYEES></EMPLOYEES>' + _S_				
					mXML_AUX = mXML_AUX + '<AMOUNT></AMOUNT>' + _S_				
					
					
					mXML_AUX = mXML_AUX + '</LINES_RUBRO>'
				ENDIF					
			ENDIF
			*------------------------------------------------------------------------ 
			
			
			*-------------------------------- RUBRO2 -------------------------------- 
			IF (comercio.rubro2 > 0) THEN
							
				* I create the index for search the rubro
				mIndex = STR(comercio.rubro2, 7, 0)
				IF SEEK(mIndex, 'rubs_com') THEN
		

					* I create the row with the data of the rubro
					mXML_AUX = mXML_AUX + '<LINES_RUBRO>'				
					mXML_AUX = mXML_AUX + '<RUBRO_CODE>' + ALLTRIM(STR(rubs_com.rubro_c)) + '</RUBRO_CODE>'  
					mXML_AUX = mXML_AUX + '<RUBRO_DESCRIPTION>' + ALLTRIM(rubs_com.dsc_rubc) + '</RUBRO_DESCRIPTION>' + _S_
					mXML_AUX = mXML_AUX + '<RUBRO_ALIQUOT>' + ALLTRIM(STR(rubs_com.porc_iddjj)) + '</RUBRO_ALIQUOT>' + _S_				
					
					mXML_AUX = mXML_AUX + '<EMPLOYEES></EMPLOYEES>' + _S_				
					mXML_AUX = mXML_AUX + '<AMOUNT></AMOUNT>' + _S_				

					
					mXML_AUX = mXML_AUX + '</LINES_RUBRO>'
				ENDIF					
			ENDIF
			*------------------------------------------------------------------------ 
			
			
			*-------------------------------- RUBRO3 -------------------------------- 
			IF (comercio.rubro3 > 0) THEN
							
				* I create the index for search the rubro
				mIndex = STR(comercio.rubro3, 7, 0)
				IF SEEK(mIndex, 'rubs_com') THEN
		

					* I create the row with the data of the rubro
					mXML_AUX = mXML_AUX + '<LINES_RUBRO>'				
					mXML_AUX = mXML_AUX + '<RUBRO_CODE>' + ALLTRIM(STR(rubs_com.rubro_c)) + '</RUBRO_CODE>'  
					mXML_AUX = mXML_AUX + '<RUBRO_DESCRIPTION>' + ALLTRIM(rubs_com.dsc_rubc) + '</RUBRO_DESCRIPTION>' + _S_
					mXML_AUX = mXML_AUX + '<RUBRO_ALIQUOT>' + ALLTRIM(STR(rubs_com.porc_iddjj)) + '</RUBRO_ALIQUOT>' + _S_	
					
			mXML_AUX = mXML_AUX + '<EMPLOYEES></EMPLOYEES>' + _S_				
					mXML_AUX = mXML_AUX + '<AMOUNT></AMOUNT>' + _S_				

								
					mXML_AUX = mXML_AUX + '</LINES_RUBRO>'
				ENDIF					
			ENDIF
			*------------------------------------------------------------------------ 
			
			
			*-------------------------------- RUBRO4 -------------------------------- 
			IF (comercio.rubro4 > 0) THEN
							
				* I create the index for search the rubro
				mIndex = STR(comercio.rubro4, 7, 0)
				IF SEEK(mIndex, 'rubs_com') THEN
		

					* I create the row with the data of the rubro
					mXML_AUX = mXML_AUX + '<LINES_RUBRO>'				
					mXML_AUX = mXML_AUX + '<RUBRO_CODE>' + ALLTRIM(STR(rubs_com.rubro_c)) + '</RUBRO_CODE>'  
					mXML_AUX = mXML_AUX + '<RUBRO_DESCRIPTION>' + ALLTRIM(rubs_com.dsc_rubc) + '</RUBRO_DESCRIPTION>' + _S_
					mXML_AUX = mXML_AUX + '<RUBRO_ALIQUOT>' + ALLTRIM(STR(rubs_com.porc_iddjj)) + '</RUBRO_ALIQUOT>' + _S_	
					
					mXML_AUX = mXML_AUX + '<EMPLOYEES></EMPLOYEES>' + _S_				
					mXML_AUX = mXML_AUX + '<AMOUNT></AMOUNT>' + _S_				
								
					mXML_AUX = mXML_AUX + '</LINES_RUBRO>'
				ENDIF					
			ENDIF
			*------------------------------------------------------------------------ 
			
			
			*-------------------------------- RUBRO5 -------------------------------- 
			IF (comercio.rubro5 > 0) THEN
							
				* I create the index for search the rubro
				mIndex = STR(comercio.rubro5, 7, 0)
				IF SEEK(mIndex, 'rubs_com') THEN
		

					* I create the row with the data of the rubro
					mXML_AUX = mXML_AUX + '<LINES_RUBRO>'				
					mXML_AUX = mXML_AUX + '<RUBRO_CODE>' + ALLTRIM(STR(rubs_com.rubro_c)) + '</RUBRO_CODE>'  
					mXML_AUX = mXML_AUX + '<RUBRO_DESCRIPTION>' + ALLTRIM(rubs_com.dsc_rubc) + '</RUBRO_DESCRIPTION>' + _S_
					mXML_AUX = mXML_AUX + '<RUBRO_ALIQUOT>' + ALLTRIM(STR(rubs_com.porc_iddjj)) + '</RUBRO_ALIQUOT>' + _S_	
					
					mXML_AUX = mXML_AUX + '<EMPLOYEES></EMPLOYEES>' + _S_				
					mXML_AUX = mXML_AUX + '<AMOUNT></AMOUNT>' + _S_				
								
					mXML_AUX = mXML_AUX + '</LINES_RUBRO>'
				ENDIF					
			ENDIF
			*------------------------------------------------------------------------ 
			
			
			*-------------------------------- RUBRO6 -------------------------------- 
			IF (comercio.rubro6 > 0) THEN
							
				* I create the index for search the rubro
				mIndex = STR(comercio.rubro6, 7, 0)
				IF SEEK(mIndex, 'rubs_com') THEN
		

					* I create the row with the data of the rubro
					mXML_AUX = mXML_AUX + '<LINES_RUBRO>'				
					mXML_AUX = mXML_AUX + '<RUBRO_CODE>' + ALLTRIM(STR(rubs_com.rubro_c)) + '</RUBRO_CODE>'  
					mXML_AUX = mXML_AUX + '<RUBRO_DESCRIPTION>' + ALLTRIM(rubs_com.dsc_rubc) + '</RUBRO_DESCRIPTION>' + _S_
					mXML_AUX = mXML_AUX + '<RUBRO_ALIQUOT>' + ALLTRIM(STR(rubs_com.porc_iddjj)) + '</RUBRO_ALIQUOT>' + _S_	
					
					mXML_AUX = mXML_AUX + '<EMPLOYEES></EMPLOYEES>' + _S_				
					mXML_AUX = mXML_AUX + '<AMOUNT></AMOUNT>' + _S_				
								
					mXML_AUX = mXML_AUX + '</LINES_RUBRO>'
				ENDIF					
			ENDIF
			*------------------------------------------------------------------------ 
			
			
			*-------------------------------- RUBRO7 -------------------------------- 
			IF (comercio.rubro7 > 0) THEN
							
				* I create the index for search the rubro
				mIndex = STR(comercio.rubro7, 7, 0)
				IF SEEK(mIndex, 'rubs_com') THEN
		

					* I create the row with the data of the rubro
					mXML_AUX = mXML_AUX + '<LINES_RUBRO>'				
					mXML_AUX = mXML_AUX + '<RUBRO_CODE>' + ALLTRIM(STR(rubs_com.rubro_c)) + '</RUBRO_CODE>'  
					mXML_AUX = mXML_AUX + '<RUBRO_DESCRIPTION>' + ALLTRIM(rubs_com.dsc_rubc) + '</RUBRO_DESCRIPTION>' + _S_
					mXML_AUX = mXML_AUX + '<RUBRO_ALIQUOT>' + ALLTRIM(STR(rubs_com.porc_iddjj)) + '</RUBRO_ALIQUOT>' + _S_	
					
					mXML_AUX = mXML_AUX + '<EMPLOYEES></EMPLOYEES>' + _S_				
					mXML_AUX = mXML_AUX + '<AMOUNT></AMOUNT>' + _S_				
								
					mXML_AUX = mXML_AUX + '</LINES_RUBRO>'
				ENDIF					
			ENDIF
			*------------------------------------------------------------------------ 
			
			
			*-------------------------------- RUBRO8 -------------------------------- 
			IF (comercio.rubro8 > 0) THEN
							
				* I create the index for search the rubro
				mIndex = STR(comercio.rubro8, 7, 0)
				IF SEEK(mIndex, 'rubs_com') THEN
		

					* I create the row with the data of the rubro
					mXML_AUX = mXML_AUX + '<LINES_RUBRO>'				
					mXML_AUX = mXML_AUX + '<RUBRO_CODE>' + ALLTRIM(STR(rubs_com.rubro_c)) + '</RUBRO_CODE>'  
					mXML_AUX = mXML_AUX + '<RUBRO_DESCRIPTION>' + ALLTRIM(rubs_com.dsc_rubc) + '</RUBRO_DESCRIPTION>' + _S_
					mXML_AUX = mXML_AUX + '<RUBRO_ALIQUOT>' + ALLTRIM(STR(rubs_com.porc_iddjj)) + '</RUBRO_ALIQUOT>' + _S_	
					
					mXML_AUX = mXML_AUX + '<EMPLOYEES></EMPLOYEES>' + _S_				
					mXML_AUX = mXML_AUX + '<AMOUNT></AMOUNT>' + _S_				
								
					mXML_AUX = mXML_AUX + '</LINES_RUBRO>'
				ENDIF					
			ENDIF
			*------------------------------------------------------------------------ 
			
			
			*-------------------------------- RUBRO9 -------------------------------- 
			IF (comercio.rubro9 > 0) THEN
							
				* I create the index for search the rubro
				mIndex = STR(comercio.rubro9, 7, 0)
				IF SEEK(mIndex, 'rubs_com') THEN
		

					* I create the row with the data of the rubro
					mXML_AUX = mXML_AUX + '<LINES_RUBRO>'				
					mXML_AUX = mXML_AUX + '<RUBRO_CODE>' + ALLTRIM(STR(rubs_com.rubro_c)) + '</RUBRO_CODE>'  
					mXML_AUX = mXML_AUX + '<RUBRO_DESCRIPTION>' + ALLTRIM(rubs_com.dsc_rubc) + '</RUBRO_DESCRIPTION>' + _S_
					mXML_AUX = mXML_AUX + '<RUBRO_ALIQUOT>' + ALLTRIM(STR(rubs_com.porc_iddjj)) + '</RUBRO_ALIQUOT>' + _S_	
					
					mXML_AUX = mXML_AUX + '<EMPLOYEES></EMPLOYEES>' + _S_				
					mXML_AUX = mXML_AUX + '<AMOUNT></AMOUNT>' + _S_				
								
					mXML_AUX = mXML_AUX + '</LINES_RUBRO>'
				ENDIF					
			ENDIF
			*------------------------------------------------------------------------ 
			
			
			*-------------------------------- RUBRO10 -------------------------------- 
			IF (comercio.rubro10 > 0) THEN
							
				* I create the index for search the rubro
				mIndex = STR(comercio.rubro10, 7, 0)
				IF SEEK(mIndex, 'rubs_com') THEN
		

					* I create the row with the data of the rubro
					mXML_AUX = mXML_AUX + '<LINES_RUBRO>'				
					mXML_AUX = mXML_AUX + '<RUBRO_CODE>' + ALLTRIM(STR(rubs_com.rubro_c)) + '</RUBRO_CODE>'  
					mXML_AUX = mXML_AUX + '<RUBRO_DESCRIPTION>' + ALLTRIM(rubs_com.dsc_rubc) + '</RUBRO_DESCRIPTION>' + _S_
					mXML_AUX = mXML_AUX + '<RUBRO_ALIQUOT>' + ALLTRIM(STR(rubs_com.porc_iddjj)) + '</RUBRO_ALIQUOT>' + _S_		
					
					mXML_AUX = mXML_AUX + '<EMPLOYEES></EMPLOYEES>' + _S_				
					mXML_AUX = mXML_AUX + '<AMOUNT></AMOUNT>' + _S_				
							
					mXML_AUX = mXML_AUX + '</LINES_RUBRO>'
				ENDIF					
			ENDIF
			*------------------------------------------------------------------------ 
				



			
						
		ENDIF		
		mXML_AUX = mXML_AUX + '</RUBROS>' + _S_	
	
	
	CATCH TO oErr
	   mXML_AUX = '<ERROR>' + ALLTRIM(oErr.Message) + '</ERROR>'
	FINALLY
	   	*CLEAR EVENTS
	ENDTRY
 
 
	RETURN ALLTRIM(mXML_AUX)
ENDPROC