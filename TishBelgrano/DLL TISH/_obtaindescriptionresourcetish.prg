
*_ObtainDescriptionResourceTISH:
* this function returns
PROCEDURE _ObtainDescriptionResourceTISH()as String
	PRIVATE mResourceNumber as String
	PRIVATE mResourceDescription as String
	PRIVATE mIndex as String
	PRIVATE mResult as String
	
	
	* I open table config
	IF (!USED('config')) THEN
		=UseT('asa\config')
	ENDIF
	SELECT config
	SCATTER MEMVAR BLANK
	 
	 		
	* Obtain the number
	mResourceNumber = ''	
	mIndex = PADR('RECUR',8,' ') + 'RECURSO_TISH'
	SET ORDER TO 1
	IF SEEK(mIndex) THEN
		mResourceNumber = ALLTRIM(config.cont_par)
	ENDIF
			
	
	
	
			

	* I open table codifics
	IF (!USED('codifics')) THEN
		=UseT('recur\codifics')
	ENDIF
	SELECT codifics
	SCATTER MEMVAR BLANK
	 	
	
	*Obtain the description
	mResourceDescription = ''
	mIndex = 'RECURS' + ALLTRIM(mResourceNumber)
	SET ORDER TO 1	
	IF SEEK(mIndex) THEN
		mResourceDescription = ALLTRIM(codifics.Desc_cod)
	ENDIF
			
			
			
	mResult = ''			
	mResult = '(' + mResourceNumber + ') ' + mResourceDescription
	RETURN ALLTRIM(mResult)
ENDPROC