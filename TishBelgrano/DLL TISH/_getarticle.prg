*_getArticle:
PROCEDURE _getArticle() as String
	PRIVATE mResult as String
	PRIVATE mIndex as String
	
	
	IF (!USED('config')) THEN
		=UseT('asa\config')
	ENDIF
	SELECT config
	SCATTER MEMVAR BLANK
	 	
	
	* i get the articulo
	mResult = ''	
	mIndex = PADR('RECUR',8,' ') + 'WEB_ART_TISH'
	IF (SEEK(mIndex)) THEN
		mResult = ALLTRIM(config.cont_par)
	ENDIF
									
	RETURN ALLTRIM(mResult)
ENDPROC