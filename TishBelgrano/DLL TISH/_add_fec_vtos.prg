 

*_ADD_FEC_VTOS:
* Description: 
PROCEDURE _ADD_FEC_VTOS (pResource as String, pYear as Number, pQuota as Number, pDate_Quota1 as String, pDate_Quota2 as String, pDate_p_ddjj as String) as String
	PRIVATE mResult as String
	PRIVATE mIndex as String



	* I open table fec_vtos
	IF (!USED('fec_vtos')) THEN
		=UseT('recur\fec_vtos')
	ENDIF
	SELECT fec_vtos
	SCATTER MEMVAR BLANK


	mIndex = ALLTRIM(pResource) + STR(pYear, 4, 0) + STR(pQuota, 3, 0)
	SET ORDER TO 1
	IF (SEEK(mIndex)) THEN
		mResult = 'Per�odo Existente'
		
	ELSE
		* I add the new record in the table
		APPEND BLANK	
		REPLACE fec_vtos.RECURSO 	WITH ALLTRIM(pResource), ;
		fec_vtos.ANIO 				WITH pYear, ;			
		fec_vtos.CUOTA 				WITH pQuota, ;			
		fec_vtos.FV1_CUOTA			WITH CTOD(ALLTRIM(pDate_Quota1)), ;
		fec_vtos.FV2_CUOTA			WITH CTOD(ALLTRIM(pDate_Quota2)), ;
		fec_vtos.FV_P_DDJJ			WITH CTOD(ALLTRIM(pDate_p_ddjj))
		
		mResult = 'OK'
		
	ENDIF 
	
	RETURN ALLTRIM(mResult)
ENDPROC