*******************************************************************
* FUNCTION FX_INTER
*
* Funci�n para c�lculo de intereses por mora de importes adeudados
* desarrollada especialmente para la Municipalidad de Navarro
*
*******************************************************************
PARAMETERS m.importe
PRIVATE interes

m.interes=0
IF recurso = 'MA' OR recurso='PV' OR RECURSO='67' OR RECURSO='21' AND orig_mov='ND' and m.fecha_vto >= CTOD('01/06/2001')
	m.interes= Fintere(m.importe, 'INTMAT', m.fecha_vto, m.fecha_final)
ELSE
	m.interes= Fintere(m.importe, 'INTSIM', m.fecha_vto, m.fecha_final)
ENDIF
IF M.PROGRAMA = 'PGENPLA ' 
		IF m.cod_plan='SUCO' OR m.cod_plan='SRCO' OR m.cod_plan='SHCO' OR m.cod_plan='IACO'
			m.interes= Fintere(m.importe, 'INT05%', m.fecha_vto, m.fecha_final)
		ENDIF	
ENDIF
IF M.PROGRAMA = 'PGENPLA ' 
	IF m.cod_plan='SR06' OR m.cod_plan = 'SR12' OR m.cod_plan='SU06' OR m.cod_plan='SU12' OR m.cod_plan='SH06' OR m.cod_plan='SH12' OR m.cod_plan='IA06' OR m.cod_plan='IA12'
		m.interes= Fintere(m.importe, 'INT1% ', m.fecha_vto, m.fecha_final)
	ENDIF	
ENDIF

IF recurso = 'PR' and m.fecha_vto >= CTOD('01/01/2007')
	m.interes= Fintere(m.importe, 'INTSIM', m.fecha_vto, m.fecha_final)
ENDIF

IF recurso = 'PR' and m.fecha_vto < CTOD('01/01/2007')
	m.interes= 0
ENDIF

RETURN m.interes