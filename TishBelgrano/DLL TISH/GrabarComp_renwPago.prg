PROCEDURE GrabarComp_renwPago
  SELECT comp_rew
  APPEND BLANK
  REPLACE	comp_rew.grupo_comp WITH AuxGrupoCompWeb,;
		comp_rew.nro_comp   WITH AuxNroComp,;
		comp_rew.reng_comp  WITH AuxCantReng,;
		comp_rew.tipo_imp   WITH P_ArchMovSelec.tipoimponible,;
		comp_rew.nro_imp    WITH P_ArchMovSelec.nroimponible,;
		comp_rew.recurso    WITH P_ArchMovSelec.recurso,;
		comp_rew.anio       WITH P_ArchMovSelec.anio,;
		comp_rew.cuota      WITH P_ArchMovSelec.cuota,;
		comp_rew.nro_mov    WITH P_ArchMovSelec.nromov,;
		comp_rew.conc_cc    WITH rec_cc.conc_cc,;
		comp_rew.nro_plan   WITH rec_cc.nro_plan,;
		comp_rew.imp_reng   WITH m.imp_ori,;
		comp_rew.actualiza1 WITH m.actualiz,;
		comp_rew.intereses1 WITH m.interes,;
		comp_rew.imp1_iva1  WITH 0,;
		comp_rew.imp1_iva2  WITH 0,;
		comp_rew.actualiza2 WITH 0,;
		comp_rew.intereses2 WITH 0,;
		comp_rew.imp2_iva1  WITH 0,;
		comp_rew.imp2_iva2  WITH 0,;
		comp_rew.actualiza3 WITH 0,;
		comp_rew.intereses3 WITH 0,;
		comp_rew.imp3_iva1  WITH 0,;
		comp_rew.imp3_iva2  WITH 0,;
		comp_rew.debita_cc  WITH 'S',;
		comp_rew.venc_conc  WITH 0
ENDPROC