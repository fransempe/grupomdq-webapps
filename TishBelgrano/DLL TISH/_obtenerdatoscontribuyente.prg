PROCEDURE _ObtenerDatosContribuyente(pNroComercio as String) as String

	PRIVATE mXML_AUX as String
		PRIVATE mResultado as String
		PRIVATE mIndice as String
		
	 	 
	 	 
	 	* Seteo parametros de fox y leo donde estan los recursos tanto de sistema como de base de datos 
	 	mResultados = ''
		IF (AmbienteOK)
					 
			EfimuniWeb()
							
				 				
			* Abro las tablas que voy a utilizar
			=UseT('recur\comercio')
			SET ORDER TO primario IN comercio
			SELECT comercio
			SCATTER MEMVAR BLANK
			 
		 	mIndice = PADL(pNroComercio, 10, ' ')
		
		 	
			mXML_AUX = '<?xml version="1.0" encoding="UTF-8"?>' + _S_
		 	mXML_AUX = mXML_AUX + Indent(1) + '<DATOS>' + _S_
		 	
		 	
		 	* Obtengo los datos 
		 	IF SEEK(mIndice)
				
				IF (EMPTY(comercio.fec_baja))
					
					mXML_AUX = mXML_AUX + Indent(2) + '<COMERCIO_NRO>' + ALLTRIM(STR(comercio.nro_com,10,0)) + '</COMERCIO_NRO>' + _S_
					mXML_AUX = mXML_AUX + Indent(2) + '<COMERCIO_NOMBRE>' + (ALLTRIM(comercio.nomb_com) + ' - ' + ALLTRIM(comercio.nomb_fan)) + '</COMERCIO_NOMBRE>' + _S_
					mXML_AUX = mXML_AUX + Indent(2) + '<CONTRIBUYENTE_NOMBRE>' + ALLTRIM('nombre') + '</CONTRIBUYENTE_NOMBRE>' + _S_
					mXML_AUX = mXML_AUX + Indent(2) + '<CONTRIBUYENTE_CUIT>' + ALLTRIM('cuit') + '</CONTRIBUYENTE_CUIT>' + _S_
					mXML_AUX = mXML_AUX + Indent(2) + '<CONTRIBUYENTE_DIRECCION>' + ALLTRIM('direccion') + '</CONTRIBUYENTE_DIRECCION>' + _S_
					
					mXML_AUX = mXML_AUX + Indent(2) + '<CONTRIBUYENTE_LOCALIDAD>' + ALLTRIM(comercio.nomb_loc) + '</CONTRIBUYENTE_LOCALIDAD>' + _S_
					mXML_AUX = mXML_AUX + Indent(2) + '<CONTRIBUYENTE_COD_LOCALIDAD>' + ALLTRIM('cod_localidad') + '</CONTRIBUYENTE_COD_LOCALIDAD>' + _S_								
					mXML_AUX = mXML_AUX + Indent(2) + '<CONTRIBUYENTE_SIT_IVA>' + ALLTRIM('sit_iva') + '</CONTRIBUYENTE_SIT_IVA>' + _S_								
			
				ELSE
				
					mXML_AUX = mXML_AUX + 'Comercio en baja' + _S_
				ENDIF		  
				
			ELSE
				mXML_AUX = mXML_AUX + 'Comercio Inexistente' + _S_			
			ENDIF
			
			mXML_AUX = mXML_AUX + Indent(1) + '</DATOS>' + _S_
		ENDIF
		
		
		 
		RETURN STRCONV(mXML_AUX ,_STRCONV_)
		

ENDPROC
****************************************************************************************************





