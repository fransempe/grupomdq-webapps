

* _ObtainDateOfExpiry:
* This function returns periods with date of expiry bigger than today
PROCEDURE _ObtainDateOfExpiry() as String
	PRIVATE mXML as String
	PRIVATE mXML_AUX as String
	PRIVATE strRecursoSH as String
	
	
	* Obtengo el recurso de Seguridad e higiene
	strRecursoSH = _ObtenerRecursoTISH()
	

	IF (!USED('fec_vtos')) THEN
		=UseT('recur\fec_vtos')
	ENDIF			
	SELECT fec_vtos
	SET ORDER TO Primario IN fec_vtos	
	SET KEY TO RANGE ALLTRIM(strRecursoSH) + STR((YEAR(DATE()) -1), 4, 0), ALLTRIM(strRecursoSH) + STR(YEAR(DATE()), 4, 0) + STR(MONTH(DATE()), 3, 0) IN fec_vtos


	mXML_AUX = '<PERIODS>' + _S_
	GO TOP IN fec_vtos
	DO WHILE NOT EOF('fec_vtos')		

		mXML_AUX = mXML_AUX + '<ROWS_PERIODS>'
		
		mXML_AUX = mXML_AUX + '<ANIO>' + ALLTRIM(STR(fec_vtos.anio)) + '</ANIO>'  
		mXML_AUX = mXML_AUX + '<CUOTA>' + ALLTRIM(STR(fec_vtos.cuota)) + '</CUOTA>'  
		mXML_AUX = mXML_AUX + '<FV_CUOTA1>' + ALLTRIM(DTOC(fec_vtos.fv1_cuota)) + '</FV_CUOTA1>'  
		mXML_AUX = mXML_AUX + '<FV_CUOTA2>' + ALLTRIM(DTOC(fec_vtos.fv2_cuota)) + '</FV_CUOTA2>'  
		mXML_AUX = mXML_AUX + '<FV_PRESENTACION>' + ALLTRIM(DTOC(fec_vtos.fv_p_ddjj)) + '</FV_PRESENTACION>'  
		
		mXML_AUX = mXML_AUX + '</ROWS_PERIODS>'
	
		SKIP IN fec_vtos
	ENDDO
	SET KEY TO ''
	mXML_AUX = mXML_AUX + '</PERIODS>' + _S_ 

	
 		

 	* I add the head-board of the XML and the label contenedora
	mXML = '<?xml version="1.0" encoding="UTF-8"?>' + _S_ 
	mXML = mXML + '<DATO>' + _S_ + mXML_AUX + _S_ + '</DATO>'
 
	RETURN ALLTRIM(mXML)
ENDPROC