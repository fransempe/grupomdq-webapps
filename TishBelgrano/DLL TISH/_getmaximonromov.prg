PROCEDURE _getMaximoNroMov (p_strRecurso as String, p_numNroComercio as Integer,;
					  		p_numAnio as Integer, p_numCuota as Integer) as Number				   

	PRIVATE numNro_mov_cc as Number
	PRIVATE numNro_mov_ccc as Number
	PRIVATE numNro_mov as Number

		
	IF (!USED('rec_cc')) THEN
		=UseT('recur\rec_cc')
	ENDIF
		
	IF (!USED('rec_ccc')) THEN
		=UseT('recur\rec_ccc')
	ENDIF
	
	
	* Asigno valores por defecto
	numNro_mov_cc 	= 0
	numNro_mov_ccc 	= 0
	numNro_mov 		= 0


	* Obtengo el m�ximo n�mero de movimiento en rec_cc
	SELECT rec_cc
	SET ORDER TO Primario IN rec_cc
	IF (SEEK(ALLTRIM(p_strRecurso) + 'C' + STR(p_numNroComercio,10,0) + STR(p_numAnio, 4, 0) + STR(p_numCuota, 3, 0), 'rec_cc')) THEN
		numNro_mov_cc = rec_cc.Nro_mov	
	ENDIF
	

	* Obtengo el m�ximo n�mero de movimiento en rec_ccc
	SELECT rec_ccc
	SET ORDER TO Primario IN rec_ccc
	IF (SEEK(ALLTRIM(p_strRecurso) + 'C' + STR(p_numNroComercio,10,0) + STR(p_numAnio, 4, 0) + STR(p_numCuota, 3, 0), 'rec_ccc')) THEN
		numNro_mov_ccc = rec_ccc.Nro_mov	
	ENDIF


	
	* Devuelvo el n�mero de movimiento mayor m�s uno entre la rec_cc y la rec_ccc
	IF (numNro_mov_cc <> 0) OR (numNro_mov_ccc <> 0) THEN
		IF (numNro_mov_cc > numNro_mov_ccc) THEN
			numNro_mov = (numNro_mov_cc + 1)
		ELSE
			numNro_mov = (numNro_mov_ccc + 1)
		ENDIF 
	ELSE
		numNro_mov = 1
	ENDIF
	
	RETURN numNro_mov	
ENDPROC