﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace entidades {
    [XmlRootAttribute("com_ddjj", Namespace = "", IsNullable = false)]
    public class com_ddjj {           
        private string strNroComercio;
        private string strAnio;
        private string strCuota;
        private string strEmpleados;
        private double dblMontoDeclarado;
        private rubro ObjRubro;


        public string nroComercio {
            get { return this.strNroComercio.Trim(); }
            set { this.strNroComercio = value.Trim(); }
        }
        
        public string anio {
            get { return this.strAnio.Trim(); }
            set { this.strAnio = value.Trim(); }
        }
        
        public string cuota {
            get { return this.strCuota.Trim(); }
            set { this.strCuota = value.Trim(); }
        }

        public string empleados {
            get { return this.strEmpleados.Trim(); }
            set { this.strEmpleados = value.Trim(); }
        }

        public double montoDeclarado {
            get { return this.dblMontoDeclarado; }
            set { this.dblMontoDeclarado = value; }
        }
        
        public rubro rubro {
            get { return this.ObjRubro; }
            set { this.ObjRubro = value; }
        }

    }
}
