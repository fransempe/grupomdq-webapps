﻿Option Explicit On
Option Strict On


Public Class clsEncriptar

#Region "Variables"
    Private mCadena As String
    Private mCadena_WS As String

    Private mCredencial_WEB1 As String
    Private mCredencial_WEB2 As String
#End Region


#Region "Propertys"

    Public WriteOnly Property Cadena() As String
        Set(ByVal value As String)
            Me.mCadena = value.ToString.Trim
        End Set
    End Property


    Public WriteOnly Property Cadena_WS() As String
        Set(ByVal value As String)
            Me.mCadena_WS = value.ToString.Trim
        End Set
    End Property

    Public ReadOnly Property Credencial_WEB1() As String
        Get
            Return mCredencial_WEB1.ToString.Trim
        End Get
    End Property

    Public ReadOnly Property Credencial_WEB2() As String
        Get
            Return mCredencial_WEB2.ToString.Trim
        End Get
    End Property


#End Region

#Region "Contructores"
    Public Sub New()
        Me.mCadena = ""
        Me.mCadena_WS = ""

        Me.mCredencial_WEB1 = ""
        Me.mCredencial_WEB2 = ""
    End Sub

    Protected Overrides Sub Finalize()
        Me.mCadena = ""
        Me.mCadena_WS = ""

        MyBase.Finalize()
    End Sub
#End Region


#Region "Funciones Privadas"

#End Region


#Region "Funciones Publicas"

    'Esta funcion encripta una cadena 
    Public Function Encriptar() As String
        Dim mClave As String
        Dim mPass2 As String
        Dim mCAR As String
        Dim mCodigo As String
        Dim i As Integer

        Try

            mClave = "m1d2q3"
            mPass2 = ""
            For i = 1 To Len(Me.mCadena)
                mCAR = Mid(Me.mCadena, i, 1)
                mCodigo = Mid(mClave, ((i - 1) Mod Len(mClave)) + 1, 1)
                mPass2 = mPass2 & Microsoft.VisualBasic.Strings.Right("0" & Hex(Asc(mCodigo) Xor Asc(mCAR)), 2)
            Next i


            Return mPass2.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function

    'Esta funcion desencripta una cadena 
    Public Function DesEncriptar() As String
        Dim mClave As String
        Dim mPass2 As String
        Dim mCAR As String
        Dim mCodigo As String
        Dim i As Integer
        Dim j As Integer

        Try

            mClave = "m1d2q3"
            mPass2 = ""
            j = 1
            For i = 1 To Len(Me.mCadena) Step 2
                mCAR = Mid(Me.mCadena, i, 2)
                mCodigo = Mid(mClave, ((j - 1) Mod Len(mClave)) + 1, 1)
                mPass2 = mPass2 & Chr(CInt(Asc(mCodigo) Xor CLng(Val("&h" + mCAR))))
                j = j + 1
            Next i

            Return mPass2.ToString

        Catch ex As Exception
            Return ""
        End Try
    End Function


    'Obtengo las dos credenciales para comunicarme con el Web Services
    Public Function ObtenerCredenciales() As Boolean
        Dim mOK As Boolean
        Dim mCredencial_AUX As String


        Try

            mOK = False

            'Obtengo la Credencial 1
            mCredencial_AUX = System.Configuration.ConfigurationManager.AppSettings("Credencial1").ToString.Trim
            Me.mCadena = mCredencial_AUX.ToString.Trim
            Me.mCredencial_WEB1 = DesEncriptar()


            'Obtengo la credencial 2
            mCredencial_AUX = System.Configuration.ConfigurationManager.AppSettings("Credencial2").ToString.Trim
            Me.mCadena = mCredencial_AUX.ToString.Trim
            Me.mCredencial_WEB2 = DesEncriptar()


            mOK = True
        Catch ex As Exception
            Me.mCredencial_WEB1 = ""
            Me.mCredencial_WEB2 = ""
            mOK = False
        End Try

        Return mOK
    End Function

    'Valido la Credencial que nos da el Web Services
    Public Function ValidarCredencialWebServices() As Boolean
        Dim mCredencial_OK As Boolean
        Dim mCredencial_AUX As String
        Dim mCadena_WS_AUX As String


        Try

            mCredencial_OK = False
            If (Me.mCadena_WS.ToString.Trim <> "") Then


                'Obtengo la credencial del Web Services
                Me.Cadena = System.Configuration.ConfigurationManager.AppSettings("Credencial3").ToString.Trim


                'Desencripto la Credencial del Web Services
                mCredencial_AUX = ""
                mCredencial_AUX = DesEncriptar()



                'Obtengo la Credencial
                mCadena_WS_AUX = Me.mCadena_WS.ToString.Trim.Substring(17, 8)
                mCadena_WS_AUX = mCadena_WS_AUX.ToString.Trim & Me.mCadena_WS.ToString.Trim.Substring(42, 8)
                mCadena_WS_AUX = mCadena_WS_AUX.ToString.Trim & Me.mCadena_WS.ToString.Trim.Substring(8, 8)
                mCadena_WS_AUX = mCadena_WS_AUX.ToString.Trim & Me.mCadena_WS.ToString.Trim.Substring(33, 8)
                mCadena_WS_AUX = mCadena_WS_AUX.ToString.Trim.ToLower



                'Verifico que las dos credenciales sean iguales
                If (mCredencial_AUX.ToString.Trim = mCadena_WS_AUX.ToString.Trim) Then
                    mCredencial_OK = True
                End If
            End If


        Catch ex As Exception
            mCredencial_OK = False
        End Try

        Return mCredencial_OK
    End Function

    'EncriptarMD5:
    'This function returns the result of MD5
    Public Function EncriptarMD5(ByVal mCadena As String) As String
        Dim mSha As System.Security.Cryptography.SHA1CryptoServiceProvider
        Dim mBytes As Byte()
        Dim mArreglo As Byte()

        'Create MD5
        mSha = New System.Security.Cryptography.SHA1CryptoServiceProvider
        mBytes = System.Text.Encoding.UTF8.GetBytes(mCadena)
        mArreglo = mSha.ComputeHash(mBytes)


        Return Convert.ToBase64String(mArreglo)
    End Function

#End Region


End Class
