﻿Imports System.Web
Imports System.Web.Services

Public Class handler_getPeriodos
    Implements System.Web.IHttpHandler

    Private mWS As ws_tish.Service1

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim strYear As String
        Dim strFechas As String
        Dim strHtml As String
        Dim mResponse As String


        Try

            context.Response.ContentType = "text/plain"


            'I obtain the variables
            strYear = context.Request.Form("year")
            strFechas = context.Request.Form("fechas")


            strHtml = ""
            If (strFechas.Trim() = "N") Then
                strHtml = Me.createHTML(CInt(strYear))
            Else
                strHtml = Me.createHTML(CInt(strYear))
            End If


            'create the response
            mResponse = ""
            mResponse = strHtml.ToString().Trim()


        Catch ex As Exception
            mResponse = "<span id='lblmensaje' style='color: Red; font-weight: bold;'>" & _
                            "Error al intentar recuperar los datos." & _
                        "</span>"
        End Try

        context.Response.Write(mResponse.ToString.Trim)
    End Sub

    Private Function createHTML(ByVal p_intYear As Integer) As String
        Dim dsData As DataSet = Nothing        
        Dim strPeriodos As String = ""


        Try
            'I obtain the data of the trade
            dsData = Me.ObtainList_DateExpiry(p_intYear)

            'I assign the data        
            If (clsTools.HasData(dsData)) Then

                'Creo los renglones de la tabla
                For i = 0 To dsData.Tables(0).Rows.Count - 1
                    strPeriodos += dsData.Tables(0).Rows(i).Item("ANIO").ToString().Trim() & " - " & _
                                   dsData.Tables(0).Rows(i).Item("CUOTA").ToString.Trim & "|"
                Next
            End If

        Catch ex As Exception            
        End Try

        Return strPeriodos.Trim()
    End Function


    Private Function createHtmlFechas(ByVal p_intYear As Integer) As String
        Dim dsData As DataSet = Nothing
        Dim strPeriodos As String = ""


        Try
            'I obtain the data of the trade
            dsData = Me.ObtainList_DateExpiry(p_intYear)

            'I assign the data        
            If (clsTools.HasData(dsData)) Then

                'Creo los renglones de la tabla
                For i = 0 To dsData.Tables(0).Rows.Count - 1
                    strPeriodos += dsData.Tables(0).Rows(i).Item("ANIO").ToString().Trim() & " - " & _
                                   dsData.Tables(0).Rows(i).Item("CUOTA").ToString.Trim & "|"
                Next
            End If

        Catch ex As Exception
        End Try

        Return strPeriodos.Trim()
    End Function

    Private Function ObtainList_DateExpiry(ByVal pYear As Integer) As DataSet
        Dim mXMLData As String
        Dim dsData As DataSet

        Try

            'I obtain the data of the trade
            Me.mWS = New ws_tish.Service1
            mXMLData = Me.mWS.ObtainList_DateExpiry(pYear)
            Me.mWS = Nothing


            dsData = New DataSet
            dsData = clsTools.ObtainDataXML(mXMLData.Trim)

        Catch ex As Exception
            dsData = Nothing
        End Try

        Return dsData
    End Function


    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class