﻿Imports System.Web
Imports System.Web.Services

Public Class handler_logo
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Dim mHTML As String
        Dim mImage_Logo As String



        Try
            mImage_Logo = ""
            context.Response.ContentType = "text/plain"
            mImage_Logo = System.Configuration.ConfigurationManager.AppSettings("image_logo").ToString.Trim()
            mHTML = "<img  class='image_logo' alt='' src='../imagenes/" & mImage_Logo.Trim & "' />"


        Catch ex As Exception
            mHTML = "<img  class='image_logo' alt='' src='' />"
        End Try


        context.Response.Write(mHTML.Trim)
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class