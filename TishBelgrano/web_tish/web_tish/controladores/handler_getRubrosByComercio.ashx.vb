﻿Imports System.Web
Imports System.Web.Services

Public Class handler_getRubrosByComercio
    Implements System.Web.IHttpHandler

#Region "Variables"
    Private mWS As ws_tish.Service1
#End Region


    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest        
        Dim strNroComercio As String = ""
        Dim strResponse As String = ""

        Try
            context.Response.ContentType = "text/plain"

            strNroComercio = CLng(context.Request.Form("comercio"))
            strResponse = Me.getRubrosByComercio(strNroComercio.Trim)
        Catch ex As Exception
            strResponse = ""
        End Try

        context.Response.Write(strResponse.Trim)
    End Sub

    Private Function getRubrosByComercio(ByVal p_strComercio As String) As String
        Dim rubro As New List(Of entidades.rubro)
        Dim strXML As String = ""
        Dim strRubros As String = ""

        Try
            Me.mWS = New ws_tish.Service1
            strXML = Me.mWS.getRubrosByComercio(p_strComercio)
            Me.mWS = Nothing

            rubro = entidades.serializacion.deserializarXML(Of List(Of entidades.rubro))(strXML.Trim)
            If (rubro IsNot Nothing) Then
                For Each rubro_aux As entidades.rubro In rubro
                    strRubros += rubro_aux.codigo & "?" & rubro_aux.descripcion & "|"
                Next
            End If

        Catch ex As Exception
            strRubros = ""
        End Try

        Return strRubros.Trim
    End Function



    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class