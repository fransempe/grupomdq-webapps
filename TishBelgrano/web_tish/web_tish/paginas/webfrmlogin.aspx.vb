﻿Option Explicit On
Option Strict On


Partial Public Class webfrmlogin
    Inherits System.Web.UI.Page


#Region "Variables"
    Private mWS As ws_tish.Service1
    Private mXML As String
    Private ObjEncriptar As clsEncriptar
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        If Not (IsPostBack) Then



            'Saber de donde viene
            Dim ip As String
            ip = Request.UserHostAddress




            'Limpio la Session por las dudas que el usuario este logueado y quiera volver a loguearse
            Call Me.LimpiarSession()


            'Obtengo los datos de la municipalidad para mostrarlos en el pie de la pagina y para asignar las variables de session
            Call Me.CargarDatosMunicipalidad()


            Call Me.SloganMuniicpalidad()


            'Asigno eventos javasript
            Me.link_login.Attributes.Add("onclick", "javascript:return validar_datos();")



            Me.txtcuit.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtcuit.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtclave.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtclave.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtcaptcha.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtcaptcha.Attributes.Add("onblur", "javascript:event_focus(this);")


            Me.txtcuit.Attributes.Add("onkeyup", "javascript:key_up_txt(event, 'txtclave');")
            Me.txtclave.Attributes.Add("onkeyup", "javascript:key_up_txt(event, 'txtcaptcha');")
            Me.txtcaptcha.Attributes.Add("onkeyup", "javascript:key_up_txt(event, 'link_login');")



            Me.txtcuit.Focus()
        Else
            'Call LimpiarControles()
        End If

        'Teclado virtual
        Me.txtclave.CssClass = "keyboardInputInitiator"
    End Sub


    Private Sub SloganMuniicpalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema TISH."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema TISH."
        End If

    End Sub

    Private Sub CargarDatosMunicipalidad()
        Dim dsDataMunicipalidad As DataSet
        Dim mMunicipalidadName As String


        Try

            'Obtengo los datos         
            Me.mWS = New ws_tish.Service1
            Me.mXML = Me.mWS.ObtainMunicipalidadData()
            Me.mWS = Nothing



            dsDataMunicipalidad = New DataSet
            dsDataMunicipalidad = clsTools.ObtainDataXML(Me.mXML.Trim)


            mMunicipalidadName = ""
            mMunicipalidadName = System.Configuration.ConfigurationManager.AppSettings("Nombre_Municipalidad").ToString.Trim()
            Me.lblnombre_municipalidad.Text = mMunicipalidadName.Trim
            Me.lbltelefono_municipalidad.Text = dsDataMunicipalidad.Tables(0).Rows(0).Item("MUNICIPALIDAD_TELEFONO").ToString.Trim
            Me.lblemail_municipalidad.Text = dsDataMunicipalidad.Tables(0).Rows(0).Item("MUNICIPALIDAD_MAIL").ToString.Trim
            Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim


            'Asigno las variables de session con los datos del municipio                    
            Session("municipalidad_nombre") = Me.lblnombre_municipalidad.Text
            Session("municipalidad_telefono") = Me.lbltelefono_municipalidad.Text
            Session("municipalidad_mail") = Me.lblemail_municipalidad.Text

        Catch ex As Exception
            Me.mWS = Nothing
        End Try
    End Sub



    Private Sub GenerarMensaje(ByVal mMensaje As String, ByVal EsCaptcha As Boolean)
        Me.lblmensaje.Visible = True
        Me.div_mensaje.Visible = True
        Me.lblmensaje.Text = mMensaje.ToString.Trim


        'Seteo colores al control txtcaptcha
        If (EsCaptcha) Then
            Me.txtcaptcha.BackColor = Drawing.Color.Red
            Me.txtcaptcha.ForeColor = Drawing.Color.White
        Else
            Me.txtcaptcha.BackColor = Drawing.Color.White
            Me.txtcaptcha.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff009900")
        End If
    End Sub


    'Si los datos son Invalidos borro cualquier Session
    Private Sub LimpiarSession()
        Session.RemoveAll()
        Session.Clear()
        Session.Abandon()
    End Sub

    Protected Sub link_login_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_login.Click
        Dim mLogin_OK As String
        Dim mLogin_OK_AUX() As String
        'Dim mComercio_XML As String
        'Dim dsComercio As DataSet



        Try
            If (Me.Validar()) Then


                'Limpio la Session por las dudas que el usuario este logueado y quiera volver a loguearse
                'Call Me.LimpiarSession()



                'Realizo el Logueo
                mLogin_OK = ""
                Me.mWS = New ws_tish.Service1
                mLogin_OK = Me.mWS.Login(txtcuit.Text.Trim, txtclave.Text.Trim)
                Me.mWS = Nothing


                'Obtengo los datos del Login
                mLogin_OK_AUX = mLogin_OK.ToString.Trim.Split(CChar("|"))


                'Si el Logueo no es valido le doy un aviso al usuario
                If (mLogin_OK_AUX(0).ToString.Trim < "1") Then
                    Call Me.GenerarMensaje("Usuario no registrado.", False)
                    Call Me.LimpiarControles()
                    Exit Sub
                End If


                'Si el Logueo no es valido le doy un aviso al usuario
                If (mLogin_OK_AUX(2).ToString.Trim <> "") Then
                    Call Me.GenerarMensaje("Usuario dado de baja, para regularizar su situación diríjase al palacio municipal.", False)
                    Call Me.LimpiarControles()
                    Exit Sub
                End If




                'Asigno Variables de Session
                Session("login") = True


                'Obtengo los datos de la municipalidad para mostrarlos en el pie de la pagina y para asignar las variables de session
                Call Me.CargarDatosMunicipalidad()



                'Si el Login me devuelve una "A", es porque el usuario tiene la clave por defecto y tiene que cambiarla por una propia
                'Session("tipo_clave") = mLogin_OK_AUX(1).ToString.Trim()
                Session("cuit_manager") = mLogin_OK_AUX(0).ToString.Trim()
                If (mLogin_OK_AUX(1).ToString.Trim = "A") Then
                    Session("own_key") = True
                    Response.Redirect("webfrmcambiar_clave.aspx", False)
                Else
                    Session("own_key") = False
                    Response.Redirect("webfrmseleccionar_comercio.aspx", False)
                End If


            Else
                Me.txtclave.ControlStyle.CssClass = "keyboardInputCenter"
            End If


        Catch ex As Exception
            'mLog = New Clslog
            'mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            'mLog = Nothing
            Response.Redirect("webfrmerrorweb.aspx", False)
        End Try

    End Sub

    Private Function Validar() As Boolean


        'CUIT
        If (Me.txtcuit.Text.Trim.Length <> 13) Then
            Return False
        End If


        'Clave
        If (Me.txtclave.Text.Trim = "") Then
            Return False
        End If


        'Validacion CAPTCHA
        Me.verificador_captcha.ValidateCaptcha(txtcaptcha.Text.Trim())
        If Not (Me.verificador_captcha.UserValidated) Then

            Call Me.GenerarMensaje("Código de Verificación Incorrecto.", True)
            Me.txtcaptcha.Text = ""
            Me.txtcaptcha.Focus()

            Return False
        End If


        Return True
    End Function


    'This procedure cleans then controls
    Private Sub LimpiarControles()
        Me.txtcuit.Text = ""
        Me.txtclave.Text = ""
        Me.txtcaptcha.Text = ""
        Me.txtcuit.Focus()
    End Sub


End Class