﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmcambiar_clave.aspx.vb" Inherits="web_tish.webfrmcambiar_clave" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
    
        
        <!-- Seteo the resources -->
        <script src="../js/funciones.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../css/estilos.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_botones.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_change_key.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_carteles.css" type="text/css" />
        <%           
            'Seteo el css que voy a utilizar
            Dim mPath_css As String                
            mPath_css = ""
            mPath_css = System.Configuration.ConfigurationManager.AppSettings("css").ToString.Trim()
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_genericos.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_botones.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_change_key.css' type='text/css' />")
        %>    

       
        
        
        <!-- ajax -->       
        <script type="text/javascript" language="javascript" src="../js/jquery1-2-6-min.js"></script>
        <script type="text/javascript" language="javascript">
        
            jQuery.noConflict();
            function check_key(puser, pkey){          
              
                var _codehtml = '<table border="0" width="40%" aling="center">' +
                                    '<tr>' + 
                                        '<td align="center"><img alt="" src="../imagenes/loading.gif"/ width="24px" height="24px" /></td>' + 
                                        '<td><p>Analizando clave ...</p></td>' +                                      
                                    '</tr>' +
                                '</table>';
             
             
                var _control = document.getElementById('mensaje');                
                _control.innerHTML = _codehtml;
            
                                  
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_change_key.ashx",                    
                    data:"user=" + puser + "&key=" + pkey, 
                    success: ver_respuesta
                });
            }
            
            
            function ver_respuesta(html){            
                if (html == 'OK') {
                    __doPostBack("change_key",'')
                } else {
                    var _control = document.getElementById('mensaje');
                    _control.innerHTML = '<div id="div_messenger" class="Globo GlbRed" style="width:70%;">' +
                                            '<p id="lblmessenger">' +
                                                'La clave ingresada es incorrecta.' +
                                            '</p>' +
                                        '</div>'
                    
                    
                    return false;
                }
                
            return true;    
            }            
            
            
            
            /* Carga del logo de la municipalidad */
            function ajax_load_logo(){   
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_logo.ashx",                                                            
                    success: see_response_logo
                });
            }
            
            
            function see_response_logo(html){                   
                if (html != '') {
                    var _control = document.getElementById('div_header');                   
                    _control.innerHTML = html;                   
                    return false;
                }
                
            return true;    
            }        
            
            
        </script>
        
           
        
         <!-- Virtual keyboard -->       
        <script type="text/javascript" src="../keyboard/keyboard.js" charset="UTF-8"></script>
        <link rel="stylesheet" href="../keyboard/keyboard.css" type="text/css" />
        
              
               
       <!-- function validator data -->
       <script type="text/javascript">
             function validate_data() {             
          
                /* string empty */
                if (control_vacio(window.document.getElementById('txtclave_anterior'),'Debe Ingresar su clave actual')) {          
                    return false;
                }

                /* Validate string */
                if (!validar_cadena(window.document.getElementById('txtclave_anterior'),'Clave actual')) {          
                    return false;
                }
                
                
                /* string empty */
                if (control_vacio(window.document.getElementById('txtclave_nueva'),'Debe Ingresar su nueva clave')) {          
                    return false;
                }

                /* Validate string */
                if (!validar_cadena(window.document.getElementById('txtclave_nueva'),'Clave nueva')) {          
                    return false;
                }
                
                
                
                /* string empty */
                if (control_vacio(window.document.getElementById('txtconfirmar_clave'),'Debe confirmar su nueva clave')) {          
                    return false;
                }

                /* Validate string */
                if (!validar_cadena(window.document.getElementById('txtconfirmar_clave'),'Confirmar nueva clave')) {          
                    return false;
                }
             
             
             
                /* Length key */
                if (window.document.getElementById('txtclave_nueva').value.length < 6) {
                    alert('Nueva Clave:\nSu nueva clave no puede tener menos de 6 caracteres');
                    return false;
                }
                
                
             
                /* Confirm key */
                var key_new = ''
                var key_confirm = ''
                
                key_new = window.document.getElementById('txtclave_nueva').value;
                key_confirm = window.document.getElementById('txtconfirmar_clave').value;
                
                if (key_new != key_confirm) {
                    alert('La nueva clave es distinta a la clave de confirmación');
                    return false;
                }
             
             
             
                /* check the login with ajax */
                var _check_key = false;    
                _check_key = check_key(window.document.getElementById('lblcuit_manager').innerHTML, window.document.getElementById('txtclave_anterior').value);                                               
                if (_check_key)  {
                    return false;
                }
               
               return false;              
             //return true
             }
       </script>
               
               
               
               
        <!-- function to calculator the level of security of the key -->       
        <script type="text/javascript" language="javascript">
        
            var v_numbers = "0123456789";
            var v_words = "abcdefghyjklmnñopqrstuvwxyz";
            var v_words_upper = "ABCDEFGHYJKLMNÑOPQRSTUVWXYZ";


            /* seek if the string has numbers */
            function has_numbers(pkey_aux){
               for(i=0; i<pkey_aux.length; i++){
                  if (v_numbers.indexOf(pkey_aux.charAt(i),0)!=-1){
                     return 1;
                  }
               }
               return 0;
            } 


            /* seek if the string has words */
            function has_words(pkey_aux){
               pkey_aux = pkey_aux.toLowerCase();
               for(i=0; i<pkey_aux.length; i++){
                  if (v_words.indexOf(pkey_aux.charAt(i),0)!=-1){
                     return 1;
                  }
               }
               return 0;
            } 


            /* seek if the string has lower words */
            function has_words_lower(pkey_aux){
               for(i=0; i<pkey_aux.length; i++){
                  if (v_words.indexOf(pkey_aux.charAt(i),0)!=-1){
                     return 1;
                  }
               }
               return 0;
            } 


            /* seek if the string has upper words */
            function has_words_upper(pkey_aux){
               for(i=0; i<pkey_aux.length; i++){
                  if (v_words_upper.indexOf(pkey_aux.charAt(i),0)!=-1){
                     return 1;
                  }
               }
               return 0;
            } 


        
        
        
        
        
        
            /* this function calculate the level of security of the key entered */
            function level_security_key(pkey) {
            var v_level_security = 0;
                
               if (pkey.length!=0){
                    if (has_numbers(pkey) && has_words(pkey)){
                        v_level_security += 30;
                    }
                    
                if (has_words_lower(pkey) && has_words_upper(pkey)){
                    v_level_security += 30;
                }
                
                
                if (pkey.length >= 6 && pkey.length <= 7){
                    v_level_security += 10;
                } else {
                    if (pkey.length >= 7 && pkey.length <= 8){
                        v_level_security += 30;
                    } else {
                        if (pkey.length > 8){
                            v_level_security += 40;
                        }
                    }
                  }
               }
               
            return v_level_security            
            }    
            
            
            
            
           /* this function assign the value of security of the key and assign the colour according the porcentage */ 
           function calculate_level_security(pkey){
                var v_level_security = 0;
                
                /* I assign the level of security */
	            v_level_security = level_security_key(pkey);
	            window.document.getElementById('lbllevel_key').innerHTML = v_level_security + "%";	            


                /* I assign the color according to the security level */
                if (v_level_security <= 30) {
                    window.document.getElementById('lbllevel_key').style.color = "#FF0000";        
                } else {
                    if ((v_level_security > 30) && (v_level_security <= 60)) {
                        window.document.getElementById('lbllevel_key').style.color = "#FF9900";
                    } else {
                        window.document.getElementById('lbllevel_key').style.color = "#006600";        
                    }
                }

            }
            
        </script>               
               
               
               
               
               
               
        
        
        
        <!-- Diferent functions -->
        <script type="text/javascript">

            /* cerrar_session: */
            /* Esta funcion genera el mensaje al usuario de que esta a punto de cerrar la session de usuario */
            function cerrar_session() {
                return confirm("Usted esta a punto de cerrar su sesión\n¿Desea continuar?")              
            }

    
            /* cantidad_registros: */
            /* Esta Funcion CUENTA la CANTIDAD de COMPROBANTES por GRILLA */                
            function cantidad_registros(id_grilla){
                var grilla = document.getElementById(id_grilla);
                if (grilla != null) {
                    return (document.getElementById(id_grilla).rows.length -1);
                } else {
                    return 0;
                }
            }
        
            
         	        
	        
	        
	        /* key_state_div: */
	        /* This function shows or hides the div with the advice to generate a key */
	        function key_state_div() {
	        	                                                                           
	            if (window.document.getElementById('p_label_key').innerHTML == 'Conozca algunos consejos para crear una clave segura (VER)') {
	                window.document.getElementById('p_label_key').innerHTML = 'Conozca algunos consejos para crear una clave segura (OCULTAR)';
	                window.document.getElementById('div_key').style.display = 'block';
	                window.document.getElementById('tabla_clave').style.display = 'none';
	                window.document.getElementById('div_menssage_count_characters_key').style.display = 'none';
	                
	                
	            } else {	                                                                           	                                                                           
	                window.document.getElementById('p_label_key').innerHTML = 'Conozca algunos consejos para crear una clave segura (OCULTAR)'
	                window.document.getElementById('p_label_key').innerHTML = 'Conozca algunos consejos para crear una clave segura (VER)'
	                window.document.getElementById('div_menssage_count_characters_key').style.display = 'block';
	                window.document.getElementById('tabla_clave').style.display = 'block';
	                window.document.getElementById('div_key').style.display = 'none';
	                                
	            }
	        
	        }
	        
	        
	        
	        
	        function quantity_characters(txt_name, lbl_name) {
	            var count_characters = '0';
	            var key_aux = '';
	            
	                     
	            count_characters = window.document.getElementById(txt_name).value;
	            window.document.getElementById(lbl_name).innerHTML = '(' + count_characters.length + ') ';
	            
	            
	            if (txt_name = 'txtclave_nueva') {
	                key_aux = window.document.getElementById(txt_name).value;
	                calculate_level_security(key_aux);
	            }	        
	        }
	        
        </script>
        
        
        
        <!-- recursos para crear la ventana modal -->
	    <link rel="stylesheet" href="../modal/Assets/LightFace.css" />	
	    <script src="../modal/mootools.js" type="text/javascript"></script>
	    <script src="../modal/mootools-more-drag.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.IFrame.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Image.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Request.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Static.js" type="text/javascript"></script>

	
	    
        <!-- Creo la ventana modal -->
        <script type="text/javascript">    	
	 	    function key_tips() {	
	 	        var _html;
	 	        
	 	        _html = '<div id="div2" align="left"  style="width:100%" class="accContent">' + 
                            '<ul class="sidemenu">' + 
                                '<li>No debe contener su nombre.</li>' + 
                                '<li>No debe ser una palabra común del diccionario.</li>' + 
                                '<li>Debe contener uno o más números.</li>' +               
                                '<li>Debe tener letras mayúsculas y minúsculas.</li>' + 
                                '<li>Deben de ser más de 6 caracteres.</li>' +  					
                                '<li>Debe ser diferente a claves anteriores.</li>' + 
	                        '</ul>' + 
	                    '</div>';
	 	    	    	
	 	    	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema TISH :: Tips', 
				    width: 400,
				    height: 120,
	  			    content: _html,
	 			    buttons: [					
					    {
						    title: 'Cerrar',
						    event: function() { this.close(); }
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }        
	 	    
	 	    
	 	    function logout() {	 	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema TISH :: Cerrar sesión', 
				    width: 250,
				    height: 50,
	  			    content: '<div align="left">Usted esta a punto de cerrar su sesión.\n¿Desea continuar?</div>',
	 			    buttons: [		
	 			        {
						    title: 'Aceptar',
						    event: function() { this.close(); window.location = "webfrmlogin.aspx"; }
					    },
					    {
						    title: 'Cerrar',
						    event: function() { this.close();}
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }        
	 	    
	 	    
	 	    function change_trade() {	 	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema TISH :: Cambiar comercio', 
				    width: 300,
				    height: 50,
	  			    content: 'Usted esta a punto de cambiar de comercio\n¿Desea continuar?',
	 			    buttons: [		
	 			        {
						    title: 'Aceptar',
						    event: function() { this.close();window.location= "webfrmseleccionar_comercio.aspx";}
					    },
					    {
						    title: 'Cerrar',
						    event: function() { this.close();}
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }        


        </script>

        
        
        
        <!-- title of the page -->
        <title>Sistema TISH :: Cambiar clave</title>
        <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
    	
    </head>




    <body onload="efecto();">
        
    
        <!-- wrap starts here -->	
        <div id="wrap">
         
		    <div id="header">
		    
		        <!-- div container font -->
                <div id="container_font">			        
		            <div  style="width:5%; float:left;  padding:0px 5px 0px 5px;">
                        <a href="#" title="Usar fuente menor" class="link_font" onclick="assign_font('-');">A-</a> 
                    </div>
                    
                    <div  style="width:5%; float:left;">
		                <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 
                    </div>		            
                    
		            <div  style="width:5%; float:left;">
		                <a href="#" title="Usar fuente mayor" class="link_font" onclick="assign_font('+');">A+</a> 
		            </div>	            
                </div>
		    
		        <!--<div id="div_header"  class="div_image_logo">                
 	                <img  class="image_logo" alt="" src="../imagenes/topiz.jpg"  />
                </div>   -->
		    
    			<h1 id="logo-text">Tasa de Inspección por Seguridad e Higiene</h1>			
	    		<h2 id="slogan">
                    <asp:Label ID="lblslogan_municipalidad" runat="server" Text=""></asp:Label></h2>				
		    </div>
	  
	          
                 
        
	        <div id="content-wrap">
	    
	    
	            <!-- div container page title -->
                <div id="div_title" style="background-color:Black;" align ="left" >
                    <strong class="titulo_page">Sistema TISH :: Cambiar clave</strong>
                </div>
	  
	          <!-- div container right -->
                <div id="div_optiones"  class="menu_options">
                
                    <div id="div_comercio" style=" padding-top:20px;">
                        <div align="center">
                            <table style="width:95%">
                                <tr>
                                    <td  rowspan="2"><img id="user" alt="" src="../imagenes/user.png"  style=" width:50px; height:60px; border:none"/></td>
                                    <td><h1 align ="left" style="color:Black">Usuario Web</h1></td>
                                </tr>
                                <tr>                                
                                    <td>
                                        CUIT: <asp:Label ID="lblcuit_manager" runat="server" Font-Bold="True"></asp:Label>                                        
                                    </td>
                                </tr>
                                
                                
                            </table>
                        </div>
                       
                        
                        
                        
                        
                    </div>
                    
                    
                    <h1 id= "h1_accesos"  align ="left" style="color:Black" runat="server">Accesos</h1>                    			
	                <ul id= "ul_accesos" class="sidemenu" runat="server">               
                        <li><a href="webfrmdeclaracionesjuradas.aspx">Ver listado de DDJJ</a></li>
                        <li><a href="webfrmctacte.aspx">Ver cuenta corriente</a></li>
                        <li><a href="webfrmopciones.aspx">Datos del comercio</a></li>				                
                        <li><a href="#" onclick="javascript:change_trade();">Cambiar comercio</a></li>			                        
                    </ul>	
	                
	                <h1 align ="left" style="color:Black">Usuario Web</h1>				
                    <ul class="sidemenu">
	                    <li id="li_misdatos" runat="server"><a href="webfrmdatos_representante.aspx">Mis datos</a></li>				                
		                <li id="li_cambiarclave" runat="server"><a href="webfrmcambiar_clave.aspx">Cambiar clave</a></li>	
		                <li><a href="#" onclick="javascript:logout();">Cerrar sesión</a></li>
	                </ul>	
                </div>
	  
	  
	  		<div id="main"> 
					 
			    <div> 
			        <p id="p_mensaje" runat="server">
                        <strong>ATENCIÓN:</strong>
                        Usted contiene la clave generada automáticamente por el sistema para su seguridad le pedimos que cambie la clave por una propia.    
                    </p>
                    
                </div>			
                
                <form id="formulario" runat="server" style="padding-left:0px;">
                 
                             
    		                            
                    <!-- Div contenedor de la tabla de movimientos -->
                    <div id="div_contenedor" runat="server" style="height:100%;">
                    
                        <div id="mensaje" align="center">
                            <div id="div_mensaje" class="Globo GlbRed" style="width:45%;" runat="server" visible="false">
                                <asp:Label ID="lblmensaje" runat="server" Text="mensaje" Font-Bold="True" ForeColor="Red" Visible="False"></asp:Label>               
                            </div>
                        </div>
   
                       
                       <div class="accContent"> 
                            <div id="div_menssage_count_characters_key" align="center">
                            <p>
                                <strong>Clave:</strong>
                                Tu nueva clave debe contener al menos 6 caracteres
                            </p>
                       </div>
   
    
                            <div id="div_tabla_clave" align="center" style="width:100%; padding-bottom:0px;">
	                             <table id="tabla_clave" border="1" cellspacing="10" style="border-style: double; width: 90%; height: 100%; background-color: #FFFFFF; color: #000000;" align="center" runat="server">
                                    <tr>
                                        <td class="columna_titulo">Clave actual:</td>
                                        <td class="columna_dato">                                            
                                            <asp:TextBox ID="txtclave_anterior" runat="server" CssClass="keyboardInputInitiator" MaxLength="20" TextMode="Password" Width="70%"></asp:TextBox>                                
                                            <asp:Label ID="lblquantity_characters_key_old" runat="server" Text="(0)"></asp:Label>
                                        </td>
                                    </tr> 
                                            
                                    <tr>
                                        <td class="columna_titulo">Clave nueva:</td>                        
                                        <td class="columna_dato">
                                            <asp:TextBox ID="txtclave_nueva" runat="server" CssClass="textbox" MaxLength="20" TextMode="Password" Width="70%"></asp:TextBox>                                
                                            <asp:Label ID="lblquantity_characters_key_new" runat="server" Text="(0)"></asp:Label>
                                        </td>                                                                                                   
                                    </tr>
                            
                            
                                    <tr>                                            
                                        <td colspan="2" align="center">                                                
                                            Nivel de Seguridad: 
                                            <asp:Label ID="lbllevel_key" runat="server" Text="0%" Font-Bold="True" 
                                                Font-Italic="False" Font-Size="Small" Font-Strikeout="False" 
                                                Font-Underline="False" ForeColor="Red"></asp:Label>
                                        </td>                                                                                                   
                                    </tr>
                            
                                    <tr>
                                        <td class="columna_titulo">Confirmar clave:</td>                        
                                        <td class="columna_dato">
                                            <asp:TextBox ID="txtconfirmar_clave" runat="server" CssClass="textbox" MaxLength="20" TextMode="Password" Width="70%"></asp:TextBox>                                
                                            <asp:Label ID="lblquantity_characters_key_confirm" runat="server" Text="(0)"></asp:Label>
                                        </td>                                                                                                   
                                    </tr>
                            
                            
                            
                                    <tr>
                                        <td class="columna_titulo" colspan="2" align="center">     
                                           <div id="div_contenedor_botones" style=" height:100%">                                                                                       
                                                    <div id="div_boton_informe_deuda" style="width:100%"; align="center">
                                                       <asp:Button ID="btncambiar_clave" runat="server"                                                          
                                                            Text="Cambiar clave" BorderStyle="None" 
                                                            CssClass="boton" Height="35px" Width="200px"                                                             
                                                            ToolTip="Con esta opción usted cambiara su clave por una nueva."                                                           
                                                        />
                                                    </div>
                                            </div>  
                                            
                  
                                        </td>           
                                                                                                                                        
                                    </tr>
                                </table>   
                           </div> 
                       </div>                                                                         
                    
                       
                        <div id="key_link_div" align="center" style="width:100%;"  onclick="javascript:key_tips()" class="link">
                            <table border="0" width="65%" style="cursor:pointer;" align="center">
                                <tr>
                                    <td align="center"><img alt="" src="../imagenes/key.gif" / width="24px" height="24px" /></td>
                                    <td><p id="p_label_key" title="Conozca algunos consejos para crear una clave segura">Conozca algunos consejos para crear una clave segura</p></td>                                                
                                </tr>
                                                                        
                            </table>
                         </div>
                            
                        
                       
                       
                    </div> 
                            
                        
                            
                        </form>    
		    </div>
				
				
					
			
							
  		</div> 	
			  
			  
			  
	  		<div id="footer">
		
		        <div id="div_pie_municipalidad" style="width:70%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		        Municipalidad de
		            <strong> 
		                <asp:Label ID="lblnombre_municipalidad" runat="server" Text="NOMBRE_MUNICIPIO"></asp:Label> 
                    </strong> 
                    
				<br />
				Teléfono: 
				    <strong> 
                        <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                    </strong> 
                 | Email: 
				    <strong> 
                        <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                    </strong> 
                
			</div>
		   
		        <div id="div_pie_grupomdq" class="footer_grupomdq">
				Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ S.A</a></strong>  
				<br />
				Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				<a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
				<asp:Label ID="lblversion" runat="server" Text=""></asp:Label>
		   </div>
                  
		    </div>	
		
		
		            		         
		
		
		</div>
		
		



    </body>
</html>