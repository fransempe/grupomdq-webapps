﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmaltaddjj.aspx.vb" Inherits="web_tish.webfrmaltaddjj" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
    
    
     <style type="text/css">
    
    /* base semi-transparente */
    .overlay{
        display: none;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 720px;
        background: #000;
        z-index:1001;
		/*opacity=.75;*/
        -moz-opacity: 0.75;
        /*filter: alpha(opacity=75);*/
    }
	
    /* estilo para lo q este dentro de la ventana modal */
    .modal {
        display: none;
        position: absolute;
        /*
        top: 25%;
        left: 25%;
        width: 50%;
        height: 50%;
        */
        
        left: 50%;
        top: 50%;
        width: 700px;
        height: 350px;
        margin-top: -120px;
        margin-left: -250px;
        
        padding: 0px;
        background: #fff;
		color: #333;
        z-index:1002;
        overflow: auto;        
    }
    
    
    .modal_column_title{ width:50%; text-align:right;}
	.modal_column_data{ width:50%; text-align:left;}
	
    
    .modal_boton {
	    height: 30px;
	    width:90px;
	    background-color:#5b6951;
	    line-height: 35px;
    	
	    margin: 2px 5px 2px 5px;
	    color: #fff;
	    font-size: 1em;
	    font-weight:bold;
	    text-decoration: none;
	    cursor:pointer;
	
    }

    .modal_boton:hover  {
	    color:white;
	    background-color:Gray;	
    }
    
    
    .modal_div_header {
    	background-color:#5b6951;
    	color:White;
    	padding-top:5px;
    	padding-bottom:5px;
    }
    
    
    .modal_div_header_h2 {
    	font-family: 'Trebuchet MS', Arial, sans-serif;
	    font-weight: bold;
	    font-size: 1.3em;
	    text-transform: uppercase;
    }
    
    
    .modal_div_resource {    	
    	padding-top:20px;
    	width:80%;
    }
    
       
</style>
        
        <!-- Variable pública -->
        <script type="text/javascript">
            var intIndiceRubro = 0;            
        </script>
               
               
        <!-- recursos para crear la ventana modal -->
	    <link rel="stylesheet" href="../modal/Assets/LightFace.css" />	
	    <script src="../modal/mootools.js" type="text/javascript"></script>
	    <script src="../modal/mootools-more-drag.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.IFrame.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Image.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Request.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Static.js" type="text/javascript"></script>
        
        <script type="text/javascript">    	    
      	    function logout() {	 	    	 	        
 	            box = new LightFace({ 
 			        title: 'Sistema TISH :: Cerrar sesión', 
			        width: 250,
			        height: 50,
  			        content: '<div align="left">Usted esta a punto de cerrar su sesión.\n¿Desea continuar?</div>',
 			        buttons: [		
 			            {
					        title: 'Aceptar',
					        event: function() { this.close();window.location= "webfrmlogin.aspx";}
				        },
				        {
					        title: 'Cerrar',
					        event: function() { this.close();}
				        }
			        ]
 		        });
 		        box.open();		
 	        }     
 	        
 	        
 	        function change_trade() {	 	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema TISH :: Cambiar comercio', 
				    width: 300,
				    height: 50,
	  			    content: '<div align="left">Usted esta a punto de cambiar de comercio\n¿Desea continuar?</div>',
	 			    buttons: [		
	 			        {
						    title: 'Aceptar',
						    event: function() { this.close();window.location= "webfrmseleccionar_comercio.aspx";}
					    },
					    {
						    title: 'Cerrar',
						    event: function() { this.close();}
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }




	 	    function mensaje_limpiarRubro(p_title, p_width, p_height, p_html, p_indice) {
	 	        box = new LightFace({
	 	            title: 'Sistema TISH :: ' + p_title,
	 	            width: p_width,
	 	            height: p_height,
	 	            content: p_html,
	 	            buttons: [
	 	                 {
	 	                     title: 'Aceptar',
	 	                     event: function() { this.close(); borrarRubro(p_indice); }
	 	                 },
				        {
				            title: 'Cerrar',
				            event: function() { this.close(); }
				        }
			        ]
	 	        });
	 	        box.open();
	 	    }


	 	    function verRubros(p_title, p_width, p_height, p_html) {	 	        
	 	        box = new LightFace({
	 	            title: 'Sistema TISH :: ' + p_title,
	 	            width: p_width,
	 	            height: p_height,
	 	            content: p_html,
	 	            buttons: [
	 	                 {
	 	                     title: 'Aceptar',
	 	                     event: function() {	 	                     
	 	                     this.close();
	 	                         	 	                         
	 	                     cargarRubro(); }
	 	                 },
				        {
				            title: 'Cerrar',
				            event: function() { this.close(); }
				        }
			        ]
	 	        });
	 	        box.open();
	 	    }        


	 	    function modal_window(p_title, p_width, p_height, p_html) {	 	        
 	            box = new LightFace({
 	                title: 'Sistema TISH :: ' + p_title,
 	                width: p_width,
 	                height: p_height,
 	                content: p_html,
 	                buttons: [
				        {
				            title: 'Cerrar',
				            event: function() { this.close(); }
				        }
			        ]
 	            });
	 	        box.open();	 	        
	 	    }        
            </script>
               
               
        <!-- Seteo the resources -->
        <script src="../js/funciones.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../css/estilos_listado.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_botones.css" type="text/css" />        
        <%           
            'Seteo el css que voy a utilizar
            Dim mPath_css As String                
            mPath_css = ""
            mPath_css = System.Configuration.ConfigurationManager.AppSettings("css").ToString.Trim()
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_listado.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_genericos.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_botones.css' type='text/css' />")
        %>                   
               
               
        
         
         
         
        <!-- ajax -->       
        <script type="text/javascript" language="javascript" src="../js/jquery1-2-6-min.js"></script>
        <script type="text/javascript" language="javascript">
        
            
            
            /* verifico que no exista DDJJ */
            function ajax_exist_ddjj(pdeclaration_and_voucher){   
                var _comercio;
                var _period;
                var _message;


                _comercio = window.document.getElementById('lblcomercio_user').innerHTML;
                control_combo = window.document.getElementById('cmbperiod');
                index = control_combo.selectedIndex;
                _period = control_combo.options[index].text;
            
                
                 jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_ddjj.ashx",        
                    data:"action=exist&comercio=" + _comercio + "&period="+_period,
                    success: function(data) {
                        if (data == 'NO') {
                            declaration(pdeclaration_and_voucher);                            
                        } else {
                        
                            message = "<div align='left'>" +
                                            "Usted ya realizó la declaración jurada correspondiente al período seleccionado.<br/>" +
                                            "Nuestro sistema Web aún no cuenta con la posibilidad de realizar rectificativas (modificaciones en su declaración jurada), " +
                                            "para poder llevar a cabo esta operación, usted deberá dirigirse al palacio municipal.<br/>" +                                            
                                            "<br/>" +
                                            "Sistema TISH :: Volver al <a href='webfrmdeclaracionesjuradas.aspx' class='link'>listado de declaraciones juradas</a>" +
                                      "</div>" 
                            modal_window('Presentación DDJJ', 500, 100, message);
                        }
                    }
                });                
            }

                
                
            
            function see_response_exist_ddjj(html){                   
                if (html != '') {
                
                    if (html == 'SI') {
                        alert('Periodo existente')
                        return false;
                    }            
                alert('se puede agregar')        
                }                
            return true;    
            }          
            
            
            
            
            /* Obtengo el articulo de la municipalidad relacionado a esta tasa */
            function ajax_getRubros(p_indice) {
                this.intIndiceRubro = p_indice;
                
                jQuery.ajax({
                    type:"POST",
                    url: "../controladores/handler_getRubros.ashx",
                    data: "indice=" + p_indice,
                    success: see_response_rubros
                });
            }


            function see_response_rubros(html) {                
                if (html != '') {
                    verRubros('Listado de rubros', 750, 250, html);
                    return false;
                }                
            return true;    
            }
            
            
        </script>          
               
               

        <script type="text/javascript" language="javascript">

            /* Asigno el código del rubro en base a la descripción seleecionada en el combo */
            function asignarCodigoRubro(p_index) {
                control_combo = window.document.getElementById('cbRubros' + p_index);
                index_aux = control_combo.selectedIndex;
                valor_aux = control_combo.options[index_aux].value;

                if (valor_aux == 0) {
                    window.document.getElementById('td_codigo' + p_index).innerHTML = '[Código]';
                } else {
                    window.document.getElementById('td_codigo' + p_index).innerHTML = valor_aux;
                }
            }
        
        
        
            function refresh_date() {
                var _dates;
                var _date;

                //window.document.getElementById('td_date_expiry_quota1').innerHTML = 'Actualizado fecha ...';
                //window.document.getElementById('td_date_expiry_quota2').innerHTML = 'Actualizado fecha ...';
                window.document.getElementById('td_date_expiry_presentation').innerHTML = 'Actualizado fecha ...';

                control_combo = window.document.getElementById('cmbperiod');
                index_aux = control_combo.selectedIndex;
                _dates = control_combo.options[index_aux].value;                           
                _date = _dates.split('#');

                //window.document.getElementById('td_date_expiry_quota1').innerHTML = _date[0];
                //window.document.getElementById('td_date_expiry_quota2').innerHTML = _date[1];
                window.document.getElementById('td_date_expiry_presentation').innerHTML = _date[2];
            }



            /* Creo el popup para agregar periodos */
            function buscarRubro(p_indice) {
                var control_list_aux;

                
                control_list_aux = window.document.getElementById('selListRubro');
                if (control_list_aux != null) {
                    padre = control_list_aux.parentNode;
                    padre.removeChild(control_list_aux);
                }
                
               ajax_getRubros(p_indice);
            }



            function cargarRubro() {            
                var control_list = window.document.getElementById('selListRubro');
                var index_aux = control_list.selectedIndex;
                var valor_aux = control_list.options[index_aux].value;
                var rubro_aux = valor_aux.split('-');


                window.document.getElementById('td_codigo_rubro' + this.intIndiceRubro).innerHTML = rubro_aux[0];
                window.document.getElementById('td_descripcion_rubro' + this.intIndiceRubro).innerHTML = rubro_aux[1];
                
            }



            function limpiarRubro(p_indice) {
                var mensaje = "<div align='left'>" +
                                    "Usted esta a punto de borrar el rubro seleccionado.<br/>" +
                                    "¿Desea continuar?" + 
                              "</div>"

                this.mensaje_limpiarRubro('Limpiar Rubro', 350, 50, mensaje, p_indice);            
            }



            function borrarRubro(p_indice) {
                window.document.getElementById('td_codigo_rubro' + p_indice).innerHTML = '[Código]';
                window.document.getElementById('td_descripcion_rubro' + p_indice).innerHTML = '[Descripción del rubro]';
            }
          
        
                                   
            /* This function validated the quantity of employees and the declared amount */        
            function to_validate_data(){
                var id_grilla = 'gridrubros';
                var bandera = false;
                
               /*Este control valida que si los campos son = a 0 (cero) consulte con el operador si esta OK.*/
                 for (i=1;i<11;i++){
                    var txtemployees = window.document.getElementById('txtemployees'+i).value;
                    var tdcodigorubro= window.document.getElementById('td_codigo_rubro'+i).innerHTML;
                     if (tdcodigorubro != '[Código]' ){
                        if( txtemployees == 0 ){
                            if (confirm("El valor del campo de Empleados es 0 (cero). ¿ Desea dar de alta la DDJJ ?") == false){
                                window.document.getElementById('txtemployees'+i).focus();
                                return;
                                }
                            }      
                        }
                    } 
                 /*Acá se verifica que el monto cuando es 0(CERO) sea confirmado por el operador*/
                 for (i=1;i<11;i++) {
                    var txtamount= window.document.getElementById('txtamount'+i).value;
                    var tdcodigorubro= window.document.getElementById('td_codigo_rubro'+i).innerHTML;
                    if (tdcodigorubro != '[Código]' ){
                        if(txtamount == 0 ){
                            if (confirm("El valor del campo de Monto es 0 (cero). ¿ Desea dar de alta la DDJJ ?") == false){
                                window.document.getElementById('txtamount'+i).focus();
                                return;
                                }
                            }      
                        }
                 }
                
                                
                for (var i=1; i<document.getElementById(id_grilla).rows.length; i++) {

                    /* valido que se haya seleccionado al menos un rubro */
                    if (!bandera) {

                        var valor_rubro = window.document.getElementById('td_codigo_rubro' + i).innerHTML;                    
                        if (valor_rubro != '[Código]') {
                            bandera = true;
                        }                        
                    }
                     
                                       
                    /* setear controls */                    
                    var control_txt_employees = window.document.getElementById('txtemployees'+i);
                    var control_txt_amount = window.document.getElementById('txtamount'+i);
                    
                    
                    /* count employees validated */
                    if (control_txt_employees != null){                        
                        if (!validar_cadena_solo_numeros(control_txt_employees, 'Cantidad de empleados')) {
                            return false;                       
                        }
                    }

                    /* amount declared validated */
                    if (control_txt_amount != null){                                                                       
                        if (!validar_cadena_importe(control_txt_amount, 'Monto declarado')) {                            
                            return false;                       
                        }
                    }                                                            
                }


                /* Si no se seleeciono ningún rubro devuelvo falso */
                if (!bandera) {
                    return false;
                }
                
            return true;   
            }
    

            function loading(p_message) {
                var _html;
            
        	    _html = '<div id="loading" align="center">'+
        	                '<table border= "0">' +
                                '<tr>' +
                                    '<td align="center" style="width:20%;"><img id="img_load" alt="" src="../imagenes/loading.gif" /></td>' +
                                    '<td align="left" style="width:80%;">' + p_message + ' ....' +'</td>' +
                                '</tr>' +
                            '</table>' +
	                    '</div>';          
                 window.document.getElementById('div_contenedor_botones').innerHTML = _html;
            }

            
        </script>                
                
                
                
      
        
        
         
        
        
        <!-- funciones varias -->
        <script type="text/javascript">

            
            /* Esta Funcion ASIGNA ESTILO a un BOTON */          
            function efecto_over(boton) { boton.className = "boton_generico_gris"; }
    	    
            /* Esta Funcion ASIGNA ESTILO a un BOTON */          
  	        function efecto_out(boton){ boton.className = "boton_generico_verde"; }
        
    
    
            /* calculate_declaration: */
            /* This function calculates in declared amount */        
            function calculate_declaration(num){
                var total = 0;
                var total_aux = 0;            
                var celda_importe = 4;
                var id_grilla = 'gridrubros';
                var tasa= 0;
                var tasa_resultado = 0;
                var tasa_max;
        
                    
        
        
                   var amount = document.getElementById('txtamount'+num).value;
                   var alicuota = document.getElementById('lblAlicuota'+num).innerHTML;
                   
                   
                  tasa = parseFloat(amount) * (alicuota / 1000); 
					tasa = parseFloat(tasa);
					tasa_resultado = tasa.toFixed(2);

                    /*tasa = parseFloat(tasa) / 100;*/

                    if (isNaN(tasa)){
                        tasa = "0";
                    }
                    document.getElementById('lblTasa'+num).innerHTML = tasa_resultado;

                    tasa_max = tasa_resultado;
                    

                    var tasa1 = document.getElementById('lblTasa1').innerHTML;
                    var tasa2 = document.getElementById('lblTasa2').innerHTML;
                    var tasa3 = document.getElementById('lblTasa3').innerHTML;
                    var tasa4 = document.getElementById('lblTasa4').innerHTML;
                    var tasa5 = document.getElementById('lblTasa5').innerHTML;
                    var tasa6 = document.getElementById('lblTasa6').innerHTML;
                    var tasa7 = document.getElementById('lblTasa7').innerHTML;
                    var tasa8 = document.getElementById('lblTasa8').innerHTML;
                    var tasa9 = document.getElementById('lblTasa9').innerHTML;
                    var tasa10 = document.getElementById('lblTasa10').innerHTML;
                       
                    total = parseFloat(tasa1) + parseFloat(tasa2) + parseFloat(tasa3) + parseFloat(tasa4) + parseFloat(tasa5) + parseFloat(tasa6)+ parseFloat(tasa7)+ parseFloat(tasa8)+ parseFloat(tasa9)+ parseFloat(tasa10);
                    
                    document.getElementById('td_rubros_total').innerHTML = formato_importe(total);                        
                        
                    /*Calcula mayor de los minimos*/
                    
                    var minimo1 = document.getElementById('lblMinimo1').innerHTML;
                    var minimo2 = document.getElementById('lblMinimo2').innerHTML;
                    var minimo3 = document.getElementById('lblMinimo3').innerHTML;
                    var minimo4 = document.getElementById('lblMinimo4').innerHTML;
                    var minimo5 = document.getElementById('lblMinimo5').innerHTML;
                    var minimo6 = document.getElementById('lblMinimo6').innerHTML;
                    var minimo7 = document.getElementById('lblMinimo7').innerHTML;
                    var minimo8 = document.getElementById('lblMinimo8').innerHTML;
                    var minimo9 = document.getElementById('lblMinimo9').innerHTML;
                    var minimo10 = document.getElementById('lblMinimo10').innerHTML;
                    var TasaTotal = document.getElementById('td_rubros_total').innerHTML;
                    var mayor = 0
                    
                  var numeros = [minimo1,minimo2,minimo3,minimo4,minimo5,minimo6,minimo7,minimo8,minimo9,minimo10];

                  var numeroMayor = numeros[0];
                 
                  for (x=1;x<numeros.length;x++) {
                        if (numeros[x] > numeroMayor)
                        numeroMayor = numeros[x];
                    }

                 if (parseFloat(numeroMayor)< parseFloat(total)){
                 numeroMayor= parseFloat(Total);
                 }   
                   
                 document.getElementById('td_rubros_total').innerHTML = formato_importe(numeroMayor);
                 
                    
                /*for (var i=1; i<document.getElementById(id_grilla).rows.length; i++) {
                                         
                    var control_txt = document.getElementById('lblTasa'+i);                    
                    if (control_txt != null){                        
                                                
                        if (control_txt.innerHTML != '') {                    
                            total_aux = control_txt.innerHTML;
                            total = parseFloat(total) + parseFloat(total_aux);                           
                        }
                    }                                       
                }  */
                    
                    
                /* asigno datos               
                document.getElementById('td_rubros_total').innerHTML = formato_importe(total);     */         
            }
    
            
            
    
            /* formato_importe: */
            /* Esta Funcion ASIGNA FORMATO a un IMPORTE */        
            function formato_importe(importe) {
                importe = importe.toString().replace(/$|,/g,'');
                if(isNaN(importe))
                    importe = "0";
                    
                sign = (importe == (importe = Math.abs(importe)));
                importe = Math.floor(importe*100+0.50000000001);
                cents = importe%100;
                importe = Math.floor(importe/100).toString();
                
                if(cents<10)
                    cents = "0" + cents;
                    
                for (var i = 0; i < Math.floor((importe.length-(1+i))/3); i++)
                    importe = importe.substring(0,importe.length-(4*i+3))+','+
                    
                importe.substring(importe.length-(4*i+3));
                return (((sign)?'':'-') + '$ ' + importe + '.' + cents);
            }


            function declaration(declaration_and_voucher) {                
                var _xml_rubros;
                var _xml_constanciadata;
                var _message;

                if (this.to_validate_data()) {
                    loading('Generando comprobantes');
                   
                    _xml_rubros = this.get_rubros();
                    _xml_constanciadata = this.get_constanciadata(declaration_and_voucher); 
                                       
                    this.declaration_pdf(_xml_rubros, _xml_constanciadata);
                    
                }
            }




            /* Genero el XML  */
            function get_rubros() {
                var id_grilla = 'gridrubros';
                var _period;
                var _xml;
                var _xml_selected;
                var _txtcontrol_employees;
                var _txtcontrol_amount;
                var control_codigo_rubro;
                var acum = 0; 
                var _lblTasa_amount;
                
                _xml = '<RUBRO>\n' +
                           '<NRO_COM>1</NRO_COM>\n' +
                           '<ANIO>1800</ANIO>\n' +
                           '<CUOTA>1.1</CUOTA>\n' +
                           '<NRO_TITULP>0</NRO_TITULP>\n' +
                           '<NRO_EMPLP>1.1</NRO_EMPLP>\n' +
                           '<MONT_FACTP>0.00</MONT_FACTP>\n' +
                           '<MONT_GANP>0.00</MONT_GANP>\n' +
                           '<MONT_GASTP>0.00</MONT_GASTP>\n' +
                           '<MONT_SUELP>0.00</MONT_SUELP>\n' +
                           '<PRES_DDJJP>P</PRES_DDJJP>\n' +
                           '<FECH_DDJJP>01/01/1800</FECH_DDJJP>\n' +
                           '<MONT_DDJJP>0.0</MONT_DDJJP>\n' +
                           '<RUBRO_NUMERO>0.0</RUBRO_NUMERO>\n' +
                           '<RUBRO_C>1.1</RUBRO_C>\n' +
                      '</RUBRO>';
                
                
                _xml_selected = '';
                for (var i=1; i < (window.document.getElementById(id_grilla).rows.length -2); i++) {

                    /* obtengo el periodo */
                    control_combo_periodo = window.document.getElementById('cmbperiod');
                    index_aux = control_combo_periodo.selectedIndex;
                    periodo_aux = control_combo_periodo.options[index_aux].text;
                    _period = periodo_aux.split('-');


                    /* obtengo los valores ingresados*/                    
                    _txtcontrol_employees = window.document.getElementById('txtemployees' + i).value;
                    _txtcontrol_amount = window.document.getElementById('txtamount' + i).value;
                    control_codigo_rubro = window.document.getElementById('td_codigo_rubro' + i).innerHTML;
                    _lblTasa_amount = window.document.getElementById('lblTasa' + i).innerHTML;
                    acum = parseFloat(acum) + parseFloat(_lblTasa_amount) ; 
                    

                    if (control_codigo_rubro != '[Código]') {
                        _xml_selected = _xml_selected + '<RUBRO>\n' +
                                                            '<NRO_COM>' + window.document.getElementById('lblcomercio_user').innerHTML + '</NRO_COM>\n' +
                                                            '<ANIO>' + _period[0].replace(/^\s+|\s+$/g, "") + '</ANIO>\n' +
                                                            '<CUOTA>' + _period[1].replace(/^\s+|\s+$/g, "") + '.1</CUOTA>\n' +
                                                            '<NRO_TITULP>0</NRO_TITULP>\n' +
                                                            '<NRO_EMPLP>' + _txtcontrol_employees + '</NRO_EMPLP>\n' +
                                                            '<MONT_FACTP>0.00</MONT_FACTP>\n' +
                                                            '<MONT_GANP>0.00</MONT_GANP>\n' +
                                                            '<MONT_GASTP>0.00</MONT_GASTP>\n' +
                                                            '<MONT_SUELP>0.00</MONT_SUELP>\n' +
                                                            '<PRES_DDJJP>P</PRES_DDJJP>\n' +
                                                            '<FECH_DDJJP>' + window.document.getElementById('td_date_presentation').innerHTML + '</FECH_DDJJP>\n' +
                                                            '<MONT_DDJJP>' + _txtcontrol_amount + '</MONT_DDJJP>\n' +
                                                            '<RUBRO_NUMERO>' + i.toString() + '</RUBRO_NUMERO>\n' +
                                                            '<RUBRO_C>' + control_codigo_rubro + '</RUBRO_C>\n' +
                                                        '</RUBRO>';
                    }
                }                                                        
                _xml = _xml + _xml_selected;                
                _xml = '<DDJJ>\n' + _xml + '</DDJJ>';
                
            return _xml;
            }



            /* Genero el XML  */
            function get_constanciadata(ptype_pdf) {
                var id_grilla = 'gridrubros';               
                var _xml;
                var _xml_rubros;
                var _txtcontrol_employees;
                var _txtcontrol_amount;
                var _txtcontrol_alicuota;
                var _txtcontrol_minimo;
                var _txtcontrol_tasa;


                control_combo = window.document.getElementById('cmbperiod');
                index_aux = control_combo.selectedIndex;
                valor_aux = control_combo.options[index_aux].text;                               
                                
                     
                _xml = '<PERIODOS>\n' + 
                            '<PERIODO>\n' +
                                valor_aux + '\n' +
                            '</PERIODO>\n' +
                       '</PERIODOS>\n' +
                       '<PDF_TYPE>\n' + 
                            '<ADD_AND_VOUCHER>\n' +
                                ptype_pdf + '\n' +
                            '</ADD_AND_VOUCHER>\n' +
                       '</PDF_TYPE>\n';
                
                
                _xml_rubros = '';
                for (var i = 1; i < (window.document.getElementById(id_grilla).rows.length - 2); i++) {

                    debugger;
                    /* verifico que el combo contenga un rubro seleccionado, de no ser así, lo salteo */
                    codigo_rubro = window.document.getElementById('td_codigo_rubro' + i).innerHTML;
                    descripcion_rubro = window.document.getElementById('td_descripcion_rubro' + i).innerHTML;

                    if (codigo_rubro != '[Código]') {

                        /* obtengo los valores ingresados*/
                        _txtcontrol_employees = window.document.getElementById('txtemployees' + i).value;
                        
                        _txtcontrol_amount = window.document.getElementById('txtamount' + i).value;
                        
                        _txtcontrol_alicuota = document.getElementById('lblAlicuota' + i).innerHTML;
                        
                        _txtcontrol_minimo = document.getElementById('lblMinimo' + i).innerHTML;
                        
                        _txtcontrol_tasa = document.getElementById('lblTasa' + i).innerHTML;
                        
                        
                        /* I save the data of the "rubro" */
                        _xml_rubros = _xml_rubros + '<RUBROS>\n' +
                                                        '<RUBRO_CODE>' + codigo_rubro + '</RUBRO_CODE>\n' +
                                                        '<RUBRO_DESCRIPTION>' + descripcion_rubro + '</RUBRO_DESCRIPTION>\n' +
                                                        '<RUBRO_EMPLOYEES>' + _txtcontrol_employees + '</RUBRO_EMPLOYEES>\n' +
                                                        '<RUBRO_AMOUNT>' + _txtcontrol_amount + '</RUBRO_AMOUNT>\n' +
                                                        '<RUBRO_ALICUOTA>' + _txtcontrol_alicuota + '</RUBRO_ALICUOTA>\n' +
                                                        '<RUBRO_MINIMO>' + _txtcontrol_minimo + '</RUBRO_MINIMO>\n' +
                                                        '<RUBRO_TASA>' + _txtcontrol_tasa + '</RUBRO_TASA>\n' + 
                                                    '</RUBROS>\n';
                    }
                }                                                        
                
                _xml_rubros = '<CONTENEDOR_RUBROS>\n' + _xml_rubros + '</CONTENEDOR_RUBROS>\n';
                _xml = _xml + _xml_rubros;
                _xml = '<CONSTANCIA>\n' + _xml + '</CONSTANCIA>';
            return _xml;
            }

        </script>
        
        
        
        <!-- funcion doPostBack y quienes las usan -->
        <script type="text/javascript" language="javascript">
            
            function declaration_pdf(p_xml_rubros, p_xml_constanciadata) { __doPostBack(p_xml_rubros, p_xml_constanciadata); }
        
            function __doPostBack(eventTarget, eventArgument) {
                var form;                
                                
                form = document.forms["formulario"];                                
                form.__EVENTTARGET.value = eventTarget.split("$").join(":");
                form.__EVENTARGUMENT.value = eventArgument;
                form.submit();
            }
        </script>
        
        
        <title>Sistema TISH :: Presentación de declaración jurada</title>
        <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
    	        
    </head>


    <body>
    
        <!-- div container of the page -->	
        <div id="wrap" style="width: 1000px;">
            
            <!-- div header -->	            
           
		        <div id="header" style="width:1000px;">			   
                    <div id="container_font">
		                <div  style="width:5%; float:left;  padding:0px 5px 0px 5px;">
                            <a href="#" title="Usar fuente menor"  class="link_font" onclick="assign_font('-');">A-</a> 
                        </div>
                        
                        <div  style="width:5%; float:left;">
		                    <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 
                        </div>		            
                        
		                <div  style="width:5%; float:left;">
		                    <a href="#" title="Usar fuente mayor" class="link_font" onclick="assign_font('+');">A+</a> 
		                </div>	            
                    </div>

                    <div id="div_header"  class="div_image_logo">                
                    
                    
                    <div id="div_menssage" align="center" runat="server" style="width:1000px;">                               
                        <asp:Label ID="lblmessage" runat="server" Text="mensaje" Font-Bold="True" ForeColor="Red" Visible="False"></asp:Label>                               
                    </div>                                   		                            
                </div>       		    
		    	 	     
		    	 	     
			    <h1 id="logo-text">Tasa de Inspección por Seguridad e Higiene</h1>			
			    <h2 id="slogan"><asp:Label ID="lblslogan_municipalidad" runat="server" Text=""></asp:Label></h2>				
		    </div>
	  
	  
	  
	            <!-- div container -->	
	            <div id="content-wrap">
	  
	     	        <!-- div container page title -->
                    <div id="div_title" style="background-color:Black; width:1000px;" align ="left" >
                        <strong class="titulo_page">Sistema TISH :: Presentación de declaración jurada - Esta DDJJ es fiel expresión de la verdad.</strong>
                    </div>
  					  
	  
	  	            <!-- div container right -->
                    <div id="div_optiones"  style="margin-right: -100px;" class="menu_options">                
                        <div id="div_comercio" >
                            <div align="center">
                                <table>
                                    <tr>
                                        <td  rowspan="2"><img id="user" alt="" src="../imagenes/user.png"  style=" width:50px; height:60px; border:none"/></td>
                                        <td><h1 align ="left" style="color:Black">Comercio</h1></td>
                                    </tr>
                                    <tr>                                
                                        <td>
                                            NUMERO: <asp:Label ID="lblcomercio_user" runat="server" Font-Bold="True"></asp:Label>
                                        </td>
                                    </tr>                                                               
                                </table>
                            </div>
                        </div>
                    
                    
                        <h1 align ="left" style="color:Black">Comercio</h1>                    			
	                    <ul class="sidemenu">               
                            <li><a href="webfrmdeclaracionesjuradas.aspx">Ver listado de DDJJ</a></li>
                            <li><a href="webfrmctacte.aspx">Ver cuenta corriente</a></li>				                
                            <li><a href="webfrmopciones.aspx">Datos del comercio</a></li>				                
                            <li><a href="#" onclick="javascript:change_trade();">Cambiar comercio</a></li>			                        
                        </ul>	
    	                
	                    <h1 align ="left" style="color:Black">Usuario Web</h1>				
                        <ul class="sidemenu">
	                        <li><a href="webfrmdatos_representante.aspx">Mis datos</a></li>				                
		                    <li><a href="webfrmcambiar_clave.aspx">Cambiar clave</a></li>	
		                    <li><a href="#" onclick="javascript:return logout();">Cerrar sesión</a></li>
	                    </ul>	
                    </div>
	  
	  
	  
                    <!-- div container left -->		  
	  		        <div id="main"> 

                <!-- form -->	                 
         <form id="formulario" runat="server">
        
                    <!-- Controles para utilizar la funcion doPostBack sin controles ASP -->
                    <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
                    <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
                  

                   <div id= "div_period" runat="server">
                   
                   </div>
                   
                   
                    
            
                    <!-- Div contenedor de la tabla de movimientos -->
                    <div id="div_rubros">
                    
                                                 
                        <table id='gridrubros' cellpadding='0' cellspacing='0' border='0' class='tabla_rubros_ddjj'>
                            <thead>     
                                <tr>         
                                    <td colspan="4" align="center"><strong>Rubros</strong></td>
                                </tr>
                                <tr>         
                                    <th><h6 style='width:1px;' align='center'></h6></th>                                    
                                    <th><h6 style='width:40px;' align='center'>Código</h6></th>                                    
                                    <th><h6 style='width:170px;' align='left'>Descripción</h6></th>                                    
                                    <th><h6 style='width:25px;' align='center'>Emp.</h6></th>
                                    <th><h6 style='width:60px;' align='center'>Monto</h6></th>
                                    <th><h6 style='width:65px;' align='center'>Alicuota</h6></th>
                                    <th><h6 style='width:60px;' align='center'>Minimo</h6></th>
                                    <th><h6 style='width:60px;' align='center'>Tasa</h6></th>
                                    
                                    <th><h6 style='width:1px;' align='center'></h6></th>                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>                                    
                                    <td style='width:1px;' align='center'>
                                        <%--<img alt='' src="../imagenes/lupa.png" height='20px' title='Buscar rubro' style='cursor:pointer';  onclick = 'javascript:buscarRubro(1);' />--%>
                                    </td>                               
                                    <td id = 'td_codigo_rubro1' align='center' style='width:10px;' runat="server">[Código]</td>     
                                    <td id='td_descripcion_rubro1' align='left' runat="server">[Descripción del rubro]</td>                                                                                                                
                                    <td align='center' style="width:30px;">
                                        <input id='txtemployees1' name='txtemployees1' runat="server" type='text' value='0' style='text-align:center; width:40px;' maxlength='3' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'/> 
                                    </td>
                                    <td align='left' style="width:40px;">
                                        <input id='txtamount1' name='txtamount1' type='text' runat="server" value='0.00' style='text-align:center; width:80px;' maxlength='14' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'  onkeyup='javascript:calculate_declaration(1);'/>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblAlicuota1" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label align='center' ID="lblMinimo1" runat="server" Text=""></asp:Label></td>
                                    <td align='left' style="width:40px;">
                                        <asp:Label align='center' ID="lblTasa1" runat="server" Text="0"  onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'  onkeyup='javascript:calculate_declaration();'></asp:Label></td>
                                    </td>
                                    <td style='width:10px;' align= "center">
                                        <%--<img alt='' src="../imagenes/basura.png" height='20px' title='Limpiar el registro' style='cursor:pointer';  onclick = 'javascript:limpiarRubro(1);' />--%>
                                    </td>                               
                                </tr>
                                <tr>                                    
                                    <td style='width:1px;' align='center'>
                                        <%--<img alt='' src="../imagenes/lupa.png" height='20px' title='Buscar rubro' style='cursor:pointer';  onclick = 'javascript:buscarRubro(2);' />--%>
                                    </td>                               
                                    <td id = 'td_codigo_rubro2' align='center' style='width:10px;' runat="server">[Código]</td>                                         
                                    <td id='td_descripcion_rubro2' align='left' runat="server">[Descripción del rubro]</td>
                                    <td align='center'>
                                        <input id='txtemployees2' name='txtemployees2' runat="server" type='text' value='0' style='text-align:center; width:40px;' maxlength='3' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'/>
                                    </td>
                                    <td align='center'>
                                        <input id='txtamount2' name='txtamount2' runat="server" type='text' value='0.00' style='text-align:center; width:80px;' maxlength='14' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'  onkeyup='javascript:calculate_declaration(2);'/>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblAlicuota2" align='center' runat="server" Text="0"></asp:Label>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblMinimo2" align='center' runat="server" Text="0"></asp:Label></td>
                                    <td align='left' style="width:40px;">
                                        <asp:Label align='center' ID="lblTasa2" runat="server" Text="0"></asp:Label></td>
                                    </td>
                                    <td style='width:10px;' align= "center">
                                        <%--<img alt='' src="../imagenes/basura.png" height='20px' title='Limpiar el registro' style='cursor:pointer';  onclick = 'javascript:limpiarRubro(2);' />--%>
                                    </td>                               
                                </tr>
                                <tr>
                                    <td style='width:1px;' align='center'>
                                        <%--<img alt='' src="../imagenes/lupa.png" height='20px' title='Buscar rubro' style='cursor:pointer';  onclick = 'javascript:buscarRubro(3);' />--%>
                                    </td>                               
                                    <td id = 'td_codigo_rubro3' align='center' style='width:10px;' runat="server">[Código]</td>                                         
                                    <td id='td_descripcion_rubro3' align='left' runat="server">[Descripción del rubro]</td>
                                    <td align='center'>
                                        <input id='txtemployees3' name='txtemployees3' runat="server" type='text' value='0' style='text-align:center; width:40px;' maxlength='3' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'/>
                                    </td>
                                    <td align='center'>
                                        <input id='txtamount3' name='txtamount3' runat="server" type='text' value='0.00' style='text-align:center; width:80px;' maxlength='14' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'  onkeyup="javascript:calculate_declaration(3);"/>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblAlicuota3" align='center' runat="server" Text="0"></asp:Label>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblMinimo3" align='center' runat="server" Text="0"></asp:Label></td>
                                    <td align='left' style="width:40px;">
                                        <asp:Label align='center' ID="lblTasa3" runat="server" Text="0"></asp:Label></td>
                                    </td>
                                    <td style='width:10px;' align= "center">
                                        <%--<img alt='' src="../imagenes/basura.png" height='20px' title='Limpiar el registro' style='cursor:pointer';  onclick = 'javascript:limpiarRubro(3);' />--%>
                                    </td>                               
                                </tr>
                                <tr>
                                    <td style='width:1px;' align='center'>
                                        <%--<img alt='' src="../imagenes/lupa.png" height='20px' title='Buscar rubro' style='cursor:pointer';  onclick = 'javascript:buscarRubro(4);' />--%>
                                    </td>                               
                                    <td id = 'td_codigo_rubro4' align='center' style='width:10px;' runat="server">[Código]</td>                                         
                                    <td id='td_descripcion_rubro4' align='left' runat="server">[Descripción del rubro]</td>
                                    <td align='center'>
                                        <input id='txtemployees4' name='txtemployees4' runat="server" type='text' value='0' style='text-align:center; width:40px;' maxlength='3' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'/> 
                                    </td>
                                    <td align='center'>
                                        <input id='txtamount4' name='txtamount4' runat="server" type='text' value='0.00' style='text-align:center; width:80px;' maxlength='14' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'  onkeyup='javascript:calculate_declaration(4);'/>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblAlicuota4" align='center' runat="server" Text="0"></asp:Label>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblMinimo4" align='center' runat="server" Text="0"></asp:Label></td>
                                    <td align='left' style="width:40px;">
                                        <asp:Label align='center' ID="lblTasa4" runat="server" Text="0"></asp:Label></td>
                                    </td>
                                    <td style='width:10px;' align= "center">
                                        <%--<img alt='' src="../imagenes/basura.png" height='20px' title='Limpiar el registro' style='cursor:pointer';  onclick = 'javascript:limpiarRubro(4);' />--%>
                                    </td>                               
                                </tr>
                                <tr>
                                    <td style='width:1px;' align='center'>
                                        <%--<img alt='' src="../imagenes/lupa.png" height='20px' title='Buscar rubro' style='cursor:pointer';  onclick = 'javascript:buscarRubro(5);' />--%>
                                    </td>                               
                                    <td id = 'td_codigo_rubro5' align='center' style='width:10px;' runat="server">[Código]</td>                                         
                                    <td id = 'td_descripcion_rubro5' align='left' runat="server">[Descripción del rubro]</td>
                                    <td align='center'>
                                        <input id='txtemployees5' name='txtemployees5' runat="server" type='text' value='0' style='text-align:center; width:40px;' maxlength='3' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'/>
                                    </td>
                                    <td align='center'>
                                        <input id='txtamount5' name='txtamount5' runat="server" type='text' value='0.00' style='text-align:center; width:80px;' maxlength='14' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'  onkeyup='javascript:calculate_declaration(5);'/>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblAlicuota5" align='center' runat="server" Text="0"></asp:Label>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblMinimo5" align='center' runat="server" Text="0"></asp:Label></td>
                                    <td align='left' style="width:40px;">
                                        <asp:Label align='center' ID="lblTasa5" runat="server" Text="0"></asp:Label></td>
                                    </td>
                                    <td style='width:10px;' align= "center">
                                        <%--<img alt='' src="../imagenes/basura.png" height='20px' title='Limpiar el registro' style='cursor:pointer';  onclick = 'javascript:limpiarRubro(5);' />--%>
                                    </td>
                                    <tr>
                                    <td style='width:1px;' align='center'>
                                        <%--<img alt='' src="../imagenes/lupa.png" height='20px' title='Buscar rubro' style='cursor:pointer';  onclick = 'javascript:buscarRubro(5);' />--%>
                                    </td>                               
                                    <td id = 'td_codigo_rubro6' align='center' style='width:10px;' runat="server">[Código]</td>                                         
                                    <td id = 'td_descripcion_rubro6' align='left' runat="server">[Descripción del rubro]</td>
                                    <td align='center'>
                                        <input id='txtemployees6' name='txtemployees6' runat="server" type='text' value='0' style='text-align:center; width:40px;' maxlength='3' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'/>
                                    </td>
                                    <td align='center'>
                                        <input id='txtamount6' name='txtamount6' runat="server" type='text' value='0.00' style='text-align:center; width:80px;' maxlength='14' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'  onkeyup='javascript:calculate_declaration(6);'/>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblAlicuota6" align='center' runat="server" Text="0"></asp:Label>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblMinimo6" align='center' runat="server" Text="0"></asp:Label></td>
                                    <td align='left' style="width:40px;">
                                        <asp:Label align='center' ID="lblTasa6" runat="server" Text="0"></asp:Label></td>
                                    </td>
                                    <td style='width:10px;' align= "center">
                                        <%--<img alt='' src="../imagenes/basura.png" height='20px' title='Limpiar el registro' style='cursor:pointer';  onclick = 'javascript:limpiarRubro(5);' />--%>
                                    </td>                               
                                </tr>
                                <tr>
                                    <td style='width:1px;' align='center'>
                                        <%--<img alt='' src="../imagenes/lupa.png" height='20px' title='Buscar rubro' style='cursor:pointer';  onclick = 'javascript:buscarRubro(5);' />--%>
                                    </td>                               
                                    <td id = 'td_codigo_rubro7' align='center' style='width:10px;' runat="server">[Código]</td>                                         
                                    <td id = 'td_descripcion_rubro7' align='left' runat="server">[Descripción del rubro]</td>
                                    <td align='center'>
                                        <input id='txtemployees7' name='txtemployees7' runat="server" type='text' value='0' style='text-align:center; width:40px;' maxlength='3' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'/>
                                    </td>
                                    <td align='center'>
                                        <input id='txtamount7' name='txtamount7' runat="server" type='text' value='0.00' style='text-align:center; width:80px;' maxlength='14' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'  onkeyup='javascript:calculate_declaration(7);'/>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblAlicuota7" align='center' runat="server" Text="0"></asp:Label>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblMinimo7" align='center' runat="server" Text="0"></asp:Label></td>
                                    <td align='left' style="width:40px;">
                                        <asp:Label align='center' ID="lblTasa7" runat="server" Text="0"></asp:Label></td>
                                    </td>
                                    <td style='width:10px;' align= "center">
                                        <%--<img alt='' src="../imagenes/basura.png" height='20px' title='Limpiar el registro' style='cursor:pointer';  onclick = 'javascript:limpiarRubro(5);' />--%>
                                    </td>                               
                                </tr>
                                <tr>
                                    <td style='width:1px;' align='center'>
                                        <%--<img alt='' src="../imagenes/lupa.png" height='20px' title='Buscar rubro' style='cursor:pointer';  onclick = 'javascript:buscarRubro(5);' />--%>
                                    </td>                               
                                    <td id = 'td_codigo_rubro8' align='center' style='width:10px;' runat="server">[Código]</td>                                         
                                    <td id = 'td_descripcion_rubro8' align='left' runat="server">[Descripción del rubro]</td>
                                    <td align='center'>
                                        <input id='txtemployees8' name='txtemployees8' runat="server" type='text' value='0' style='text-align:center; width:40px;' maxlength='3' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'/>
                                    </td>
                                    <td align='center'>
                                        <input id='txtamount8' name='txtamount8' runat="server" type='text' value='0.00' style='text-align:center; width:80px;' maxlength='14' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'  onkeyup='javascript:calculate_declaration(8);'/>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblAlicuota8" align='center' runat="server" Text="0"></asp:Label>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblMinimo8" align='center' runat="server" Text="0"></asp:Label></td>
                                    <td align='left' style="width:40px;">
                                        <asp:Label align='center' ID="lblTasa8" runat="server" Text="0"></asp:Label></td>
                                    </td>
                                    <td style='width:10px;' align= "center">
                                        <%--<img alt='' src="../imagenes/basura.png" height='20px' title='Limpiar el registro' style='cursor:pointer';  onclick = 'javascript:limpiarRubro(5);' />--%>
                                    </td>                               
                                </tr>
                                <tr>
                                    <td style='width:1px;' align='center'>
                                        <%--<img alt='' src="../imagenes/lupa.png" height='20px' title='Buscar rubro' style='cursor:pointer';  onclick = 'javascript:buscarRubro(5);' />--%>
                                    </td>                               
                                    <td id = 'td_codigo_rubro9' align='center' style='width:10px;' runat="server">[Código]</td>                                         
                                    <td id = 'td_descripcion_rubro9' align='left' runat="server">[Descripción del rubro]</td>
                                    <td align='center'>
                                        <input id='txtemployees9' name='txtemployees9' runat="server" type='text' value='0' style='text-align:center; width:40px;' maxlength='3' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'/>
                                    </td>
                                    <td align='center'>
                                        <input id='txtamount9' name='txtamount9' runat="server" type='text' value='0.00' style='text-align:center; width:80px;' maxlength='14' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'  onkeyup='javascript:calculate_declaration(9);'/>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblAlicuota9" align='center' runat="server" Text="0"></asp:Label>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblMinimo9" align='center' runat="server" Text="0"></asp:Label></td>
                                    <td align='left' style="width:40px;">
                                        <asp:Label align='center' ID="lblTasa9" runat="server" Text="0"></asp:Label></td>
                                    </td>
                                    <td style='width:10px;' align= "center">
                                        <%--<img alt='' src="../imagenes/basura.png" height='20px' title='Limpiar el registro' style='cursor:pointer';  onclick = 'javascript:limpiarRubro(5);' />--%>
                                    </td>                               
                                </tr>                               
                                <tr>
                                    <td style='width:1px;' align='center'>
                                        <%--<img alt='' src="../imagenes/lupa.png" height='20px' title='Buscar rubro' style='cursor:pointer';  onclick = 'javascript:buscarRubro(5);' />--%>
                                    </td>                               
                                    <td id = 'td_codigo_rubro10' align='center' style='width:10px;' runat="server">[Código]</td>                                         
                                    <td id = 'td_descripcion_rubro10' align='left' runat="server">[Descripción del rubro]</td>
                                    <td align='center'>
                                        <input id='txtemployees10' name='txtemployees10' runat="server" type='text' value='0' style='text-align:center; width:40px;' maxlength='3' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'/>
                                    </td>
                                    <td align='center'>
                                        <input id='txtamount10' name='txtamount10' runat="server" type='text' value='0.00' style='text-align:center; width:80px;' maxlength='14' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);'  onkeyup='javascript:calculate_declaration(10);'/>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblAlicuota10" align='center' runat="server" Text="0"></asp:Label>
                                    </td>
                                    <td align='center' style="width:40px;">
                                        <asp:Label ID="lblMinimo10" align='center' runat="server" Text="0"></asp:Label></td>
                                    <td align='left' style="width:40px;">
                                        <asp:Label align='center' ID="lblTasa10" runat="server" Text="0"></asp:Label></td>
                                    </td>
                                    <td style='width:10px;' align= "center">
                                        <%--<img alt='' src="../imagenes/basura.png" height='20px' title='Limpiar el registro' style='cursor:pointer';  onclick = 'javascript:limpiarRubro(5);' />--%>
                                    </td>                               
                                </tr>
                                <tr align='right' style='background-color:#999999; font-weight:bold; color:#006600;'>
                                    <td colspan='5'>Monto total declarado: </td>
                                    <td id='td_rubros_total' colspan='5'>0.00</td>
                                </tr>
                            </tbody>
                        </table>
                        
                        
                        <div id='div_contenedor_botones' style='width:90%; float:left; padding-top:10px; padding-left: 30px;' align='center'>
                            <div id='div_boton_declaration' style='width:49%; float:left'; align='center'>
                                <input type='button' value='Presentar' class='boton' style='width:200px; height:35px; cursor:pointer' onclick='javascript:ajax_exist_ddjj("false");'/>
                        </div>
                        <div id='div_button_declaration_pay' align='center' style='width:49%; float:right;' align='center'>
                            <input type='button' value='Presentar y pagar' class='boton' style='width:200px; height:35px; cursor:pointer' onclick='javascript:ajax_exist_ddjj("true");'/>
                        </div>
                    </div> 

                </div>    
                
                 </form>		       
		    </div>
				
				
					
			
		
			
							
  		</div> 	
			  
			  
            <!-- div container footer -->				  
	  		<div id="footer">
		
		        <div id="div_pie_municipalidad" style="width:70%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		            Municipalidad de
		            <strong> 
		                <asp:Label ID="lblnombre_municipalidad" runat="server" Text="NOMBRE_MUNICIPIO"></asp:Label> 
                    </strong> 
                    
				    <br />
				    Teléfono: 
				    <strong> 
                        <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                    </strong> 
                    | Email: 
				    <strong> 
                        <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                    </strong>                 
			    </div>
		   		   
		        <div id="div_pie_grupomdq"  class="footer_grupomdq">
				    Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ S.A</a></strong>  
				    <br />
				    Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				    <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
                    <asp:Label ID="lblversion" runat="server" Text=""></asp:Label>
		        </div>
			
		    </div>	
		
		
            
		         
		
		
		<!-- content-wrap ends here -->
		</div>	
	
	
	
     
	
	
    </body>
</html>