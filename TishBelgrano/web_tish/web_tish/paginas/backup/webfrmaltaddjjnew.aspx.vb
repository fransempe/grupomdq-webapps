﻿Public Partial Class webfrmaltaddjjnew
    Inherits System.Web.UI.Page


#Region "Variables"
    Private mWS As ws_tish.Service1
    Private dtDataComercio_AUX As DataTable
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dsConstanciaData As DataSet
        Dim mPeriod As String

        If Not (IsPostBack) Then

            Call Me.SloganMunicipalidad()
            Call Me.ValidarSession()
            Call Me.CargarPie()


            'I fill combo whit the periods
            Call Me.LoadComboPeriods()


            'I fill the grilla
            Call Me.LoadDataRubros()


        Else


            If (Request.Params.Get("__EVENTTARGET").Trim <> "") And (Request.Params.Get("__EVENTARGUMENT").Trim <> "") Then

                dsConstanciaData = New DataSet
                dsConstanciaData = clsTools.ObtainDataXML(Request.Params.Get("__EVENTARGUMENT").Trim)
                If (dsConstanciaData IsNot Nothing) Then

                    mPeriod = dsConstanciaData.Tables("PERIODOS").Rows(0).Item(0).ToString.Trim
                    If (Me.AddDDJJ(Request.Params.Get("__EVENTTARGET").Trim, mPeriod.Trim)) Then
                        Call Me.GenerarDataSetPDF(dsConstanciaData)
               

                        Session("add_and_voucher") = dsConstanciaData.Tables("PDF_TYPE").Rows(0).Item(0).ToString.Trim
                        Response.Redirect("comprobantes/webfrmcomprobanteddjj.aspx", False)
                    End If
                End If
            End If
        End If

    End Sub




    Private Sub SloganMunicipalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema TISH."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema TISH."
        End If

    End Sub


    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("contribuyente_nombre") IsNot Nothing) Then
                If (Session("contribuyente_nombre").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        If (mLogin_OK) Then

            'I Update the dataset
            dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))
            lblcomercio_user.Text = dtDataComercio_AUX.Rows(0).Item("COMERCIO_NUMERO").ToString.Trim()
        Else
            Response.Redirect("webfrmlogin.aspx", False)
        End If

    End Function

    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            Me.lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            Me.lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If


        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub


    'LoadComboPeriods
    'This function loads the combo whit the periods
    Private Sub LoadComboPeriods()
        Dim dsDataPeriods As DataSet
        Dim mXML_AUX As String
        Dim i As Integer


        Try



            'I obtain the periods
            Me.mWS = New ws_tish.Service1
            mXML_AUX = Me.mWS.ObtainDateOfExpiry()
            Me.mWS = Nothing




            'I read the XML
            dsDataPeriods = New DataSet
            dsDataPeriods = clsTools.ObtainDataXML(mXML_AUX.ToString.Trim)
            Session("ROWS_PERIODS") = dsDataPeriods.Tables("ROWS_PERIODS").Copy

            'If the dsListadoDDJJ is not empty I assign information
            If (clsTools.HasData(dsDataPeriods)) Then
                Me.div_period.InnerHtml = Me.CreateHTMLPeriod(dsDataPeriods.Tables("ROWS_PERIODS"))
            End If



        Catch ex As Exception
        Finally
            mWS = Nothing
        End Try

    End Sub


    'LoadDataRubros
    'This function loads the rubros
    Private Sub LoadDataRubros()
        Dim dsDataRubros As DataSet
        Dim mXML_AUX As String


        Try

            'I Update the dataset
            dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))


            If (dtDataComercio_AUX IsNot Nothing) Then

                'I look for the information of the comercio
                'Me.mWS = New ws_tish.Service1
                'mXML_AUX = Me.mWS.ObtainRubros(CInt(dtDataComercio_AUX.Rows(0).Item("COMERCIO_NUMERO")))
                'Me.mWS = Nothing


                'I read the XML
                'dsDataRubros = New DataSet
                'dsDataRubros = clsTools.ObtainDataXML(mXML_AUX.ToString.Trim)


                'If the dsListadoDDJJ is not empty I assign information
                'If (clsTools.HasData(dsDataRubros)) Then
                'Me.div_rubros.InnerHtml = Me.CreateHTMLRubros(dsDataRubros.Tables(0))
                Me.div_rubros.InnerHtml = Me.CreateHTMLRubros(Nothing)
                'End If
            End If

        Catch ex As Exception
        Finally
            mWS = Nothing
        End Try


    End Sub


    Private Function CreateHTMLPeriod(ByVal dtData As DataTable) As String 
        Dim mHTML As String
        Dim mHTML_ComboItems As String
        Dim mValue As String


        Try

            mHTML_ComboItems = ""
            mValue = ""
            If Not (CBool(Session("Rectificativa"))) Then
                If (dtData IsNot Nothing) Then
                    For i = 0 To dtData.Rows.Count - 1

                        mValue = dtData.Rows(i).Item("FV_CUOTA").ToString.Trim & "#" & dtData.Rows(i).Item("FV_PRESENTACION").ToString.Trim
                        If (i = 0) Then
                            mHTML_ComboItems = mHTML_ComboItems & "<option value='" & mValue.Trim & "' selected='selected'>" & dtData.Rows(i).Item("ANIO").ToString.Trim & " - " & dtData.Rows(i).Item("CUOTA").ToString.Trim & "</option>"
                        Else
                            mHTML_ComboItems = mHTML_ComboItems & "<option value='" & mValue.Trim & "'>" & dtData.Rows(i).Item("ANIO").ToString.Trim & " - " & dtData.Rows(i).Item("CUOTA").ToString.Trim & "</option>"
                        End If
                    Next
                End If

            Else
                mHTML_ComboItems = mHTML_ComboItems & "<option>" & Session("Rectificativa_Period").ToString.Trim & "</option>"
            End If




            mHTML = ""
            mHTML = mHTML & "<table id='table_period' align='center'  border='0' style='width:90%; border:1px solid #C0C0C0;'>"
            mHTML = mHTML & "   <tr>"
            If Not (CBool(Session("Rectificativa"))) Then
                mHTML = mHTML & " <td colspan='4' style='color:Black; font-weight:bold;' align='center'>Original :: Seleccione un período a declarar</td>"
            Else
                mHTML = mHTML & " <td colspan='4' style='color:Black; font-weight:bold;' align='center'>Rectificativa :: Los datos del periodo son puramente informativos.</td>"
            End If
            mHTML = mHTML & "   </tr>"
            mHTML = mHTML & "   <tr><td colspan='4' class='linea'><hr /></td></tr>"
            mHTML = mHTML & "   <tr>"
            mHTML = mHTML & "       <td style='width:10%'>Período:</td>"
            mHTML = mHTML & "       <td style='width:20%' align='left'>"
            mHTML = mHTML & "           <select id = 'cmbperiod' name='cmbperiod' style='width:60%;' onchange=""javascript:refresh_date();"">" & mHTML_ComboItems.Trim & "</select>"
            mHTML = mHTML & "       </td>"
            mHTML = mHTML & "       <td style='width:20%'>Fecha Presentación:</td>"
            mHTML = mHTML & "       <td id='td_date_presentation' style='width:15%'>" & Format(Now, "dd/MM/yyyy") & "</td>"
            mHTML = mHTML & "   </tr>"
            mHTML = mHTML & "   <tr>"
            mHTML = mHTML & "       <td style='width:15%'>Fecha Vto. cuota:</td>"
            mHTML = mHTML & "       <td id='td_date_expiry_quota' style='width:15%'>" & dtData.Rows(0).Item("FV_CUOTA").ToString.Trim & "</td>"
            mHTML = mHTML & "       <td style='width:15%'>Fecha Vto. presentación:</td>"
            mHTML = mHTML & "       <td id='td_date_expiry_presentation' style='width:15%'>" & dtData.Rows(0).Item("FV_PRESENTACION").ToString.Trim & "</td>"
            mHTML = mHTML & "   </tr>"
            mHTML = mHTML & "   <tr><td colspan='4' class='linea'><hr /></td></tr>"
            mHTML = mHTML & "   <tr><td colspan='4' class='linea'><hr /></td></tr>"
            mHTML = mHTML & "</table>"


        Catch ex As Exception
            mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                        "<p id='lblmessenger' style='color: Red; font-weight: inherit;'> " & _
                            "Error al intentar obtener los datos." & _
                        "</p>" & _
                    "</div>"
        End Try

        Return mHTML.Trim
    End Function


    Private Function CreateHTMLRubros(ByVal dtData As DataTable) As String
        Dim mHTML_Table_Header As String
        Dim mHTML_Table_Body As String
        Dim mHTML As String
        Dim mFuncionJavascript As String
        Dim mEmployeesUsing As Boolean


        Try

            'mEmployeesUsing = Me.EmployeesUsing()
            mEmployeesUsing = True

            mHTML = ""


            'Cabecera de la tabla
            mHTML_Table_Header = ""
            mHTML_Table_Header = mHTML_Table_Header & "<thead>"
            mHTML_Table_Header = mHTML_Table_Header & "     <tr>"
            mHTML_Table_Header = mHTML_Table_Header & "         <th style='width:10px;'><h6 align='center'>Código</h6></th>"
            mHTML_Table_Header = mHTML_Table_Header & "         <th style='width:150px;'><h6 align='left'>Descripción</h6></th>"

            If (mEmployeesUsing) Then
                mHTML_Table_Header = mHTML_Table_Header & "         <th style='width:10px;'><h6 align='center'>Emplea.</h6></th>"
            End If

            mHTML_Table_Header = mHTML_Table_Header & "         <th style='width:10px;'><h6 align='center'>Monto</h6></th>"
            mHTML_Table_Header = mHTML_Table_Header & "     </tr>"
            mHTML_Table_Header = mHTML_Table_Header & "</thead>"




            'Creo los renglones de la tabla
            mHTML_Table_Body = ""
            'For i = 0 To dtData.Rows.Count - 1
            For i = 0 To 5

                mHTML_Table_Body = mHTML_Table_Body & "<tr>"
                mHTML_Table_Body = mHTML_Table_Body & "<td id='td_codigo" & (i + 1) & "' align='center'>[Código]</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='left'>" & _
                                                        "<select id= 'cbRubros" & (i + 1) & "' name='cbRubros" & (i + 1) & "' onclick='return asignarCodigoRubro(" & (i + 1) & ")'>" & _
                                                            "<option value='0' selected='selected'>[Seleccione un rubro]</option>" & _
                                                            "<option value='1'>COMERCIO CATEGORIA A</option>" & _
                                                            "<option value='2'>COMERCIO CATEGORIA B</option>" & _
                                                            "<option value='3'>COMERCIO CATEGORIA 3</option>" & _
                                                            "<option value='4'>COMERCIO CATEGORIA 4</option>" & _
                                                            "<option value='5'>COMERCIO CATEGORIA 5</option>" & _
                                                            "<option value='6'>COMERCIO CATEGORIA 6</option>" & _
                                                            "<option value='8'>CATERORIA 8</option>" & _
                                                            "<option value='9'>CATEGORIA 9</option>" & _
                                                            "<option value='50'>CATEGORIA A</option>" & _
                                                            "<option value='51'>CATEGORIA B</option>" & _
                                                            "<option value='52'>CATEGORIA C</option>" & _
                                                            "<option value='53'>CATEGORIA D</option>" & _
                                                            "<option value='54'>CATEGORIA E</option>" & _
                                                            "<option value='55'>CATEGORIA F</option>" & _
                                                            "<option value='56'>CATEGORIA G</option>" & _
                                                            "<option value='57'>CATEGORIA H</option>" & _
                                                            "<option value='58'>CATEGORIA I</option>" & _
                                                            "<option value='59'>CATEGORIA J</option>" & _
                                                            "<option value='60'>CATEGORIA K</option>" & _
                                                            "<option value='1000'>RUBRO1000</option>" & _
                                                            "<option value='2000'>RUBRO2000</option>" & _
                                                            "<option value='3000'>RUBRO3000</option>" & _
                                                            "<option value='4000'>RUBRO4000</option>" & _
                                                            "<option value='5000'>RUBRO5000</option>" & _
                                                        "</select>" & _
                                                    "</td>"


                If (mEmployeesUsing) Then
                    mHTML_Table_Body = mHTML_Table_Body & "<td align='center'> "
                    mHTML_Table_Body = mHTML_Table_Body & "<input id='txtemployees" & (i + 1).ToString & "' name='txtemployees" & (i + 1).ToString & "' type='text' value='0' style='text-align:center; width:25px;' maxlength='3' onfocus=""javascript:event_focus(this);"" onblur=""javascript:event_focus(this);""/> "
                    mHTML_Table_Body = mHTML_Table_Body & "</td>"
                End If

                mHTML_Table_Body = mHTML_Table_Body & "<td align='center'> "
                mHTML_Table_Body = mHTML_Table_Body & "<input id='txtamount" & (i + 1).ToString & "' name='txtamount" & (i + 1).ToString & "' type='text' value='0.00' style='text-align:center; width:60px;' maxlength='9' onfocus=""javascript:event_focus(this);"" onblur=""javascript:event_focus(this);""  onkeyup=""javascript:calculate_declaration();""/> "
                mHTML_Table_Body = mHTML_Table_Body & "</td>"
            Next



            'Tabla totales
            mHTML_Table_Body = mHTML_Table_Body & "<tr align='right' style='background-color:#999999; font-weight:bold; color:#006600;'>"

            If (mEmployeesUsing) Then
                mHTML_Table_Body = mHTML_Table_Body & "     <td colspan='3'>Monto total declarado: </td>"
            Else
                mHTML_Table_Body = mHTML_Table_Body & "     <td colspan='2'>Monto total declarado: </td>"
            End If

            mHTML_Table_Body = mHTML_Table_Body & "     <td id='td_rubros_total'>0.00</td>"
            mHTML_Table_Body = mHTML_Table_Body & "</tr>"




            mHTML = "<table id='gridrubros' cellpadding='0' cellspacing='0' border='0' class='table_periods' >" & _
                        mHTML_Table_Header.Trim & vbCr & _
                        "<tbody>" & mHTML_Table_Body.Trim & "</tbody>" & _
                    "</table>"



            mFuncionJavascript = "ajax_exist_ddjj"
            If (CBool(Session("Rectificativa"))) Then
                mFuncionJavascript = "declaration"
            End If
            mHTML = mHTML & " <div id='div_contenedor_botones' style='width:90%; float:left; padding-top:10px; padding-left: 30px;' align='center'> " & _
                                " <div id='div_boton_declaration' style='width:49%; float:left'; align='center'> " & _
                                    " <input type='button' value='Presentar' class='boton' style='width:200px; height:35px; cursor:pointer' onclick=""javascript:" & mFuncionJavascript.Trim & "('false');""/> " & _
                                " </div> " & _
                                " <div id='div_button_declaration_pay' align='center' style='width:49%; float:right;' align='center'> " & _
                                    " <input type='button' value='Presentar y pagar' class='boton' style='width:200px; height:35px; cursor:pointer' onclick=""javascript:" & mFuncionJavascript.Trim & "('true');""/> " & _
                                " </div> " & _
                            " </div> "



        Catch ex As Exception
            mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                        "<p id='lblmessenger' style='color: Red; font-weight: inherit;'> " & _
                            "Error al intentar obtener los datos." & _
                        "</p>" & _
                    "</div>"
        End Try


        Return mHTML.ToString.Trim
    End Function

    'Private Function CreateHTMLRubros(ByVal dtData As DataTable) As String
    '    Dim mHTML_Table_Header As String
    '    Dim mHTML_Table_Body As String
    '    Dim mHTML As String
    '    Dim mFuncionJavascript As String
    '    Dim mEmployeesUsing As Boolean


    '    Try

    '        mEmployeesUsing = Me.EmployeesUsing()

    '        mHTML = ""


    '        'Cabecera de la tabla
    '        mHTML_Table_Header = ""
    '        mHTML_Table_Header = mHTML_Table_Header & "<thead>"
    '        mHTML_Table_Header = mHTML_Table_Header & "     <tr>"
    '        mHTML_Table_Header = mHTML_Table_Header & "         <th style='width:10px;'><h6 align='center'>Código</h6></th>"
    '        mHTML_Table_Header = mHTML_Table_Header & "         <th style='width:150px;'><h6 align='left'>Descripción</h6></th>"
    '        mHTML_Table_Header = mHTML_Table_Header & "         <th style='width:10px;'><h6 align='center'>Alícuota</h6></th>"

    '        If (mEmployeesUsing) Then
    '            mHTML_Table_Header = mHTML_Table_Header & "         <th style='width:10px;'><h6 align='center'>Emplea.</h6></th>"
    '        End If

    '        mHTML_Table_Header = mHTML_Table_Header & "         <th style='width:10px;'><h6 align='center'>Monto</h6></th>"
    '        mHTML_Table_Header = mHTML_Table_Header & "     </tr>"
    '        mHTML_Table_Header = mHTML_Table_Header & "</thead>"




    '        'Creo los renglones de la tabla
    '        mHTML_Table_Body = ""
    '        For i = 0 To dtData.Rows.Count - 1

    '            mHTML_Table_Body = mHTML_Table_Body & "<tr>"
    '            mHTML_Table_Body = mHTML_Table_Body & "<td align='center'>" & dtData.Rows(i).Item("RUBRO_CODE").ToString.Trim & "</td>"
    '            mHTML_Table_Body = mHTML_Table_Body & "<td align='left'>" & dtData.Rows(i).Item("RUBRO_DESCRIPTION").ToString.Trim & "</td>"
    '            mHTML_Table_Body = mHTML_Table_Body & "<td align='center'>" & dtData.Rows(i).Item("RUBRO_ALIQUOT").ToString.Trim & "</td>"

    '            If (mEmployeesUsing) Then
    '                mHTML_Table_Body = mHTML_Table_Body & "<td align='center'> "
    '                mHTML_Table_Body = mHTML_Table_Body & "<input id='txtemployees" & (i + 1).ToString & "' name='txtemployees" & (i + 1).ToString & "' type='text' value='0' style='text-align:center; width:25px;' maxlength='3' onfocus=""javascript:event_focus(this);"" onblur=""javascript:event_focus(this);""/> "
    '                mHTML_Table_Body = mHTML_Table_Body & "</td>"
    '            End If

    '            mHTML_Table_Body = mHTML_Table_Body & "<td align='center'> "
    '            mHTML_Table_Body = mHTML_Table_Body & "<input id='txtamount" & (i + 1).ToString & "' name='txtamount" & (i + 1).ToString & "' type='text' value='0.00' style='text-align:center; width:60px;' maxlength='9' onfocus=""javascript:event_focus(this);"" onblur=""javascript:event_focus(this);""  onkeyup=""javascript:calculate_declaration();""/> "
    '            mHTML_Table_Body = mHTML_Table_Body & "</td>"
    '        Next



    '        'Tabla totales
    '        mHTML_Table_Body = mHTML_Table_Body & "<tr align='right' style='background-color:#999999; font-weight:bold; color:#006600;'>"

    '        If (mEmployeesUsing) Then
    '            mHTML_Table_Body = mHTML_Table_Body & "     <td colspan='4'>Monto total declarado: </td>"
    '        Else
    '            mHTML_Table_Body = mHTML_Table_Body & "     <td colspan='3'>Monto total declarado: </td>"
    '        End If

    '        mHTML_Table_Body = mHTML_Table_Body & "     <td id='td_rubros_total'>0.00</td>"
    '        mHTML_Table_Body = mHTML_Table_Body & "</tr>"




    '        mHTML = "<table id='gridrubros' cellpadding='0' cellspacing='0' border='0' class='table_periods' >" & _
    '                    mHTML_Table_Header.Trim & vbCr & _
    '                    "<tbody>" & mHTML_Table_Body.Trim & "</tbody>" & _
    '                "</table>"



    '        mFuncionJavascript = "ajax_exist_ddjj"
    '        If (CBool(Session("Rectificativa"))) Then
    '            mFuncionJavascript = "declaration"
    '        End If
    '        mHTML = mHTML & " <div id='div_contenedor_botones' style='width:90%; float:left; padding-top:10px; padding-left: 30px;' align='center'> " & _
    '                            " <div id='div_boton_declaration' style='width:49%; float:left'; align='center'> " & _
    '                                " <input type='button' value='Presentar' class='boton' style='width:200px; height:35px; cursor:pointer' onclick=""javascript:" & mFuncionJavascript.Trim & "('false');""/> " & _
    '                            " </div> " & _
    '                            " <div id='div_button_declaration_pay' align='center' style='width:49%; float:right;' align='center'> " & _
    '                                " <input type='button' value='Presentar y pagar' class='boton' style='width:200px; height:35px; cursor:pointer' onclick=""javascript:" & mFuncionJavascript.Trim & "('true');""/> " & _
    '                            " </div> " & _
    '                        " </div> "



    '    Catch ex As Exception
    '        mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
    '                    "<p id='lblmessenger' style='color: Red; font-weight: inherit;'> " & _
    '                        "Error al intentar obtener los datos." & _
    '                    "</p>" & _
    '                "</div>"
    '    End Try


    '    Return mHTML.ToString.Trim
    'End Function


    Private Function AddDDJJ(ByVal pXML As String, ByVal pPeriod As String) As Boolean
        Dim mResult_XML As String
        Dim mOK_ADD As Boolean
        Dim mNumberRecurso As String
        Dim mPeriod() As String


        Try
            mOK_ADD = False
            mResult_XML = ""


            If (pXML.ToString.Trim <> "") Then

                'I Update the dataset with the information of the comercio
                dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))


                'I obtain the number of the recurso
                mNumberRecurso = Me.ObtainNumberRecursoTISH()


                'I obtain the year and the quota
                mPeriod = pPeriod.ToString.Split(CChar("-"))




                'I add the new DDJJ
                Me.mWS = New ws_tish.Service1
                mResult_XML = Me.mWS.AddDDJJ(mNumberRecurso.ToString.Trim, dtDataComercio_AUX.Rows(0).Item("CONTRIBUYENTE_CUIT").ToString.Trim, _
                                             CInt(dtDataComercio_AUX.Rows(0).Item("COMERCIO_NUMERO").ToString.Trim), _
                                             CInt(mPeriod(0).ToString.Trim), CInt(mPeriod(1).ToString.Trim), _
                                             pXML.ToString.Trim)
                Me.mWS = Nothing
            End If


            Session("xml_voucher_add_ddjj") = mResult_XML.ToString.Trim
            Session("is_new_ddjj") = True
            mOK_ADD = True
        Catch ex As Exception
            Session("xml_voucher_add_ddjj") = Nothing
            mOK_ADD = False
            Me.mWS = Nothing
        End Try


        Return mOK_ADD
    End Function



    'ObtainNumberRecursoTISH:
    'This function returns the number of the recurso TISH
    Private Function ObtainNumberRecursoTISH() As String
        Dim mNumberRecurso As String

        Try
            mNumberRecurso = ""
            Me.mWS = New ws_tish.Service1
            mNumberRecurso = Me.mWS.ObtainNumberRecursoTISH()
            Me.mWS = Nothing

        Catch ex As Exception
            mNumberRecurso = ""
        End Try

        Return mNumberRecurso.ToString.Trim
    End Function


    Private Sub GenerarDataSetPDF(ByVal pdsConstanciaData As DataSet)
        Dim dsVoucherDDJJ As DataSet
        Dim mPeriod() As String
        Dim dtRubtos As DataTable

        Try

            'I Update the dataset with the information of the comercio
            dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))


            dtRubtos = New DataTable
            dtRubtos = pdsConstanciaData.Tables("RUBROS").Copy

            'I create the dataset with the data of the comercio and the data of the ddjj
            dsVoucherDDJJ = New DataSet
            dsVoucherDDJJ.Tables.Add(dtDataComercio_AUX)
            dsVoucherDDJJ.Tables.Add(dtRubtos)


            'I obtain the year and the quota
            mPeriod = pdsConstanciaData.Tables("PERIODOS").Rows(0).Item(0).ToString.Split(CChar("-"))


            'I assing then period
            Session("period_voucher_add_ddjj") = mPeriod(0).ToString.Trim & "/" & mPeriod(1).ToString.Trim


            'I assign the dataset a the variable of session
            Session("xml_constancia_add_ddjj") = dsVoucherDDJJ

        Catch ex As Exception
            Session("xml_constancia_add_ddjj") = Nothing
        End Try

    End Sub


    Private Function EmployeesUsing() As Boolean
        Dim mUsing As String

        Try
            mUsing = "N"

            'Me.mWS = New ws_tish.Service1
            'mUsing = Me.mWS.Exist_DDJJ(CInt(pComercioNumber), CInt(mPeriod(0).ToString.Trim), CInt(mPeriod(1).ToString.Trim))
            'Me.mWS = Nothing

        Catch ex As Exception
            mUsing = "N"
        Finally
            Me.mWS = Nothing
        End Try

        Return CBool(mUsing.Trim = "S")
    End Function

End Class