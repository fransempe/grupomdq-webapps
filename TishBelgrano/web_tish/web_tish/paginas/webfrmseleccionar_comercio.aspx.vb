﻿Option Explicit On
Option Strict On

Partial Public Class webfrmseleccionar_comercio
    Inherits System.Web.UI.Page

#Region "Variables"
    Private mWS As ws_tish.Service1
    Private mXML As String

    Private dtDataComercio_AUX As DataTable
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not (IsPostBack) Then

            Session("trade_number") = 0
            Call Me.SloganMunicipalidad()
            Call Me.ValidarSession()
            Call Me.CargarPie()
            Call Me.cargarDatosUsuario()
        Else

            If (Request.Params.Get("__EVENTTARGET").Trim <> "") Then
                Call Me.comercioSeleccionado(Request.Params.Get("__EVENTTARGET").Trim)
            End If
        End If

    End Sub

    Private Sub SloganMunicipalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema TISH."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema TISH."
        End If

    End Sub



    Private Sub cargarDatosUsuario()
        Dim dsDataManager_AUX As DataSet
        Dim strListaComercios As String
        Dim strXMLComercios As String
        Dim comercios As List(Of entidades.comercio)
        Dim strHTML As String


        'Obtengo los datos del usuario y los comercios asociados
        mWS = New ws_tish.Service1
        dsDataManager_AUX = mWS.ObtainDataManager(Session("cuit_manager").ToString.Trim)
        mWS = Nothing


        If (dsDataManager_AUX IsNot Nothing) Then

            'Nombre del usuario logueado
            lblname_user.Text = "Bienvenido usuario: " & dsDataManager_AUX.Tables("Manager").Rows(0).Item("CUIT").ToString.Trim & _
                                " - " & dsDataManager_AUX.Tables("Manager").Rows(0).Item("RAZON_SOCIAL").ToString.Trim


            'Armo un cadena con los números de comercios asociados al usuario, esta cadena la voy a utilizar para obtener
            'los datos de los comercios de FOX
            strListaComercios = ""
            For i = 0 To dsDataManager_AUX.Tables("Trades").Rows.Count - 1
                strListaComercios += dsDataManager_AUX.Tables("Trades").Rows(i).Item("NRO_COMERCIO").ToString.Trim() & "|"
            Next


            'Busco la información de los comercios en FOX
            strXMLComercios = ""
            If (strListaComercios.ToString.Trim <> "") Then
                mWS = New ws_tish.Service1
                strXMLComercios = mWS.getListadoDeComercios(strListaComercios.Trim())
                mWS = Nothing
            End If


            comercios = New List(Of entidades.comercio)()
            comercios = entidades.serializacion.deserializarXML(Of List(Of entidades.comercio))(strXMLComercios.Trim())

            strHTML = "<table id='tabla_comercios' cellpadding='0' cellspacing='0' border='0' class='tabla_rubros_ddjj'>" & _
                      "<thead>" & _
                            "<tr>" & _
                                "<td colspan='4' align='center'><strong>Listado de comercios</strong></td>" & _
                            "</tr>" & _
                            "<tr>" & _
                                "<th><h6 style='width:60px;' align='center'>Comercio</h6></th>" & _
                                "<th><h6 style='width:200px;' align='left'>Razón social</h6></th>" & _
                                "<th><h6 style='width:200px;' align='left'>Contribuyente</h6></th>" & _
                                "<th><h6 style='width:70px;' align='center'>Seleccionar</h6></th>" & _
                            "</tr>" & _
                      "</thead>" & _
                      "<tbody>"



            For Each comercio_aux As entidades.comercio In comercios
                If (comercio_aux.contribuyente <> "Contribuyente sin asignar") Then
                    strHTML += "<tr>" & _
                                    "<td align='center' style='width:10px;'>" & comercio_aux.numero & "</td>" & _
                                    "<td align='left'>" & comercio_aux.razonSocial & "</td>" & _
                                    "<td align='left'>" & comercio_aux.contribuyente & "</td>" & _
                                    "<td style='width:10px;' align='center'>" & _
                                        "<img alt='' src='../imagenes/flecha_d.png' height='20px' title='Seleccionar comercio' style='cursor:pointer';  onclick = 'javascript:comercioSeleccionado(" & comercio_aux.numero & ");' />" & _
                                    "</td>" & _
                               "</tr>"

                Else
                    strHTML += "<tr style='color:#FF0000'; title='El comercio no puede ser seleccionado, ya que no cuenta con un contribuyente asociado'>" & _
                                    "<td align='center' style='width:10px;'>" & comercio_aux.numero & "</td>" & _
                                    "<td align='left'>" & comercio_aux.razonSocial & "</td>" & _
                                    "<td align='left'>" & comercio_aux.contribuyente & "</td>" & _
                                    "<td style='width:10px;' align='center'>" & _
                                    "</td>" & _
                               "</tr>"
                End If
            Next
            strHTML += "</tbody>"
            strHTML += "</table>"


            Me.div_tabla_comercios.InnerHtml = strHTML.Trim()
        End If

    End Sub


    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If


        'Cuit manager
        If (mLogin_OK) Then
            If (Session("cuit_manager") IsNot Nothing) Then
                If (Session("cuit_manager").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("contribuyente_nombre") IsNot Nothing) Then
                If (Session("contribuyente_nombre").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        If (mLogin_OK) Then

            'I Update the dataset
            dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))
            'lblname_user.Text = dtDataComercio_AUX.Rows(0).Item("COMERCIO_NUMERO").ToString.Trim()
        Else
            Response.Redirect("webfrmlogin.aspx", False)
        End If

    End Function

    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            Me.lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            Me.lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If

        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub


    Private Sub comercioSeleccionado(ByVal p_strNroComercio As String)
        Session("trade_number") = CInt(p_strNroComercio.Trim())
        Response.Redirect("webfrmopciones.aspx", False)
    End Sub



End Class