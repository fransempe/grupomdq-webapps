﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmdatos_representante.aspx.vb" Inherits="web_tish.webfrmdatos_representante" %>
<%@ Register src="~/ex_controls/WebUserControl_Menu.ascx" TagName="Subgurim" TagPrefix="web_user_control_menu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
    
        
        <!-- Seteo the resources -->
        <script src="../js/funciones.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../css/estilos_login.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        <%           
            'Seteo el css que voy a utilizar
            Dim mPath_css As String                
            mPath_css = ""
            mPath_css = System.Configuration.ConfigurationManager.AppSettings("css").ToString.Trim()
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_login.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_genericos.css' type='text/css' />")            
        %>    
                
                
        <!-- ajax -->       
        <script type="text/javascript" language="javascript" src="../js/jquery1-2-6-min.js"></script>
        <script type="text/javascript" language="javascript">
        
            /* Carga del logo de la municipalidad */
            function ajax_load_logo(){   
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_logo.ashx",                                        
                    success: see_response_logo
                });
            }
            
            
            function see_response_logo(html){                   
                if (html != '') {
                    var _control = document.getElementById('div_header');                   
                    _control.innerHTML = html;                   
                    return false;
                }
                
            return true;    
            }          
        </script>          
                
        
                <!-- recursos para crear la ventana modal -->
	    <link rel="stylesheet" href="../modal/Assets/LightFace.css" />	
	    <script src="../modal/mootools.js" type="text/javascript"></script>
	    <script src="../modal/mootools-more-drag.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.IFrame.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Image.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Request.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Static.js" type="text/javascript"></script>
	
	    
        <!-- Creo la ventana modal -->
        <script type="text/javascript">    	
	 	   	 	    
	 	    function logout() {	 	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema TISH :: Cerrar sesión', 
				    width: 250,
				    height: 50,
	  			    content: '<div align="left">Usted esta a punto de cerrar su sesión.\n¿Desea continuar?</div>',
	 			    buttons: [		
	 			        {
						    title: 'Aceptar',
						    event: function() { this.close(); window.location = "webfrmlogin.aspx"; }
					    },
					    {
						    title: 'Cerrar',
						    event: function() { this.close();}
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }        
	 	    
	 	    
	 	    function change_trade() {	 	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema TISH :: Cambiar comercio', 
				    width: 300,
				    height: 50,
	  			    content: 'Usted esta a punto de cambiar de comercio\n¿Desea continuar?',
	 			    buttons: [		
	 			        {
						    title: 'Aceptar',
						    event: function() { this.close();window.location= "webfrmseleccionar_comercio.aspx";}
					    },
					    {
						    title: 'Cerrar',
						    event: function() { this.close();}
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }        


        </script>


    	<style type="text/css">
    	    .table_user_data { width:50%;}
    	    .table_user_data_column1{ width:30%;  text-align:left;  font-weight:bold;}
    	    .table_user_data_column2{ width:70%;  text-align:left; }
    	</style>
    	

        
       
       
        <title>Sistema TISH :: Datos del usuario Web</title>
        <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
    	
    </head>


    <body>
    
        
        <!-- div container of the page -->	
        <div id="wrap">
                  
            <!-- div header -->
		    <div id="header">
			
			   <!-- div container font -->
                <div id="container_font">
		            <div  style="width:5%; float:left;  padding:0px 5px 0px 5px;">
                        <a href="#" title="Usar fuente menor" class="link_font" onclick="assign_font('-');">A-</a> 
                    </div>
                    
                    <div  style="width:5%; float:left;">
		                <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 
                    </div>		            
                    
		            <div  style="width:5%; float:left;">
		                <a href="#" title="Usar fuente mayor" class="link_font" onclick="assign_font('+');">A+</a> 
		            </div>	            
                </div>
                
                <!-- <div id="div_header"  class="div_image_logo">                
                    <img  class="image_logo" alt="" src="../imagenes/topiz.jpg"  />
                </div>   -->    
   
                    
			    <h1 id="logo-text">Tasa de Inspección por Seguridad e Higiene</h1>			
			    <h2 id="slogan">
                    <asp:Label ID="lblslogan_municipalidad" runat="server" Text=""></asp:Label></h2>	
		    </div>
	  
	  
	        <!-- div container -->		
	        <div id="content-wrap">
    	  
                 <!-- div sub container -->		
	  		    <div id="main">     	   
				    <!-- div container page title -->
                    <div id="div_title" style="background-color:Black;" align ="left" >
                        <strong class="titulo_page">Sistema TISH :: Datos del usuario Web</strong>
                    </div>

    				
    				
                    
                    <!-- -->
                    <div id="div_container" style="width:100%; height:350px;">
                    

                        <!-- div container right -->                        
                        <div id="div_options"  class="menu_options">                           
                            <h1 align ="left" style="color:Black">Comercio</h1>				
	                        <ul class="sidemenu">
                                <li><a href="webfrmdeclaracionesjuradas.aspx">Ver listado de DDJJ</a></li>
                                <li><a href="webfrmctacte.aspx">Ver cuenta corriente</a></li>                                <li><a href="webfrmopciones.aspx">Datos del comercio</a></li>				                
                                <li><a href="#" onclick="javascript:change_trade();">Cambiar comercio</a></li>			                        
	                        </ul>	
	                        
	                        
	                        <h1 align ="left" style="color:Black">Usuario Web</h1>				
	                        <ul class="sidemenu">
	                            <li><a href="webfrmdatos_representante.aspx">Mis datos</a></li>				                
		                        <li><a href="webfrmcambiar_clave.aspx">Cambiar clave</a></li>	
		                        <li><a href="#" onclick="javascript:logout();">Cerrar sesión</a></li>
	                        </ul>	
                        </div>
                        
                    
                    
                        <!-- div container left -->
                        <div id="div_container_left" style="width:76%; float:left; height:300px;">
                    
                            <!-- greet user -->				
                            <div class="titulo_3" style="padding-top:30px; padding-bottom:10px;" align="center">
                                <asp:Label ID="lblname_manager" runat="server" Text=""></asp:Label>
                                <div style="width:80%;">
                                    <hr class="linea" />
                                </div>
                            </div>
                                
                            
                    
                            
                            <div id="div_table_comercio" align="center" >
                                <table  class="table_user_data">
                                    <tr>
                                        <td>
                                            <table  align ="center" style="width:100%";>
                                                                                                             
                                                <tr>
                                                    <td class="table_user_data_column1">CUIT:</td>                        
                                                    <td class="table_user_data_column2">
                                                        <asp:Label ID="lblCUIT" runat="server" Text="" Font-Bold="false"></asp:Label>                                
                                                    </td>                                                                                                                                                                                                                                                        
                                                </tr>
                                                
                                                
                                                <tr>
                                                    <td class="table_user_data_column1">Razón social:</td>                        
                                                    <td class="table_user_data_column2">
                                                        <asp:Label ID="lblreason_social" runat="server" Text="" Font-Bold="false"></asp:Label>                                
                                                    </td>                                                                                                   
                                                </tr>
                                                
                                                
                                                <tr>
                                                    <td class="table_user_data_column1">Contacto:</td>                        
                                                    <td class="table_user_data_column2">
                                                        <asp:Label ID="lblcontact" runat="server" Text="" Font-Bold="false"></asp:Label>                                
                                                    </td>                                                                                                   
                                                </tr>

                                                
                                                <tr>
                                                    <td class="table_user_data_column1">Dirección:</td>                        
                                                    <td class="table_user_data_column2">
                                                        <asp:Label ID="lbladdress" runat="server" Text="" Font-Bold="false"></asp:Label>                                
                                                    </td>                                                                                                   
                                                </tr>
                                                
                                                <tr>
                                                    <td class="table_user_data_column1">Teléfono:</td>                        
                                                    <td class="table_user_data_column2">
                                                        <asp:Label ID="lbltelephono" runat="server" Text="" Font-Bold="false"></asp:Label>                                
                                                    </td>                                                                                                   
                                                </tr>
                                                
                                                
                                                <tr>
                                                    <td class="table_user_data_column1">E-mail:</td>                        
                                                    <td class="table_user_data_column2">
                                                        <asp:Label ID="lblemail" runat="server" Text="" Font-Bold="false"></asp:Label>                                
                                                    </td>                                                                                                   
                                                </tr>
                                                
                                                
                                                <tr>
                                                    <td class="table_user_data_column1">Tipo de clave:</td>
                                                    <td class="table_user_data_column2">
                                                        <asp:Label ID="lbltype_key" runat="server" Text="" Font-Bold="false"></asp:Label>                                
                                                    </td>                                                                                                   
                                                </tr>
                                               
                                                            
                                            </table>
                                        </td>   
                                    </tr>                                
                                </table>
                            </div>
                            
                                                        
                    
                        </div>
                                         
                    </div>
                    
                    
    			
    					
    						
    						
    				
    			
    				
    								
	  		    </div> 	
    			  
    	  		 	
    		
		    <!-- content-wrap ends here -->
		    </div>
	
	
	        <!-- div footer -->	
		    <div id="footer">
		
		   <div id="div_pie_municipalidad" 
                style="width:70%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		        Municipalidad de
		            <strong> 
		                <asp:Label ID="lblnombre_municipalidad" runat="server" Text="NOMBRE_MUNICIPIO"></asp:Label> 
                    </strong> 
                    
				<br />
				Teléfono: 
				    <strong> 
                        <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                    </strong> 
                 | Email: 
				    <strong> 
                        <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                    </strong> 
                
			</div>
		   
		   <div id="div_pie_grupomdq" class="footer_grupomdq" >
				Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ S.A</a></strong>  
				<br />
				Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				<a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
               <asp:Label ID="lblversion" runat="server" Text=""></asp:Label>
		   </div>
			
			
		</div>	

        
        </div>	
        
    </body>
</html>

