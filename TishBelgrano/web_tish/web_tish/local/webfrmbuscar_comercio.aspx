﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmbuscar_comercio.aspx.vb" Inherits="web_tish.webfrmbuscar_comercio" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <script src="../js/funciones.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../css/estilos.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_botones.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        
           <!-- Esta funcion da el efecto de cargando... miestras va al server -->
        <link href="../efecto_ajax/cssUpdateProgress.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript">
	        var ModalProgress = '<%= ModalProgress.ClientID %>';         
        </script>
        
        <title>Sistema TISH.-</title>	
        
        
        <script type="text/javascript">

    function seleccionado(codigo) {    
    
        //return confirm("Usted esta a punto de cerrar su sesion.\n¿Desea continuar?")
    }
    
    
    /* efecto_over: */
    /* Esta Funcion ASIGNA ESTILO a un BOTON */          
    function efecto_over(boton){
        boton.className = "boton_gris"; 	        
    }
    
    /* efecto_out: */
    /* Esta Funcion ASIGNA ESTILO a un BOTON */          
    function efecto_out(boton){
        boton.className = "boton_verde";    
    }
    
   
</script>
        
        
    </head>

    <body>
    
        
        <!-- wrap starts here -->	
        <div id="wrap">
         
		<div id="header">
		 
		 
		     <!-- div container font -->
            <div id="div_container_font" style="width:60%; height:20%; float:left; padding-left:10px;">			        
		        <div  style="width:7%; float:left;  padding:0px 5px 0px 5px;">
                    <a href="#" title="Usar fuente menor" style="font-family: Jokerman; font-size: large; font-weight: bold;" onclick="assign_font('-');">A-</a> 
                </div>
                
                <div  style="width:7%; float:left;">
		            <a href="#" title="Usar fuente por defecto" style="font-family: Jokerman; font-size: large; font-weight: bold;" onclick="assign_font('=');">A</a> 
                </div>		            
                
		        <div  style="width:7%; float:left;">
		            <a href="#" title="Usar fuente mayor" style="font-family: Jokerman; font-size: large; font-weight: bold;" onclick="assign_font('+');">A+</a> 
		        </div>	            
            </div>
                    
		    
		     		 
		 
			<h1 id="logo-text">Tasa de Seguridad e Higiene</h1>			
			<h2 id="slogan">Sistema TISH.</h2>	
					
					
						
			
		</div>
	  
	  <!-- content-wrap starts here -->
	  <div id="content-wrap">
	  
	  
	   <!-- div container right -->
                <div id="div_optiones" style="width:27%;  height:100%; float:right;">
                
                    <div id="div_comercio" style=" padding-top:20px;">
                        <div align="center">
                            <table class="modelborder"  style="width:95%">
                                <tr>
                                    <td  rowspan="2"><img alt="" src="../imagenes/user.jpg"  style=" width:60px; height:60px; border:none"/></td>
                                    <td><p  class="titulo_usuario"align ="left" style="color:Black; height: 25px;">Bienvenido</p></td>
                                </tr>
                                <tr>                                
                                    <td>
                                        Usuario: <asp:Label ID="lblnombre_usuario" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                
                                
                            </table>
                        </div>
                       
                        
                        
                        
                        
                    </div>
                    
                    
                    <h1 align ="left" style="color:Black">Opciones</h1>                    			
	                <ul class="sidemenu">               
                        
                        <li><a href="../webfrmindex.aspx" onclick="javascript:return cerrar_sesion_usuario();">Cerrar sesion</a></li>	                					
	                </ul>	
                </div>
	    
	  
	  
	  		<div id="main"> 
					 
				<p>
				    <strong>Sistema TISH:</strong>
				    Esta función le permitirá realizar la búsqueda de un proveedor por cualquier columna de la grilla.
				    <br />
				    Columnas: (código, razón social o cuit)
			    </p>  
			    
 			    				
			    
                <h1>BUSCADOR DE COMERCIOS</h1>
                
             
                    
                    <form id="formulario" runat="server">
                    
                        <script type="text/javascript" src="../efecto_ajax/jsUpdateProgress.js"></script>		
		                <asp:ScriptManager ID="ScriptManager1" runat="server">
                        </asp:ScriptManager>
		                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
		                        <ContentTemplate >
    		   
    		        
    		                
                                 <div id="buscador" align="center" style="width:100%; ">                                 
                                    <table border="1"  align="center" style=" width:100%">
                                        <tr>
                                            <td align="center" style="width:30%">                                               
                                                <asp:DropDownList ID="cmbtipo_busqueda" runat="server" Width="150px">
                                                    <asp:ListItem>Codigo</asp:ListItem>
                                                </asp:DropDownList>                                                
                                            </td>
                                            
                                            <td align="center" style="width:40%">
                                                <asp:TextBox ID="txtbuscar" runat="server" Width="90%" CssClass="textbox"></asp:TextBox>                                                 
                                            </td>
                                            
                                            <td align="center" style="width:30%">
                                                <asp:Button ID="btnbuscar" runat="server" Text="Buscar Comercio"   CssClass="boton_verde" Height="40px" Width="150px"  onmouseover="efecto_over(this);" onmouseout="efecto_out(this);"/>                                                
                                            </td>                                            
                                        </tr>
                                    </table>                                      
                                </div>
                                 
                                
                                <div id="div_mensaje_busqueda" align="center" style="width:100%;"  runat="server" visible ="false">
                                    <h1>
                                        <asp:Label ID="lblmensaje_busqueda" runat="server" Text="RESULTADO DE LA BUSQUEDA"></asp:Label>
                                    </h1>                                    
                                </div>
                    
                    
                                <div id="id_tabla" align="center" style="width:100%;">
                                    <table id="tabla_login" border="1" cellspacing="10" style="border-style: double; width: 100%; height: 100%; background-color: #FFFFFF; color: #000000;" align="center">
                    		            
                                            <tr>
                    		                
                                                <td class="">
                                                <asp:GridView ID="gvcomercios" runat="server" AutoGenerateColumns="False" 
                                                        BackColor="White" BorderColor="#999999" 
                                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="1" ForeColor="Black" 
                                                        GridLines="Vertical" PageSize="5" Width="100%" AllowSorting="True" 
                                                        EnableTheming="True" >
                                                    
                                                        <FooterStyle BackColor="#CCCCCC" />
                                                            <RowStyle ForeColor="Black" />
                                                                <Columns>
                                                                    <asp:BoundField HeaderText="Número" DataField="COMERCIO_NUMERO" 
                                                                        HeaderStyle-Width="55px">
                                                                        <HeaderStyle Width="55px" />
                                                                        <ItemStyle Width="55px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    </asp:BoundField>
                                                                    
                                                                    <asp:BoundField DataField="COMERCIO_NOMBRE" HeaderText="Nombre" >                                     
                                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                    </asp:BoundField>                                     
                                                                    <asp:BoundField DataField="COMERCIO_DOMICILIO" HeaderText="Domicilio" />
                                                                    <asp:BoundField DataField="CONTRIBUYENTE_CUIT" HeaderText="Cuit" />
                                                                    <asp:ButtonField Text="Selc." >
                                                                        <ItemStyle ForeColor="#009900" />
                                                                    </asp:ButtonField>
                                                                </Columns>
                                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                                        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                                                        <AlternatingRowStyle BackColor="#CCCCCC" />
                                                </asp:GridView>
                                                </td>
                                                
                                            </tr> 
                                                                                    
                                            
                                        </table>    	
                                </div>
                        
                        
                                </ContentTemplate >		
                            </asp:UpdatePanel>
                            
                            
                            
                            <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
		                    <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server">
			                    <ProgressTemplate>
				                    <div style="position: relative; top: 30%; text-align: center;">
					                    <img src="../efecto_ajax/loading.gif" style="vertical-align: middle" alt="Processing" />
					                    Cargando ...
				                    </div>
			                    </ProgressTemplate>
		                    </asp:UpdateProgress>
	                    </asp:Panel>
	    
	                    <ajaxToolkit:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress" BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
                        
                            
		             </form>		         
		       </div>
				
					
						
				
			
				
								
	  		</div> 	
			  
	  		 	<div id="footer">
		
		   <div id="div_pie_municipalidad" 
                style="width:72%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		        Municipalidad de
		            <strong> 
		                <asp:Label ID="lblnombre_municipalidad" runat="server" Text="NOMBRE_MUNICIPIO"></asp:Label> 
                    </strong> 
                    
				<br />
				Teléfono: 
				    <strong> 
                        <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                    </strong> 
                 | Email: 
				    <strong> 
                        <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                    </strong> 
                
			</div>
		   
		   <div id="div_pie_grupomdq"  style="width:26%; height:100%;  float:right; padding-top:5px; ">
				Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ.-</a></strong>  
				<br />
				Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				<a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>
		   </div>
			
			
		</div>	
		
		<!-- content-wrap ends here -->
		</div>
		
		

<!-- wrap ends here -->		
</div>	



    </body>
</html>