﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmabm_fechas_vto.aspx.vb" Inherits="web_tish.webfrmabm_fechas_vto" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Sistema TISH.-</title>	
        <script src="../js/funciones.js" type="text/javascript"></script>
        <script src="../js/jquery.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../css/estilos_listado.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_botones.css" type="text/css" />
        
        
        
        <!-- ajax -->       
        <script type="text/javascript" language="javascript" src="../js/jquery1-2-6-min.js"></script>
        <script type="text/javascript" language="javascript">
        
            jQuery.noConflict();
            function ajax_load_manager(p_cuit){                                
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/Handler_load_manager.ashx",                    
                    data:"action=down&cuit=" + p_cuit, 
                    success: see_response
                });
            }
            
            
            function see_response(html){                   
                if (html != '') {
                    var _control = document.getElementById('div_data_manager');
                    _control.style.display = 'none';
                    _control.innerHTML = html;
                    show_div();
                    return false;
                }
                
            return true;    
            }          
            
            
            
	        function show_div() {	
	            var _control;

		        jQuery("#div_data_manager").slideToggle();		
		        
		        _control = document.getElementById('div_button');
		        if (!control_exists(document.getElementById('lblmessenger'))) {		            
		            _control.style.display = 'block';            
		            
		            document.getElementById("txtmotive").focus();

		        } else {
		            _control.style.display = 'none';            
		        }		            
	        }
	        
        </script>
        
        
        
        
        <script type="text/javascript">
            var _control;
            var _codehtml;
        
        
            function load_manager() {  
            
                if (validate_data()) {
                
                    _codehtml = '<table border="0" width="100%" align="center">' +
                                    '<tr>' + 
                                        '<td align="right" style="width:35%;"><img alt="" src="../imagenes/load.gif"/ width="24px" height="24px" /></td>' + 
                                        '<td align="left" style="width:75%;"><p>Cargando datos ...</p></td>' +                                      
                                    '</tr>' +
                                '</table>';


                    
                    _control = document.getElementById('div_data_manager');                
                    _control.innerHTML = _codehtml;
                    
                              
                    
                    ajax_load_manager(window.document.getElementById('txtcuit').value);
                    
                
                }
            return false;
            }
        
        
        
             function validate_data() {

                    //Cadena Vacia
                    if (control_vacio(window.document.getElementById('txtcuit'),'Número de comercio:\nDebe ingresar un número de comercio.')) {
                       return false;
                    }
                    
                    //Cadena Valida
                    if (!validar_cadena_solo_numeros(window.document.getElementById('txtcuit'),'Número de comercio')) {          
                        return false;
                    }
                    
                return true;
                }
                
                
            function down_user() {
                var _messenger;
                
                _messenger = false;
                _messenger = confirm('Usted esta a punto de dar de baja el usuario.\n ¿Desea continuar?');    
                if (_messenger) {
                
                    _codehtml = '<table border="0" width="100%" align="center">' +
                                    '<tr>' + 
                                        '<td align="right" style="width:35%;"><img alt="" src="../imagenes/load.gif"/ width="24px" height="24px" /></td>' + 
                                        '<td align="left" style="width:75%;"><p>Generando clave ...</p></td>' +                                      
                                    '</tr>' +
                                '</table>';
                
                     
                    _control = document.getElementById('div_data_manager');                
                    _control.innerHTML = _codehtml;
               
                
                    __doPostBack("btndown_user",'')               
                   
                    return true;
                }
                
            return false;
            }
                
                
                
                
        </script>
        
        
        
        <script type="text/javascript">

            function to_validate_data() {

                /* -- CUIT -- */
                //control empty
                if (control_vacio(window.document.getElementById('txtcuit'),'CUIT:\nDebe ingresar un número de cuit.')) {
                   return false;
                }
                
                //string validate
                if (!validar_cuit(window.document.getElementById('txtcuit'),'CUIT')) {          
                    return false;
                }
                
                
                
                //motive down
                if (window.document.getElementById('txtmotive').value == '') {
                    alert('Debe ingresar un motivo para la baja');
                    return false
                }
                
                // messenger user web
                var _messenger = false;
                _messenger = confirm('Usted esta a punto de dar de baja este Representante/Titular.\n¿Desea continuar?');                
                if (!_messenger) {
                    return false;
                }
                


                
                _codehtml = '<table id= "prueba" border="0" width="100%" align="center">' +
                                '<tr>' + 
                                    '<td align="right" style="width:35%;">hola</td>' + 
                                    '<td align="left" style="width:75%;"><p>caua</p></td>' +                                      
                                '</tr>' +
                            '</table>';
                
                     
                _control = document.getElementById('div_button');                
                _control.innerHTML = _codehtml;
 
                
                
                //__doPostBack                
                 var _motive = window.document.getElementById('txtmotive').value;               
                 //__doPostBack('btndown_user', _motive); 
                 __doPostBack('btndown_user',''); 
                
                
            return true;
            }
            
    
    
            function to_validate_load_manager() {
                             
                //control empty
                if (control_vacio(window.document.getElementById('txtcuit'),'CUIT:\nDebe ingresar un número de cuit.')) {
                   return false;
                }
                
                //string validate
                if (!validar_cuit(window.document.getElementById('txtcuit'),'CUIT')) {          
                    return false;
                }
            
            return true;
            }
            
    
     
    
 
    
    
    
    
    /* Esta Funcion ASIGNA ESTILO a un BOTON */          
    function efecto_over(boton){
        boton.className = "boton_gris"; 	        
	}
	    

    /* Esta Funcion ASIGNA ESTILO a un BOTON */          
  	function efecto_out(boton){
        boton.className = "boton_verde";    
	}
</script>

<!-- recursos para crear la ventana modal -->
	    <link rel="stylesheet" href="../modal/Assets/LightFace.css" />	
	    <script src="../modal/mootools.js" type="text/javascript"></script>
	    <script src="../modal/mootools-more-drag.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.IFrame.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Image.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Request.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Static.js" type="text/javascript"></script>
        

        <!-- Logout -->
        <script type="text/javascript">
        	function logout() {	 	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema TISH :: Cerrar sesión', 
				    width: 250,
				    height: 50,
	  			    content: 'Usted esta a punto de cerrar su sesión.\n¿Desea continuar?',
	 			    buttons: [		
	 			        {
						    title: 'Aceptar',
						    event: function() { this.close();window.location= "../webfrmindex.aspx";}
					    },
					    {
						    title: 'Cerrar',
						    event: function() { this.close();}
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }        
        </script>



        
        
    </head>



    <body>
    
        
        <!-- wrap starts here -->	
        <div id="wrap">
                     
		    <div id="header">
		          
		        <!-- div container font -->
                <div id="div_container_font" style="width:60%; height:20%; float:left; padding-left:10px;">			        
		        <div  style="width:7%; float:left;  padding:0px 5px 0px 5px;">
                    <a href="#" title="Usar fuente menor" style="font-family: Jokerman; font-size: large; font-weight: bold;" onclick="assign_font('-');">A-</a> 
                </div>
                
                <div  style="width:7%; float:left;">
		            <a href="#" title="Usar fuente por defecto" style="font-family: Jokerman; font-size: large; font-weight: bold;" onclick="assign_font('=');">A</a> 
                </div>		            
                
		        <div  style="width:7%; float:left;">
		            <a href="#" title="Usar fuente mayor" style="font-family: Jokerman; font-size: large; font-weight: bold;" onclick="assign_font('+');">A+</a> 
		        </div>	            
            </div>
                    
		          
		    <!-- div header-->
			<h1 id="logo-text">Tasa de Seguridad e Higiene</h1>			
			<h2 id="slogan">Sistema TISH.-</h2>	
					
						
			
		</div>
	  
	  
            <!-- content-wrap starts here -->
            <div id="content-wrap">
    	  
    	  
                <!-- div container right -->
                <div id="div_optiones" style="width:27%;  height:100%; float:right;">
                    
                    <div id="div_user" style=" padding-top:20px;">
                            <div align="center">
                                <table class="modelborder"  style="width:95%">
                                    <tr>
                                        <td  rowspan="2"><img alt="" src="../imagenes/user.jpg"  style=" width:60px; height:60px; border:none"/></td>
                                        <td><p  class="titulo_usuario"align ="left" style="color:Black; height: 25px;">Bienvenido</p></td>
                                    </tr>
                                    <tr>                                
                                        <td>
                                            Usuario: <asp:Label ID="lblname_user" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        
                    <h1 align ="left" style="color:Black">Opciones</h1>                    			
                    <ul class="sidemenu">                                       	                    
                        <li><a href="webfrmopciones_rafam.aspx" >Volver a opciones</a></li>	                					
                        <li><a href="../webfrmindex.aspx" onclick="javascript:logout();">Cerrar sesion</a></li>	                					
                    </ul>	
                </div>
    	    
    	  
    	  
  		        <div id="main"> 
    			
    			    
                    <p>
			            <strong>Sistema TISH:</strong>
			            Esta función le permita a usted realizar la generación o el blanqueo de la clave de acceso Web para el comercio seleccionado.
		            </p>  

    		
                    
                    <form id="formulario" runat="server">
                        
                        <!-- div container -->
                        <div id="div_container" align="center" style="padding-bottom:30px;"  runat="server">
                                         
                            <div id="div_conteiner_data" style="padding-top:20px; width:100%;" align="center">
                           
                               <table id="table_data_manager" border="0" cellspacing="10" style="border-style: double; width: 80%; height: 220px; background-color: #FFFFFF; color: #000000;" align="center">
                
                                   <tr>
                                        <td class="column_data1" align="right">Recurso:</td>
                                        <td class="column_data2">
                                            <asp:TextBox ID="txresource" runat="server" CssClass="textbox" MaxLength="11" Width="80%" ></asp:TextBox>                                        
                                        </td>
                                    </tr>    
                                    
                                    <tr>
                                        <td class="column_data1" align="right">Año:</td>
                                        <td class="column_data2">
                                            <asp:TextBox ID="txtyear" runat="server" CssClass="textbox" MaxLength="11" Width="80%" ></asp:TextBox>                                        
                                        </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <td class="column_data1" align="right">Cuota:</td>
                                        <td class="column_data2">
                                            <asp:TextBox ID="txtquota" runat="server" CssClass="textbox" MaxLength="11" Width="80%" ></asp:TextBox>                                        
                                        </td>
                                    </tr>


                                    <tr>
                                        <td class="column_data1" align="right">Fecha DDJJ:</td>
                                        <td class="column_data2">
                                            <asp:TextBox ID="txtday_ddjj" runat="server" CssClass="textbox" MaxLength="11" Width="80%" ></asp:TextBox>                                        
                                        </td>
                                    </tr>


                                    <tr>
                                        <td class="column_data1" align="right">Fecha cuota:</td>
                                        <td class="column_data2">
                                            <asp:TextBox ID="txtday_quota" runat="server" CssClass="textbox" MaxLength="11" Width="80%" ></asp:TextBox>                                        
                                        </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <td  colspan=2>
                                            <div style="width:100%">
                                                <asp:Button ID="btnadd" runat="server" 
                                                                                                                                            
                                                Text="Aceptar" BorderStyle="None" 
                                                CssClass="boton_verde" Height="35px" Width="100px" 
                                                onmouseover="efecto_over(this)"
                                                onmouseout="efecto_out(this)"
                                                ToolTip="Con esta opción usted grabara los datos cargados."                                                           
                                            />                        
                                            </div>                
                                        </td>
                                    </tr> 
                                 
                                   
                                                                     
                                </table>
                                            
                                
                               <!-- div container buttons-->            
                               <div id="div_button" align="center" style="padding-top:15px; display:none;">
                                    <table id="table_buttons">
                                        <tr>
                                            <td align="center">
                                                <asp:ImageButton ID="btndown_user" runat="server" Height="32px" ImageUrl="~/imagenes/delete_user.jpg" Width="32px" />                                                                                                                    
                                            </td>
                                             <td><strong>Baja de usuario</strong></td>
                                        </tr>
                                    </table>
                                </div>                                                                      
                             </div>
                        
	                    </div>
	                    
	                    
                       
	                </form>
    				      								
  		        </div> 	
    
	        </div>
		
		
		
		    <!-- div data footer -->
		    <div id="footer">	
		       <div id="div_pie_municipalidad" 
                    style="width:72%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		            Municipalidad de
		                            <strong> 
		                    <asp:Label ID="lblnombre_municipalidad" runat="server" Text="NOMBRE_MUNICIPIO"></asp:Label> 
                        </strong> 
                        
				    <br />
				    Teléfono: 
				        <strong> 
                            <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                        </strong> 
                     | Email: 
				        <strong> 
                            <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                        </strong> 
                    
			    </div>
    		   
		       <div id="div_pie_grupomdq"  style="width:26%; height:100%;  float:right; padding-top:5px; ">
				    Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ.-</a></strong>  
				    <br />
				    Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				    <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>
		       </div>
		    </div>	
		    
        </div>	



    </body>
</html>
