PROCEDURE _EmitircompPagoDeudaVenc (AuxArchComp as string, XMLDeudaVenc as String, ambiente as Logical) as String
	PRIVATE AuxHuboError
	PRIVATE mFechaVTOInteres as Date
	
	
	AuxHuboError = NOT ambiente
	SET ORDER TO imponibl IN rec_cc
	AuxCantMaxReng = 0
	AuxGrupoCompWeb = 0
	
	XMLTOCURSOR(XMLDeudaVenc,'P_ArchMovSelec')
	SELECT P_ArchMovSelec
	IF UPPER(ALIAS()) # UPPER('P_ArchMovSelec')
		AuxHuboError = .T.
		M_Mensaje = 'Imposible generar cursor de datos.'
	ENDIF
	
	IF NOT AuxHuboError
		AuxCantMaxReng = VAL(Get_Para('RECUR','CANTRENGCOMPWEB'))
		IF EMPTY(AuxCantMaxReng)
			AuxHuboError = .T.
			M_Mensaje = 'El par�metro de configuraci�n CANTRENGCOMPWEB no est� definido o est� mal definido. No se pueden generar comprobantes de pago para cancelaci�n de deuda.'
		ENDIF
		
		GO TOP IN P_ArchMovSelec
		* Debido a problemas de conversi�n con la funci�n XMLTOCURSOR el primer registro
		*	se utiliza como m�scara de campos y, por la tanto, se omite.
		SKIP IN P_ArchMovSelec
		_TIPOIMP_ = P_ArchMovSelec.tipoimponible
		_NROIMP_ = P_ArchMovSelec.nroimponible
		
		IF NOT AuxHuboError
			AuxGrupoCompWeb = VAL(Get_Para('RECUR','GRUPO_COMP_WEB'))
			IF EMPTY(AuxGrupoCompWeb)
				AuxHuboError = .T.
				M_Mensaje = 'El par�metro de configuraci�n GRUPO_COMP_WEB no est� definido o est� mal definido. No se pueden generar comprobantes de pago para cancelaci�n de deuda.'
			ELSE
				*Ya sea que se trabaje con base de datos en l�nea o replicada siempre chequeamos que el
				*numerador exista en numerad. No chequeamos que exista en numeradw porque si la base
				*est� replicada s�lo basta con chequear en los datos del sistema en l�nea.
				IF NOT SEEK('COMPR '+ STR(AuxGrupoCompWeb,2,0),'numerad')
					AuxHuboError = .T.
					M_Mensaje ='El valor ' + ALLTRIM(STR(AuxGrupoCompWeb,2,0)) + ' no corresponde a un numerador v�lido de comprobantes. No se pueden generar comprobantes de pago para cancelaci�n de deuda.'
				ENDIF
			ENDIF
		ENDIF
			   	
*		IF NOT AuxHuboError
*			AuxFechaVto1 = EVALUATE(Get_Para('RECUR','FECHA_VTO'))
*			IF EMPTY(AuxFechaVto1)
*				AuxHuboError = .T.
*				M_Mensaje ='El par�metro de configuraci�n FECHA_VTO no est� definido o est� mal definido. No se pueden generar comprobantes de pago para cancelaci�n de deuda.'
*			ENDIF
*		ENDIF


		IF NOT AuxHuboError
			*AuxFechaVto1 = EVALUATE(Get_Para('RECUR','FECHA_VTO_WEB'))
			AuxFechaVto1 = _ObtenerFechaVTOWebCalculada()
			IF EMPTY(AuxFechaVto1)
				AuxHuboError = .T.
				M_Mensaje ='El par�metro de configuraci�n FECHA_VTO_WEB no est� definido o est� mal definido. No se pueden generar comprobantes de pago para cancelaci�n de deuda.'
			ENDIF
		ENDIF
		
		
		IF NOT AuxHuboError
			m.cuit1 = 0 
			m.cuit2 = 0 
			m.nombretitular = ''
			m.nombreresppago = ''
			
			=FCONTVIN(P_ArchMovSelec.tipoimponible,P_ArchMovSelec.nroimponible,.F.,.F.,@m.cuit1,@m.cuit2)
			* Si el imponible es un contribuyente y no tiene ninguna entrada en VINC_IMP ==> ese mismo
			*	contribuyente se toma c�mo titular titular.
			IF m.cuit1 <= 0 AND P_ArchMovSelec.tipoimponible = 'N'
				m.cuit1 = P_ArchMovSelec.nroimponible
			ENDIF
			
			IF m.cuit1 > 0
				IF SEEK (STR(m.cuit1,10,0),'contrib')
					m.nombretitular = STRTRAN(contrib.nomb_cont, '&', '&amp;')
				ENDIF
			ENDIF
			
			IF m.cuit2 > 0
				IF SEEK (STR(m.cuit2,10,0),'contrib')
					m.nombreresppago = STRTRAN(contrib.nomb_cont, '&', '&amp;')
				ENDIF
			ENDIF
			
			AuxArchComp = AuxArchComp + Indent(1) + '<IMPONIBLE>' + _S_
			AuxArchComp = AuxArchComp + Indent(2) + '<TIPOIMPONIBLE>' + ;
				ALLT(P_ArchMovSelec.tipoimponible) + '</TIPOIMPONIBLE>' + _S_
			AuxArchComp = AuxArchComp + Indent(2) + '<NROIMPONIBLE>' + ;
				ALLT(STR(P_ArchMovSelec.nroimponible,10,0)) + '</NROIMPONIBLE>' + _S_
			AuxArchComp = AuxArchComp + Indent(2) + '<TITULAR>' + ALLT(m.nombretitular) + ;
				'</TITULAR>' + _S_
			AuxArchComp = AuxArchComp + Indent(2) + '<DOMICILIO>' + ;
				ALLT(ALLT(contrib.nomb_calle) + ' ' + ALLT(contrib.puerta) + ' ' + ;
				ALLT(contrib.puertabis) + ' ' + ALLT(contrib.piso) + ' ' + ;
				ALLT(contrib.depto)) + '</DOMICILIO>' + _S_
			AuxArchComp = AuxArchComp + Indent(2) + '<LOCALIDAD>' + ;
				IIF(NOT EMPTY(contrib.cod_post),'(' + ALLT(contrib.cod_post) + ;
				') ','') + ALLT(contrib.nomb_loc) + '</LOCALIDAD>' + _S_
			AuxArchComp = AuxArchComp + Indent(1) + '</IMPONIBLE>' + _S_
			AuxArchComp = AuxArchComp + Indent(1) + '<COMPROBANTES>' + _S_

		ENDIF

		IF NOT AuxHuboError
			AuxCantReng = 0
			AuxTipoInc  = ''
			AuxNroComp  = 0
			
			DO WHILE NOT EOF('P_ArchMovSelec') AND NOT AuxHuboError
				* Si es el primer movimiento o si se llen� la cantidad m�xima de renglones del
				*	comprobante hay que generar un nuevo comprobante
				IF  (AuxCantReng = 0) OR (AuxCantReng = AuxCantMaxReng)
					* Si se llen� un comprobante hay que generarlo
					IF (AuxCantReng = AuxCantMaxReng)
					  IF ALLT(_DATOSREPLICADOS) = 'S'
						  GrabarComprobWPago()
						  * Agregar comprobante al XML de salida
						  AgregCompWAArchSalida (@AuxArchComp)
					  ELSE
						  GrabarComprobPago()
						  * Agregar comprobante al XML de salida
						  AgregCompAArchSalida (@AuxArchComp)
					  ENDIF
					ENDIF
					
					m.tot_ori = 0
					m.tot_act = 0
					m.tot_int = 0
					
					AuxCantReng = 0
					AuxTipoInc  = ''
					AuxNroComp  = 0
					
					IF ALLT(_DATOSREPLICADOS) = 'S'
					  IF NOT FPrxNumW('COMPR', AuxGrupoCompWeb, .T., .T., .F., .F., @AuxNroComp, @AuxTipoInc)
						AuxHuboError = .T.
						M_Mensaje = 'No se pudo generar la cabecera del comprobante.'
					  ENDIF
					ELSE
					  IF NOT FPrxNum('COMPR', AuxGrupoCompWeb, .T., .T., .F., .F., @AuxNroComp, @AuxTipoInc)
						AuxHuboError = .T.
						M_Mensaje = 'No se pudo generar la cabecera del comprobante.'
					  ENDIF
					ENDIF
				
					IF NOT AuxHuboError
						AuxDVComp = FDigCom(AuxGrupoCompWeb, AuxNroComp, P_ArchMovSelec.tipoimponible, P_ArchMovSelec.nroimponible)
					ENDIF
				ENDIF
				
				IF NOT AuxHuboError
					AuxCantreng = AuxCantreng + 1
					
					*_SEEK_ = P_ArchMovSelec.tipoimponible + STR(P_ArchMovSelec.nroimponible,10,0) + P_ArchMovSelec.recurso + STR(P_ArchMovSelec.anio,4,0) + STR(P_ArchMovSelec.cuota,3,0) + STR(P_ArchMovSelec.nromov,3,0)
					_SEEK_ = P_ArchMovSelec.tipoimponible + STR(P_ArchMovSelec.nroimponible,10,0) + P_ArchMovSelec.recurso + STR(P_ArchMovSelec.anio,4,0) + STR(P_ArchMovSelec.cuota,3,0) + STR(P_ArchMovSelec.nromov,3,0)
					IF SEEK	(_SEEK_,'rec_cc')
						* Determinar si elmovimiento est� prorrogado para no calcularle recargos
						*	por mora
						IF SEEK(rec_cc.recurso + STR(rec_cc.anio,4,0) + ;
								STR(rec_cc.cuota,3,0) + SUBSTR(rec_cc.id_orig,1,5),'rec_fec') ;
								AND (rec_fec.fe_pro1 >= DATE())
							REPLACE rec_cc.fecven_mov WITH rec_fec.fe_pro1
						ENDIF
						
						m.actualiz = 0
						m.interes  = 0
						
						IF rec_cc.fecven_mov < DATE()
						
							mFechaVTOInteres = _ObtenerFechaVTOIntereses()
							=FACTINT(rec_cc.fecven_mov, mFechaVTOInteres, rec_cc.imp_mov, ;
									@m.actualiz, @m.interes)
						ENDIF
						
						m.imp_ori = ROUND(VAL(rec_cc.imp_mov),2)
						m.total = m.imp_ori + m.actualiz + m.interes
						m.tot_ori = m.tot_ori + m.imp_ori
						m.tot_act = m.tot_act + m.actualiz
						m.tot_int = m.tot_int + m.interes
						
						* Rengl�n del comprobante
			            IF ALLTRIM(_DATOSREPLICADOS) = 'S'
						  GrabarComp_renwPago()
						ELSE
						  GrabarComp_renPago()
						ENDIF
					ELSE
						AuxhuboError    = .T.
						M_Mensaje = 'No se encontr� en la cuenta corriente el movimiento ' + P_ArchMovSelec.recurso + ;
										   '-' + STR (P_ArchMovSelec.anio,4,0) + '-' + STR(P_ArchMovSelec.cuota,3,0) + '-' + STR(P_ArchMovselec.nromov,3,0) + ;
										    '. No se pueden generar comprobantes de pago para cancelaci�n de deuda.'
					ENDIF			
				ENDIF					
				SKIP IN P_ArchmovSelec
			ENDDO
			
			IF AuxCantReng > 0
            	IF ALLTRIM(_DATOSREPLICADOS) = 'S'
					GrabarComprobWPago()	
					* Agregar comprobante al XML de salida
					AgregCompWAArchSalida (@AuxArchComp)
				ELSE
					GrabarComprobPago()	
					* Agregar comprobante al XML de salida
					AgregCompAArchSalida (@AuxArchComp)
        		ENDIF
			ENDIF
		ENDIF
		AuxArchComp = AuxArchComp + Indent(1) + '</COMPROBANTES>' + _S_		
	ELSE
		M_Mensaje = 'Ha habido un error en el procesamiento de los datos.'
	ENDIF
	
	RETURN NOT AuxHuboError
ENDPROC	

