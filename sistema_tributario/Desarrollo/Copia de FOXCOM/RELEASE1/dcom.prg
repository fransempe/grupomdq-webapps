*_FILE_ = FOPEN('c:\xml\deudaven.xml')
*nBytes = FSEEK(_FILE_,0,2)
*= FSEEK(_FILE_,0,0)

*IF CrearTodo()
*	? ConsultarDeuda('I',75000)
*ELSE
*	? 'Error'
*ENDIF

*FCLOSE(_FILE_)

DEFINE CLASS recurweb as Session olepublic

	***********************************************************************************
	*	Constructor
	***********************************************************************************
	PROCEDURE Init as Boolean
		PUBLIC AmbienteOK
		AmbienteOK = AmbienteWeb()
		
		RETURN .T.
	ENDPROC
	***********************************************************************************
	
	
	
	***********************************************************************************
	*	Crear todo
	***********************************************************************************
	PROCEDURE CrearTodo as Boolean
		BorrarTodo()
		
		AmbienteOK = AmbienteWeb()
		
		RETURN AmbienteOK
	ENDPROC
	***********************************************************************************
	
	
	
	***********************************************************************************
	*	ConsultarDeuda
	***********************************************************************************
	PROCEDURE ConsultarDeuda (TipoImp as String, NroImp as Integer) as String
		PRIVATE retorno
		
		AuxArchDeud = '<?xml version="1.0" encoding="UTF-8"?>' + _S_
		AuxArchDeud = AuxArchDeud + '<VFPDATA>' + _S_

		IF AmbienteOK
			EfimuniWeb()
		
			* BEGIN Apertura de tablas
			=UseT('recur\pla_imp')
			SELECT pla_imp
			SCATTER MEMVAR BLANK
			=UseT('recur\codifics')
			SELECT codifics
			SCATTER MEMVAR BLANK
			=UseT('recur\concs_cc')
			SELECT concs_cc
			SCATTER MEMVAR BLANK
			=UseT('recur\rec_cc')
			SELECT rec_cc
			SCATTER MEMVAR BLANK
			=UseT('recur\rec_fec')
			SELECT rec_fec
			SCATTER MEMVAR BLANK
			=UseT('recur\comprob')
			SELECT comprob
			SCATTER MEMVAR BLANK
			=UseT('recur\comp_ren')
			SELECT comp_ren
			SCATTER MEMVAR BLANK
			* END Apertura de tablas
			
			* Declaración de cursores
			cre_cursores()
					
			_ConsultarDeuda (@AuxArchDeud, TipoImp, NroImp, AmbienteOK)
		ENDIF
		
		AgregarTelefonoYMail(@AuxArchDeud)
		
		AuxArchDeud = AuxArchDeud + Indent(1) + '<NOTAS>' + _S_
		IF NOT EMPTY(M_Mensaje)
			AuxArchDeud = AuxArchDeud + Indent(2) + '<REGISTRO_NOTAS>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<MENSAJE>' + ALLTRIM(M_Mensaje) + '</MENSAJE>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(2) + '</REGISTRO_NOTAS>' + _S_
		ENDIF
		AuxArchDeud = AuxArchDeud + Indent(1) + '</NOTAS>' + _S_

		AuxArchDeud = AuxArchDeud + '</VFPDATA>' + _S_
		
		RETURN STRCONV(AuxArchDeud,_STRCONV_)
	ENDPROC
	***********************************************************************************
	
	
	
	***********************************************************************************
	*	EmitirCompPagoNoVenc
	***********************************************************************************
	PROCEDURE EmitirCompPagoNoVenc (XMLGrupoNroComp as String) as String
		PRIVATE retorno
		
		AuxArchCompNoVenc = '<?xml version="1.0" encoding="UTF-8"?>' + _S_
		AuxArchCompNoVenc = AuxArchCompNoVenc + '<VFPDATA>' + _S_
		
		IF AmbienteOK
			EfimuniWeb()
		
			* BEGIN Apertura de tablas
			=UseT('recur\concs_cc')
			SELECT concs_cc
			SCATTER MEMVAR BLANK
			=UseT('recur\rec_cc')
			SELECT rec_cc
			SCATTER MEMVAR BLANK
			=UseT('recur\rec_fec')
			SELECT rec_fec
			SCATTER MEMVAR BLANK
			=UseT('recur\comprob')
			SELECT comprob
			SCATTER MEMVAR BLANK
			=UseT('recur\comp_ren')
			SELECT comp_ren
			SCATTER MEMVAR BLANK
			* END Apertura de tablas
			
			_EmitirCompPagoNoVenc (@AuxArchCompNoVenc, XMLGrupoNroComp, AmbienteOK)
		ENDIF
		
		AgregarTelefonoYMail(@AuxArchCompNoVenc)
		
		AuxArchCompNoVenc = AuxArchCompNoVenc + Indent(1) + '<ERROR>' + _S_		
		IF NOT EMPTY(M_Mensaje)
			AuxArchCompNoVenc = AuxArchCompNoVenc + Indent(2) + '<MENSERROR>' + M_Mensaje + '</MENSERROR>' + _S_
		ENDIF
		AuxArchCompNoVenc = AuxArchCompNoVenc + Indent(1) + '</ERROR>' + _S_		
		
		AuxArchCompNoVenc = AuxArchCompNoVenc + '</VFPDATA>' + _S_

		RETURN STRCONV(AuxArchCompNoVenc,_STRCONV_)
	ENDPROC		
	***********************************************************************************
	
	
	
	***********************************************************************************
	*	EmitirCompPagoDeudaVenc
	***********************************************************************************
	PROCEDURE EmitircompPagoDeudaVenc (XMLDeudaVenc as String) as String
		
		AuxArchComp = '<?xml version="1.0" encoding="UTF-8"?>' + _S_
		AuxArchComp = AuxArchComp + '<VFPDATA>' + _S_
		
		IF AmbienteOK
			EfimuniWeb()

			* BEGIN Apertura de tablas
			=UseT('recur\numerad')		
			=UseT('recur\comprob')
			SELECT comprob
			SCATTER MEMVAR BLANK
			=UseT('recur\comp_ren')
			SELECT comp_ren
			SCATTER MEMVAR BLANK
			=UseT('recur\rec_cc')
			SELECT rec_cc
			SCATTER MEMVAR BLANK
			=UseT('recur\rec_fec')
			SELECT rec_fec
			SCATTER MEMVAR BLANK
			=UseT('recur\concs_cc')
			SELECT concs_cc
			SCATTER MEMVAR BLANK
			* END Apertura de tablas
			
			_EmitircompPagoDeudaVenc (@AuxArchComp, XMLDeudaVenc, AmbienteOK)
		ENDIF
		
		AgregarTelefonoYMail(@AuxArchComp)
		
		AuxArchComp = AuxArchComp + Indent(1) + '<ERROR>' + _S_		
		IF NOT EMPTY(M_Mensaje)
			AuxArchComp = AuxArchComp + Indent(2) + '<MENSERROR>' + M_Mensaje + '</MENSERROR>' + _S_	
		ENDIF
		AuxArchComp = AuxArchComp + Indent(1) + '</ERROR>' + _S_		
		
		AuxArchComp = AuxArchComp + '</VFPDATA>' + _S_

		RETURN STRCONV(AuxArchComp,_STRCONV_)
	ENDPROC
	***********************************************************************************
	
	
	
	***********************************************************************************
	*	ObtenerTelefonoYMail
	***********************************************************************************
	PROCEDURE ObtenerTelefonoYMail as String
		AuxTodoOK = AmbienteOK
		
		AuxArchComp = '<?xml version="1.0" encoding="UTF-8"?>' + _S_
		AuxArchComp = AuxArchComp + '<VFPDATA>' + _S_
		
		IF AuxTodoOK
			AgregarTelefonoYMail(@AuxArchComp)
		ELSE
			M_Mensaje = 'Error inesperado.'
		ENDIF
		
		AuxArchComp = AuxArchComp + Indent(1) + '<ERROR>' + _S_		
		IF NOT EMPTY(M_Mensaje)
			AuxArchComp = AuxArchComp + Indent(2) + '<MENSERROR>' + M_Mensaje + '</MENSERROR>' + _S_
		ENDIF
		AuxArchComp = AuxArchComp + Indent(1) + '</ERROR>' + _S_
		
		AuxArchComp = AuxArchComp + '</VFPDATA>' + _S_
		
		RETURN STRCONV(AuxArchComp,_STRCONV_)
	ENDPROC
	***********************************************************************************
	
	
	
	***********************************************************************************
	*	Limpiar memoria
	***********************************************************************************
	PROCEDURE BorrarTodo
		CLOSE ALL
		CLEAR MEMORY
	ENDPROC
	***********************************************************************************
	
	
	
	***********************************************************************************
	*	Destructor Implícito
	***********************************************************************************
	PROCEDURE Destroy
		CLOSE ALL
		CLEAR MEMORY
	ENDPROC
	***********************************************************************************
	
ENDDEFINE