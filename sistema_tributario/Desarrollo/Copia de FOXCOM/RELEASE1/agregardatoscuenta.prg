PROCEDURE AgregarDatosCuenta(AuxArchDeud as String)
	PRIVATE AuxCuentaOK
	
	AuxCuentaOK = .F.
	
	AuxArchDeud = AuxArchDeud + Indent(1) + '<DATOSCUENTA>' + _S_
	
	DO CASE
		CASE P_TipoImponible = 'I'
			AuxArchDeud = AuxArchDeud + Indent(2) + '<TIPOIMPONIBLE>Inmueble</TIPOIMPONIBLE>' + _S_
			IF SEEK(STR(P_NroImponible,10,0),'inmueble')
				AuxCuentaOK = .T.
			ENDIF
		CASE P_TipoImponible = 'C'
			AuxArchDeud = AuxArchDeud + Indent(2) + '<TIPOIMPONIBLE>Comercio</TIPOIMPONIBLE>' + _S_
			IF SEEK(STR(P_NroImponible,10,0),'comercio')
				AuxCuentaOK = .T.
			ENDIF
		
		CASE P_TipoImponible = 'O'
			AuxArchDeud = AuxArchDeud + Indent(2) + '<TIPOIMPONIBLE>Ocupante</TIPOIMPONIBLE>' + _S_
			IF SEEK(STR(P_NroImponible,10,0),'ubic_cem')
				AuxCuentaOK = .T.
			ENDIF
		
		CASE P_TipoImponible = 'V'
			AuxArchDeud = AuxArchDeud + Indent(2) + '<TIPOIMPONIBLE>Veh�culo</TIPOIMPONIBLE>' + _S_
			IF SEEK(STR(P_NroImponible,10,0),'vehiculo')
				AuxCuentaOK = .T.
			ENDIF
		
		OTHERWISE
			AuxArchDeud = AuxArchDeud + Indent(2) + '<TIPOIMPONIBLE>Inexistente</TIPOIMPONIBLE>' + _S_
			AuxCuentaOK = .F.
	ENDCASE
		
	AuxArchDeud = AuxArchDeud + Indent(2) + '<NROIMPONIBLE>' + ALLTRIM(STR(P_NroImponible,10,0)) + '</NROIMPONIBLE>' + _S_	
	m.nombretitular = ''
	IF AuxCuentaOK
		m.cuit1 = 0
		m.cuit2 = 0
		=FContVin(P_TipoImponible,P_NroImponible,.F.,.F.,@m.cuit1,@m.cuit2)
		IF m.cuit1>0
			IF SEEK(STR(m.cuit1,10,0),'contrib')
				m.nombretitular = contrib.nomb_cont
			ENDIF
		ENDIF
	ELSE
		M_Mensaje = 'Cuenta Inexistente'
	ENDIF
	
	AuxArchDeud = AuxArchDeud + Indent(2) + '<TITULAR>' + ALLT(m.nombretitular) + '</TITULAR>' + _S_
	AuxArchDeud = AuxArchDeud + Indent(1) + '</DATOSCUENTA>' + _S_
	
	RETURN AuxCuentaOK
ENDPROC