PROCEDURE AgregarTiempo (cad as String)
	TimeRound = SECONDS() - TimeStart
	
	cad = cad + Indent(1) + '<TIEMPODEPROCESAMIENTO>' + _S_
	cad = cad + Indent(2) + '<TIEMPO>' + IIF(TimeRound>0,ALLTRIM(STR(TimeRound,10,3)),'0.000') + '</TIEMPO>' + _S_
	cad = cad + Indent(1) + '</TIEMPODEPROCESAMIENTO>' + _S_
ENDPROC