* VFP
PARAMETERS sMascara, nCant, nNum1, nNum2, nNum3, nNum4, nNum5, nNum6, nNum7, nNum8, nNum9, nNum10
LOCAL nNum, sRet, sAux

sRet = ""
FOR nNum = 1 TO nCant
	sAux = LTRIM(STR(nNum,2))
	sRet = sRet + TRANSFORM(nNum&sAux,sMascara)
ENDFOR
RETURN sRet