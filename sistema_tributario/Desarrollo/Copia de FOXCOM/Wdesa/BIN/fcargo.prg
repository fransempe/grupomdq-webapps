*************************************************
*FUNCTION FCargo
*************************************************
* 
* Funcionamiento:  Saca la descripci�n del cargo para ser impresa en el
* recibo de haberes.

PRIVATE Leyenda
Leyenda = '                               '

* Apertura de tablas necesarias

m.alias_ant = Alias ()

IF !Used ('cargos')
	=USET ('rechum\cargos')
ENDIF

SELE cargos
SEEK (SUBSTR(CAGENTES.Nomencla,1,2)+SUBSTR(CAGENTES.Nomencla,4,2)+SUBSTR(CAGENTES.Nomencla,10,2))

Leyenda = cargos.dsc_cargo

IF !Empty (m.alias_ant)
	SELECT (m.alias_ant)
ENDIF

RETURN Leyenda
