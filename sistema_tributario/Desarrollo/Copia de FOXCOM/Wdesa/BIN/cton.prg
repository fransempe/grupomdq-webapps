*************************************************
* FUNCTION CTON
*************************************************
* Autor : Nicol�s 
* Fecha : (15/01/98)
* 
* Funcionamiento: Devuelve un N�mero formateado con la cantidad de
* 				        decimales solicitada
*
* Ejemplo : Cadena  = 1111
*				1 :	deci		= 0	-> retorno = 1111
*				2 : deci    = 2 -> retorno = 11.11
*
* Par�metros: cadena C : Cadena con el n�mero sin comas o puntos.
*			  deci   N : Cantidad de decimales 
*			  mens	 L : Boolean para emitir mensaje.	  
* 
PARAMETERS cadena,deci,mens
PRIVATE retorno,entero,decimal
retorno = 0

IF PARAMETERS() < 2
	=Msg('La cantidad de Par�metros de la Funci�n CTON() es incorrecta.')
ELSE

	*
	* Modificado el 14/07/98.
	* Si se le pasaba como par�metro '1', 2, en vez de retornar
	* 0.01, retornaba 0.10.
	*
	* Modificado el 17/07/98.
	* Se pinchaba con n�meros negativos.
	* Se verifica que todos los caracteres sean d�gitos.
	* 

	FOR m.i = 1 TO Len (m.cadena)
		IF (SubStr (m.cadena, m.i, 1) < '0' OR SubStr (m.cadena, m.i, 1) > '9') AND SubStr (m.cadena, m.i, 1) != '-' AND SubStr (m.cadena, m.i, 1) != ' '
			IF m.mens
				= Msg ('Se ha encontrado un valor num�rico con el sig. formato: "' + m.cadena + '"')
			ENDIF
			m.cadena = '0'
			EXIT
		ENDIF
	NEXT


	cadena = AllTrim (m.cadena)
	IF Left (m.cadena, 1) = '-'
		cadena = '-' + Replicate ('0', m.deci) + AllTrim (SubStr (m.cadena, 2, Len (m.cadena) - 1))
	ELSE
		cadena = Replicate ('0', m.deci) + m.cadena
	ENDIF

	decimal = RIGHT (m.cadena, deci)
	entero  = SUBSTR(m.cadena,1,LEN(m.cadena)-m.deci)
	retorno = VAL(m.entero + '.'+ m.decimal)
ENDIF
RETURN retorno
