* devuelve campo STR con la descripcion del cargo
* Jorge Borrelli 01/1998 - PEHUAJO
FUNCTION des_carg
        PRIVATE m.area_act,m.order_ag,m.order_ca,descar
		descar=SPACE(30)
        IF !USED('AGENTES')
                =USET('\RECHUM\AGENTES')
        ENDIF
        IF !USED('CARGOS')
                =USET('\RECHUM\CARGOS')
        ENDIF
        m.area_act = SELECT()
        SELECT agentes
		m.order_ag = ORDER()
        SET ORDER TO PRIMARIO IN agentes
		SEEK STR(cagentes.nro_legajo,6,0) + STR(cagentes.nro_cargo,3,0)
		IF !EOF('agentes')
	        SELECT cargos
			m.order_ca = ORDER()
	        SET ORDER TO PRIMARIO IN cargos
			SEEK agentes.grupo_ocup+agentes.nivel_ocup+agentes.cargo
				IF !EOF('cargos')
					descar = cargos.dsc_cargo
				ENDIF
			SET ORDER TO (m.order_ca)
		SELE agentes
		SET ORDER TO (m.order_ag)
		ENDIF		
        SELECT (m.area_act)
RETURN descar
