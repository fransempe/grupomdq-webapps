******************************************************
*FUNCTION BuscPcia
******************************************************
*Retorna el c�digo de Pcia. nomenclado seg�n AFIP para
*el armado del TXT de retenciones de ganancias.
* Funcion seg�n CODIFICS de NECOCHEA
PARAMETERS provi
PRIVATE retorno
m.retorno = ' '
DO CASE
	CASE m.provi = 'BS.AS'
		m.retorno = '01'
	CASE m.provi = 'CATAMA'
		m.retorno = '02'	
	CASE m.provi = 'CORDOB'
		m.retorno = '03'
	CASE m.provi = 'CORRIE'
		m.retorno = '04'
	CASE m.provi = 'CHACO'
		m.retorno = '16'
	CASE m.provi = 'CHUBUT'
		m.retorno = '17'
	CASE m.provi = 'E.RIOS'
		m.retorno = '05'
	CASE m.provi = 'FORMOS'
		m.retorno = '18'
	CASE m.provi = 'JUJUY'
		m.retorno = '06'
	CASE m.provi = 'L.PAMP'
		m.retorno = '21'
	CASE m.provi = 'L.RIOJ'
		m.retorno = '08'
	CASE m.provi = 'MENDOZ'
		m.retorno = '07'
	CASE m.provi = 'MISION'
		m.retorno = '19'
	CASE m.provi = 'NEUQUE'
		m.retorno = '20'
	CASE m.provi = 'R.NEGR'
		m.retorno = '22'
	CASE m.provi = 'S.CRUZ'
		m.retorno = '23'
	CASE m.provi = 'S.ESTE'
		m.retorno = '13'
	CASE m.provi = 'S.JUAN'
		m.retorno = '10'
	CASE m.provi = 'S.LUIS'
		m.retorno = '11'
	CASE m.provi = 'SALTA'
		m.retorno = '09'
	CASE m.provi = 'STA.FE'
		m.retorno = '12'
	CASE m.provi = 'T.FUEG'
		m.retorno = '24'
	CASE m.provi = 'TUCUMA'
		m.retorno = '14'																				
	
ENDCASE


RETURN m.retorno