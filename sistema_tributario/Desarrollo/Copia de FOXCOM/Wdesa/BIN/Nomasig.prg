*************************************************
* FUNCTION NOMASIG
************************************************* 
PRIVATE retorno, exp_cont
PRIVATE m.camp_ascii

retorno = ''

* Abro la Tabla con los campos ASCII
IF !USED('cpo_asc')
	=uset('asa\cpo_asc')
ENDIF

exp_cont = GETKEY('cpo_asc',.T.,3,'primario')

* Seteo el campo
m.camp_ascii = 'CODENTE'

IF SEEK(EVAL(exp_cont),'cpo_asc')
	IF CPO_ASC.tipo == 'C' 
		retorno = 'ENTE'+PADL(ALLT(CPO_ASC.contenido),4,'0')+'.TXT'
	ENDIF
ENDIF
RETURN retorno
