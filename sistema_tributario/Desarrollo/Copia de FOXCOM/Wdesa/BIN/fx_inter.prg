*******************************************************************
* FUNCTION FX_INTER
*
* Funci�n para c�lculo de intereses por mora de importes adeudados
* Municipalidad de Azul
*
*******************************************************************

PARAMETERS importe
PRIVATE interes, anio, mes, coeficiente, indice

IF m.tipo_imp = 'V'
 IF m.fecha_vto < {01/01/1993} 
  m.fecha_vto = {04/01/1993}
 ENDIF
ENDIF

m.interes = 0

IF recurso = 'AU' AND conc_cc = 'MOCAD'
 RETURN m.interes
ENDIF

IF recurso = 'AU' AND m.fecha_vto <= {30/04/2006}
  m.interes = FBuscInt ()
 IF m.fecha_final > m.fecha_vto
    if m.fecha_vto <= {31/03/1991}
     m.interes = m.interes + FActual (Val (imp_mov), 'INTVIE', m.fecha_vto, m.fecha_final)
    endif
    if m.fecha_vto <= {05/02/2011} and m.fecha_vto > {31/03/1991}
      m.interes = m.interes + FInt2 (Val (imp_mov), 'INT   ', m.fecha_vto, m.fecha_final)
     else
      m.interes = m.interes + FInt3 (Val (imp_mov), 'INT   ', m.fecha_vto, m.fecha_final)
    endif  
 ENDIF
ELSE
 if m.fecha_vto <= {31/03/1991}
   m.interes = FActual (Val (imp_mov), 'INTVIE', m.fecha_vto, m.fecha_final)
 endif
 if m.fecha_vto <= {05/02/2011} and m.fecha_vto > {31/03/1991}
   m.interes = FInt2 (Val (imp_mov), 'INT   ', m.fecha_vto, m.fecha_final)
  else
   m.interes = FInt3 (Val (imp_mov), 'INT   ', m.fecha_vto, m.fecha_final)
  endif
ENDIF

if recurso # 'VI' and recurso # 'CM'
 m._multazul = MultAzul (m.fecha_vto, m.fecha_final, Val (imp_mov), m.interes)
else
 m._multazul = 0
endif 

RETURN m.interes + m._multazul

**********************************************
FUNCTION FBuscInt
**********************************************
PRIVATE interes
m.interes = 0

IF !USED ('int_auto')
 =UseT ('recur\int_auto')
ENDIF

IF SEEK (recurso + tipo_imp + STR (nro_imp, 10, 0) + STR (anio, 4, 0) + STR (cuota, 3, 0) + STR (nro_mov, 3, 0), 'int_auto')
 m.interes = int_auto.imp_int
 IF EMPTY (int_auto.fecha_act)
  m.fecha_vto = {01/04/2003}
 ELSE
  m.fecha_vto = int_auto.fecha_act + 1
 ENDIF
ENDIF

RETURN m.interes

*************************************************
FUNCTION UltDia
*************************************************
PARAMETERS fecha
PRIVATE retorno, dia, mes, anio
retorno = {//}

m.dia = 1
m.mes = Month (m.fecha) + 1
m.anio = Year (m.fecha)
IF m.mes > 12
 m.mes = 1
 m.anio = m.anio + 1
ENDIF
retorno = CToD (AllTrim (Str (m.dia, 2, 0)) + '/' + ;
          AllTrim (Str (m.mes, 2, 0)) + '/' + ;
		  AllTrim (Str (m.anio, 4, 0)))
retorno = retorno - 1

RETURN retorno

