*************************************************
* FUNCTION FDevPat
*************************************************
PRIVATE retorno, importe
m.importe = 0

IF !USED ('recur\val_mode')
	=UseT ('recur\val_mode')
ENDIF

IF SEEK (vehiculo.tipo_vehi + vehiculo.cod_marca + vehiculo.cod_modelo + STR (vehiculo.anio_vehi, 4, 0), 'val_mode') AND vehiculo.anio_vehi >= YEAR (DATE ()) - 23
	* m.importe = ((val_mode.val_modelo * 2.5) / 100) / 12
	m.importe = ((val_mode.val_modelo * 3) / 100) / 12
ELSE
	&& Error: no se encontr� la valuaci�n del Veh�culo
	m.importe = 0
ENDIF
IF Type ('c_conc.cuota') = 'N'
	*
	* CUOTA 13 ES ANUAL
	*
	IF c_conc.cuota = 13
		m.importe = Round (m.importe * 12, 2)
	ENDIF
ENDIF

RETURN m.importe
