*******************
FUNCTION AVANCOPY
*******************
* Funci�n especial para el E.M.S.Ur.
* Se utiliza en el reporte de Orden de Compra Ord_com para que seg�n
* el valor de la vble. p�blica _WAWA imprima la leyenda "ORIGINAL", 
* "DUPLICADO", "TRIPLICADO" o "CUADRUPLICADO" en cada copia impresa.

IF TYPE ('_WAWA')="U"
	PUBLIC _WAWA
	_WAWA = 0
ENDIF

IF _pageno=1 AND _WAWA<4
	_WAWA=_WAWA+1
ELSE
	* SQLMODI01
	* _WAWA=1
	IF _pageno=1
		IF _WAWA=4
			_WAWA=0
		ENDIF
	ENDIF
	* FIN SQLMODI01
ENDIF

RETURN ' '
