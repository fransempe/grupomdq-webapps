*************************************************
* 
* Funcionamiento:
* 
* Codifica:
* . calculo cantidad de d�as date () - {1/1/1980}
* . calculo d�gito verificador:
*   Int (Int ((date () - {4/1/1980}) ^ 2) % 10
* . sumo el d�gito verificador a cada otro de los d�gitos
* 	si el valor es > 9, tomo valor - 10
* 
* 
PARAMETERS fecha
PRIVATE retorno, digitos, cantidad, numero, i
retorno = 0

m.cantidad = m.fecha - {1/1/1965}

DIMENSION digitos[6]

digitos[6] = Int (m.cantidad ^ 2) % 10
IF digitos[6] = 0
	digitos[6] = 9
ENDIF

digitos[5] = m.cantidad % 10
m.cantidad = Int (m.cantidad / 10)
digitos[4] = m.cantidad % 10
m.cantidad = Int (m.cantidad / 10)
digitos[3] = m.cantidad % 10
m.cantidad = Int (m.cantidad / 10)
digitos[2] = m.cantidad % 10
m.cantidad = Int (m.cantidad / 10)
digitos[1] = m.cantidad % 10

m.numero = ''
FOR m.i = 1 TO 5
	digitos[m.i] = digitos[m.i] + digitos[6]
	IF digitos[m.i] > 9
		digitos[m.i] = 	digitos[m.i] - 10
	ENDIF
	m.numero = m.numero + Str (digitos[m.i], 1, 0)
NEXT
m.numero = m.numero + Str (digitos[6], 1, 0)

retorno = Val (m.numero)

RETURN retorno