#INCLUDE TEXTREPO.H

SET CONSOLE OFF
SET ALTERNATE TO (ADDBS(SYS(2023)) + SYS(3) + ".TXT")
SET ALTERNATE ON
SET PRINTER TO
SET PRINTER ON PROMPT

* Seteos iniciales
??PC_PAGELENGTH + chr(66) + PC_DRAFT + PC_BOLDOFF + PC_ITALICOFF + PC_12CPI
??CHR(27) + "0" + CHR(27) + "C" + CHR(22) && setea el interlineado

* Impresi�n del cheque
? SPACE(40) + PADL( ALLT( TRAN(comp_emi.imp_comp,"9,999,999.99") ) , 12, '*')
?
?
?
? m.nro_comp
? SPACE(40) + PC_15CPI + PADR(dtostr(DATE(), .F.), 15) + PC_12CPI + "     " + STR(YEAR(DATE()), 4)
?
? SPACE(20) + comp_emi.benefic
? SPACE(20) + m.imp_let1
? SPACE(20) + m.imp_let2
?
?
?
?
?
?

CLOSE ALTERNATE
SET ALTERNATE TO
SET ALTERNATE OFF
SET PRINTER OFF
SET CONSOLE ON

SET PRINTER TO
RETURN

*****************************************
FUNCTION SETCP(sStr)
*****************************************
sStr = STRTRAN(sStr, "�", "�")
sStr = STRTRAN(sStr, "�", CHR(130))
sStr = STRTRAN(sStr, "�", "�")
sStr = STRTRAN(sStr, "�", "�")
sStr = STRTRAN(sStr, "�", "�")
sStr = STRTRAN(sStr, "�", "�")
sStr = STRTRAN(sStr, "�", "�")

*!*	sStr = STRTRAN(sStr, "�","�")
*!*	sStr = STRTRAN(sStr, "�", "�")
*!*	sStr = STRTRAN(sStr, "�", "�")
*!*	sStr = STRTRAN(sStr, "�", "�")
*!*	sStr = STRTRAN(sStr, "�", "�")
*!*	sStr = STRTRAN(sStr, CHR(190), "�")
*!*	sStr = STRTRAN(sStr, "�", "�")
*!*	sStr = STRTRAN(sStr, CHR(183), "�")
*!*	sStr = STRTRAN(sStr, "�", "�")
*!*	sStr = STRTRAN(sStr, "�", "�")
*!*	sStr = STRTRAN(sStr, "�", "�")
*!*	sStr = STRTRAN(sStr, "�", "�")
RETURN sStr