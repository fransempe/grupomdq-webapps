* Funci�n OC_Gur� de armado de imputaciones de la Orden de Compra
* del Municipio de San Nicol�s

******************************************************
* FUNCTION OC_Gur�
******************************************************
PRIVATE i, j, memo_ant, cant_reg, itemsXhoja, imputXhoja, hojas_imp
PRIVATE tot_acum, pagina


*************************************************************
*
* CANTIDAD DE LINEAS DE DETAIL (SOLO ITEMS DE LA OC) QUE SE
* IMPRIMEN POR CADA PAGINA DE LA O.C.
*
*************************************************************
m.itemsXhoja = 16


*************************************************************
*
* CANTIDAD DE LINEAS DE IMPUTACIONES QUE SE
* IMPRIMEN POR CADA PAGINA DE LA O.C.
*
*************************************************************
m.imputXhoja = 5


memo_ant	= SET('MEMOWIDTH')
SET MEMO TO 57

CREATE CURSOR tmp_imp (c_dest		 C(20), ;
											 c_cant 	 C(9), ;
											 c_desc 	 C(57), ;
											 c_preu 	 C(16), ;
											 c_trans	 C(50), ;
											 c_pagina	 N(4), ;
											 c_totite  C(16))

STORE '' TO m.c_cant, m.c_desc, m.c_preu, m.c_totite

SELECT tmp_imp
IF ALIAS() = 'tmp_imp'
	ZAP
ENDIF

*
* OBTENGO LA CANTIDAD DE HOJAS QUE OCUPARAN LAS IMPUTACIONES
*
m.cant_reg = 0
GO TOP IN c_imp_oc
DO WHILE !Eof ('c_imp_oc')
	m.cant_reg = m.cant_reg + 1
	SKIP IN c_imp_oc
ENDDO
GO TOP IN c_imp_oc

m.hojas_imp = 0
IF m.cant_reg > 0
	m.hojas_imp = Ceil (m.cant_reg / m.imputXhoja)
ENDIF


*
* GENERO EL CURSOR CON LA INFORMACION DE LOS ITEMS
*
GO TOP IN c_ite_oc

m.cant_reg = 0
m.tot_acum = 0
m.pagina = 1

DO WHILE !Eof ('c_ite_oc')
	SELECT tmp_imp
	APPEND BLANK
	REPLACE	c_dest		WITH PadR (c_ite_oc.dest_it_oc, 20) ;
					c_cant		WITH Transform (c_ite_oc.cant_it_oc, '99,999.99') ;
					c_desc		WITH MLine (c_ite_oc.desc_it_oc, 1) ;
					c_preu		WITH Transform (c_ite_oc.pre_uni_oc, '999,999,999.999') ;
					c_trans		WITH '' ;
					c_pagina	WITH m.pagina ;
					c_totite	WITH Transform (Round (c_ite_oc.pre_uni_oc * c_ite_oc.cant_it_oc, 2), '999,999,999.999')

	m.tot_acum = m.tot_acum + Round (c_ite_oc.pre_uni_oc * c_ite_oc.cant_it_oc, 2)

	m.cant_reg = m.cant_reg + 1

	= VeriTransp ()

	FOR m.i = 2 TO MemLines (c_ite_oc.desc_it_oc)
		SELECT tmp_imp
		APPEND BLANK
		REPLACE	c_dest		WITH '' ;
						c_cant		WITH '' ;
						c_desc		WITH MLine (c_ite_oc.desc_it_oc, m.i) ;
						c_preu		WITH '' ;
						c_trans		WITH '' ;
						c_pagina	WITH m.pagina ;
						c_totite	WITH ''

		m.cant_reg = m.cant_reg + 1

		= VeriTransp ()

	ENDFOR

	SKIP IN c_ite_oc
ENDDO

*
* SI LA CANTIDAD HOJAS QUE OCUPAN LOS ITEMS ES MENOR A LA CANTIDAD DE
* HOJAS QUE OCUPAN LAS IMPUTACIONES, TENGO QUE AGREGAR TANTOS
* REGISTROS EN BLANCO SUFICIENTES PARA QUE LA CANTIDAD DE HOJAS DE LOS
* ITEMS SEA IGUAL A LA DE IMPUTACIONES
*
IF Ceil (m.cant_reg / m.itemsXhoja) < m.hojas_imp
	FOR m.i = Ceil (m.cant_reg / m.itemsXhoja) + 1 TO m.hojas_imp
		FOR m.j = 1 TO m.itemsXhoja
			SELECT tmp_imp
			APPEND BLANK
			REPLACE c_pagina WITH m.pagina
			IF m.i = m.hojas_imp
				IF m.j >= m.itemsXhoja - 3
					= ImprimeTotal ()
					EXIT
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
ELSE
	= ImprimeTotal ()
ENDIF

GO TOP IN tmp_imp
SET MEMOWIDTH TO (memo_ant)

RETURN .T.


*************************************************
FUNCTION VeriTransp
*************************************************
PRIVATE retorno
retorno = .T.

IF ((m.cant_reg + 1) % m.itemsXhoja) = 0
	SELECT tmp_imp
	APPEND BLANK
	REPLACE	c_dest		WITH '' ;
					c_cant		WITH '' ;
					c_desc		WITH '' ;
					c_preu		WITH '' ;
					c_trans		WITH 'Transporte Total de Items Hacia Hoja Siguiente' ;
					c_pagina	WITH m.pagina ;
					c_totite	WITH Transform (m.tot_acum, '999,999,999.999')

	m.pagina = m.pagina + 1

	SELECT tmp_imp
	APPEND BLANK
	REPLACE	c_dest		WITH '' ;
					c_cant		WITH '' ;
					c_desc		WITH '' ;
					c_preu		WITH '' ;
					c_trans		WITH 'Transporte Total de Items desde Hoja Anterior' ;
					c_pagina	WITH m.pagina ;
					c_totite	WITH Transform (m.tot_acum, '999,999,999.999')

	m.cant_reg = m.cant_reg + 2
ENDIF

RETURN retorno


*************************************************
FUNCTION ImprimeTotal
*************************************************
PRIVATE retorno
retorno = .T.

SELECT tmp_imp
APPEND BLANK
REPLACE	c_dest		WITH '' ;
				c_cant		WITH '' ;
				c_desc		WITH '' ;
				c_preu		WITH '' ;
				c_trans		WITH PadL ('Total :', Len (c_trans)) ;
				c_pagina	WITH m.pagina ;
				c_totite	WITH Transform (m.tot_acum, '999,999,999.999')

APPEND BLANK
REPLACE	c_pagina	WITH m.pagina

APPEND BLANK
REPLACE	c_dest		WITH '' ;
				c_cant		WITH '' ;
				c_desc		WITH '' ;
				c_preu		WITH '' ;
				c_trans		WITH m.pieoc + IIF(ALLT(adjudic.tipo_adj) = "CD", " " + DTOC(m.fe_emision),"");
				c_pagina	WITH m.pagina ;
				c_totite	WITH ''

RETURN retorno
