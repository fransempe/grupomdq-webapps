****************************************************
*FUNCTION DebEfBSE
****************************************************
*Funci�n que retorna el nombre del archivo ASCII donde
*se leen los datos efectuados para el Bco. de Sgo. Estero

PRIVATE retorno
m.retorno = 'BSE_'+ PADL(m.mm,2,'0')+ SUBSTR(STR(m.aa,4,0),3,2)  + '.TXT'
RETURN m.retorno