*	
*	SQLMODI01
*		Usuario							: Baradero
*		Fecha								: 20/07/99
*		Quien modific�			: Vero
*		Detalle problema		: Cuando el n�mero tiene una s�la unidad de mil
*													no especifica su escritura.Ahora debe salir.
*													Ej:1000 Antes.. MIL; Ahora.. UN MIL
*		Detalle correcci�n	: Se cambia una condici�n de entrada aun proceso.
*													
*													
*		Programas tocados		: 
*													
*													
************************************************************************


***************************************
FUNCTION numlet
***************************************
PARAMETERS m.numero, m.mayus
PRIVATE subcad1, subcad2, subcad3, subcad10, subcad100, lect, ;
        cadnum, neg_num

neg_num = .F.
DO CASE
	CASE PARAMETERS() = 0
		m.numero = 0
		m.mayus = .T.
	CASE PARAMETERS() = 1
		m.mayus = .T.
ENDCASE

IF (m.numero > 999999999999.99) OR (m.numero < - 999999999999.99)
	= MSG ('N�mero pasado como par�metro a funci�n NUMLET fuera de rango. El n�mero debe estar entre - 999999999999.99 y 999999999999.99')
ENDIF
IF (m.numero < 0)
	neg_num = .T.
	m.numero = m.numero * -1	
ENDIF

DIMENSION unid[21]
	unid[1]=''
	unid[2]='UNO'
	unid[3]='DOS'
	unid[4]='TRES'
	unid[5]='CUATRO'
	unid[6]='CINCO'
	unid[7]='SEIS'
	unid[8]='SIETE'
	unid[9]='OCHO'
	unid[10]='NUEVE'
	unid[11]='DIEZ'
	unid[12]='ONCE'
	unid[13]='DOCE'
	unid[14]='TRECE'
	unid[15]='CATORCE'
	unid[16]='QUINCE'
	unid[17]='DIECISEIS'
	unid[18]='DIECISIETE'
	unid[19]='DIECIOCHO'
	unid[20]='DIECINUEVE'
	unid[21]='VEINTE'

DIMENSION unid1[22]
	unid1[1]=''
	unid1[2]='UN'
	unid1[3]='DOS'
	unid1[4]='TRES'
	unid1[5]='CUATRO'
	unid1[6]='CINCO'
	unid1[7]='SEIS'
	unid1[8]='SIETE'
	unid1[9]='OCHO'
	unid1[10]='NUEVE'
	unid1[11]='DIEZ'
	unid1[12]='ONCE'
	unid1[13]='DOCE'
	unid1[14]='TRECE'
	unid1[15]='CATORCE'
	unid1[16]='QUINCE'
	unid1[17]='DIECISEIS'
	unid1[18]='DIECISIETE'
	unid1[19]='DIECIOCHO'
	unid1[20]='DIECINUEVE'
	unid1[21]='VEINTE'

DIMENSION dec[11]
	dec[3]='VEINTI'
	dec[4]='TREINTA Y'
	dec[5]='CUARENTA Y'
	dec[6]='CINCUENTA Y'
	dec[7]='SESENTA Y'
	dec[8]='SETENTA Y'
	dec[9]='OCHENTA Y'
	dec[10]='NOVENTA Y'
	dec[1]=''

DIMENSION dec1[11]
	dec1[3]='VEINTE'
	dec1[4]='TREINTA'
	dec1[5]='CUARENTA'
	dec1[6]='CINCUENTA'
	dec1[7]='SESENTA'
	dec1[8]='SETENTA'
	dec1[9]='OCHENTA'
	dec1[10]='NOVENTA'
	dec1[11]='CIEN'

DIMENSION cen[11]
	cen[2]='CIENTO'
	cen[3]='DOSCIENTOS'
	cen[4]='TRESCIENTOS'
	cen[5]='CUATROCIENTOS'
	cen[6]='QUINIENTOS'
	cen[7]='SEISCIENTOS'
	cen[8]='SETECIENTOS'
	cen[9]='OCHOCIENTOS'
	cen[10]='NOVECIENTOS'
	cen[11]='MIL'
	cen[1]=''

DIMENSION cen1[11]
	cen1[2]='CIEN'
	cen1[3]='DOSCIENTOS'
	cen1[4]='TRESCIENTOS'
	cen1[5]='CUATROCIENTOS'
	cen1[6]='QUINIENTOS'
	cen1[7]='SEISCIENTOS'
	cen1[8]='SETECIENTOS'
	cen1[9]='OCHOCIENTOS'
	cen1[10]='NOVECIENTOS'
	cen1[11]='MIL'
	cen1[1]=''


cadnum=STR(m.numero,15,2)

lect=''

****************************************************************************
****************  999.###.###.###  ~~  1.###.###.###  **********************
****************************************************************************

IF m.numero>=1000000000				
	subcad1=VAL(SUBSTR(cadnum,1,1))
	subcad2=VAL(SUBSTR(cadnum,2,1))
	subcad3=VAL(SUBSTR(cadnum,3,1))
	subcad10=VAL(SUBSTR(cadnum,2,2))
	subcad100=VAL(SUBSTR(cadnum,1,3))
	IF subcad100>1
		IF subcad10>0
			lect=RTRIM(lect) +' '+cen[subcad1+1]
			IF subcad10<=20
				lect=RTRIM(lect)+' '+unid1[subcad10+1]+' MIL'
			ELSE
				IF subcad3=0
					lect=RTRIM(lect)+' '+dec1[subcad2+1]+' MIL'
				ELSE
					IF subcad10 < 30
						lect=RTRIM(lect)+' '+dec[subcad2+1]+unid1[subcad3+1]+' MIL'
					ELSE
						lect=RTRIM(lect)+' '+dec[subcad2+1]+' '+unid1[subcad3+1]+' MIL'
					ENDIF
				ENDIF
			ENDIF
		ELSE
			lect=RTRIM(lect)+' '+cen1[subcad1+1]+' MIL'	
		ENDIF
	ENDIF
	IF subcad100=1
		lect=RTRIM(lect)+' MIL'
	ENDIF
ENDIF

****************************************************************************
**********************  999.###.###  ~~  1.###.###  ************************
****************************************************************************

IF m.numero>=1000000				
	subcad1=VAL(SUBSTR(cadnum,4,1))
	subcad2=VAL(SUBSTR(cadnum,5,1))
	subcad3=VAL(SUBSTR(cadnum,6,1))
	subcad10=VAL(SUBSTR(cadnum,5,2))
	subcad100=VAL(SUBSTR(cadnum,4,3))
	IF subcad100>0
		IF subcad10>0
			lect=RTRIM(lect) +' '+cen[subcad1+1]
			IF subcad10<=20
				IF subcad10=1
					IF m.numero >= 2000000
						lect=RTRIM(lect)+' '+'UN MILLONES '
					ELSE
						lect=RTRIM(lect)+' '+'UN MILLON '
					ENDIF    
				ELSE
					lect=RTRIM(lect)+' '+unid[subcad10+1]+' MILLONES'
				ENDIF
			ELSE
				IF subcad3=0
					lect=RTRIM(lect)+' '+dec1[subcad2+1]+' MILLONES'
				ELSE
					IF subcad10 < 30
						lect=RTRIM(lect)+' '+dec[subcad2+1]+unid1[subcad3+1]+' MILLONES'
					ELSE
						lect=RTRIM(lect)+' '+dec[subcad2+1]+' '+unid1[subcad3+1]+' MILLONES'
					ENDIF
				ENDIF
			ENDIF
		ELSE
			lect=RTRIM(lect)+' '+cen1[subcad1+1]+' MILLONES'
		ENDIF
	ELSE
		lect=RTRIM(lect)+' MILLONES'
	ENDIF
ENDIF

****************************************************************************
***************************  999.###  ~~  1.###  ***************************
****************************************************************************


***********************************************************
**SQLMODI01:  La condici�n antes era m.numero > 1000
***********************************************************

IF m.numero>=1000					
	subcad1=VAL(SUBSTR(cadnum,7,1))
	subcad2=VAL(SUBSTR(cadnum,8,1))
	subcad3=VAL(SUBSTR(cadnum,9,1))
	subcad10=VAL(SUBSTR(cadnum,8,2))
	subcad100=VAL(SUBSTR(cadnum,7,3))
	IF subcad100>=1
		IF subcad10>0
			lect=RTRIM(lect) +' '+cen[subcad1+1]
			IF subcad10<=20
				lect=RTRIM(lect)+' '+unid1[subcad10+1]+' MIL'
			ELSE
				IF subcad3=0
					lect=RTRIM(lect)+' '+dec1[subcad2+1]+' MIL'
				ELSE
					IF subcad10 < 30
						lect=RTRIM(lect)+' '+dec[subcad2+1]+unid1[subcad3+1]+' MIL'
					ELSE
						lect=RTRIM(lect)+' '+dec[subcad2+1]+' '+unid1[subcad3+1]+' MIL'
					ENDIF
				ENDIF
			ENDIF
		ELSE
			lect=RTRIM(lect)+' '+cen1[subcad1+1]+' MIL'
		ENDIF
	ENDIF

**********************************************************
*SQLMODI01:Se comento este bloque
**********************************************************

	*IF subcad100=1
	*	lect=RTRIM(lect)+' MIL'
	*ENDIF
ENDIF
*FIN SQLMODI01 ********************************************


****************************************************************************
*****************************  999  ~~  1  *********************************
****************************************************************************

IF m.numero>0
	subcad1=VAL(SUBSTR(cadnum,10,1))
	subcad2=VAL(SUBSTR(cadnum,11,1))
	subcad3=VAL(SUBSTR(cadnum,12,1))
	subcad10=VAL(SUBSTR(cadnum,11,2))
	subcad100=VAL(SUBSTR(cadnum,10,3))
	IF subcad100>0
		IF subcad10>0
			lect=RTRIM(lect) +' '+cen[subcad1+1]
			IF subcad10<=20
				lect=RTRIM(lect)+' '+unid[subcad10+1]
			ELSE
				IF subcad3=0
					lect=RTRIM(lect)+' '+dec1[subcad2+1]
				ELSE
					IF subcad10 < 30
					
					  * Mariano 29/09/97 termina en uno quedaba mal
					  
						lect=RTRIM(lect)+' '+dec[subcad2+1]+unid[subcad3+1]
						
					ELSE
		
						* Mariano 29/09/97 termina en uno quedaba mal
											
						lect=RTRIM(lect)+' '+dec[subcad2+1]+' '+unid[subcad3+1]
			
					ENDIF
				ENDIF
			ENDIF
		ELSE
			lect=RTRIM(lect)+' '+cen1[subcad1+1]
		ENDIF
	ENDIF
ENDIF

****************************************************************************
******************************  #.99 ~~ 0  *********************************
****************************************************************************

subcad1=VAL(SUBSTR(cadnum,14,1))
subcad2=VAL(SUBSTR(cadnum,15,1))
subcad10=VAL(SUBSTR(cadnum,14,2))
IF subcad10>0
	IF m.numero > 1
		lect=RTRIM(lect) +' CON  '
	ENDIF
	IF subcad10<=20
		IF subcad10 = 1
			lect=RTRIM(lect)+' '+unid1[subcad10+1]+' CENTAVO'
		ELSE
			lect=RTRIM(lect)+' '+unid1[subcad10+1]+' CENTAVOS'
		ENDIF
	ELSE
		IF subcad2=0
			lect=RTRIM(lect)+' '+dec1[subcad1+1]+' CENTAVOS'
		ELSE
			IF subcad10 < 30
				lect=RTRIM(lect)+' '+dec[subcad1+1]+unid1[subcad2+1]+' CENTAVOS'
			ELSE
				lect=RTRIM(lect)+' '+dec[subcad1+1]+' '+unid1[subcad2+1]+' CENTAVOS'
			ENDIF
		ENDIF
	ENDIF
ELSE
	lect=RTRIM(lect)+' '
ENDIF

****************************************************************************
****************************************************************************
****************************************************************************
IF neg_num
	lect = 'MENOS ' + ALLTRIM(lect)
ENDIF

IF m.mayus
	RETURN(ALLTRIM(lect))
ELSE
	RETURN(LOWER(ALLTRIM(lect)))
ENDIF
****************************************************************************
****************************************************************************
****************************************************************************
