*************************************************
*FUNCTION GetFact
*************************************************
* 
* Funcionamiento:
* Abre la tabla FACT_OP y la ordena.
* 
* 
* 
PRIVATE retorno
*numero=0
*retorno = .T.
*retorno=0
*retorno={}
retorno=''

=uset("conta\facturas")

SET ORDER TO primario IN fact_op
SET KEY TO STR(c_op.nro_or_pag,5,0) IN FACT_OP
GO TOP IN FACT_OP
DO WHILE !EOF("FACT_OP")
	m.retorno= m.retorno + FACT_OP.TCOMP_DGI + " " + FACT_OP.NRO_FACT + "   " +;
	DTOC(FACT_OP.FECHA_FAC)+" " + STR(FACT_OP.IMP_PA_OP,16,2)+"     " +;
	IIF (SEEK (FACT_OP.TCOMP_DGI+FACT_OP.NRO_FACT,"FACTURAS"),;
	FACTURAS.TIP_EGRE+STR(FACTURAS.NRO_OR_COM,6,0),"       ")+CHR(13)
	SKIP IN FACT_OP
ENDDO
SET KEY TO "" IN FACT_OP

*SQLMODI_04 de LORDPAG
IF !EMPTY(m.retorno)
	m.retorno = 'Comprobantes' + CHR(13) + m.retorno
ENDIF
*FINSQLMODI_04

RETURN retorno
