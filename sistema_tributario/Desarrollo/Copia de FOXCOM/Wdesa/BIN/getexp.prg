*************************************************
FUNCTION GetExp
*************************************************
* Funcionamiento: Dada una Orden de Compra o una Orden
* 				  de Pago retorna un string con los Expedientes
* 				  que se encuentren vinculados a ella en la
*                 tabla Vi_expop
* 
* Parámetros:	  tipo de Orden: 'OC' - 'OP'
* 				  Nro de Orden : N(5)
* 				  [tip_egre]   : C(1)
* 
PARAMETERS m.tipo_or, nro_or, tip_eg
PRIVATE retorno,sigue
retorno = ''
m.sigue = .T.

IF m.tipo_or != 'OC' AND m.tipo_or != 'OP'
	m.retorno	= ''
	m.sigue	= .F.
	=Msg('Pasaje de parámetro incorrecto a la función GetExp')
ENDIF
IF m.tipo_or = 'OC' AND EMPTY(m.tip_eg) AND m.sigue
	m.retorno	= ''
	m.sigue		= .F.
	=Msg('Pasaje de parámetro incorrecto a la función GetExp')
ENDIF
IF m.sigue
	IF !FILE(ALLT(_DBASE) + _EJERC + '\CONTA\VI_EXPOP.dbf')
		m.retorno = ''
	ELSE
		=USET('CONTA\VI_EXPOP')
		IF m.tipo_or = 'OP'
			m.retorno = ExpOP()
		ELSE
			m.retorno = ExpOC()
		ENDIF
	ENDIF
		
ENDIF

RETURN retorno

*************************************************
FUNCTION ExpOC
*************************************************
PRIVATE retorno
m.retorno = ''

SET ORDER TO primario IN vi_expop
SET KEY TO m.tip_eg + STR(m.nro_or,5,0) IN vi_expop
GO TOP IN vi_expop
IF !Eof('vi_expop')
	m.retorno = 'Expediente: '
ENDIF
DO WHILE !Eof('vi_expop')
	m.retorno = m.retorno + vi_expop.nro_exped + '/ '
	SKIP IN vi_expop
ENDDO

SET KEY TO '' IN vi_expop

RETURN m.retorno

*************************************************
FUNCTION ExpOP
*************************************************
PRIVATE retorno
m.retorno = ''

SET ORDER TO ord_pag IN vi_expop
SET KEY TO STR(m.nro_or,5,0) IN vi_expop
GO TOP IN vi_expop
IF !Eof('vi_expop')
	m.retorno = 'Expediente: '
ENDIF
DO WHILE !Eof('vi_expop')
	m.retorno = m.retorno + vi_expop.nro_exped + '/ '
	SKIP IN vi_expop
ENDDO

SET KEY TO '' IN vi_expop

RETURN m.retorno
