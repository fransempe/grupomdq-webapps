*************************************************
* FUNCTION FCobBAp
*************************************************
*
* Hay que asignarle valores a las variables:
*
*		m.grupo_comp	= 0
*		m.nro_comp		= 0
*		m.dv_comp			= 0
*		m.fe_cob			= {}
*		m.tot_comp		= 0
* 
PRIVATE retorno, alias
retorno = .T.

m.alias = ALIAS ()

IF NOT USED('comprob')
	=UseT('recur\comprob')
ENDIF

*
* Importe del Comprobante
*
m.tot_comp = VAL (ALLTRIM (SUBSTR (m.registro, 78, 9)) + '.' + AllTrim (SUBSTR (m.registro, 87, 2)))

*
* Fecha de Cobro
*
m.anio = '20' + SUBSTR (m.registro, 225, 2)
m.mes	= SUBSTR (m.registro, 227, 2)
m.dia	= SUBSTR (m.registro, 229, 2)		
m.fe_cob = {&dia/&mes/&anio}		
	
IF SUBSTR (m.registro, 164+1, 3) == '164'	&& C�digo de Barras
	*
	* N�mero de comprobante
	*
	m.grupo_comp = VAL (SUBSTR (m.registro, 164+26, 2))
	m.nro_comp = VAL (SUBSTR (m.registro, 164+28, 8))
	IF SEEK (STR (m.grupo_comp, 2, 0) + STR( m.nro_comp, 9, 0), 'comprob')
		m.dv_comp = comprob.dv_comp
	ENDIF
ELSE
	m.retorno = .F.
ENDIF	

IF NOT EMPTY(m.alias)
	SELECT (m.alias)
ENDIF

RETURN retorno
