private mIMPORTE, VAL_MOD, mMONO, mGANANCIAS, mBRUTOS, mRUBRO, mSUBRUBRO, mACTIVOFIJO
private PRESENTO, mr2, ms2

mIMPORTE=0
VAL_MOD=4.5
mGANANCIAS=0
mBRUTOS=0
mACTIVOFIJO=0
mMONO=ALLTRIM(CONTRIB.TIPO_CONT)
mRUBRO=COMERCIO.RUBRO1
mSUBRUBRO=COMERCIO.SUBRUB1
mR2=COMERCIO.RUBRO2
mS2=COMERCIO.SUBRUB2

*WAIT WINDOW "R="+STR(mRUBRO)+"S=" +STR(MSUBRUBRO)+" "+mMONO

* VEO SI EXISTE LA DECLARACION JURADA *
***************************************
PRESENTO="N"
m.area_act = SELECT()
IF !USED ('COM_DDJJ')
          =USET ('RECUR\COM_DDJJ')
ENDIF
SELECT COM_DDJJ
SET ORDER TO PRIMARIO IN COM_DDJJ

** BUSCO 2001
SEEK STR(COMERCIO.NRO_COM,10,0)+STR(2001,4,0)+STR(0,3,0)
IF FOUND()
   PRESENTO="S"
   mGANANCIAS=COM_DDJJ.MONT_GANP
   mBRUTOS=COM_DDJJ.MONT_GASTP
   mACTIVOFIJO=COM_DDJJ.MONT_SUELP
ELSE
   ** SINO BUSCO 2000
   SEEK STR(COMERCIO.NRO_COM,10,0)+STR(2000,4,0)+STR(0,3,0)
   IF FOUND()
       PRESENTO="S"
	   mGANANCIAS=COM_DDJJ.MONT_GANP
	   mBRUTOS=COM_DDJJ.MONT_GASTP
       mACTIVOFIJO=COM_DDJJ.MONT_SUELP
   ENDIF     
ENDIF   
SELECT (m.area_act)
***************************************

DO CASE
  *** MONOTRIBUTO ***
  CASE PRESENTO="S" AND (mMONO="M0" .OR. mMONO="M1" .OR. mMONO="M2" .OR. mMONO="M3" .OR. mMONO="M4" OR mMONO="M5" .OR. mMONO="M6" .OR. mMONO="M7")
	   IF mMONO="M5" .OR. mMONO="M6" .OR. mMONO="M7"
           mIMPORTE=9*VAL_MOD
       ELSE 
           mIMPORTE=6*VAL_MOD
       ENDIF
       
  *** GRANDES CONTRIBUYENTES FIJOS ***       
  CASE mRUBRO=70 OR mRUBRO=80 OR mRUBRO=90 OR mRUBRO=100 OR mRUBRO=110 OR mRUBRO=120 OR mRUBRO=130 OR mRUBRO=140 OR mRUBRO=150 OR mRUBRO=160 OR mRUBRO=170 OR mRUBRO=180 OR mRUBRO=190 OR mRUBRO=200 OR mRUBRO=179
       * OJO si se agrega uno cambiar en funcion BUSDDJJ, VERIFICA y mas abajo 
       DO CASE
          CASE mRUBRO=70
               mIMPORTE=VAL_MOD/500*CONC_IMP.VAL1_CDI
               
          CASE mRUBRO=80 
               mIMPORTE=500*VAL_MOD

          CASE mRUBRO=90 
               mIMPORTE=500*VAL_MOD
               
          CASE mRUBRO=100
               mIMPORTE=300*VAL_MOD

          CASE mRUBRO=110
               mIMPORTE=300*VAL_MOD
               
          CASE mRUBRO=120
               mIMPORTE=150*VAL_MOD
               
          CASE mRUBRO=130  
               mIMPORTE=50*VAL_MOD 
               
          CASE mRUBRO=140
               mIMPORTE=50*VAL_MOD              

          CASE mRUBRO=145
               mIMPORTE=350*VAL_MOD
               
          CASE mRUBRO=150  && nuevo ESPECIAL 150 MOD.
               mIMPORTE=150*VAL_MOD
               
          CASE mRUBRO=160  && nuevo ESPECIAL 300 MOD.
               mIMPORTE=300*VAL_MOD               
               
          CASE mRUBRO=170  && nuevo ESPECIAL 50 MOD.
               mIMPORTE=50*VAL_MOD               
               
          CASE mRUBRO=180  && nuevo ACTIVO FIJO ENTRE 500.000 Y 1.000.000
               mIMPORTE=40*VAL_MOD               
               
          CASE mRUBRO=190  && nuevo ACTIVO FIJO ENTRE 1.000.000 Y 2.000.000
               mIMPORTE=150*VAL_MOD               
               
          CASE mRUBRO=200  && nuevo ACTIVO FIJO >2.000.000
               mIMPORTE=300*VAL_MOD
               
          CASE mRUBRO=179 && ACTIVO FIJO segun datos cargados
               do case
               case mactivofijo>500000 and mactivofijo<1000000
                 mimporte=40
               case mactivofijo>=1000000 and mactivofijo<2000000
                 mimporte=150
               case mactivofijo>2000000
                 mIMPORTE=300 + (mactivofijo-2000000)*15/1000000
               endcase
               Mimporte=Mimporte*VAL_MOD               
       ENDCASE 

  *** GRANDES CONTRIBUYENTES GANANCIAS >1.000.000 ***         
  CASE PRESENTO="S" AND (mganancias>1000000)
           mIMPORTE=300 + (mganancias-1000000)*50/1000000
           Mimporte=Mimporte*VAL_MOD

  *** REGIMEN GENERAL ***         
  CASE PRESENTO="S" AND (mganancias>0 OR mbrutos>0)
       if mganancias>0
          mimporte=mganancias*0.1/100
       else 
           if mbrutos>0
               mimporte=mbrutos*0.02/100
               do case
                  case COMERCIO.NRO_COM=2686 
	   	                mimporte=(mbrutos*0.03/100)/2
	           endcase
           endif
       endif
       ** CHEQUEAR SEGUN MINIMO POR ACTIVIDAD **
       CHEQ=CHEQUEO(mRUBRO, mSUBRUBRO)*VAL_MOD   && primer rubro
       CHEQ=CHEQ+CHEQUEO(mR2, mS2)*VAL_MOD       && segundo rubro
       IF mIMPORTE<CHEQ
          mIMPORTE=CHEQ
       ENDIF
       
       * Tope a importe fijo
       *IF mIMPORTE>XXXXX
       *   mIMPORTE=ZZZZZ
       *ENDIF

  *** REGIMEN GENERAL negativos o en 0 por comenzar actividad 2000***         
  CASE PRESENTO="S" AND mganancias=0 AND mbrutos=0
       mIMPORTE=CHEQUEO(mRUBRO, mSUBRUBRO)*VAL_MOD  && primer rubro
       mIMPORTE=mIMPORTE+CHEQUEO(mR2, mS2)*VAL_MOD  && segundo rubro
       
  *** NO PRESENTO ***         
  CASE PRESENTO="N" and mRUBRO<>70 AND mRUBRO<>80 AND mRUBRO<>90 AND mRUBRO<>100 AND mRUBRO<>110 AND mRUBRO<>120 AND mRUBRO<>130 AND mRUBRO<>140 AND mRUBRO<>150 AND mRUBRO<>160 AND mRUBRO<>170 and mRUBRO<>180 and mRUBRO<>190 and mRUBRO<>200 AND mRUBRO<>179
      * OJO si se agrega uno cambiar en funcion BUSDDJJ, VERIFICA y mas arriba, 
       mIMPORTE=CHEQUEO(mRUBRO, mSUBRUBRO)*VAL_MOD  && primer rubro
       mIMPORTE=mIMPORTE+CHEQUEO(mR2, mS2)*VAL_MOD  && segundo rubro      
       *** SI SE USA MULTA
       *mIMPORTE=mIMPORTE*3 
              
ENDCASE       
return mIMPORTE

FUNCTION CHEQUEO
PARAMETERS R,S
PRIVATE V

V=0
DO CASE
   CASE R=1 AND S=1
        V=17
   CASE R=1 AND S=2
        V=30
        
   CASE R=2 AND S=1
        V=40
   CASE R=2 AND S=2
        V=30                
   CASE R=2 AND S=3
        V=30
   CASE R=2 AND S=4
        V=80
   CASE R=2 AND S=5
        V=100
   CASE R=2 AND S=6
        V=20
   CASE R=2 AND S=7
        V=30        
   CASE R=2 AND S=8
        V=60        
   CASE R=2 AND S=9
        V=20
   CASE R=2 AND S=10
        V=40        
   CASE R=2 AND S=11
        V=45
   CASE R=2 AND S=12
        V=40
   CASE R=2 AND S=13
        V=11
   CASE R=2 AND S=14
        V=20
   CASE R=2 AND S=15
        V=30
   CASE R=2 AND S=16
        V=50

        
   CASE R=3 AND S=1
        V=30
   CASE R=3 AND S=2
        V=15
   CASE R=3 AND S=3
        V=350                
   CASE R=3 AND S=4
        V=400
   CASE R=3 AND S=5
        V=60        
   CASE R=3 AND S=6
        V=60        
   CASE R=3 AND S=7
        V=20
        
   CASE R=4 AND S=1
        V=30        
        
   CASE R=5 AND S=100
        V=20        
   CASE R=5 AND S=110
        V=40        
   CASE R=5 AND S=120
        V=80
   CASE R=5 AND S=2
        V=5*CONC_IMP.VAL1_CDI
   CASE R=5 AND S=3
        V=60                
   CASE R=5 AND S=4
        V=20        
   CASE R=5 AND S=5
        V=20
   CASE R=5 AND S=6
        V=50        
   CASE R=5 AND S=7
        V=11
   CASE R=5 AND S=8
        V=5*CONC_IMP.VAL1_CDI
        IF V<20
           V=20
        ENDIF
   CASE R=5 AND S=9
        V=8*CONC_IMP.VAL1_CDI
   CASE R=5 AND S=10
        V=CONC_IMP.VAL1_CDI/1000*7
   CASE R=5 AND S=11
        V=100
   CASE R=5 AND S=12
        V=11
   CASE R=5 AND S=13
        V=11

   CASE R=6 AND S=100
        V=70        
   CASE R=6 AND S=110
        V=40        
   CASE R=6 AND S=2
        V=20
   CASE R=6 AND S=3
        V=20                
   CASE R=6 AND S=4
        V=18        
   CASE R=6 AND S=5
        V=30

   CASE R=7 AND S=1
        V=20
   CASE R=7 AND S=2
        V=20

        
   CASE R=10 AND S=1
        V=16
   CASE R=10 AND S=2   
        V= 20-3
   CASE R=10 AND S=3
        V= 20-1.5
ENDCASE        
RETURN V
                  