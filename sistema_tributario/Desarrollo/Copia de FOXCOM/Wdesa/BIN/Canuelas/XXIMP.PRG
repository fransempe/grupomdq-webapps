* funcion XXIMP
*
* FUNCIONAMIENTO: Lee un imponible en la tabla maestra correspondiente
*
* Parametros: m.tipo_imp (C): tipo de imponible
*             m.nro_imp  (N): nro de imponible
* 
PARAMETER m.tipo_imp, m.nro_imp
PRIVATE m.retorno, m.area_act

m.retorno  = .T.
m.area_act = SELECT()

IF m.tipo_imp='I'
   SELECT INMUEBLE
ELSE
   IF m.tipo_imp='C'
       SELECT COMERCIO
   ELSE
      IF m.tipo_imp='N'
           SELECT CONTRIB
      ELSE
          IF m.tipo_imp='O'
             SELECT CEMENT
          ELSE
             m.retorno=.F.
          ENDIF          
      ENDIF
   ENDIF
ENDIF

IF m.retorno
   m.retorno=SEEK(STR(m.nro_imp,10,0))
ENDIF

IF M.RETORNO
	IF INMUEBLE.CIRCUN=2  .AND. (ALLT(INMUEBLE.SECCION)="Q" OR ALLT(INMUEBLE.SECCION)="q")
	&&.AND. INMUEBLE.NUMCHACRA=11
	 	M.RETORNO=.T.
	ELSE
		M.RETORNO=.F.
	ENDIF	
ENDIF      
SELECT (m.area_act)

return m.retorno

