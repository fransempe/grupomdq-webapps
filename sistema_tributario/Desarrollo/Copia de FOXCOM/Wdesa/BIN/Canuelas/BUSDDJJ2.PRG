parameters nrocom, quecosa


* VEO SI EXISTE LA DECLARACION JURADA *
***************************************
mretorno=0
m.area_act = SELECT()
IF !USED ('COM_DDJJ')
          =USET ('RECUR\COM_DDJJ')
ENDIF
SELECT COM_DDJJ
SET ORDER TO PRIMARIO IN COM_DDJJ
go top

** BUSCO 2001
SEEK STR(NROCOM,10,0)+STR(2001,4,0)+STR(0,3,0)
IF FOUND()
   DO CASE
      CASE quecosa="G"
         mretorno=COM_DDJJ.MONT_GANP
      CASE quecosa="I"     
         mretorno=COM_DDJJ.MONT_GASTP
      CASE quecosa="A"     
         mretorno=COM_DDJJ.MONT_SUELP
   ENDCASE  
ELSE
   ** SINO BUSCO 2000
   SEEK STR(NROCOM,10,0)+STR(2000,4,0)+STR(0,3,0)
   IF FOUND()
       DO CASE
         CASE quecosa="G"
            mretorno=COM_DDJJ.MONT_GANP
         CASE quecosa="I"     
            mretorno=COM_DDJJ.MONT_GASTP
         CASE quecosa="A"     
            mretorno=COM_DDJJ.MONT_SUELP
       ENDCASE    
   ENDIF
ENDIF   
SELECT (m.area_act)
***************************************
return MRETORNO

