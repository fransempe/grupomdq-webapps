* Funci�n NOMEN
*
* Devuelve nomenclatura
*
* Esta funci�n debi� desarrollarse debido a que FOX no permite evaluar
* macros con m�s de 255 caracteres.
* Esta funci�n debe invocarse para obtener la 'Identificaci�n' de los
* inmuebles. Por lo tanto, el contenido del campo EXP_IDENT de la tabla
* TIPS_IMP (Tipos de Imponibles) debe ser: FNomenc()
*
*PARAMETER m.nro_imp
PRIVATE M.RETORNO, m.area_act, m.no1, m.no2, m.no3, m.no4, m.no5, m.no6, m.no7, resu, ok

*m.retorno  = .T.
*m.area_act = SELECT()
*SELECT INMUEBLE
*IF m.retorno
*   m.retorno=SEEK(STR(m.nro_imp,10,0))
*ENDIF      
*IF M.RETORNO
*   ok=''
*else   
*   RETURN ' '
*ENDIF   

private m.no1, m.no2, m.no3, m.no4, m.no5, m.no6, m.no7, resu
m.no1=''
m.no2=''
m.no3=''
m.no4=''
m.no5=''
m.no6=''
m.no7=''
resu=''

m.no1 = alltrim(STR(inmueble.CIRCUN,2,0))+" "+ALLTRIM(inmueble.SECCION)+' '
if inmueble.numchacra<>0
	m.no2='Ch. '+ALLTRIM(STR(inmueble.NUMCHACRA,4,0))+ALLTRIM(inmueble.LETCHACRA)+' '
endif
if inmueble.numquinta<>0        
    m.no3='Qta. '+ALLTRIM(STR(inmueble.NUMQUINTA,4,0))+ALLTRIM(inmueble.LETQUINTA)+' '
endif    
if inmueble.numfrac<>0
	m.no4='Fr. '+ALLTRIM(STR(inmueble.NUMFRAC,4,0))+' '+ALLTRIM(inmueble.LETFRAC)+' '
endif
if inmueble.nummazna<>0
	m.no5='Mz. '+ALLTRIM(STR(inmueble.NUMMAZNA,4,0))+ALLTRIM(inmueble.LETMAZNA)+' '
endif
if inmueble.numparc<>0
	m.no6="Par :"+ALLTRIM(STR(inmueble.NUMPARC,4,0))+ALLTRIM(inmueble.LETPARC)
endif
m.no7=" "+ALLTRIM(inmueble.SUBPARC)
*SELECT (m.area_act)

resu=m.no1+m.no2+m.no3+m.no4+m.no5+m.no6+m.no7
return resu

