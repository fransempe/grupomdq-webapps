****************************
FUNCTION DTOSTR
****************************
* Autor : CRISTIAN
*
* Fecha : 7/11/94
*
* Ultima Modificaci�n : 7/11/94
*
* Convierte una fecha completa (dd/mm/aa) en una cadena en la que se indica
* el nombre del mes.
* 
*
* Ejemplo1 :    Llamada .... DTOSTR ({12/5/94}, .T.)
*
*									Resultado....'12 de Mayo de 1994'
*
* Ejemplo2 :    Llamada .... DTOSTR ({12/5/94}, .F.)
*
*									Resultado....'12 de Mayo'
* 
* Par�metros :      wfecha  (D)  : fecha a convertir.
*										con_anio (L) : flag que determina si se indica el anio.
*																	 .T. : en la cadena se indica el a�o.
*																	 .F. : en la cadena NO se indica el a�o.
*
***************************************************************************


PARAMETERS wfecha, con_anio
PRIVATE cadena, wdia, wmes, wanio, wnomes, retorno

retorno = .T.
cadena  = ''

IF SET('DEBUG') = 'ON'
	IF PARAMETER() < 2
		WAIT WIND 'El Nro. de par�metros pasados a DTOSTR () es incorrecto.'
		retorno = .F.
	ELSE
		IF TYPE('wfecha') # 'D' OR TYPE('con_anio') # 'L'
			WAIT WIND 'Tipo de par�metro incorrecto pasado a DTOSTR ().'
			retorno = .F.
		ENDIF
	ENDIF
ENDIF

wdia   = DAY (wfecha)
IF wdia = 0
	retorno = .F.
	??CHR(7)
	= msg('ERROR: Debe pasar una fecha a la funci�n DTOSTR()')
ENDIF
IF retorno

	wdia   = DAY (wfecha)
	wmes   = STR (MONTH (wfecha), 2)
*	wnomes = TI_CONTA ('MESESINT','descl','wmes')
	wnomes = tab_int( "MESESINT", 'DESCL', 'wmes','','CONTA')
	wanio  = YEAR (wfecha)

	cadena = ALLT (STR (wdia)) + ' de ' + ALLT (wnomes)

	IF con_anio
		cadena = cadena + ' de ' + ALLT (STR (wanio, 4))
	ENDIF
ENDIF

RETURN cadena
*
