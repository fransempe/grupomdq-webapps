*************************************************
* FUNCTION get_DeCo
*************************************************
* Autor : Gabriel Zubieta
* Dise�o: Gabriel
* 
* Fecha : 12/05/95
* 
* Funcionamiento:
*  Dado tipo y c�digo devuelve descripcion de la condicion
*  referencia devuelve la condicion.
* 
* 
* Par�metros:
* tipcond  -C- Tipo de Condici�n
* codcond  -C- C�digo de Condici�n 
*
* Modificaciones:
* 
PARAMETERS m.tipcond, m.codcond
PRIVATE retorno
retorno = ''
IF SEEK( m.tipcond + m.codcond , 'condprog' )
	retorno = condprog.dsc_cond
*ELSE	
*	=msg( 'No se encuentra el C�digo de Condici�n ' + m.tipcond + ' ' + m.codcond )
ENDIF
RETURN retorno
