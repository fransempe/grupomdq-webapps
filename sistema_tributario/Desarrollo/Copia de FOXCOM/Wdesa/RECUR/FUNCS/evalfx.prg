*************************************************
* FUNCTION EVALFX
*************************************************
* Autor : Rodrigo
* Dise�o: Rodrigo
* Dada una f�rmula y un tipo debe evaluarla y validarla contra el tipo
* devolviendo el valor evaluado (Se captura el ONERROR)
*
* Par�metros:
* formula -C- Fla para evaluar.
* Tipofla -C- Tipo que debe ser. Caracter, Numerica, Logica, etc.
*
* NOTA:
*				Se debe setear loa variable 'onerror' al principio del programa	
*				principal con:
*											onerror = on('error')
*
*
* Modificaciones:
* 30/06/1999: Cuando restauraba el seteo de ON ERROR, en vez de hacerlo
*							con la variable onerror, lo hac�a con nerror.
*							Ejecutando el sistema fuera del entorno, luego de utilizar
*							un programa que llama a esta funci�n, al salir y tratar de
*							ingresar a otro programa, arrojaba el mensaje Debe ingresar
*							al sistema con el comando SIFIM.
*
PARAMETERS m.formula, m.tipofla, msg
PRIVATE m.nerror, m.exp

IF Type ('m.onerror') != 'C'
	PRIVATE onerror
	m.onerror = On ('error')
ENDIF

m.nerror 	= " "
IF !EMPTY(m.formula)
	on error m.nerror = message()
	m.exp 		= EVAL( m.formula) 
	on error &onerror
	IF TYPE('m.exp') # m.tipofla
		DO CASE
		CASE m.tipofla = 'C'
			m.exp	=	''
		CASE m.tipofla = 'N'
			m.exp	=	0
		CASE m.tipofla = 'L'
			m.exp	=	.T.
		CASE m.tipofla = 'D'
			m.exp	=	{}
		ENDCASE
	ENDIF
ELSE
	DO CASE
	CASE m.tipofla = 'C'
		m.exp	=	''
	CASE m.tipofla = 'N'
		m.exp	=	0
	CASE m.tipofla = 'L'
		m.exp	=	.T.
	CASE m.tipofla = 'D'
		m.exp	=	{}
	ENDCASE
ENDIF

IF !(m.nerror == " " )
	IF msg
		=MSG('La Expresi�n es inv�lida : ' + m.nerror)
  ENDIF
ENDIF

RETURN m.exp
