*************************************************
*FUNCTION FCalIva
*************************************************
* Autor :Elio
* Dise�o:Pachu
* 
* Fecha :28-02-95
* 
* Funcionamiento:Calcula los Importes del Impuesto al Valor Agregado
* 
* 
* Par�metros:	m.imp_grav	(N): Importe gravado por el Iva
*	 			m.recurso	(C): recurso
*				m.conc_cc	(C): concepto de Cuenta Cte.
*				m.sit_iva	(C): situacion del contribuyente frente al Iva
*				m.msg		(L): emite mensaje ?
*				m.cal_ivape	(L): calcula iva percepcion?
*				@m.conc_iva1(N): Concepto de Deveng. del IVA 1
*				@m.conc_iva2(N): Concepto de Deveng. del IVA 2
*				m.minimo	(L): Tiene en cuenta el m�nimo en IVA PE
* 
* Modificaciones:
* 
* IMPORTANTE : para trabajar con esta funci�n se deben declarar en el
* 						 proceso principal las siguientes variables:
*		iva_cf, iva_ri, iva_rn, iva_ex, iva_pe (Numeric)
*
PARAMETERS m.imp_grav, m.recurso, m.conc_cc, m.sit_iva, m.msg, m.cal_ivape,;
m.conc_iva1, m.conc_iva2, m.minimo

IF SET ('DEBUG') = 'ON'
	=VerNuPar ('FCALIVA', PARAMETERS()	, 9 )
	=VerTiPar ('FCALIVA', 'M.IMP_GRAV'	,'N')
	=VerTiPar ('FCALIVA', 'M.RECURSO'	,'C')
	=VerTiPar ('FCALIVA', 'M.CONC_CC'   ,'C')
	=VerTiPar ('FCALIVA', 'M.SIT_IVA'   ,'C')
	=VerTiPar ('FCALIVA', 'M.MSG'  	  	,'L')
	=VerTiPar ('FCALIVA', 'M.CAL_IVAPE' ,'L')
	=VerTiPar ('FCALIVA', 'M.CONC_IVA1' ,'N')
	=VerTiPar ('FCALIVA', 'M.CONC_IVA2' ,'N')
	=VerTiPar ('FCALIVA', 'M.MINIMO'	,'L')
ENDIF

IF TYPE('iva_cf') = 'U' OR TYPE('iva_ri') = 'U' OR TYPE('iva_rn') = 'U';
OR TYPE('iva_ex') = 'U' OR TYPE('iva_pe') = 'U'
  = msg('Para utilizar la Funcion FCALIVA() se deben definir unas variables como globles,';
   + CHR(13) +'; por favor, verifique la documentaci�n.')
  RETURN .F.
ENDIF

PRIVATE retorno

m.conc_iva1	= 0
m.conc_iva2	= 0
STORE 0 TO m.iva_cf , m.iva_ri, m.iva_rn, m.iva_ex, m.iva_pe

retorno =  FCon_CC(m.recurso, m.conc_cc, m.msg)
IF retorno AND concs_cc.grav_iva = 'S'

	DO CASE
	CASE m.sit_iva = 'CF'

		m.iva_cf 	= ROUND(concs_cc.por_ivacf / 100 * m.imp_grav, 2)
		m.conc_iva1	=	concs_cc.con_ivacf
		m.conc_iva2	=	0

	CASE m.sit_iva = 'RI'

		m.iva_ri 	= ROUND(concs_cc.por_ivari / 100 * m.imp_grav, 2)
		m.conc_iva1	=	concs_cc.con_ivari
		m.conc_iva2	=	0
		IF m.cal_ivape
			retorno = FCalIPE(m.imp_grav, m.recurso, m.msg, @m.conc_iva2, m.minimo)
		ENDIF

	CASE m.sit_iva = 'RN'

		m.iva_ri	= ROUND(concs_cc.por_ivari / 100 * m.imp_grav, 2)
		m.iva_rn 	= ROUND(concs_cc.por_ivarn / 100 * m.imp_grav, 2)
		m.conc_iva1	= concs_cc.con_ivari
		m.conc_iva2	= concs_cc.con_ivarn

	CASE m.sit_iva = 'EX'

		m.iva_ex 	= ROUND(concs_cc.por_ivaex / 100 * m.imp_grav, 2)
		m.conc_iva1	= concs_cc.con_ivaex
		m.conc_iva2	= 0

	CASE m.sit_iva = 'EP'

		m.iva_ri	= ROUND(concs_cc.por_ivari / 100 * m.imp_grav, 2)
		m.conc_iva1	= concs_cc.con_ivari
		m.conc_iva2	= 0

	CASE m.sit_iva = 'MO'

		m.iva_ri	= ROUND(concs_cc.por_ivamo1 / 100 * m.imp_grav, 2)
		m.iva_rn 	= ROUND(concs_cc.por_ivamo2 / 100 * m.imp_grav, 2)
		m.conc_iva1	= concs_cc.con_ivamo1
		m.conc_iva2	= concs_cc.con_ivamo2

	OTHERWISE

		m.conc_iva1	= 0
		m.conc_iva2	= 0
		retorno 	= .F.
		IF m.msg
			= mens_avi(sistema, 'SITUACION DE IVA NO VALIDA')
		ENDIF

	ENDCASE
ELSE
	IF m.msg AND !retorno
		= mens_avi(sistema, 'NO SE PUDO CALCULAR IVA')
	ENDIF
ENDIF

RETURN retorno

