*************************************************
*FUNCTION fvcontri
*************************************************
* Autor  : Gabriel Zubieta
* Dise�o : Pachu
* Fecha  : 23/02/95
* 
* Funcionamiento:
*  prepara datos y llama screens para mostrarlos.
*
* Parametros:
*  tipo_imp -C- Tipo de imponible
*	 nro      -N- Numero de Comercio o Inmueble correspondiente	  
*
* Supone :
*     - Contrib y Codifics abierta y exp_con y exp_cod seteadas 
*       para busqueda sobre primario pl 1 y pl 2 respectiva//.
*     - Debe estar incorporada entre las ventanas secundarias la 
*       ventana fvcontri.SC* y la tabla de validacion fvcontri.DB*
* 
************************************************************************
*	
*	SQLMODI-01
*		Usuario							: Todos
*		Fecha								: 22/11/2000
*		Quien modific�			: Carlos
*		Detalle problema		: Cuando es invocada desde un ABM (comercios,
*													cemenerio, veh�culos, pisa el campo deleg
*													del programa llamador.
*													
*		Detalle correcci�n	: Se puso private la variable deleg
*													
*		Programas tocados		: FVContri.prg y se recompilan los ABMs de comercios
*													cementerio 1 y 2 y veh�culos.
*													
************************************************************************
PARAMETERS m.tipo_imp, m.nro

PRIVATE retorno, m.cuit1, m.cuit2, area_ant

PRIVATE m.cuit, m.dig_veri, m.nomb_cont, m.cod_post, m.nomb_loc, m.cod_calle
PRIVATE m.nomb_calle, m.puerta, m.puertabis, m.piso, m.depto, m.fec_nac
PRIVATE m.tel1_con, m.tel2_con, m.tel3_con,m.sit_iva, m.tipo_cont,m.obs_cont
PRIVATE m.cod_bajcn, m.fec_baja

PRIVATE m.auxiliar1

PRIVATE m.tipo_vinc, m.d_tipo, m.porc_vinc,	m.obs_vinc 

PRIVATE m.d_sitiva, m.d_tipocont


* Comienzo SQLMODI-01

PRIVATE m.deleg
m.deleg = ''

* Fin SQLMODI-01


PUSH KEY
IF !c_nuevo AND !c_edita
	ON KEY LABEL CTRL+S *
	ON KEY LABEL CTRL+A *
ENDIF
ON KEY LABEL F5 *
ON KEY LABEL F6 *

retorno = .T.
m.cuit1 = 0
m.cuit2 = 0
area_ant = SELECT()

= FVinCon( m.tipo_imp , m.nro, .F. , .F. , @m.cuit1 , @m.cuit2 )

IF m.cuit1 < 0 AND m.cuit2 > 0
	=mens_avi( m.sistema , "IMPONIB. SIN CUIT1 CON CUIT2" )
ENDIF

IF m.cuit1 > 0 OR m.cuit2 > 0
	
	IF m.cuit1 > 0
		m.cuit = m.cuit1
	ELSE
		m.cuit = m.cuit2
	ENDIF
		
	SELECT contrib
	IF SEEK( EVAL(exp_con) , 'contrib' )
		
		SCATTER MEMVAR MEMO
			
		IF !EMPTY( m.sit_iva )
			m.d_sitiva = tab_int( 'SITSIVA' , 'DESCC' , 'm.sit_iva','',"RECUR" )
		ELSE
			m.d_sitiva = ''	
		ENDIF
			
		m.d_tipocont = get_desc( m.tipo_cont , "TIPCON" )
			
		m.tipo_vinc	 = vinc_imp.tipo_vinc
		m.d_tipo     = tips_vin.dsc_tvinc
		m.porc_vinc  = vinc_imp.porc_vinc
		m.obs_vinc   = vinc_imp.obs_vinc		 
			
	ELSE
		
		=msg('El Imponible est� vinculado con un Contribuyente con C.U.I.T. ' + ALLT(STR(m.cuit)) + ' pero tal Contribuyente no existe.')
		retorno = .F.	
	ENDIF		
		
ELSE
	
	=mens_avi( m.sistema , "IMPONIB. SIN CONTRIB.VINC." )
	retorno = .F.
ENDIF
	
IF retorno
	HIDE WIND CONTROLS
	DO fvcontri.SPR
	SHOW WIND CONTROLS
*	_CUROBJ = OBJNUM( m.contri )			
ENDIF

SELECT( area_ant )
POP KEY

RETURN retorno
*
