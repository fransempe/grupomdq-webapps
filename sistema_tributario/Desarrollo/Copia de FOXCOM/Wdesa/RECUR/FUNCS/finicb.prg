*************************************************
* FIniCB
*************************************************
* Autor : Gabriel Zubieta
* Dise�o: Gabriel Zubieta
* 
* Fecha : 28/03/95
* 
* Funcionamiento:
* 
* 
* Par�metros:
* codigo -C- Codigo para ser convertido a bytes y puesto en los lugares
*  			     correspondientes del vector.
* 
* Modificaciones:
* 
PARAMETERS m.codigo
EXTERNAL ARRAY VCodBar
PRIVATE retorno, m.cod, j

retorno = .T.
m.cod = ''
j=1

DO WHILE j <= 10
	m.nro = SUBSTR( m.codigo , j , 1 )
	DO CASE
	CASE m.nro = '0'
		m.cod = m.cod + CHR(0) + CHR(0) + CHR(0)
		
	CASE m.nro = '1'
		m.cod = m.cod + CHR(0) + CHR(0) + CHR(0) + CHR(0) +;
									  CHR(0) + CHR(0) + CHR(0)
	
	CASE m.nro = '2'
		m.cod = m.cod + CHR(255) + CHR(255)
	
	CASE m.nro = '3'
		m.cod = m.cod + CHR(255) + CHR(255) + CHR(255) + CHR(255) +;
									  CHR(255) + CHR(255)
	
	ENDCASE
	j = j + 1
ENDDO
VCodBar[i] = m.cod
i = i + 1

RETURN retorno

