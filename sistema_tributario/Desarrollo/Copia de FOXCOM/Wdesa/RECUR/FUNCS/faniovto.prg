*************************************************
FUNCTION FAnioVto
*************************************************
* 
* Fecha : 14/12/2000
* 
* Funcionamiento:
* Busca en la cta. cte. o en la cta. cte. cancelada un movimiento
* y retorna el a�o de la fecha de vto. del mismo.
* Si el movimiento no existe, retorna el a�o pasado como par�metro.
* 
PARAMETERS recurso, tipo_imp, nro_imp, anio, cuota, nro_mov
PRIVATE retorno
retorno = m.anio

IF Seek (m.recurso + m.tipo_imp + Str (m.nro_imp, 10, 0) + ;
				 Str (m.anio, 4, 0) + Str (m.cuota, 3, 0) + Str (m.nro_mov, 3, 0), 'rec_cc')
	retorno = Year (rec_cc.fecven_mov)
ELSE
	IF Seek (m.recurso + m.tipo_imp + Str (m.nro_imp, 10, 0) + ;
					 Str (m.anio, 4, 0) + Str (m.cuota, 3, 0) + Str (m.nro_mov, 3, 0), 'rec_ccc')
		retorno = Year (rec_ccc.fecven_mov)
	ENDIF
ENDIF

RETURN retorno
