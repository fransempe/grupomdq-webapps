*************************************************
*FUNCTION FCalIPE
*************************************************
* Autor :Elio
* Dise�o:Pachu
* 
* Fecha :13-03-95
* 
* Funcionamiento: carga en variables globales el concepto, porcentage,
* 								e importe minimo  del IVA Percepcion y calcula dicho Iva
* 
* 
* Par�metros:	m.imp_grav		(N) : importe gravado por el iva
*				m.recurso 		(C) 
* 				m.msg     		(L) : emite mensajes de aviso ?
* 				@m.conc_iva2	(N)	: Concepto de Dev. corres.p al IVA 2
*				m.minimo		(L)	: Tiene en cuenta el minimo?
*
* Modificaciones:
* IMPORTANTE : para trabajar con esta funci�n se deben declarar en el
* 						 proceso principal las siguientes variables:
*		ivape_conf (L): IVA PE Configurado ## debe estar inicializada como .F.
*		conc_ivape (N): Concepto de IVA Percepcion
*		por_ivape  (N): Porcentaje de Iva Percepcion
*		min_ivape  (N): Minimo de Iva Percepcion
* 
PARAMETERS m.imp_grav, m.recurso, m.msg, m.conc_iva2, m.minimo

IF SET ('DEBUG') = 'ON'
	=VerNuPar ('FCALIPE', PARAMETERS()	, 5 )
	=VerTiPar ('FCALIPE', 'M.IMP_GRAV'	,'N')
	=VerTiPar ('FCALIPE', 'M.RECURSO'	,'C')
	=VerTiPar ('FCALIPE', 'M.MSG'       ,'L')
	=VerTiPar ('FCALIPE', 'M.CONC_IVA2'	,'N')
	=VerTiPar ('FCALIPE', 'M.MINIMO'    ,'L')
ENDIF

IF TYPE('m.ivape_conf') # 'L' OR TYPE('m.conc_ivape') # 'N' OR TYPE('m.por_ivape') # 'N' OR TYPE('m.min_ivape') # 'N'
	= msg('Para correr esta funci�n "FIVA_PE", se deben declarar unas variables en el programa principal. Verifique la documentaci�n')
	RETURN .F.
ENDIF

PRIVATE retorno
retorno = .T.
IF !m.ivape_conf
	retorno = FIVA_PE(m.recurso, m.msg, .F.)
ENDIF
IF retorno
	IF m.conc_ivape > 0
		m.iva_pe = ROUND(m.por_ivape / 100 * m.imp_grav, 2)
		IF !m.minimo AND ABS(m.iva_pe) < m.min_ivape
			m.iva_pe = 0
		ENDIF
	ELSE
		m.iva_pe = 0
	ENDIF
ELSE
	m.iva_pe = 0
ENDIF

IF m.iva_pe # 0
	m.conc_iva2	=	m.conc_ivape
ELSE
	m.conc_iva2	=	0
ENDIF

RETURN retorno