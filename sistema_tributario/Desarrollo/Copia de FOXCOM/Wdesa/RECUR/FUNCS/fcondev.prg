*************************************************
*FUNCTION FConDev
*************************************************
* Autor :Elio
* Dise�o:Pachu
*
* Fecha :28-02-95
*
* Funcionamiento: lee el registro correspondiente a un Concepto de
* 								Devengamiento
*
*
* Par�metros: M.RECURSO  (C): tipo de recurso
* 						M.CONC_DEV (C): concepto de devengamiento
* 						M.MSG			 (L): emite mensaje ?
*
* Modificaciones:
*
parameter m.recurso, m.conc_dev, m.msg

IF SET ('DEBUG') = 'ON'
	=vernupar ('FCONDEV', parameters()	, 3 )
	=vertipar ('FCONDEV', 'M.RECURSO'		,'C')
	=vertipar ('FCONDEV', 'M.CONC_DEV'  ,'N')
	=vertipar ('FCONDEV', 'M.MSG '		  ,'L')
ENDIF

private retorno
retorno = .t.
if concsdev.recurso = m.recurso and concsdev.conc_dev = m.conc_dev
   retorno = .t.
else
   private clave_cdev
   clave_cdev = getkey('concsdev', .t.)
   if !seek(EVAL(clave_cdev), 'concsdev')
      retorno = .f.
      if m.msg
         = mens_avi(sistema, 'CONC. DEV. INEXISTENTE')
      endif
   endif
endif

return retorno
*
