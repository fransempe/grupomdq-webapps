*************************************************
* FUNCTION FVinCon
*************************************************
* Autor  : Elio
* Diseno : Pachu
* Fecha  : 16-02-95
* 
* Funcionamiento:Retorna CUITs de Contibuyentes vinculado con un 
*                imponible determinado
* 
* Par�metros:	m.tipo_imp (C): tipo de imponible
* 				m.nro_imp  (N): nro. de imponible
*				m.bloq     (L): �Bloquea?
*				m.msg      (L): emite mensage de aviso ?
*				@m.cuit1   (N): cuit del responsable del pago
*				@m.cuit2   (N): cuit del inquilino
* 
* Modificaciones: Elio 24-02-95 Ver Diagrama de Flujo
* 					Leo se sacaron variables y se suponen definidas previamente
*                   para mejorar la performance, que era un asco
*					Rodrigo: Se agregaron los tipos de vinculaciones para
*					el CUIT1 y CUIT2 (23/5/95)
*
*	NOTA: Para usar las variables TIPO_VINC1 y TIPO_VINC2 de referencia
*			global, se deben declarar en el SETUP de la pantalla principal.
*
PARAMETER m.tipo_imp, m.nro_imp, m.bloq, m.msg, m.cuit1, m.cuit2

IF SET ('DEBUG') = 'ON'
	IF TYPE('clave_tip') <> 'C'
		= MSG('No est� definido el indice clave_tip, para acceder a TIP_VINC. Hablar con Leo.')
	ENDIF

	IF TYPE('tipo_impc') <> 'C'
		= MSG('No est� definido la variable m.tipo_impc, para acceder al tipo de Contribuyente. Hablar con Leo.')
	ENDIF

	IF TYPE('clave_vin') <> 'C'
		= MSG('No est� definido el indice clave_vin, para acceder a VINC_IMP. Hablar con Leo.')
	ENDIF

	IF TYPE('clave_vic') <> 'C'
		= MSG('No est� definido el indice clave_vic, para acceder a VINC_IMP. Hablar con Leo.')
	ENDIF
	*clave_tip = getkey('tips_vin', .T.)
	*clave_vin = getkey('vinc_imp', .T., 3)
	*clave_vic = getkey('vinc_imp', .T.)
	
ENDIF

PRIVATE retorno, m.prior1, m.prior2
PRIVATE m.tipo_impv, m.tipo_vinc

retorno 		= .T.
m.cuit1 		= -1
m.cuit2 		= -1
* OJO:  No poner estas 2 variables como PRIVATE
m.tipo_vinc1	= ''
m.tipo_vinc2	= ''
m.prior1		= 10
m.prior2 		= 10

m.tipo_impv = m.tipo_impc

*MODI TESTEO:Seleccionar tabla por error en un reporte*
PRIVATE guarda_alias
m.guarda_alias = ALIAS()
SELECT vinc_imp
*FIN MODI TESTEO

= SEEK (EVAL(clave_vin), 'vinc_imp')

*MODI TESTEO:Seleccionar tabla por error en un reporte*
SELECT (m.guarda_alias)
*FIN MODI TESTEO
	
DO WHILE retorno AND !EOF('vinc_imp') AND m.tipo_imp = vinc_imp.tipo_imp;
	AND m.nro_imp = vinc_imp.nro_imp AND m.tipo_impc = vinc_imp.tipo_impv

	IF EMPTY(vinc_imp.fec_baja)
		m.tipo_vinc = vinc_imp.tipo_vinc

		IF tips_vin.tipo_vinc == vinc_imp.tipo_vinc OR ;
			SEEK (EVAL (clave_tip), 'tips_vin')

			IF tips_vin.pri_cuit1 > 0 AND tips_vin.pri_cuit1 < m.prior1
				m.cuit1 		= vinc_imp.nro_impv
				m.prior1 		= tips_vin.pri_cuit1
				m.tipo_vinc1	= vinc_imp.tipo_vinc
			ENDIF

			IF tips_vin.pri_cuit2 > 0 AND tips_vin.pri_cuit2 < m.prior2
				m.cuit2 		= vinc_imp.nro_impv
				m.prior2 		= tips_vin.pri_cuit2
				m.tipo_vinc2	= vinc_imp.tipo_vinc
			ENDIF
		ELSE
			retorno = .F.
			IF m.msg
				= mens_avi(sistema, 'TIPO DE VINCULACION INEXIST.')
			ENDIF
		ENDIF
	ENDIF
	SKIP IN vinc_imp
ENDDO

IF retorno	
	retorno = lee_ContVinTip()
ENDIF

RETURN retorno


*************************************************
FUNCTION lee_ContVinTip
*************************************************
* Autor:Elio
* DIse�o:Pachu
* Fecha:24-02-95
* 
PRIVATE retorno, m.mens, m.cuit_cont, m.nro_impv
retorno = .T.
IF m.msg
	m.mens = 1
ELSE
	m.mens = 0
ENDIF

m.cuit_cont = 0

IF m.cuit1 > 0
	m.cuit_cont = m.cuit1
ELSE
	IF m.cuit2 > 0
		m.cuit_cont = m.cuit2
	ELSE
		retorno = .F.
	ENDIF
ENDIF

IF m.cuit_cont > 0
	retorno = FLeeImp(m.tipo_impc, m.cuit_cont, -1, m.bloq, m.mens, ' ')

	IF retorno
		m.nro_impv = m.cuit_cont
		IF SEEK (EVAL(clave_vic), 'vinc_imp')
			IF tips_vin.tipo_vinc # vinc_imp.tipo_vinc
				m.tipo_vinc = vinc_imp.tipo_vinc
				= SEEK (EVAL(clave_tip), 'tips_vin')
			ENDIF
		ENDIF
	ELSE
		IF m.msg
			= MENS_AVI('RECUR','CONTRIB. VINC. DADO DE BAJA')		
		ENDIF
	ENDIF

ENDIF

RETURN retorno

