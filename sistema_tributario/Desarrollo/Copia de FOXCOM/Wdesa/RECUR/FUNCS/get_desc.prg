*************************************************
FUNCTION GET_DESC
*************************************************
*
* Fecha : 06/10/2000
* 
* Funcionamiento: Es una copia de la get_desc de conta/funcs, pero
* recibe solo 2 par�metros y al rev�s que la otra.
* 
* Est� hecha as� porque hab�a una get_desc.prg en recur que no s� donde
* habr� ido a parar, y todos los programas que la usaban, con la de
* conta no andan (no trae las descripciones), justamente por tener los
* par�metros al rev�s.
* 
PARAMETERS codif, tipo_cod, desc_corta
PRIVATE retorno, act_reg

retorno = ''
IF PARAMETERS() < 2
	=Msg('La cantidad de Par�metros es err�nea. Funci�n Get_Desc')
ELSE
	act_reg = RECNO ('codifics')
	
	PRIVATE alias_ante
	m.alias_ante = ALIAS()
	SELECT codifics
	
	IF SEEK (PADR (m.tipo_cod,6) + PADR (m.codif,6),'codifics')
		retorno = codifics.desc_cod
	ELSE
		retorno = ''
	ENDIF
	
	SELECT (m.alias_ante)
	
	=IR_REG (act_reg, 'codifics')
ENDIF

RETURN retorno
