*************************************************
* FUNCTION FLeeComp
*************************************************
&& Lee un comprobante pasado como par�metro.
&& 
&& Autor			: Carlos.
&& Program�		: Cristian.
&&
&& Par�metros :
&& 		grupo_comp  (N) : grupo del comprobante.
&&		nro_comp		(N)	: nro. del comprobante.
&&		dv_comp			(N)	: d�g. verificador del comprobante.
&&		walias			(C)	: alias de la tabla caj_tran.
&&		chkdig			(L)	: indica si chequea d�g. verificador.
&&		bloquea			(L)	: indica si bloquea el registro.
&&		mensaje			(L)	: indica si saca mensajes por pantalla.
&&
PARAMETERS grupo_comp, nro_comp, dv_comp, walias, chkdig, bloquea, mensaje 
PRIVATE retorno, wexp_comp
retorno = .T.

=VerNuPar ('FLeeComp', PARAMETERS (), 7)

=VerTiPar ('FLeeComp', 'grupo_comp', 'N')
=VerTiPar ('FLeeComp', 'nro_comp', 'N')
=VerTiPar ('FLeeComp', 'dv_comp', 'N')
=VerTiPar ('FLeeComp', 'walias', 'C')
=VerTiPar ('FLeeComp', 'chkdig', 'L')
=VerTiPar ('FLeeComp', 'bloquea', 'L')
=VerTiPar ('FLeeComp', 'mensaje', 'L')

retorno = USET ('recur\comprob', 'PRIMARIO', walias)
IF retorno
	wexp_comp = GETKEY (walias, .T.)
	IF SEEK (&wexp_comp, walias)
		IF chkdig
			IF m.dv_comp # &walias..dv_comp
				retorno = .F.
				IF mensaje
					=MSG ('El d�gito verificador es incorrecto. Verifique.')
				ENDIF
			ENDIF
		ENDIF
		IF retorno
			IF bloquea
				IF !RLOCK (walias)
					retorno = .F.
					IF mensaje
						=MSG ('El comprobante especificado no se puede bloquear.')
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		retorno = .F.
		IF mensaje	
			=MSG ('El comprobante especificado no existe.')
		ENDIF
	ENDIF
ENDIF

RETURN retorno
