*************************************************
* FUNCTION get_cond
*************************************************
* Autor : Gabriel Zubieta
* Dise�o: Gabriel
* 
* Fecha : 21/03/95
* 
* Funcionamiento:
*  Dado tipo y c�digo devuelve .T. o .F. si pudo tomar condicion y por
*  referencia devuelve la condicion.
* 
* 
* Par�metros:
* tipcond  -C- Tipo de Condici�n
* codcond  -C- C�digo de Condici�n 
* @cond    -C- Condici�n
* tipodato -C- Tipo de dato que debe tener la condicion
*
* Modificaciones:
* 
PARAMETERS m.tipcond, m.codcond, m.cond, m.tipodato
PRIVATE retorno
retorno = .F.
m.cond = ''
IF SEEK( m.tipcond + m.codcond , 'condprog' )
	m.cond = condprog.condicion
	IF val_form( m.cond , m.tipodato )
		retorno = .T.
	ENDIF
ELSE
	=msg( 'No se encuentra el C�digo de Condici�n ' + m.tipcond + ' ' + m.codcond )
ENDIF
RETURN retorno

