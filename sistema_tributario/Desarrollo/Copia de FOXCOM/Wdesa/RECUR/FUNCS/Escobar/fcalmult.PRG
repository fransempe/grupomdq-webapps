**************************************************
* FUNCTION FCalMult
************************************************** 
* Funcionamiento:
* Calcula el importe de multa para la Municipalidad de Escobar.
PARAMETERS fecha_vto, factual, importe, interes
PRIVATE retorno, porcentaje
retorno = 0

IF m.programa # 'PGENPL2' OR !(m.cod_plan = 'LE')
	RETURN retorno
ENDIF

IF !EMPTY (juicio)
	m.porcentaje = 50
	
	IF m.fecha_vto < m.factual
		retorno = m.importe * (m.porcentaje / 100)     
	ENDIF
ENDIF

RETURN retorno
