*************************************************
*FUNCTION FCIvaPE
*************************************************
* Autor :Elio
* Dise�o:Pachu
* 
* Fecha :13-03-95
* 
* Funcionamiento: Calcula unicamente el importe por IVA Percepcion
* 								
* 
* Par�metros: m.imp_grav		(N) : importe gravado por el IVA
*							m.recurso 		(C) 
*							m.sit_iva 		(C) : situacion del contribuyente frente al IVA
* 						m.msg     		(L) : emite mensajes de aviso ?
* 						@m.conc_iva2	(N)	:	Concepto de deveng. corresp. al IVA 2
*							m.minimo			(L)	:	Tiene en cuenta el minimo
*
* Modificaciones:
* IMPORTANTE : para trabajar con esta funci�n se deben declarar en el
* 						 proceso principal las siguientes variables:
*		ivape_conf (L): IVA PE Configurado ## debe estar inicializada como .F.
*		conc_ivape (N): Concepto de IVA Percepcion
*		por_ivape  (N): Porcentaje de Iva Percepcion
*		min_ivape  (N): Minimo de Iva Percepcion
* 
PARAMETERS m.imp_grav, m.recurso, m.sit_iva, m.msg, m.conc_iva2, m.minimo

IF SET ('DEBUG') = 'ON'
	=VerNuPar ('FCIVAPE', PARAMETERS()	, 6 )
	=VerTiPar ('FCIVAPE', 'M.IMP_GRAV'	,'N')
	=VerTiPar ('FCIVAPE', 'M.RECURSO'		,'C')
	=VerTiPar ('FCIVAPE', 'M.SIT_IVA'		,'C')
	=VerTiPar ('FCIVAPE', 'M.MSG'       ,'L')
	=VerTiPar ('FCIVAPE', 'M.CONC_IVA2'	,'N')
	=VerTiPar ('FCIVAPE', 'M.MINIMO'    ,'L')
ENDIF

IF TYPE('m.ivape_conf') # 'L' OR TYPE('m.conc_ivape') # 'N';
 OR TYPE('m.por_ivape') # 'N' OR TYPE('m.min_ivape') # 'N'
	= msg('Para correr esta funci�n "FCIVAPE", se deben declarar unas variables en el programa principal. Verifique la documentaci�n')
	RETURN .F.
ENDIF

PRIVATE retorno
retorno = .T.
IF m.sit_iva = 'RI'
	m.iva_pe = 0
	retorno = FCalIPE(m.imp_grav, m.recurso, m.msg, @m.conc_iva2, m.minimo)
	IF !retorno
		IF m.msg
			= mens_avi(sistema, 'NO SE PUDO CALCULAR IVA PE')
		ENDIF
	ENDIF
ENDIF

RETURN retorno