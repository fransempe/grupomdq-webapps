*************************************************
* FUNCTION FLeeTIm
*************************************************
* Autor  : Gabriel Zubieta
* Dise�o : Pachu
* Fecha  : 09/02/95
* 
* Funcionamiento:
* Levanta de tips_imp el tipo de imponible, la mascara del tipo de imp.
* y el flag de si usa digito verificador
* 
* Par�metros:
* In  tab_maes  -C-
* In  emit_msg  -L-
* Ou  tipo_imp  -C-
* Ou  masc_timp -C-
* Ou  dig_timp  -C- 
*
*
* Modificaciones:
*
PARAMETERS m.tab_maes, m.emit_msg, m.tipo_imp, m.masc_timp, m.dig_timp
PRIVATE retorno, exp_tips

retorno = .T.
m.tipo_imp  = ''
m.masc_timp = ''
m.dig_timp  = ''

=USET('recur\tips_imp','tab_maes','tips')
*=USET ('D:\DBASE\Rojas\recur\tips_imp','tab_maes','tips')
exp_tips = GETKEY ('tips', .T. , 1 , 'tab_maes' )

IF SEEK( EVAL (exp_tips), 'tips' )
	m.tipo_imp  = tips.tipo_imp   && C1
	m.masc_timp = tips.masc_timp  && C15
	m.dig_timp  = tips.usa_digver && C1
ELSE
	IF ( m.emit_msg )
		=msg('El Tipo de Imponible ' + m.tab_maes + ' es Inv�lido ' )
	ENDIF	
	retorno = .F.
ENDIF
m.masc_timp = ALLT( m.masc_timp )
RETURN retorno

