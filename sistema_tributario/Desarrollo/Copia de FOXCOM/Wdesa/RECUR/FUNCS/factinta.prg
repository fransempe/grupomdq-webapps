*************************************************
* FUNCTION FACTINTA
*************************************************
* Autor : Leo
* Dise�o: Leo
* 
* Fecha : 16/3
* 
* Funcionamiento: Actualiza un Importe
* Par�metros: fecha de Vento
*             fecha final
*             importe origen
*             actualiza
*             interes
*
* Modificaciones:
* 
*PARAMETERS fecha_vto, fecha_act, importe, actualiza, interes
PARAMETERS fecha_vto, fecha_final, importe, actualiza, interes
PRIVATE dia_o, mes_o, anio_o, dias_m     
EXTERNAL ARRAY vec_act, vec_int
*IF SET ('DEBUG') = 'ON'
*	= VerNuPar('FACTINT', PARA(), 5)
*	= VerTiPar('FACTINT', 'fecha_vto',   'D')
*	= VerTiPar('FACTINT', 'fecha_final',   'D')
*	= VerTiPar('FACTINT', 'importe',     'C')
*	= VerTiPar('FACTINT', 'actualiza',   'N')
*	= VerTiPar('FACTINT', 'interes',     'N')
* IF TYPE ('vec_act') # 'N' OR TYPE ('vec_int') # 'N'
* 	=MSG ('No se configuraron los Indices de Actualizaci�n.')
* ENDIF
* IF TYPE ('anio_inic') # 'N' OR TYPE ('anio_cant') # 'N'
* 	=FINAL (1, 'No se configur� el a�o inicial o la cantidad inicial. Deber�a haber abortado antes el programa.')
* ENDIF
*ENDIF

*IF fecha_vto < fecha_act
IF fecha_vto < fecha_final OR m._int_noven = 'S'
	m.dia_o = DAY   (m.fecha_vto)
	m.mes_o = MONTH (m.fecha_vto)
	m.anio_o= YEAR  (m.fecha_vto)

	IF m.anio_o < m.anio_inic
		m.anio_o = m.anio_inic
	ENDIF
	IF m.mes_o = 0
		m.mes_o = 1
	ENDIF
	
	* preparo el dia 1 de este mes, mas 1 mes, menos 1 dia, para determinar la
	* cantidad de dias del mes de vencimiento
	m.dias_m=	DAY (GOMONTH (CTOD ('01/' + STR (m.mes_o, 2) + '/' + ;
                              STR (m.anio_o,4)), 1) - 1)
                       
	m.anio_o= (m.anio_o - m.anio_inic) * 12

	m.actualiza = CalcActual (VAL (m.importe))
	m.interes   = CalcInter  (VAL (m.importe) + m.actualiza)
ELSE
	m.actualiza = 0
	m.interes   = 0
ENDIF

RETURN .T.  



*************************************************
FUNCTION CalcActual
*************************************************
* Autor : Leo
* Dise�o: Leo
* 
* Fecha : 16/3/95
* 
* Funcionamiento: calcula la actualizaci�n
* Par�metros: importe
* Modificaciones:
* 
PARAMETERS importe
PRIVATE coefic

IF usa_fx_act
	RETURN ROUND (FX_ACTUA (m.importe), 2)
ENDIF

m.coefic = vec_act [m.anio_o + m.mes_o, 1]

IF vec_act [m.anio_o + m.mes_o, 2] <> 0
	m.coefic = m.coefic * (1 + vec_act [m.anio_o + m.mes_o, 2] *;
						 (m.dias_m - m.dia_o))
ENDIF

IF m.coefic < 1
	m.coefic = 0
ELSE
	m.coefic = m.coefic - 1
ENDIF

RETURN ROUND (m.importe * m.coefic, 2)


*************************************************
FUNCTION CalcInter
*************************************************
* Autor : Leo
* Dise�o: Leo
* 
* Fecha : 16/3/95
* 
* Funcionamiento: calcula el interes
* Par�metros: importe
* Modificaciones:
* 
PARAMETERS importe
PRIVATE coefic

IF usa_fx_int
	RETURN ROUND (FX_INTER (m.importe), 2)
ENDIF

m.coefic = vec_int [m.anio_o + m.mes_o, 1]
	
IF vec_int [m.anio_o + m.mes_o, 2] <> 0
	m.coefic = m.coefic + vec_int [m.anio_o + m.mes_o, 2] * (m.dias_m - m.dia_o)
ENDIF

RETURN ROUND (m.importe * m.coefic, 2)

