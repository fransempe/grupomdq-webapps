****************************************************************************
*	FUNCTION MONTHW
****************************************************************************
*	mes: n�mero de mes
*	capf: Si es verdadero devuelve el mes con la primera letra en may�scula
****************************************************************************
PARAMETERS mes, capf
PRIVATE mesw, retorno
DIMENSION mesw[12]
mesw[1] = 'Enero'
mesw[2] = 'Febrero'
mesw[3] = 'Marzo'
mesw[4] = 'Abril'
mesw[5] = 'Mayo'
mesw[6] = 'Junio'
mesw[7] = 'Julio'
mesw[8] = 'Agosto'
mesw[9] = 'Setiembre'
mesw[10] = 'Octubre'
mesw[11] = 'Noviembre'
mesw[12] = 'Diciembre'

IF capf
	retorno = mesw[mes]
ELSE
	retorno = LOWER(mesw[mes])
ENDIF

RETURN retorno