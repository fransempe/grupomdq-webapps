*************************************************
*FUNCTION FCon_CC
*************************************************
* Autor :Elio
* Dise�o:Pachu
* 
* Fecha :28-02-95
* 
* Funcionamiento: lee el registro correspondiente a un Concepto de Cuenta
* 								Corriente dado
* 
* 
* Par�metros: M.RECURSO  (C): tipo de recurso
* 						M.CONC_CC	 (C): concepto de Cuenta Cte.
* 						M.MSG			 (L): emite mensaje ?
* 
* Modificaciones:
* 
PARAMETER m.recurso, m.conc_cc, m.msg
IF SET ('DEBUG') = 'ON'
	=VerNuPar ('FCON_CC', PARAMETERS()	, 3 )
	=VerTiPar ('FCON_CC', 'M.RECURSO'		,'C')
	=VerTiPar ('FCON_CC', 'M.CONC_CC'   ,'C')
	=VerTiPar ('FCON_CC', 'M.MSG '		  ,'L')
ENDIF

PRIVATE retorno
retorno = .T.
IF concs_cc.recurso = m.recurso AND concs_cc.conc_cc = m.conc_cc
	retorno = .T.
ELSE
	PRIVATE clave_cta
	clave_cta = getkey('concs_cc', .T.)
	IF !SEEK( EVAL( clave_cta) , 'concs_cc')
		retorno = .F.
		IF m.msg
			= mens_avi(sistema, 'CONC. CTA. CTE. INEXISTENTE')
		ENDIF
	ENDIF
ENDIF
	
RETURN retorno

