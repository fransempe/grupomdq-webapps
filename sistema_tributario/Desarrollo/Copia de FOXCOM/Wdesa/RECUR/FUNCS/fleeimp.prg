*************************************************
*************************************************
* Autor  : Elio
* Diseno : Pachu
* Fecha  : 16-02-95
* 
* Funcionamiento:Lee un Imponible en la tabla maestra correspondiente
* 
* Par�metros: m.tipo_imp (C): tipo de imponible
* 						m.nro_imp  (N): nro. de imponible
*							m.dig_veri (N): digito verificador del imponible
*							m.bloq     (L): �Bloquea?
*							m.niv_msg  (N):  0 no emite ningun msg 
*                              1 emite todos los msg de aviso
*															-1 emite todos menos el mensaje de inex. de imponible
*							@m.est_imp (C): estado del imponible
* 
* Modificaciones:
* Elio    21/02/95          Se agreg� el comando EXTERNAL
* Gabriel 21/02/95 16,30 Se agrego retorno 'O' cuando devolvia OK
* Gabriel 22/02/95 11,00 Se cambia parametro msg(L) por niv_msg(N1)
*
PARAMETER m.tipo_imp, m.nro_imp, m.dig_veri, m.bloq, m.niv_msg, m.est_imp
*  estos arreglos deben ser declarados en el programa principal
EXTERNAL ARRAY masc_imp, dig_imp, tab_imp, ident_imp, desc_tab

PRIVATE retorno, m.nro_elem
PRIVATE m.msg, area_act

m.nro_elem = 0
retorno = .T.

area_act = SELECT ()

IF ( niv_msg != 0 )
	m.msg = .T.
ELSE
	m.msg = .F.
ENDIF

retorno = VerTipImp(m.tipo_imp, m.msg, @m.nro_elem)
IF retorno
	SELECT (tab_imp[m.nro_elem])
	
	IF SEEK (STR (m.nro_imp, 10))
	
		IF m.dig_veri > -1 AND m.dig_veri < 10 AND dig_imp[m.nro_elem] = 'S' AND m.dig_veri # dig_veri
			retorno = .F.
			m.est_imp = 'D'
			IF m.msg
				= mens_avi( sistema , 'DIGITO VERIFICADOR INCORRECTO')
			ENDIF
		ELSE
			m.est_imp = 'O'
		ENDIF
		
		IF retorno
			IF !EMPTY (fec_baja)
				retorno = .F.
				m.est_imp = 'B'
				IF m.msg
					= mens_avi(sistema, 'IMPONIBLE DADO DE BAJA')
				ENDIF
			ELSE
				IF m.bloq
					IF !RLOCK ()
						retorno = .F.
						m.est_imp = 'L'
						IF m.msg
							= mens_avi(sistema, 'IMPONIBLE BLOQUEADO')
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		retorno = .F.
		m.est_imp = 'N'
		IF m.niv_msg > 0
			= mens_avi(sistema, 'IMPONIBLE INEXISTENTE')
		ENDIF
	ENDIF
ELSE
	m.est_imp = 'T'
ENDIF

SELECT (area_act)
			
RETURN retorno


*************************************************
FUNCTION VerTipImp
*************************************************
* Autor:Elio
* Diseno:Pachu
* Fecha:16-02-95
* 
* Funcionamiento:Verifica la existencia de un tipo de imponible
* 
* Par�metros: m.tipo_imp  (C): tipo de imponible
*							m.msg       (L): � emite mensage de aviso ?
*							@m.nro_elem (N): n�mero de elemento de las matrices
* 
* Modificaciones:
* 
PARAMETER m.tipo_imp, m.msg, m.nro_elem

PRIVATE retorno
retorno = .T.
m.nro_elem = ASC(m.tipo_imp) - 64
IF m.nro_elem < 0 OR m.nro_elem > 26
	retorno = .F.
	IF m.msg
		= mens_avi(sistema, 'TIPO DE IMPONIBLE NO VALIDO')
	ENDIF
ELSE
	IF EMPTY(tab_imp[m.nro_elem])
		retorno = .F.
		IF m.msg
			= mens_avi(sistema, 'TIPO DE IMP.SIN TABLA MAESTRA')
		ENDIF
	ELSE
		IF EMPTY(ident_imp[m.nro_elem])
			retorno = .F.
			= mens_avi(sistema, 'TIPO DE IMPON. SIN CAMPO CLAVE')
		ENDIF
	ENDIF
ENDIF

RETURN retorno

