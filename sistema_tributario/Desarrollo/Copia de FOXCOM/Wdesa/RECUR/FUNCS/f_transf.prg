*************************************************
FUNCTION F_TRANSF
*************************************************
* Funcionamiento: Formatea la salida del TRANSFORM() seg�un la cant. de
* 								decimales deseados.
* 
* Par�metros:
* 		CANT_DECIM (N): la cant. de decimales a setear
* 		EXP_FOR		(C/N): la expresi�n caracter o num�rica a formatear
* 		EXP_CAR    (C): los c�digos PICTURE o FUNCTION para formatear
************************************************* 
PARAMETERS cant_decim, exp_for, exp_car
PRIVATE retorno, ant_decim
*retorno = .T.

ant_decim = SET('DECIMALS')
SET DECIMALS TO cant_decim
retorno = TRANSFORM(exp_for, exp_car)
SET DECIMALS TO ant_decim

RETURN retorno

