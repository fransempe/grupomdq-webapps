*- [CONVERTER] Instrucci�n Parameter generada por Converter
*- [CONVERTER] Pasar estos valores al nuevo formulario
PARAMETERS tipcodbaj, fecbaj, codbaj

*!*	LOCAL _aParm, _cparmstr, _nctr
*!*	DIMENSION _aParm[3]
*!*	_aParm[1] = [tipcodbaj]
*!*	_aParm[2] = [fecbaj]
*!*	_aParm[3] = [codbaj]

*!*	_cparmstr = []

*!*	IF PARAMETERS() > 0
*!*		_cparmstr = [WITH ]
*!*		_cparmstr = _cparmstr + _aParm[1]
*!*		FOR m._nctr = 2 TO PARAMETERS()
*!*			_cparmstr = _cparmstr + [,] + _aParm[m._nctr]
*!*		NEXT
*!*	ENDIF

*- [CONVERTER] _rval almacenar� el valor devuelto
LOCAL _rval

*!*	EXTERNAL PROC FBAJLOG.SCX

&&
*!*	PARAMETERS m.tipcodbaj, m.fecbaj, m.codbaj

=VerNuPar ('FBAJLOG', PARAMETERS(), 3 )
=VerTiPar ('FBAJLOG','TIPCODBAJ'  ,'C')
=VerTiPar ('FBAJLOG','FECBAJ'     ,'D')
=VerTiPar ('FBAJLOG','CODBAJ'     ,'C')

PRIVATE retorno
PRIVATE m.cod_baj, m.desc_baj, m.fec_baj, codif, m.motivo, m.tipo_cod PRIVATE m.grup_mem

* init. valores de pantalla
m.fec_baj  = {//}
m.cod_baj  = SPACE(6)
m.desc_baj = ''

m.grup_mem = ''

* init. valores de parametros
m.fecbaj = {}
m.codbaj = SPACE(6)

retorno = .T.

IF retorno
	=USET ('recur\codifics')
	=USET ('recur\tips_cod')
	IF !SEEK( m.tipcodbaj , 'tips_cod' )
		=Msg ("EL tipo de C�digo " + m.tipcodbaj + " Pasado como Par�metro a la Funci�n FBajLog es err�neo " )
		retorno = .F.
	ENDIF
ENDIF

IF !retorno
	RETURN .F.
ENDIF

PUSH KEY

IF !USED('fbajlog')
	USE ('fbajlog') AGAIN ALIAS ('fbajlog') ORDER 1 IN 0
ENDIF

IF EMPTY (m.fecbaj)
	m.fec_baj = DATE ()
ELSE
	m.fec_baj = m.fecbaj
ENDIF

m.tipo_cod = m.tipcodbaj

* Debo redefinir F3, porque al presionar el bot�n borra o F6,
* F3 queda desactivada
ON KEY LABEL F3 DO val_gral  WITH (UPPER (VARREAD()))
ON KEY LABEL F5 *
ON KEY LABEL F6 *

&&

* DO FORM "FBAJLOG.SCX" NAME _S5K0V8Y55 && LINKED &_cparmstr TO m._rval  ### C/S
DO FORM "FBAJLOG.SCX" NAME _S5K0V8Y55 TO m._rval

RETURN m._rval

*- [CONVERTER] Principio de CLEANUP y otros procedimientos de formulario de la versi�n 2.x


FUNCTION v_cod_baj
*************************************************
* 
* Funcionamiento:
* Valida el motivo de anulaci�n.
* 
PRIVATE retorno, m.desc_cod
retorno = .T.
m.desc_cod = ''
m.codif = m.cod_baj
retorno =IN_TABLA ('codifics', 'Motivos de Anulaci�n', 'codif:H="MOTIVO",desc_cod:H="DESCRIPCION"')
IF retorno
	m.cod_baj  = m.codif
	m.desc_baj = m.desc_cod
	SHOW GETS
ENDIF
RETURN retorno


*************************************************
FUNCTION b_aceptar
*************************************************
* 
* Funcionamiento:
* Se ejecuta cuando se presiona el bot�n aceptar.
* Primero hace una validaci�n general de todos
* los campos, y luego, asigna a fec_anul y
* mot_anul (que se pasaron por referencia), los
* valores que se cargaron en los campos fecha y
* motivo.
* 
PRIVATE retorno
retorno = .F.
IF val_grab ('D')
	m.fecbaj = m.fec_baj
	m.codbaj = m.cod_baj
	retorno = .T.	
	CLEAR READ
ENDIF
RETURN retorno


*************************************************
FUNCTION b_ignorar
*************************************************
* 
* Funcionamiento:
* Sale de la funci�n, retornando .F.
* 
retorno = .F.
CLEAR READ
RETURN retorno


*************************************************
FUNCTION v_fec_baj
*************************************************
* 
PRIVATE retorno
retorno = .T.
IF ( m.fec_baj > DATE() )
	=mens_Avi( m.sistema , "FECHA MENOR O IGUAL QUE HOY" )
	retorno = .F.
ENDIF
RETURN retorno



*- [CONVERTER] Fin de CLEANUP y otros procedimientos del formulario de la versi�n 2.x
