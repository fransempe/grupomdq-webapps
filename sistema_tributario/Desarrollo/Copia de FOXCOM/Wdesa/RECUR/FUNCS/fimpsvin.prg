*************************************************
* FUNCTION FImpSVin
*************************************************
* Autor  : Gabriel Zubieta
* Dise�o : Pachu 
* Fecha  : 23/02/95
* 
* Funcionamiento:
* Verifica que no haya imponibles vinculados para imponibles.
* 
* 
* Par�metros:
*	m.tipimp  -C- Tipo de Imponible 
* 	m.nro     -C- Numero de Imponible
* 	m.salemsg -L- Flag q' indica si sale mensaje
*
* Retorno
*   True si no el imponible no tiene vinulaciones y False en caso contrario
*
* Modificaciones:
*
PARAMETERS m.tipimp, m.nro, m.salemsg 
=VerNuPar ('FIMPSVIN', PARAMETERS(), 3 )
=VerTiPar ('FIMPSVIN','M.TIPIMP'   ,'C')
=VerTiPar ('FIMPSVIN','M.NRO'      ,'N')
=VerTiPar ('FIMPSVIN','M.SALEMSG'  ,'L')

PRIVATE retorno, m.tipo_imp, m.nro_imp, m.tipo_impv, m.nro_impv
PRIVATE exp_vin_p, exp_vin_i
retorno = .T.

ord_ant = ORDER('vinc_imp')
rec_ant = RECNO('vinc_imp')

exp_vin_p = GETKEY( 'vinc_imp' , .T. , 2 , 'primario' )
exp_vin_i = GETKEY( 'vinc_imp' , .T. , 2 , 'imp_vinc' )

m.tipo_imp = m.tipimp
m.nro_imp  = m.nro

=SEEK( EVAL(exp_vin_p) , 'vinc_imp' )
DO WHILE ( !EOF('vinc_imp')               ) AND ;
				 ( m.tipo_imp = vinc_imp.tipo_imp ) AND ;
				 ( m.nro_imp  = vinc_imp.nro_imp  ) AND ;
				 ( retorno )
	IF EMPTY( vinc_imp.fec_baja )
		IF m.salemsg
			=mens_avi( m.sistema , "IMPONIBLE CON VINCULAC." )
		ENDIF		
		retorno = .F.
	ELSE
		SKIP IN vinc_imp
	ENDIF
ENDDO

IF retorno
	SET ORDER TO imp_vinc IN vinc_imp
	m.tipo_impv = m.tipimp
	m.nro_impv  = m.nro
	=SEEK( EVAL(exp_vin_i) , 'vinc_imp' )
	DO WHILE ( !EOF('vinc_imp')                 ) AND ;
					 ( m.tipo_impv = vinc_imp.tipo_impv ) AND ;
					 ( m.nro_impv  = vinc_imp.nro_impv  ) AND ;
					 ( retorno  )
		IF EMPTY( vinc_imp.fec_baja )
			IF m.salemsg
				= Msg ("El " + IIF (SEEK (m.tipimp, 'tips_imp'), tips_imp.desc_timp, '') + " " + ALLT(STR(m.nro)) + " es un imponible vinculado del " + IIF (SEEK (vinc_imp.tipo_imp, 'tips_imp'), tips_imp.desc_timp, '') + " " + ALLT(STR(vinc_imp.nro_imp)))
			ENDIF
			retorno = .F.
		ELSE
			SKIP IN vinc_imp
		ENDIF				 
	ENDDO
ENDIF

=ir_reg( rec_ant , 'vinc_imp' )
SET ORDER TO ord_ant IN vinc_imp

RETURN retorno
*

