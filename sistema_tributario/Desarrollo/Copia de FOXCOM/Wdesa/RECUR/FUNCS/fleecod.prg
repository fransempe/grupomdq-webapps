*************************************************
FUNCTION FLeeCod
*************************************************
* Autor : RODRIGO
* Dise�o:	LUIS
PARAMETERS tip_cod, cod, msg
PRIVATE retorno , m.tipo_cod , m.codif
retorno = .T.
=VerNuPar ('FLEECOD', PARAMETERS(), 3 )
=VerTiPar ('FLEECOD','TIP_COD'	  ,'C')
=VerTiPar ('FLEECOD','COD'        ,'C')
=VerTiPar ('FLEECOD','MSG'        ,'L')

IF !(codifics.tipo_cod = tip_cod AND codifics.codif = cod)
	m.tipo_cod 	= tip_cod
	m.codif			= cod
	IF !SEEK(EVAL(exp_cod),'CODIFICS')
		IF msg
			=MENS_AVI('RECUR','CODIGO GENERAL INEXISTENTE')
		ENDIF
		retorno = .F.
	ENDIF
ENDIF

RETURN retorno
