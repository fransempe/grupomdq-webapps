*************************************************
* FUNCTION FSemafor
*************************************************
* 
* Funcionamiento:
* Intenta abrir en forma exclusiva la tabla SEMAFORO.
* Se utiliza en varios programas no actualicen a la vez.
* 
PARAMETERS obs

************************************************************************
*	
*	SQLMODI_01
*		Usuario							: Todos
*		Fecha								: 06/09/2001
*		Quien modific�			: Carlos
*		Detalle problema		: Cuando abr�a la tabla semaforo se pinchaba.
*													
*		Detalle correcci�n	: El error ocurr�a cuando estaba seteada la
*													variable dbase_recur. Intentaba abrir
*													semaforo.dbf con order primario, y esta tabla
*													no tiene �ndice.
*													Se quit� el ORDER primario.
*													
*		Programas tocados		: FSemafor.prg
*													
************************************************************************

PRIVATE retorno, alias_ant, onerror, salir, relojito, posicion
retorno = .T.

IF Type ('m.obs') != 'C'
	m.obs = 'Sin definir'
ENDIF

m.alias_ant = Alias ()

DIMENSION relojito[8]
m.posicion = 1
relojito[1] = '|'
relojito[2] = '/'
relojito[3] = '-'
relojito[4] = '\'
relojito[5] = '|'
relojito[6] = '/'
relojito[7] = '-'
relojito[8] = '\'

IF !Used ('semaforo')
	IF !Used ('semaforn')
		IF TYPE ('_DB_RECUR') = 'C' AND !EMPTY (_DB_RECUR)	
			USE &_DB_RECUR.recur\semaforn IN 0
		ELSE
			USE recur\semaforn IN 0
		ENDIF
	ENDIF

	m.onerror = On ('ERROR')

	m.salir = .F.
	DO WHILE !m.salir
		m.retorno = .T.

		ON ERROR retorno = .F.

		IF TYPE ('_DB_RECUR') = 'C' AND !EMPTY (_DB_RECUR)	


			* Comienzo SQLMODI_01

			* USE &_DB_RECUR.recur\semaforo IN 0 ORDER primario EXCL
			USE &_DB_RECUR.recur\semaforo IN 0 EXCL

			* Fin SQLMODI_01


		ELSE
			USE recur\semaforo IN 0 EXCL
		ENDIF

		ON ERROR &onerror

		IF retorno
			SELECT semaforn
			GO TOP IN semaforn
			IF Eof ('semaforn')
				APPEND BLANK
			ENDIF
			REPLACE nomb_pers WITH m._usuario ;
							obs				WITH m.obs
			m.salir = .T.
		ELSE
			GO TOP IN semaforn
			WAIT CLEAR
			WAIT WIND 'Actualizaci�n en proceso. Aguarde por favor ' + relojito[m.posicion] + Chr (13) + ;
								'Usuario: ' + AllTrim (semaforn.nomb_pers) + Chr (13) + ;
								'Acci�n : ' + AllTrim (semaforn.obs) TIMEOUT 2
			m.posicion = m.posicion + 1
			IF m.posicion >= 9
				m.posicion = 1
			ENDIF
		ENDIF
	ENDDO
ENDIF

WAIT CLEAR

IF !Empty (m.alias_ant)
	SELECT (m.alias_ant)
ENDIF

RETURN retorno
