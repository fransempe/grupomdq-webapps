*************************************************
* FCBI25
*************************************************
* Autor : Gabriel Zubieta
* Dise�o: Gabriel Zubieta
* 
* Fecha : 28/03/95
* 
* Funcionamiento:
* Dado un digito calcula el strings que sera enviado a la impresora y
* dibujara el codigo de barra   
*
* Aclaracion : en el programa que use esta funcion hay que dimensionar
* VCodBar[100] y luego llamar a la funcion FcvI25 que carga este vector
* el cual se esta usando en esta fcion.  
*
*
* Par�metros:
* m.digito -C- digito para calcular
* 
* Modificaciones:
* 
PARAMETERS m.digito
EXTERNAL ARRAY VCodBar

IF SET('DEBUG') = 'ON'
	=VerNuPar( 'FCBI25', PARAMETERS(), 1 )
	=VerTiPar( 'FCBI25','digito'     ,'C')
	IF LEN(m.digito) != 13
		=msg('El Largo del n�mero para calcular d�gito debe ser 13')
		RETURN ''
	ENDIF
ENDIF

PRIVATE cb, m.dig, m.par, m.cPares, m.cb
m.dig = fdigv10( m.digito , "3131313131313" )
m.digito = m.digito + STR(m.dig,1)
m.cb = CHR(255) + CHR(255) +;
       CHR(0)   + CHR(0)   + CHR(0) +;
       CHR(255) + CHR(255) +;
       CHR(0)   + CHR(0)   + CHR(0)
FOR m.cPares = 1 TO 7
	m.par = SUBSTR( m.digito , m.cPares*2-1 , 2 )
	m.cb = m.cb + VCodBar( VAL(m.par) + 1 )
ENDFOR
m.cb = m.cb +;
			 CHR(255) + CHR(255) + CHR(255) + CHR(255) + CHR(255) + CHR(255) +;
			 CHR(0)   + CHR(0)   + CHR(0) +;
    	 CHR(255) + CHR(255) +;
		   CHR(0)  
RETURN m.cb
*

