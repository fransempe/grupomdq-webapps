*************************************************
FUNCTION ir_reg
*************************************************
* Funcionamiento
*  Devuelve .F. si no pudo posicionar el nro. de registro indicado.
*	 Devuelve .T. si lo pudo posicionar . En este caso si se le pasa
*  un n�m de reg v�lido deja el puntero en este reg, si en cambio no
*	 se pasa un reg o el reg pasado es inv�lido el puntero queda en el 
*	 reg en que se encontraba.
*
* Par�metros
*	[reg]         : (N) (ninguno, queda en el actual) n�mero de registro  
*						      		en que se quiere posicionar el puntero.
* [tab_a_verif] : (C) (tabla activa) Nombre de la tabla de la que se quiere 
* 										saber si est� vac�a.
*************************************************

	PARAMETERS reg, tab_a_verif
	PRIVATE ant_reg

	IF TYPE('tab_a_verif') <> 'C'
		tab_a_verif = ALIAS()
	ENDIF
	
	IF USED(tab_a_verif)
		IF TYPE('reg') <> 'N'	
			reg = RECNO(tab_a_verif)
		ENDIF		
		IF EOF(tab_a_verif)
			ant_reg = 0
		ELSE
			ant_reg = RECNO(tab_a_verif)
		ENDIF		
		IF RECCOUNT(tab_a_verif) <> 0
			GO TOP IN (tab_a_verif)
			IF EOF(tab_a_verif)
				RETURN .F.
			ELSE
				IF !(reg > RECCOUNT(tab_a_verif) OR reg <= 0)				
					GO reg IN (tab_a_verif)
					IF DELETED(tab_a_verif)
						IF ant_reg <> 0
							GO ant_reg IN (tab_a_verif)
						ELSE
							GO BOTT IN (tab_a_verif)
						ENDIF
						RETURN .F.
					ELSE	
						RETURN .T.
					ENDIF
				ELSE
					IF ant_reg <> 0
						GO ant_reg IN (tab_a_verif)
					ELSE
						GO BOTT IN (tab_a_verif)
					ENDIF
					RETURN .F.
				ENDIF
			ENDIF
		ELSE
			RETURN .F.
		ENDIF
	ELSE
		WAIT WINDOW NOWAIT 'Alias '+tab_a_verif+ ', no existe'
		RETURN .F.
	ENDIF	
	