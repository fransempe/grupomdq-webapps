*************************************************
FUNCTION Mens_Avi
*************************************************
* Funcionamiento:
* Se le pasan como par�metros el nombre del sistema del programa
* que la llam�, y el c�d. del aviso. Seg�n la gravedad del aviso
* muestra un mensaje sencillo � con detalles.
* 
* Par�metros:
* 	m_sis  = nombre del sistema.
* 	m_avis = c�digo del aviso.
* 
PARAMETERS m_sis, m_avis
PRIVATE retorno, m_alias, m_reg_ant, m_mensa, w_descr, w_dsc, m.reto

PUSH KEY
=DEAC_TEC()

KEYBOARD '{shift+f12}' CLEAR
WAIT WIND ''

retorno   = .T.
m_alias   = ALIAS()
m_reg_ant = RECNO()
w_descr   = ''
w_dsc     = ''
m_sis     = PADR (m_sis, 8, ' ')

=uset('asa\mens_avi')
select mens_avi
IF SEEK(m_sis + m_avis)
	w_descr = del_chr( mens_avi.desc_avis )
	IF mens_avi.grav_avis < 5
		=MSG (&w_descr)		
	ELSE			
		m_mensa = ' Subsistema : '+ ALLTRIM(m_sis) + CHR(13) + CHR(13) + '  Aviso : '+ ;
		ALLTRIM( mens_avi.cod_avis ) + CHR(13) + CHR(13) + &w_descr
		=Vent_Exp(m_mensa)		
	ENDIF
	m.reto = .T.
	
ELSE
	=MSG ('No se encuentra Mensaje de Aviso ' + CHR(13) + m_avis + '.' +;
			  CHR(13) + 'Informe a SICO.')
	m.reto = .F.
ENDIF

=msale()
	
KEYBOARD '{shift+f12}' CLEAR
WAIT WIND ''
POP KEY
	
RETURN m.reto
*


*************************************************
FUNCTION msale
*************************************************
* Funcionamiento:
* Cierra las tablas que se abrieron dentro de la funci�n y 
* restaura, si hay, la tabla en uso antes del llamado.
* 
* 
* Par�metros:
* 
*	no tiene. 
* 
	USE IN mens_avi
	IF !EMPTY(m_alias)
		SELECT &m_alias
		=tab_vac(m_reg_ant)
	ENDIF


*************************************************
FUNCTION Vent_Exp
*************************************************
* Funcionamiento:
* Se llama para los c�d. de aviso mayores que 5. Si elige opci�n
* Explicaci�n se mostrar� una explicaci�n del aviso. Si elige la 
* opcion Indicaciones se mostrar� la/s posibles soluciones.
* 
* Par�metros:
* 
* 
* 
PARAMETERS m.mensaje, nowait

	PRIVATE m.mensaje, largomen, m.pos, m.pos1, largomemo, m.altura
	IF TYPE('m.mensaje') <> "C"
		m.mensaje = "Presione ENTER para continuar"
	ENDIF

	IF TYPE ('nowait') <> 'L'
		nowait = .T.
	ENDIF	

	largomemo = SET("MEMOWIDTH")
	m.altura = 1
	m.mensaje = ALLTRIM(m.mensaje)
	largomen = LEN(m.mensaje)
	IF largomen < 25
		largomen = 30
	ELSE
		IF largomen > 40
			SET MEMOWIDTH TO 40
			m.altura = MEMLINES(m.mensaje)
			largomen = 46
		ELSE
			largomen = largomen+6
		ENDIF
	ENDIF

	DEFINE WINDOW M_VENTMEN ;
		FROM	INT ( (SROW()-(6+m.altura)) / 2 ),;
					INT ( (SCOL()-largomen) / 2 );
		TO	INT ( (SROW()-(6+m.altura)) / 2 ) + (5+m.altura),;
				INT ( (SCOL()-largomen) / 2 ) +largomen-1+3 ;
		FLOAT ;
		NOCLOSE ;
		SHADOW ;
		DOUBLE ;
		COLOR SCHEME 7

	ACTIVATE WINDOW M_VENTMEN NOSHOW

	FOR i_msg=1 TO m.altura
		m.pos = (largomen - 2 - LEN (MLINE (m.mensaje, i_msg)) ) / 2
		@ i_msg, m.pos SAY MLINE (m.mensaje, i_msg)
	ENDFOR 

*	m.pos1 = (largomen - 2 - 43) / 2
	IF ! nowait
		DO CASE	
			CASE !EMPTY(ALLTRIM (mens_avi.expl_avis)) AND !EMPTY(ALLTRIM (mens_avi.sol_avis))
				m.pos1 = 1
				@ m.altura+2, m.pos1 GET m.confirm ;
					PICTURE "@*HN \!\?\<Acepta; \<Explicaci�n ; \<Indicaciones " ;
					SIZE 1, 12, 1 ;
					DEFAULT 1 ;
					VALID msg_conf_valid() ;
					MESSAGE "Acepta  Continuar, Explicaci�n  M�s informaci�n, Indicaciones  Soluciones"

			CASE EMPTY(ALLTRIM (mens_avi.expl_avis)) AND !EMPTY(ALLTRIM (mens_avi.sol_avis))
				m.pos1 = 9
				@ m.altura+2, m.pos1 GET m.confirm ;
					PICTURE "@*HN \!\?\<Acepta; \<Indicaciones " ;
					SIZE 1, 12, 1 ;
					DEFAULT 1 ;
					VALID msg_conf_valid() ;
					MESSAGE "Acepta  Continuar, Indicaciones  Soluciones"

			CASE !EMPTY(ALLTRIM (mens_avi.expl_avis)) AND EMPTY(ALLTRIM (mens_avi.sol_avis))
				m.pos1 = 9
				@ m.altura+2, m.pos1 GET m.confirm ;
					PICTURE "@*HN \!\?\<Acepta; \<Explicaci�n " ;
					SIZE 1, 12, 1 ;
					DEFAULT 1 ;
					VALID msg_conf_valid() ;
					MESSAGE "Acepta  Continuar, Explicaci�n  M�s informaci�n"

			CASE EMPTY(ALLTRIM (mens_avi.expl_avis)) AND EMPTY(ALLTRIM (mens_avi.sol_avis))
				m.pos1 = (largomen - 7) / 2
				@ m.altura+2, m.pos1 GET m.confirm ;
					PICTURE "@*HN \!\?\<Acepta" ;
					SIZE 1, 8, 1 ;
					DEFAULT 1 ;
					VALID msg_conf_valid() ;
					MESSAGE "Acepta  Continuar"

		ENDCASE
		

		ACTIVATE WINDOW M_VENTMEN
*		READ CYCLE MODAL TIMEOUT 15
*		READ CYCLE 
*   comente linea anterior por MODAL
		READ CYCLE MODAL
		
		RELEASE WINDOW M_VENTMEN

	ENDIF


	SET MEMOWIDTH TO largomemo
	
	IF EMPTY ( WONTOP())
		@ 24,0 CLEAR
	ENDIF

	KEYBOARD '{shift+f12}' CLEAR
	WAIT WIND ''

*********************************************
FUNCTION msg_conf_valid   &&  m.confirm VALID
************************
	DO CASE
		CASE m.confirm = 1
			CLEAR READ
			
		CASE m.confirm = 2
			IF EMPTY(ALLTRIM (mens_avi.expl_avis))
				IF !EMPTY(ALLTRIM (mens_avi.sol_avis))
					=MSG (ALLTRIM (mens_avi.sol_avis))
				ENDIF
			ELSE	
				=MSG (ALLTRIM (mens_avi.expl_avis))
			ENDIF	
			RETURN .F.
		CASE m.confirm = 3
			IF !EMPTY(ALLTRIM (mens_avi.sol_avis))
				=MSG (ALLTRIM (mens_avi.sol_avis))
			ENDIF	
			RETURN .F.
	ENDCASE
*

