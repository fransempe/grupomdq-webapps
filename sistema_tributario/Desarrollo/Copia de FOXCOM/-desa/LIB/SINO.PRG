**************************************************
FUNCTION SINO
**************************************************
* Funcionamiento:
* 	Muestra un mensaje dentro de una ventana y pide una 
* confirmaci�n para continuar. 
*		Se usa para pedir confirmaciones.(ej.: BORRA ? )
*
*		Devuelve .T. si se selecciona < Si >
*		Devuelve .F. si se selecciona < No >
*
* Par�metros
*  [mensaje] : (C) ('Su elecci�n . . .?') Mensaje que se 
*							 quiere mostrar.
**************************************************



* SINO seleccion de opciones SI / NO

PARAMETERS m.mensaje
PUSH KEY
=DEAC_TEC()
PRIVATE m.mensaje, largomen, m.pos, m.pos1, largomemo, m.altura, m_mline
PRIVATE i

IF LAST () = 27
	KEYBOARD '{shift+f12}' CLEAR
	WAIT WIND ''
ENDIF

IF TYPE('m.mensaje') = "L"
	m.mensaje = "Su elecci�n . . . ?"
ENDIF

largomemo = SET("MEMOWIDTH")
m.altura = 1
m.mensaje = ALLTRIM(m.mensaje)
largomen = LEN(m.mensaje)

IF largomen < 25
	largomen = 30
ELSE
	IF largomen > 40
		SET MEMOWIDTH TO 40
		m.altura = MEMLINES(m.mensaje)				
		largomen = 46
	ELSE
		largomen = largomen+6
	ENDIF
ENDIF

IF NOT WEXIST("_qdv0ttc1u")
	DEFINE WINDOW _qdv0ttc1u ;
		FROM INT((SROW()-(6+m.altura))/2),INT((SCOL()-largomen)/2) ;
		TO INT((SROW()-(6+m.altura))/2)+(5+m.altura),INT((SCOL()-largomen)/2)+largomen-1 ;
		FLOAT ;
		NOCLOSE ;
		SHADOW ;
		DOUBLE ;
		COLOR SCHEME 7
ENDIF

#REGION 1

IF WVISIBLE("_qdv0ttc1u")
	ACTIVATE WINDOW _qdv0ttc1u SAME
ELSE
	ACTIVATE WINDOW _qdv0ttc1u NOSHOW
ENDIF

m_mline = _mline
FOR i=1 TO m.altura
	m.pos = (largomen-2-LEN(MLINE(m.mensaje,i)))/2
	@ i,m.pos SAY MLINE(m.mensaje,i)
ENDFOR
_mline = m_mline

m.pos1 = (largomen-2-19)/2
@ m.altura+2,m.pos1 GET m.sino ;
	PICTURE "@*HN \!\<Si;\?\<No" ;
	SIZE 1,8,3 ;
	DEFAULT 1 ;
	VALID _qdv0tterb() ;
	MESSAGE "Si: confirma      -      No: anula"

IF NOT WVISIBLE("_qdv0ttc1u")
	ACTIVATE WINDOW _qdv0ttc1u
ENDIF

READ CYCLE MODAL
RELEASE WINDOW _qdv0ttc1u

SET MEMOWIDTH TO largomemo

if wontop() == ''
	@ 24,0 CLEAR
endif

KEYBOARD '{shift+f12}' CLEAR
WAIT WIND ''
POP KEY
IF m.sino = 1
	RETURN .T.
ELSE
	RETURN .F.
ENDIF
	
FUNCTION _qdv0tterb     &&  m.sino VALID
	#REGION 1
	CLEAR READ
	
*
