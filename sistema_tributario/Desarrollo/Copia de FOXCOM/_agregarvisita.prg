PROCEDURE _agregarVisita(p_sFecha as String, p_sHora as String, p_sTipoImponible as String, p_nNroImponible as Number) as String
	PRIVATE sResultado as String

	IF (!USED('web_vis')) THEN
		=UseT('recur\web_vis')
	ENDIF
	SELECT web_vis
	SCATTER MEMVAR BLANK
	
	
	APPEND BLANK	
	REPLACE FECHA	 	WITH CTOD(p_sFecha)
	REPLACE HORA	 	WITH p_sHora
	REPLACE TIPO_IMP 	WITH p_sTipoImponible			
	REPLACE NRO_IMP 	WITH p_nNroImponible			
	REPLACE IMPRIMIO 	WITH 'N'			

	sResultado = 'OK'	
	RETURN sResultado
ENDPROC