PROCEDURE CargarCompAVenc
	PRIVATE AuxTodoOK, AuxNroVenc, AuxCompOK
	PRIVATE strCondicionEspecial as String
	PRIVATE strRecursoOcultar as String

	AuxNroVenc = 0
	AuxTodoOK = .T.
	strCondicionEspecial = ''
	strRecursoOcultar = ''
	
	
	* Busco si existe un recurso a ocultar
	strRecursoOcultar = _getRecursosOcultar()
	
	

	SELECT comprob
	SET ORDER TO imponib IN comprob
	SET KEY TO P_TipoImponible + STR(P_NroImponible,10,0) IN comprob
	GO TOP IN comprob
	DO WHILE NOT EOF('comprob')
		AuxCompOK = .T.	
		strCondicionEspecial = ''
		
		* Omitir ciertos tipos de comprobantes
		IF comprob.tipo_comp # 'DE' AND comprob.tipo_comp # 'PP' AND comprob.tipo_comp # 'ND'
			SKIP IN comprob
			LOOP
		ENDIF
		
		
		* Surgi� un caso de una ND no vencida que ten�a 2 renglones (movimientos) y sobre uno de los movimientos
        * hab�an hecho un plan de pago por lo que el movimiento qued� EN PLAN.
        * Como la ND no estaba vencida se cargaba en la Web y se pod�a imprimir y se lleg� a pagar un movimiento
        * EN PLAN.
        * Asi que ahora si alguno de los movimientos del comprob. est� en plan directamente no se carga el
        * comprobante en pantalla.
		* Tampoco se carga si alguno de los movimientos ya est� cancelado.
        IF CompTieneMovEnPlanOCanc() THEN
          SKIP IN comprob
          LOOP
        ENDIF
        

		* Si el comprobante es DE PLAN y el plan de pago est� dado de baja, el comprobante no debe mostrarse.
        IF CompDePlanDadoDeBaja() THEN
          SKIP IN comprob
          LOOP
        ENDIF
        
        
        * Esto es para evitar que salgan comprobantes con valores menores o igual a cero
        IF comprob.tot_comp <= 0 THEN
			SKIP IN comprob
        	LOOP
        ENDIF
        
        
        * Si esta seteado que se tiene que ocultar los comprobantes que contengan alg�n recurso utilizo esta funci�n
        IF (!EMPTY(strRecursoOcultar)) THEN
			IF (excluirComprobante()) THEN
	        	SKIP IN comprob
	        	LOOP
	        ENDIF
        ENDIF
        
		

		IF (comprob.est_comp = 'NO') THEN
				
			* Si el sistema esta configurado poder imprimir solo comprobantes para los cuales se haya presentado previamente la
			* declaraci�n jurada, busco en los renglones del comprobante si existe un renglon que corresponda al recurso 
			* "Seguridad e higiene", si existe esta todo OK, pero en el caso de que no exista asigno a la variable "strCondicionEspecial"
			* la leyenda "Falta DDJJ" para despu�s desde .NET poder indentificar que este comprobante no puede ser impreso			
			IF (P_TipoImponible = 'C') AND (boolNoImprimirSinDDJJ) THEN			
 				SET KEY TO STR(comprob.grupo_comp,2,0) + STR(comprob.nro_comp,9,0) IN comp_ren
				GO TOP IN comp_ren
				DO WHILE NOT EOF('comp_ren') AND AuxCompOK
				
					IF (UPPER(ALLTRIM(comp_ren.Recurso)) = UPPER(ALLTRIM(strCodigoRecursoSH))) THEN
						IF (comp_ren.anio > 0) AND NOT SEEK(STR(P_NroImponible,10,0) + STR(comp_ren.anio,4,0) + STR(comp_ren.cuota,3,0),'com_ddjj')
							AuxCompOK = .F.
							strCondicionEspecial = 'Falta DDJJ'
						ENDIF					
					ENDIF 
					SKIP IN comp_ren
				ENDDO
				SET KEY TO '' IN comp_ren
			ENDIF
			
			
			AuxNroVenc = 0
			IF comprob.fec1_comp >= DATE()
				AuxNroVenc = 1
			ELSE
				IF comprob.fec2_comp >= DATE()
					AuxNroVenc = 2
				ELSE
					IF comprob.fec3_comp >= DATE()
						AuxNroVenc = 3
					ENDIF
				ENDIF
			ENDIF
			
			

			IF (AuxNroVenc > 0) THEN 
				SELECT CompAVenc
				APPEND BLANK
				REPLACE CompAVenc.grupo_comp	WITH comprob.grupo_comp,			;
						CompAVenc.nro_comp		WITH comprob.nro_comp,				;
						CompAVenc.dv_comp		WITH comprob.dv_comp,				;
						CompAVenc.venc			WITH AuxNroVenc,					;
						CompAVenc.tot_ori		WITH 0								;
						CompAVenc.cond_esp		WITH ALLTRIM(strCondicionEspecial)
					
				
				DO SetDetalleComp
				
				DO CASE
					CASE AuxNroVenc = 1
						REPLACE CompAVenc.fecha_venc	WITH comprob.fec1_comp,						;
								CompAVenc.tot_actint	WITH comprob.tot_act + comprob.tot_int,		;
								CompAVenc.tot_iva		WITH comprob.tot_iva1 + comprob.tot_iva2,	;
								CompAVenc.tot_comp		WITH comprob.tot_comp
					
					CASE AuxNroVenc = 2
						REPLACE CompAVenc.fecha_venc	WITH comprob.fec2_comp,						;
								CompAVenc.tot_actint	WITH comprob.tot2_act + comprob.tot2_int,		;
								CompAVenc.tot_iva		WITH comprob.tot2_iva1 + comprob.tot2_iva2,	;
								CompAVenc.tot_comp		WITH CompAVenc.tot_ori + CompAVenc.tot_actint + CompAVenc.tot_iva
					
					CASE AuxNroVenc = 3
						REPLACE CompAVenc.fecha_venc	WITH comprob.fec3_comp,						;
								CompAVenc.tot_actint	WITH comprob.tot3_act + comprob.tot3_int,		;
								CompAVenc.tot_iva		WITH comprob.tot3_iva1 + comprob.tot3_iva2,	;
								CompAVenc.tot_comp		WITH CompAVenc.tot_ori + CompAVenc.tot_actint + CompAVenc.tot_iva
				ENDCASE
				
				SET KEY TO STR(comprob.grupo_comp,2,0) + STR(comprob.nro_comp,9,0) IN comp_ren
				GO TOP IN comp_ren
				DO WHILE NOT EOF('comp_ren')
					SELECT CompAVencReng
					APPEND BLANK
					REPLACE CompAVencReng.grupo_comp	WITH comp_ren.grupo_comp,	;
							CompAVencReng.nro_comp		WITH comp_ren.nro_comp,		;
							CompAVencReng.reng_comp		WITH comp_ren.reng_comp,	;
							CompAVencReng.recurso		WITH comp_ren.recurso,		;
							CompAVencReng.anio			WITH comp_ren.anio,			;
							CompAVencReng.cuota			WITH comp_ren.cuota,		;
							CompAVencReng.nro_mov		WITH comp_ren.nro_mov
							
					* Si el recurso no est� inclu�do en el cursor de recursos para Referencias, agregarlo.
					* Leer CODIFICS en este momento para obtener la descripci�n del recurso ser�a cr�tico
					*	porque los movimientos de cuenta corriente se leen a partir de CODIFICS. Por eso
					*	es que la obtenci�n de la descripci�n de los recursos se hace al final.
					IF NOT SEEK(comp_ren.recurso,'RefRecursos')
						SELECT RefRecursos
						APPEND BLANK
						Replace RefRecursos.recurso WITH comp_ren.recurso
					ENDIF
					SKIP IN comp_ren
				ENDDO
				SET KEY TO '' IN comp_ren
			ENDIF
		ENDIF
		SKIP IN comprob
	ENDDO
	SET KEY TO '' IN comprob
	RETURN AuxTodoOK
ENDPROC



********************************************************************
FUNCTION CompTieneMovEnPlanOCanc
********************************************************************
PRIVATE retorno
retorno = .F.

SET KEY TO STR(comprob.grupo_comp,2,0) + STR(comprob.nro_comp,9,0) IN comp_ren
GO TOP IN comp_ren
DO WHILE NOT EOF('comp_ren') AND !retorno
  IF comp_ren.debita_cc = 'S' AND comp_ren.nro_mov > 0
    IF SEEK(comp_ren.recurso + comp_ren.tipo_imp + STR(comp_ren.nro_imp,10) + STR(comp_ren.anio,4) +;
          STR(comp_ren.cuota,3) + STR(comp_ren.nro_mov,3), 'rec_cc')
      IF rec_cc.est_mov = 'EP'
        retorno = .T.
      ENDIF
    ELSE
      IF SEEK(comp_ren.recurso + comp_ren.tipo_imp + STR(comp_ren.nro_imp,10) + STR(comp_ren.anio,4) +;
          STR(comp_ren.cuota,3) + STR(comp_ren.nro_mov,3), 'rec_ccc')
        retorno = .T.
      ENDIF
    ENDIF  
  ENDIF
  
  SKIP IN comp_ren
ENDDO

RETURN retorno
ENDFUNC



********************************************************************
FUNCTION CompDePlanDadoDeBaja
********************************************************************
PRIVATE retorno
retorno = .F.

IF comprob.tipo_comp = 'PP' THEN
  SET KEY TO STR(comprob.grupo_comp,2,0) + STR(comprob.nro_comp,9,0) IN comp_ren
  GO TOP IN comp_ren
  DO WHILE NOT EOF('comp_ren') AND !retorno
    IF comp_ren.nro_plan > 0 AND SEEK(comp_ren.tipo_imp + STR(comp_ren.nro_imp,10) +;
          STR(comp_ren.nro_plan,3), 'pla_imp') AND !EMPTY(pla_imp.fec_baja)
      retorno = .T.
    ENDIF

    SKIP IN comp_ren
  ENDDO
ENDIF

RETURN retorno
ENDFUNC



********************************************************************
FUNCTION excluirComprobante
********************************************************************
	PRIVATE retorno
	retorno = .F.

	SET KEY TO STR(comprob.grupo_comp, 2, 0) + STR(comprob.nro_comp, 9, 0) IN comp_ren
	GO TOP IN comp_ren
	DO WHILE NOT EOF('comp_ren') AND !retorno		
	    IF (ALLTRIM(comp_ren.recurso) = ALLTRIM(strRecursoOcultar)) THEN
	        retorno = .T.
	    ENDIF
	  
	  SKIP IN comp_ren
	ENDDO

	RETURN retorno
ENDFUNC




********************************************************************
PROCEDURE SetDetalleComp
********************************************************************
	PRIVATE AuxDetalleComp, AuxCantReng, AuxDescConc
	
	AuxDetalleComp = ''
	AuxCantReng = 0
	AuxDescConc = ''
		
	SET KEY TO STR(comprob.grupo_comp,2,0) + STR(comprob.nro_comp,9,0) IN comp_ren
	GO TOP IN comp_ren
	DO WHILE NOT EOF('comp_ren')
		AuxCantReng = AuxCantReng + 1
		IF AuxCantReng = 1
			AuxDescConc = ''
			IF SEEK(comp_ren.recurso + comp_ren.conc_cc,'concs_cc')
				AuxDescConc = Concs_cc.dsc_ccc
			ENDIF
			AuxDetalleComp = ALLTRIM(comp_ren.recurso) + ' ' + ALLTRIM(STR(comp_ren.cuota,3,0)) + ;
				'/' + ALLTRIM(STR(comp_ren.anio,4,0)) + ' ' + ALLTRIM(AuxDescConc)
		ENDIF
		
		IF AuxCantReng = 2
			AuxDetalleComp = AuxDetalleComp + 'y otros'
		ENDIF
		
		
		REPLACE CompAVenc.tot_ori WITH CompAVenc.tot_ori + ROUND(comp_ren.imp_reng,2)
		*DO CASE
		*	CASE AuxNroVenc = 1
		*		IF (comp_ren.venc_conc = 0) OR (comp_ren.venc_conc = 1)
		*			REPLACE CompAVenc.tot_ori WITH CompAVenc.tot_ori + ROUND(comp_ren.imp_reng,2)
		*		ENDIF
		*	
		*	CASE AuxNroVenc = 2
		*		IF (comp_ren.venc_conc = 0) OR (comp_ren.venc_conc = 2)
		*			REPLACE CompAVenc.tot_ori WITH CompAVenc.tot_ori + ROUND(comp_ren.imp_reng,2)
		*		ENDIF
		*	
		*	CASE AuxNroVenc = 3
		*		IF (comp_ren.venc_conc = 0) OR (comp_ren.venc_conc = 3)
		*			REPLACE CompAVenc.tot_ori WITH CompAVenc.tot_ori + ROUND(comp_ren.imp_reng,2)
		*		ENDIF
		*	
		*ENDCASE
		
		SKIP IN comp_ren
	ENDDO
	REPLACE CompAVenc.detalle_comp WITH AuxDetalleComp
ENDPROC