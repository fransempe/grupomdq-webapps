PROCEDURE GrabarComprobWPago
	PRIVATE cuit1, cuit2

	m.cuit1 = 0
	m.cuit2 = 0
	=FCONTVIN(_TIPOIMP_, _NROIMP_, .F., .F., @m.cuit1, @m.cuit2)
	
	IF m.cuit1 > 0
		m.sit_iva = contrib.sit_iva
	ELSE
		m.sit_iva = ''
	ENDIF
	
	SELECT comprobw
	APPEND BLANK
	REPLACE comprobw.grupo_comp			WITH AuxGrupoCompWeb,					;
			comprobw.nro_comp			WITH AuxNroComp,						;
			comprobw.tipo_imp			WITH _TIPOIMP_,							;
			comprobw.nro_imp				WITH _NROIMP_,							;
			comprobw.dv_comp				WITH AuxDVComp,							;
			comprobw.tipo_comp			WITH 'CP',								;
			comprobw.fecemicomp			WITH DATE(),							;
			comprobw.fec1_comp			WITH AuxFechaVto1,						;
			comprobw.fec2_comp			WITH {},								;
			comprobw.fec3_comp			WITH {},								;
			comprobw.tot_ori				WITH m.tot_ori,							;
			comprobw.tot_act				WITH m.tot_act,							;
			comprobw.tot_int				WITH m.tot_int,							;
			comprobw.tot_iva1			WITH 0,									;
			comprobw.tot_iva2			WITH 0,									;
			comprobw.tot_comp			WITH m.tot_ori + m.tot_act + m.tot_int,	;
			comprobw.tot2_act			WITH 0,									;
			comprobw.tot2_int			WITH 0,									;
			comprobw.tot2_iva1			WITH 0,									;
			comprobw.tot2_iva2			WITH 0,									;
			comprobw.tot3_act			WITH 0,									;
			comprobw.tot3_int			WITH 0,									;
			comprobw.tot3_iva1			WITH 0,									;
			comprobw.tot3_iva2			WITH 0,									;
			comprobw.sit_iva				WITH m.sit_iva,							;
			comprobw.nro_venc			WITH 1,									;
			comprobw.est_comp			WITH 'NO',								;
			comprobw.exp_comp			WITH '',								;
			comprobw.horemicomp			WITH INT(SToT(SECONDS())),				;
			comprobw.cuit				WITH m.cuit1,							;
			comprobw.nro_aud				WITH 0
	
ENDPROC