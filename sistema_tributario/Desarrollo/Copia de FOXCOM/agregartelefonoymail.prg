PROCEDURE AgregarTelefonoYMail(AuxArch as String)
	AuxNroTelRecWeb = ALLTRIM(Get_Para('ASA','NROTELRECWEB'))
	AuxDirMailRecWeb = ALLTRIM(Get_Para('ASA','DIRMAILRECWEB'))

	AuxArch = AuxArch + Indent(1) + '<DATOSCONTACTOCONMUNICIPALIDAD>' + _S_
	AuxArch = AuxArch + Indent(2) + '<NROTELRECLAMOSWEB>' + AuxNroTelRecWeb + '</NROTELRECLAMOSWEB>' + _S_
	AuxArch = AuxArch + Indent(2) + '<DIRMAILRECLAMOSWEB>' + AuxDirMailRecWeb + '</DIRMAILRECLAMOSWEB>' + _S_		
	AuxArch = AuxArch + Indent(1) + '</DATOSCONTACTOCONMUNICIPALIDAD>' + _S_
ENDPROC