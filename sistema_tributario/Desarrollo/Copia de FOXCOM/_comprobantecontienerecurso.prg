***********************************************************************************
*	M�todo _comprobanteContieneRecurso:  
***********************************************************************************
* 	Date:          05/07/2012
* 	Author:        Juan
* 	Design:        Juan
***********************************************************************************
*	Funcionamiento:
*		Filtra los datos de un comprobantes determinado y recorre los renglones
*		buscando el recurso recibido por par�metro.
* 		Si lo encuentra, devuelve 'TRUE', sino 'FALSE'
***********************************************************************************
PROCEDURE _comprobanteContieneRecurso(p_numGrupoComprobante as Number, p_numNumeroComprobante as Number, p_strRecurso as String) as string
	PRIVATE strContieneRecurso as string
	
	
	IF (!USED ('comp_ren')) THEN
		=USET ('recur\comp_ren')
	ENDIF
	SELECT comp_ren
	SET ORDER TO PRIMARIO IN comp_ren
	

	strContieneRecurso = 'FALSE'
	SET KEY TO STR(p_numGrupoComprobante, 2, 0) + STR(p_numNumeroComprobante, 9, 0) IN comp_ren 
	GO TOP IN comp_ren
	DO WHILE NOT EOF('comp_ren') AND strContieneRecurso = 'FALSE'
	
		IF (ALLTRIM(UPPER(comp_ren.Recurso)) = ALLTRIM(UPPER(p_strRecurso))) THEN
			strContieneRecurso = 'TRUE'	
		ENDIF			
		
		SKIP IN comp_ren
	ENDDO
	
	SET KEY TO ''
	RETURN ALLTRIM(strContieneRecurso)
ENDPROC