
PROCEDURE _VerificarParametros(pVerParametros as Boolean) as string

	* Variables ASA
	PRIVATE mNombre as String
	PRIVATE mDireccion as String
	PRIVATE mTelefono as String
	PRIVATE mMail as String
	PRIVATE mWeb as String
	PRIVATE strWebVerCredito as String
	PRIVATE strParaNoImprimirSinDDJJ as String
	PRIVATE strCodigoRecursoSH as String
	
	
	


	* Variables RECUR
	PRIVATE mCantRengCompWeb as Number
	PRIVATE mGrupoCompWeb as Number
	PRIVATE mFechaActuaWeb as String
	PRIVATE mFechaVtoWeb as String
	PRIVATE strRecursoExcluido as String

	
	PRIVATE mParametro as String
	PRIVATE mXML as String



	IF (!USED ('config')) THEN
		=USET ('asa\config')
	ENDIF
	SELECT config
	SET ORDER TO Primario IN config
	

	************************************* Parametros ASA *************************************
	* Parametro Partido
	**********************	
	mParametro = ''	
	mIndex = PADR('ASA', 8, ' ') + 'PARTIDO'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)		
		IF (EMPTY(mParametro)) THEN
			RETURN 'El parámetro PARTIDO no se encuentra definido'				
		ELSE
			mNombre = mParametro 
		ENDIF
	ELSE
		RETURN 'El parámetro PARTIDO no se encuentra en la tabla CONFIG'
	ENDIF
	
	
	
	* Parametro Direccion
	************************
	mParametro = ''	
	mIndex = PADR('ASA', 8, ' ') + 'DOMICILIO'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		IF (EMPTY(mParametro)) THEN		
			RETURN 'El parámetro DOMICILIO no se encuentra definido'						
		ELSE
			mDireccion = mParametro 
		ENDIF
	ELSE
		RETURN 'El parámetro DOMICILIO no se encuentra en la tabla CONFIG'
	ENDIF


	* Parametro Telefono
	***********************	
	mParametro = ''	
	mIndex = PADR('ASA', 8, ' ') + 'NROTELRECWEB'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		IF (EMPTY(mParametro)) THEN
			RETURN 'El parámetro NROTELRECWEB no se encuentra definido'				
		ELSE
			mTelefono = mParametro 
		ENDIF
	ELSE
		RETURN 'El parámetro NROTELRECWEB no se encuentra en la tabla CONFIG'
	ENDIF



	* Parametro Mail
	******************	
	mParametro = ''	
	mIndex = PADR('ASA', 8, ' ') + 'DIRMAILRECWEB'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		IF (EMPTY(mParametro)) THEN
			RETURN 'El parámetro DIRMAILRECWEB no se encuentra definido'				
		ELSE
			mMail = mParametro 
		ENDIF
	ELSE
		RETURN 'El parámetro DIRMAILRECWEB no se encuentra en la tabla CONFIG'
	ENDIF



	* Parametro WEB
	*****************
	mParametro = ''	
	mIndex = PADR('ASA', 8, ' ') + 'WEB'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		IF (EMPTY(mParametro)) THEN
			RETURN 'El parámetro WEB no se encuentra definido'				
		ELSE
			mWeb = mParametro 
		ENDIF
	ELSE
		RETURN 'El parámetro WEB no se encuentra en la tabla CONFIG'
	ENDIF
	
	
	
	* Parametro WEB_VER_CREDITO
	*********************************
	mParametro = ''	
	mIndex = PADR('ASA', 8, ' ') + 'WEB_VER_CREDITO'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		IF (EMPTY(mParametro)) THEN
			RETURN 'El parámetro WEB_VER_CREDITO no se encuentra definido'				
		ELSE
			strWebVerCredito = mParametro 
		ENDIF
	ELSE
		RETURN 'El parámetro WEB_VER_CREDITO no se encuentra en la tabla CONFIG'
	ENDIF
	

	
	* Parametro WEB_NOIMP_SINDJ
	*********************************
	mParametro = ''	
	mIndex = PADR('ASA', 8, ' ') + 'WEB_NOIMP_SINDJ'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		IF (EMPTY(mParametro)) THEN
			RETURN 'El parámetro WEB_NOIMP_SINDJ no se encuentra definido'				
		ELSE
			strParaNoImprimirSinDDJJ = mParametro 
		ENDIF
	ELSE
		RETURN 'El parámetro WEB_NOIMP_SINDJ no se encuentra en la tabla CONFIG'
	ENDIF
	
	
	
	* Parametro WEB_NOIMP_SINDJ
	*********************************
	mParametro = ''	
	mIndex = PADR('ASA', 8, ' ') + 'WEB_CODRECUR_SH'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		IF (EMPTY(mParametro)) THEN
			RETURN 'El parámetro WEB_CODRECUR_SH no se encuentra definido'				
		ELSE
			strCodigoRecursoSH = mParametro 
		ENDIF
	ELSE
		RETURN 'El parámetro WEB_CODRECUR_SH no se encuentra en la tabla CONFIG'
	ENDIF
	********************************************************************************************
	********************************************************************************************







	************************************* Parametros RECUR *************************************
	* Parametro CANTRENGCOMPWEB
	****************************
	mParametro = ''
	mIndex = PADR('RECUR', 8, ' ') + 'CANTRENGCOMPWEB'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)	
			
		IF (EMPTY(mParametro)) OR (TYPE('VAL(mParametro)') # 'N') OR (VAL(mParametro) < 1) THEN		
			RETURN 'El parámetro CANTRENGCOMPWEB no se encuentra definido'						
		ELSE
			mCantRengCompWeb = VAL(mParametro)
		ENDIF
	ELSE
		RETURN 'El parámetro CANTRENGCOMPWEB no se encuentra en la tabla CONFIG'
	ENDIF
	
	
	
	
	* Parametro GRUPO_COMP_WEB
	****************************
	mParametro = ''
	mIndex = PADR('RECUR', 8, ' ') + 'GRUPO_COMP_WEB'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)

		IF (EMPTY(mParametro)) OR (TYPE('VAL(mParametro)') # 'N') OR (VAL(mParametro) < 1) THEN		
			RETURN 'El parámetro GRUPO_COMP_WEB no se encuentra definido'						
		ELSE
			mGrupoCompWeb = VAL(mParametro)
		ENDIF
	ELSE	
		RETURN 'El parámetro GRUPO_COMP_WEB no se encuentra en la tabla CONFIG'	
	ENDIF
	
	


	* Parametro FECHA_ACTUA_WEB
	****************************	
	mParametro = ''
	mIndex = PADR('RECUR', 8, ' ') + 'FECHA_ACTUA_WEB'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		
		IF (EMPTY(mParametro)) OR (TYPE('EVALUATE(mParametro)') # 'D') THEN		 
			RETURN 'El parámetro FECHA_ACTUA_WEB no se encuentra definido'						 
		ELSE
			mFechaActuaWeb = mParametro
		ENDIF
	ELSE
		RETURN 'El parámetro FECHA_ACTUA_WEB no se encuentra en la tabla CONFIG'
	ENDIF



	* Parametro FECHA_VTO_WEB
	**************************
	mParametro = ''
	mIndex = PADR('RECUR', 8, ' ') + 'FECHA_VTO_WEB'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		
		IF (EMPTY(mParametro)) OR (TYPE('EVALUATE(mParametro)') # 'D') THEN		
			RETURN 'El parámetro FECHA_VTO_WEB no se encuentra definido'						
		ELSE
			mFechaVtoWeb = mParametro
		ENDIF
	ELSE
		RETURN 'El parámetro FECHA_VTO_WEB no se encuentra en la tabla CONFIG'
	ENDIF
	********************************************************************************************
	********************************************************************************************



	* Parametro RECUR_EXCLU_WEB
	***************************
	mParametro = ''	
	mIndex = PADR('RECUR', 8, ' ') + 'RECUR_EXCLU_WEB'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		IF (EMPTY(mParametro)) THEN
			RETURN 'El parámetro RECUR_EXCLU_WEB no se encuentra definido'				
		ELSE
			strRecursoExcluido = mParametro 
		ENDIF
	ELSE
		RETURN 'El parámetro RECUR_EXCLU_WEB no se encuentra en la tabla CONFIG'
	ENDIF
	********************************************************************************************
	********************************************************************************************





	************************************* Numerad *************************************
	* Ya sea que se trabaje con base de datos en línea o replicada siempre chequeamos que el
	* numerador exista en numerad. No chequeamos que exista en numeradw porque si la base
	* está replicada sólo basta con chequear en los datos del sistema en línea.

	IF (!USED ('numerad')) THEN
		=USET ('recur\numerad')
	ENDIF
	SELECT numerad
	SET ORDER TO 1

 
	IF NOT (SEEK('COMPR '+ STR(mGrupoCompWeb, 2, 0))) THEN
		RETURN 'El valor ' + ALLTRIM(STR(mGrupoCompWeb, 2, 0)) + ' no corresponde a un numerador válido de comprobantes. No se pueden generar comprobantes de pago para cancelación de deuda.'
	ENDIF
	********************************************************************************************
	********************************************************************************************








	************************************* RESPUESTA *************************************
	* Genero el XML para que salga por pantalla o devuelvo un OK
	IF (pVerParametros) THEN
		mXML = ''
		mXML = mXML + Indent(1) + '<PARAMETROS>' + _S_
		
			mXML = mXML + Indent(1) + '<PARAMETROS_ASA>' + _S_
				mXML = mXML + Indent(2) + '<PARTIDO>' + ALLTRIM(mNombre) + '</PARTIDO>' + _S_
				mXML = mXML + Indent(2) + '<DIRECCION>' + ALLTRIM(mDireccion) + '</DIRECCION>' + _S_
				mXML = mXML + Indent(2) + '<NROTELRECWEB>' + ALLTRIM(mTelefono) + '</NROTELRECWEB>' + _S_
				mXML = mXML + Indent(2) + '<DIRMAILRECWEB>' + ALLTRIM(mMail) + '</DIRMAILRECWEB>' + _S_	
				mXML = mXML + Indent(2) + '<WEB>' + ALLTRIM(mWeb) + '</WEB>' + _S_	
				mXML = mXML + Indent(2) + '<WEB_VER_CREDITO>' + ALLTRIM(strWebVerCredito) + '</WEB_VER_CREDITO>' + _S_					
				mXML = mXML + Indent(2) + '<WEB_NOIMP_SINDJ>' + ALLTRIM(strParaNoImprimirSinDDJJ) + '</WEB_NOIMP_SINDJ>' + _S_	
				mXML = mXML + Indent(2) + '<WEB_CODRECUR_SH>' + ALLTRIM(strCodigoRecursoSH) + '</WEB_CODRECUR_SH>' + _S_	
			mXML = mXML + Indent(1) + '</PARAMETROS_ASA>' + _S_
	
		 
			mXML = mXML + Indent(1) + '<PARAMETROS_RECUR>' + _S_
				mXML = mXML + Indent(2) + '<CANTRENGCOMPWEB>' + ALLTRIM(STR(mCantRengCompWeb)) + '</CANTRENGCOMPWEB>' + _S_
				mXML = mXML + Indent(2) + '<GRUPO_COMP_WEB>' + ALLTRIM(STR(mGrupoCompWeb)) + '</GRUPO_COMP_WEB>' + _S_
				mXML = mXML + Indent(2) + '<FECHA_ACTUA_WEB>' + ALLTRIM(mFechaActuaWeb) + '</FECHA_ACTUA_WEB>' + _S_
				mXML = mXML + Indent(2) + '<FECHA_VTO_WEB>' + ALLTRIM(mFechaVtoWeb) + '</FECHA_VTO_WEB>' + _S_
				mXML = mXML + Indent(2) + '<RECUR_EXCLU_WEB>' + ALLTRIM(strRecursoExcluido) + '</RECUR_EXCLU_WEB>' + _S_				
			mXML = mXML + Indent(1) + '</PARAMETROS_RECUR>' + _S_

	 	
		mXML = mXML + Indent(1) + '</PARAMETROS>' + _S_
	ELSE
		mXML = 'OK'
	ENDIF

	RETURN ALLTRIM(mXML)
ENDPROC