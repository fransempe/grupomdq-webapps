PROCEDURE AgregCompWAArchSalida (AuxArchCompGral as String)
	PRIVATE mDescripcionRecurso as String
	PRIVATE mSeek as String
	
	
	IF NOT USED('codifics')
		=USET('recur\codifics')
	ENDIF
	
	
	
	AuxImpTotOrig1 = 0
	AuxImpTotOrig2 = 0
	AuxImpTotOrig3 = 0
	
	AuxArchCompGral = AuxArchCompGral + Indent(2) + '<COMPROBANTE>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<NROCOMP>' + PADL(ALLT(STR(comprobw.grupo_comp,2,0)),2,'0') + '/' + PADL(ALLT(STR(comprobw.nro_comp,9,0)),9,'0') + '/' + PADL(ALLT(STR(comprobw.dv_comp,2,0)),2,'0') + '</NROCOMP>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<FECHA_EMISION>' + IIF(NOT EMPTY(comprobw.fecemicomp),DTOC(comprobw.fecemicomp),'') + '</FECHA_EMISION>' + _S_
			
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<RENGLONES>' + _S_
	SET KEY TO STR(comprobw.grupo_comp,2,0) + STR(comprobw.nro_comp,9,0) IN comp_rew
	GO TOP IN comp_rew
	DO WHILE NOT EOF('comp_rew')
		AuxDescConc = ''
		IF SEEK(comp_rew.recurso+comp_rew.conc_cc,'concs_cc')
			AuxDescConc = concs_cc.dsc_ccc
		ENDIF
		AuxCodPlan = ''
		IF comp_rew.nro_plan > 0
			IF SEEK(comprobw.tipo_imp+STR(comprobw.nro_imp,10,0)+STR(comp_rew.nro_plan,3,0),'pla_imp')
				AuxCodPlan = pla_imp.cod_plan
			ENDIF
		ENDIF
		DO CASE
			CASE comp_rew.venc_conc = 0
				AuxImpTotOrig1 = AuxImpTotOrig1 + ROUND(comp_rew.imp_reng,2)
				IF NOT EMPTY(comprobw.fec2_comp)
					AuxImpTotOrig2 = AuxImpTotOrig2 + ROUND(comp_rew.imp_reng,2)
				ENDIF
				IF NOT EMPTY(comprobw.fec3_comp)
					AuxImpTotOrig3 = AuxImpTotOrig3 + ROUND(comp_rew.imp_reng,2)
				ENDIF
			CASE comp_rew.venc_conc = 1
				AuxImpTotOrig1 = AuxImpTotOrig1 + ROUND(comp_rew.imp_reng,2)
			CASE comp_rew.venc_conc = 2
				IF NOT EMPTY(comprobw.fec2_comp)
					AuxImpTotOrig2 = AuxImpTotOrig2 + ROUND(comp_rew.imp_reng,2)
				ENDIF
			CASE comp_rew.venc_conc = 3
				IF NOT EMPTY(comprobw.fec3_comp)
					AuxImpTotOrig3 = AuxImpTotOrig3 + ROUND(comp_rew.imp_reng,2)
				ENDIF
		ENDCASE
		IF (comp_rew.venc_conc = 0) OR (comp_rew.venc_conc = 1)
			AuxArchCompGral = AuxArchCompGral + Indent(4) + '<RENGLON>' + _S_
			
		 
 
			* Obtengo la descripcion del recurso
			mDescripcionRecurso = ''
			mSeek = PADR('RECURS', 6) + PADR(ALLTRIM(comp_rew.recurso), 6)
			IF (SEEK(ALLTRIM(mSeek), 'codifics')) THEN
				mDescripcionRecurso = ALLTRIM(codifics.Desc_cod)
			ENDIF
					
			
			AuxArchCompGral = AuxArchCompGral + Indent(5) + '<RECURSO>' + ALLT(comp_rew.recurso) + ' - ' +  ALLTRIM(mDescripcionRecurso) + '</RECURSO>' + _S_					
			AuxArchCompGral = AuxArchCompGral + Indent(5) + '<ANIO>' + ALLT(STR(comp_rew.anio,4,0)) + '</ANIO>' + _S_
			AuxArchCompGral = AuxArchCompGral + Indent(5) + '<CUOTA>' + ALLT(STR(comp_rew.cuota,3,0)) + '</CUOTA>' + _S_
			AuxArchCompGral = AuxArchCompGral + Indent(5) + '<CONCEPTO>' + ALLT(AuxDescConc) + '</CONCEPTO>' + _S_
			AuxArchCompGral = AuxArchCompGral + Indent(5) + '<CODPLAN>' + ALLT(AuxCodPlan) + '</CODPLAN>' + _S_
			AuxArchCompGral = AuxArchCompGral + Indent(5) + '<IMPORIGENRENG>' + ALLT(STR(comp_rew.imp_reng,15,2)) + '</IMPORIGENRENG>' + _S_
			AuxArchCompGral = AuxArchCompGral + Indent(5) + '<IMPRECARGOSRENG>' + ALLT(STR(comp_rew.actualiza1 + comp_rew.intereses1,15,2)) + '</IMPRECARGOSRENG>' + _S_
			AuxArchCompGral = AuxArchCompGral + Indent(5) + '<IMPTOTALRENG>' + ALLT(STR(comp_rew.imp_reng + comp_rew.actualiza1 + comp_rew.intereses1,15,2)) + '</IMPTOTALRENG>' + _S_
			AuxArchCompGral = AuxArchCompGral + Indent(4) + '</RENGLON>' + _S_
		ENDIF
		SKIP IN comp_rew
	ENDDO
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '</RENGLONES>' + _S_
	SET KEY TO '' IN comp_rew
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<FECHA_VENCIMIENTO_1>' + IIF(NOT EMPTY(comprobw.fec1_comp),DTOC(comprobw.fec1_comp),'') + '</FECHA_VENCIMIENTO_1>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_TOTAL_ORIGEN1>' + ALLT(STR(AuxImpTotOrig1,15,2)) + '</IMPORTE_TOTAL_ORIGEN1>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_RECARGO_1>' + ALLT(STR(comprobw.tot_act + comprobw.tot_int,15,2)) + '</IMPORTE_RECARGO_1>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_TOTAL_1>' + ALLT(STR(comprobw.tot_comp,15,2)) + '</IMPORTE_TOTAL_1>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<FECHA_VENCIMIENTO_2>' + IIF(NOT EMPTY(comprobw.fec2_comp),DTOC(comprobw.fec2_comp),'') + '</FECHA_VENCIMIENTO_2>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_TOTAL_ORIGEN2>' + ALLT(STR(AuxImpTotOrig2,15,2)) + '</IMPORTE_TOTAL_ORIGEN2>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_RECARGO_2>' + IIF(NOT EMPTY(comprobw.fec2_comp),ALLT(STR(comprobw.tot2_act + comprobw.tot2_int,15,2)),'0.00') + '</IMPORTE_RECARGO_2>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_TOTAL_2>' + IIF(NOT EMPTY(comprobw.fec2_comp),ALLT(STR(AuxImpTotOrig2 + comprobw.tot2_act + comprobw.tot2_int,15,2)),'0.00') + '</IMPORTE_TOTAL_2>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<FECHA_VENCIMIENTO_3>' + IIF(NOT EMPTY(comprobw.fec3_comp),DTOC(comprobw.fec3_comp),'') + '</FECHA_VENCIMIENTO_3>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_TOTAL_ORIGEN3>' + ALLT(STR(AuxImpTotOrig3,15,2)) + '</IMPORTE_TOTAL_ORIGEN3>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_RECARGO_3>' + IIF(NOT EMPTY(comprobw.fec3_comp),ALLT(STR(comprobw.tot3_act + comprobw.tot3_int,15,2)),'0.00') + '</IMPORTE_RECARGO_3>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_TOTAL_3>' + IIF(NOT EMPTY(comprobw.fec3_comp),ALLT(STR(AuxImpTotOrig3 + comprobw.tot3_act + comprobw.tot3_int,15,2)),'0.00') + '</IMPORTE_TOTAL_3>' + _S_
	
	AuxStringCodBarra = ''
	
	IF ALLTRIM(_DATOSREPLICADOS) = 'S' AND comprobw.grupo_comp = AuxGCompWeb 
	  FCODBARRA = 'FCBWebW()'
	ELSE
	  FCODBARRA = 'FCBWeb()'
	ENDIF
	
	*_fcbweb_ = "'" + ALLTRIM(_SISTEMA) + ALLTRIM(m.sistema) + "\" + ALLTRIM(m.modulo) + "\FCBWEB.PRG'"
	_fcbwebpath_ = "'" + ALLTRIM(_SISTEMA) + ALLTRIM(m.sistema) + "\" + ALLTRIM(m.modulo) + "\'"
	_oldpath = SET("Path")
	SET PATH TO &_fcbwebpath_
	
	*IF FILE(&_fcbweb_)
	IF FILE('FCBWEB.FXP')
		*SET PROCEDURE TO &_fcbweb_
		AuxStringCodBarra = &FCODBARRA
		*SET PROCEDURE TO MINIEFIPROC
		*ELSE
		*AuxStringCodBarra = 'holaaaaaa' + "'" + ALLTRIM(_SISTEMA) + ALLTRIM(m.sistema) + "\" + ALLTRIM(m.modulo) + "\'"
	ENDIF
	
	SET PATH TO &_oldpath
	
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<STRINGCODBARRA>' + AuxStringCodBarra + '</STRINGCODBARRA>' + _S_
	
	AuxArchCompGral = AuxArchCompGral + Indent(2) + '</COMPROBANTE>' + _S_
ENDPROC