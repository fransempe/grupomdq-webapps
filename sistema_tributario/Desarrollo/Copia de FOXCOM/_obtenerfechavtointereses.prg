* ObtenerFechaVTOIntereses: Obtiene la fecha a la cual se van a generar los intereses
*****************************************************************************************************************
FUNCTION _ObtenerFechaVTOIntereses() as Date
PRIVATE mFechaVTO as Date
PRIVATE mFechaVTOInt as Date



	mFechaVTOInt = _ObtenerFechaActuaWeb()	
	mFechaVTO = _ObtenerFechaVtoWeb()	
	
	
	IF (mFechaVTOInt = mFechaVTO) THEN	
		IF (INLIST(ALLTRIM(UPPER(CDOW(mFechaVTOInt))), 'SATURDAY', 'S�BADO')) THEN 
			mFechaVTOInt = mFechaVTOInt + 2
		ENDIF


		IF (INLIST(ALLTRIM(UPPER(CDOW(mFechaVTOInt))), 'SUNDAY', 'DOMINGO')) THEN 
			mFechaVTOInt = mFechaVTOInt + 1
		ENDIF	
	ENDIF
		
	RETURN mFechaVTOInt
ENDFUNC
*****************************************************************************************************************
*****************************************************************************************************************