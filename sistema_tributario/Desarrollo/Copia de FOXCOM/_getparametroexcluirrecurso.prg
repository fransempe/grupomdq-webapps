***********************************************************************************
*	M�todo _getParametroExcluirRecurso:  
***********************************************************************************
* 	Date:          05/07/2012
* 	Author:        Juan
* 	Design:        Juan
***********************************************************************************
*	Funcionamiento:
*		Busca en la tabla de configuraci�n del sistema el par�metro que indica si
*		se debe excluir alg�n recurso de la impresi�n de los comprobantes Web
***********************************************************************************
PROCEDURE _getParametroExcluirRecurso() as string
	PRIVATE strParametro as String
	PRIVATE strIndex as String
	
	
	IF (!USED ('config')) THEN
		=USET ('asa\config')
	ENDIF
	SELECT config
	SET ORDER TO PRIMARIO
	
		
	strParametro = ''
	strIndex = PADR('RECUR', 8, ' ') + 'RECUR_EXCLU_WEB'
	IF (SEEK(strIndex)) THEN
		strParametro = ALLTRIM(config.cont_par)		
	ENDIF
	
	
	RETURN ALLTRIM(strParametro)
ENDPROC