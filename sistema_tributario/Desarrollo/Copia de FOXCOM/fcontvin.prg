*************************************************
* FUNCTION FContVin
*************************************************
* Diseno : Pachu
* Fecha  : 9-02-2007
* 
* Funcionamiento:Retorna CUITs de Contibuyentes vinculado con un 
*                imponible determinado sin cargar m�scaras de tipos de imponibles
*                (desarrollado para la web en base a la funci�n FVINCON)
* 
* Par�metros:	m.tipo_imp (C): tipo de imponible
* 	 		m.nro_imp  (N): nro. de imponible
*			m.bloq     (L): �Bloquea?
*			m.msg      (L): emite mensage de aviso ?
*			@m.cuit1   (N): cuit del responsable del pago
*			@m.cuit2   (N): cuit del inquilino
* 
PARAMETER m.tipo_imp, m.nro_imp, m.bloq, m.msg, m.cuit1, m.cuit2
PRIVATE retorno, m.prior1, m.prior2
PRIVATE m.tipo_impv, m.tipo_vinc

retorno 		= .T.
m.cuit1 		= -1
m.cuit2 		= -1
m.tipo_vinc1	= ''
m.tipo_vinc2	= ''
m.prior1		= 10
m.prior2 		= 10

m.tipo_impv = m.tipo_impc
= SEEK (EVAL (clave_vin), 'vinc_imp')
	
DO WHILE retorno AND !EOF('vinc_imp') AND m.tipo_imp = vinc_imp.tipo_imp;
	AND m.nro_imp = vinc_imp.nro_imp AND m.tipo_impc = vinc_imp.tipo_impv

	IF EMPTY(vinc_imp.fec_baja)
		m.tipo_vinc = vinc_imp.tipo_vinc

		IF tips_vin.tipo_vinc == vinc_imp.tipo_vinc OR ;
			SEEK (EVAL (clave_tip), 'tips_vin')

			IF tips_vin.pri_cuit1 > 0 AND tips_vin.pri_cuit1 < m.prior1
				m.cuit1 		= vinc_imp.nro_impv
				m.prior1 		= tips_vin.pri_cuit1
				m.tipo_vinc1	= vinc_imp.tipo_vinc
			ENDIF

			IF tips_vin.pri_cuit2 > 0 AND tips_vin.pri_cuit2 < m.prior2
				m.cuit2 		= vinc_imp.nro_impv
				m.prior2 		= tips_vin.pri_cuit2
				m.tipo_vinc2	= vinc_imp.tipo_vinc
			ENDIF
		ELSE
			retorno = .F.
		ENDIF
	ENDIF
	SKIP IN vinc_imp
ENDDO

IF retorno	
	retorno = LeeContribVinc()
ENDIF

RETURN retorno


*************************************************
FUNCTION LeeContribVinc
*************************************************
PRIVATE retorno, m.cuit_cont, m.nro_impv
retorno = .T.

m.cuit_cont = 0

IF m.cuit1 > 0
	m.cuit_cont = m.cuit1
ELSE
	IF m.cuit2 > 0
		m.cuit_cont = m.cuit2
	ELSE
		retorno = .F.
	ENDIF
ENDIF

IF m.cuit_cont > 0
	retorno = FLeeImp(m.tipo_impc, m.cuit_cont, -1, m.bloq, 0, ' ')
	retorno = SEEK(STR(m.cuit_cont,10), 'contrib')

	IF retorno
		m.nro_impv = m.cuit_cont
		IF SEEK (EVAL (clave_vic), 'vinc_imp')
			IF tips_vin.tipo_vinc # vinc_imp.tipo_vinc
				m.tipo_vinc = vinc_imp.tipo_vinc
				= SEEK (EVAL(clave_tip), 'tips_vin')
			ENDIF
		ENDIF
	ENDIF

ENDIF

RETURN retorno

