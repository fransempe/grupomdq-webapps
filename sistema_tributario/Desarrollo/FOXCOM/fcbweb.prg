*************************************************
* FUNCTION FBAPROCB
*************************************************
* Fecha : 25/09/2012
* 
* Funcionamiento: Arma la cadena necesaria para imprimir el c�digo
*                 de barras para BAPRO.
* 
PARAMETERS fec_ven1, imp_comp, fec_ven2, imp_comp2, grupo_comp, nro_comp, dv_comp
PRIVATE retorno


retorno = '1419' && C�digo de Empresa
retorno = retorno + PADL (ALLT (STR (DAY (comprob.fec1_comp), 2, 0)), 2, '0') + PADL (ALLT (STR (MONTH (comprob.fec1_comp), 2, 0)), 2, '0') + SUBSTR (STR (YEAR (comprob.fec1_comp), 4, 0), 3, 2)  && Fecha del Primer Vencimiento
retorno = retorno + PADL (ALLT (STR (INT (comprob.tot_comp))), 8, '0') + PADL (ALLT (SUBSTR (STR (comprob.tot_comp - INT (comprob.tot_comp), 12, 2), 11, 2)), 2, '0') && Importe al Primer Vencimiento

IF EMPTY(comprob.fec2_comp) then 
	retorno = retorno + PADL (ALLT (STR (DAY (comprob.fec1_comp), 2, 0)), 2, '0') + PADL (ALLT (STR (MONTH (comprob.fec1_comp), 2, 0)), 2, '0') + SUBSTR (STR (YEAR (comprob.fec1_comp), 4, 0), 3, 2)  && Fecha del Primer Vencimiento
	retorno = retorno + PADL (ALLT (STR (INT (comprob.tot_comp))), 8, '0') + PADL (ALLT (SUBSTR (STR (comprob.tot_comp - INT (comprob.tot_comp), 12, 2), 11, 2)), 2, '0') && Importe al Primer Vencimiento
ELSE
	retorno = retorno + PADL (ALLT (STR (DAY (comprob.fec2_comp), 2, 0)), 2, '0') + PADL (ALLT (STR (MONTH (comprob.fec2_comp), 2, 0)), 2, '0') + SUBSTR (PADL(ALLTRIM(STR (YEAR (comprob.fec2_comp), 4, 0)),4,'0'), 3, 2)  && Fecha del Segundo Vencimiento	
	retorno = retorno + PADL (ALLT (STR (INT (AuxImpTotOrig2 + comprob.tot2_act + comprob.tot2_int))), 8, '0') + PADL (ALLT (SUBSTR (STR (AuxImpTotOrig2 + comprob.tot2_act + comprob.tot2_int - INT (AuxImpTotOrig2 + comprob.tot2_act + comprob.tot2_int), 12, 2), 11, 2)), 2, '0') && Importe al Segundo Vencimiento
ENDIF

retorno = retorno + PADL (ALLT (STR (comprob.grupo_comp, 2, 0)), 2, '0') + PADL (ALLT (STR (comprob.nro_comp, 9, 0)), 9, '0') + PADL (ALLT (STR (comprob.dv_comp, 2, 0)), 2, '0') && N�mero de Comprobante
retorno = retorno + Str (DVMod11 (retorno), 1, 0)


RETURN retorno


*************************************************
FUNCTION DVMod11
*************************************************
* 
* Funcionamiento:
* Calcula el D�gito Verificador del N�mero dado
* Utiliza el Algoritmo del "Mod 11". 
*
*
* Par�metros:
*  m.numero -C- Numero sobre el cual se quiere calcular el d�gito verificador
* 
* 
PARAMETERS m.numero
PRIVATE retorno, m.digver, m.modulo, m.largo, i

retorno = .T.
m.largo = LEN( m.numero )
m.digver = 0
m.modulo = 2

FOR i = m.largo TO 1 STEP -1
	IF isdigit( SUBSTR(m.numero,i,1) )
		m.digver = m.digver + ( VAL( SUBSTR(m.numero,i,1) ) * m.modulo )
	ENDIF
	m.modulo = m.modulo + 1
	IF m.modulo >= 8
		m.modulo = 2
	ENDIF
ENDFOR

m.digver = MOD( m.digver , 11 )

IF m.digver > 0
	m.digver = 11 - m.digver
ENDIF

IF m.digver = 10
	m.digver = 9
ENDIF

retorno = INT( m.digver )

RETURN retorno
