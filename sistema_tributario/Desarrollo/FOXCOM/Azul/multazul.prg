**************************************************
* FUNCTION MultAzul
************************************************** 
* Funcionamiento:
* Calcula el importe de multa p/Municipalidad de Azul.
PARAMETERS fecha_vto, factual, importe, interes
PRIVATE retorno, porcentaje, suma_int
retorno = 0

*IF m.programa = 'PGENPL2' AND es_pla(m.cod_plan)
*	RETURN retorno
*ENDIF

IF m.programa = 'PGENPL2' 
 IF m.cod_plan = 'PM01' or m.cod_plan = 'PM02' or;
    m.cod_plan = 'PM03' or m.cod_plan = 'PM04' or;
    m.cod_plan = 'PM05' or m.cod_plan = 'PM06' or;
    m.cod_plan = 'PM07' or m.cod_plan = 'PM08' or;
    m.cod_plan = 'PM09' or m.cod_plan = 'PM10' or;
    m.cod_plan = 'PM11' or m.cod_plan = 'PM12' or;
    m.cod_plan = 'PM13' or m.cod_plan = 'PM14' or;
    m.cod_plan = 'PM15' or m.cod_plan = 'PM16' or;
    m.cod_plan = 'PM17' or m.cod_plan = 'PM18' or;
    m.cod_plan = 'PM19' or m.cod_plan = 'PM20' or;
    m.cod_plan = 'PM21' or m.cod_plan = 'PM22' or;
    m.cod_plan = 'PM23' or m.cod_plan = 'PM24' or;
    m.cod_plan = 'PM25' or m.cod_plan = 'PM26' or;
    m.cod_plan = 'PM27' or m.cod_plan = 'PM28' or;
    m.cod_plan = 'PM29' or m.cod_plan = 'PM30' or;
    m.cod_plan = 'PM31' or m.cod_plan = 'PM32' or;
    m.cod_plan = 'PM33' or m.cod_plan = 'PM34' or;
    m.cod_plan = 'PM35' or m.cod_plan = 'PM36' or;
    m.cod_plan = 'PM37' or m.cod_plan = 'PM38' or;
    m.cod_plan = 'PM39' or m.cod_plan = 'PM40'
 endif
endif   
IF m.programa = 'PGENPL2'  
 if m.cod_plan = 'PF01' or m.cod_plan = 'PF02' or;
    m.cod_plan = 'PF03' or m.cod_plan = 'PF04' or;
    m.cod_plan = 'PF05' or m.cod_plan = 'PF06' or;
    m.cod_plan = 'PF07' or m.cod_plan = 'PF08' or;
    m.cod_plan = 'PF09' or m.cod_plan = 'PF10' or;
    m.cod_plan = 'PF11' or m.cod_plan = 'PF12' or;
    m.cod_plan = 'PF13' or m.cod_plan = 'PF14' or;
    m.cod_plan = 'PF15' or m.cod_plan = 'PF16' or;
    m.cod_plan = 'PF17' or m.cod_plan = 'PF18' or;
    m.cod_plan = 'PF19' or m.cod_plan = 'PF20' or;
    m.cod_plan = 'PF21' or m.cod_plan = 'PF22' or;
    m.cod_plan = 'PF23' or m.cod_plan = 'PF24' or;
    m.cod_plan = 'PF25' or m.cod_plan = 'PF26' or;
    m.cod_plan = 'PF27' or m.cod_plan = 'PF28' or;
    m.cod_plan = 'PF29' or m.cod_plan = 'PF30' or;
    m.cod_plan = 'PF31' or m.cod_plan = 'PF32' or;
    m.cod_plan = 'PF33' or m.cod_plan = 'PF34' or;
    m.cod_plan = 'PF35' or m.cod_plan = 'PF36' or;
    m.cod_plan = 'PF37' or m.cod_plan = 'PF38' or;
    m.cod_plan = 'PF39' or m.cod_plan = 'PF40'                              
	RETURN retorno
 endif	
ENDIF

m.porcentaje = 0
m.suma_int = .F.
IF m.fecha_vto >= {^1996-03-01} AND m.fecha_vto <= {^2000-12-31} AND tipo_imp='I' 	
	m.porcentaje = 10
ENDIF

IF m.fecha_vto >= {^1980-01-01} AND m.fecha_vto <= {^1991-03-31} AND tipo_imp !='V'	
	m.suma_int = .T.	
	m.porcentaje = 20
ENDIF

IF m.fecha_vto + 15 < m.factual
 if recurso # 'VI' and recurso # 'CM'    
	IF m.suma_int     
		retorno = (m.importe + m.interes) * (m.porcentaje / 100)     
	ELSE     
		retorno = m.importe * (m.porcentaje / 100)     
	ENDIF
 endif	
ENDIF

*	= Msg (dtoc(m.fecha_vto) + str(retorno,12,2))

RETURN retorno
