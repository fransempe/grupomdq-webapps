PARAMETERS importe, indice, fechaini, fechafin
PRIVATE coefic, anio, mes, dia, dia_o, mes_o, anio_o, dias_m, anio_ant, mes_ant

IF !USED ('INDICES')
	=USET ('RECUR\INDICES')
ENDIF

coefic = 0

anio_o = YEAR  (m.fechaini)
mes_o  = MONTH (m.fechaini)
dia_o  = DAY   (m.fechaini)
anio_f = year(m.fechafin)
mes_f  = month(m.fechafin)
dia_f = day(m.fechafin)

dias = m.fechafin - m.fechaini
valor = m.importe * dias * 12 / 36500

RETURN ROUND (valor,2)

