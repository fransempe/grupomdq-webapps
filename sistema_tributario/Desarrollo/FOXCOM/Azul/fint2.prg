PARAMETERS importe, indice, fechaini, fechafin
PRIVATE coefic, anio, mes, dia, dia_o, mes_o, anio_o, dias_m, anio_ant, mes_ant

*IF !USED ('INDICES')
* sele 21
* USE f:\sifim2\dbase\RECUR\INDICES share
*ENDIF

dias_12=0
dias_24=0
valor_12=0
valor_24=0

coefic = 0
dias_1=0
dias_2=0

anio_o = YEAR  (m.fechaini)
mes_o  = MONTH (m.fechaini)
dia_o  = DAY   (m.fechaini)
anio_f = year(m.fechafin)
mes_f  = month(m.fechafin)
dia_f = day(m.fechafin)

anio_hoy=year(date())

dias_12=0
dias_24=0

*wait window dtoc(fechaini) + ' ' + dtoc(fechafin)


if fechafin <= {^2011/02/04}
 if fechafin >= {^2009/01/01}
  if fechaini >= {^2008/12/31}
   dias_24=(fechafin - fechaini) - 1
  else
   dias_24=fechafin - {^2008/12/31} - 1
   dias_12={^2009/01/01} - fechaini - 1
  endif
 else
  dias_12 = fechafin - fechaini - 1 
 endif
endif   
  
if fechafin > {^2011/02/04} 
 if fechaini > {^2009/01/01}
  dias_12=fechafin - {^2011/02/04}
  dias_24={^2011/02/05} - fechaini - 1
 endif
endif

if fechafin > {^2011/02/04}
 if fechaini < {^2009/01/01}  
  dias_12=fechafin - {^2011/02/04} - 1
  dias_24={^2011/02/05} - {^2009/01/01}
  dias_12=dias_12 + {^2009/01/01} - fechaini
 endif
endif  


**dias_12 = {01/01/2009} - fechaini
**dias_24 = {05/02/2011} - {01/01/2009}

valor_12=m.importe * dias_12 * 12 / 36500
valor_24=m.importe * dias_24 * 24 / 36500
valor=valor_12 + valor_24


**if anio_f > 2008 
** dias_1={31/12/2008} - m.fechaini
** dias_2=(m.fechafin - {31/12/2008}) - 1 
**else
** dias_1=m.fechafin - m.fechaini
**endif

**valor1=m.importe * dias_1 * 12 / 36500
**valor2=m.importe * dias_2 * 24 / 36500
**valor=valor1 + valor2

RETURN ROUND (valor,2)
