PROCEDURE _getDatoByImponible(p_impTipo as String, p_impNro as number) as String
 	PRIVATE dato as String

	IF (!USED('inmueble')) THEN
		=UseT('recur\inmueble')
	ENDIF	
	IF (!USED('comercio')) THEN
		=UseT('recur\comercio')
	ENDIF
	IF (!USED('vehiculo')) THEN
		=UseT('recur\vehiculo')
	ENDIF
		
	
	dato = ''
	DO CASE 
		CASE UPPER(ALLTRIM(p_impTipo)) = 'I'
			SELECT inmueble
			SET ORDER TO primario IN inmueble
			IF (SEEK(STR(p_impNro, 10, 0), 'inmueble')) THEN
			
				dato = dato + IIF((inmueble.Circun <> 0), ALLTRIM(STR(inmueble.Circun)) + '-', '')
				dato = dato + IIF(NOT EMPTY(inmueble.Seccion), ALLTRIM(inmueble.Seccion) + '-', '')
				
				dato = dato + IIF((inmueble.NumChacra <> 0), ALLTRIM(STR(inmueble.NumChacra)), '')			
				dato = dato + IIF(NOT EMPTY(inmueble.LetChacra), ALLTRIM(inmueble.LetChacra) + '-', '')
				
				dato = dato + IIF((inmueble.NumQuinta <> 0), ALLTRIM(STR(inmueble.NumQuinta)), '')			
				dato = dato + IIF(NOT EMPTY(inmueble.LetQuinta), ALLTRIM(inmueble.LetQuinta) + '-', '')
				
				dato = dato + IIF((inmueble.NumFrac <> 0), ALLTRIM(STR(inmueble.NumFrac)), '')			
				dato = dato + IIF(NOT EMPTY(inmueble.LetFrac), ALLTRIM(inmueble.LetFrac) + '-', '')
				
				dato = dato + IIF((inmueble.NumMazna <> 0), ALLTRIM(STR(inmueble.NumMazna)), '')			
				dato = dato + IIF(NOT EMPTY(inmueble.LetMazna), ALLTRIM(inmueble.LetMazna) + '-', '')
				
				dato = dato + IIF((inmueble.NumParc <> 0), ALLTRIM(STR(inmueble.NumParc)), '')			
				dato = dato + IIF(NOT EMPTY(inmueble.LetParc), ALLTRIM(inmueble.LetParc) + '-', '')
			
				dato = dato + IIF(NOT EMPTY(inmueble.SubParc), ALLTRIM(inmueble.SubParc), '')			
			ENDIF
			
			
		CASE UPPER(ALLTRIM(p_impTipo)) = 'C'		
			SELECT comercio
			SET ORDER TO primario IN comercio
			IF (SEEK(STR(p_impNro, 10, 0), 'comercio')) THEN						
				IF (NOT EMPTY(comercio.Nomb_Fan)) THEN
					dato = ALLTRIM(comercio.Nomb_Fan)
				ELSE
					IF (NOT EMPTY(comercio.Nomb_Com)) THEN
						dato = ALLTRIM(comercio.Nomb_Com)
					ENDIF
				ENDIF
			ENDIF
		
		
		CASE UPPER(ALLTRIM(p_impTipo)) = 'V'
			SELECT vehiculo
			SET ORDER TO primario IN vehiculo
			IF (SEEK(STR(p_impNro, 10, 0), 'vehiculo')) THEN
				dato = dato + IIF(NOT EMPTY(vehiculo.Marca), ALLTRIM(vehiculo.Marca) + '-', '')
				dato = dato + IIF(NOT EMPTY(vehiculo.Modelo), ALLTRIM(vehiculo.Modelo) + '-', '')
				dato = dato + IIF((vehiculo.Anio_vehi <> 0), ALLTRIM(STR(vehiculo.Anio_vehi)), '')			
			ENDIF
	ENDCASE

	
	RETURN dato
ENDPROC