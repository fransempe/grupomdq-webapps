PROCEDURE _getVisitasCantidadDetalle(p_sFechaDesde as String, p_sFechaHasta as String) as String
	PRIVATE sRespuesta as String
	PRIVATE dFechaDesde as Date
	PRIVATE dFechaHasta as Date
	PRIVATE nConsultaInmuebles as number
	PRIVATE nConsultaComercios as number
	PRIVATE nConsultaVehiculos as number
	PRIVATE nConsultaCementerios as number
	PRIVATE nConsultaContribuyentes as number
	PRIVATE nConsultaImpresionInmuebles as number
	PRIVATE nConsultaImpresionComercios as number
	PRIVATE nConsultaImpresionVehiculos as number
	PRIVATE nConsultaImpresionCementerios as number
	PRIVATE nConsultaImpresionContribuyentes as number


	IF (!USED('web_vis')) THEN
		=UseT('recur\web_vis')
	ENDIF
	SELECT web_vis
	SCATTER MEMVAR BLANK
	
	nConsultaInmuebles = 0
	nConsultaComercios = 0
	nConsultaVehiculos = 0
	nConsultaCementerios = 0
	nConsultaContribuyentes = 0
	nConsultaImpresionInmuebles = 0
	nConsultaImpresionComercios = 0
	nConsultaImpresionVehiculos = 0
	nConsultaImpresionCementerios = 0
	nConsultaImpresionContribuyentes = 0
	
	
	dFechaDesde = CTOD(p_sFechaDesde)
	dFechaHasta = CTOD(p_sFechaHasta)
	
	
	*** Solo consultas ***
	COUNT TO nConsultaInmuebles FOR FECHA >= dFechaDesde AND FECHA <= dFechaHasta AND TIPO_IMP = 'I' AND IMPRIMIO = 'N'
	COUNT TO nConsultaComercios FOR FECHA >= dFechaDesde AND FECHA <= dFechaHasta AND TIPO_IMP = 'C' AND IMPRIMIO = 'N'
	COUNT TO nConsultaVehiculos FOR FECHA >= dFechaDesde AND FECHA <= dFechaHasta AND TIPO_IMP = 'V' AND IMPRIMIO = 'N'
	COUNT TO nConsultaCementerios FOR FECHA >= dFechaDesde AND FECHA <= dFechaHasta AND TIPO_IMP = 'E' AND IMPRIMIO = 'N'
	COUNT TO nConsultaContribuyentes FOR FECHA >= dFechaDesde AND FECHA <= dFechaHasta AND TIPO_IMP = 'N' AND IMPRIMIO = 'N'

	*** Consultas e impresiones ***				
	COUNT TO nConsultaImpresionInmuebles FOR FECHA >= dFechaDesde AND FECHA <= dFechaHasta AND TIPO_IMP = 'I' AND IMPRIMIO = 'S'
	COUNT TO nConsultaImpresionComercios FOR FECHA >= dFechaDesde AND FECHA <= dFechaHasta AND TIPO_IMP = 'C' AND IMPRIMIO = 'S'
	COUNT TO nConsultaImpresionVehiculos FOR FECHA >= dFechaDesde AND FECHA <= dFechaHasta AND TIPO_IMP = 'V' AND IMPRIMIO = 'S'
	COUNT TO nConsultaImpresionCementerios FOR FECHA >= dFechaDesde AND FECHA <= dFechaHasta AND TIPO_IMP = 'E' AND IMPRIMIO = 'S'
	COUNT TO nConsultaImpresionContribuyentes FOR FECHA >= dFechaDesde AND FECHA <= dFechaHasta AND TIPO_IMP = 'N' AND IMPRIMIO = 'S'


	*** Genero la cadena de respuesta ***				
	sRespuesta = 'I'+ '|' + ALLTRIM(STR(nConsultaInmuebles)) + '|' + ALLTRIM(STR(nConsultaImpresionInmuebles)) + '|' + ALLTRIM(STR(nConsultaInmuebles + nConsultaImpresionInmuebles)) + ';' + ;
				 'C'+ '|' + ALLTRIM(STR(nConsultaComercios)) + '|' + ALLTRIM(STR(nConsultaImpresionComercios)) + '|' + ALLTRIM(STR(nConsultaComercios + nConsultaImpresionComercios)) + ';' + ;
				 'V'+ '|' + ALLTRIM(STR(nConsultaVehiculos)) + '|' + ALLTRIM(STR(nConsultaImpresionVehiculos)) + '|' + ALLTRIM(STR(nConsultaVehiculos + nConsultaImpresionVehiculos)) + ';' + ;
				 'E'+ '|' + ALLTRIM(STR(nConsultaCementerios)) + '|' + ALLTRIM(STR(nConsultaImpresionCementerios)) + '|' + ALLTRIM(STR(nConsultaCementerios + nConsultaImpresionCementerios)) + ';' + ;
				 'N'+ '|' + ALLTRIM(STR(nConsultaContribuyentes)) + '|' + ALLTRIM(STR(nConsultaImpresionContribuyentes)) +  '|' + ALLTRIM(STR(nConsultaContribuyentes + nConsultaImpresionContribuyentes))+ ';' 


	RETURN ALLTRIM(sRespuesta)
ENDPROC