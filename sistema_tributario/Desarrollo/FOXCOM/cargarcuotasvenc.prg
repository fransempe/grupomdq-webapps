PROCEDURE CargarCuotasVenc
	PRIVATE AuxTodoOK, tempfecven_mov
	PRIVATE mFechaVTOInteres as Date
	PRIVATE strVerCreditos as string
	PRIVATE strRecursoOcultar as String
	PRIVATE strRecursoExcluir as String
	PRIVATE strUsaCheck as String
	PRIVATE strDescConc as String
	PRIVATE AuxStrExcluirRecurso as String 
	AuxTodoOK = .T.
	
	
	* �ste par�metro define si se tienen que mostrar los cr�ditos en los per�odos
	* vencidos, esta modificaci�n fue solicitada por la municipalidad de Z�rate
	strVerCreditos = 'S'
	strVerCreditos = _getParametroVerCredito()


	
	* Busco si existe un recurso a ocultar
	strRecursoOcultar = ''
	strRecursoOcultar = _getRecursosOcultar()
	
	
	* Busco si existe un recurso a excluir:
	* Esto quiere decir que el comprobante se debe mostrar desde la Web pero no poder seleccionar
	strRecursoExcluir = _getParametroExcluirRecurso()

    * Busco si hay que mostrar el c�digo de concepto de cuenta corriente o su descripci�n
	strDescConc=_getparametrodescconc()
	
	SELECT codifics
	SET KEY TO 'RECURS' IN codifics
	GO TOP IN codifics
	DO WHILE NOT EOF('codifics')
		SET KEY TO SUBSTR(codifics.codif,1,2) + P_TipoImponible + STR(P_NroImponible,10,0) IN rec_cc
		GO TOP IN rec_cc
		DO WHILE NOT EOF('rec_cc')
		
			IF EMPTY(rec_cc.fecven_mov)
				M_Mensaje = 'Inconsistencia de datos: Se encontraron movimientos en la ' + ;
							'cuenta corriente sin fecha de vencimiento'
				AuxTodoOK = .F.
			ENDIF
			IF AuxTodoOK
				* Si el movimiento est� en plan de pago o el movimiento no est� vencido
				*	o el movimiento est� vencido en alguno de los comprobantes a vencer
				*	(por ejemplo, porque su segundo o tercer vencimiento no hayan transcu-
				*	rrido), no se incluye en el cursor de cuotas vencidas
				* Si el movimiento estuviera prorrogado tambi�n se incluye en el grupo
				*	de los movimientos vencidos porque sino fuera as� no saldr�a ni en este
				*	ni tampoco en el de los comprobantes a vencer porque seguramente la 
				*	original de vencimiento del comprobante es anterior a hoy
				*****************************************************************************************************
				* Modificaci�n: 
				* Date: 23/05/2012
				* Author: Juan
				* Funci�n:
				* 		-> Se agrego la condici�n (ALLTRIM(strVerCreditos) = 'N' AND VAL(rec_cc.imp_mov) < 0)
				* 		   para que mostrar los cr�ditos sea manejado por el par�metro "WEB_VER_CREDITO"
				*		   y que de esta forma sea opcional para cada municipalidad		
				*****************************************************************************************************				
				IF rec_cc.est_mov = 'EP' OR rec_cc.fecven_mov >= DATE() OR MovIncluidoEnCompAVenc() OR (ALLTRIM(strVerCreditos) = 'N' AND VAL(rec_cc.imp_mov) < 0) OR;
				   (!EMPTY(strRecursoOcultar) AND ALLTRIM(strRecursoOcultar) = ALLTRIM(rec_cc.recurso))
					SKIP IN rec_cc
					LOOP
				ENDIF
				
				
				* Si el movimiento contiene el recurso a excluir no debe poder selecionarse desde la Web
				strUsaCheck = 'S'
				IF (!EMPTY(strRecursoExcluir) AND ALLTRIM(UPPER(strRecursoExcluir)) <> 'NINGUNO') THEN
					IF (ALLTRIM(strRecursoExcluir) = ALLTRIM(rec_cc.recurso)) THEN
						strUsaCheck = 'N'
					ENDIF
				ENDIF
			ENDIF

			
			tempfecven_mov = rec_cc.fecven_mov


			IF AuxTodoOK
				* Determinar si el movimiento est� prorrogado para no calcularle recargos por mora
				IF SEEK(rec_cc.recurso + STR(rec_cc.anio,4,0) + STR(rec_cc.cuota,3,0) + ;
						SUBSTR(rec_cc.id_orig,1,5),'rec_fec') AND (rec_fec.fe_pro1 >= DATE())
					tempfecven_mov = rec_fec.fe_pro1
				ENDIF
				
				m.actualiz = 0
				m.interes = 0
				
				IF tempfecven_mov < DATE()								
					mFechaVTOInteres = ObtenerFechaVTOIntereses()									
					AuxTodoOK = FActInt(tempfecven_mov, mFechaVTOInteres, rec_cc.imp_mov, @m.actualiz, @m.interes, rec_cc.recurso, rec_cc.conc_cc)
					*AuxTodoOK = FActInt(tempfecven_mov, mFechaVTOInteres, rec_cc.imp_mov, @m.actualiz, @m.interes)
				ENDIF
				
				m.imp_ori = ROUND(VAL(rec_cc.imp_mov),2)
				m.total = m.imp_ori + m.actualiz + m.interes
				
				SELECT CuotasVenc
				APPEND BLANK
				REPLACE recurso			WITH rec_cc.recurso,		;
						anio			WITH rec_cc.anio,			;
						cuota			WITH rec_cc.cuota,			;
						nro_mov			WITH rec_Cc.nro_mov,		;
						fecven_mov		WITH tempfecven_mov,		;
						imp_ori			WITH m.imp_ori,				;
						imp_actint		WITH m.actualiz + m.interes,;
						imp_iva			WITH 0,						;
						imp_tot			WITH m.total,				;
						cond_esp		WITH '',					;
						usa_check		WITH ALLTRIM(strUsaCheck)
						
				IF NOT EMPTY(ALLTRIM(rec_cc.juicio))
					REPLACE cond_esp WITH 'En juicio'
				ELSE
					IF NOT EMPTY(ALLTRIM(rec_cc.gest_leg))
						REPLACE cond_esp WITH 'Gest. Legal'
					ELSE
					
					
						* Si el imponible es un comercio, el recurso es "Seguridad e higiene", el estado del movimiento es normal 
						* y esta configurado que se debe presentar la DDJJ para poder imprimir los comprobantes desde la Web.
						* Entonces busco la declaraci�n jurada en la tabla recur/com_ddjj, en el caso de que no la encuentre asigno 
						* al campo "cond_esp" la descripci�n "Falta DDJJ".
						* Esto lo identifico de esta forma para que luego desde la Web sepa que no tengo que darle al usuario
						* la posibilidad de imprimir esta cuota.
						IF (rec_cc.tipo_imp = 'C') AND (UPPER(ALLTRIM(rec_cc.Recurso)) = UPPER(ALLTRIM(strCodigoRecursoSH)));
							AND (rec_cc.est_mov =='NO') AND (boolNoImprimirSinDDJJ) THEN										
								IF NOT SEEK(STR(rec_cc.nro_imp,10,0) + STR(rec_cc.anio,4,0) + STR(rec_cc.cuota,3,0),'com_ddjj')
									REPLACE cond_esp WITH 'Falta DDJJ'
								ENDIF
						ENDIF
						
						
					ENDIF
				ENDIF
				IF rec_cc.est_mov = 'DP'
					REPLACE de_plan WITH 'Si'
				ELSE
					REPLACE de_plan WITH ''
				ENDIF
				
				IF SEEK(rec_cc.recurso + rec_cc.conc_cc,'concs_cc')
							REPLACE dsc_ccc WITH concs_cc.dsc_ccc
					ELSE
							REPLACE dsc_ccc WITH ''
				ENDIF
                IF ALLTRIM(strDescConc) = 'CODIGO'
                  REPLACE dsc_ccc WITH ALLTRIM(rec_cc.conc_cc)
                ENDIF
				
				IF rec_cc.est_mov = 'DP'
					IF NOT EMPTY(ALLT(rec_cc.juicio)) OR NOT EMPTY(ALLT(rec_cc.gest_leg))
						AuxOrigPlanCCE = AuxOrigPlanCCE + m.imp_ori
						AuxRecPlanCCE = AuxRecPlanCCE + m.actualiz + m.interes
						AuxTotPlanCCE = AuxTotPlanCCE + m.total
					ELSE
						AuxOrigPlanSCE = AuxOrigPlanSCE + m.imp_ori
						AuxRecPlanSCE = AuxRecPlanSCE + m.actualiz + m.interes
						AuxTotPlanSCE = AuxTotPlanSCE + m.total
					ENDIF
				ELSE
					IF NOT EMPTY(ALLT(rec_cc.juicio)) OR NOT EMPTY(ALLT(rec_cc.gest_leg))
						AuxOrigNorCCE = AuxOrigNorCCE + m.imp_ori
						AuxRecNorCCE = AuxRecNorCCE + m.actualiz + m.interes
						AuxTotNorCCE = AuxTotNorCCE + m.total
					ELSE
						AuxOrigNorSCE = AuxOrigNorSCE + m.imp_ori
						AuxRecNorSCE = AuxRecNorSCE + m.actualiz + m.interes
						AuxTotNorSCE = AuxTotNorSCE + m.total
					ENDIF
				ENDIF
				
				* Si el recurso a�n no est� incluido en el cursor de recursos para Referencias,
				*	agregarlo. Leer CODIFICS en este momento para obtener la descripci�n del
				*	ser�a cr�tico porque los movimientos de cuenta corriente se leen a partir
				*	de CODIFICS. Por eso es que la obtenci�n de la descripci�n de los recursos
				*	se hace al final.
				IF NOT SEEK(rec_cc.recurso,'RefRecursos')
					SELECT RefRecursos
					APPEND BLANK
					REPLACE recurso WITH rec_cc.recurso
				ENDIF
			ENDIF
			SKIP IN rec_cc
		ENDDO
		SET KEY TO '' IN rec_cc
		SKIP IN codifics
	ENDDO
	SET KEY TO '' IN codifics
	RETURN AuxTodoOK
ENDPROC


***********************************************************************
FUNCTION MovIncluidoEnCompAVenc
***********************************************************************
	PRIVATE AuxYaIncluido
	
	AuxYaIncluido = .F.
	GO TOP IN CompAVencReng
	DO WHILE NOT EOF('CompAVencReng')
		SELECT CompAVencReng
		IF (recurso = rec_cc.recurso) AND (anio = rec_cc.anio) AND (cuota = rec_cc.cuota) ;
				AND (nro_mov = rec_cc.nro_mov)
			AuxYaIncluido = .T.
		ENDIF
		SKIP IN CompAVencReng
	ENDDO
	
	RETURN AuxYaIncluido
ENDFUNC



* ObtenerFechaVTOIntereses: Obtiene la fecha a la cual se van a generar los intereses
*****************************************************************************************************************
FUNCTION ObtenerFechaVTOIntereses() as Date
PRIVATE mFechaVTO as Date

	mFechaVTO = _ObtenerFechaVTOIntereses()		
	RETURN mFechaVTO

ENDFUNC
*****************************************************************************************************************
*****************************************************************************************************************
