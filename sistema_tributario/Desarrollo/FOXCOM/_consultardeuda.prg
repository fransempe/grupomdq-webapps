PROCEDURE _ConsultarDeuda (AuxArchDeud as string, TipoImp as String, NroImp as double, ambiente as Logical) as String
	EXTERNAL ARRAY vec_int
	PRIVATE AuxTodoOK
	PRIVATE strParaNoImprimirSinDDJJ as String
	PRIVATE boolNoImprimirSinDDJJ as Boolean
	PRIVATE strCodigoRecursoSH as String
	PRIVATE AuxTiposImp as String
	
				
	SELECT rec_cc		
	SET ORDER TO primario IN rec_cc
	AuxTodoOK = ambiente
	IF TYPE('TipoImp') = 'C' AND AuxTodoOK
        *IF UPPER(SUBSTR(TipoImp,1,5)) = 'CEMEN'
        IF UPPER(SUBSTR(TipoImp,1,1)) = 'E'
          AuxTiposImp = ALLTRIM(Get_Para('RECUR','WEB_TIPOSIMP'))
          IF AT('E', AuxTiposImp) > 0
            P_TipoImponible = 'E'
          ELSE
            IF AT('O', AuxTiposImp) > 0
              P_TipoImponible = 'O'
            ENDIF
          ENDIF
        ELSE
          IF UPPER(SUBSTR(TipoImp,1,5)) = 'CONTR'
            P_TipoImponible = 'N'
          ELSE
		    P_TipoImponible = SUBSTR(TipoImp,1,1)
		  ENDIF
        ENDIF

        IF TYPE('NroImp') = 'N'
	      IF NroImp <= 9999999999
		    P_NroImponible = NroImp
	      ELSE
			AuxTodoOK = .F.
			M_Mensaje = 'N�mero de imponible no v�lido. Debe ser menor que 9.999.999.999.'
		  ENDIF
	    ELSE
		  AuxTodoOK = .F.
		  M_Mensaje = 'Error de transferencia de datos en el N�mero de Imponible.'
		ENDIF
	ELSE
		AuxTodoOK = .F.
		M_Mensaje = 'Error de transferencia de datos en el Tipo de Imponible.'
	ENDIF
	
	IF AuxTodoOK	
		PRIVATE AuxOrigPlanCCE, AuxRecPlanCCE, AuxTotPlanCCE
		PRIVATE AuxOrigPlanSCE, AuxRecPlanSCE, AuxTotPlanSCE
		PRIVATE AuxOrigNorCCE, AuxRecNorCCE, AuxTotNorCCE				
		PRIVATE AuxOrigNorSCE, AuxRecNorSCE, AuxTotNorSCE
		AuxOrigPlanCCE = 0
		AuxRecPlanCCE = 0
		AuxTotPlanCCE = 0
		AuxOrigPlanSCE = 0
		AuxRecPlanSCE = 0
		AuxTotPlanSCE = 0
		AuxOrigNorCCE = 0
		AuxRecNorCCE = 0
		AuxTotNorCCE = 0		
		AuxOrigNorSCE = 0
		AuxRecNorSCE = 0
		AuxTotNorSCE = 0
	ENDIF


 


	* Date: 11/10/2012
	* Author: Juan
	* Busco el par�metro "WEB_NOIMP_SINDJ" para ver si el sistema tiene que checkear que para
	* poder imprimir cuotas de cuenta corriente o comprobantes se haya presentado previamente
	* la declaraci�n jurada. En el caso de que el valor del par�metro sea "S" busco el segundo 
	* par�metro que se llama "WEB_CODRECUR_SH" para obtener cual es el recurso de Seguridad e
	* higiene
	strParaNoImprimirSinDDJJ = ''
	boolNoImprimirSinDDJJ = .F.
	strCodigoRecursoSH = ''
	IF (AuxTodoOK) THEN	 
		strParaNoImprimirSinDDJJ = Get_Para ('ASA','WEB_NOIMP_SINDJ')
		IF (NOT EMPTY(strParaNoImprimirSinDDJJ)) THEN				
			IF (UPPER(ALLTRIM(strParaNoImprimirSinDDJJ)) = 'S') THEN				
				boolNoImprimirSinDDJJ = .T.
				strCodigoRecursoSH = Get_Para ('ASA','WEB_CODRECUR_SH')

				AuxTodoOK = (NOT EMPTY(strCodigoRecursoSH)) 			
			ENDIF
		ENDIF		
	ENDIF

	

	* Buscar y agregar al cursor CompAVenc los comprobantes a�n no vencidos que est�n
	*	sin cancelar
	IF AuxTodoOK
		AuxTodoOK = CargarCompAVenc()
	ENDIF
	
	* Buscar y agregar al cursor CuotasVenc los movimientos de cuenta corriente sin
	*	cancelar ya vencidos que no est�n incluidos en ninguno de los comprobantes
	*	por vencer
	IF AuxTodoOK
		AuxTodoOK = CargarCuotasVenc()
	ENDIF
	
	IF AuxTodoOK
		AuxTodoOK = AgregarDatosCuenta(@AuxArchDeud)
		IF AuxTodoOK
			IF (RECCOUNT('CuotasVenc') = 0) AND (RECCOUNT('CompAVenc') = 0)
				M_Mensaje = 'La cuenta no registra deuda al ' + DTOC(DATE()) + '.'
			ELSE
				AgregarCuotasVenc(@AuxArchDeud)
				AgregarProxCompAVenc(@AuxArchDeud)
				AgregarOtrosCompAVenc(@AuxArchDeud)
				AgregarReferencias(@AuxArchDeud)
			ENDIF
		ENDIF
	ENDIF

	RETURN AuxTodoOK
ENDPROC
