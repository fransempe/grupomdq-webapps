PROCEDURE _ObtenerImponibleBloqueado(pTipoImponible as String, pNroImponible as Number) as String
PRIVATE AuxEstado

	IF SEEK(pTipoImponible+STR(pNroImponible,10), 'IMPOBLOQ')
		AuxEstado='BLOQ'
	ELSE
		AuxEstado='NOBLOQ'
	ENDIF

	RETURN AuxEstado

ENDPROC
