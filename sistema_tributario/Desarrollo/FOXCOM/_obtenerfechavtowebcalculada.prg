FUNCTION _ObtenerFechaVTOWebCalculada() as Date
PRIVATE mFechaVTO as Date

	mFechaVTO = _ObtenerFechaVtoWeb()			
	IF (INLIST(ALLTRIM(UPPER(CDOW(mFechaVTO))), 'SATURDAY', 'S�BADO')) THEN 
		mFechaVTO = mFechaVTO + 2
	ENDIF

	IF (INLIST(ALLTRIM(UPPER(CDOW(mFechaVTO))), 'SUNDAY', 'DOMINGO')) THEN 
		mFechaVTO = mFechaVTO + 1
	ENDIF
	
	RETURN mFechaVTO
ENDFUNC