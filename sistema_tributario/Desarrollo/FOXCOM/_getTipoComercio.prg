PROCEDURE _getTipoComercio(p_impTipo as String, p_impNro as number) as String
 	PRIVATE dato as String
 		
	IF (!USED('comercio')) THEN
		=UseT('recur\comercio')
	ENDIF
	
		dato = ''

		IF UPPER(ALLTRIM(p_impTipo)) = 'C'		
			SELECT comercio
			SET ORDER TO primario IN comercio
			IF (SEEK(STR(p_impNro, 10, 0), 'comercio')) THEN
				dato = dato + ALLTRIM(comercio.gran_cont)						
			ENDIF
		ELSE dato=''
		ENDIF

	
	RETURN dato
ENDPROC