PROCEDURE AmbienteWeb() as Boolean

	PUBLIC _STRCONV_, _S_, _CR_, _LF_, _SERVER, _DBASE, sistema, modulo, programa, M_Mensaje
	PRIVATE retorno
	retorno = .T.
	
	M_Mensaje = ''
	_STRCONV_ = 9	&& C�digo de conversi�n de codificaci�n para pasar a STRCONV()
	_CR_ = CHR(13)
	_LF_ = CHR(10)
	_S_ = _CR_ + _LF_
	
	***************************************************************
	*
	* Seteos de FoxPro
	*
	***************************************************************
	SET DEBUG OFF
	SET BLOCKSIZE TO 33
	SET COLLATE TO 'SPANISH'
	SET CONFIRM ON
	SET CENTURY ON
	SET DATE TO DMY
	SET DELETED ON
	SET EXACT OFF
	SET EXCLUSIVE OFF
	SET MARK TO '/'
	SET MESSAGE TO 24 CENTER
	SET NEAR ON
	SET POINT TO '.'
	SET PRINT OFF
	SET REPROCESS TO 1
	SET SAFETY OFF
	SET SEPARATOR TO ','
	SET CONSOLE OFF
	SET DECIMALS TO 2
	SET ESCAPE OFF
	SET MULTILOCKS ON
	SET NOTIFY ON
	SET REFRESH TO 1,1
	SET SPACE ON
	SET UDFPARMS TO VALUE
	SET TALK OFF
	SET STRICTDATE TO 0
	SET PROCEDURE TO MINIEFIPROC

	***************************************************************
	*
	* Lectura de archivo .INI
	*
	***************************************************************
	_X_ = ""
	_PATH = ""
	_SERVER = ""
	_DBASE = ""
	m.sistema = ""
	m.modulo = ""
	
	IF LeerIni("PATH",@_X_) = 0
		_PATH = _X_
		SET PATH TO &_X_
	ELSE
		retorno = .F.
	ENDIF
	_X_ = ""
	IF retorno AND LeerIni("SERVER",@_X_) # 0
		retorno = .F.
	ELSE
		_SERVER = _X_
	ENDIF
	_X_ = ""		
	IF retorno AND LeerIni("DBASE",@_X_) # 0
		retorno = .F.
	ELSE
		_DBASE = _X_
	ENDIF
	_X_ = ""
	IF retorno AND LeerIni("SISTEMA",@_X_) # 0
		retorno = .F.
	ELSE
		m.sistema = PADR(ALLTRIM(_X_),8)
	ENDIF
	_X_ = ""
	IF retorno AND LeerIni("MODULO",@_X_) # 0
		retorno = .F.
	ELSE
		m.modulo = PADR(ALLTRIM(_X_),8)
	ENDIF
	_X_ = ""
	IF retorno AND LeerIni("PROGRAMA",@_X_) # 0
		retorno = .F.
	ELSE
		m.programa = PADR(ALLTRIM(_X_),8)
	ENDIF

	*
	*	Control de activaci�n
	*
	IF retorno AND NOT Reg2FX(m.programa)
		M_Mensaje = 'Servicio no disponible.'
		retorno = .F.
	ENDIF

	RETURN retorno
	
ENDPROC