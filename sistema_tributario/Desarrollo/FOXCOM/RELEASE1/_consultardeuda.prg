PROCEDURE _ConsultarDeuda (AuxArchDeud as string, TipoImp as String, NroImp as Integer, ambiente as Logical) as String
	EXTERNAL ARRAY vec_int
	PRIVATE AuxTodoOK
	
	SET ORDER TO primario IN rec_cc
	AuxTodoOK = ambiente
	IF TYPE('TipoImp') = 'C' AND AuxTodoOK
		P_TipoImponible = SUBSTR(TipoImp,1,1)
		IF TYPE('NroImp') = 'N'
			IF NroImp <= 9999999999
				P_NroImponible = NroImp
			ELSE
				AuxTodoOK = .F.
				M_Mensaje = 'N�mero de imponible no v�lido. Debe ser menor que 9.999.999.999.'
			ENDIF
		ELSE
			AuxTodoOK = .F.
			M_Mensaje = 'Error de transferencia de datos en el N�mero de Imponible.'
		ENDIF
	ELSE
		AuxTodoOK = .F.
		M_Mensaje = 'Error de transferencia de datos en el Tipo de Imponible.'
	ENDIF
	

	IF AuxTodoOK	
		PRIVATE AuxOrigPlanCCE, AuxRecPlanCCE, AuxTotPlanCCE
		PRIVATE AuxOrigPlanSCE, AuxRecPlanSCE, AuxTotPlanSCE
		PRIVATE AuxOrigNorCCE, AuxRecNorCCE, AuxTotNorCCE				
		PRIVATE AuxOrigNorSCE, AuxRecNorSCE, AuxTotNorSCE
		AuxOrigPlanCCE = 0
		AuxRecPlanCCE = 0
		AuxTotPlanCCE = 0
		AuxOrigPlanSCE = 0
		AuxRecPlanSCE = 0
		AuxTotPlanSCE = 0
		AuxOrigNorCCE = 0
		AuxRecNorCCE = 0
		AuxTotNorCCE = 0		
		AuxOrigNorSCE = 0
		AuxRecNorSCE = 0
		AuxTotNorSCE = 0
	ENDIF

	* Buscar y agregar al cursor CompAVenc los comprobantes a�n no vencidos que est�n
	*	sin cancelar
	IF AuxTodoOK
		AuxTodoOK = CargarCompAVenc()
	ENDIF
	
	* Buscar y agregar al cursor CuotasVenc los movimientos de cuenta corriente sin
	*	cancelar ya vencidos que no est�n incluidos en ninguno de los comprobantes
	*	por vencer
	IF AuxTodoOK
		AuxTodoOK = CargarCuotasVenc()
	ENDIF
	
	IF AuxTodoOK
		AuxTodoOK = AgregarDatosCuenta(@AuxArchDeud)
		IF AuxTodoOK
			IF (RECCOUNT('CuotasVenc') = 0) AND (RECCOUNT('CompAVenc') = 0)
				AuxArchDeud = AuxArchDeud + Indent(1) + '<NOTAS>' + _S_
				AuxArchDeud = AuxArchDeud + Indent(2) + '<REGISTRO_NOTAS>' + _S_
				AuxArchDeud = AuxArchDeud + Indent(3) + ;
					'<MENSAJE>La cuenta no registra deuda al ' + ;
					DTOC(DATE()) + '.' + '</MENSAJE>' + _S_
				AuxArchDeud = AuxArchDeud + Indent(2) + '</REGISTRO_NOTAS>' + _S_
				AuxArchDeud = AuxArchDeud + Indent(1) + '</NOTAS>' + _S_
			ELSE
				AgregarCuotasVenc(@AuxArchDeud)
				AgregarProxCompAVenc(@AuxArchDeud)
				AgregarOtrosCompAVenc(@AuxArchDeud)
				AgregarReferencias(@AuxArchDeud)
			ENDIF
		ENDIF
	ENDIF

	RETURN AuxTodoOK
ENDPROC
