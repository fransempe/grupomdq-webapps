PROCEDURE AgregarOtrosCompAVenc(AuxArchDeud as String)
	PRIVATE AuxPrimeraVez
	
	AuxPrimeraVez = .T.
	
	GO TOP IN CompAVenc
	SELECT CompAVenc
	DO WHILE NOT EOF('CompAVenc')
		IF CompAVenc.fecha_venc > DATE() + 60
			IF AuxPrimeraVez
				AuxArchDeud = AuxArchDeud + Indent(1) + '<DETALLEOTROSCOMPAVENC>' + _S_
			ENDIF
			AuxArchDeud = AuxArchDeud + Indent(2) + '<REGISTRO_DETALLEOTROSCOMPAVENC>' + _S_
			
			AuxArchDeud = AuxArchDeud + Indent(3) + '<COMPROBANTE>' + PADL(ALLTRIM(STR(grupo_comp,2,0)),2,'0') ;
				+ '/' + PADL(ALLTRIM(STR(nro_comp,9,0)),9,'0') + '/' + ;
				PADL(ALLTRIM(STR(dv_comp,2,0)),2,'0') + '</COMPROBANTE>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DOC_DETALLE>' + ALLTRIM(detalle_comp) + '</DOC_DETALLE>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DOC_VENC>' + ALLTRIM(STR(venc,1,0)) + '</DOC_VENC>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DOC_FECHAVENC>' + DTOC(fecha_venc) + '</DOC_FECHAVENC>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DOC_IMPORTEORIGEN>' + ALLTRIM(STR(tot_ori,15,2)) + '</DOC_IMPORTEORIGEN>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DOC_IMPORTERECARGO>' + ALLTRIM(STR(tot_actint,15,2)) + '</DOC_IMPORTERECARGO>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DOC_IMPORTETOTAL>' + ALLTRIM(STR(tot_comp,15,2)) + '</DOC_IMPORTETOTAL>' + _S_
			
			AuxArchDeud = AuxArchDeud + Indent(2) + '</REGISTRO_DETALLEOTROSCOMPAVENC>' + _S_
			IF AuxPrimeraVez
				AuxArchDeud = AuxArchDeud + Indent(1) + '</DETALLEOTROSCOMPAVENC>' + _S_
				AuxPrimeraVez = .F.
			ENDIF
		ENDIF
		SKIP IN CompAVenc
	ENDDO
	IF AuxPrimeraVez
		AuxArchDeud = AuxArchDeud + Indent(1) + '<DETALLEOTROSCOMPAVENC>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(1) + '</DETALLEOTROSCOMPAVENC>' + _S_
	ENDIF
ENDPROC
