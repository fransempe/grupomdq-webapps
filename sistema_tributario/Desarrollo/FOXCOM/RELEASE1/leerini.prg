PROCEDURE LeerIni (VarALeer as String, VarADevolver as String) as Integer
	* Devuelve
	*	0	Lectura exitosa, valor en VarADevolver
	*	1	Error, no se encuentra el archivo
	*	2	No se encontr� el valor
	PRIVATE ArchIni, EncontroValor, retorno
	
	retorno = 0
	EncontroValor = .F.
	ArchIni = 'recurweb.ini'
	
	oldpath = SET("Path")
	_SYSPATH_ = GETENV("Path")
	SET PATH TO &_SYSPATH_
	
	_FILE = FOPEN(ArchIni)
	SET PATH TO &oldpath
	IF _FILE > 0
		DO WHILE NOT FEOF(_FILE)
			_LIN_ = FGETS(_FILE)
			_SEPARADOR_ = RAT('=',_LIN_,1)
			IF ALLTRIM(SUBSTR(_LIN_,1,_SEPARADOR_-1)) == ALLTRIM(VarALeer)
				VarADevolver = ALLTRIM(SUBSTR(_LIN_,_SEPARADOR_+1))
				EncontroValor = .T.
				EXIT
			ENDIF
		ENDDO		
		FCLOSE(_FILE)
		IF NOT EncontroValor
			retorno = 2
		ENDIF
	ELSE
		retorno = 1
	ENDIF

	RETURN retorno
ENDPROC