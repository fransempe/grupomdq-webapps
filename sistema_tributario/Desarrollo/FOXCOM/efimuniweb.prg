PROCEDURE EfimuniWeb

	***************************************************************
	*
	* Declaraci�n de variables p�blicas
	*
	***************************************************************
	PUBLIC m.d_sitiva, m.d_tipocont, m.d_codbaja, m.up_contrib, m.leeimp
	PUBLIC m.tipo_imp, m.masc_cont, m.dig_cont, m.status
	PUBLIC exp_cod, m.calle_ant, m.loc_ant, m.est_imp
	PUBLIC anio_inic, anio_cant, m.nro_indice, m.rep_def, m.drep_def
	PUBLIC m.cant_reg, m.tipo_impc, clave_vin, clave_tip, clave_vic
	PUBLIC usa_fx_act, usa_fx_int
	PUBLIC masc_imp, dig_imp, tab_imp, ident_imp
	PUBLIC desc_tab, ident_exp, ubic_exp, indi_ide, indi_ubi
	PUBLIC vec_act, vec_int
	
	***************************************************************
	*
	* Apertura de tablas
	*
	***************************************************************
	
	
	=UseT('recur\tips_vin')	
	=UseT('recur\vinc_imp')



	=UseT('recur\comercio')
	SELECT comercio
	SCATTER MEMVAR BLANK
		
	
	=UseT('recur\contrib')
	SELECT contrib
	SCATTER MEMVAR BLANK
	
	
	
	=UseT('recur\inmueble')
	SELECT inmueble
	SCATTER MEMVAR BLANK
	
	=UseT('recur\vehiculo')
	SELECT vehiculo
	SCATTER MEMVAR BLANK
			
	
	
	=UseT('recur\ubic_cem')
	SELECT ubic_cem
	SCATTER MEMVAR BLANK
	
	=UseT('recur\cement')
	SELECT cement
	SCATTER MEMVAR BLANK
		
	
		
	=UseT('recur\impobloq')
	SELECT impobloq
	SCATTER MEMVAR BLANK
	
	=UseT('recur\rec_inf')
	SELECT REC_INF
	SCATTER MEMVAR BLANK
	
		
	***************************************************************
	*
	* Comienzo variables necesarias de EFIMUNI
	*
	***************************************************************
	DIMENSION masc_imp[26] , dig_imp[26] , tab_imp[26] , ident_imp[26]
	DIMENSION desc_tab[26], ident_exp[26], ubic_exp[26], indi_ide[26],indi_ubi[26]
	DIMENSION vec_act[1], vec_int[1]

	m.tipo_impc = ''
	m.masc_cont = ''
	m.dig_cont  = ''
	m.dig_tip	= ' '
	m.tipoc		= ''
	m.masc_com 	= ''
	m.dig_com  	= ''
	m.rep_def	= SPACE(3)
	m.drep_def	= ''
	anio_inic		=	0
	anio_cant		=	0
	vec_act[1]	=	0
	vec_int[1]	=	0
	m.totiva1ori=	0
	m.totiva2ori=	0
	m.totivaori = 0
	m.totactual	=	0
	m.totinteres=	0
	m.totiva1rec=	0
	m.totiva2rec=	0
	m.imptotal	=	0
	on_off		=	'  S'
	m.ivape_conf= .F.
	m.conc_ivape=	0
	m.por_ivape	=	0
	m.min_ivape	=	0
	m.tipo_vinc1=	''
	m.tipo_vinc2=	''
	m.cuit1 		= 0
	m.cuit2 		= 0
	m.desc_imp_	=	''
	m.sit_iva		=	''
	m.desc_sitiv=	''
	hay_pases 	= .F.
	m.exp_comp	=	SPAC(10)
	m.fecha_ld	=	''
	m.fecrc_ini	=	''
	m.fecrc_fin	=	''
	m.dsc_nroexp= ''
	m.desde_rc	=	''
	m.cant_reg = 0


	=FCarMas()
	m.status = FLeeTim( "CONTRIB",.T., @m.tipo_impc, @m.masc_cont, @m.dig_cont )
	m.status = m.status AND FLeeTim( "COMERCIO",.T., @m.tipoc, @m.masc_com,;
				 @m.dig_com )
	m.tipo_impc = 'N'
	clave_tip = getkey('tips_vin', .T.)
	clave_vin = getkey('vinc_imp', .T., 3)
	clave_vic = getkey('vinc_imp', .T.)
	
	usa_fx_act = .F.
	usa_fx_int = .F.
	=FCARIND(.T.,'','')
	***************************************************************
	*
	* Fin variables necesarias de EFIMUNI
	*
	***************************************************************

ENDPROC