PROCEDURE _EmitirCompPagoNoVenc (AuxArchCompNoVenc as string, XMLGrupoNroComp as String, ambiente as Logical) as String
	PRIVATE AuxHuboError
	PRIVATE strDatoVariable as String
	PRIVATE strDatoVariable2 AS STRING
	PRIVATE strDatoVariable3 AS STRING
		
	SET ORDER TO primario IN rec_cc
	AuxHuboError = NOT ambiente
	
	XMLTOCURSOR(XMLGrupoNroComp,'P_ArchCompSelec')
	SELECT P_ArchCompSelec
	IF UPPER(ALIAS()) # UPPER('P_ArchCompSelec')
		AuxHuboError = .T.
		M_Mensaje = 'Imposible generar cursor de datos.'
	ENDIF

	IF NOT AuxHuboError
		m.cuit1 = 0
		m.cuit2 = 0
		m.nombretitular = ''
		m.nombreresppago = ''
					
		AuxArchCompNoVenc = AuxArchCompNoVenc + Indent(1) + '<IMPONIBLE>' + _S_
		GO TOP IN P_ArchCompSelec
		* Debido a problemas de conversi�n con la funci�n XMLTOCURSOR el primer registro
		*	se utiliza como m�scara de campos y, por la tanto, se omite.
		SKIP IN P_ArchCompSelec
		=FContVin(P_ArchCompSelec.tipoimponible,P_ArchCompSelec.nroimponible,.F.,.F.,@m.cuit1,@m.cuit2)
		* Si el imponible es un contribuyente y no tiene ninguna entrada en VINC_IMP ==> ese mismo
		*	contribuyente se toma c�mo titular titular.
		IF m.cuit1 <= 0 AND P_ArchCompSelec.tipoimponible = 'N'
			m.cuit1 = P_ArchCompSelec.nroimponible
		ENDIF
		IF m.cuit1 > 0
			IF SEEK(STR(m.cuit1,10,0),'contrib')
				m.nombretitular = ALLT(STRTRAN(contrib.nomb_cont, '&', '&amp;'))
			ENDIF
		ENDIF
		IF m.cuit2 > 0
			IF SEEK(STR(m.cuit2,10,0),'contrib')
				m.nombreresppago = ALLTRIM(STRTRAN(contrib.nomb_cont, '&', '&amp;'))
			ENDIF
		ENDIF


		
		strDatoVariable = _getDatoByImponible(UPPER(ALLTRIM(P_ArchCompSelec.tipoimponible)), P_ArchCompSelec.nroimponible)
		strDatoVariable2 = _getDatoByImponible2(UPPER(ALLTRIM(P_ArchCompSelec.tipoimponible)), P_ArchCompSelec.nroimponible)
		strDatoVariable3 = _getTipoComercio(UPPER(ALLTRIM(P_ArchCompSelec.tipoimponible)), P_ArchCompSelec.nroimponible)
		
		
		AuxArchCompNoVenc = AuxArchCompNoVenc + Indent(2) + '<TIPOIMPONIBLE>' + ALLT(P_ArchCompSelec.tipoimponible) + '</TIPOIMPONIBLE>' + _S_
		AuxArchCompNoVenc = AuxArchCompNoVenc + Indent(2) + '<NROIMPONIBLE>' + ALLT(STR(P_ArchCompSelec.nroimponible,10,0)) + '</NROIMPONIBLE>' + _S_
		AuxArchCompNoVenc = AuxArchCompNoVenc + Indent(2) + '<TITULAR>' + ALLT(m.nombretitular) + '</TITULAR>' + _S_
		AuxArchCompNoVenc = AuxArchCompNoVenc + Indent(2) + '<DOMICILIO>' + ALLT(ALLT(contrib.nomb_calle) + ' ' + ALLT(contrib.puerta) + ' ' + ALLT(contrib.puertabis) + ' ' + ALLT(contrib.piso) + ' ' + ALLT(contrib.depto)) + '</DOMICILIO>' + _S_
		AuxArchCompNoVenc = AuxArchCompNoVenc + Indent(2) + '<LOCALIDAD>(' + ALLT(ALLT(contrib.cod_post) + ') ' + ALLT(contrib.nomb_loc)) + '</LOCALIDAD>' + _S_		
		AuxArchCompNoVenc = AuxArchCompNoVenc + Indent(2) + '<DATOVARIABLE>' + ALLT(strDatoVariable) + '</DATOVARIABLE>' + _S_
		AuxArchCompNoVenc = AuxArchCompNoVenc + Indent(2) + '<DATOVARIABLE2>' + ALLT(strDatoVariable2) + '</DATOVARIABLE2>' + _S_
		AuxArchCompNoVenc = AuxArchCompNoVenc + Indent(2) + '<DATOVARIABLE3>' + ALLT(strDatoVariable3) + '</DATOVARIABLE3>' + _S_
								
		AuxArchCompNoVenc = AuxArchCompNoVenc + Indent(1) + '</IMPONIBLE>' + _S_
		AuxArchCompNoVenc = AuxArchCompNoVenc + Indent(1) + '<COMPROBANTES>' + _S_
		DO WHILE NOT EOF('P_ArchCompSelec')			
			SET ORDER TO primario IN comprob
			P_Grupocomprobantes = P_ArchCompSelec.grupocomp
			P_NroComprobante = P_ArchCompSelec.nrocomp
			IF SEEK(STR(P_GrupoComprobantes,2,0)+STR(P_NroComprobante,9,0),'comprob')
				AgregCompAArchSalida (@AuxArchCompNoVenc)
			ENDIF				
			SKIP IN P_ArchCompSelec
		ENDDO
		AuxArchCompNoVenc = AuxArchCompNoVenc + Indent(1) + '</COMPROBANTES>' + _S_			
	ELSE
		M_Mensaje = 'Ha habido un error en el procesamiento de datos.'
	ENDIF
	
	RETURN NOT AuxHuboError
ENDPROC
