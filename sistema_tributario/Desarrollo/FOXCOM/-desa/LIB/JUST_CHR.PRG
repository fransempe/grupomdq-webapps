*************************************************
FUNCTION JUST_CHR
*************************************************
* Autor : RODRIGO
* Dise�o: RODRIGO
PARAMETERS cadena, caract
PRIVATE retorno, occ, dif, i, cad_res, occ2, bk_agreg
retorno = .T.
cadena  = TRIM (cadena)
occ 		= OCCURS(' ',cadena)
occ2		= occ
dif 		= caract - LEN(cadena)
cad_res	=	cadena
bk_agreg= 0
IF occ > 0 AND dif >= 0
	FOR I = 1 to occ
		blanco 	= AT(' ',cadena,i)
		cant_bk	= CEILING(dif/occ2)
		cad_res	= STUFF(cad_res,blanco+bk_agreg,0,SPAC(cant_bk))
		bk_agreg= bk_agreg + cant_bk 
		dif			= dif - cant_bk
		occ2		=	occ2 - 1 
	ENDFOR
ENDIF
RETURN cad_res
*
