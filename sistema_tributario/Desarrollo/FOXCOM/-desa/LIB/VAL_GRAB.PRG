***************************************************
FUNCTION VAL_GRAB
*************************************************
* Autor: RODRIGO
* 
* Fecha: 08/10/94
* 
* Funcionamiento: Valida los campos de la DBF interna (DBF_GRAL) en la secuencia
* en que se encuentran ubicados en la pantalla. Dependiendo el parametro que se 
* le pase valida TODOS los campos, los campos CLAVES Y los campos NO CLAVES.
* 
* Par�metros: GQUE_VAL		(C) : Permite solo estos valores:
*																											T: Valida TODOS los campos
*																											C: Valida solo CLAVES 
* 																										D: Valida solo NO CLAVES
* 
* Modificaciones: 14/08/1994 
*									Se cambiaron los nombres de variables, y el manejo de las
*	Tablas de Validaciones que causaban problemas al interactuar con los pro-
*	gramas.
*
*	06/01/95	(RODRIGO) El seteo de la variable grup_mem se hacia en todos lados cuando
* deber�a hacerse solo si "corresponde". Un "if retorno" al salir de la parte que valida 
* grupos.
*
 
PARAMETERS gque_val
PRIVATE gretorno, gi, guna_vez,gn, gcorresp, gcpo, gnom_grup, gclave

IF PARAMETERS() # 1
	WAIT WIND "Error al pasar los parametros de la funcion VAL_GRAB"
	RETU .F.
ELSE
	gque_val = UPPER (gque_val)
	IF gque_val = 'T' OR gque_val = 'D' OR gque_val = 'C'
		gretorno = .T.
	ELSE
		WAIT WIND "Error al pasar los parametros de la funcion VAL_GRAB"
		RETU .F.
	ENDIF
ENDIF

IF LAST () = 27
	KEYBOARD '{shift+f12}' CLEAR
	WAIT WIND ''
ENDIF

gi 				= 1
guna_vez 	= .T.
gn 				= UltCpoPant()

gtab_gra	= WONTOP()
IF gtab_gra = 'CONTROLS'
	gi       = 15
	gtab_gra = vent_ppal
ENDIF

DO WHILE (gi <= gn ) AND gretorno
	
	gcpo = SUBSTR(OBJVAR (gi),3,10)
	gcpo = PADR(ALLTRIM(UPPER(gcpo)),10)
	IF SEEK (gcpo+'V',gtab_gra)
		gnom_grup = EVAL (gtab_gra + '.nom_grup')
		gclave		= EVAL (gtab_gra + '.clave')
		IF guna_vez
			guna_vez = .F.
		ELSE
			IF !(grup_mem == UPPER(ALLTRIM(gnom_grup)))
				IF !EMPTY(grup_mem)
					gretorno = VAL_GRAL(grup_mem,.T.,.T.)
				ENDIF
			ENDIF
		ENDIF
		IF gretorno
			corresp		= .F.
			DO CASE
	
			CASE gque_val = 'T'
				corresp = .T.
	
			CASE gque_val = 'D'		
				IF gclave # 'CL ' AND gclave # 'CLU'
					corresp = .T.
				ENDIF
	
			CASE gque_val = 'C'
				IF gclave = 'CL ' OR gclave = 'CLU'
					corresp = .T.
				ENDIF

			OTHERWISE
				WAIT WIND "Error en el pedido: " +gque_val+" de VAL_GRAB"
				gretorno = .F.

			ENDCASE
		
			IF corresp
				gretorno 	= VAL_GRAL (gcpo, .T.)
*				grup_mem  = UPPER(ALLTRIM(EVAL (gtab_gra + '.nom_grup')))
				grup_mem  = UPPER(ALLTRIM(gnom_grup))
			ENDIF
		ENDIF
	ENDIF
	gi = gi + 1
ENDDO

RETURN gretorno
*

