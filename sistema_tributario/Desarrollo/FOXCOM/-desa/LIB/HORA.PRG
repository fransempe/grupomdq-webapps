**************************************************
FUNCTION TTOS
**************************************************
* Funcionamiento
*  Pasa un tiempo en formato HHMMSS a segundos
*  Si el valor de tiempo es incorrecto devuelve -1 
*
* Par metros:
*  tiempo  : (N) Tiempo en formato HHMMSS 		
*  [warn]  : (N) (0) 0 = no avisa si est  mal el tiempo ingresado
*										 1 = avisa si est  mal el tiempo ingresado
*	 [es_dia]: (N) (0) 0 = Si permite mayor cantidad de horas que tiene un dia
*										 1 = Si NO permite mayor cantidad de horas que tiene un dia
* 
**************************************************

PARAMETERS Tiempo, Warn, es_dia
PRIVATE Horas, Minutos, Segundos, CantSegundos

IF EMPTY(es_dia)
	es_dia = 0
ENDIF

IF PARAMETERS () < 2
	Warn = 0
ENDIF

IF PARAMETERS () < 1
	Tiempo = 0
ENDIF

Segundos = MOD (Tiempo, 100)
Minutos  = MOD (INT (Tiempo / 100), 100)
Horas    = INT (Tiempo / 10000)

IF Segundos < 0 .OR. Segundos > 59
	IF Warn # 0
		=msg( 'Cantidad de Segundos Incorrecta. ' +;
			ALLTRIM (STR (Segundos)) + ' segundos ')
	ENDIF
	CantSegundos = -1
ELSE
	IF Minutos < 0 .OR. Minutos > 59
		IF Warn # 0
			=msg('Cantidad de Minutos Incorrecta. ' +;
				ALLTRIM (STR (Minutos)) +	' minutos ')
		ENDIF
		CantSegundos = -1
	ELSE
		IF Horas < 0 OR (Horas > 23 AND es_dia = 1)
			IF Warn # 0
				=msg( 'Cantidad de Horas Incorrecta. ' +;
				     	ALLTRIM (STR (Horas)) +	' horas ' )
			ENDIF
			CantSegundos = -1
		ELSE
			CantSegundos = Horas * 3600 + Minutos * 60 + Segundos
		ENDIF
	ENDIF
ENDIF	

RETURN CantSegundos


**************************************************
FUNCTION STOT
**************************************************
* Funcionamiento
*  Pasa una cantidad de segundos a formato HHMMSS 
*  Si el valor de segundos es negativo devuelve 0 
*
* Par metros:
*  CantSegundos : (N) Cantidad de segundos  		
**************************************************

PARAMETERS CantSegundos
PRIVATE Horas, Minutos, Segundos, Tiempo

IF PARAMETERS () < 1
	CantSegundos = 0
ENDIF

IF CantSegundos < 0
	CantSegundos = 0
ENDIF

Segundos = MOD (CantSegundos, 60)
Minutos  = INT (MOD (CantSegundos, 3600) / 60)
Horas    = INT (CantSegundos / 3600)

Tiempo = Horas * 10000 + Minutos * 100 + Segundos

RETURN Tiempo


**************************************************
FUNCTION TTOM
**************************************************
* Funcionamiento
*  Pasa un tiempo en formato HHMM a Minutos
*  Si el valor de tiempo es incorrecto devuelve -1 
*
* Par metros:
*  tiempo  : (N) Tiempo en formato HHMM
*  [warn]  : (N) (0) 0 = no avisa si est  mal el tiempo ingresado
*										 1 = avisa si est  mal el tiempo ingresado
*	 [es_dia]: (N) (0) 0 = Si permite mayor cantidad de horas que tiene un dia
*										 1 = Si NO permite mayor cantidad de horas que tiene un dia
*
**************************************************

PARAMETERS Tiempo, Warn, es_dia
PRIVATE Horas, Minutos, CantMinutos

IF EMPTY(es_dia)
	es_dia = 0
ENDIF

IF PARAMETERS () < 2
	Warn = 0
ENDIF

IF PARAMETERS () < 1
	Tiempo = 0
ENDIF

Minutos  = MOD (Tiempo, 100)
Horas    = INT (Tiempo / 100)

IF Minutos < 0 .OR. Minutos > 59
	IF Warn # 0
		=msg( 'Cantidad de Minutos Incorrecta. ' +;
			ALLTRIM (STR (Minutos)) +	' minutos ' )
	ENDIF
	CantMinutos = -1
ELSE
	IF Horas < 0 OR (Horas > 23 AND es_dia = 1)
		IF Warn # 0
			=msg( 'Cantidad de Horas Incorrecta. ' +;
   				ALLTRIM (STR (Horas)) +	' horas ' )
		ENDIF
		CantMinutos = -1
	ELSE
		CantMinutos = Horas * 60 + Minutos
	ENDIF
ENDIF	

RETURN CantMinutos


**************************************************
FUNCTION MTOT
**************************************************
* Funcionamiento
*  Pasa una cantidad de minutos a formato HHMM
*  Si el valor de minutos es negativo devuelve 0 
*
* Par metros:
*  Cantminutos : (N) Cantidad de minutos  		
**************************************************

PARAMETERS Cantminutos
PRIVATE Horas, Minutos, Tiempo

IF PARAMETERS () < 1
	Cantminutos = 0
ENDIF

IF Cantminutos < 0
	Cantminutos = 0
ENDIF

Minutos  = MOD (Cantminutos, 60)
Horas    = INT (Cantminutos / 60)

Tiempo = Horas * 100 + Minutos

RETURN Tiempo

