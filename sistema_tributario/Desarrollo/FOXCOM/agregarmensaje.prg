PROCEDURE AgregarMensaje (cad as String, nodo as String, reg as String)
	IF NOT EMPTY(nodo)
		nodo_C	= '</' + nodo + '>'
		nodo	= '<' + nodo + '>'
	ENDIF
	IF NOT EMPTY(reg)
		reg_C	= '</' + reg + '>'
		reg		= '<' + reg + '>'
	ENDIF
	
	cad = cad + Indent(1) + nodo + _S_
	IF NOT EMPTY(M_Mensaje)
		cad = cad + Indent(2) + reg + M_Mensaje + reg_C + _S_
	ENDIF
	cad = cad + Indent(1) + nodo_C + _S_
ENDPROC