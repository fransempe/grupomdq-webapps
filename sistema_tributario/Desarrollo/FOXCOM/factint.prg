*************************************************
* FUNCTION FACTINT
*************************************************
* Autor : Leo
* Dise�o: Leo
* 
* Fecha : 16/3
* 
* Funcionamiento: Actualiza un Importe
* Par�metros: fecha de Vento
*             fecha final
*             importe origen
*             actualiza
*             interes
*
* Modificaciones: Se agrego "m." a los par�metros (RODRIGO - 25/04/95)
* 
PARAMETERS m.fecha_vto, m.fecha_final, m.importe, m.actualiza, m.interes, m.cod_recur, m.cod_conc
PRIVATE retorno, dia_o, mes_o, anio_o, dia, mes, anio, dias_m
retorno = .T.

IF SET ('DEBUG') = 'ON'
	= VerNuPar('FACTINT', PARA(), 7)
	= VerTiPar('FACTINT', 'm.fecha_vto',   'D')
	= VerTiPar('FACTINT', 'm.fecha_final', 'D')
	= VerTiPar('FACTINT', 'm.importe',     'C')
	= VerTiPar('FACTINT', 'm.actualiza',   'N')
	= VerTiPar('FACTINT', 'm.interes',     'N')
	= VerTiPar('FACTINT', 'm.cod_recur',   'N')
	= VerTiPar('FACTINT', 'm.cod_conc',    'N')
	IF TYPE ('vec_act') # 'N' OR TYPE ('vec_int') # 'N'
		=MSG ('No se configuraron los Indices de Actualizaci�n.')
	ENDIF
	IF TYPE ('anio_inic') # 'N' OR TYPE ('anio_cant') # 'N'
		=FINAL (1, 'No se configur� el a�o inicial o la cantidad inicial. Deber�a haber abortado antes el programa.')
	ENDIF
ENDIF
EXTERNAL ARRAY vec_act, vec_int

IF m.fecha_vto < m.fecha_final OR m._int_noven = 'S'
	m.dia_o = DAY   (m.fecha_vto)
	m.mes_o = MONTH (m.fecha_vto)
	m.anio_o= YEAR  (m.fecha_vto)

	IF m.anio_o < m.anio_inic
		m.anio_o = m.anio_inic
	ENDIF

	IF m.anio_o >= m.anio_inic + m.anio_cant
		m.anio_o = m.anio_inic + m.anio_cant - 1
	ENDIF

	IF m.mes_o < 1
		m.mes_o = 1
	ENDIF

	* preparo el dia 1 de este mes, mas 1 mes, menos 1 dia, para determinar la
	* cantidad de dias del mes de vencimiento
	m.dias_m=	DAY (GOMONTH (CTOD ('01/' + STR (m.mes_o, 2) + '/' + ;
                              STR (m.anio_o,4)), 1) - 1)
                       
	m.anio_o= (m.anio_o - m.anio_inic) * 12

	m.dia   = DAY   (m.fecha_final)
	m.mes   = MONTH (m.fecha_final)
	m.anio  = YEAR  (m.fecha_final)

	IF m.anio < m.anio_inic
		m.anio = m.anio_inic
	ENDIF

	IF m.anio >= m.anio_inic + m.anio_cant
		m.anio = m.anio_inic + m.anio_cant - 1
	ENDIF
	m.anio  = (m.anio - m.anio_inic) * 12

	IF m.mes < 1
		m.mes = 1
	ENDIF

        IF !m.act_d_int
          m.actualiza = CalcActual(VAL(m.importe))
        ENDIF

	m.interes = CalcInter  (VAL (m.importe) + m.actualiza)

        IF m.act_d_int
          m.actualiza = CalcActual(VAL(m.importe) + m.interes)
        ENDIF
ELSE
	m.actualiza = 0
	m.interes   = 0
ENDIF

RETURN retorno



*************************************************
FUNCTION CalcActual
*************************************************
* Autor : Leo
* Dise�o: Leo
* 
* Fecha : 16/3/95
* 
* Funcionamiento: calcula la actualizaci�n
* Par�metros: m.importe
* Modificaciones:
* 
PARAMETERS m.importe
PRIVATE m.coefic
EXTERNAL ARRAY vec_act, vec_int

IF usa_fx_act
	RETURN ROUND (FX_ACTUA (m.importe), 2)
ENDIF

m.coefic = vec_act [m.anio + m.mes - 1, 1] / vec_act [m.anio_o + m.mes_o, 1]

IF vec_act [m.anio_o + m.mes_o, 2] <> 0
	m.coefic = m.coefic * (1 + vec_act [m.anio_o + m.mes_o, 2] *;
					 (m.dias_m - m.dia_o))
ENDIF

IF vec_act [m.anio + m.mes, 2] <> 0
	m.coefic = m.coefic * (1 + vec_act [m.anio + m.mes, 2] * m.dia)
ENDIF

IF m.coefic < 1
	m.coefic = 0
ELSE
	m.coefic = m.coefic - 1
ENDIF

RETURN ROUND (m.importe * m.coefic, 2)



*************************************************
FUNCTION CalcInter
*************************************************
* Autor : Leo
* Dise�o: Leo
* 
* Fecha : 16/3/95
* 
* Funcionamiento: calcula el interes
* Par�metros: m.importe
* Modificaciones:
* 
PARAMETERS m.importe
PRIVATE m.coefic
EXTERNAL ARRAY vec_act, vec_int

IF usa_fx_int
	RETURN ROUND (FX_INTER (m.importe), 2)
ENDIF

m.coefic = 0

IF m.anio_o = m.anio AND m.mes_o = m.mes
	m.coefic = vec_int [m.anio + m.mes, 2] * (m.dia - m.dia_o)
ELSE
	m.coefic = vec_int [m.anio + m.mes - 1, 1] - vec_int [m.anio_o + m.mes_o, 1]
	
	IF vec_int [m.anio_o + m.mes_o, 2] <> 0
		m.coefic = m.coefic + vec_int [m.anio_o + m.mes_o, 2] * (m.dias_m - m.dia_o)
	ENDIF

	IF vec_int [m.anio + m.mes, 2] <> 0
		m.coefic = m.coefic + vec_int [m.anio + m.mes, 2] * m.dia
	ENDIF
ENDIF

IF m.coefic < 0
	m.coefic = 0
ENDIF

RETURN ROUND (m.importe * m.coefic, 2)


