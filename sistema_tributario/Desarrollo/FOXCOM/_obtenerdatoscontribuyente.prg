
PROCEDURE _ObtenerDatosContribuyente(pTipoImponible as String, pNroImponible as String) as String

	m.cuit1 = 0 
	m.cuit2 = 0 
	m.nombretitular = ''
	m.nombreresppago = ''
	
	
		
	=FCONTVIN(pTipoImponible, pNroImponible, .F., .F., @m.cuit1, @m.cuit2)
	* Si el imponible es un contribuyente y no tiene ninguna entrada en VINC_IMP ==> ese mismo
	*	contribuyente se toma c�mo titular titular.
	IF m.cuit1 <= 0 AND P_ArchMovSelec.tipoimponible = 'N'
		m.cuit1 = pNroImponible
	ENDIF
	
	IF m.cuit1 > 0
		IF SEEK (STR(m.cuit1,10,0),'contrib')
			m.nombretitular = contrib.nomb_cont
		ENDIF
	ENDIF
	
	IF m.cuit2 > 0
		IF SEEK (STR(m.cuit2,10,0),'contrib')
			m.nombreresppago = contrib.nomb_cont
		ENDIF
	ENDIF
	
	
	
	* Agrego la direccion del contribuyente
	mDatos = '|'
	mDatos = mDatos + ALLT(ALLT(contrib.nomb_calle) + ' ' + ALLT(contrib.puerta) + ' ' + ;
	 				  ALLT(contrib.puertabis) + ' ' + ALLT(contrib.piso) + ' ' + ;
					  ALLT(contrib.depto)) + '|'

	* Agrego la localidad del contribuyente
	mDatos = mDatos + IIF(NOT EMPTY(contrib.cod_post),'(' + ALLT(contrib.cod_post) + ') ','') + ALLT(contrib.nomb_loc) + '|'
	

	
	RETURN TRIM(mDatos)
ENDPROC



	 

	 