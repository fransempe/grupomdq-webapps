
DEFINE CLASS recurweb as Session OLEPUBLIC

	***********************************************************************************
	*	Constructor
	***********************************************************************************
	PROCEDURE Init as Boolean
		PUBLIC AmbienteOK,TimeStart
		
		TimeStart = SECONDS()
		AmbienteOK = AmbienteWeb()
		
		RETURN .T.
	ENDPROC
	***********************************************************************************
	***********************************************************************************
		
	
	
	
	***********************************************************************************
	*	ConsultarDeuda
	***********************************************************************************
	PROCEDURE ConsultarDeuda (TipoImp as String, NroImp as double) as String
		PRIVATE retorno
		PRIVATE mEstadoParametros as String
		
		AuxArchDeud = '<?xml version="1.0" encoding="UTF-8"?>' + _S_
		AuxArchDeud = AuxArchDeud + '<VFPDATA>' + _S_
		 
		IF (AmbienteOK) THEN
		
			EfimuniWeb()
		
			* Verifico que todos los parametros esten OK
			mEstadoParametros = _VerificarParametros(.F.)
			IF (ALLTRIM(mEstadoParametros) = 'OK') THEN
		
										
				* BEGIN Apertura de tablas
				=UseT('recur\pla_imp')
				SELECT pla_imp
				SCATTER MEMVAR BLANK
				
				=UseT('recur\codifics')
				SELECT codifics
				SCATTER MEMVAR BLANK
				
				=UseT('recur\concs_cc')
				SELECT concs_cc
				SCATTER MEMVAR BLANK
				
				=UseT('recur\rec_cc')
				SELECT rec_cc
				SCATTER MEMVAR BLANK
				
				=UseT('recur\rec_ccc')
				SELECT rec_ccc
				SCATTER MEMVAR BLANK
				
				=UseT('recur\rec_fec')
				SELECT rec_fec
				SCATTER MEMVAR BLANK
				
				=UseT('recur\comprob')
				SELECT comprob
				SCATTER MEMVAR BLANK
			
				=UseT('recur\comp_ren')
				SELECT comp_ren   				       
				SCATTER MEMVAR BLANK
				
				=UseT('recur\com_ddjj')
				SELECT com_ddjj
				SCATTER MEMVAR BLANK
				
				
				IF _ObtenerCodigoMunicipalidad() = '006'
				  =UseT('recur\no_inter')
				  SELECT no_inter
				  SCATTER MEMVAR BLANK
				  
				  =UseT('recur\int_auto')
				  SELECT int_auto
				  SCATTER MEMVAR BLANK
                ENDIF
                
                IF _ObtenerCodigoMunicipalidad() = '038'
                
				  =UseT('recur\VINC_INT')
				  SELECT vinc_int
				  SCATTER MEMVAR BLANK
             
                ENDIF
                
				* Creaci�n de cursores
				cre_cursores()
				

				_ConsultarDeuda (@AuxArchDeud, TipoImp, NroImp, AmbienteOK)
				AgregarTelefonoYMail(@AuxArchDeud)				
			ELSE
				RETURN ALLTRIM(mEstadoParametros)
			ENDIF			
		ENDIF
		
		
		
		AuxArchDeud = AuxArchDeud + Indent(1) + '<NOTAS>' + _S_
		AgregarMensaje (@AuxArchDeud,'REGISTRO_NOTAS','MENSAJE')
		AuxArchDeud = AuxArchDeud + Indent(1) + '</NOTAS>' + _S_
		
		AgregarTiempo (@AuxArchDeud)
		
		AuxArchDeud = AuxArchDeud + '</VFPDATA>' + _S_
		
		RETURN STRCONV(AuxArchDeud,_STRCONV_)		
	ENDPROC
	***********************************************************************************
	***********************************************************************************
	
	
	
	
	***********************************************************************************
	*	EmitirCompPagoNoVenc
	***********************************************************************************
	PROCEDURE EmitirCompPagoNoVenc (XMLGrupoNroComp as String) as String
		PRIVATE retorno
		PRIVATE mEstadoParametros as String
		
		
		AuxArchCompNoVenc = '<?xml version="1.0" encoding="UTF-8"?>' + _S_
		AuxArchCompNoVenc = AuxArchCompNoVenc + '<VFPDATA>' + _S_
		
		IF (AmbienteOK) THEN
		
			* Verifico que todos los parametros esten OK
			mEstadoParametros = _VerificarParametros(.F.)
			IF (ALLTRIM(mEstadoParametros) = 'OK') THEN

		
				EfimuniWeb()
			
				* BEGIN Apertura de tablas
				=UseT('recur\concs_cc')
				SELECT concs_cc
				SCATTER MEMVAR BLANK
				=UseT('recur\rec_cc')
				SELECT rec_cc
				SCATTER MEMVAR BLANK
				=UseT('recur\rec_fec')
				SELECT rec_fec
				SCATTER MEMVAR BLANK
				=UseT('recur\comprob')
				SELECT comprob
				SCATTER MEMVAR BLANK
				=UseT('recur\comp_ren')
				SELECT comp_ren
				SCATTER MEMVAR BLANK
				=UseT('recur\pla_imp')
				SELECT pla_imp
				SCATTER MEMVAR BLANK
				
				_EmitirCompPagoNoVenc (@AuxArchCompNoVenc, XMLGrupoNroComp, AmbienteOK)
				AgregarTelefonoYMail(@AuxArchCompNoVenc)
			ELSE
				RETURN ALLTRIM(mEstadoParametros)
			ENDIF
		ENDIF
		
		
		AgregarMensaje (@AuxArchCompNoVenc,'ERROR','MENSERROR')		
		AgregarTiempo (@AuxArchCompNoVenc)		
		AuxArchCompNoVenc = AuxArchCompNoVenc + '</VFPDATA>' + _S_

		RETURN STRCONV(AuxArchCompNoVenc,_STRCONV_)
	ENDPROC		
	***********************************************************************************
	***********************************************************************************
	
	
	
	
	
	***********************************************************************************
	*	EmitirCompPagoDeudaVenc
	***********************************************************************************
	PROCEDURE EmitircompPagoDeudaVenc (XMLDeudaVenc as String) as String
		PRIVATE mEstadoParametros as String
				
				
		AuxArchComp = '<?xml version="1.0" encoding="UTF-8"?>' + _S_
		AuxArchComp = AuxArchComp + '<VFPDATA>' + _S_
		
		IF (AmbienteOK) THEN
		
			* Verifico que todos los parametros esten OK
			mEstadoParametros = _VerificarParametros(.F.)
			IF (ALLTRIM(mEstadoParametros) = 'OK') THEN
		
				EfimuniWeb()

				* BEGIN Apertura de tablas
				=UseT('recur\numerad')		
				=UseT('recur\comprob')
				SELECT comprob
				SCATTER MEMVAR BLANK
				=UseT('recur\comp_ren')
				SELECT comp_ren
				SCATTER MEMVAR BLANK
				=UseT('recur\rec_cc')
				SELECT rec_cc
				SCATTER MEMVAR BLANK
				=UseT('recur\rec_fec')
				SELECT rec_fec
				SCATTER MEMVAR BLANK
				=UseT('recur\concs_cc')
				SELECT concs_cc
				SCATTER MEMVAR BLANK
				=UseT('recur\pla_imp')
				SELECT pla_imp
				SCATTER MEMVAR BLANK
				
				
				* DATOS REPLICADOS
				IF ALLTRIM(_DATOSREPLICADOS) = 'S'
					=UseT('recur\numeradw')
					SELECT numeradw
					SCATTER MEMVAR BLANK
				
					=UseT('recur\comprobw')
					SELECT comprobw
					SCATTER MEMVAR BLANK

					=UseT('recur\comp_rew')
					SELECT comp_rew
					SCATTER MEMVAR BLANK
				ENDIF
				
				 			 
				_EmitircompPagoDeudaVenc (@AuxArchComp, XMLDeudaVenc, AmbienteOK)
				AgregarTelefonoYMail (@AuxArchComp)
			ELSE
				RETURN ALLTRIM(mEstadoParametros)
			ENDIF
		ENDIF
		
		
		AgregarMensaje (@AuxArchComp,'ERROR','MENSERROR')
		AgregarTiempo (@AuxArchComp)		
		AuxArchComp = AuxArchComp + '</VFPDATA>' + _S_

		RETURN STRCONV(AuxArchComp,_STRCONV_)
	ENDPROC
	***********************************************************************************
	***********************************************************************************
	
	
	
	
	
	***********************************************************************************
	*	ObtenerTelefonoYMail
	***********************************************************************************
	PROCEDURE ObtenerTelefonoYMail() as String
		PRIVATE mEstadoParametros as String
		AuxTodoOK = AmbienteOK
		
		
		* Verifico que todos los parametros esten OK
		mEstadoParametros = _VerificarParametros(.F.)
		IF (ALLTRIM(mEstadoParametros) = 'OK') THEN
		
			AuxArchComp = '<?xml version="1.0" encoding="UTF-8"?>' + _S_
			AuxArchComp = AuxArchComp + '<VFPDATA>' + _S_
			
			IF AuxTodoOK
				AgregarTelefonoYMail(@AuxArchComp)
			ENDIF
			
			AgregarMensaje (@AuxArchComp,'ERROR','MENSERROR')
			AgregarTiempo (@AuxArchComp)			
			AuxArchComp = AuxArchComp + '</VFPDATA>' + _S_
			
			RETURN STRCONV(AuxArchComp,_STRCONV_)
		ELSE
			RETURN ALLTRIM(mEstadoParametros)
		ENDIF
	ENDPROC
	***********************************************************************************	
	***********************************************************************************

	
	
	
	
	***********************************************************************************
	*	Lee el archivo ini y devuelve la ruta de directorios para acceder a los archivos
	***********************************************************************************
	PROCEDURE VerRuta() as String
		PRIVATE mRuta as String
		mRuta = obtener_path()		
		RETURN ALLTRIM(mRuta)
	ENDPROC
	***********************************************************************************
	***********************************************************************************		
	
	
	
	
	***********************************************************************************
	*	Obtiene un N�mero de Cuenta en Base a un Dominio
	***********************************************************************************
	PROCEDURE ObtenerNroCuentaPorPatente(pDominio as String) as String
		PRIVATE mDato as String
		
		EfimuniWeb()
		
		mDato = ''
		mDato = FLeeDom(ALLTRIM(pDominio))
		RETURN ALLTRIM(mDato)
	ENDPROC
	***********************************************************************************
	***********************************************************************************
	
	
	
	
	**********************************************************************************
	*	Obtengo los datos del municipio
	***********************************************************************************
	PROCEDURE ObtenerDatosMunicipalidad() as String
		PRIVATE mXML as String 
		PRIVATE mEstadoParametros as String
		
		* Verifico que todos los parametros esten OK
		mEstadoParametros = _VerificarParametros(.F.)
		IF (ALLTRIM(mEstadoParametros) = 'OK') THEN		
			mXML = _ObtenerDatosMunicipalidad()
			RETURN ALLTRIM(mXML)
		ELSE
			RETURN 	ALLTRIM(mEstadoParametros)
		ENDIF
	ENDPROC
	***********************************************************************************
	***********************************************************************************


	
	
	**********************************************************************************
	*	Obtengo el codigo de municipalidad para generar el barcode
	***********************************************************************************
	*PROCEDURE ObtenerCodigoMunicipalidad as String
	*	PRIVATE mCodigo as String 
	*	mCodigo = _ObtenerCodigoMunicipalidad()
	*	RETURN TRIM(mCodigo)
	*ENDPROC
	***********************************************************************************
	***********************************************************************************		
	
	
	
	
	**********************************************************************************
	*	Obtengo los datos del contribuyente para el listado de deuda
	***********************************************************************************
	PROCEDURE ObtenerDatosContribuyente(pTipoImponible as String, pNroImponible as String) as String
		PRIVATE mDatos as String 
		EfimuniWeb()
		
		
		mDatos = _ObtenerDatosContribuyente(ALLTRIM(pTipoImponible), ALLTRIM(pNroImponible))
		RETURN ALLTRIM(mDatos)
	ENDPROC
	
	
	***********************************************************************************
	* Verifico que existan y esten definidos todos los parametros antes de iniciar 
	* cualquier operacion en el sistema
	***********************************************************************************
	PROCEDURE VerificarParametros(pVerParametros as Boolean) as String
		PRIVATE mXML as String
		
		EfimuniWeb()
		mXML = _VerificarParametros(pVerParametros)
		
		RETURN ALLTRIM(mXML)	
	ENDPROC
	***********************************************************************************
	***********************************************************************************
	
	***********************************************************************************
	*01/07/2015 Este m�todo devuelve la cadena BLOQ o NOBLOQ si un imponible de 
	* los comprobantes de la Web esta BLOQUEADO en la tabla IMPOBLOQ (Brandsen)
	***********************************************************************************
	
	PROCEDURE ObtenerImponibleBloqueado(pTipoImponible as String, pNroImponible as Number) as String		 		
		PRIVATE AuxEstado
		EfimuniWeb()
		AuxEstado = _ObtenerImponibleBloqueado(pTipoImponible, pNroImponible)		   		   
		RETURN ALLTRIM(AuxEstado)
	ENDPROC	
		
	***********************************************************************************
	***********************************************************************************
	
		
	***********************************************************************************
	* Verifico que existan y esten definidos todos los parametros antes de iniciar 
	* cualquier operacion en el sistema
	***********************************************************************************
	PROCEDURE ObtenerFechaActuaWeb() as String		
		PRIVATE mXMLDate as Date
		
		EfimuniWeb()
		mXMLDate = _ObtenerFechaVTOIntereses()				
		
		RETURN ALLTRIM(DTOC(mXMLDate))
	ENDPROC
	***********************************************************************************
	***********************************************************************************
		
		
				
		
		
	***********************************************************************************
	* Verifico que existan y esten definidos todos los parametros antes de iniciar 
	* cualquier operacion en el sistema
	***********************************************************************************
	PROCEDURE ObtenerFechaVTOWebCalculada() as String		
		PRIVATE mXMLDate as Date
		
		EfimuniWeb()
		mXMLDate = _ObtenerFechaVTOWebCalculada()				
				
		RETURN ALLTRIM(DTOC(mXMLDate))
	ENDPROC
	***********************************************************************************
	***********************************************************************************
	
	
	***********************************************************************************
	* Obtener la informaci�n de recursos en rec_inf
	* 
	***********************************************************************************
	PROCEDURE obtRecursosConPagoElect() as String		
		PRIVATE mString as STRING
		EfimuniWeb()
		mString = _obtRecursosConPagoElect()				
				
		RETURN(mString)
	ENDPROC
	***********************************************************************************
	***********************************************************************************
	

	
		
	***********************************************************************************
	* Verifico que existan y esten definidos todos los parametros antes de iniciar 
	* cualquier operacion en el sistema
	***********************************************************************************
	PROCEDURE createLog(p_sPath as String, p_sMensaje as String) as String		
		RETURN _CreateLog((p_sPath), ALLTRIM(p_sMensaje))		
	ENDPROC
	***********************************************************************************
	***********************************************************************************
	
	

	
		
	***********************************************************************************
	* Guardo los datos de la visita para despues poder mostrarlos y usar un contador
	***********************************************************************************
	PROCEDURE agregarVisita(p_sFecha as String, p_sHora as String, p_sTipoImponible as String, p_nNroImponible as Number) as String
		RETURN _agregarVisita(ALLTRIM(p_sFecha), ALLTRIM(p_sHora), ALLTRIM(p_sTipoImponible), p_nNroImponible)
	ENDPROC
	***********************************************************************************
	***********************************************************************************
	
	
	
	

	***********************************************************************************
	* Guardo los datos de la visita para despues poder mostrarlos y usar un contador
	***********************************************************************************
	PROCEDURE actualizarVisita(p_sFecha as String, p_sHora as String, p_sTipoImponible as String, p_nNroImponible as Number) as String
		RETURN _actualizarVisita(ALLTRIM(p_sFecha), ALLTRIM(p_sHora), ALLTRIM(p_sTipoImponible), p_nNroImponible)
	ENDPROC
	***********************************************************************************
	***********************************************************************************
	
	
	
	
	
	***********************************************************************************
	* Obtengo la cantidad de visitas totales realizadas
	***********************************************************************************
	PROCEDURE getVisitasCantidad() as String
		RETURN _getVisitasCantidad()
	ENDPROC
	***********************************************************************************
	***********************************************************************************
	
	
	
	
	
	***********************************************************************************
	* Obtengo la cantidad de visitas discriminado por imponible y si solo consulto o
	* consulto e imprimio
	***********************************************************************************
	PROCEDURE getVisitasCantidadDetalle(p_sFechaDesde as String, p_sFechaHasta as String) as String
		RETURN _getVisitasCantidadDetalle(ALLTRIM(p_sFechaDesde), ALLTRIM(p_sFechaHasta))
	ENDPROC
	***********************************************************************************
	***********************************************************************************
	
	
	
	***********************************************************************************
	* Este m�todo devuelve el recurso que se debe excluir de la impresi�n de 
	* los comprobantes de la Web
	***********************************************************************************
	PROCEDURE getParametroExcluirRecurso() as String		 		
		EfimuniWeb()
		RETURN _getParametroExcluirRecurso()		   		   
	ENDPROC
	***********************************************************************************
	***********************************************************************************
	
	***********************************************************************************
	* Este m�todo devuelve la cadena de los tipos de imponible de 
	* los comprobantes de la Web
	***********************************************************************************
	
	PROCEDURE getObtenerTiposImponible() as String		 		
		EfimuniWeb()
		RETURN _getObtenerTiposImponible()		   		   
	ENDPROC	
		
	***********************************************************************************
	
	
	***********************************************************************************
	* 28/06/2016 Ca�uelas: Este m�todo devuelve el parametro para mostrar u ocultar el codigo o la descripcion
	* de los comprobantes.
	***********************************************************************************
	***********************************************************************************
	
	PROCEDURE getParametroDescConc() as String		 		
		EfimuniWeb()
		RETURN _getParametroDescConc()		   		   
	ENDPROC	
	
	
	
	***********************************************************************************
	* Este m�todo devuelve TRUE o FALSE indicando si se utiliza la opci�n de
	* loguearse con un usuario admin
	***********************************************************************************
	PROCEDURE getUsaLoginAdmin() as String
		RETURN _getUsaLoginAdmin()
	ENDPROC
	***********************************************************************************
	
	***********************************************************************************
	*Este metodo devuelve S o N si imprime o no el codigo de barra para COMERCIO DE Z�RATE
	*Francisco 12/06/2017
	PROCEDURE getParametroImprimeCodBar() as String		 		
		EfimuniWeb()
		RETURN _getParametroImprimeCodBar()		   		   
	ENDPROC			
	
	***********************************************************************************
	*	Limpiar memoria
	***********************************************************************************
	PROCEDURE BorrarTodo
		CLOSE ALL
		CLEAR MEMORY
	ENDPROC
	***********************************************************************************
	***********************************************************************************	
	
	
	
	***********************************************************************************
	*	Destructor Impl�cito
	***********************************************************************************
	PROCEDURE Destroy
		CLOSE ALL
		CLEAR MEMORY
	ENDPROC
	***********************************************************************************
	***********************************************************************************
	
ENDDEFINE