***********************************************************************************
*	Método _getObtenerTiposImponible:  
***********************************************************************************
* 	Date:          11/09/2014
* 	Author:        Francisco	 				
* 	Design:        Francisco
***********************************************************************************
*	Funcionamiento:
*		Busca en la tabla de configuración del sistema el parámetro que indica cuales son
*		los Tipos de Imponible que se van a listar en los comprobantes Web
***********************************************************************************

PROCEDURE _getObtenerTiposImponible() as String
	PRIVATE mTipos as String
	PRIVATE mIndex as String
		
	IF (!USED ('config')) THEN
		=USET ('asa\config')
	ENDIF
	SELECT config
	SET ORDER TO Primario
	
		
	mTipos = ''
	mIndex = PADR('RECUR', 8, ' ') + 'WEB_TIPOSIMP'
	IF (SEEK(mIndex, 'config')) THEN
		mTipos = ALLTRIM(config.cont_par)		
	ENDIF
	
	RETURN mTipos
ENDPROC