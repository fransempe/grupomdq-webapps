**********************************************************
*FUNCTION SetTipo
**********************************************************
*Fx. para Junin de los Andes esqueleto de Prep. de L�quidos
PRIVATE retorno
m.retorno = ' '

IF !USED('alega')
	=USET ('RECHUM\legajos','primario','alega')
ENDIF
*IF SEEK(STR(cursliq.nro_legajo,6,0),'alega')
IF DBSEEK([STR(cursliq.nro_legajo,6,0)],"alega")
	DO CASE
		CASE alega.tipo_docum = '0'
			m.retorno = 'NI'
		CASE alega.tipo_docum = '1'
			m.retorno = 'DN'
		CASE alega.tipo_docum = '2'
			m.retorno = 'CI'
		CASE alega.tipo_docum = '3'
			m.retorno = 'LC'
		CASE alega.tipo_docum = '4'
			m.retorno = 'LE'
	ENDCASE

ENDIF

RETURN m.retorno