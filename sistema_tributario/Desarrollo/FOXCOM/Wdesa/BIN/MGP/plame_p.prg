LPARAMETERS p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13
LOCAL nParam, sRet, sAux, sTran

nParam = PCOUNT()

IF nParam = 0
	sRet = "          Enero        Febrero          Marzo          Abril           Mayo          Junio          Julio         Agosto     Septiembre        Octubre      Noviembre      Diciembre Tot. x partida"
ELSE
	sRet = ""
	FOR nParam = 1 TO PCOUNT()
		sAux = LTRIM(STR(nParam,2,0))
		sRet = sRet + " " + TRANSFORM(p&sAux, "@R 999,999,999.99")
	ENDFOR
ENDIF

RETURN sRet