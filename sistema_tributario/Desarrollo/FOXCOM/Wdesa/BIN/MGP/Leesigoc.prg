*************************************************
* FUNCTION LeeSigOC
*************************************************
* 
* Funcionamiento:
* 
* Se utiliza desde el reporte de la O.C.
* La finalidad es de cargar un vector de N posiciones, con las
* N imputaciones siguientes desde la �ltima impresa.
* 
PARAMETERS cantidad
PRIVATE retorno
retorno = ''

IF Type ('finali[1]') != 'C'
	DIMENSION finali[m.cantidad]
	DIMENSION funcit[m.cantidad]
	DIMENSION rubnom[m.cantidad]
	DIMENSION restnom[m.cantidad]
	DIMENSION nro_prog[m.cantidad]
	DIMENSION activi[m.cantidad]
	DIMENSION unieje[m.cantidad]
	DIMENSION descrip[m.cantidad]
	DIMENSION imping[m.cantidad]
ENDIF

IF ALen (finali) < m.cantidad
	DIMENSION finali[m.cantidad]
	DIMENSION funcit[m.cantidad]
	DIMENSION rubnom[m.cantidad]
	DIMENSION restnom[m.cantidad]
	DIMENSION nro_prog[m.cantidad]
	DIMENSION activi[m.cantidad]
	DIMENSION unieje[m.cantidad]
	DIMENSION descrip[m.cantidad]
	DIMENSION imping[m.cantidad]
ENDIF

FOR m.i = 1 TO m.cantidad
	finali[m.i] = ''
	funcit[m.i] = ''
	rubnom[m.i] = ''
	restnom[m.i] = ''
	nro_prog[m.i] = ''
	activi[m.i] = ''
	unieje[m.i] = ''
	descrip[m.i] = ''
	imping[m.i] = 0
ENDFOR

m.i = 1
DO WHILE m.i <= m.cantidad AND !Eof ('c_imp_oc')
	IF !EMPTY(c_imp_oc.imp_ing)
		finali[m.i] = c_imp_oc.finalidad
		funcit[m.i] = c_imp_oc.func_item
		rubnom[m.i] = c_imp_oc.rubro_nom
		restnom[m.i] = c_imp_oc.rest_nom
		nro_prog[m.i] = c_imp_oc.program
		activi[m.i] = c_imp_oc.actividad
		unieje[m.i] = c_imp_oc.uni_ejec
		IF SEEK(STR(c_imp_oc.finalidad,1,0)+STR(c_imp_oc.func_item, 2,0)+c_imp_oc.rubro_nom+c_imp_oc.rest_nom+STR(c_imp_oc.program,3,0)+STR(c_imp_oc.actividad,2,0)+STR(c_imp_oc.uni_ejec,9,0), "presupu")
			descrip[m.i] = presupu.nabr_ctap
		ELSE
			descrip[m.i] = ""
		ENDIF
		imping[m.i] = c_imp_oc.imp_ing
		m.i = m.i + 1
	ENDIF	
	SKIP IN c_imp_oc
ENDDO

RETURN retorno