
FUNCTION R4052Fil
*
* Función que determina si se debe retener
* por Resolución General 4052 de la AFIP 
* Regimen del SUSS para Empresas Constructoras

PRIVATE retorno

IF  type ('proveed.ret_rg4052') = "C" AND proveed.ret_rg4052 = "S"
	retorno = .T.
ELSE
	retorno = .F.
ENDIF

RETURN retorno

