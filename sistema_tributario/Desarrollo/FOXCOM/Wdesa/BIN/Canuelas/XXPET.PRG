* funcion XXPET    SOLO PETION
*
* FUNCIONAMIENTO: Lee un imponible en la tabla maestra correspondiente
*
* Parametros: m.tipo_imp (C): tipo de imponible
*             m.nro_imp  (N): nro de imponible
* 
PARAMETER m.tipo_imp, m.nro_imp
PRIVATE m.retorno, m.area_act

m.retorno  = .T.
m.area_act = SELECT()

IF m.tipo_imp='I'
   SELECT INMUEBLE
ELSE
   IF m.tipo_imp='C'
       SELECT COMERCIO
   ELSE
      IF m.tipo_imp='N'
           SELECT CONTRIB
      ELSE
          IF m.tipo_imp='O'
             SELECT CEMENT
          ELSE
             m.retorno=.F.
          ENDIF          
      ENDIF
   ENDIF
ENDIF

IF m.retorno
   m.retorno=SEEK(STR(m.nro_imp,10,0))
ENDIF
M.QUE=0
IF M.RETORNO
    DO CASE
       ** 2 B, MANZANAS 1 A 10
       ***********************
	   CASE INMUEBLE.CIRCUN=2  .AND. (ALLT(INMUEBLE.SECCION)="B" or ALLT(INMUEBLE.SECCION)="b")
		   m.QUE=INMUEBLE.NUMMAZNA
		   IF m.QUE>=1 AND m.QUE<=10
	   	 	    M.RETORNO=.T.
	   	   ENDIF
	   	   
	   ** 2 B, FRACIONES 1 A 4	   
	   ***********************
	   CASE INMUEBLE.CIRCUN=2  .AND. (ALLT(INMUEBLE.SECCION)="B" or ALLT(INMUEBLE.SECCION)="b")
		   m.QUE=INMUEBLE.NUMFRAC
		   IF m.QUE>=1 AND m.QUE<=4
	   	 	    M.RETORNO=.T.
	   	   ENDIF

       ** 2 B, MANZANAS 22 A 26, 37 A 41, 52 A 56, 70, 71, 85, 86, 100, 101,115,116
       ****************************************************************************
	   CASE INMUEBLE.CIRCUN=2  .AND. (ALLT(INMUEBLE.SECCION)="B" or ALLT(INMUEBLE.SECCION)="b")
		   m.QUE=INMUEBLE.NUMMAZNA
		   IF (m.QUE>=22 AND m.QUE<=26) OR (m.QUE>=37 AND m.QUE<=41) OR (m.QUE>=52 AND m.QUE<=56) OR m.QUE=70 OR m.QUE=71 OR m.QUE=85 OR m.QUE=86 OR m.QUE=100 OR m.QUE=101 OR m.QUE=115 OR m.QUE=116
	   	 	    M.RETORNO=.T.
	   	   ENDIF
	   	   
	   OTHERWISE
   		   M.RETORNO=.F.
	ENDCASE
ENDIF      
SELECT (m.area_act)

return m.retorno

