PARAMETER lafecha
PRIVATE m.fecha, m.dia, m.mes, m.anio

m.fecha=dtoc(lafecha)
m.dia =substr(m.fecha,1,2)
m.mes =substr(m.fecha,4,2)
m.anio=substr(m.fecha,9,2)
m.fecha=m.dia+"/"+m.mes+"/"+m.anio
return m.fecha

