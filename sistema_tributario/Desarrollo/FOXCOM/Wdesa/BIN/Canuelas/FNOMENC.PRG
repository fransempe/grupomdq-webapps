* Funci�n FNOMENC
*
* Eval�a la nomenclatura catastral de un inmueble
*
* Esta funci�n debi� desarrollarse debido a que FOX no permite evaluar
* macros con m�s de 255 caracteres.
* Esta funci�n debe invocarse para obtener la 'Identificaci�n' de los
* inmuebles. Por lo tanto, el contenido del campo EXP_IDENT de la tabla
* TIPS_IMP (Tipos de Imponibles) debe ser: FNomenc()
*

private m.nomenc1, m.nomenc2
m.nomenc1 = STR(inmueble.CIRCUN,2,0)+'-'+inmueble.SECCION+'-'+STR(inmueble.NUMCHACRA,4,0)+'-'+inmueble.LETCHACRA+'-'+STR(inmueble.NUMQUINTA,4,0)+'-'+inmueble.LETQUINTA+'-'+STR(inmueble.NUMFRAC,4,0)+'-'
m.nomenc2 = inmueble.LETFRAC+'-'+STR(inmueble.NUMMAZNA,4,0)+'-'+inmueble.LETMAZNA+'-'+STR(inmueble.NUMPARC,4,0)+'-'+inmueble.LETPARC+'-'+inmueble.SUBPARC
return m.nomenc1+m.nomenc2

