*FUNCTION L_CL
* FUNCIONAMIENTO: Lee SI TIENE CONCEPTO SG-70 DE CLOACAS
*

PRIVATE m.retorno, m.area_act, reg
*PARAMETER N_IMP

m.retorno  = .T.
m.area_act = SELECT()
IF !USED ('CONC_IMP')
          =USET ('RECUR\CONC_IMP')
ENDIF
SELECT CONC_IMP
SET ORDER TO PRIMARIO IN CONC_IMP
*reg=recno()

IF m.retorno
   m.retorno=SEEK("I"+STR(dev_comp.nro_imp,10,0)+"SG"+STR(0,2,0)+STR(70,4,0))
ENDIF   

*goto reg
if m.retorno
   m.retorno="S"
else
   m.retorno="N"
endif      
SELECT (m.area_act)
return m.retorno

