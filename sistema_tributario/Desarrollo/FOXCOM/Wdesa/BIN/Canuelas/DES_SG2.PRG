*FUNCTION DES_SG2
* FUNCIONAMIENTO: Lee SI TIENE CONCEPTO SG-22 de descuento 50 % casares
*                                       SG-23 de descuento 50% jubilados
*                                       SG-24 de descuento 50% maximo paz 


PRIVATE m.retorno, m.area_act, reg, XANIO, XCUOTA
*PARAMETER N_IMP

m.retorno  = .T.
m.area_act = SELECT()
SELECT DEV_CD
SET ORDER TO 1
GO TOP
XANIO=DEV_CD.ANIO
XCUOTA=DEV_CD.CUOTA
*reg=recno()

* concepto 22 casares
m.retorno=SEEK(dev_comp.remesa+dev_comp.tanda+"I"+STR(dev_comp.nro_imp,10,0)+str(grupo_conc,2,0)+"SG"+str(xanio,4,0)+str(xcuota,3,0)+"NORMA"+STR(22,4,0))
IF m.retorno=.t.  and  dev_cd.imp_cd<0
     return "50% D311/01"
endif

* concepto 23 jubilados
m.retorno=SEEK(dev_comp.remesa+dev_comp.tanda+"I"+STR(dev_comp.nro_imp,10,0)+str(grupo_conc,2,0)+"SG"+str(xanio,4,0)+str(xcuota,3,0)+"NORMA"+STR(23,4,0))
IF m.retorno=.t.  and  dev_cd.imp_cd<0
     return "50% D498/01"
endif

* concepto 24 maximo paz
m.retorno=SEEK(dev_comp.remesa+dev_comp.tanda+"I"+STR(dev_comp.nro_imp,10,0)+str(grupo_conc,2,0)+"SG"+str(xanio,4,0)+str(xcuota,3,0)+"NORMA"+STR(24,4,0))
IF m.retorno=.t.  and  dev_cd.imp_cd<0
     return "50% D312/01"
endif

*goto reg
SELECT (m.area_act)
return " "

