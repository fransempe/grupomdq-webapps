* funcion CUO_PP
*
* FUNCIONAMIENTO: LEE LAS CUOTAS DEL PLAN DE PAGO GENERADO LAS CUENTA Y LAS SUMA
*
* Parametros: m.tipo_imp (C): tipo de imponible
*             m.nro_imp  (N): nro de imponible
* 
*PARAMETER m.tipo_imp, m.nro_imp
PRIVATE m.retorno, m.area_act


m.retorno  = .T.
m.area_act = SELECT()
IF !USED ('FERDETA')
          =USET ('RECUR\FERDETA')
ENDIF

IF !USED ('PLA_DES')
          =USET ('RECUR\PLA_DES')
ENDIF
SELECT PLA_DES
SET ORDER TO PRIMARIO IN PLA_DES

*IF m.retorno
*   m.retorno=SEEK(M.TIPO_IMP+STR(M.NRO_IMP,10,0)+STR(m.nro_plan,3,0)+PLA_CONF.RECURSO+STR(0,4,0)+STR(1,3,0)+STR(1,3,0))
*ENDIF    LO SAQUE 21/03/00 Y PUSE LO DE ABAJO

IF m.retorno
   m.retorno=SEEK(M.TIPO_IMP+STR(M.NRO_IMP,10,0)+STR(m.nro_plan,3,0)+PLA_CONF.RECURSO+STR(0,4,0)+STR(1,3,0)+STR(1,3,0))
   if m.retorno=.f.
       m.retorno=SEEK(M.TIPO_IMP+STR(M.NRO_IMP,10,0)+STR(m.nro_plan,3,0)+PLA_CONF.RECURSO+STR(0,4,0)+STR(1,3,0)+STR(2,3,0))   
       if m.retorno=.f.
             m.retorno=SEEK(M.TIPO_IMP+STR(M.NRO_IMP,10,0)+STR(m.nro_plan,3,0)+PLA_CONF.RECURSO+STR(0,4,0)+STR(1,3,0)+STR(3,3,0))   
             if m.retorno=.f.
                  m.retorno=SEEK(M.TIPO_IMP+STR(M.NRO_IMP,10,0)+STR(m.nro_plan,3,0)+PLA_CONF.RECURSO+STR(0,4,0)+STR(1,3,0)+STR(4,3,0))   
                  **
                  IF m.retorno=.f.
                      m.retorno=SEEK(M.TIPO_IMP+STR(M.NRO_IMP,10,0)+STR(m.nro_plan,3,0)+PLA_CONF.RECURSO+STR(0,4,0)+STR(1,3,0)+STR(5,3,0))   
                      IF m.retorno=.f.
                          m.retorno=SEEK(M.TIPO_IMP+STR(M.NRO_IMP,10,0)+STR(m.nro_plan,3,0)+PLA_CONF.RECURSO+STR(0,4,0)+STR(1,3,0)+STR(6,3,0))   
                      ENDIF
                  ENDIF    
                  **
             ENDIF
       ENDIF
   ENDIF
ENDIF   


M.XXCUOTA=0
IF M.RETORNO
   m.xxcuota=0
   m.nuevotexto=""
   select FERDETA
   REPLACE XXTEXTO WITH space(10)
   DO WHILE NOT EOF()         
         IF M.TIPO_IMP=PLA_DES.TIPO_IMP  AND M.NRO_IMP=PLA_DES.NRO_IMP AND M.NRO_PLAN=PLA_DES.NRO_PLAN AND PLA_CONF.RECURSO=PLA_DES.RECURSO .AND. PLA_DES.ANIO=0
             IF PLA_DES.CUOTA<10
	             *m.nuevotexto="Cuota "+allt(str(pla_des.cuota,2,0))+" -  Vencimiento:     "+dtoc(pla_des.fecven_mov)+"       Importe $: "+allt(str(pla_des.imp_cuopp,15,2))
	             m.nuevotexto=allt(str(pla_des.cuota,2,0))+space(18)+dtoc(pla_des.fecven_mov)+space(15)+allt(str(pla_des.imp_cuopp,15,2))	             
	         ELSE
	             *m.nuevotexto="Cuota "+allt(str(pla_des.cuota,2,0))+"-  Vencimiento:     "+dtoc(pla_des.fecven_mov)+"       Importe $: "+allt(str(pla_des.imp_cuopp,15,2))
	             m.nuevotexto=allt(str(pla_des.cuota,2,0))+space(17)+dtoc(pla_des.fecven_mov)+space(15)+allt(str(pla_des.imp_cuopp,15,2))	             	             
	         ENDIF    
             select FERDETA
             replace xxtexto with xxtexto+chr(13)+m.nuevotexto
             select pla_des
         ENDIF
         SKIP
   ENDDO      
   select FERDETA
   *replace xxtexto with xxtexto+chr(13)+chr(13)+"Cuotas incluidas en "+DTOC(DATE())+" " +TIME()
ENDIF

* select pla_des	### XXX Alejandro
SELECT (m.area_act)

return FERDETA.xxtexto

