*FUNCTION DES_SH
* FUNCIONAMIENTO: Lee SI TIENE CONCEPTO Sh-601 o 660 de descuento
*

PRIVATE m.retorno, m.area_act, reg, XANIO, XCUOTA
*PARAMETER N_IMP

m.retorno  = .T.
m.area_act = SELECT()
SELECT DEV_CD
SET ORDER TO 1
GO TOP
XANIO=DEV_CD.ANIO
XCUOTA=DEV_CD.CUOTA
*reg=recno()

IF m.retorno
   m.retorno=SEEK(dev_comp.remesa+dev_comp.tanda+"C"+STR(dev_comp.nro_imp,10,0)+str(grupo_conc,2,0)+"SH"+str(xanio,4,0)+str(xcuota,3,0)+"NORMA"+STR(2,4,0))
   IF m.retorno=.t.
      if dev_cd.imp_cd>=0
          m.retorno=.f.
      endif
   endif
   *IF M.RETORNO=.F.
   *   m.retorno=SEEK(dev_comp.remesa+dev_comp.tanda+"C"+STR(dev_comp.nro_imp,10,0)+str(grupo_conc,2,0)+"SH"+str(xanio,4,0)+str(xcuota,3,0)+"NORMA"+STR(660,4,0))
   *   IF m.retorno=.t.
   *       if dev_cd.imp_cd>=0
   *           m.retorno=.f.
   *       endif
   *   endif
   *ENDIF
ENDIF   

*goto reg
SELECT (m.area_act)
return m.retorno

