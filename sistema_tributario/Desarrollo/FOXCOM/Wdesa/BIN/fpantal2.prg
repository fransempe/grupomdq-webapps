*- [CONVERTER] Instrucci�n Parameter generada por Converter*- [CONVERTER] Pasar estos valores al nuevo formularioPARAMETERS tit_ventana, fec_ini, fec_fin, caj_ini, caj_fin,; 
	 trans_ini, trans_fin, normales, nom_func, fec_abier, fec_nobloq,; 
	 fec_antcer, fec_proing, fec_exis, fec_rango, caj_rango,; 
	 tra_rango, valid_fec, cing_rango, cod_ingini, cod_ingfin

LOCAL _aParm, _cparmstr, _nctr
DIMENSION _aParm[21]
_aParm[1] = [tit_ventana]
_aParm[2] = [fec_ini]
_aParm[3] = [fec_fin]
_aParm[4] = [caj_ini]
_aParm[5] = [caj_fin]
_aParm[6] = [; 
	 trans_ini]
_aParm[7] = [trans_fin]
_aParm[8] = [normales]
_aParm[9] = [nom_func]
_aParm[10] = [fec_abier]
_aParm[11] = [fec_nobloq]
_aParm[12] = [; 
	 fec_antcer]
_aParm[13] = [fec_proing]
_aParm[14] = [fec_exis]
_aParm[15] = [fec_rango]
_aParm[16] = [caj_rango]
_aParm[17] = [; 
	 tra_rango]
_aParm[18] = [valid_fec]
_aParm[19] = [cing_rango]
_aParm[20] = [cod_ingini]
_aParm[21] = [cod_ingfin]

_cparmstr = []

IF PARAMETERS() > 0
	_cparmstr = [WITH ]
	_cparmstr = _cparmstr + _aParm[1]
	FOR m._nctr = 2 TO PARAMETERS()
		_cparmstr = _cparmstr + [,] + _aParm[m._nctr]
	NEXT
ENDIF

EXTERNAL PROC FPANTAL2.SCX

DO FORM "FPANTAL2.SCX" NAME _1210OMI3P LINKED &_cparmstr

*- [CONVERTER] Principio de CLEANUP y otros procedimientos de formulario de la versi�n 2.x

FUNCTION WFec_Fin
*******************************************
* When del campo fecha final
PRIVATE retorno
retorno = .T.

IF m.fec_rango = 0
	retorno = .F.
ENDIF

RETURN retorno

*******************************************
FUNCTION VFec_Ini
*******************************************
* Valida fecha inicial
PRIVATE retorno
retorno = .T.

retorno = NO_NULO (m.fec_ini, 'Fecha Inicial')
IF retorno
	IF m.fec_exis = 1
		retorno = FFechCal (m.fec_ini, m.fec_abier, m.fec_nobloq,;
												m.fec_antcer, m.fec_proing)
	ENDIF

	************************************************************************
	* Agregado por Fernando el d�a 10/01/1997 por un problema surgido en 
	* 9 de Julio: el sistema permiti� registrar contablemente los ingresos
	* de un d�a en el ejercicio '96 y en el '97.
	* Ahora se valida que el a�o de la fecha de caja (fecha inicial ingresada)
	* por el usuario corresponda al mismo a�o del ejercicio.
	IF retorno AND !EMPTY(m.valid_fec) AND m.valid_fec > 0
		IF(YEAR(m.fec_ini) <> VAL(_EJERC))
			=msg('ATENCION! No se pueden registrar contablemente ingresos del ejercicio ' +;
			STR(YEAR(m.fec_ini),4,0) + ' en el ejercicio ' + SUBSTR(_EJERC,1,4) + '.')
			retorno = .F.
		ENDIF
	ENDIF
	************************************************************************
ENDIF

RETURN retorno

*******************************************
FUNCTION VFec_Fin
*******************************************
* Valida fecha final
PRIVATE retorno
retorno = .T.

IF m.fec_rango = 1
	retorno = NO_NULO (m.fec_fin, 'Fecha Final')
	IF retorno
		IF m.fec_exis = 1
			retorno = FFechCal (m.fec_fin, m.fec_abier, m.fec_nobloq,;
													m.fec_antcer, m.fec_proing)
		ENDIF
	ENDIF
ENDIF

RETURN retorno

*******************************************
FUNCTION VGrup_Fec
*******************************************
* Validaci�n de grupo de fechas.
PRIVATE retorno
retorno = .T.

IF m.fec_rango = 1
	IF m.fec_ini > m.fec_fin
*		=MSG ('La fecha inicial, debe ser menor o igual que la fecha final.')
		=MENS_AVI ('RECUR', 'FECHA INI DEBE SER MENOR FIN')
		retorno = .F.
	ENDIF
ENDIF

RETURN retorno

*******************************************
FUNCTION WCaj_ini
*******************************************
* When del campo Caja inicial
PRIVATE retorno
retorno = .T.

IF m.caj_rango = -1
	retorno = .F.
ENDIF

RETURN retorno

*******************************************
FUNCTION WCaj_Fin
*******************************************
* When del campo Caja final
PRIVATE retorno
retorno = .T.

IF m.caj_rango = 0 OR m.caj_rango = -1
	retorno = .F.
ENDIF

RETURN retorno


*******************************************
FUNCTION VCaj_Ini
*******************************************
* Validaci�n del campo caja inicial .
PRIVATE retorno
retorno = .T.

IF m.caj_rango = 1 OR m.caj_rango = 0
	m.tipo_cod = 'CAJAS '
	m.codif    = m.caj_ini
	retorno = IN_TABLA ('codifics', 'CAJAS', 'codif:H="COD.",;
						desc_cod:H="DESCRIPCION"')
	m.caj_ini = SUBS (m.codif, 1, 4)
	m.dsc_cajini = codifics.desc_cod
	SHOW GETS OFF
	IF retorno
		retorno = NO_NULO (m.caj_ini, 'Caja Inicial')
	ENDIF
ENDIF

RETURN retorno

*******************************************
FUNCTION VCaj_Fin
*******************************************
* Validaci�n del campo caja final.
PRIVATE retorno
retorno = .T.

IF m.caj_rango = 1
	m.tipo_cod = 'CAJAS '
	m.codif    = m.caj_fin
	retorno = IN_TABLA ('codifics', 'CAJAS', 'codif:H="COD.",;
						desc_cod:H="DESCRIPCION"')
	m.caj_fin = SUBS (m.codif, 1, 4)
	m.dsc_cajfin = codifics.desc_cod
	SHOW GETS OFF
	IF retorno
		retorno = NO_NULO (m.caj_fin, 'Caja Final')
	ENDIF
ENDIF

RETURN retorno

*******************************************
FUNCTION VGrup_Caj
*******************************************
* Validaci�n del Grupo de Cajas.
PRIVATE retorno
retorno = .T.

IF m.caj_rango = 1
	IF m.caj_ini > m.caj_fin
*		= MSG ('La Caja Inicial, debe ser menor o igual que la fecha final.')
		=MENS_AVI ('RECUR', 'CAJA INI DEBE SER MENOR FIN')
		retorno = .F.
	ENDIF
ENDIF

RETURN retorno


*******************************************
FUNCTION WTrans_Ini
*******************************************
* When del campo Transacci�n inicial
PRIVATE retorno
retorno = .T.

IF m.caj_rango = 1
	IF m.caj_ini # m.caj_fin
		retorno = .F.
	ENDIF
ENDIF
IF retorno
	IF m.tra_rango = -1
		retorno = .F.
	ENDIF
ENDIF

RETURN retorno


*******************************************
FUNCTION WTrans_Fin
*******************************************
* When del campo Transacci�n final
PRIVATE retorno
retorno = .T.

IF m.caj_rango = 1
	IF m.caj_ini # m.caj_fin
		retorno = .F.
	ENDIF
ENDIF
IF retorno
	IF m.tra_rango # 1
		retorno = .F.
	ENDIF
ENDIF

RETURN retorno

*******************************************
FUNCTION VTrans_Ini
*******************************************
* Validaci�n del campo Transacci�n inicial .
PRIVATE retorno
retorno = .T.

IF m.tra_rango # -1 AND m.caj_ini = m.caj_fin
	retorno = RANGO (m.trans_ini, 'N�mero de Transacci�n Inicial', 2, 1)
ENDIF

RETURN retorno

*******************************************
FUNCTION VTrans_Fin
*******************************************
* Validaci�n del campo Transacci�n final .
PRIVATE retorno
retorno = .T.

IF m.tra_rango = 1 AND m.caj_ini = m.caj_fin
	retorno = RANGO (m.trans_fin, 'N�mero de Transacci�n Final', 2, 1)
ENDIF

RETURN retorno

*******************************************
FUNCTION VGrup_Tran
*******************************************
* Validaci�n del grupo de Transacciones.
PRIVATE retorno
retorno = .T.

IF m.tra_rango = 1 AND m.caj_ini = m.caj_fin
	IF m.trans_ini > m.trans_fin
*		= MSG ('La transacci�n debe ser menor o igual que la transacci�n final.')
		=MENS_AVI ('RECUR', 'TRANS. INI DEBE SER MENOR FIN')
		retorno = .F.
	ENDIF
ENDIF

RETURN retorno

*******************************************
FUNCTION V_ACEPTA
*******************************************
&& Valid del bot�n Acepta
PRIVATE retorno
retorno = .T.

retorno = VAL_GRAB ('D')
IF retorno
		
	IF LEN (m.tit_ventana) + 44 < 70
		DEFINE WINDOW vent_wait FROM  18,2 TO 22,77
		ACTIVATE WINDOW vent_wait
		@01,02 SAY 'Generando ' + m.tit_ventana +;
							 ', Espere un momento Por Favor...'
	ELSE
		DEFINE WINDOW vent_wait FROM  17,2 TO 23,77
		ACTIVATE WINDOW vent_wait
		@01,02 SAY 'Generando ' + m.tit_ventana
		@03,08 SAY 'Espere un momento Por Favor...'
	ENDIF
	
	=EVAL ( m.nom_func )
	
	DEACTIVATE WINDOW vent_wait			
	
ENDIF

RETURN retorno

*******************************************
FUNCTION V_CANCELA
*******************************************
&& Valid del bot�n Cancela
PRIVATE retorno
retorno = .T.

IF sino ( '� Desea Abandonar el Proceso...?')
	CLEAR READ
ENDIF

RETURN retorno

*******************************************
FUNCTION Old_ValFecha
*******************************************
* * * 
* * *   ESTA FUNCION QUEDO OBSOLETA CUANDO FUE REEMPLAZADA POR LA;
* * *   FUNCION FFECHCAL.PRG  (09/05/95)
* * *
&& Funci�n que valida un fecha
&& Par�metro : wfecha	(D) fecha a validar en calenda.
&&
PARAMETERS wfecha
PRIVATE retorno
retorno = .T.

IF m.fec_exis = 1
	m.fecha = wfecha
	IF SEEK (EVAL (exp_calen), 'CALENDA')
		retorno = ValFecCal (wfecha)
	ELSE
		=MSG ('La fecha ingresada no est� habilitada para procesar ' +;
					'informaci�n de tesorer�a.')
		retorno = .F.
	ENDIF
	IF retorno AND m.fec_antcer = 1
		retorno = FecAntCerr (wfecha)
	ENDIF
ENDIF

RETURN retorno

*******************************************
FUNCTION Old_ValFecCal
*******************************************
&& Funci�n que valida un fecha
&& Par�metro : wfecha	(D) fecha a validar en calenda.
&&
PARAMETERS wfecha
PRIVATE retorno
retorno = .T.

IF calenda.est_caj = 'NOCER'
	IF m.fec_abier = -1
		=MSG ('La Caja correspondiente a la fecha ingresada est� abierta.')
		retorno = .F.
	ENDIF
ELSE
	IF m.fec_abier = 1
		=MSG ('La Caja correspondiente a la fecha ingresada NO est� abierta.')
		retorno = .F.
	ENDIF
ENDIF

IF retorno
	IF calenda.fec_bloq = 'N'
		IF m.fec_nobloq = -1
			=MSG ('La Fecha ingresada NO se encuentra bloqueada.')
			retorno = .F.
		ENDIF
	ELSE
		IF m.fec_nobloq = 1
			=MSG ('La Fecha ingresada se encuentra bloqueada.')
			retorno = .F.
		ENDIF
	ENDIF
ENDIF

IF retorno
	IF calenda.proc_ing != 'S'
		IF m.fec_proing = 1
			=MSG ('A�n no fueron procesados los ingresos correspondientes a la fecha.')
			retorno = .F.
		ENDIF
	ELSE
		IF m.fec_proing = -1
			=MSG ('Ya fueron procesados los ingresos correspondientes a la fecha.')
			retorno = .F.
		ENDIF
	ENDIF
ENDIF

RETURN retorno

*******************************************
FUNCTION Old_FecAntCerr
*******************************************
&& Funci�n que verifica si la caja correspondiente a la fecha anterior
&& a la pasada como par�metro, est� en estado cerrada.
&& Par�metro : wfecha	(D) fecha a verificar.
&&
PARAMETERS wfecha
PRIVATE retorno
retorno = .T.

m.fecha = wfecha
IF SEEK (EVAL (exp_calen), 'CALENDA')
	SKIP -1 IN CALENDA
	IF calenda.est_caj = 'NOCER' AND !BOF ('CALENDA')
		=MSG ('La Caja correspondiente a la fecha anterior a la especificada,'+;
					' no est� cerrada.')
		retorno = .F.
	ENDIF
ENDIF

RETURN retorno


*************************************************
FUNCTION W_CIngIni
*************************************************
PRIVATE retorno
retorno = .T.

IF !m.cing_rango
	retorno = .F.
ENDIF

RETURN retorno


*************************************************
FUNCTION V_CIngIni
*************************************************
PRIVATE retorno, cod_ingr
retorno = .T.

m.dsc_cingin = ''
IF m.cod_ingini > 0 OR LastKey () = -2
	m.cod_ingr = m.cod_ingini
	retorno = In_Tabla ('cod_ing', 'CODIGOS DE INGRESO', 'cod_ingr:H="COD.",dsc_coding:H="DESCRIPCION"')
	IF retorno
		m.cod_ingini = m.cod_ingr
		IF m.cod_ingr > 0
			m.dsc_cingin = m.dsc_coding
		ELSE
			m.dsc_cingin = ''
		ENDIF
	ENDIF
ENDIF
SHOW GETS

RETURN retorno


*************************************************
FUNCTION W_CIngFin
*************************************************
PRIVATE retorno
retorno = .T.

IF !m.cing_rango
	retorno = .F.
ENDIF

RETURN retorno


*************************************************
FUNCTION V_CIngFin
*************************************************
PRIVATE retorno
retorno = .T.

m.dsc_cingfi = ''
IF m.cod_ingfin > 0 OR LastKey () = -2
	m.cod_ingr = m.cod_ingfin
	retorno = In_Tabla ('cod_ing', 'CODIGOS DE INGRESO', 'cod_ingr:H="COD.",dsc_coding:H="DESCRIPCION"')
	IF retorno
		m.cod_ingfin = m.cod_ingr
		IF m.cod_ingr > 0
			m.dsc_cingfi = m.dsc_coding
		ELSE
			m.dsc_cingfi = ''
		ENDIF
	ENDIF
ENDIF
SHOW GETS

RETURN retorno


******** FIN DEL CLEANUP ****************************


*- [CONVERTER] Fin de CLEANUP y otros procedimientos del formulario de la versi�n 2.x