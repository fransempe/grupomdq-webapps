*MODI_01
* SQLMODI_12 del ORD_COM
*
*
* Cambio de unidad ejecutora de 5 a 9 digitos
*
*FINMODI_01

******************************************************
FUNCTION ar_tm2im
*
* Funci�n especial para Pta. Alta
*
******************************************************
PRIVATE lin_imp
PRIVATE i, lin_ite, ped_memo, fal_imp, fal_ite, fal_ped
PRIVATE cant_imp, cant_ite, tot_items, primvez, memo_ant, cant_mem
*
* i          -N- Vble para impresion de lineas del memo
* lin_imp    -N- Vble que controla cantidad de imput. que se imprimen.
* lin_ite    -N- Vble que controla cantidad de items que se imprimen.
* ped_memo   -C- Vble para guardar pedazo de memo.
* fal_imp    -B- Flag que faltan imputaciones
* fal_ite    -B- Flag que faltan items.
* fal_ped    -B- Flag indicador que falta memo de descripcion
* cant_imp   -N- Indica la cantidad de imputaciones
* cant_ite   -N- Indica la cantidad de items
* primvez    -B- Flag que indica si se imprimio el total de los items
* tot_items  -N- Total de items
* tot_imput  -N- Total de imputaciones
* HuboTransp -B- Flag indicador de si hubo transporte
* CantBlnfin -N- Cantidad de Blancos que se imprimen al final de los Items
* 							 para que no sobreescriban la firma del proveedor

memo_ant	= SET('MEMOWIDTH')
* Cambio de Ale 30 a 41
SET MEMO TO 41
*
primvez   = .T.
tot_items = 0
tot_imput = 0
cant_imp  = 8
cant_ite   = 24  && cant_ite + CantBlnfin = 30 !!!Siempre
CantBlnfin = 6   && Si se modifica alguno de los 2 modificar el otro
fal_ite   = .T.
fal_imp   = .T.
fal_ped   = .F.
lin_imp   = 0
lin_ite   = 0
cant_mem	= 0

HuboTransp = .F.

*Modi Nicol�s (9/Ene/98): Pedido por Ale para Mor�n
* Consiste en agregar el reng_oc en el reporte (en el cursor tmp_imp).
*Modi Sato (16/Ene/98): Pedido por Leo para P.Alta
* Se agreg� c_indice y c_fechaem al cursor.
*Modi Nicol�s (6/Feb/98) : Pedido de P.Alta
* Se redujo el campo c_dest de 12 a 10 caracteres 

* Cambio de Ale 30 - 41
*												c_desc 		C(30),;
*
*MODI_01
*CREATE CURSOR tmp_imp ( ;
*												c_indice  N(4),;
*												c_banda		C(10),;
*												c_nrorden	N(4),;
*												c_reng_it	C(3),;
*												c_finali 	C(2),;
*												c_funcit 	C(3),;
*												c_rubnom 	C(9),;
*												c_restnom C(9),;
*												c_progra 	C(4),;
*												c_activi 	C(3),;
*												c_unieje 	C(5),;
*												c_imping 	C(16),;
*												c_totimp 	C(16),;
*												c_tipegre C(1),;
*												c_nroped 	C(5),;
*												c_transde C(50),;
*												c_tottde 	C(16),;
*												c_dest 		C(10),;
*												c_cant 		C(9),;
*												c_desc 		C(41),;
*												c_preu 		C(16),;
*												c_total 	C(16),;
*												c_transha C(50),;
*												c_tottha 	C(16),;
*												c_descto 	C(20),;
*												c_totite 	C(16),;
*												c_pie 		C(30),;
*												c_fecemi 	C(10),;
*												c_reng_oc C(6) ,;
*												c_fechaem  C(16) )
CREATE CURSOR tmp_imp ( ;
												c_indice  N(4),;
												c_banda		C(10),;
												c_nrorden	N(4),;
												c_reng_it	C(3),;
												c_finali 	C(2),;
												c_funcit 	C(3),;
												c_rubnom 	C(9),;
												c_restnom C(9),;
												c_progra 	C(4),;
												c_activi 	C(3),;
												c_unieje 	C(9),;
												c_imping 	C(16),;
												c_totimp 	C(16),;
												c_tipegre C(1),;
												c_nroped 	C(5),;
												c_transde C(50),;
												c_tottde 	C(16),;
												c_dest 		C(10),;
												c_cant 		C(9),;
												c_desc 		C(41),;
												c_preu 		C(16),;
												c_total 	C(16),;
												c_transha C(50),;
												c_tottha 	C(16),;
												c_descto 	C(20),;
												c_totite 	C(16),;
												c_pie 		C(30),;
												c_fecemi 	C(10),;
												c_reng_oc C(6) ,;
												c_fechaem  C(16) )
*FINMODI_01

m.c_banda 	= ''
m.c_reng_it	= ''

STORE '' TO m.c_finali,		m.c_funcit, 	m.c_rubnom,		m.c_fecemi,;
						m.c_restnom, 	m.c_progra, 	m.c_activi,		m.c_unieje,;
						m.c_imping,  	m.c_totimp, 	m.c_tipegre,	m.c_nroped,;
						m.c_transde, 	m.c_tottde, 	m.c_dest,			m.c_cant,;
						m.c_desc,    	m.c_preu,   	m.c_total,		m.c_transha,;
						m.c_tottha,  	m.c_descto, 	m.c_totite,		m.c_pie, m.c_reng_oc

m.cont_items = 0



SELECT tmp_imp
IF ALIAS() = 'tmp_imp'
	ZAP
ENDIF

GO TOP IN c_imp_oc
GO TOP IN c_ite_oc

* Bandas:
* 		PEDIDO							4 Lineas
* 		ITEM_TR_D						1 Lineas
* 		ITEM_BODY	(cant_ite) 	Lineas
* 		ITEM_TR_H						1 Lineas
* 		ITEM_TOTAL					4 Lineas
* 		IMPUTACS	(cant_imp)	Lineas
*			EN_BLANCO						? Lineas

*Modi de Nicol�s.(13/02/98) se modifica la condici�n del WHILE
* DO WHILE fal_ite OR fal_imp

DO WHILE fal_ite OR fal_imp OR fal_ped

**********************************************************************
 m.c_banda = 'PEDIDO'
**********************************************************************
* Si se agrega esto, descontar lineas de items o impts.
*	=limp_vars()
*	m.c_tipegre = m.tip_egre
*	m.c_nroped  = STR( m.nro_ped , 5 )
*	=push_lin()
*	=imp_blank( 3 )
				
**********************************************************************
 m.c_banda = 'ITEM_TR_D'
**********************************************************************
	lin_ite = 0
	IF HuboTransp
		=limp_vars()
		m.c_transde = 'Transporte Total de Items desde Hoja Anterior '
		m.c_tottde  = TRANS( tot_items , '9,999,999,999.99' )
		=push_lin()
		lin_ite = lin_ite + 1
		HuboTransp = .F.
	ENDIF

**********************************************************************
 m.c_banda = 'ITEM_BODY'
**********************************************************************
	IF fal_ped
		=imp_memo()
	ENDIF		
	DO WHILE !EOF('c_ite_oc') AND ( lin_ite < cant_ite ) AND fal_ite
		ped_memo = del_chr( c_ite_oc.desc_it_oc )
		i	= 1
		=limp_vars()
		=ord_oc()
		=push_lin()
		lin_ite = lin_ite + 1
		=imp_memo()
		SKIP IN c_ite_oc
	ENDDO

* Modi de Nicol�s (13/02/98). Se modifica el fal_ite
*	fal_ite = ( lin_ite >= cant_ite ) 

	fal_ite = !EOF('c_ite_oc')	

**********************************************************************
 m.c_banda = 'ITEM_TR_H'
**********************************************************************
	IF fal_ite
		=limp_vars()
		m.c_transha = 'Transporte Total de Items Hacia Hoja Siguiente '
		m.c_tottha  = TRANS( tot_items , '9,999,999,999.99' ) 
		=push_lin()
		lin_ite = lin_ite + 1
		HuboTransp = .T.
	ENDIF

**********************************************************************
 m.c_banda = 'ITEM_TOTAL'
**********************************************************************
	IF !fal_ite AND primvez AND !HuboTransp
		=imp_blank( 1 )
		=limp_vars()
		m.c_descto = ' Total : '
		m.c_totite = TRANS( tot_items , '9,999,999,999.99' )
		=push_lin()
		* Se elimino esto ya que no se imprime la Fecha de Emision ac�
		*=imp_blank( 1 )
		*=limp_vars()
		*m.c_pie    = m.pieoc
		*m.c_fecemi = DTOC( m.fe_emision )
		*=push_lin()
		* Al sacar lo anterior se rest� dos en la suma
		*
    *lin_ite = lin_ite + 4
    lin_ite = lin_ite + 2
		primvez = .F.
	ENDIF

*	=imp_blank( (cant_ite - lin_ite) + 1 )
* Agregado por Nicol�s (13/02/98)
* Si (cant_ite - lin_ite) >= 0
* Imprimo 'CantBlnfin' lineas + la cantidad de lineas que faltan
* sino
* Imprimo 'CantBlnfin' lineas - la cantidad de lineas que uso el 
* 'Total  : ' sobrepasando el l�mite de cant_ite
* El + 2 final es porque se reservan lineas en blanco
*
	=imp_blank( CantBlnfin + (cant_ite - lin_ite) + 2)
		
**********************************************************************
 m.c_banda = 'IMPUTACS'
**********************************************************************
	
	=imp_blank(1)

* [*] Modi Nicol�s (06/02/98). Elimino la descripci�n que est� abajo y
* la reemplazo por una linea en blanco
*	=limp_vars()
*	m.c_transha = 'Imputaciones de la Orden de Compra'
*	=push_lin()
	=imp_blank(1)
* Fin Modi

	=imp_blank(1)
	lin_imp = 3
	REPLACE tmp_imp.c_fechaem WITH FE_EMISI()
	DO WHILE !EOF('c_imp_oc') AND ( lin_imp < cant_imp ) AND fal_imp
		=limp_vars()
		=ord_coma()
		
		IF c_imp_oc.imp_ing != 0
			=push_lin()
			lin_imp = lin_imp + 1
			REPLACE tmp_imp.c_fechaem WITH ''
		ENDIF
		

		SKIP IN c_imp_oc
	ENDDO
	fal_imp = ( lin_imp >= cant_imp )
	=imp_blank( cant_imp - lin_imp )
	* Modi. Nicol�s (13/02/98) 
	* Imprimo Al final 1 l�nea en blanco siempre
	=imp_blank(1)

ENDDO

* ( Inicializar tabla temporal )
GO TOP IN tmp_imp

SET MEMOWIDTH TO (memo_ant)

RETURN .T.
*


********************************
FUNCTION imp_blank
********************************
PARAMETERS cant
PRIVATE ite, area, m.c_banant
m.c_banant = m.c_banda
**********************************************************************
 m.c_banda = 'EN_BLANCO'
**********************************************************************
area = SELECT()
SELECT tmp_imp
=limp_vars()
m.c_finali = CHR(255)
* Para pruebas 
*m.c_finali = '***'
FOR ite = 1 to cant
	=PUSH_LIN()
ENDFOR
m.c_banda = m.c_banant
SELECT( area )
RETURN .T.


********************************
FUNCTION push_lin
********************************
PARAMETERS nomb_tempo, lin, m.c_nrorden
PRIVATE ite, area
area = SELECT()
SELECT tmp_imp
CALC MAX(c_nrorden) TO m.c_nrorden FOR c_banda = m.c_banda
m.c_nrorden = m.c_nrorden + 1
APPEND BLANK
GATHER MEMVAR
SELECT( area )
RETURN .T.

*************************************************
FUNCTION IMP_MEMO
*************************************************
PRIVATE retorno
retorno 	= .T.
cant_mem 	= MEMLINES(ped_memo)
DO WHILE ( i <= cant_mem ) AND ( lin_ite < cant_ite )
	lin_mem = MLINE(ped_memo,i)
	IF !EMPTY(lin_mem)
		=limp_vars()
* Cambio de Ale 30 a 41
* y saco justificado
*		m.c_desc = JUST_CHR(lin_mem,30)
		m.c_desc = lin_mem
		=push_lin()
		lin_ite = lin_ite + 1
	ENDIF
	i = i + 1
ENDDO
fal_ped = (i <= cant_mem)
RETURN retorno

********************************************
FUNCTION ORD_OC
********************************************
PRIVATE total, l_mem, ant_dec

total 		= ROUND( c_ite_oc.pre_uni_oc * c_ite_oc.cant_it_oc , 2 )
tot_items = tot_items + total

* Modi de Nico (12/02/98). Cambio el ancho del memo de 30 a 41, y saco el
* justificado
* l_mem			=	JUST_CHR( MLINE( ped_memo , i ) , 30 )
l_mem			=	MLINE( ped_memo , i )
i	=	i + 1		 

* [*] Modi de Nicol�s reduje la cantidad de caracteres de m.c_dest

m.c_dest  = PADR( c_ite_oc.dest_it_oc, 10     )
m.c_cant  = TRANS( c_ite_oc.cant_it_oc ,         '99,999.99' )
m.c_desc  = l_mem

m.cont_items= m.cont_items + 1
m.c_reng_it = STR(m.cont_items,3,0)

ant_dec = SET('deci')
SET DECI TO 3
m.c_preu  = TRANS( c_ite_oc.pre_uni_oc ,   '999,999.999' )
SET DECI TO ant_dec

m.c_total =	TRANS( total               , '9,999,999.99'  )    	

* Modi SATO (31/Dic/97)
m.c_reng_oc = STR(c_ite_oc.reng_oc,6,0)

RETURN ''
*

***********************
FUNCTION ord_coma
***********************
* Arma la Imputacion para imprimir si corresponde

IF ( c_imp_oc.imp_ing != 0 )
	m.c_finali  = STR( c_imp_oc.finalidad , 1      ) + '-'
	m.c_funcit  = STR( c_imp_oc.func_item , 2      ) + '-'
	m.c_rubnom  = TRANS( c_imp_oc.rubro_nom , '@R 99-99-99' ) + '-'
	m.c_restnom = TRANS( c_imp_oc.rest_nom  , '@R 99-99-99' ) + '-'
	m.c_progra  = STR( c_imp_oc.program   , 3      ) + '-'
	m.c_activi  = STR( c_imp_oc.actividad , 2      ) + '-'
	*MODI_01
	*m.c_unieje  = STR( c_imp_oc.uni_ejec  , 5      ) 
	m.c_unieje  = STR( c_imp_oc.uni_ejec  , 9      ) 
	*FINMODI_01
	m.c_imping  = TRANS( c_imp_oc.imp_ing   , '9,999,999,999.99' ) 

* No se usa por ahora	
*  tot_imput = tot_imput + c_imp_oc.imp_ing         
ENDIF 

RETURN ''


*****************************
FUNCTION limp_vars
*****************************
STORE '' TO m.c_finali, m.c_funcit, m.c_rubnom,;
m.c_restnom, m.c_progra, m.c_activi,  m.c_unieje,;
m.c_imping,  m.c_totimp, m.c_tipegre, m.c_nroped,;
m.c_transde, m.c_tottde, m.c_dest,    m.c_cant,;
m.c_desc,    m.c_preu,   m.c_total,   m.c_transha,;
m.c_tottha,  m.c_descto, m.c_totite,  m.c_pie,;
m.c_fecemi,  m.c_reng_it, m.c_reng_oc



RETURN ''

