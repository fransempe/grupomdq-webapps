*************************************************
* FUNCTION NOMACRE
*************************************************
* Funcionamiento : Devuelve el nombre de archivo que 
* aparece en el rotulo guardado 
PRIVATE retorno

IF !EMPTY(CTASESQ.rotulo)
	retorno =  ALLT(CTASESQ.rotulo)
ENDIF

RETURN retorno

