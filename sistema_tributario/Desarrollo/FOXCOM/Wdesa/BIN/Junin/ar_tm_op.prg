******************************************************
* FUNCTION ar_tm_op
******************************************************
* Funci�n especial para LA BANDA para poder manejar el reporte
* esta funci�n se utiliza fuera del proyecto.
*
* MODI_LB (visual): en funci�n del reporte se arma el cursor de impresi�n
*					LGEN_OP2 : 5 imputaciones con "Recib� ..." y "Firma ..."
* 					LGEN_OP3 : 5 imputaciones sin "Recib� ..." ni "Firma ..."
* 					LGEN_OP4 : 31 imputaciones con "Recib� ..." y "Firma ..."

PRIVATE i, lin_imp, lin_ite, ped_memo, fal_imp, fal_ite, fal_ped
PRIVATE cant_imp, cant_ite, tot_items, primvez, ant_reg
*
* i        -N- Vble para impresion de lineas en blanco
* lin_imp  -N- Vble que controla cantidad de imput. que se imprimen. 
* lin_ite  -N- Vble que controla cantidad de items que se imprimen.
* fal_imp  -B- Flag que faltan imputaciones
* fal_ite  -B- Flag que faltan items.
* cant_imp -N- Indica la cantidad de imputaciones
* cant_ite -N- Indica la cantidad de items
* primvez  -B- Flag que indica si se imprimio el total de los items

tot_items = 0

* MODI_LB
DO CASE
	CASE UPPER(m.reporte) = 'LGEN_OP4'
		cant_imp = 31
	CASE (UPPER(m.reporte) = 'LGEN_OP3') OR (UPPER(m.reporte) = 'LGEN_OP2')
		cant_imp = 5
ENDCASE
* FIN MODI_LB

cant_ite  = 3
fal_ite   = .T.
fal_imp   = .T.
lin_imp   = 0 
lin_ite   = 0 
subtot    = 0
valtotimp = 0
tot_ret   = 0
tot_ret_imp = 0
prim_ret  = .T.
tot_imp_imp = .F.
impor_cheq = ord_pag.importe_op

*[*] Modificaci�n Nicol�s .Agregu� campo. c_nabrctap

* CREATE CURSOR tmp_imp ( linea C(96) )
CREATE CURSOR tmp_imp (c_finalid C(2), c_func_it C(2), c_rubro1 C(2), ;
	c_rubro2 C(2), c_rubro3 C(2), c_rest1 C(2), c_rest2 C(2), ;
	c_rest3 C(2), c_prog C(3), c_activ C(2), c_uniej C(5),c_nabrctap C(15),;
	c_ctadsc C(40), c_ctaimp C(20), c_ctarec C(16),;
	c_imp_op C(20), c_trans1 C(10), c_trans2 C(20), c_total C(26), ;
	c_nroctao C(6), c_nomctao C(35), c_imp_op2 C(20), c_descret C(20), ;
	c_impret C(20), c_impche C(20), c_totop C(20), c_suma C(96), c_pagar C(96),;
	c_ord_com C(7), c_codnctap C(5))
	
SELECT tmp_imp
IF ALIAS() = 'tmp_imp'
	ZAP
ENDIF

SELECT c_ret
GO TOP IN c_ret
DO WHILE ! EOF ('c_ret')
	impor_cheq = impor_cheq - c_ret.valor	
	SKIP IN c_ret
ENDDO

COUNT TO tot_ret
tot_ret_imp = 2

GO TOP IN c_ret

ant_reg = RECNO ('ORPA_IM')
DO WHILE fal_ite OR fal_imp

	lin_imp = 0

* Linea comentada por RODRIGO para que haga bien el transporte
*	subtot = 0

	DO WHILE ! EOF('ORPA_IM') AND ;
	(ORPA_IM.nro_or_pag = ORD_PAG.nro_or_pag) AND ;
	( lin_imp < cant_imp ) AND ;
	fal_imp
		IF orpa_im.imp_imp_op > 0
			subtot = subtot + orpa_im.imp_imp_op
			valtotimp = valtotimp + orpa_im.imp_imp_op
			= lin_imput ()
			IF SEEK (STR(ORPA_IM.FINALIDAD,1,0)+STR(ORPA_IM.FUNC_ITEM,2,0)+;
				 ORPA_IM.RUBRO_NOM+ORPA_IM.REST_NOM+STR(ORPA_IM.PROGRAM,3,0)+;
				 STR(ORPA_IM.ACTIVIDAD,2,0)+STR(ORPA_IM.UNI_EJEC,5,0),"PRESUPU")
				m.c_nabrctap = PRESUPU.nabr_ctap
				m.c_codnctap = PRESUPU.codn_ctap
			ELSE
				m.c_nabrctap = "--------"
			ENDIF

			c_ord_com= ORPA_IM.TIP_EGRE + "-" + STR(ORPA_IM.NRO_OR_COM,5,0)

			= push_lin ()
			lin_imp = lin_imp + 1
			m.c_codnctap = ''
			
		ENDIF
		SKIP IN ORPA_IM
	ENDDO
	
	IF fal_imp 
		IF ( lin_imp = cant_imp ) AND ! EOF ('ORPA_IM') AND ;
		(ORPA_IM.nro_or_pag = ORD_PAG.nro_or_pag)
			fal_imp = .T.
		ELSE
			fal_imp = .F.
		ENDIF
	ENDIF

	IF fal_imp

		*Vero
		= imp_blank (5)
		
		= limp_vars ()
		m.c_trans1 = 'TRANSPORTE'
		m.c_trans2 = TRANS (subtot, '9,999,999,999,999.99')
		= push_lin ()
		
		
	ELSE
*		= imp_blank (40 - lin_imp - 1 )
		= imp_blank (cant_imp - lin_imp - 1 )
		IF tot_imp_imp = .F.
			= limp_vars ()
			m.c_suma =   '________________________________________________________________________________________'
			= push_lin ()
			m.c_suma  = ' '
			m.c_total = 	'TOTAL:'+TRANS (valtotimp, '9,999,999,999,999.99') 
			= push_lin ()
			tot_imp_imp = .T.
			
		ELSE
			= imp_blank (1)
		ENDIF
	ENDIF
	
	
	*= imp_blank (9)
				
	IF ! fal_imp AND ((tot_ret - tot_ret_imp) <= 3)
		= imp_blank (3)
*		= imp_blank (8)
				
		= limp_vars ()
*!*			m.c_pagar= '                          _________________________         __________________________'
*!*			= push_lin ()		
*!*			m.c_pagar= '                                  Intervino                      CONTADOR GENERAL'
*!*			= push_lin ()		 
		= imp_blank (2)

		* MODI_LB
		IF (UPPER(m.reporte) = 'LGEN_OP2') OR (UPPER(m.reporte) = 'LGEN_OP4')
			m.c_pagar = ' '
			m.c_pagar = '   Recib� de la Municipalidad de Junin de los Andes, la suma de pesos:'
			= push_lin ()
			m.c_pagar = ' '
			
			
			*MODI VERO: No sal�an los dos renglones de importe en letras **
			*imp1=PADR(PROPER(NUMLET(ord_pag.importe_op, .F.))+' ',62,'*')
			imp1=PADR(LINLET((PROPER(NUMLET(ord_pag.importe_op, .F.))+' '),1,71),71,'�')
			m.c_suma = '  ' + PROPER(imp1)

			= push_lin ()
			imp2=PADR(LINLET((PROPER(NUMLET(ord_pag.importe_op, .F.))+' '),2,71),71,'�')
			m.c_suma = m.imp2
			= push_lin ()
		ENDIF
		* FIN MODI_LB

	ELSE
		= imp_blank (15)
  ENDIF

	IF ! fal_imp OR tot_ret > 5 && xxx para que ret en ult. hoja
*		= imp_blank (2)
		= imp_blank (1)
		IF ((tot_ret - tot_ret_imp) <= 3)
			= limp_vars ()
	*		m.c_suma     = '                                                      ...........................'
			m.c_descret= 'Importe cheque Nro. ................:'
			m.c_impche = trans (impor_cheq,'999,999,999.99')
			= push_lin ()
			m.c_descret= ' '
			m.c_impche = ' '
			= push_lin ()
	*		m.c_suma     = '                                           		 Documento de Identidad '
			= push_lin ()
		ELSE
	*		= imp_blank (3)
			= imp_blank (4)		
		ENDIF
		
		IF prim_ret
			prim_ret = .F.
			= limp_vars ()
	*		m.c_suma     = '                                                        Tipo:     Nro: '

			m.c_descret= 'Ret.a las Ganancias:'
			m.c_impret = trans (c_ret.valor, '999,999,999.99')
			= push_lin ()
			m.c_suma = ' '
			SKIP IN c_ret
			m.c_descret= 'Ret.por Ing.Brutos:'
			m.c_impret =  trans (c_ret.valor, '999,999,999.99')
			= push_lin ()
			SKIP IN c_ret
		ELSE
			= imp_blank (2)
		ENDIF

		lin_ite = 0
		= limp_vars ()
		DO WHILE ! EOF('c_ret') AND ( lin_ite < cant_ite ) AND fal_ite
			m.c_descret = ''
			m.c_impret = ''
			m.c_descret = c_ret.desc
			m.c_impret = TRANS (c_ret.valor, '999,999,999.99')
			=push_lin()
			tot_ret_imp = tot_ret_imp + 1
			lin_ite = lin_ite + 1
			SKIP IN c_ret
		ENDDO
		IF ( lin_ite = cant_ite ) AND  ! EOF ('c_ret')
			fal_ite = .T.
		ELSE
			fal_ite = .F.
		ENDIF
	*	fal_ite = ( lin_ite > cant_ite )

*!*			= imp_blank (1)
		m.c_descret = ''
		m.c_impret = ''
		
		IF fal_ite
			m.c_totop = 'CONT. EN OTRA HOJA'
			= push_lin ()
		ELSE
	*!*			= imp_blank (2 - lin_ite )
	*!*			m.c_suma      = '                                                                                          '
	*!*			= push_lin ()
	*		m.c_suma      = '                                                                                         '

	*!*			m.c_descret = 'TOTAL ORDEN DE PAGO:'
			= imp_blank (1)	&& ### XXX
			m.c_suma = 'TOTAL ORDEN DE PAGO:'
			m.c_totop = TRANS (ord_pag.importe_op, '999,999,999.99')
			= push_lin ()

			* MODI_LB
			IF (UPPER(m.reporte) = 'LGEN_OP2') OR (UPPER(m.reporte) = 'LGEN_OP4')
				m.c_totop = ''
				= imp_blank (3)
				m.c_suma  = '                                                 Firma : ___________________________'
				= push_lin ()
				= imp_blank (3)
				m.c_suma  = '                                            Aclaraci�n : ___________________________'
				= push_lin ()
				= imp_blank (3)
				m.c_suma  = '                                Documento de Identidad : ___________________________'
				= push_lin ()
			ENDIF
			* FIN MODI_LB

		ENDIF
	ELSE
			= imp_blank (7)
	ENDIF && FIN XXX
*	= imp_blank (5)
*	= imp_blank (3)

ENDDO
= IR_REG (ant_reg, 'ORPA_IM')
RETURN .T.
*
