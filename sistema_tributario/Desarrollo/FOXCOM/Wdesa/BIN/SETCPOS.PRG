****************************************************
*FUNCTION SetCpos
****************************************************
* Funci�n para la Lectura de D�bitos Informados de
* de La Banda 
PARAMETERS prest, motivo
PRIVATE retorno, alias_ant,fin
m.retorno = .T.

m.alias_ant = ALIAS()

SELECT cursdeb
REPLACE pres_ser		WITH	 m.prest 	ALL
REPLACE	mot_debi	WITH	m.motivo ALL
REPLACE	deb_cred	WITH	'D' ALL

GO TOP IN cursdeb
DO WHILE !Eof('cursdeb')
	SET KEY TO cursdeb.nro_lega IN legajos
	GO TOP IN agentes
	m.fin = .F.
	DO WHILE !Eof('agentes') AND !m.fin
		IF agentes.situacion = 'S'
			m.fin = .T.
			m.n_cargo = agentes.nro_cargo
		ENDIF
		SKIP IN agentes
	ENDDO
	IF m.fin
		REPLACE cargo		WITH	STR(m.n_cargo,3,0)
	ELSE
		REPLACE cargo		WITH	'0'
	ENDIF
	SET KEY TO  '' IN legajos
	SKIP IN cursdeb
ENDDO

IF !EMPTY(m.alias_ant)
	SELECT(m.alias_ant)
ENDIF

RETURN m.retorno
