*************************************************
* Function SetNacio 
*************************************************
PARAMETERS tipo
PRIVATE retorno
retorno = ' '
DO CASE
	CASE tipo = 'ARG.' OR tipo = 'ARGE' OR tipo = 'ARGN' OR tipo = 'ARGO'
		retorno = 'A'
	CASE tipo = 'EXTR'
		retorno = 'E'
ENDCASE
RETURN retorno
