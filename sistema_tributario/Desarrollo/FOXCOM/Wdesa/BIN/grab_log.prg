*************************************************
* FUNCTION Grab_Log
*************************************************
* Autor : Alejandro
* Dise�o: Alejandro
* 
* Fecha : 22/08/2003
* 
* Funcionamiento: Graba un registro en la tabla LOG_PROG
* 
* Par�metros:
* 	Mensaje	C(200)
* 
* Modificaciones:
* 
PARAMETERS mensaje
PRIVATE retorno, alias_act
retorno = .T.

m.alias_act = SELECT ()

IF !USED ('asa\log_prog')
	=UseT ('asa\log_prog')
ENDIF

SELECT log_prog
APPEND BLANK
REPLACE sistema			WITH m.sistema, ;
				modulo			WITH m.modulo, ;
				programa		WITH m.programa, ;
				num_audit		WITH m.num_aud, ;
				fecha				WITH DATE (), ;
				hora_log		WITH VAL (STRTRAN (STRTRAN (TIME (1), ':', ''), '.', '')), ;
				usuario			WITH m.usuario, ;
				text_mensa	WITH m.mensaje

SELECT (m.alias_act)

RETURN retorno

