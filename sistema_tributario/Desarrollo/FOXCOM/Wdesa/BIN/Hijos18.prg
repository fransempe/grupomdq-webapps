* Dado un nro de legajo y una edad devuelve la cantidad de hijos con esa edad o menos que 
* tiene el legajo (FAF)
* Modifico con el permiso de FAF - Vero 
* No tomo el DATE para calcular fecha base del c�lculo, sino el primer
* d�a del mes/anio actual

*PARAMETERS nAnios
*LOCAL nAnios  && Cantidad de a�os a considerar
PRIVATE nCount, dFecha
*LOCAL nValor  && Constante de asignaci�n por hijo
*LOCAL nCount, dFecha

nAnios = 18
*nValor = 1.5

=USET("RECHUM\GR_FAMIL")

PRIVATE dia
*dFecha = GOMONTH(DATE(),-12 * nAnios)
*m.dia = CTOD('01/'+PADL(MONTH(DATE()),2,'0')+'/'+STR(YEAR(DATE()),4,0))
m.dia = CTOD('01/'+PADL(parametr.mes_haber,2,'0')+'/'+STR(YEAR(DATE()),4,0))
dFecha = GOMONTH(m.dia,-12 * nAnios)

nCount = 0
IF SEEK(STR(AGENTES.NRO_LEGAJO, 6, 0),"GR_FAMIL")	
	DO WHILE GR_FAMIL.NRO_LEGAJO = AGENTES.NRO_LEGAJO

		IF (GR_FAMIL.FEC_NACLEG >= dFecha) AND;
		   ((GR_FAMIL.TIPO_FAM = "HIJO/A") OR (GR_FAMIL.TIPO_FAM = "MENOR") OR (GR_FAMIL.TIPO_FAM = "NIETO/A")) AND;
		   (GR_FAMIL.VIVE = 'S') AND (GR_FAMIL.CONASIG='S') AND;
       (GR_FAMIL.P_DISCAP = 0)	
			nCount = nCount + 1

		ENDIF
		SKIP IN GR_FAMIL
	ENDDO
ENDIF
USE IN GR_FAMIL

*RETURN nCount * nValor
RETURN nCount