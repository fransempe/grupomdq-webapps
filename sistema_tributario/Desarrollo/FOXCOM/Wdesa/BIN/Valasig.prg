******************************************************
* FUNCTION VALASIG
******************************************************
* Funcionamiento: Valida que lo datos de asignaciones tenga el mismo
* c�digo de ENTE que el que viene en el nombre del archivo. 
*
PRIVATE retorno,cod_ente,old_tab,exp_cpos
PRIVATE m.camp_ascii

retorno = .T.
cod_ente = ALLT(SUBSTR(m.diskarch,5,4))
old_tab = SELECT ()

IF !USED('CPO_ASC')
	=USET ('ASA\CPO_ASC')
ENDIF

exp_cpos = GETKEY('CPO_ASC',.T.)

* Verifico que el C�digo del Ente que figura en el nombre del Archivo
* sea el mismo que el que figura en los registros

SELECT CURSCTAS
DO WHILE !EOF() AND retorno
	retorno = (ALLT(CURSCTAS.codente) == cod_ente)
	SKIP 1 IN CURSCTAS
ENDDO

* Modifico la informaci�n del C�digo del Ente.

IF !retorno
	= Msg('Revise el ASCII. No concuerda el c�digo de ente de los registros.')
ELSE
	SELECT CPO_ASC
	m.camp_ascii = 'CODENTE'
	IF SEEK (EVAL(exp_cpos))
		IF CPO_ASC.contenido = '' OR CPO_ASC.contenido = '0000'
			REPLACE CPO_ASC.contenido WITH cod_ente
		ENDIF
	ELSE
		= Msg('No se pudo actualizar el c�digo del Ente. Funci�n VALASIG().')
		retorno = .F.
	ENDIF
ENDIF

SELECT (old_tab)

RETURN retorno

