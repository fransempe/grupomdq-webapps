*************************************************
* Function ModDia
*************************************************
* Funcionamiento: Formatea el dia en formato numerico del tipo
* 								AAMMDD o DDMMAA segun una variable condicional
* 
* 
* 
* Par�metros: 
*							fecha : Campo del Tipo Fecha
* 						tipo  : Campo Booleano 	
* 										.t. -> AAMMDD
* 										.f. -> DDMMAA
*
*							[anioc] (.T.): Campo Booleano
*											.t. -> El a�o(es decir AA) es con 2 caracteres
*											.f. -> El a�o es con 4 caracteres (AAAA)
* 
*
PARAMETERS fecha,tipo,anioc
PRIVATE retorno,dia,mes,anio,aniocorto
retorno = 0

IF PARAMETERS () < 3
	aniocorto = .T.
ELSE
	aniocorto = anioc
ENDIF

dia = DAY(fecha)
mes = MONTH(fecha)

IF aniocorto
	anio = INT(VAL(SUBSTR(STR(YEAR(fecha),4),3)))
ELSE
	anio = YEAR(fecha)
ENDIF

IF tipo
	retorno = dia + mes * 100 + anio * 10000 && AAMMDD
ELSE
	IF !aniocorto
		retorno = anio + mes * 10000 + dia * 1000000 && DDMMAAAA
	ELSE
		retorno = anio + mes * 100 + dia * 10000 && DDMMAA
	ENDIF
ENDIF
RETURN retorno
