*************************************************
FUNCTION REG2FX
*************************************************
PARAMETERS prg
PRIVATE retorno, cant_dias, fecha, alias_ant
retorno = .T.
rr			= ''
m.alias_ant = Alias ()
IF !USED('PROG')
	close_prog = .T.
	=USET ('ASA\PROG')
ELSE
	close_prog = .F.
ENDIF
IF SEEK(m.sistema+m.modulo+prg,'PROG')
	ewok = prog.cla_prog
	IF !EMPTY(ewok)
		wdmuni = GET_PARA('ASA','PARTIDO')
		wdmuni = UPPER(RIGHT(wdmuni, LEN(wdmuni)-RAT(' ',wdmuni)))

		IF Len (AllTrim (ewok)) <= 6
			FOR I = 1 TO 6
				digito = SUBSTR(prg,i,1)
				IF ISALPHA(digito) OR ISDIGIT(digito)
					rr = rr + CHR(ASC(digito) + ASC(SUBSTR(wdmuni,i,1)) - 50 + i)
				ENDIF
			ENDFOR
			retorno = ( AllT (ewok) == AllT (rr) )
		ELSE
			prg = PadR (prg, 6)
			FOR I = 1 TO 6
				digito = SUBSTR(Upper (prg),i,1)


				*	Comienzo SQLMODI_01

				* IF Asc (m.digito) != 0
				IF Asc (m.digito) != 0 AND Asc (m.digito) != 32

				*	Fin SQLMODI_01


					rr = rr + CHR(ASC(digito) + ASC(SUBSTR(wdmuni,i,1)) - 50)
				ELSE
					rr = rr + CHR(21)
				ENDIF
			ENDFOR

			DIMENSION cc[6]
			DIMENSION ff[6]
			cc = '021'
			FOR I = 1 TO 12
				digito = SUBSTR(ewok,i,1)
				IF i % 2 = 0
					IF m.i = 12
						ff[i/2] = Str (Asc (digito), 3, 0)
					ELSE
						ff[i/2] = Str (Asc (digito) % 10, 1, 0)
					ENDIF
				ELSE
					cc[Int (i/2) + 1] = m.digito
				ENDIF
			ENDFOR
			retorno = (AllT (rr) == AllTrim (cc[3] + cc[2] + cc[4] + cc[6] + cc[5] + cc[1]))
			IF retorno

				m.fecha = FGetClaF (prog.long_prog)
				SELECT prog
				REPLACE long_prog WITH FSetClaF (m.fecha)
				IF !Empty (m.alias_ant)
					SELECT (m.alias_ant)
				ENDIF

				*m.cant_dias = Val (ff[3] + ff[5] + ff[2] + ff[4] + ff[1]) - (m.fecha - {01/01/1980})
				m.cant_dias = Val (ff[3] + ff[5] + ff[2] + ff[4] + ff[1]) - (m.fecha - {^1980-01-01})
				IF m.cant_dias >= 0
					retorno = .T.
				ELSE
					= Msg ("La activaci�n provisoria del programa " + prog.programa + " ha expirado.")
					retorno = .F.
				ENDIF
				IF retorno
					retorno = Val (ff[6]) = Int (Val (ff[2] + ff[4] + ff[1]) / 7)
					IF !retorno
						= Msg (".La activaci�n provisoria del programa " + prog.programa + " ha expirado.")
					ENDIF
				ENDIF
				IF retorno
					IF m.cant_dias <= 10
						= Msg ("La activaci�n provisoria del programa " + prog.programa + " expirar� en " + AllTrim (Str (m.cant_dias)) + " d�as.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		retorno = .F.	
	ENDIF
ELSE
	retorno = .F.
ENDIF
IF close_prog
	USE IN prog
ENDIF
RETURN retorno


*************************************************
FUNCTION OldREG2FX
*************************************************
PARAMETERS prg
PRIVATE retorno
retorno = .T.
rr			= ''
IF !USED('PROG')
	close_prog = .T.
	=USET ('ASA\PROG')
ELSE
	close_prog = .F.
ENDIF
= SEEK(m.sistema+m.modulo+prg,'PROG')
ewok = prog.cla_prog
IF close_prog
	USE IN prog
ENDIF
IF !EMPTY(ewok)
	wdmuni = GET_PARA('ASA','PARTIDO')
	wdmuni = UPPER(RIGHT(wdmuni, LEN(wdmuni)-RAT(' ',wdmuni)))
	FOR I = 1 TO 6
		digito = SUBSTR(prg,i,1)
		IF ISALPHA(digito) OR ISDIGIT(digito)
			rr = rr + CHR(ASC(digito) + ASC(SUBSTR(wdmuni,i,1)) - 50 + i)
		ENDIF
	ENDFOR
	retorno = ( AllT (ewok) == AllT (rr) )
ELSE
	retorno = .F.	
ENDIF
RETURN retorno
