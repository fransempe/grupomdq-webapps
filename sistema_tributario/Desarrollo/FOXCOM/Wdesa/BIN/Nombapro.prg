*************************************************
* FUNCTION NOMBAPRO
*************************************************
PARAMETERS m.archdest
PRIVATE retorno,m.handarch,m.renglon,m.old_tab
retorno = ''
m.old_tab = SELECT ()

CREATE CURSOR c_ini (fecproc  C (8),;
										 codente  C (4)) 

retorno = DesASCII (m.archdest,m.sistema,m.modulo,m.esqueleto,'c_ini')

IF retorno
	retorno = PADL(ALLT(c_ini.codente),4,'0') +;
						SUBSTR(c_ini.fecproc,3,4)+'.emp'
ENDIF

USE IN C_INI

SELECT (m.old_tab)

RETURN retorno

