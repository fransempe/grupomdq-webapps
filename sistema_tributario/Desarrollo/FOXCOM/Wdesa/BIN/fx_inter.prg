*****************************
*CA�UELAS
****************************
PARAMETERS m.importe
PRIVATE m.coefic

if rec_cc.recurso="VI" .OR. rec_cc.recurso="CL" .OR. rec_cc.recurso="FP"
   RETURN 0
endif
if rec_cc.recurso="SG" .AND. rec_cc.conc_cc="CL"
   RETURN 0
endif

if rec_cc.recurso="SG" .AND. rec_cc.conc_cc="NCCL"
   RETURN 0
endif

if rec_cc.recurso="SG" .AND. rec_cc.conc_cc="NDCL"
   RETURN 0
endif

if rec_cc.recurso="AM" .AND. rec_cc.conc_cc="NDPP"
   RETURN 0
endif

IF type('m.anio')='U' OR type('m.mes')='U' OR type('m.dia')='U'
  m.coefic=vec_int[m.anio_o + m.mes_o,1]
  IF vec_int[m.anio_o + m.mes_o,2] <> 0
		m.coefic= m.coefic + vec_int[m.anio_o + m.mes_o, 2] * (m.dias_m - m.dia_o)
  ENDIF
ELSE
  m.coefic=0
  IF m.anio_o=m.anio AND m.mes_o=m.mes
	 m.coefic= vec_int[m.anio+m.mes,2] * (m.dia - m.dia_o)
  ELSE
  	 m.coefic= vec_int[m.anio+m.mes - 1, 1] - vec_int[m.anio_o + m.mes_o ,1] 
 	 IF vec_int[m.anio_o + m.mes_o,2] <> 0
		m.coefic= m.coefic + vec_int[m.anio_o + m.mes_o, 2] * (m.dias_m - m.dia_o)
	 ENDIF
	
	 IF vec_int[m.anio + m.mes,2] <> 0
		m.coefic= m.coefic + vec_int[m.anio + m.mes, 2] * m.dia
	 ENDIF
  ENDIF

  IF m.coefic<0
	m.coefic=0
  ENDIF
ENDIF

RETURN ROUND(m.importe*m.coefic,2)			

