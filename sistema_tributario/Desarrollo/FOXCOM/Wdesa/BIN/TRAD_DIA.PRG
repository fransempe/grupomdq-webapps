****************************
* FUNCTION Trad_dia
****************************
*
* Autor : Gabriel 
* 
* Devuelve el Nombre de dia en castellano
*
* Ej. de llamado : = Trad_dia( CDOW({01/01/95}) )
* Modificado 20/Ago/96 (se le sacaron acentos por problemas de compatibilidad)
*
PARAMETERS d_ingles
PRIVATE retorno
retorno = ''
DO CASE
CASE d_ingles = 'SUNDAY'
	retorno = 'Domingo'
CASE d_ingles = 'MONDAY'
	retorno = 'Lunes'
CASE d_ingles = 'TUESDAY'
	retorno = 'Martes'
CASE d_ingles = 'WEDNESDAY'
	retorno = 'Miercoles'
CASE d_ingles = 'THURSDAY'
	retorno = 'Jueves'
CASE d_ingles = 'FRIDAY'
	retorno = 'Viernes'
CASE d_ingles = 'SATURDAY'
	retorno = 'Sabado'
ENDCASE
RETURN retorno
