*************************************************
* FUNCTION LeeSigRt
*************************************************
* 
* Funcionamiento:
* 
* Se utiliza desde el reporte de la O.P.
* La finalidad es de cargar un vector de N posiciones, con las
* N retenciones siguientes desde la �ltima impresa.
* 
PARAMETERS cantidad
PRIVATE retorno, i
retorno = ''

IF Type ('DescRet[1]') != 'C'
	DIMENSION DescRet[m.cantidad]
	DIMENSION ImpRet[m.cantidad]
ENDIF

IF ALen (DescRet) < m.cantidad
	DIMENSION DescRet[m.cantidad]
	DIMENSION ImpRet[m.cantidad]
ENDIF

FOR m.i = 1 TO m.cantidad
	DescRet[m.i] = ''
	ImpRet[m.i] = 0
ENDFOR

IF _pageno = 1
	GO TOP IN c_ret
ENDIF

m.i = 3
DO WHILE m.i <= m.cantidad AND !Eof ('c_ret')

	IF c_ret.desc = 'GANAN'
		DescRet[1] = c_ret.desc
		ImpRet[1] = c_ret.valor

		SKIP IN c_ret
		LOOP
	ENDIF

	IF c_ret.desc = 'INBRU'
		DescRet[2] = c_ret.desc
		ImpRet[2] = c_ret.valor

		SKIP IN c_ret
		LOOP
	ENDIF

	DescRet[m.i] = c_ret.desc
	ImpRet[m.i] = c_ret.valor

	SKIP IN c_ret
	m.i = m.i + 1
ENDDO

RETURN retorno
