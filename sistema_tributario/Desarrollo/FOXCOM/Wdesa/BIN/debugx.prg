*************************************************
FUNCTION DEBUG
*************************************************
* Autor: Leonardo
* 
* Fecha:
* 
* Funcionamiento:
* 	Permite browsear las tablas abiertas
* 
* 
*
* Carga las areas en un vector
* abre una ventana para seleccionar la tabla
* permite abrir nuevas tablas
* puede browsear las tablas

PRIVATE i, sele_ant, debstatus
PRIVATE tablas, j, k, oo, l_debug, tipo_brow, relacs
PRIVATE deb_campo,deb_tabla, m.vs_retorna, onerr

sele_ant = SELE ()
OO			 = .F.

onerr    = ON ('ERROR')
ON ERROR vari_brow = ERROR ()

tipo_brow = .F.
IF TYPE ('_server') <> 'U' 
	IF _SERVER = 'SICO'
		tipo_brow = .T.
	ENDIF
ENDIF
	

DEFINE WINDOW WIN_DEBUG1 ;
	FROM	1, 1 TO 24, 78 ;
	FLOAT ;
	NOCLOSE ;
	SHADOW ;
	DOUBLE ;
	COLOR SCHEME 1

ACTIVATE WINDOW WIN_DEBUG1 NOSHOW

DIMENSION tablas [225]

j = 1
FOR i = 1 TO 225
	SELECT (i)
	IF !EMPTY (ALIAS ())
		tablas [i] = PADR (ALIAS (), 12) + STR (RECC(),7) + ' ' + ORDER ()
	ELSE
		tablas [i] = STR (i, 4)
	ENDIF
ENDFOR

SELE (sele_ant)

ON KEY LABEL CTRL+W

DO WHILE !OO
	=debugint()
ENDDO

IF SET ('DEBUG') # 'ON'
	ON KEY LABEL CTRL+W *
ENDIF

RELEASE WINDOW WIN_DEBUG1

SELE (sele_ant)

ON ERROR &ONERR
* Fin del Programa DEBUG.PRG


*******************************************************
FUNCTION debugint

PRIVATE area_ant, relacs
area_ant = ALIAS()
relacs   = 0

l_debug = SELE ()
@ 0,1 GET l_debug FROM tablas PICTURE "@&T" ;
	SIZE 10, 74 ;
	VALID br_debug () WHEN whe_lista ()

@ 10, 2 SAY 'Tabla               : '
@ 11, 2 SAY 'Clave               : '
@ 12, 2 SAY 'Indice              : '
@ 13, 2 SAY 'Variable            : '

debindice = SPAC (10)
@ 12,24 GET debindice VALID deb_indice ()

deb_campo = SPAC (20)
@ 13,24 GET deb_campo VALID deb_campov ()

IF !EMPTY (VARREAD ())
	@ 14,2 SAY 'VARREAD             : ' + VARREAD ()
ENDIF

IF TYPE ('gs_num_cpo') # 'U'
	@ 15,2  SAY 'gs_num_cpo          : ' + OBJV (gs_num_cpo)
ENDIF

IF TYPE ('gs_valor_cpo') # 'U'
	@ 16,2   SAY 'gs_valor_c          : ' 
	@ 16,24  SAY gs_valor_c 
ENDIF
IF TYPE ('grup_mem') # 'U'
	@ 17,2  SAY 'grup_mem            : ' + grup_mem
ENDIF

@ 18,2  SAY 'Abrir               : '
deb_tabla = SPAC(33)
@ 18,24 GET deb_tabla ;
	VALID deb_tablav ()

IF tipo_brow
	@ 19,2 SAY 'Ejecutar o Ver      : '
	deb_run = SPAC(33)
	@ 19,24 GET deb_run ;
		VALID deb_runv ()
ENDIF

@ 20,2 SAY 'DBFs Directorio     : ' 

dirdeb  = SPAC(33)
@ 20,24 GET dirdeb VALID recordeb (dirdeb)

@ 21,1 GET debmemo PICT '@* Disp Memory' DEFA 0 ;
	VALID val_memo ()

@ 21,15 GET debstatus PICT '@* Disp Status' DEFA 0 ;
	VALID val_stat ()

@ 21,29 GET debstatus PICT '@* Archivos Libres' DEFA 0 ;
	VALID val_arch ()

@ 21,47 GET relacs PICT '@* Relaciones' VALID relacs ()

@ 21,65 GET m.vs_retorna ;
	PICTURE "@*HN \?\<Retornar" ;
	SIZE 1,10,1 ;
	DEFAULT 1 ;
	VALID PPPPP()

ACTIVATE WINDOW WIN_DEBUG1
READ CYCLE TIMEOUT 600 

*************************************
FUNCTION PPPPP
*************************************
CLEA READ
OO = .T.
RETU
	
*************************************	
FUNCTION br_debug
*************************************
PRIVATE archivo, tabla

IF SUBS (tablas[l_debug], 1, 2) <> '  '
	tabla = ALLT (SUBS (tablas[l_debug], 1, 10))
ELSE
	tabla =	l_debug
ENDIF
SELE (tabla)

IF EMPTY (ALIAS())
	archivo = LOCFILE ('','dbf')
	IF TYPE ('archivo') = 'C'
		USE (archivo) ORDER 1
		tablas [l_debug	] = PADR (ALIAS (), 12) + STR (RECC(),7) + ' ' + ORDER ()
	ENDIF
ENDIF

IF !EMPTY (ALIAS())

	DEFINE WINDOW WIN_BROW ;
		FROM	1,1 TO SROW()-1, SCOL()-1 ;
		FLOAT ;
		NOCLOSE ;
		DOUBLE ;
		COLOR SCHEME 11
		
	ACTI WIND WIN_BROW
	PRIVATE rec_ant
	rec_ant = RECNO()

	IF tipo_brow
		BROW IN WIND WIN_BROW 
	ELSE
		BROW NODELETE NOAPPEND NOMODIFY IN WIND WIN_BROW 
	ENDIF
	
	RELE WIND WIN_BROW
	=IR_REG (rec_ant)

ENDIF
	
*********************************
FUNC deb_campov

*No permite evaluar funciones fuera de SICO

IF deb_campo > '                    '
	IF tipo_brow
		IF TYPE ('deb_campo') <> "U"
			@ 12, 44 SAY EVAL (deb_campo)
		ELSE
			@ 12, 44 SAY EVAL (deb_campo)
		ENDIF
	ELSE
		IF TYPE ('deb_campo') <> "U"
			@ 12, 44 SAY EVAL (deb_campo)
		ENDIF
	ENDIF			
ELSE
	@ 12, 24 SAY '                              '
ENDIF

*********************************
FUNC deb_indice

IF !EMPTY (debindice)
	tabla = ALLT (SUBS (tablas[l_debug], 1, 10))
	SELECT (tabla)
	
	SET ORDER TO debindice
	i = SELECT ()

	tablas [i] = PADR (ALIAS (), 12) + STR (RECC(),7) + ' ' + ORDER ()
	l_debug = i
	SHOW GET l_debug
ENDIF
debindice = SPAC (10)


*********************************
FUNC deb_tablav

IF !EMPTY ('deb_tabla')
	IF TYPE ('deb_tabla') = 'C' AND !EMPTY (deb_tabla)
		SELE 0
		tabla = ALLTRIM (deb_tabla)
		USE (tabla) ORDER 1
		i = SELECT ()

		tablas [i] = PADR (ALIAS (), 12) + STR (RECC(),7) + ' ' + ORDER ()
		l_debug = i
		SHOW GET l_debug
	ENDIF
ENDIF
deb_tabla = SPAC(33)

*************************************
FUNCTION val_stat
*************************************
PRIVATE arch_stat
arch_stat = SYS (2015)

DISPLAY status TO &arch_stat NOCONSOLE
modi file &arch_stat..txt
DELETE FILE &arch_stat..txt

*************************************
FUNCTION val_memo
*************************************
PRIVATE arch_stat
arch_stat = SYS (2015)

DISPLAY memory TO &arch_stat NOCONSOLE
modi file &arch_stat..txt
DELETE FILE &arch_stat..txt

*************************************
FUNCTION val_arch
*************************************
PRIVATE a, b, i, j
DIME a[255], b[255]

FOR i = 1 TO 255
	a[i] = sys(3)
	b[i] = fcrea (a[i])
	IF b[i] = -1
		EXIT
	ENDIF
ENDFOR

FOR j = 1 TO i - 1
	=FCLOSE (b[j])
ENDFOR

WAIT WIND 'Cantidad de archivos disponibles: ' + STR (i - 1 ,3)


*********************************
FUNC deb_runv

IF deb_run > '                                '
	IF AT ('command', deb_run) > 0 AND ! tipo_brow
		WAIT WIND "No puede ejecutar el command."
	ELSE
		PRIVA adeb_run 
		adeb_run = 'RUN ' + deb_run
		&adeb_run
	ENDIF
ENDIF
deb_run = SPAC(33)


***********************************
FUNC recordeb
PARA directorio

PRIVATE a
a = 1

IF EMPTY (directorio)
	RETURN
ENDIF

directorio = directorio + '\'

DIME adbf[1]

=ADIR (adbf, (directorio + '*.DBF'))

dbfs = ALEN (adbf, 1)
IF dbfs = 1
	WAIT WIND 'No hay tablas en el directorio.'
	RETURN
ENDI

DIME adbfa [dbfs]

SELE 0

FOR i = 1 TO dbfs
	USE directorio + adbf [i,1] alias recordeb
	adbfa [i] =  PADR (adbf [i,1], 10) + STR (recc ()) + STR (adbf[i,2])
ENDF
USE

=ASORT (adbfa)

@ 2,2 MENU adbfa, dbfs
READ MENU TO a
* Se limpia la variable del directorio
dirdeb  = SPAC(33)


***********************************
FUNCTION relacs

PRIVATE relac, a, j, i, sele_ant
DIMENSION relac [200]

sele_ant = SELE ()

j = 0
FOR i = 1 TO 225
	SELECT (i)
	IF !EMPTY (ALIAS ())
		k = 1
		DO WHILE ! EMPTY (TARGET (k))
			j = j + 1
			relac [j] = PADR (TARGET (k),11) + PADR (RELA (k), 11)
			k = k + 1
		ENDDO
	ENDIF
ENDFOR

IF j = 0
	WAIT WIND "No hay relaciones entre tablas."
ELSE
	DIME relac [j]
	@ 2,2 MENU relac, j
	READ MENU TO a
ENDIF

SELE (sele_ant)

*************************************
FUNCTION whe_lista

PRIVATE area
SELE (l_debug)
alias = SELE ()

@ 10, 24 SAY SPAC (52)
@ 11, 24 SAY SPAC (52)
IF !EMPTY (alias)
	@ 10, 24 SAY DBF ()
	IF !EMPTY (SYS (14,1, alias))
		@ 11, 24 SAY EVAL (SYS (14,1, alias))
	ENDIF
	debindice = PADR (TAG (), 10)
	SHOW GET debindice
ENDIF
