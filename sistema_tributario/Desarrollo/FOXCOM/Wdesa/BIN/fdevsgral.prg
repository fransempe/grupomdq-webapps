*************************************************
FUNCTION FDevSgral
*************************************************
* 
* Funcionamiento:
* Funci�n de c�lculo de tasa Servicios Generales para Escobar
* 
* Modificado: 19/12/2006

PRIVATE minimo, importe, valor1, valor2
m.minimo = 0
m.importe = 0
m.valor2 = 0

IF m.cuota = 13 AND !EMPTY (REC_IMP.COD_DEBAUT) AND EMPTY(REC_IMP.FECBAJ_DEB) OR REC_IMP.FECBAJ_DEB >= date()
	RETURN 0
ENDIF

DO CASE
	CASE inmueble.cod_zona = '01' 	
		* Se toma hasta 20 mts
		IF inmueble.met_fte1 + inmueble.met_fte2 <= 20
			m.valor1 = (inmueble.met_fte1 + inmueble.met_fte2) * 2.75
		ELSE
			m.valor1 = 20 * 2.75
			* La diferencia entre el total de mts y 20, se fracciona por cada 5 mts o fraccion menor
			m.valor2 = ((inmueble.met_fte1 + inmueble.met_fte2 - 20) / 5)
			m.valor2 = CEILING (m.valor2) * 2.75
		ENDIF
		m.importe = m.valor1 + m.valor2
	CASE inmueble.cod_zona = '2A' 
		* Se toma hasta 20 mts
		IF inmueble.met_fte1 + inmueble.met_fte2 <= 20
			m.valor1 = (inmueble.met_fte1 + inmueble.met_fte2) * 1.89
		ELSE
			m.valor1 = 20 * 1.89
			* La diferencia entre el total de mts y 20, se fracciona por cada 5 mts o fraccion menor
			m.valor2 = ((inmueble.met_fte1 + inmueble.met_fte2 - 20) / 5)
			m.valor2 = CEILING (m.valor2) * 1.89
		ENDIF
		m.importe = m.valor1 + m.valor2
	CASE inmueble.cod_zona = '2B' 
		* Se toma hasta 20 mts
		IF inmueble.met_fte1 + inmueble.met_fte2 <= 20
			m.valor1 = (inmueble.met_fte1 + inmueble.met_fte2) * 1.18
		ELSE
			m.valor1 = 20 * 1.18
			* La diferencia entre el total de mts y 20, se fracciona por cada 5 mts o fraccion menor
			m.valor2 = ((inmueble.met_fte1 + inmueble.met_fte2 - 20) / 5)
			m.valor2 = CEILING (m.valor2) * 1.18
		ENDIF
		m.importe = m.valor1 + m.valor2
	CASE inmueble.cod_zona = '2C'
		* Se toma hasta 20 mts
		IF inmueble.met_fte1 + inmueble.met_fte2 <= 20
			m.valor1 = (inmueble.met_fte1 + inmueble.met_fte2) * 1.13
		ELSE
			m.valor1 = 20 * 1.13
			* La diferencia entre el total de mts y 20, se fracciona por cada 5 mts o fraccion menor
			m.valor2 = ((inmueble.met_fte1 + inmueble.met_fte2 - 20) / 5)
			m.valor2 = CEILING (m.valor2) * 1.13
		ENDIF
		m.importe = m.valor1 + m.valor2	
	CASE inmueble.cod_zona = '3A' OR inmueble.cod_zona = '3B' OR inmueble.cod_zona = '3C'
		* Se toma hasta 20 mts
		IF inmueble.met_fte1 + inmueble.met_fte2 <= 20
			m.valor1 = (inmueble.met_fte1 + inmueble.met_fte2) * 2.70
		ELSE
			m.valor1 = 20 * 2.70
			* La diferencia entre el total de mts y 20, se fracciona por cada 5 mts o fraccion menor
			m.valor2 = ((inmueble.met_fte1 + inmueble.met_fte2 - 20) / 5)
			m.valor2 = CEILING (m.valor2) * 2.70
		ENDIF
		m.importe = m.valor1 + m.valor2
	CASE inmueble.cod_zona = '04' 
		* Se toma hasta 20 mts
		IF inmueble.met_fte1 + inmueble.met_fte2 <= 20
			m.valor1 = (inmueble.met_fte1 + inmueble.met_fte2) * 0.85
		ELSE
			m.valor1 = 20 * 0.85
			* La diferencia entre el total de mts y 20, se fracciona por cada 5 mts o fraccion menor
			m.valor2 = ((inmueble.met_fte1 + inmueble.met_fte2 - 20) / 5)
			m.valor2 = CEILING (m.valor2) * 0.85
		ENDIF
		m.importe = m.valor1 + m.valor2	
	CASE inmueble.cod_zona = '6A' 
		* Se calcula por hectarea
		m.importe = inmueble.sup_ter / 10000 * 31.40
		* minimo mensual 6A
		m.minimo = 160.00
		IF m.importe < m.minimo 
			m.importe = 160.00
		ENDIF
	CASE inmueble.cod_zona = '6B' 
		* Se toma hasta 20 mts
		IF inmueble.met_fte1 + inmueble.met_fte2 <= 20
			m.valor1 = (inmueble.met_fte1 + inmueble.met_fte2) * 4.30
		ELSE
			m.valor1 = 20 * 4.30
			* La diferencia entre el total de mts y 20, se fracciona por cada 5 mts o fraccion menor
			m.valor2 = ((inmueble.met_fte1 + inmueble.met_fte2 - 20) / 5)
			m.valor2 = CEILING (m.valor2) * 4.30
		ENDIF
		m.importe = m.valor1 + m.valor2	
		* minimo mensual 6B
		m.minimo = 210.00
		IF m.importe < m.minimo 
			m.importe = m.minimo
		ENDIF
	CASE inmueble.cod_zona = '6C' 
		* Se toma hasta 20 mts
		IF inmueble.met_fte1 + inmueble.met_fte2 <= 20
			m.valor1 = (inmueble.met_fte1 + inmueble.met_fte2) * 2.45
		ELSE
			m.valor1 = 20 * 2.45
			* La diferencia entre el total de mts y 20, se fracciona por cada 5 mts o fraccion menor
			m.valor2 = ((inmueble.met_fte1 + inmueble.met_fte2 - 20) / 5)
			m.valor2 = CEILING (m.valor2) * 2.45
		ENDIF
		m.importe = m.valor1 + m.valor2	
		* minimo mensual 6C
		m.minimo = 185.00
		IF m.importe < m.minimo 
			m.importe = m.minimo
		ENDIF
	CASE inmueble.cod_zona = '7A' OR inmueble.cod_zona = '7B' OR inmueble.cod_zona = '7C' 
		* Se calcula por hectarea
		m.importe = inmueble.sup_ter / 10000 * 1.00
		* minimo mensual 7A
		m.minimo = 30.00
		IF m.importe < m.minimo 
			m.importe = m.minimo
		ENDIF
	CASE inmueble.cod_zona = '08' 
		* Se toma hasta 20 mts
		IF inmueble.met_fte1 + inmueble.met_fte2 <= 20
			m.valor1 = (inmueble.met_fte1 + inmueble.met_fte2) * 4.00
		ELSE
			m.valor1 = 20 * 4.00
			* La diferencia entre el total de mts y 20, se fracciona por cada 5 mts o fraccion menor
			m.valor2 = ((inmueble.met_fte1 + inmueble.met_fte2 - 20) / 5)
			m.valor2 = CEILING (m.valor2) * 4.00
		ENDIF
		m.importe = m.valor1 + m.valor2	
	CASE inmueble.cod_zona = '09' 
		DO CASE
			CASE inmueble.met_fte1 = 2
				m.importe = 14.00
			CASE inmueble.met_fte1 = 3
				m.importe = 17.00
			CASE inmueble.met_fte1 = 4
				m.importe = 20.00
		ENDCASE

	CASE inmueble.cod_zona = '10' 
		* Se toma hasta 20 mts
		IF inmueble.met_fte1 + inmueble.met_fte2 <= 20
			m.valor1 = (inmueble.met_fte1 + inmueble.met_fte2) * 0.45
		ELSE
			m.valor1 = 20 * 0.45
			* La diferencia entre el total de mts y 20, se fracciona por cada 5 mts o fraccion menor
			m.valor2 = ((inmueble.met_fte1 + inmueble.met_fte2 - 20) / 5)
			m.valor2 = CEILING (m.valor2) * 0.45
		ENDIF
		m.importe = m.valor1 + m.valor2	
	CASE inmueble.cod_zona = '11A' OR inmueble.cod_zona = '11B' OR inmueble.cod_zona = '11C' 
		* Se calcula por hectarea
		m.importe = inmueble.sup_ter / 10000 * 0.85
		* minimo mensual 11A
		m.minimo = 21.50
		IF m.importe < m.minimo 
			m.importe = m.minimo
		ENDIF
	CASE inmueble.cod_zona = '12A' 
		* Se calcula por hectarea
		m.importe = inmueble.sup_ter / 10000 * 15.60
		* minimo mensual 12A
		m.minimo = 78.00
		IF m.importe < m.minimo 
			m.importe = m.minimo
		ENDIF
	CASE inmueble.cod_zona = '12B' 
		* Se toma hasta 20 mts
		IF inmueble.met_fte1 + inmueble.met_fte2 <= 20
			m.valor1 = (inmueble.met_fte1 + inmueble.met_fte2) * 2.65
		ELSE
			m.valor1 = 20 * 2.65
			* La diferencia entre el total de mts y 20, se fracciona por cada 5 mts o fraccion menor
			m.valor2 = ((inmueble.met_fte1 + inmueble.met_fte2 - 20) / 5)
			m.valor2 = CEILING (m.valor2) * 2.65
		ENDIF
		m.importe = m.valor1 + m.valor2	
		m.minimo = 104.00
		IF m.importe < m.minimo 
			m.importe = m.minimo
		ENDIF	
ENDCASE
*
*   Contribuci�n para los Bomberos
*	Incremento del 10%. Va separado para que den los centavos como a AL Computaci�n
*
m.importe = ROUND (m.importe * 1.10, 2)   
*
*	Descuento por esquina � 2 frentes
*
m.importe = m.importe - (m.importe * inmueble.lagun_perm / 100)

IF m.cuota = 13
*	m.importe = m.importe * 12
	m.importe = m.importe * 0.90 * 12
ENDIF

RETURN m.importe