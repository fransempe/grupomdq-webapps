*************************************************
* FUNCTION Val_Form
*************************************************
* Autor  : Gabriel Zubieta
* Dise�o : Pachu
* Fecha  : 10/02/95
* 
* Funcionamiento:
* Dada una fla. y n tipo debe evaluarla y validarla contra el tipo 
* devolviendo .T. o .F.
* 
* 
* Par�metros:
* formula  -C- Fla para evaluar.
* Tipofla  -C- Tipo que debe ser. Caracter, Numerica, Logica, etc.
* noemimsg -B- Flag si emite no emite mensaje.
* alias		 -C- nombre de alias donde debe posicionarse para evaluar la expresi�n
*
* Modificaciones:
* Gabriel 06/03/95 12,30 Agregue SELECT 0 para obligar a poner alias en exp. 
* Gabriel 19/04/95 11,30 Agregue 3er parametro para que indique si no se emite msg.
*
PARAMETERS m.formula, m.tipofla, m.noemimsg, m.alias
PRIVATE retorno, m.nerror, onerror, m.exp, m.tipo, area_ant
retorno = .T.

area_ant = SELECT()
IF !EMPTY(m.alias)
	SELECT (m.alias)
ELSE
	SELECT 0
ENDIF
onerror = ON('error')
ON ERROR m.nerror = message()
m.nerror = " "
m.exp = EVAL( ALLT(m.formula) )	
ON ERROR &onerror
SELECT( area_ant )
	
IF !(m.nerror == " " )
	IF !m.noemimsg
	 	=mens_avi( m.sistema, 'EXPRESION INVALIDA')
	ENDIF
	retorno = .F.	
ELSE
	IF m.tipofla = 'N' AND STR(m.exp) = '*'
		retorno = .F.
	ENDIF
ENDIF
	
IF retorno 
	m.tipo = TYPE('m.exp')
	IF !( m.tipo == m.tipofla )
		tipo_form = desc_tip(m.tipo)
		tipo_val  = desc_tip(m.tipofla)
		IF !m.noemimsg
			=mens_avi( m.sistema, 'EXPRES.CON TIPO DE DATO INVAL.')
		ENDIF
		retorno = .F.
	ENDIF
ENDIF

RETURN retorno
*


*******************************************
FUNCTION Desc_tip
*******************************************
PARAMETERS m.tip
PRIVATE retorno

DO CASE
CASE m.tip = 'C'
	retorno = 'CARACTER'
CASE m.tip = 'N'
	retorno = 'NUMERICO'
CASE m.tip = 'L'
	retorno = 'LOGICO'
CASE m.tip = 'U'
	retorno = 'INDEFINIDO'
OTHERWISE
	retorno = 'DESCONOCIDO'
ENDCASE

RETURN retorno
*