*************************************************
* FUNCTION FVBorImp
*************************************************
* Autor  : Gabriel Zubieta
* Dise�o : Pachu
* Fecha  : 23/02/95
* 
* Funcionamiento:
*  Verifica para un imponible
*  - No debe tener vinculaciones con recursos
*  - No debe registrar deuda en la cuenta corriente
*  - No debe tener vinculaciones con otros imponibles 
*
* Par�metros:
*  TipImp  -C- Tipo de Imponible
*  Nro     -C- Numero de Imponible correspondiente ( Cuit, Nro o Nro )
*  SaleMsg -L- Flag indicador si sale mensaje.
*
* Retorno : True si el imnponible esta en condiciones de darse de baja, False
*           en caso contrario. 
* 
PARAMETERS m.tipimp, m.nro, m.salemsg
=VerNuPar ('FVBORIMP', PARAMETERS(), 3 )
=VerTiPar ('FVBORIMP','M.TIPIMP'   ,'C')
=VerTiPar ('FVBORIMP','M.NRO'      ,'N')
=VerTiPar ('FVBORIMP','M.SALEMSG'  ,'L')

PRIVATE retorno

=USET ('recur\codifics') 
=USET ('recur\vinc_imp')
=USET ('recur\rec_imp' )
=USET ('recur\rec_cc'  )

retorno = FImpSRec( m.tipimp, m.nro, .T. )
IF retorno
	retorno = FImpSDe( m.tipimp, m.nro, .T. )
	IF retorno
		retorno = FImpSVin( m.tipimp, m.nro, .T. )
	ENDIF
ENDIF

IF !retorno AND m.SaleMsg
	=mens_avi( m.sistema , "IMPON. NO PUEDE DARSE DE BAJA" )
ENDIF

RETURN retorno
*
