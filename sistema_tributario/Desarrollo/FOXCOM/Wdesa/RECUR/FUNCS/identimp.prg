*************************************************
*FUNCTION IDENTIMP
*************************************************
* Autor : Rodrigo
* Dise�o: Rodrigo
* 
* Fecha : 29/03/95
* 
* Funcionamiento: Devuelve la descripci�n del tipo de imponible
* 								extra�da de la Fx Val_Form() que evalua el 
*									cpo pasado como par�metro
*
* Par�metros:			m.tipo			(C)	: Campo a evaluar
* 								@m.desc			(C)	:	Descripci�n evaluada (REF)
* 
PARAMETERS m.tipo, m.desc
PRIVATE retorno
EXTERNAL ARRAY ident_imp
retorno = .T.
IF VAL_FORM ( ident_imp [ ASC(m.tipo) - 64 ], 'C')
	m.desc 	= EVAL ( ident_imp [ ASC(m.tipo) - 64 ])
ELSE
	m.desc	=	' '
	retorno = .F.
ENDIF
RETURN retorno
