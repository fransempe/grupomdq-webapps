*************************************************
* FUNCTION FId_Imp
*************************************************
* Autor : Gabriel Zubieta
* Dise�o: Pachu
* 
* Fecha : 10/04/95
* 
* Funcionamiento:
* Condicion necesaria es haber llamado antes a la funcion FCarmas.
* 
* Par�metros:
* tipoimp -C- Tipo de Imponible sobre el cual se quiere evaluar la exp.
* 
PARAMETERS m.tipoimp
EXTERNAL ARRAY ident_exp
PRIVATE retorno, onerror, expr
onerror = on('error')
IF !EMPTY(m.tipoimp)
	expr = ALLT( ident_exp[ ASC(m.tipoimp)-64 ] )
	IF Val_form( expr , 'C', .T. ) AND !EMPTY(expr)
		retorno = EVAL( expr )
	ELSE
		retorno = ''
	ENDIF
ELSE
	retorno = ''
ENDIF
RETURN retorno

