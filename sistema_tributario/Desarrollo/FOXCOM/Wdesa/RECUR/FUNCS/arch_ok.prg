*************************************************
FUNCTION ARCH_OK
*************************************************
PARAMETERS path_arch
PRIVATE retorno, m.posic, m.long_path, m.caracter, m.archivo, m.encontre
PRIVATE m.long_arch
retorno = .T.

m.archivo	= ''
m.direct	= ''
m.path_arch = ALLT(m.path_arch)
m.long_path = LEN(m.path_arch)

IF m.long_path > 0
	m.posic		= m.long_path
	m.encontre	= .F.
	DO WHILE m.posic > 0 AND !m.encontre
		m.caracter = SUBSTR(m.path_arch, m.posic, 1)

		IF m.caracter = '\' OR m.caracter = ':'
			m.encontre = .T.
		ELSE
			m.posic = m.posic - 1
		ENDIF
	ENDDO
	m.direct  = SUBSTR(m.path_arch, 1, m.posic)
	m.archivo = SUBSTR(m.path_arch, m.posic + 1, m.long_path)
ENDIF

IF !EMPTY(m.direct)
	IF ISALPHA(m.direct) AND SUBSTR(m.direct, 2, 1) = ':'
		retorno = DIREC_OK(m.direct, .T.)
	ELSE
		retorno = DIREC_OK(m.direct, .F.)
	ENDIF
ENDIF

IF retorno AND !EMPTY(m.archivo)
	let_val		= '_-.$'
	m.long_arch	= LEN(m.archivo)
	m.posic		= 1
	retorno		= .T.
	DO WHILE m.posic <= m.long_arch AND retorno
		m.caracter = SUBSTR(m.archivo, m.posic, 1)

		IF !ISALPHA(m.caracter) AND !ISDIGIT(m.caracter) AND !(m.caracter $ let_val)
			retorno = .F.
		ENDIF

		m.posic = m.posic + 1
	ENDDO
ELSE
	retorno = .F.
ENDIF

IF !retorno
	=MSG(path_arch + ' no es un archivo v�lido. Verifique.')
ENDIF

RETURN retorno
