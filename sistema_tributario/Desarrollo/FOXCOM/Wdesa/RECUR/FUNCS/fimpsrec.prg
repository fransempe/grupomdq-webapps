*************************************************
* FUNCTION FImpSRec
*************************************************
* Autor  : Gabriel Zubieta
* Dise�o : Pachu
* Fecha  : 23/02/95
* 
* Funcionamiento:
* 
* 
* 
* 
* Par�metros:
*  tipimp   -C- tipo de imponible
*  nro      -N- nro de imponible
*  salemsg  -L- flag si sale mensaje
*
* retorno
*  True si el imponible no tiene recursos vinculados, False en caso contrario 
*
* Modificaciones:
* 
PARAMETERS m.tipimp, m.nro, m.salemsg

IF SET ('DEBUG') = 'ON'
	=VerNuPar ('fimpsrec', PARAMETERS(), 3 )
	=VerTiPar ('fimpsrec','M.TIPIMP'   ,'C')
	=VerTiPar ('fimpsrec','M.NRO'      ,'N')
	=VerTiPar ('fimpsrec','M.SALEMSG'  ,'L')
ENDIF

PRIVATE retorno, m.tipo_imp, m.nro_imp, exp_rec
retorno = .T.

exp_rec = GETKEY( 'rec_imp' , .T. , 2 , 'primario' )

m.tipo_imp = m.tipimp
m.nro_imp  = m.nro
=SEEK( &exp_rec , 'rec_imp' )
DO WHILE ( !EOF('rec_imp' )               ) AND ;
				 ( m.tipo_imp = rec_imp.tipo_imp  ) AND ;
				 ( m.nro_imp  = rec_imp.nro_imp   ) AND ;
				 ( retorno )
	IF EMPTY( rec_imp.fec_baja )
		IF m.salemsg
			=mens_avi( m.sistema , "IMPONIBLE TIENE RECURSO" )
		ENDIF
		retorno = .F.
	ELSE
		SKIP IN rec_imp
	ENDIF
ENDDO

RETURN retorno

