*************************************************
* FUNCTION FImpCB
*************************************************
* Autor : Gabriel Zubieta
* Dise�o: Idem
* 
* Fecha : 01/04/95
* 
* Funcionamiento:
* Arma las 4 l�neas donde sale el c�digo de barra.
* 
* Par�metros:
* i = 1 .. 4
* ai   -C- Texto anterior a codigo de barra en i linea
* pi   -C- Texto posterior a codigo de barra en i linea
* posh -N- Posicion donde comienza el codigo de barra
*
* Modificaciones:
* 
PARAMETERS m.a1, m.p1, m.a2, m.p2, m.a3, m.p3, m.a4, m.p4, m.posh
PRIVATE retorno, m.nro_grup, m.ini, m.fin, m.expCB
retorno = ''

m.nro_grup = STR(dev_comp.grupo_comp,2) + STR(dev_comp.nro_comp,9) + STR(dev_comp.dv_comp,2)
m.nro_grup = STRTRAN( m.nro_grup , ' ' , '0' )

m.expCB = ''
m.ini   = ''
m.fin   = ''

IF !EMPTY( m.esc_ini )
	m.ini = EVAL( ALLT( m.esc_ini ) )
ENDIF
m.expCB = EVAL( ALLT( m.secuencia) )   && FCBi25( m.nro_grup )
IF !EMPTY( m.esc_fin )
	m.fin = EVAL( ALLT( m.esc_fin ) )
ENDIF

IF TYPE('m.a1') = 'C'
	IF TYPE('m.p1') != 'C'
		m.p1 = ''
	ENDIF
	
	??? PADR( m.a1 , m.posh )
	??? m.ini
	??? m.expCB + m.fin
	??? m.p1 + CHR(13) + CHR(10)
	
ENDIF    

IF TYPE('m.a2') = 'C'
	IF TYPE('m.p2') != 'C'
		m.p2 = ''
	ENDIF
	
	??? PADR( m.a2 , m.posh )
	??? m.ini
	??? m.expCB + m.fin
	??? m.p2 + CHR(13) + CHR(10)
	
ENDIF    

IF TYPE('m.a3') = 'C'
	IF TYPE('m.p3') != 'C'
		m.p3 = ''
	ENDIF
	
	??? PADR( m.a3 , m.posh )
	??? m.ini
	??? m.expCB + m.fin
	??? m.p3 + CHR(13) + CHR(10)
	
ENDIF    

IF TYPE('m.a4') = 'C'
	IF TYPE('m.p4') != 'C'
		m.p4 = ''
	ENDIF
	
	??? PADR( m.a4 , m.posh )
	??? m.ini
	??? m.expCB + m.fin
	??? m.p4 + CHR(13) + CHR(10)
	
ENDIF    


RETURN retorno

