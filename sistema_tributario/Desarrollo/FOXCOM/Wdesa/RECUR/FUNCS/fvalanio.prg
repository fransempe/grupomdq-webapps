*************************************************
* FUNCTION FValAnio
*************************************************
* Autor : Carlos
* Dise�o: Carlos
* 
* Fecha : 07/03/1995
* 
* Funcionamiento:
* Recibe un a�o pasado como par�metro por referencia,
* y verifica si es v�lido.
* Retorna falso si es < 0 o >= 100 y < a�o actual - 100 o > a�o actual + 100.
* Sino, retorna verdadero.
* Si el a�o est� entre 0 y 99, lo transforma a 1995 o 2003 por ejemplo.
* Si es <= 50, le suma 2000, sino, le suma 1900.
* 
* Par�metros:
* anio			: (N)				Es el n�mero de a�o a verificar. (por referencia)
* anio_cero	: (L) [.T.]	Si es .T., permite un a�o 0, sino, si el
*												a�o es 0, lo transforma a 2000.
* mensajes	: (L) [.T.]	Si es .T., emite mensaje de error si el a�o
*												es inv�lido.
* 
* Modificaciones:
* 
PARAMETERS anio, anio_cero, mensajes
PRIVATE retorno, parametros

m.parametros = PARAMETERS ()

IF SET ('DEBUG') = 'ON'
	=VerTiPar ('FValAnio', 'anio', 'N')
ENDIF

IF m.parametros > 1
	=VerTiPar ('FValAnio', 'anio_cero', 'L')
	IF m.parametros > 2
		=VerTiPar ('FValAnio', 'mensajes', 'L')
	ELSE
		m.mensajes = .T.
	ENDIF
ELSE
	m.anio_cero = .T.
	m.mensajes = .T.
ENDIF

retorno = .T.

IF m.anio = 0
	IF ! m.anio_cero
		m.anio = 2000
	ENDIF
ELSE
	IF (m.anio < 0) OR ;
		 (m.anio >= 100 AND m.anio < YEAR (DATE ()) - 100) OR ;
		 (m.anio > YEAR (DATE ()) + 100)
		retorno = .F.
		IF m.mensajes
			= mens_avi ('RECUR   ', 'A�O INCORRECTO')
		ENDIF
	ELSE
		IF m.anio > 0 AND m.anio <= 50
			m.anio = m.anio + 2000
		ELSE
			IF m.anio > 50 AND m.anio < 100
				m.anio = m.anio + 1900
			ENDIF
		ENDIF
	ENDIF
ENDIF

RETURN retorno
