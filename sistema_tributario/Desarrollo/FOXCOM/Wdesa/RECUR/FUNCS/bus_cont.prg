*************************************************
*************************************************
* Autor : Gabriel Zubieta
* Dise�o: Idem
* 
* Fecha : 15/03/95
* 
* Par�metros
* alias -C- Alias que se utizando actualmente.
* t_imp -C- Tipo de Imponible 
* cpo   -C- Nombre de cpo para vincular
*
* Funcionamiento:
* Busca un contribuyente dado un nro de alias.
* 
* Modificaciones:
* 
PARAMETERS m.alias, m.t_imp, cpo
PRIVATE retorno, m.cuit1, m.cuit2

m.cuit1 = -1
m.cuit2 = -2
retorno = FVinCon( m.t_imp , EVAL( m.alias + '.' + cpo ) , .F. , .F. , @m.cuit1 , @m.cuit2 )

RETURN retorno
*

