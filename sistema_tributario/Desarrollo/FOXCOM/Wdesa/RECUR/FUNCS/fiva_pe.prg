*************************************************
*FUNCTION FIva_PE
*************************************************
* Autor :Elio
* Dise�o:Pachu
* 
* Fecha :28-02-95
* 
* Funcionamiento: carga en variables globales la configuracion del IVA
* 								Percepcion (porcentaje, importe minimo y concepto)
* 
* 
* Par�metros:	m.recurso (C) 
*				m.msg     (L) : emite mensajes de aviso ?
* 				m.cheq_rec(L) : Chequea o no el recurso
* 
* Modificaciones:
* IMPORTANTE : para trabajar con esta funci�n se deben declarar en el
* 						 proceso principal las siguientes variables:
*		ivape_conf (L): IVA PE Configurado ## debe estar inicializada como .F.
*		conc_ivape (N): Concepto de IVA Percepcion
*		por_ivape  (N): Porcentaje de Iva Percepcion
*		min_ivape  (N): Minimo de Iva Percepcion
* 
PARAMETER m.recurso, m.msg, m.cheq_rec
IF SET ('DEBUG') = 'ON'
	=VerNuPar ('FIVA_PE', PARAMETERS()	, 3 )
	=VerTiPar ('FIVA_PE', 'M.RECURSO'	,'C')
	=VerTiPar ('FIVA_PE', 'M.MSG'       ,'L')
	=VerTiPar ('FIVA_PE', 'M.CHEQ_REC'  ,'L')
ENDIF

IF TYPE('m.ivape_conf') # 'L' OR TYPE('m.conc_ivape') # 'N' OR TYPE('m.por_ivape') # 'N' OR TYPE('m.min_ivape') # 'N'
	= msg('Para correr esta funci�n "FIVA_PE", se deben declarar unas variables en el programa principal. Verifique la documentaci�n')
	RETURN .F.
ENDIF

PRIVATE retorno
retorno = .T.
m.ivape_conf = .F.
m.conc_ivape = get_para('RECUR', 'CONC_IVAPE')
IF !EMPTY(m.conc_ivape)
	m.conc_ivape = VAL(m.conc_ivape)
	IF m.conc_ivape = -1 OR (m.conc_ivape > 0 AND m.conc_ivape <= 9999)
		IF m.conc_ivape # -1
			IF m.cheq_rec
				retorno = FConDev(m.recurso, m.conc_ivape, m.msg)
			ELSE
				retorno	=	.T.
			ENDIF
			IF retorno
				m.por_ivape = get_para('RECUR', 'POR_IVAPE')
				IF !EMPTY(m.por_ivape)
					m.por_ivape = VAL(m.por_ivape)
					IF m.por_ivape >= 0 AND m.por_ivape <= 100
						m.min_ivape = get_para('RECUR', 'MIN_IVAPE')
						IF !EMPTY(m.min_ivape)
							m.min_ivape = VAL(m.min_ivape)
							IF m.min_ivape >= 0
								retorno = .T.
								m.ivape_conf = .T.
							ELSE
								retorno = .F.
								IF m.msg
									= mens_avi(sistema, 'MIN.IVA PE MAL CONFIGURADO')
								ENDIF
							ENDIF
						ELSE
							retorno = .F.
							IF m.msg
								= mens_avi(sistema, 'MIN.IVA PE NO CONFIGURADO')
							ENDIF
						ENDIF
					ELSE
						retorno = .F.
						IF m.msg
							= mens_avi(sistema, 'POR.IVA PE MAL CONFIGURADO')
						ENDIF
					ENDIF
				ELSE
					retorno = .F.
					IF m.msg
						= mens_avi(sistema, 'POR.IVA PE NO CONFIGURADO')
					ENDIF
				ENDIF
			ENDIF
		ELSE
			m.ivape_conf = .T.
			retorno = .T.
		ENDIF
	ELSE
		retorno = .F.
		= mens_avi(sistema, 'CONC. IVA PE MAL CONFIGURADO')
	ENDIF
ELSE
	retorno = .F.
	IF m.msg
		= mens_avi(sistema, 'CONC. IVA PE NO CONFIGURADO')
	ENDIF
ENDIF

		
RETURN retorno
