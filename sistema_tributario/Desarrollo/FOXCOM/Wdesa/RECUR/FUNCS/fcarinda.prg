*************************************************
* FUNCTION FCARINDA
*************************************************
* Autor : Leo
* Dise�o: Leo
* 
* Fecha : 16/3
* 
* Funcionamiento: Carga vectores de Indices a una fecha de actualizaci�n determinada
* Par�metros: usa indices de configuraci�n
*             indice de actualizaci�n
*             indice de interes
*             fecha de actualizaci�n
* Modificaciones:
* 
PARAMETERS usa_config, fecha_act, indicea, indicei
PRIVATE retorno, clave_ind, es_interes, clave_pind
retorno = .T.

*	
* SQLMODI19991220
*		Usuario							: Luj�n
*		Fecha								: 20/12/1999
*		Quien modific�			: Carlos
*		Detalle problema		: Debido a un parche, es necesario que las cuotas
*													de la cta. cte. se actualicen a�n si est�n vencidas.
*													
************************************************************************


IF SET ('DEBUG') = 'ON'
	= VerTiPar ('FCARINDA', 'usa_config', 'L')
	= VerTiPar ('FCARINDA', 'fecha_act', 'D')
ENDIF

IF usa_config
	indicea = PADR(GET_PARA ('RECUR', 'INDICE_ACTUAL'), 6)
	IF ! EMPTY (indicea)
		indicei = PADR(GET_PARA ('RECUR', 'INDICE_INTERES'), 6)
		IF EMPTY (indicei)
			= MENS_AVI ('RECUR', 'MAL CONFIG. INDICE_INTERES')
			= FINAL (15, 'Los �ndices de actualizaci�n est�n mal configurados. Se termina el programa.')
		ENDIF
	ELSE
		= MENS_AVI ('RECUR', 'MAL CONFIG. INDICE_ACTUAL')
		= FINAL (15, 'Los �ndices de actualizaci�n est�n mal configurados. Se termina el programa.')
	ENDIF
ELSE
	= VerTiPar ('FCARIND', 'indicea', 'C')
	= VerTiPar ('FCARIND', 'indicei', 'C')
ENDIF

IF TYPE ('vec_act') = 'U' OR TYPE ('vec_int') = 'U'
	= FINAL (20001, 'Error de Programa: No se crearon los vectores para los Indices de Actualizaci�n.')
ENDIF
IF TYPE ('anio_inic') = 'U' OR TYPE ('anio_cant') = 'U'
	= FINAL (20001, 'Error de Programa: No se crearon las variables de configuraci�n del a�o inicial o de la cantidad de a�os.')
ENDIF

anio_inic = VAL (GET_PARA ('RECUR', 'ANIO_INIC_ACTUA'))
IF anio_inic >= 1950
	anio_cant = VAL (GET_PARA ('RECUR', 'ANIO_CANT_ACTUA'))
	IF anio_cant + anio_inic < YEAR (DATE ())
		= MENS_AVI ('RECUR', 'MAL CONFIG. ANIO_CANT_ACTUA')
		= FINAL (15, 'Los �ndices de actualizaci�n est�n mal configurados. Se termina el programa.')
	ENDIF
ELSE
	= MENS_AVI ('RECUR', 'MAL CONFIG. ANIO_INIC_ACTUA')
	= FINAL (15, 'Los �ndices de actualizaci�n est�n mal configurados. Se termina el programa.')
ENDIF

*IF TYPE ('usa_fx_act') = 'U'
*	PUBLIC usa_fx_act
	usa_fx_act = GET_PARA ('RECUR', 'USA_FX_ACTUALIZ')
	IF !EMPTY (usa_fx_act)
		usa_fx_act = .T.
	ELSE
		usa_fx_act = .F.
	ENDIF
*ENDIF

*IF TYPE ('usa_fx_int') = 'U'
*	PUBLIC usa_fx_int
	usa_fx_int = GET_PARA ('RECUR', 'USA_FX_INTERES')
	IF !EMPTY (usa_fx_int)
		usa_fx_int = .T.
	ELSE
		usa_fx_int = .F.
	ENDIF
*ENDIF

* SQLMODI19991220
IF TYPE ('_int_noven') = 'U'
	PUBLIC m._int_noven
	m._int_noven = GET_PARA ('RECUR', 'ACT_INT_NO_VENC')
	IF m._int_noven != 'S'
		m._int_noven = 'N'
	ENDIF
ENDIF

=USET ('recur\indices')
clave_ind  = GETKEY ('indices',.T.)
clave_pind = GETKEY ('indices',.T.,1)

DIME vec_act [anio_cant*12,2]
DIME vec_int [anio_cant*12,2]

m.es_interes = .F.
retorno = CargaVector (indicea, @vec_act)

m.es_interes = .T.
retorno = retorno AND CargaVector (indicei, @vec_int)

RETURN retorno


*************************************************
FUNCTION CargaVector
*************************************************
* Autor : Leo
* Dise�o: Leo
* 
* Fecha : 16/3/95
* 
* Funcionamiento: Carga un vector con un indice
* Par�metros: no tiene
* Modificaciones:
* 
PARAMETERS indice, vector

DIMENSION vector[anio_cant*12,2]

PRIVATE indice_ant, tasa_ant, anioa, mesa, retorno
PRIVATE fecha_ant, indicef, tasaf

m.indice_ant = 1
m.tasa_ant   = 0
m.anioa	= 0
m.mesa  = 0

retorno = .T.

m.anio  = YEAR (m.fecha_act)
m.mes   = MONTH (m.fecha_act)

vector[1, 1] = m.indice_ant
vector[1, 2] = m.tasa_ant

IF !SEEK (EVAL (clave_ind), 'indices')
	= MENS_AVI ('RECUR', 'FALTA MES EN CURSO DE INDICE')
	= FINAL (15, 'Los �ndices de actualizaci�n est�n mal configurados. Se termina el programa.')
ELSE
	m.tasaf = indices.porc_ind * DAY (m.fecha_act) / 3000
	fecha_ant = GOMONTH (fecha_act, -1)

	m.anio  = YEAR (m.fecha_ant)
	m.mes   = MONTH (m.fecha_ant)

	IF !SEEK (EVAL (clave_ind), 'indices')
		= MENS_AVI ('RECUR', 'FALTA MES ANTERIOR DE INDICE')
		= FINAL (15, 'Los �ndices de actualizaci�n est�n mal configurados. Se termina el programa.')
	ELSE
		IF es_interes
			m.indicef = indices.val_ind + m.tasaf
		ELSE
			m.indicef = indices.val_ind * (1 + m.tasaf)
		ENDIF
	ENDIF
ENDIF

IF retorno
	=SEEK (EVAL (clave_pind), 'indices')
	DO WHILE !EOF ('indices') AND indices.indice = m.indice

		= ActVector (indices.anio, indices.mes)

		IF es_interes
			m.indice_ant = m.indicef - indices.val_ind 
			IF m.indice_ant < 0
				 m.indice_ant = 0
			ENDIF
		ELSE
*			m.indice_ant = m.indicef / indices.val_ind - 1
			m.indice_ant = m.indicef / indices.val_ind
		ENDIF

		m.tasa_ant   = indices.porc_ind / 3000

		IF m.anioa <= m.anio_cant - 1 AND m.anioa >= 0 AND m.mesa > 0 AND m.mesa <= 12
			vector[anioa*12+mesa, 1] = m.indice_ant
			vector[anioa*12+mesa, 2] = m.tasa_ant
		ENDIF

		SKIP IN indices
	ENDDO

	= ActVector (m.anio_inic + m.anio_cant - 1, 12)
ENDIF

RETURN retorno


*************************************************
FUNCTION ActVector
*************************************************
* Autor : Leo
* Dise�o: Leo
* 
* Fecha : 16/3
* 
* Funcionamiento: Actualiza el vector de indices de los meses que
* no estan cargados en la tabla, con los valores del ultimo mes
* leido
* Par�metros: anio y mes
PARAMETERS anio, mes


DO WHILE m.anioa * 12 + m.mesa + 1 < ;
	(m.anio - m.anio_inic) * 12 + m.mes AND ;
	m.anioa <= m.anio_cant - 1
	
	IF m.mesa > 0 AND m.mesa <= 12
		vector[m.anioa*12+m.mesa+1, 1] = m.indice_ant
		vector[m.anioa*12+m.mesa+1, 2] = 0
	ENDIF
	
	IF m.mesa = 12
		m.mesa  = 1
		m.anioa = m.anioa + 1
	ELSE
		m.mesa = m.mesa + 1
	ENDIF
ENDDO

m.mesa  = m.mes
m.anioa = m.anio - m.anio_inic

RETURN
