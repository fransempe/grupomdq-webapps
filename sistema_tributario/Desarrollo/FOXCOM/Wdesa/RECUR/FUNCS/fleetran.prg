*************************************************
* FUNCTION FLeeTran
*************************************************
&& Lee una transacci�n pasada como par�metro.
&& 
&& Autor			: Carlos.
&& Program�		: Cristian.
&&
&& Par�metros :
&& 		fec_recep		(D) : fecha de la transacci�n.
&&		caja				(C)	: c�d. de caja.
&&		nro_tran		(N)	: nro. de transacci�n.
&&		walias			(C)	: alias de la tabla caj_tran.
&&		bloquea			(L)	: indica si bloquea el registro.
&&		mensaje			(L)	: indica si saca mensajes por pantalla.
&&
PARAMETERS fec_recep, caja, nro_tran, walias, bloquea, mensaje 
PRIVATE retorno, exp_comp

=VerNuPar ('FLeeTran', PARAMETERS (), 6)

=VerTiPar ('FLeeTran', 'fec_recep', 'D')
=VerTiPar ('FLeeTran', 'caja', 'C')
=VerTiPar ('FLeeTran', 'nro_tran', 'N')
=VerTiPar ('FLeeTran', 'walias', 'C')
=VerTiPar ('FLeeTran', 'bloquea', 'L')
=VerTiPar ('FLeeTran', 'm.mensaje', 'L')

retorno = .F.
retorno = USET ('recur\caj_tran', 'PRIMARIO', walias)

IF retorno
	exp_comp = GETKEY (walias, .T.)
	IF SEEK (&exp_comp, walias)
		IF bloquea
			IF !RLOCK (walias)
				retorno = .F.
				IF mensaje
					=MSG ('La Transacci�n especificada no se puede bloquear.')
				ENDIF
			ENDIF
		ENDIF
	ELSE
		retorno = .F.
		IF mensaje
			=MSG ('La Transacci�n especificada no existe.')
		ENDIF
	ENDIF
ENDIF

RETURN retorno
