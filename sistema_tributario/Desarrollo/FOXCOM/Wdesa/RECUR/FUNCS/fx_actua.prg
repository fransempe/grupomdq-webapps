**************************************
* FUNCION Fx_Actua
* En Azul necesitan aplicar multas a las deudas.
* Se usar� el campo "Actualizaci�n" para guardar el importe de las multas.
* y se usar� esta Fx_Actua externa para calcular las multas.
**************************************

FUNCTION FX_ACTUA
PARAMETERS m.importe
PRIVATE m.retorno

if rec_cc.est_mov = 'EP'
 return 0
endif

m.retorno = ROUND(m.importe * 0.06, 2)

* Hay casos en los que no se debe calcular multa por morosidad
IF m.retorno <> 0
  IF m.importe < 0 OR m.fecha_final < {^2016/05/09} OR m.fecha_vto > {^2015/12/31}
    m.retorno = 0
  ENDIF
ENDIF

* Si el recurso es 'AB' tampoco se calcula la multa
*IF m.retorno <> 0
*  IF m.cod_recur = 'AB'
*    m.retorno = 0
*  ENDIF
*ENDIF

RETURN m.retorno

