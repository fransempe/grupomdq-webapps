*************************************************
*FUNCTION FMSGDEV
*************************************************
* Autor : Rodrigo
* Dise�o: Pachu
* 
* Fecha : 25/03/95
* 
PRIVATE retorno, curr_dbf, m.tipo_imp, m.nro_imp, m.grupo_conc, m.recurso
retorno 		= .T.
m.nro_msg		=	m.nro_msg	+	1
curr_dbf		=	SELE()
SELE dev_inc
APPEND BLANK

REPLACE	dev_inc.remesa			WITH	m.remesa,;
				dev_inc.tanda				WITH	m.tanda,;
				dev_inc.tipo_imp		WITH	rec_imp.tipo_imp,;
				dev_inc.nro_imp			WITH	rec_imp.nro_imp,;
				dev_inc.grupo_conc	WITH	conc_imp.grupo_conc
				
REPLACE	dev_inc.recurso			WITH	rec_imp.recurso,;
				dev_inc.anio				WITH	m.anio,;
				dev_inc.cuota				WITH	m.cuota,;
				dev_inc.nro_dev			WITH	m.nro_dev,;
				dev_inc.nro_msg			WITH	m.nro_msg,;
				dev_inc.txt_msg			WITH	m.txt_msg
				
SELE (curr_dbf)
RETURN retorno
