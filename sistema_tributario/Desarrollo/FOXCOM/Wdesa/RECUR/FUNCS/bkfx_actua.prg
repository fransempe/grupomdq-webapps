*******************************************************************
* FUNCTION FX_ACTUA
*
* Funci�n para c�lculo de actualizaciones de importes desarrollada
* especialmente para la Municipalidad de Tapalque
*
*******************************************************************
PARAMETER imp_orig
PRIVATE   m.anio_vto, m.mes_vto, m.imp_act

IF !USED ('indices')
	=USET('recur\indices')
ENDIF

m.anio_vto = YEAR (rec_cc.fecven_mov)
m.mes_vto  = MONTH(rec_cc.fecven_mov)

IF m.anio_vto > 1991 OR (m.anio_vto = 1991 AND m.mes_vto > 2)
	m.imp_act = 0
ELSE
	IF SEEK('ACTTAP' + STR(m.anio_vto,4,0) + STR(m.mes_vto,2,0), 'indices')
		m.imp_act = ROUND(imp_orig * indices.val_ind, 2) 
	ELSE
		m.imp_act = 0
	ENDIF
ENDIF

RETURN m.imp_act
