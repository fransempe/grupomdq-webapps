*************************************************
FUNCTION DIREC_OK
*************************************************
PARAMETERS CPATH, PATH_ABS, MSGERROR
PRIVATE retorno, ci, cantnom, cantext, pathb, exten, let_val
retorno = .T.
ci		= 1
cantnom	= 0
cantext	= 0
exten	= .F.
cpath	= TRIM(UPPER(cpath))
* Ver que otros simbolos soporta el DOS para crear subdirectorios
let_val = '\._`~!@#$%^&()-{}'

IF path_abs
	let_val = let_val + ':'
	IF ISALPHA(SUBSTR(cpath,1,1))
		IF SUBSTR(cpath,2,2) # ':\'
			retorno = .F.
		ENDIF
	ELSE
		retorno = .F.
	ENDIF
	IF retorno 
		IF !DRIVE_OK(SUBSTR(cpath,1,1)) 
			IF msgerror
				=MSG('El drive '+SUBSTR(cpath,1,2)+' No est� presente en su computadora. Verifique')
			ENDIF
			retorno = .F.
		ENDIF
	ELSE
		IF msgerror
			=MSG('El directorio debe ser un PATH absoluto (Ej: C:\)')
		ENDIF
	ENDIF
ENDIF

IF AT(':',cpath,2) > 0
	retorno = .F.
ENDIF

IF RIGHT(cpath,1) = '.' 	&&OR RIGHT(cpath,1) = '\'
	retorno = .F.
ENDIF

DO WHILE ci <= LEN(cpath) AND retorno
	pathb = SUBSTR(cpath,ci,1)
	IF ISALPHA(pathb) OR ISDIGIT(pathb) OR pathb $ let_val
		IF pathb = '\' OR pathb = '.' OR exten
			IF exten
				cantext = cantext + 1
				IF cantext = 1 AND (pathb = '.' OR pathb = '\')
					retorno = .F.
				ENDIF
				IF cantext = 3
					exten 	= .F.
					* Agregado el 14/2/95 
					IF SUBSTR(cpath,ci+1,1) # '\' AND ci+1 <= LEN(cpath)
						retorno = .F.
					ENDIF
					* Fin 
				ENDIF
			ELSE
				IF pathb # '\' AND cantnom = 0
					retorno = .F.
				ENDIF
				IF retorno
					IF pathb = '.'
						cantext = 0
						exten 	= .T.
					ELSE
						IF cantnom > 8 OR cantext > 3
							retorno = .F.
						ELSE
							cantnom = 0
							cantext = 0
						ENDIF				
					ENDIF
				ENDIF
			ENDIF
		ELSE
			cantnom = cantnom + 1		
			IF cantnom > 8 OR cantext > 3
				retorno = .F.
			ENDIF
		ENDIF
	ELSE
		IF msgerror
			=MSG('El car�cter '+pathb+' no es un caracter v�lido para crear un subdirectorio.')
		ENDIF
		retorno = .F.
	ENDIF
	ci = ci + 1
ENDDO

IF !retorno AND msgerror
	=MSG('El directorio no es correcto. Verifique.')
ENDIF

RETURN retorno

*************************************************
FUNCTION DRIVE_OK
*************************************************
PARAMETER drive
PRIVATE retorno
DIMENSION temparray(1)
retorno = .T.
string = drive + ':\*.*'
IF ADIR(temparray,(string),'D') <= 0
	retorno = .F.
ENDIF
RETURN retorno
