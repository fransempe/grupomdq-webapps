*************************************************
* FUNCTION FBuscImp
*************************************************
* Autor : Gabriel Zubieta
* Dise�o: Leo.
* 
* Fecha : 17/04/95
* 
* Funcionamiento:
* Dados 5 parametros pide un orden de busqueda para la tabla de imp.
* especificada.
* 
* Par�metros:
* tipoimp  -C- Tipo de Imponible en cuestion
* vis_baja -L- Flag si visualiza bajas.
* vis_cont -L- Flag si visualiza contribuyente.
* @nro	   -N- Nro. de Imponible
* @dig     -N- Nro. de d�gito.
* 
* Modificaciones:
* Gabriel - Cambie los EVAL por & en los BROWSE
* Gabriel - 270995 Mejore llamado a fx. FVinCon 
*
*
*
*
*
*
*
* Definiciones Previas.
* - Llamar funcion fcarmas ( =FCarMas() ), dimensionar todos los arreglos antes.
* - Abiertas las tablas de contrib,vinc_imp y tips_imp
* - Inicializado vble M.NRO_INDICE con valor en rango [1..5]
* - Ejecutada fcion fleetim ( =FLeeTim( 'CONTRIB',.T.,@m.tipo_impc,@m.nro_impc, @m.dig_impc )
* - Definida clave_tip, clave_vin y clave_vic
*    clave_tip = getkey('tips_vin', .T.)
*	   clave_vin = getkey('vinc_imp', .T., 3, 'primario' )
*	   clave_vic = getkey('vinc_imp', .T.   , 'primario' )
*
*

* Utilizo el Form de Browse
#DEFINE FORM_BROWSE

#INCLUDE "w:\-desa\bin\efiproc.h"

*!*	* Columnas de _SQL_TAB_INFO
*!*	#define TI_TABLA		1
*!*	#define TI_ORD_ACT		2
*!*	#define TI_TAG_ACT		3
*!*	#define TI_SQL_ACT		4
*!*	#define TI_INDICES		5
*!*	#define TI_COLUMNAS		30


PARAMETERS m.tipoimp, m.vis_baja, m.vis_cont, m.nro, m.dig
EXTERNAL ARRAY ident_exp, tab_imp, ident_imp, ubic_exp, indi_ubi, indi_ide
PRIVATE retorno, m.ubic, area, orden, rec, tt_tabact, exp_busc

DECLARE m.vec_rel [1]		&& ### C/S
m.vec_rel [1] = .F.

retorno = .T.
area  = SELECT()
orden = ORDER()

exp_busc = ''

IF m.tipo_impc = m.tipoimp
	IF m.nro_indice > 3
		m.nro_indice = 1
	ENDIF
	DIME ind[3]
ELSE
	DIME ind[5]
	ind[4]='Nombre Contribuyente'
	ind[5]='CUIT'
ENDIF

ind[1]='N�mero'
ind[2]='Identificaci�n'
ind[3]='Ubicaci�n'

DEFINE WINDOW vcambord FROM 6,2 TO 14,27 TITLE 'Orden de B�squeda' IN SCREEN COLOR SCHEME 10
ACTI WIND vcambord
@ 0,0 GET m.nro_indice FROM ind PICTURE '@&T' SIZE 9,24 MESSAGE "Elija el Orden por el cual quiere buscar el Imponible"
READ
RELE WIND vcambord

m.ubic    = ASC( m.tipoimp ) - 64
tt_tabact = ALLT( tab_imp[m.ubic] )

IF !(m.tipoimp == m.tipo_impc)
	=USET ( 'recur\' + tt_tabact )
ENDIF


IF INLIST (m.nro_indice, 1, 2, 3)
	SELECT (tt_tabact)
	m.cont = ""

ELSE

	SET ORDER TO imp_vinc IN vinc_imp
	SELECT contrib
	=RELT ("m.tipo_impc + STR(contrib.cuit,10) + m.tipoimp", 'vinc_imp', .T. )

	SELECT vinc_imp
	=RELT("STR(vinc_imp.nro_imp,10)", tt_tabact, .T. )
	
	SELECT contrib
	IF m.nro_indice==4
		m.cont = "LEFT(contrib.nomb_cont,30) + SPACE (1)+ STR(contrib.cuit,10)+ SPACE (1) + STR(vinc_imp.nro_imp,10)"
	ELSE
		m.cont = "STR(contrib.cuit,10) + SPACE (1) + LEFT(contrib.nomb_cont,18) + SPACE (1) + STR(vinc_imp.nro_imp,10)"
	ENDIF
ENDIF

* [*] Modi Nicol�s (06/02/98). Saco el filtro fuera de la condici�n
	IF !vis_baja
		SET FILTER TO EMPTY( fec_baja )
	ENDIF


* Define ventana para lista de opciones
DEFINE WINDOW wifbuscimp FROM 4,3 TO 24,77 IN SCREEN DOUBLE FLOAT SHADOW COLOR SCHEME 10

_jexitkey = 13
_JDBLCLICK= -1
=jkeyinit("U",""," Buscando: ","")

PUSH KEY

ON KEY LABEL home DO BusqSecu

PRIVATE lEnter , m.alias, m.tabla
lEnter = .F.

m.form = "BROWLIS2.SCX"

EXTERNAL FORM BROWLIS2.SCX
* EXTERNAL FORM BROWLIS3.SCX

* Armo el array de columnas del browse para cada uno de los �ndices
DIMENSION m.cols [100,4]
STORE "" TO m.cols

m.col = 0

m.cols [INC(@m.col) ,1] = 1
m.cols [m.col       ,2] = "PRIMARIO"
m.cols [m.col       ,3] = 'Str (Eval (ident_imp[m.ubic])) + Eval (ident_exp[m.ubic])' + IIF(m.tipo_impc # m.tipoimp AND vis_cont, ' + Vinc_Con()','')
m.cols [m.col       ,4] = "Buscando imponible por N�mero"
m.cols [INC(@m.col) ,1] = "Imponible"
m.cols [m.col       ,2] = ident_imp [m.ubic]
m.cols [INC(@m.col) ,1] = "Identif"
m.cols [m.col       ,2] = ident_exp [m.ubic]
*
m.cols [INC(@m.col) ,1] = 2
m.cols [m.col       ,2] = indi_ide[m.ubic]
m.cols [m.col       ,3] = 'Str (Eval (ident_imp[m.ubic])) + Eval (ident_exp[m.ubic])' + IIF(m.tipo_impc # m.tipoimp AND vis_cont, ' + Vinc_Con()','')
m.cols [m.col       ,4] = "Buscando imponible por Identificaci�n"
m.cols [INC(@m.col) ,1] = "Identif"
m.cols [m.col       ,2] = ident_exp [m.ubic]
m.cols [INC(@m.col) ,1] = "Imponible"
m.cols [m.col       ,2] = ident_imp [m.ubic]
*
m.cols [INC(@m.col) ,1] = 3
m.cols [m.col       ,2] = indi_ubi[m.ubic]
m.cols [m.col       ,3] = 'Str (Eval (ident_imp[m.ubic])) + Eval (ubic_exp[m.ubic])' + IIF(m.tipo_impc # m.tipoimp AND vis_cont, ' + Vinc_Con()','')
m.cols [m.col       ,4] = "Buscando imponible por Ubicaci�n"
m.cols [INC(@m.col) ,1] = "Ubic"
m.cols [m.col       ,2] = STRTRAN (STRTRAN (RTRIM (ubic_exp [m.ubic]), [ALLT(], [RTRIM(]), ":", " ")+" :50"
m.cols [INC(@m.col) ,1] = "Imponible"
m.cols [m.col       ,2] = ident_imp [m.ubic]
*
m.cols [INC(@m.col) ,1] = 4
m.cols [m.col       ,2] = "NOMB_CON"
m.cols [m.col       ,3] = 'Eval (cont) + Eval (ident_exp[m.ubic])'
m.cols [m.col       ,4] = "Buscando imponible por Nombre de Cont."
m.cols [INC(@m.col) ,1] = "Contrib"
m.cols [m.col       ,2] = m.cont
m.cols [INC(@m.col) ,1] = "Identif"
m.cols [m.col       ,2] = ident_exp [m.ubic]
*
m.cols [INC(@m.col) ,1] = 5
m.cols [m.col       ,2] = "PRIMARIO"
m.cols [m.col       ,3] = 'Eval (cont) + Eval (ident_exp[m.ubic])'
m.cols [m.col       ,4] = "Buscando imponible por N�mero de Cont."
m.cols [INC(@m.col) ,1] = "Contrib"
m.cols [m.col       ,2] = m.cont
m.cols [INC(@m.col) ,1] = "Identif"
m.cols [m.col       ,2] = ident_exp [m.ubic]

IF m.nro_indice <= 3
	FOR m.col = 1 TO ALEN(m.cols, 1)
		IF VARTYPE (m.cols [m.col,1])=="N" AND m.cols [m.col, 1] > m.nro_indice
			m.col = m.col - 1
			EXIT
		ENDIF
	NEXT

	m.cols [INC(@m.col) ,1] = "Contrib"
	m.cols [m.col       ,2] = "Vinc_Con()"
	m.cols [m.col       ,3] = ""
	m.cols [INC(@m.col), 1] = 0

	m.tabla = tt_tabact
ELSE
	m.tabla = "CONTRIB"
ENDIF

DIMENSION m.cols [m.col, 4]

* Recorro el array y armo la expresi�n de browse y el orden de la consulta para XBase / SQL
m.order    = ""
m.exp_busc = ""
m.titulo   = ""

m.col = 1
DO WHILE m.col <= ALEN(m.cols, 1)
	IF VARTYPE (m.cols [m.col, 1])=="N" AND m.cols [m.col, 1]==m.nro_indice
		m.order    = m.cols [m.col, 2]
		m.exp_busc = m.cols [m.col, 3]
		m.titulo   = m.cols [m.col, 4]
		
		m.col = m.col + 1
		m.str_brow = ""

		DO WHILE m.col <= ALEN(m.cols, 1) AND VARTYPE (m.cols [m.col, 1])=="C"
			IF EMPTY (m.cols [m.col, 3]) OR EVAL (m.cols [m.col, 3])
				m.str_brow = m.str_brow + IIF(!EMPTY(m.str_brow), [,], "") +;
					RTRIM (m.cols [m.col, 2]) +;
					[ :H='] + m.cols [m.col,1]+[']
			ENDIF
			
			m.col = m.col + 1
		ENDDO
		
		EXIT
	ENDIF
	
	m.col = m.col + 1
ENDDO

SET ORDER TO (m.order)
m.vista = ""

DO FORM (m.form) WITH m.titulo, m.str_brow

POP KEY

RELE WIND wifbuscimp

=jkeycanc()

retorno = ( LKEY()=13 OR lEnter)

IF retorno
	IF EMPTY (m.vista)
		cpo = tt_tabact + '.' + ALLT( ident_imp[m.ubic] )
	ELSE
		cpo = m.vista + '.' + ALLT( ident_imp[m.ubic] )
	ENDIF
	
	m.nro = EVAL( cpo )
	m.dig = &tt_tabact..dig_veri
ENDIF

* restaurar todo
IF INLIST (m.nro_indice, 1, 2, 3)
	SET FILTER TO
ELSE
	SET RELATION OFF INTO vinc_imp
	SET SKIP TO 
	SELECT vinc_imp
	SET RELATION OFF INTO (tab_imp[m.ubic])
	SET SKIP TO
	SELECT contrib
ENDIF

IF !EMPTY (m.vista)
	USE IN (m.vista)
	SELECT (m.tabla)
ENDIF

SET ORDER TO primario 
SET ORDER TO primario IN vinc_imp

SELECT (area)
IF !EMPTY (orden)
	SET ORDER TO orden IN (area)
ENDIF
RETURN retorno

*************************************************
FUNCTION Vinc_Con   
*************************************************
* Es similar a FVinCon pero que cubre s�lo las necesidades de este
PRIVATE retorno, m.cuit1, m.cuit2, m.prior1, m.prior2, reto, m.nrocuit, m.expresion

retorno 	= .T.
m.cuit1 	= -1
m.cuit2     = -1
m.prior1	= 10
m.prior2    = 10

m.expresion = " m.tipoimp + STR (EVAL (ident_imp[m.ubic]), 10) + m.tipo_impc "
m.expresion = '"'+EVAL(m.expresion)+'"'

SET KEY TO m.expresion IN VINC_IMP
GO TOP IN VINC_IMP
DO WHILE retorno AND !EOF('vinc_imp')
	IF EMPTY(vinc_imp.fec_baja)
		IF tips_vin.tipo_vinc == vinc_imp.tipo_vinc OR SEEK (vinc_imp.tipo_vinc, 'tips_vin')		
			IF tips_vin.pri_cuit1 > 0 AND tips_vin.pri_cuit1 < m.prior1
				m.cuit1 	= vinc_imp.nro_impv
				m.prior1	= tips_vin.pri_cuit1
			ENDIF
			IF tips_vin.pri_cuit2 > 0 AND tips_vin.pri_cuit2 < m.prior2
				m.cuit2   = vinc_imp.nro_impv
				m.prior2  = tips_vin.pri_cuit2
			ENDIF
		ELSE
			retorno = .F.
		ENDIF
	ENDIF

	SKIP IN vinc_imp
ENDDO

SET KEY TO '' IN VINC_IMP

m.nrocuit = 0
IF m.cuit1 > 0
	m.nrocuit = m.cuit1
ELSE
	IF m.cuit2 > 0
		m.nrocuit = m.cuit2
	ENDIF
ENDIF

IF m.nrocuit > 0			
	IF SEEK (STR (m.nrocuit,10), 'contrib')
		reto = STR (contrib.cuit, 10) + SPACE (1) + contrib.nomb_cont
	ELSE
		reto = SPACE (35)
	ENDIF
ELSE
	reto = SPACE (35)
ENDIF				

RETURN reto


* Elimina el alias de los campos de una expresi�n
* No toma en cuenta posibles literales en la expresi�n (ej: "alias.campo1+alias.campo2+'xxx.yyy'"
FUNCTION ELIM_ALIAS (m.expr)

LOCAL m.pos, m.ret

m.ret = ""
m.pos = AT(".", m.expr)

DO WHILE ! EMPTY (m.pos)
	m.pos1 = m.pos - 1
	DO WHILE UPPER (SUBSTR (m.expr, m.pos1, 1)) $ "ABCDEFGHIJKLMNOPQRSTUVWXYZ_"
		m.pos1 = m.pos1 - 1
	ENDDO
	m.ret = m.ret + LEFT (m.expr, m.pos1)

	m.expr = SUBSTR (m.expr, m.pos+1)
	m.pos = AT(".", m.expr)
ENDDO

m.ret = m.ret + m.expr

RETURN (m.ret)



FUNCTION INC (m.num)
m.num = m.num + 1
RETURN (m.num)


***
