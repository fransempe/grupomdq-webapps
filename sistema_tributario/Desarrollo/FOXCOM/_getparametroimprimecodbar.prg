	* 12/06/2017 : Francisco
	* Parametro WEB_IMP_CODBAR : Este par�metro es para Z�rate, se hizo para configurar si imprime o no
	* el c�digo de barras a los COMERCIOS PEQUE�OS o no.
	********************************************************************************************
	PROCEDURE _getParametroImprimeCodBar() as String
	
	PRIVATE strParametro as String
	PRIVATE strIndex as String
	
	
	IF (!USED ('config')) THEN
		=USET ('asa\config')
	ENDIF
	
	SELECT config
	SET ORDER TO PRIMARIO
		
	strParametro = ''
	strIndex = PADR('ASA', 8, ' ') + 'WEB_IMP_CODBAR'
	IF (SEEK(strIndex)) THEN
		strParametro = ALLTRIM(config.cont_par)		
	ENDIF
	
	
	RETURN ALLTRIM(strParametro)
	
	ENDPROC
	
	