﻿Option Explicit On
Option Strict On

Imports System.Drawing.Imaging
Imports System.IO
Imports System.Security.Cryptography


Public Class ClsTools


    Public Shared mVersion As String = "5.11"


    'ObtainDataXML:
    'This function create a file in disc for fiil the dataset
    Public Shared Function ObtainDataXML(ByVal mXML_AUX As String) As DataSet
        Dim mPath As String
        Dim mFile As String
        Dim dsData As DataSet


        Try

            mPath = System.Configuration.ConfigurationManager.AppSettings("path_datos_temporales").ToString.Trim
            If Not (My.Computer.FileSystem.DirectoryExists(mPath.ToString & "datos_temporales")) Then
                My.Computer.FileSystem.CreateDirectory(mPath.ToString & "datos_temporales")
            End If

            mFile = mPath.ToString & "datos_temporales\" & Format(Now, "ddMMyyyyHHmmss") & ".xml"


            Dim mCreaTeXML As New IO.StreamWriter(mFile.ToString)
            mCreaTeXML.WriteLine(mXML_AUX.ToString)
            mCreaTeXML.Close()

            dsData = New DataSet
            dsData.ReadXml(mFile.ToString)

            My.Computer.FileSystem.DeleteFile(mFile.ToString)


            Return dsData
        Catch ex As Exception


            'mLog = New Clslog
            'mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            'mLog = Nothing
            'Response.Redirect("webfrmerror.aspx", False)
            Return Nothing
        End Try
    End Function


    'HasData
    'Esta funcion valida que un dataset contenga datos
    Public Shared Function HasData(ByVal dsData_AUX As DataSet) As Boolean

        'This one created
        If (dsData_AUX Is Nothing) Then
            Return False
        End If

        'It has at least a table
        If (dsData_AUX.Tables.Count = 0) Then
            Return False
        End If


        'has rows
        If ((dsData_AUX.Tables(0).Rows.Count - 1) = -1) Then
            Return False
        End If


        Return True
    End Function



    Public Shared Function ObtenerLogo() As Byte()
        Dim mSecuencia As MemoryStream
        Dim mBuffer As Byte()
        Dim mImagen As System.Drawing.Image
        Dim mPath As String


        mSecuencia = Nothing
        mBuffer = Nothing
        mPath = ""

        Try

            mPath = System.Configuration.ConfigurationManager.AppSettings("path_logo").ToString.Trim & "\logo.jpg"
            If (My.Computer.FileSystem.FileExists(mPath.ToString.Trim)) Then

                mImagen = Drawing.Image.FromFile(mPath.ToString.Trim)
                mSecuencia = New MemoryStream()
                mImagen.Save(mSecuencia, ImageFormat.Png)
                mBuffer = mSecuencia.ToArray()
                mSecuencia.Close()
            End If

        Catch ex As Exception
            mBuffer = Nothing
        End Try


        Return mBuffer
    End Function


    Public Shared Function ObtenerNombreMunicipalidad() As String
        Dim mNombreMunicipio As String
        mNombreMunicipio = ""
        Try

            mNombreMunicipio = System.Configuration.ConfigurationManager.AppSettings("Nombre_Municipalidad").ToString.Trim
        Catch ex As Exception
            mNombreMunicipio = ""
        End Try

        Return mNombreMunicipio.ToString.Trim
    End Function


    Public Shared Function GenerateHash(ByVal SourceText As String) As String
        'Create an encoding object to ensure the encoding standard for the source text
        Dim Ue As New UnicodeEncoding()
        'Retrieve a byte array based on the source text
        Dim ByteSourceText() As Byte = Ue.GetBytes(SourceText)
        'Instantiate an MD5 Provider object
        Dim Md5 As New MD5CryptoServiceProvider()
        'Compute the hash value from the source
        Dim ByteHash() As Byte = Md5.ComputeHash(ByteSourceText)
        'And convert it to String format for return
        Return Convert.ToBase64String(ByteHash)
    End Function


End Class
