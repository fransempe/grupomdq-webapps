﻿Option Explicit On
Option Strict On


Imports web_tributaria.ws_consulta_tributaria.Service1
Imports System.Web.UI.HtmlControls
Imports System.Xml.Serialization



Partial Public Class webfrmlistado
    Inherits System.Web.UI.Page

#Region "Variables"

    Private mWS As web_tributaria.ws_consulta_tributaria.Service1
    Private mLog As Clslog
    Private mXml As String
    Private mFila As Integer
    Private strNombreMunicipio As String
    Private boolDeudaEnJuicio As Boolean

    'Esta Variable es utilizada para asignar datos a las funciones JAVASCRIPT que se crean en las filas de las grillas
    Private dtDatos_AUX As DataTable

#End Region

#Region "Eventos Formulario"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dsDatos As DataSet
        Dim mFechaRecargos As String
        Dim boolContieneDeuda As Boolean

        Try


            If (Session("tipo_cuenta") IsNot Nothing) And (Session("nro_cuenta") IsNot Nothing) Then
                If Not (IsPostBack) Then
                    mXml = ""

                    strNombreMunicipio = System.Configuration.ConfigurationManager.AppSettings("Nombre_Municipalidad").ToString.Trim
                    Me.lblmunicipalidad_name.Text = strNombreMunicipio

                    If (Session("municipalidad_telefono") IsNot Nothing) Then
                        Me.lbltelefono.Text = Session("municipalidad_telefono").ToString.Trim
                    Else
                        Me.lbltelefono.Text = Session("Tel. Municipio").ToString.Trim
                    End If

                    If (Session("municipalidad_mail") IsNot Nothing) Then
                        Me.lblmail.Text = Session("municipalidad_mail").ToString.Trim
                    Else
                        Me.lblmail.Text = "Email Municipio"
                    End If




                    'Si trabajo con FOX no uso el Nro. de Usuario
                    If (Application("tipo_conexion").ToString.ToUpper = "FOX") Then
                        Session("nro_usuario") = 0
                    End If



                    Me.mWS = New ws_consulta_tributaria.Service1
                    mXml = Me.mWS.ConsultarDeuda(Session("tipo_cuenta").ToString, CLng(Session("nro_cuenta")), CInt(Session("nro_usuario")))

                    mFechaRecargos = Me.mWS.ObtenerFechaActuaWeb()
                    Me.lblfecha_recargos.Text = mFechaRecargos.Trim


                    If (mXml.ToString <> "Error") Then
                        dsDatos = New DataSet
                        dsDatos = ObtenerDatosXML()


                        'Bandera para saber a donde estoy conectado, porque varian algunas validaciones de datos
                        Me.lbltipo_conexion.Text = Application("tipo_conexion").ToString
                        Me.lbltipo_conexion.ForeColor = Drawing.Color.White


                        'Datos del Contribuyente
                        If (dsDatos.Tables("DATOSCUENTA") IsNot Nothing) Then
                            Me.lbltipo_de_cuenta.Text = Me.ReemplazarPalabraVehiculo(dsDatos.Tables("DATOSCUENTA").Rows(0).Item("TIPOIMPONIBLE").ToString.ToUpper())
                            Me.lblnro_de_cuenta.Text = dsDatos.Tables("DATOSCUENTA").Rows(0).Item("NROIMPONIBLE").ToString.ToUpper()

                            If (CBool(System.Configuration.ConfigurationManager.AppSettings("CabeceraDatosCompletosContribuyente").ToString.Trim)) Then
                                Me.lbltitular.Text = dsDatos.Tables("DATOSCUENTA").Rows(0).Item("TITULAR").ToString.ToUpper()
                            Else
                                Me.lbltitular.Text = "-----------"
                            End If
                        Else
                            Me.lbltipo_de_cuenta.Text = "INEXISTENTE"
                            Me.lblnro_de_cuenta.Text = "INEXISTENTE"
                            Me.lbltitular.Text = "INEXISTENTE"
                        End If



                        'Cargo la Grilla de Info de recursos
                        If (dsDatos.Tables("REGISTRO_REFRECURSOS") IsNot Nothing) Then
                            Me.gvdescripcion_recursos.DataSource = Me.CargarGrillaInfoRecursos(dsDatos.Tables("REGISTRO_REFRECURSOS"))
                            Me.gvdescripcion_recursos.DataBind()
                        End If




                        'Cargo la Grilla de Comprobantes Vencidos
                        mFila = 0
                        dtDatos_AUX = Nothing
                        Me.boolDeudaEnJuicio = False
                        boolContieneDeuda = False
                        If (dsDatos.Tables("REGISTRO_DETALLEDEUDAVENCIDA") IsNot Nothing) Then

                            If (strNombreMunicipio.Trim.ToUpper = "AZUL") Then
                                Me.movimientosEnJuicioByAzul(dsDatos.Tables("REGISTRO_DETALLEDEUDAVENCIDA"))
                            End If

                            Me.gvperiodos_vencidos.DataSource = Me.CargarGrillaVencidos(dsDatos.Tables("REGISTRO_DETALLEDEUDAVENCIDA"))
                            Session("totales_grilla_cta_cte_vencida") = dsDatos.Tables("REGISTRO_TOTALESDEUDAVENCIDA").Copy
                            Me.gvperiodos_vencidos.DataBind()

                            boolContieneDeuda = True

                            'Si estoy CONECTADO RAFAM habilito la columna de COMPROBANTE, porque es utilizarda para validaciones 
                            Me.gvperiodos_vencidos.Columns(0).Visible = CBool(Application("tipo_conexion").ToString.Trim = "RAFAM")
                        Else
                            Me.lbltotal_vencidos.Visible = False
                            Me.boton_Imprimir_comprob_vencidos.Visible = False
                            Me.lblcantidad_seleccionados_vencidos.Visible = False
                            Me.lblcantidad_no_seleccionados_vencidos.Visible = False
                            Me.lblmensaje_periodos_vencidos.Visible = True
                        End If



                        'Cargo la Grilla de Comprobantes No Vencidos
                        mFila = 0
                        dtDatos_AUX = Nothing
                        If (dsDatos.Tables("REGISTRO_DETALLEPROXCOMPAVENC") IsNot Nothing) Then
                            Me.gvperiodos_no_vencidos.DataSource = Me.CargarGrillaNoVencidos(dsDatos.Tables("REGISTRO_DETALLEPROXCOMPAVENC"))
                            Me.gvperiodos_no_vencidos.DataBind()
                        Else
                            Me.lbltotal_no_vencidos.Visible = False
                            Me.lblcantidad_seleccionados_no_vencidos.Visible = False
                            Me.lblcantidad_no_seleccionados_no_vencidos.Visible = False
                            Me.boton_Imprimir_comprob_no_vencidos.Visible = False
                            Me.lblmensaje_periodos_no_vencidos.Visible = True
                        End If



                        'Cargo la Grilla de Comprobantes No Vencidos que exedan los 60 dias
                        mFila = 0
                        dtDatos_AUX = Nothing
                        If (dsDatos.Tables("REGISTRO_DETALLEOTROSCOMPAVENC") IsNot Nothing) Then
                            Me.gvperiodos_otros_comprobantes.DataSource = Me.CargarGrillaOtrosComprobantes(dsDatos.Tables("REGISTRO_DETALLEOTROSCOMPAVENC"))
                            Me.gvperiodos_otros_comprobantes.DataBind()
                        Else
                            Me.lbltotal_otros_comprobantes.Visible = False
                            Me.lblcantidad_seleccionados_otros_comprobantes.Visible = False
                            Me.lblcantidad_no_seleccionados_otros_comprobantes.Visible = False
                            Me.boton_Imprimir_comprob_otros_comprobantes.Visible = False
                            Me.lblmensaje_periodos_otros_comprobantes.Visible = True
                        End If






                        'Para azul muestro un cartel explicativo
                        If (strNombreMunicipio.Trim.ToUpper = "AZUL") Then

                            If (boolContieneDeuda) Then
                                Me.cartel_azul.Visible = True
                                If (Me.boolDeudaEnJuicio) Then
                                    Me.mensaje_azul.InnerHtml = "La cuenta registra períodos adeudados en juicio. Dirígaje a la Municipalidad para regularizar su situación."
                                Else
                                    Me.mensaje_azul.InnerHtml = "Debe seleccionar desde el período adeudado con mayor antiguedad o períodos del año actual"
                                End If
                            Else
                                Me.cartel_azul.Visible = False
                                Me.mensaje_azul.InnerHtml = ""
                            End If
                        End If
                    End If





                    'Eventos JavaScript
                    Me.link_cerrar_session.Attributes.Add("onclick", "javascript:return cerrar_session();")
                    Me.boton_informe_deuda.Attributes.Add("onclick", "javascript:return mensaje();")
                    Me.boton_Imprimir_comprob_vencidos.Attributes.Add("onclick", "javascript:return validar('gvperiodos_vencidos');")
                    Me.boton_Imprimir_comprob_no_vencidos.Attributes.Add("onclick", "javascript:return validar('gvperiodos_no_vencidos');")
                    Me.boton_Imprimir_comprob_otros_comprobantes.Attributes.Add("onclick", "javascript:return validar('gvperiodos_otros_comprobantes');")

                End If
            Else
                Call Me.LimpiarSession()
            End If


            Me.lblversion.Text = "Versión " & ClsTools.mVersion.ToString.Trim
        Catch ex As Exception
            Me.mLog = New Clslog
            Me.mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            Me.mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)
        End Try

    End Sub

    Protected Sub link_cerrar_session_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_cerrar_session.Click
        Call Me.LimpiarSession()
    End Sub


    Protected Sub link_informe_deuda_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_informe_deuda.Click
        Dim mDatosContribuyente As String


        If (Application("tipo_conexion").ToString.Trim = "RAFAM") Then

            mDatosContribuyente = Me.ObtenerDatosContribuyente()
            Session("datos_contribuyente") = Me.lbltipo_de_cuenta.Text & "|" & _
                                             Me.lblnro_de_cuenta.Text & "|" & _
                                             mDatosContribuyente.ToString


        Else
            Session("datos_contribuyente") = Me.lbltipo_de_cuenta.Text & "|" & _
                                             Me.lblnro_de_cuenta.Text & "|" & _
                                             Me.lbltitular.Text

        End If


        Response.Redirect("comprobantes/webfrminformectactevencida.aspx", False)
    End Sub


    Private Sub gvperiodos_vencidos_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvperiodos_vencidos.PageIndexChanging
        Me.gvperiodos_vencidos.PageIndex = e.NewPageIndex
        Me.gvperiodos_vencidos.DataSource = Session("grilla_cta_cte_vencida")
        Me.gvperiodos_vencidos.DataBind()
    End Sub

#End Region

#Region "Cargar Grillas"

    Private Function CargarGrillaInfoRecursos(ByVal dtDatos As DataTable) As DataTable


        Dim dtInfoRecursos As New DataTable
        Dim mFila As DataRow
        Dim i As Integer

        Try


            'Asigno las Columnas 
            mFila = dtInfoRecursos.NewRow
            dtInfoRecursos.Columns.Add("Codigo")
            dtInfoRecursos.Columns.Add("Descripcion")


            'Guardo los datos que ya estan cargados en la grilla
            For i = 0 To dtDatos.Rows.Count - 1
                mFila = dtInfoRecursos.NewRow
                mFila("Codigo") = dtDatos.Rows(i).Item("REC_CODIGO").ToString
                mFila("Descripcion") = dtDatos.Rows(i).Item("REC_DESCRIPCION").ToString

                dtInfoRecursos.Rows.Add(mFila)
            Next


            Session("grilla_cta_cte_vencida") = dtInfoRecursos
            Return dtInfoRecursos
        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)

            Return Nothing
        End Try

    End Function

    Private Function CargarGrillaVencidos(ByVal dtDatos As DataTable) As DataTable
        Dim dtCtaCteVencida As New DataTable
        Dim mFila As DataRow
        Dim boolOcultar As Boolean
        Dim i As Integer


        Try

            'Asigno las Columnas 
            mFila = dtCtaCteVencida.NewRow
            dtCtaCteVencida.Columns.Add("Nro_Movimiento")
            dtCtaCteVencida.Columns.Add("Recurso")
            dtCtaCteVencida.Columns.Add("Anio")
            dtCtaCteVencida.Columns.Add("Cuota")
            dtCtaCteVencida.Columns.Add("Concepto")
            dtCtaCteVencida.Columns.Add("Plan")
            dtCtaCteVencida.Columns.Add("Fecha_Vencimiento")
            dtCtaCteVencida.Columns.Add("Condicion_Especial")
            dtCtaCteVencida.Columns.Add("Importe_Origen")
            dtCtaCteVencida.Columns.Add("Importe_Recargos")
            dtCtaCteVencida.Columns.Add("Importe_Total")
            dtCtaCteVencida.Columns.Add("usa_check")



            'Guardo los datos que ya estan cargados en la grilla
            For i = 0 To dtDatos.Rows.Count - 1
 
                mFila = dtCtaCteVencida.NewRow
                mFila("Nro_Movimiento") = dtDatos.Rows(i).Item("DDV_NROMOV").ToString
                mFila("Recurso") = dtDatos.Rows(i).Item("DDV_REC").ToString
                mFila("Anio") = dtDatos.Rows(i).Item("DDV_ANIO").ToString
                mFila("Cuota") = dtDatos.Rows(i).Item("DDV_CUOTA").ToString
                mFila("Concepto") = dtDatos.Rows(i).Item("DDV_CONCEPTO").ToString
                mFila("Plan") = dtDatos.Rows(i).Item("DDV_DEPLAN").ToString
                mFila("Fecha_Vencimiento") = dtDatos.Rows(i).Item("DDV_FECHAVENC").ToString
                mFila("Condicion_Especial") = dtDatos.Rows(i).Item("DDV_CONDESPECIAL").ToString
                mFila("Importe_Origen") = DarFormato(dtDatos.Rows(i).Item("DDV_IMPORTEORIGEN").ToString)
                mFila("Importe_Recargos") = DarFormato(dtDatos.Rows(i).Item("DDV_IMPORTERECARGO").ToString)
                mFila("Importe_Total") = DarFormato(dtDatos.Rows(i).Item("DDV_IMPORTETOTAL").ToString)

                If (Application("tipo_conexion").ToString.ToUpper = "FOX") Then
                    mFila("usa_check") = dtDatos.Rows(i).Item("DDV_USA_CHECK").ToString.Trim
                End If

                dtCtaCteVencida.Rows.Add(mFila)
            Next


            Session("grilla_cta_cte_vencida") = dtCtaCteVencida
            dtDatos_AUX = dtCtaCteVencida

            Return dtCtaCteVencida
        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)

            Return Nothing
        End Try

    End Function

    Private Function CargarGrillaNoVencidos(ByVal dtDatos As DataTable) As DataTable
        Dim dtCtaCteNoVencida As New DataTable
        Dim mFila As DataRow
        Dim i As Integer


        Try

            'Asigno las Columnas 
            mFila = dtCtaCteNoVencida.NewRow
            dtCtaCteNoVencida.Columns.Add("Comprobante")
            dtCtaCteNoVencida.Columns.Add("Detalle")
            dtCtaCteNoVencida.Columns.Add("Vencimiento")
            dtCtaCteNoVencida.Columns.Add("Fecha_Vencimiento")
            dtCtaCteNoVencida.Columns.Add("Condicion_Especial")
            dtCtaCteNoVencida.Columns.Add("Importe_Origen")
            dtCtaCteNoVencida.Columns.Add("Importe_Recargo")
            dtCtaCteNoVencida.Columns.Add("Importe_Total")
            dtCtaCteNoVencida.Columns.Add("usa_check")


            'Guardo los datos que ya estan cargados en la grilla
            For i = 0 To dtDatos.Rows.Count - 1

                mFila = dtCtaCteNoVencida.NewRow
                mFila("Comprobante") = dtDatos.Rows(i).Item("DPC_COMPROBANTE").ToString.Trim
                mFila("Detalle") = dtDatos.Rows(i).Item("DPC_DETALLE").ToString.Trim
                mFila("Vencimiento") = dtDatos.Rows(i).Item("DPC_VENC").ToString.Trim
                mFila("Fecha_Vencimiento") = dtDatos.Rows(i).Item("DPC_FECHAVENC").ToString.Trim
                'mFila("Condicion_Especial") = dtDatos.Rows(i).Item("DDV_CONDESPECIAL").ToString.Trim
                mFila("Importe_Origen") = DarFormato(dtDatos.Rows(i).Item("DPC_IMPORTEORIGEN").ToString)
                mFila("Importe_Recargo") = DarFormato(dtDatos.Rows(i).Item("DPC_IMPORTERECARGO").ToString)
                mFila("Importe_Total") = DarFormato(dtDatos.Rows(i).Item("DPC_IMPORTETOTAL").ToString)

                If (Application("tipo_conexion").ToString.ToUpper = "FOX") Then
                    mFila("usa_check") = dtDatos.Rows(i).Item("DPC_USA_CHECK").ToString.Trim
                End If

                dtCtaCteNoVencida.Rows.Add(mFila)
            Next


            Session("grilla_cta_cte_no_vencida") = dtCtaCteNoVencida
            dtDatos_AUX = dtCtaCteNoVencida
            Return dtCtaCteNoVencida
        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)

            Return Nothing
        End Try

    End Function

    Private Function CargarGrillaOtrosComprobantes(ByVal dtDatos As DataTable) As DataTable
        Dim dtCtaCteOtrosComprobantes As New DataTable
        Dim mFila As DataRow        
        Dim i As Integer


        Try

            'Asigno las Columnas 
            mFila = dtCtaCteOtrosComprobantes.NewRow
            dtCtaCteOtrosComprobantes.Columns.Add("Comprobante")
            dtCtaCteOtrosComprobantes.Columns.Add("Detalle")
            dtCtaCteOtrosComprobantes.Columns.Add("Vencimiento")
            dtCtaCteOtrosComprobantes.Columns.Add("Fecha_Vencimiento")
            dtCtaCteOtrosComprobantes.Columns.Add("Condicion_Especial")
            dtCtaCteOtrosComprobantes.Columns.Add("Importe_Origen")
            dtCtaCteOtrosComprobantes.Columns.Add("Importe_Recargo")
            dtCtaCteOtrosComprobantes.Columns.Add("Importe_Total")
            dtCtaCteOtrosComprobantes.Columns.Add("usa_check")


            'Guardo los datos que ya estan cargados en la grilla
            For i = 0 To dtDatos.Rows.Count - 1

                mFila = dtCtaCteOtrosComprobantes.NewRow
                mFila("Comprobante") = dtDatos.Rows(i).Item("COMPROBANTE").ToString.Trim
                mFila("Detalle") = dtDatos.Rows(i).Item("DOC_DETALLE").ToString.Trim
                mFila("Vencimiento") = dtDatos.Rows(i).Item("DOC_VENC").ToString.Trim
                mFila("Fecha_Vencimiento") = dtDatos.Rows(i).Item("DOC_FECHAVENC").ToString.Trim

                'mFila("Condicion_Especial") = dtDatos.Rows(i).Item("DDV_CONDESPECIAL").ToString.Trim
                mFila("Importe_Origen") = DarFormato(dtDatos.Rows(i).Item("DOC_IMPORTEORIGEN").ToString)
                mFila("Importe_Recargo") = DarFormato(dtDatos.Rows(i).Item("DOC_IMPORTERECARGO").ToString)
                mFila("Importe_Total") = DarFormato(dtDatos.Rows(i).Item("DOC_IMPORTETOTAL").ToString)


                If (Application("tipo_conexion").ToString.ToUpper = "FOX") Then
                    mFila("usa_check") = dtDatos.Rows(i).Item("DOC_USA_CHECK").ToString.Trim
                End If

                dtCtaCteOtrosComprobantes.Rows.Add(mFila)
            Next


            Session("grilla_cta_cte_otros_comprobantes") = dtCtaCteOtrosComprobantes
            dtDatos_AUX = dtCtaCteOtrosComprobantes
            Return dtCtaCteOtrosComprobantes
        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)

            Return Nothing
        End Try

    End Function



#End Region

#Region "Procedimientos y Funciones"

    Private Sub LimpiarSession()
        Dim mNroUsuario As Integer

        Try
            If (Application("tipo_conexion") IsNot Nothing) Then
                If (Application("tipo_conexion").ToString.Trim = "RAFAM") Then
                    If (Session("nro_usuario") IsNot Nothing) Then
                        If (Session("nro_usuario").ToString.Trim <> "") Then

                            mNroUsuario = CInt(Session("nro_usuario"))

                            Me.mWS = New ws_consulta_tributaria.Service1
                            Me.mWS.LimpiarDatosSession(mNroUsuario)
                            Me.mWS = Nothing

                            Application("cant_usuarios") = CInt(Application("cant_usuarios")) - 1
                        End If
                    End If
                End If
            End If


        Catch ex As Exception
            Me.mLog = New Clslog
            Me.mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            Me.mLog = Nothing
        Finally

            Session.RemoveAll()
            Session.Clear()
            Session.Abandon()

            Call RedireccionarPaginaInicio()
        End Try
    End Sub


    Private Function ObtenerDatosContribuyente() As String
        Dim mNroUsuario As Integer
        Dim mDatosContribuyente As String


        Try
            mDatosContribuyente = "|||"
            If (Session("nro_usuario") IsNot Nothing) Then
                If (Session("nro_usuario").ToString.Trim <> "") Then

                    mNroUsuario = CInt(Session("nro_usuario"))

                    Me.mWS = New ws_consulta_tributaria.Service1
                    'mDatosContribuyente = Me.mWS.ObtenerDatosContribuyente(mNroUsuario)
                    Me.mWS = Nothing

                End If
            End If

            Return mDatosContribuyente.ToString
        Catch ex As Exception
            Me.mLog = New Clslog
            Me.mLog.GenerarLog(ex, Me.ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            Me.mLog = Nothing
            Return "|||"
        End Try

    End Function

    Private Function ObtenerDatosContribuyente(ByVal pTipoImponible As String, ByVal pNroImponible As String) As String
        Dim mDatosContribuyente As String


        Try
            mDatosContribuyente = "|||"
            If (Session("nro_usuario") IsNot Nothing) Then
                If (Session("nro_usuario").ToString <> "") Then

                    mWS = New ws_consulta_tributaria.Service1
                    'mDatosContribuyente = mWS.ObtenerDatosContribuyente(pTipoImponible, )
                    mWS = Nothing

                End If
            End If

            Return mDatosContribuyente.ToString
        Catch ex As Exception
            Me.mLog = New Clslog
            Me.mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            Me.mLog = Nothing
            Return "|||"
        End Try

    End Function




    Private Function ObtenerDatosXML() As DataSet
        Dim mRutaFisica As String
        Dim mArchivo As String
        Dim dsDatos As DataSet

        Try

            mRutaFisica = Request.ServerVariables("APPL_PHYSICAL_PATH").ToString

            If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString & "datos_temporales")) Then
                My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString & "datos_temporales")
            End If

            mArchivo = mRutaFisica.ToString & "datos_temporales\" & Format(Now, "ddMMyyyyHHmmss") & ".xml"


            Dim mCrearXML As New IO.StreamWriter(mArchivo.ToString)
            mCrearXML.WriteLine(mXml.ToString)
            mCrearXML.Close()

            dsDatos = New DataSet
            dsDatos.ReadXml(mArchivo.ToString)

            My.Computer.FileSystem.DeleteFile(mArchivo.ToString)


            Return dsDatos
        Catch ex As Exception
            Me.mLog = New Clslog
            Me.mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            Me.mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)
            Return Nothing
        End Try
    End Function

    Private Sub RedireccionarPaginaInicio()
        Response.Redirect("webfrmindex.aspx", False)
    End Sub


#End Region

#Region "LOG"

    Private Function ObtenerDetalleWEB() As String
        Dim mInfoLog As String = ""
        Try

            For Each mNombreVariable In Request.ServerVariables
                mInfoLog = mInfoLog & mNombreVariable.ToString & ": " & Request.ServerVariables(mNombreVariable.ToString)
                mInfoLog = mInfoLog & vbCrLf
            Next

            Return mInfoLog.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function
#End Region

#Region "Generacion XMLs"

    Private Function ObtenerXMLComprobantesVencidos() As String
        Dim mXML As String
        Dim mDato() As String



        mXML = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCrLf
        mXML = mXML & "<VFPDATA>" & vbCrLf

        If (Application("tipo_conexion").ToString = "FOX") Then
            mXML = mXML & "<DETALLEPERIODOSSELECCIONADOS>" & vbCrLf & _
                          "<NROIMPONIBLE></NROIMPONIBLE>" & vbCrLf & _
                          "<TIPOIMPONIBLE></TIPOIMPONIBLE>" & vbCrLf & _
                          "<RECURSO>A1</RECURSO>" & vbCrLf & _
                          "<ANIO></ANIO>" & vbCrLf & _
                          "<CUOTA></CUOTA>" & vbCrLf & _
                          "<NROMOV></NROMOV>" & vbCrLf & _
                          "</DETALLEPERIODOSSELECCIONADOS>" & vbCrLf


            For i = 0 To gvperiodos_vencidos.Rows.Count - 1
                If CType(gvperiodos_vencidos.Rows(i).Cells(11).Controls(1), CheckBox).Checked = True Then

                    mXML = mXML & "<DETALLEPERIODOSSELECCIONADOS>" & vbCrLf & _
                                  "<NROIMPONIBLE>" & lblnro_de_cuenta.Text & "</NROIMPONIBLE>" & vbCrLf & _
                                  "<TIPOIMPONIBLE>" & ObtenerTipoCuenta(Session("tipo_cuenta").ToString) & "</TIPOIMPONIBLE>" & vbCrLf & _
                                  "<RECURSO>" & gvperiodos_vencidos.Rows(i).Cells(1).Text & "</RECURSO>" & vbCrLf & _
                                  "<ANIO>" & gvperiodos_vencidos.Rows(i).Cells(2).Text & "</ANIO>" & vbCrLf & _
                                  "<CUOTA>" & gvperiodos_vencidos.Rows(i).Cells(3).Text & "</CUOTA>" & vbCrLf & _
                                  "<NROMOV>" & gvperiodos_vencidos.Rows(i).Cells(0).Text & "</NROMOV>" & vbCrLf & _
                                  "</DETALLEPERIODOSSELECCIONADOS>" & vbCrLf
                End If
            Next




            'XML para RAFAM
        Else


            mXML = ""
            mXML = mXML & Session("nro_usuario").ToString
            For i = 0 To gvperiodos_vencidos.Rows.Count - 1
                If CType(gvperiodos_vencidos.Rows(i).Cells(11).Controls(1), CheckBox).Checked = True Then

                    'Obtengo el Tipo y el Numero de Comprobante
                    'mDato = gvperiodos_vencidos.DataKeys.Item(i).Value.ToString.Split(CChar("|"))
                    mDato = gvperiodos_vencidos.Rows(i).Cells(0).Text.Split(CChar("/"))
                    mXML = mXML & "#" & gvperiodos_vencidos.Rows(i).Cells(1).Text & "|" & mDato(0).ToString & "|" & mDato(1).ToString
                End If
            Next

            Return mXML.ToString



            '############################################################################################################
            '# Esto esta comentado porque por una actualización de seguridad no se permite enviar xml al Web Services   #
            '# Ahora envío una cadena de string separada por el caracter pipe                                           #
            '############################################################################################################
            ''Asigno el Nro. de Usuario
            'mXML = mXML & "<USUARIO><NRO_USUARIO>" & Session("nro_usuario").ToString & "</NRO_USUARIO></USUARIO>"

            'For i = 0 To gvperiodos_vencidos.Rows.Count - 1
            '    If CType(gvperiodos_vencidos.Rows(i).Cells(11).Controls(1), CheckBox).Checked = True Then

            '        'Obtengo el Tipo y el Numero de Comprobante
            '        'mDato = gvperiodos_vencidos.DataKeys.Item(i).Value.ToString.Split(CChar("|"))
            '        mDato = gvperiodos_vencidos.Rows(i).Cells(0).Text.Split(CChar("/"))

            '        mXML = mXML & "<DETALLEPERIODOSSELECCIONADOS>" & vbCrLf & _
            '                            "<NRO_RECURSO>" & gvperiodos_vencidos.Rows(i).Cells(1).Text & "</NRO_RECURSO>" & vbCrLf & _
            '                            "<TIPO_COMPROBANTE>" & mDato(0).ToString & "</TIPO_COMPROBANTE>" & vbCrLf & _
            '                            "<NRO_COMPROBANTE>" & mDato(1).ToString & "</NRO_COMPROBANTE>" & vbCrLf & _
            '                      "</DETALLEPERIODOSSELECCIONADOS>" & vbCrLf
            '    End If
            'Next
        End If

        mXML = mXML & "</VFPDATA>"
        Return mXML.ToString
    End Function

    Private Function ObtenerXMLComprobantesNoVencidos(ByVal mGrilla As GridView) As String
        Dim mGrillaGenerica As GridView
        Dim mXML As String
        Dim mDato() As String
        Dim detalle_aux As String


        'Obtengo la Grilla de la cual obtengo los datos
        mGrillaGenerica = New GridView
        mGrillaGenerica = mGrilla



        mXML = "<VFPDATA>" & vbCrLf



        If (Application("tipo_conexion").ToString = "FOX") Then
            mXML = mXML & "<DETALLECOMPROBSELECCIONADOS>" & vbCrLf & _
                                   "<TIPOIMPONIBLE></TIPOIMPONIBLE>" & vbCrLf & _
                                   "<NROIMPONIBLE></NROIMPONIBLE>" & vbCrLf & _
                                   "<GRUPOCOMP></GRUPOCOMP>" & vbCrLf & _
                                   "<NROCOMP></NROCOMP>" & vbCrLf & _
                          "</DETALLECOMPROBSELECCIONADOS>"


            For i = 0 To mGrillaGenerica.Rows.Count - 1
                If CType(mGrillaGenerica.Rows(i).Cells(8).Controls(1), CheckBox).Checked = True Then


                    'Obtengo el Grupo y el Nro del Comprobante
                    mDato = mGrillaGenerica.Rows(i).Cells(0).Text.ToString.Split(CChar("/"))


                    mXML = mXML & "<DETALLECOMPROBSELECCIONADOS>" & vbCrLf & _
                                        "<TIPOIMPONIBLE>" & ObtenerTipoCuenta(Session("tipo_cuenta").ToString) & "</TIPOIMPONIBLE>" & vbCrLf & _
                                        "<NROIMPONIBLE>" & lblnro_de_cuenta.Text & "</NROIMPONIBLE>" & vbCrLf & _
                                        "<GRUPOCOMP>" & mDato(0).ToString & "</GRUPOCOMP>" & vbCrLf & _
                                        "<NROCOMP>" & mDato(1).ToString & "</NROCOMP>" & vbCrLf & _
                                  "</DETALLECOMPROBSELECCIONADOS>" & vbCrLf
                End If
            Next





            'XML para RAFAM
        Else



            mXML = ""
            mXML = mXML & Session("nro_usuario").ToString
            For i = 0 To mGrillaGenerica.Rows.Count - 1
                If CType(mGrillaGenerica.Rows(i).Cells(8).Controls(1), CheckBox).Checked = True Then

                    'Obtengo el Tipo y el Numero de Comprobante
                    'mDato = gvperiodos_vencidos.DataKeys.Item(i).Value.ToString.Split(CChar("|"))
                    mDato = mGrillaGenerica.Rows(i).Cells(0).Text.Split(CChar("/"))

                    detalle_aux = getDescripcion(mGrillaGenerica.Rows(i).Cells(1).Text)
                    mXML = mXML & "#" & detalle_aux & "|" & mDato(0).ToString & "|" & mDato(1).ToString
                End If
            Next

            Return mXML.ToString




            '############################################################################################################
            '# Esto esta comentado porque por una actualización de seguridad no se permite enviar xml al Web Services   #
            '# Ahora envío una cadena de string separada por el caracter pipe                                           #
            '############################################################################################################
            ''Asigno el Nro. de Usuario
            'mXML = mXML & "<USUARIO><NRO_USUARIO>" & Session("nro_usuario").ToString & "</NRO_USUARIO></USUARIO>"

            'For i = 0 To mGrillaGenerica.Rows.Count - 1
            '    If CType(mGrillaGenerica.Rows(i).Cells(7).Controls(1), CheckBox).Checked = True Then

            '        'Obtengo el Tipo y el Numero de Comprobante
            '        mDato = mGrillaGenerica.Rows(i).Cells(0).Text.ToString.Split(CChar("/"))

            '        mXML = mXML & "<DETALLEPERIODOSSELECCIONADOS>" & vbCrLf & _
            '                            "<NRO_RECURSO>0</NRO_RECURSO>" & vbCrLf & _
            '                            "<TIPO_COMPROBANTE>" & mDato(0).ToString & "</TIPO_COMPROBANTE>" & vbCrLf & _
            '                            "<NRO_COMPROBANTE>" & mDato(1).ToString & "</NRO_COMPROBANTE>" & vbCrLf & _
            '                      "</DETALLEPERIODOSSELECCIONADOS>" & vbCrLf
            '    End If
            'Next
        End If

        mXML = mXML & "</VFPDATA>"
        Return mXML.ToString
    End Function

#End Region

#Region "Generacion de Filas"

    Private Sub gvperiodos_vencidos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvperiodos_vencidos.RowDataBound
        Dim mTipo_Y_NroComprobante As String
        Dim mFila_AUX As Integer
        Dim boolExcluirCheck As Boolean
        Dim boolContieneCondicionesEspeciales As Boolean


        If (e.Row.RowType = DataControlRowType.DataRow) Then


            'Obtengo el Tipo Y Nro de Comprobantes
            If (Application("tipo_conexion").ToString = "RAFAM") Then
                mTipo_Y_NroComprobante = dtDatos_AUX.Rows(mFila).Item("Nro_Movimiento").ToString.Substring(0, dtDatos_AUX.Rows(mFila).Item("Nro_Movimiento").ToString.Length - 2)
            Else
                mTipo_Y_NroComprobante = dtDatos_AUX.Rows(mFila).Item("Nro_Movimiento").ToString
            End If


            ' Si el movimiento corresponde a un recurso a excluir y no esta logueado como admin, no permito que lo seleccione
            boolExcluirCheck = False
            If (dtDatos_AUX.Rows(mFila).Item("usa_check").ToString.Trim() = "N") Then
                boolExcluirCheck = True

                If (Session("loginAdmin") IsNot Nothing) Then
                    If (CBool(Session("loginAdmin"))) Then
                        boolExcluirCheck = False
                    End If
                End If
            End If



            ' Si en la columna "Condicion_Especial" viene alguna leyenda significa que no se pude 
            ' imprimir esta cuota desde la Web
            boolContieneCondicionesEspeciales = False
            boolContieneCondicionesEspeciales = CBool(dtDatos_AUX.Rows(mFila).Item("Condicion_Especial").ToString.Trim() <> "")



            If (strNombreMunicipio.Trim.ToUpper = "AZUL") And (Me.boolDeudaEnJuicio) Then
                e.Row.Cells(11).Controls.Clear()
            Else

                'Eventos JAVASCRIPT
                mFila = mFila + 1
                mFila_AUX = mFila + 1
                If (boolExcluirCheck) Or (boolContieneCondicionesEspeciales) Then
                    e.Row.Cells(11).Controls.Clear()
                Else
                    e.Row.Cells(11).Attributes.Add("onclick", "javascript:evento_click_check('gvperiodos_vencidos', '" & mTipo_Y_NroComprobante.ToString & "', '" & mFila_AUX.ToString.PadLeft(2, CChar("0")) & "');")
                    e.Row.Cells(11).Attributes.Add("onMouseUp", "javascript:refresh('gvperiodos_vencidos', '" & mTipo_Y_NroComprobante.ToString & "', '" & mFila_AUX.ToString.PadLeft(2, CChar("0")) & "');")
                End If
            End If
        End If
    End Sub


    Private Sub gvperiodos_no_vencidos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvperiodos_no_vencidos.RowDataBound
        Dim mTipo_Y_NroComprobante As String
        Dim mFila_AUX As Integer
        Dim boolContieneCondicionesEspeciales As Boolean
        Dim boolExcluirCheck As Boolean


        If (e.Row.RowType = DataControlRowType.DataRow) Then

            'Obtengo el Tipo Y Nro de Comprobantes
            mTipo_Y_NroComprobante = dtDatos_AUX.Rows(mFila).Item("Comprobante").ToString.Substring(0, dtDatos_AUX.Rows(mFila).Item("Comprobante").ToString.Length - 2)


            ' Si el comprobante contiene un recurso a excluir y no esta logueado como admin, no permito que lo seleccione
            boolExcluirCheck = False
            If (dtDatos_AUX.Rows(mFila).Item("usa_check").ToString.Trim() = "N") Then
                boolExcluirCheck = True
                If (Session("loginAdmin") IsNot Nothing) Then
                    If (CBool(Session("loginAdmin"))) Then
                        boolExcluirCheck = False
                    End If
                End If
            End If


            ' Si en la columna "Condicion_Especial" viene alguna leyenda significa que no se pude 
            ' imprimir esta cuota desde la Web
            boolContieneCondicionesEspeciales = False
            boolContieneCondicionesEspeciales = CBool(dtDatos_AUX.Rows(mFila).Item("Condicion_Especial").ToString.Trim() <> "")


            mFila = mFila + 1
            mFila_AUX = mFila + 1
            If (boolExcluirCheck) Or (boolContieneCondicionesEspeciales) Then
                e.Row.Cells(8).Controls.Clear()
            Else

                'Eventos de JAVASCRIPT
                e.Row.Cells(8).Attributes.Add("onclick", "javascript:evento_click_check('gvperiodos_no_vencidos', '" & mTipo_Y_NroComprobante.ToString & "', '" & mFila_AUX.ToString.PadLeft(2, CChar("0")) & "');")
                e.Row.Cells(8).Attributes.Add("onMouseUp", "javascript:refresh('gvperiodos_no_vencidos', '" & mTipo_Y_NroComprobante.ToString & "', '" & mFila_AUX.ToString.PadLeft(2, CChar("0")) & "');")
            End If


        End If
    End Sub

    Private Sub gvperiodos_otros_comprobantes_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvperiodos_otros_comprobantes.RowDataBound
        Dim mTipo_Y_NroComprobante As String
        Dim mFila_AUX As Integer        
        Dim boolContieneCondicionesEspeciales As Boolean
        Dim boolExcluirCheck As Boolean


        If (e.Row.RowType = DataControlRowType.DataRow) Then

            'Obtengo el Tipo Y Nro de Comprobantes
            mTipo_Y_NroComprobante = dtDatos_AUX.Rows(mFila).Item("Comprobante").ToString.Substring(0, dtDatos_AUX.Rows(mFila).Item("Comprobante").ToString.Length - 2)


            ' Si el comprobante contiene un recurso a excluir y no esta logueado como admin, no permito que lo seleccione
            boolExcluirCheck = False
            If (dtDatos_AUX.Rows(mFila).Item("usa_check").ToString.Trim() = "N") Then
                boolExcluirCheck = True
                If (Session("loginAdmin") IsNot Nothing) Then
                    If (CBool(Session("loginAdmin"))) Then
                        boolExcluirCheck = False
                    End If
                End If
            End If



            ' Si en la columna "Condicion_Especial" viene alguna leyenda significa que no se pude 
            ' imprimir esta cuota desde la Web
            boolContieneCondicionesEspeciales = False
            boolContieneCondicionesEspeciales = CBool(dtDatos_AUX.Rows(mFila).Item("Condicion_Especial").ToString.Trim() <> "")


            mFila = mFila + 1
            mFila_AUX = mFila + 1          
            If (boolExcluirCheck) Or (boolContieneCondicionesEspeciales) Then
                e.Row.Cells(8).Controls.Clear()
            Else
                e.Row.Cells(8).Attributes.Add("onclick", "javascript:calcular_importe('gvperiodos_otros_comprobantes', '" & mTipo_Y_NroComprobante.ToString & "', " & mFila.ToString & ");")
                e.Row.Cells(8).Attributes.Add("onMouseUp", "javascript:refresh('gvperiodos_otros_comprobantes', '" & mTipo_Y_NroComprobante.ToString & "', " & mFila.ToString & ");")
            End If
        End If
    End Sub

#End Region

    Private Function ObtenerTipoCuenta(ByVal mTipoCuenta As String) As String
        Select Case mTipoCuenta.ToUpper
            Case "INMUEBLE" : Return "I"
            Case "COMERCIO" : Return "C"
            Case "CEMENTERIO", "O" : Return "E"
            Case "VEHÍCULO"
                If (Application("tipo_conexion").ToString.ToUpper = "RAFAM") Then
                    Return "R"
                Else
                    Return "V"
                End If
            Case Else : Return ""
        End Select
    End Function


    Private Function ReemplazarPalabraVehiculo(ByVal mPalabra As String) As String
        If (mPalabra.ToString = "VEHÃ­CULO") Then
            Return "Vehículo".ToString.ToUpper
        Else
            Return mPalabra.ToString.ToUpper
        End If
    End Function


    Protected Sub boton_Imprimir_comprob_vencidos_Click(ByVal sender As Object, ByVal e As EventArgs) Handles boton_Imprimir_comprob_vencidos.Click
        Dim mXML As String = ""
        Dim mXMLComprobantesVencidos As String = ""



        If (Session("tipo_cuenta") IsNot Nothing) And (Session("nro_cuenta") IsNot Nothing) Then
            mXMLComprobantesVencidos = ObtenerXMLComprobantesVencidos()



            'Obtengo los datos
            'Dim dsDatos As New DataSet
            'dsDatos.ReadXml(New IO.StringReader(mXMLComprobantesVencidos.Trim))


            Me.mWS = New ws_consulta_tributaria.Service1
            mXML = Me.mWS.EmitirComprobantes(mXMLComprobantesVencidos.ToString, True)
            'mXML = Me.mWS.EmitirComprobantes_ds(dsDatos, True)
            Me.mWS = Nothing


            If (mXML.ToString <> "Error") Then
                Call Me.actualizarVisita()
                Session("xml_comprobantes") = mXML.ToString
                Response.Redirect("comprobantes/webfrmcomprobantepago.aspx", False)
            End If
        Else
            Call Me.LimpiarSession()
        End If
    End Sub

    Protected Sub boton_Imprimir_comprob_no_vencidos_Click(ByVal sender As Object, ByVal e As EventArgs) Handles boton_Imprimir_comprob_no_vencidos.Click
        Dim mXML As String = ""
        Dim mXMLComprobantesNoVencidos As String = ""

        If (Session("tipo_cuenta") IsNot Nothing) And (Session("nro_cuenta") IsNot Nothing) Then
            mXMLComprobantesNoVencidos = Me.ObtenerXMLComprobantesNoVencidos(Me.gvperiodos_no_vencidos)
            Me.mWS = New ws_consulta_tributaria.Service1
            mXML = Me.mWS.EmitirComprobantes(mXMLComprobantesNoVencidos.ToString, False)
            Me.mWS = Nothing


            If (mXML.ToString <> "Error") Then
                Call Me.actualizarVisita()
                Session("xml_comprobantes") = mXML.ToString
                Response.Redirect("comprobantes/webfrmcomprobantepago.aspx", False)
            End If
        Else
            Call Me.LimpiarSession()
        End If
    End Sub

    Protected Sub boton_Imprimir_comprob_otros_comprobantes_Click(ByVal sender As Object, ByVal e As EventArgs) Handles boton_Imprimir_comprob_otros_comprobantes.Click
        Dim mXML As String = ""
        Dim mXMLComprobantesNoVencidos As String = ""


        mXMLComprobantesNoVencidos = Me.ObtenerXMLComprobantesNoVencidos(Me.gvperiodos_otros_comprobantes)
        Me.mWS = New ws_consulta_tributaria.Service1        
        mXML = Me.mWS.EmitirComprobantes(mXMLComprobantesNoVencidos.ToString, False)
        Me.mWS = Nothing


        If (mXML.ToString <> "Error") Then
            Call Me.actualizarVisita()
            Session("xml_comprobantes") = mXML.ToString
            Response.Redirect("comprobantes/webfrmcomprobantepago.aspx", False)
        End If
    End Sub


    Private Function DarFormato(ByVal mImporte As String) As String

        If (Application("tipo_conexion").ToString = "RAFAM") Then
            'Return Format(CDbl(mImporte), "N2").ToString.Replace(",", Nothing)
            Return mImporte.ToString
        Else
            Return mImporte.ToString
        End If

        Return "0.00"
    End Function

    Protected Sub boton_informe_deuda_Click(ByVal sender As Object, ByVal e As EventArgs) Handles boton_informe_deuda.Click
        Dim mDatosContribuyente As String


        If (Application("tipo_conexion").ToString.Trim = "RAFAM") Then

            mDatosContribuyente = Me.ObtenerDatosContribuyente()
            Session("datos_contribuyente") = Me.lbltipo_de_cuenta.Text & "|" & _
                                             Me.lblnro_de_cuenta.Text & "|" & _
                                             mDatosContribuyente.ToString


        Else
            Session("datos_contribuyente") = Me.lbltipo_de_cuenta.Text & "|" & _
                                             Me.lblnro_de_cuenta.Text & "|" & _
                                             Me.lbltitular.Text

        End If


        Response.Redirect("comprobantes/webfrminformectactevencida.aspx", False)
    End Sub


    Private Sub actualizarVisita()
        Dim arrVisita As String()

        Try
            If (Not CBool(Session("visita_editada"))) Then
                arrVisita = Session("visita").ToString.Trim.Split(CChar("|"))

                Me.mWS = New ws_consulta_tributaria.Service1
                Me.mWS.actualizarVisita(arrVisita(0).Trim, arrVisita(1).Trim, arrVisita(2).Trim, CLng(arrVisita(3)))
            End If
        Catch ex As Exception
        Finally
            Me.mWS = Nothing
        End Try

    End Sub



    'Este método es utilizado sólo para azul.
    'En el caso de que al menos un registro este en juicio, el usuario no podrá seleccionar ningún movimiento adeudado
    Private Sub movimientosEnJuicioByAzul(ByVal dtDatos As DataTable)
        Me.boolDeudaEnJuicio = False
        For i = 0 To dtDatos.Rows.Count - 1
            If (dtDatos.Rows(i).Item("DDV_CONDESPECIAL").ToString = "En juicio") Then
                Me.boolDeudaEnJuicio = True
                Exit For
            End If
        Next
    End Sub


    Private Function getDescripcion(ByVal p_descripcion As String) As String
        Dim descripcion As String = ""

        Select Case p_descripcion
            Case "Nota de D&#233;bito" : descripcion = "Nota de Débito"
            Case "Emisi&#243;n General" : descripcion = "Emisión General"
            Case Else : descripcion = p_descripcion
        End Select

        Return descripcion
    End Function



End Class