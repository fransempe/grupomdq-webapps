﻿Option Explicit On
Option Strict On

Imports System
Imports System.Xml.Serialization

Imports System.Drawing.Color
Imports System.Drawing.Imaging
Imports System.Drawing
Imports System.IO
Imports web_tributaria.ws_consulta_tributaria.Service1
Imports System.Xml


Partial Public Class webfrmindex
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mRutaFisica As String
        Dim mNivel As String


        'Dim serializer As XmlSerializer
        'Dim comprobantesSeleccionados As List(Of clsComprobantes)
        'Dim a As String


        'comprobantesSeleccionados = New List(Of clsComprobantes)
        'comprobantesSeleccionados.Add(New clsComprobantes("1", "02", "7", "100"))
        'comprobantesSeleccionados.Add(New clsComprobantes("2", "02", "20", "10220"))
        'comprobantesSeleccionados.Add(New clsComprobantes("3", "02", "30", "1003030"))




        'serializer = New XmlSerializer(GetType(List(Of clsComprobantes)))
        'Dim sw As New System.IO.StringWriter
        'serializer.Serialize(sw, comprobantesSeleccionados)
        'a = sw.ToString
        'sw.Close()


        'Serialize object to a text file.
        'Dim objStreamWriter As New StreamWriter("C:\Product.xml")



        ''Deserialize text file to a new object.
        'Dim objStreamReader As New StreamReader("C:\prueba.xml")
        'Dim p2 As New clsComprobantes
        'p2 = CType(serializer.Deserialize(objStreamReader), clsComprobantes)
        'objStreamReader.Close()


 

        'Dim serializer1 As XmlSerializer
        'Dim stream1 As XmlTextReader


        'serializer1 = New XmlSerializer(GetType(List(Of clsComprobantes)))
        'stream1 = New XmlTextReader(New StringReader(a.Trim))


        'Dim comprob2 As New List(Of clsComprobantes)
        'comprob2 = CType(serializer1.Deserialize(stream1), List(Of clsComprobantes))




         
        







        'Obtengo la Ruta del archivo de Configuracion
        mRutaFisica = Server.MapPath("./").ToString



        'Obtengo el Tipo de conexion
        Application("tipo_conexion") = "Ninguna"
        Application("tipo_conexion") = System.Configuration.ConfigurationManager.AppSettings("TipoConexion").ToString.Trim


        'Obtengo el Nivel de Seguridad
        mNivel = "ALTO"
        mNivel = System.Configuration.ConfigurationManager.AppSettings("SeguridadLogin").ToString.Trim


        'Redirijo segun el Nivel

        Select Case mNivel.ToString.Trim
            Case "Bajo" : Response.Redirect("webfrmconsulta_b.aspx", False)
            Case "Medio" : Response.Redirect("webfrmconsulta_m.aspx", False)
            Case "Alto" : Response.Redirect("webfrmconsultab_a.aspx", False)
            Case "Mantenimiento" : Response.Redirect("webfrmmantenimiento.aspx", False)
        End Select

        'Me muevo un nivel para Atras
        mRutaFisica = Server.MapPath("../").ToString

        'Borro los Temporales
        Call LimpiarTemporales(mRutaFisica.ToString & "datos_temporales")
        Call LimpiarTemporales(mRutaFisica.ToString & "comprobantespdf")
    End Sub


    Private Sub LimpiarTemporales(ByVal mRutaFisica As String)
        Dim mDirectorio As DirectoryInfo
        Dim mArchivosInfo As FileInfo()
        Dim mArchivoBandera As String
        Dim mFechaLimpieza As Date


        Try

            mArchivoBandera = mRutaFisica.ToString & "\limpiar.txt"
            If (My.Computer.FileSystem.FileExists(mArchivoBandera.ToString)) Then
                mFechaLimpieza = CDate(My.Computer.FileSystem.ReadAllText(mArchivoBandera.ToString))
            Else
                mFechaLimpieza = Now.AddDays(-1)
            End If


            If (mFechaLimpieza <> CDate(Format(Now, "dd/MM/yyyy"))) Then
                mDirectorio = New DirectoryInfo(mRutaFisica.ToString)
                mArchivosInfo = mDirectorio.GetFiles()
                For Each mArchivo As FileInfo In mArchivosInfo
                    If (Format(mArchivo.CreationTime, "dd/MM/yyyy") < Format(Now, "dd/MM/yyyy")) Then
                        My.Computer.FileSystem.DeleteFile(mArchivo.FullName.ToString)
                    End If
                Next



                'Actualizo el archivo Bandera
                My.Computer.FileSystem.WriteAllText(mArchivoBandera.ToString, Format(Now, "dd/MM/yyyy"), False)
            End If


        Catch ex As Exception
        End Try
    End Sub


End Class