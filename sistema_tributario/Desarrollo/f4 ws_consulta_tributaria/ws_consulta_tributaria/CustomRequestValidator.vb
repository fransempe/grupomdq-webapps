﻿Imports Microsoft.VisualBasic
Imports System.Web.Util
Imports System.Runtime.InteropServices

Public Class CustomRequestValidator
    Inherits RequestValidator


    Protected Overrides Function _
       IsValidRequestString(ByVal context As HttpContext, _
        ByVal value As String,
        ByVal requestValidationSource As RequestValidationSource, _
        ByVal collectionKey As String, _
        <OutAttribute()> ByRef validationFailureIndex As Integer) As Boolean


        Return True
    End Function

End Class
