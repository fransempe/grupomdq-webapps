*************************************************
* FUNCTION FCBWEB
*************************************************
* 
* Fecha : 15/03/2007
* 
* Funcionamiento: Arma la cadena necesaria para imprimir el c�digo
*                 de barras para Escobar
* 
*	La funci�n original, FBaproCB, tiene par�metros de entrada. Esta
*	funci�n, en cambio, no recib par�metros (para que sea gen�rica para todas
*	las municipalidades) y toma los datos directamente de COMPROB
PRIVATE retorno

retorno = '164' && C�digo de Empresa
retorno = retorno + PadL (AllT (Str (comprob.tot_comp * 100, 8, 0)), 8, '0') && Importe al Primer Vencimiento
retorno = retorno + PadL (AllT (Str (Juliana (comprob.fec1_comp), 3, 0)), 3, '0') && Fecha del Primer Vencimiento Juliana
retorno = retorno + PadL (AllT (Str ((AuxImpTotOrig2 + comprob.tot2_act + comprob.tot2_int) * 100, 9, 0)), 9, '0') && Importe Fuera de T�rmino
retorno = retorno + PadL (ALLT (STR (comprob.fec2_comp - comprob.fec1_comp, 2, 0)), 2, '0')
retorno = retorno + PadL (AllT (Str (comprob.grupo_comp, 2, 0)), 2, '0') + PadL (AllT (Str (comprob.nro_comp, 8, 0)), 8, '0') && N�mero de Comprobante
retorno = retorno + PADL (ALLT (Str ((YEAR (comprob.fec1_comp) % 100), 2, 0)), 2, '0') && A�o de la fecha de vencimiento
retorno = retorno + PADL (ALLT (Str (MONTH (comprob.fec1_comp), 2, 0)), 2, '0') && Mes de la fecha de vencimiento
retorno = retorno + Str (DVCodBar (retorno), 1, 0)

RETURN retorno


*************************************************
FUNCTION DVCodBar
*************************************************
PARAMETERS numero
PRIVATE retorno, j, suma, i
retorno = 0

m.j = 1
m.suma = 0
FOR m.i = 1 TO Len (m.numero)
	m.suma = m.suma + Val (SubStr (m.numero, m.i, 1)) * j
	m.j = m.j + 2
	IF m.j > 9
		m.j = 3
	ENDIF
NEXT
retorno = Int (m.suma / 2) % 10

RETURN retorno


*************************************************
FUNCTION Juliana
*************************************************
PARAMETERS fecha
PRIVATE retorno, fecha_ini
retorno = 0

m.fecha_ini = CToD ('01/01/' + Str (Year (m.fecha), 4, 0))

retorno = m.fecha - m.fecha_ini + 1

RETURN retorno
