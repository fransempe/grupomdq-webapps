*************************************************
*FUNCTION subraya
*************************************************
PARAMETER cadena
cadena = ALLTRIM(cadena)
RETURN REPLICATE('-', LEN(cadena))
