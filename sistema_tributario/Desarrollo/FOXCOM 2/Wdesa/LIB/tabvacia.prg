*************************************************
*FUNCTION tabvacia
*************************************************
* Autor: Carlos
* 
* Fecha: 05/10/94
* 
* Funcionamiento:
* Selecciona el alias de la tabla que se pasa como
* par�metro, hace un GO TOP, y si da EOF (),
* retorna .T., dado que implica que la tabla no
* tiene registros. Sino retorna .F.
* No se utilizar reccount, porque si tengo
* registros borrados, reccount los considera, y
* me puede devolver un valor que no quiero.
* 
* Par�metros:
* 
* tabla	= N - es el alias en el que se debe verificar si
*							la tabla est� vac�a.
* 
* Modificaciones:
* 

PARAMETERS m.tabla
PRIVATE retorno, nro_reg, alias_ant
retorno = .F.

IF EMPTY (m.tabla)
	=msg ('No existe el alias ' + m.tabla)
	retorno = .T.
ENDIF
IF !retorno
	alias_ant = ALIAS ()
	SELECT (m.tabla)
	nro_reg = RECNO ()

	GO TOP

	IF EOF ()
		retorno = .T.
	ELSE
		IF nro_reg <= RECCOUNT ()
			GO nro_reg
		ELSE
			GO BOTTOM
		ENDIF
	ENDIF
	IF ! EMPTY (alias_ant)
		SELECT (alias_ant)
	ENDIF
ENDIF

RETURN retorno
