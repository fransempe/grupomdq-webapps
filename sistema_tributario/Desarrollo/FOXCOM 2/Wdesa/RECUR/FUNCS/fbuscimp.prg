*************************************************
* FUNCTION FBuscImp
*************************************************
* Autor : Gabriel Zubieta
* Dise�o: Leo.
* 
* Fecha : 17/04/95
* 
* Funcionamiento:
* Dados 5 parametros pide un orden de busqueda para la tabla de imp.
* especificada.
* 
* Par�metros:
* tipoimp  -C- Tipo de Imponible en cuestion
* vis_baja -L- Flag si visualiza bajas.
* vis_cont -L- Flag si visualiza contribuyente.
* @nro	   -N- Nro. de Imponible
* @dig     -N- Nro. de d�gito.
* 
* Modificaciones:
* Gabriel - Cambie los EVAL por & en los BROWSE
* Gabriel - 270995 Mejore llamado a fx. FVinCon 
*
*
*
*
*
*
*
* Definiciones Previas.
* - Llamar funcion fcarmas ( =FCarMas() ), dimensionar todos los arreglos antes.
* - Abiertas las tablas de contrib,vinc_imp y tips_imp
* - Inicializado vble M.NRO_INDICE con valor en rango [1..5]
* - Ejecutada fcion fleetim ( =FLeeTim( 'CONTRIB',.T.,@m.tipo_impc,@m.nro_impc, @m.dig_impc )
* - Definida clave_tip, clave_vin y clave_vic
*    clave_tip = getkey('tips_vin', .T.)
*	   clave_vin = getkey('vinc_imp', .T., 3, 'primario' )
*	   clave_vic = getkey('vinc_imp', .T.   , 'primario' )
*
*
************************************************************************
*	
*	SQLMODI-01
*		Usuario							: Azul
*		Fecha								: 27/11/2000
*		Quien modific�			: Carlos
*		Detalle problema		: Necesitaban una b�squeda alternativa por Clave.
*													
************************************************************************
*	
*	VISUALMODI_01
*		Usuario							: Escobar / Todos
*		Fecha								: 18/05/2006
*		Quien modific�			: Alejandro
*		Detalle problema		: Cuando se busca un imponible por nombre de contribuyente, trae 
*													contribuyentes que no est�n relacionados con el tipo de imponible
*													elegido.
*													
*		Detalle correcci�n	: Se muestran solamente los contribuyentes asociados al tipo de im-
*													ponible elegido.
*													
*		Programas tocados		: FBUSCIMP.PRG
*													
**********************************************************************************************
*	
*	VISUALMODI_02
*		Usuario							: Escobar / Todos
*		Fecha								: 18/05/2006
*		Quien modific�			: Alejandro
*													
*		Detalle correcci�n	: Se agreg� b�squeda por nombre de contribuyente.
*													
*		Programas tocados		: FBUSCIMP.PRG, FBuscIC.spr, FBuscIC.scx
*													
**********************************************************************************************

PARAMETERS m.tipoimp, m.vis_baja, m.vis_cont, m.nro, m.dig
EXTERNAL ARRAY ident_exp, tab_imp, ident_imp, ubic_exp, indi_ubi, indi_ide
PRIVATE retorno, m.ubic, area, orden, rec, tt_tabact, exp_busc
PRIVATE tecla

retorno = .T.
area  = SELECT()
orden = ORDER()

m.tecla = ''

exp_busc = ''

IF m.tipo_impc = m.tipoimp
	IF m.nro_indice > 3
		m.nro_indice = 1
	ENDIF
	DIME ind[3]
ELSE
	DIME ind[5]
	ind[4]='Nombre Contribuyente'
	ind[5]='CUIT'
ENDIF

ind[1]='N�mero'
ind[2]='Identificaci�n'
ind[3]='Ubicaci�n'


* Comienzo SQLMODI-01

IF m.tipoimp = 'I'
	DIME ind[6]
	ind[6]='Clave Ident.'
ENDIF

* Fin SQLMODI-01


DEFINE WINDOW vcambord FROM 6,2 TO 14,27 TITLE 'Orden de B�squeda' IN SCREEN COLOR SCHEME 10
ACTI WIND vcambord
@ 0,0 GET m.nro_indice FROM ind PICTURE '@&T' SIZE 9,24 MESSAGE "Elija el Orden por el cual quiere buscar el Imponible"
READ
RELE WIND vcambord

m.ubic    = ASC( m.tipoimp ) - 64
tt_tabact = ALLT( tab_imp[m.ubic] )

IF !(m.tipoimp == m.tipo_impc)
	=USET ( 'recur\' + tt_tabact )
ENDIF


* Comienzo SQLMODI-01

IF INLIST (m.nro_indice, 1, 2, 3, 6)

* Fin SQLMODI-01


	SELECT (tt_tabact)
*	IF !vis_baja
*		SET FILTER TO EMPTY( fec_baja )
*	ENDIF

ELSE
	SET ORDER TO imp_vinc IN vinc_imp
	SELECT contrib
	=RELT("m.tipo_impc+STR(contrib.cuit,10)+m.tipoimp", 'vinc_imp', .T. )
	SELECT vinc_imp
	=RELT("STR(vinc_imp.nro_imp,10)", tt_tabact                   , .T. )
	SELECT contrib
	cont = "STR(contrib.cuit,10) + SPAC(1) + SUBST(contrib.nomb_cont,1,18) + SPAC(1) + STR(vinc_imp.nro_imp,10)"
	
ENDIF

* [*] Modi Nicol�s (06/02/98). Saco el filtro fuera de la condici�n
	IF !vis_baja
		SET FILTER TO EMPTY( fec_baja )
	ENDIF


* Define ventana para lista de opciones
DEFINE WINDOW wifbuscimp FROM 4,3 TO 24,77 IN SCREEN DOUBLE FLOAT SHADOW COLOR SCHEME 10

_jexitkey = 13
_JDBLCLICK= -1
=jkeyinit("U",""," Buscando: ","")

PUSH KEY

* ON KEY LABEL ctrl+home DO BusqSecuen
* ON KEY LABEL home DO NewBusq
ON KEY LABEL ctrl+home DO Saltar WITH 2
ON KEY LABEL home DO Saltar WITH 1

* Comienzo Alejandro
PRIVATE lEnter 
lEnter = .F.
ON KEY LABEL Enter DO PulsoEnter
* Fin Alejandro

DO CASE 
CASE m.nro_indice = 1
	SET ORDER TO primario
	GO TOP
	IF m.tipo_impc # m.tipoimp AND vis_cont

		m.exp_busc = 'Str (Eval (ident_imp[m.ubic])) + Eval (ident_exp[m.ubic]) + Vinc_Con()'

		BROW FIELDS Imponible=&ident_imp[m.ubic] , Identif= &ident_exp[m.ubic], Contrib=Vinc_Con() ;
			NOAPPEND NODELETE NOEDIT REST TITLE 'Buscando Imponible por N�mero ' WINDOW wifbuscimp TIMEOUT 90 ;
			NOREFRESH WHEN Buscar ()
	ELSE

		m.exp_busc = 'Str (Eval (ident_imp[m.ubic])) + Eval (ident_exp[m.ubic])'

		BROW FIELDS Imponible=&ident_imp[m.ubic] , Identif= &ident_exp[m.ubic] ;
			NOAPPEND NODELETE NOEDIT REST TITLE 'Buscando Imponible por N�mero ' WINDOW wifbuscimp TIMEOUT 90 ;
			NOREFRESH WHEN Buscar ()
	ENDIF
				
CASE m.nro_indice = 2
	SET ORDER TO indi_ide[m.ubic]
	GO TOP
	IF m.tipo_impc # m.tipoimp AND vis_cont

		m.exp_busc = 'Str (Eval (ident_imp[m.ubic])) + Eval (ident_exp[m.ubic]) + Vinc_Con()'

		BROW FIELDS Identif= &ident_exp[m.ubic], Imponible=&ident_imp[m.ubic], Contrib=Vinc_Con() ;
			NOAPPEND NODELETE NOEDIT REST TITLE 'Buscando Imponible por Identificaci�n' WINDOW wifbuscimp TIMEOUT 90 ;
			NOREFRESH WHEN Buscar ()
	ELSE

		m.exp_busc = 'Str (Eval (ident_imp[m.ubic])) + Eval (ident_exp[m.ubic])'

		BROW FIELDS Identif= &ident_exp[m.ubic], Imponible=&ident_imp[m.ubic] ;
			NOAPPEND NODELETE NOEDIT REST TITLE 'Buscando Imponible por Identificaci�n' WINDOW wifbuscimp TIMEOUT 90 ;
			NOREFRESH WHEN Buscar ()
	ENDIF
	
CASE m.nro_indice = 3
	SET ORDER TO indi_ubi[m.ubic]
	GO TOP
	IF m.tipo_impc # m.tipoimp AND vis_cont

		m.exp_busc = 'Str (Eval (ident_imp[m.ubic])) + Eval (ubic_exp[m.ubic]) + Vinc_Con()'

		BROW FIELDS Ubic = &ubic_exp[m.ubic]:50 , Imponible=&ident_imp[m.ubic], Contrib=Vinc_Con() ;
			NOAPPEND NODELETE NOEDIT REST TITLE 'Buscando Imponible por Ubicaci�n' WINDOW wifbuscimp TIMEOUT 90 ;
			NOREFRESH WHEN Buscar ()
	ELSE

		m.exp_busc = 'Str (Eval (ident_imp[m.ubic])) + Eval (ubic_exp[m.ubic])'

		BROW FIELDS Ubic = &ubic_exp[m.ubic]:50 , Imponible=&ident_imp[m.ubic] ;
			NOAPPEND NODELETE NOEDIT REST TITLE 'Buscando Imponible por Ubicaci�n' WINDOW wifbuscimp TIMEOUT 90 ;
			NOREFRESH WHEN Buscar ()
	ENDIF
	
CASE m.nro_indice = 4
	SET ORDER TO nomb_con
	GO TOP

	m.exp_busc = 'Eval (cont) + Eval (ident_exp[m.ubic])'

	*	VISUALMODI_01
	* Se agreg� la l�nea FOR vinc_imp.nro_imp > 0
	
	BROW FIELDS Contrib= &cont, Identif= &ident_exp[m.ubic] ;
		FOR vinc_imp.nro_imp > 0 ;
		NOAPPEND NODELETE NOEDIT REST TITLE 'Buscando Imponible por Nombre de Cont.' WINDOW wifbuscimp TIMEOUT 90 ;
			NOREFRESH WHEN Buscar ()
		
CASE m.nro_indice = 5
	SET ORDER TO primario
	GO TOP

	m.exp_busc = 'Eval (cont) + Eval (ident_exp[m.ubic])'

	*	VISUALMODI_01
	* Se agreg� la l�nea FOR vinc_imp.nro_imp > 0

	BROW FIELDS Contrib=&cont, Identif= &ident_exp[m.ubic] ;
		FOR vinc_imp.nro_imp > 0 ;
		NOAPPEND NODELETE NOEDIT REST TITLE 'Buscando Imponible por N�mero de Cont.' WINDOW wifbuscimp TIMEOUT 90 ;
			NOREFRESH WHEN Buscar ()


* Comienzo SQLMODI-01

CASE m.nro_indice = 6
	SET ORDER TO clave
	GO TOP
	IF m.tipo_impc # m.tipoimp AND vis_cont

		m.exp_busc = 'Str (Eval (ident_imp[m.ubic])) + Eval (ident_exp[m.ubic]) + Vinc_Con()'

		BROW FIELDS Imponible=&ident_imp[m.ubic], Clave=inmueble.clave_inm, Contrib=Vinc_Con() ;
			NOAPPEND NODELETE NOEDIT REST TITLE 'Buscando Imponible por Clave ' WINDOW wifbuscimp TIMEOUT 90 ;
			NOREFRESH WHEN Buscar ()
	ELSE
		m.exp_busc = 'Str (Eval (ident_imp[m.ubic])) + Eval (ident_exp[m.ubic])'

		BROW FIELDS Imponible=&ident_imp[m.ubic], Clave=inmueble.clave_inm ;
			NOAPPEND NODELETE NOEDIT REST TITLE 'Buscando Imponible por Clave ' WINDOW wifbuscimp TIMEOUT 90 ;
			NOREFRESH WHEN Buscar ()
	ENDIF

* Fin SQLMODI-01


ENDCASE

ON KEY LABEL home

POP KEY

RELE WIND wifbuscimp

=jkeycanc()

* retorno = ( LKEY() = 13 )
retorno = (LKEY() = 13) OR lEnter	&& ### XXX Alejandro
IF retorno
	cpo = tt_tabact + '.' + ALLT( ident_imp[m.ubic] )
	m.nro = EVAL( cpo )
	m.dig = &tt_tabact..dig_veri
ENDIF

* restaurar todo
IF INLIST (m.nro_indice, 1, 2, 3)
	SET FILTER TO
ELSE
	SET RELATION OFF INTO vinc_imp
	SET SKIP TO 
	SELECT vinc_imp
	SET RELATION OFF INTO (tab_imp[m.ubic])
	SET SKIP TO
	SELECT contrib
ENDIF

SET ORDER TO primario
SET ORDER TO primario IN vinc_imp

SELECT (area)
IF !EMPTY (orden)
	SET ORDER TO orden IN (area)
ENDIF
RETURN retorno

*************************************************
FUNCTION Vinc_Con   
*************************************************
* Es similar a FVinCon pero que cubre s�lo las necesidades de este
PRIVATE retorno, m.cuit1, m.cuit2, m.prior1, m.prior2, reto, m.nrocuit, m.expresion

retorno 		= .T.
m.cuit1 		= -1
m.cuit2     = -1
m.prior1		= 10
m.prior2    = 10

m.expresion = m.tipoimp + STR (EVAL (ident_imp[m.ubic]), 10) + m.tipo_impc 
SET KEY TO m.expresion IN VINC_IMP
GO TOP IN VINC_IMP
GO TOP IN vinc_imp
DO WHILE retorno AND !EOF ('vinc_imp')
	IF EMPTY(vinc_imp.fec_baja)
		IF tips_vin.tipo_vinc == vinc_imp.tipo_vinc OR SEEK (vinc_imp.tipo_vinc, 'tips_vin')
			
			IF tips_vin.pri_cuit1 > 0 AND tips_vin.pri_cuit1 < m.prior1
				m.cuit1 	= vinc_imp.nro_impv
				m.prior1	= tips_vin.pri_cuit1
			ENDIF
			IF tips_vin.pri_cuit2 > 0 AND tips_vin.pri_cuit2 < m.prior2
				m.cuit2   = vinc_imp.nro_impv
				m.prior2  = tips_vin.pri_cuit2
			ENDIF
		ELSE
			retorno = .F.
		ENDIF
	ENDIF
	SKIP IN vinc_imp
ENDDO

SET KEY TO '' IN VINC_IMP

m.nrocuit = 0
IF m.cuit1 > 0
	m.nrocuit = m.cuit1
ELSE
	IF m.cuit2 > 0
		m.nrocuit = m.cuit2
	ENDIF
ENDIF

IF m.nrocuit > 0			
	IF SEEK (STR (m.nrocuit,10), 'contrib')
		reto = STR (contrib.cuit, 10) + SPACE (1) + contrib.nomb_cont
	ELSE
		reto = SPACE (35)
	ENDIF
ELSE
	reto = SPACE (35)
ENDIF				

RETURN reto

*****************************************
*FUNCTION Vinc_Con   Vieja, fue remplazada por la anterior
*****************************************
PRIVATE retorno, m.cuit1, m.cuit2
EXTERNAL ARRAY ident_imp

retorno = SPAC(28)
m.cuit1 = -1
m.cuit2 = -2
IF FVinCon (m.tipoimp, EVAL( ident_imp[m.ubic] ), .F., .F., @m.cuit1, @m.cuit2)
	retorno = STR( contrib.cuit, 10 ) + SPAC(1) + contrib.nomb_cont
ENDIF

RETURN retorno


*************************************************
FUNCTION NewBusq
*************************************************
* 
* Funcionamiento:
* Realiza una b�squeda secuencial sobre el browse.
* Es llamada al presionar la tecla Home.
* 
* 
PRIVATE m.desc_timp

* Desactiva Jkey
=jkeycanc()

PUSH KEY
ON KEY

IF tips_imp.tipo_imp != m.tipoimp
	= Seek (m.tipoimp, 'tips_imp')
ENDIF

DO CASE 
	CASE m.nro_indice = 1 && Nro. de imponible
		m.desc_timp = tips_imp.desc_timp
		DO FBuscI1.spr
	CASE m.nro_indice = 2 && Identificaci�n
		DO CASE
			CASE tips_imp.tipo_imp = 'I'
				* Por nomenclatura
				DO FBuscI21.spr
			CASE tips_imp.tipo_imp = 'C'
				* Por nombre del comercio
				* No necesita ventana
			CASE tips_imp.tipo_imp = 'N'
				* Por nombre del contribuyente
				* No necesita ventana
			CASE tips_imp.tipo_imp = 'O'
				* Por nomenclatura
				* No necesita ventana
			CASE tips_imp.tipo_imp = 'E'
				* Por nomenclatura
				DO FBuscI22.spr
			CASE tips_imp.tipo_imp = 'V'
				* Por dominio
				DO FBuscDom.spr
		ENDCASE
	CASE m.nro_indice = 3 && Ubicaci�n
		IF tips_imp.tipo_imp = 'I' OR ;
			 tips_imp.tipo_imp = 'C' OR ;
			 tips_imp.tipo_imp = 'N'
			m.desc_timp = tips_imp.desc_timp
			DO FBuscI3.spr
		ENDIF
	CASE m.nro_indice = 4 && Nombre contribuyente
		*	VISUALMODI_02
		DO FBuscIC.spr
	CASE m.nro_indice = 5 && CUIT contribuyente
		m.desc_timp = 'Contribuyente'
		DO FBuscI1.spr
ENDCASE

POP KEY

_jexitkey = 13
_JDBLCLICK = -1
=jkeyinit("U", ""," Buscando: ","")

RETURN .T.


*************************************************
FUNCTION BusqSecuen
*************************************************
* 
* Funcionamiento:
* Realiza una b�squeda secuencial sobre el browse.
* Es llamada al presionar la tecla Home.
* 
* 
PUBLIC _busqsecue
PRIVATE campo, busqsecue, reg_ant

* Desactiva Jkey
=jkeycanc()

PUSH KEY
ON KEY

DEFINE WINDOW ventsecue FROM 1, 30 TO 3, 77 IN SCREEN COLOR SCHEME 5
ACTIVATE WINDOW ventsecue

m.reg_ant = RecNo ()
m.busqsecue = Space (20)

IF Type ('m._busqsecue') = 'C' AND Len (m._busqsecue) = 20
	m.busqsecue = m._busqsecue
ENDIF
m._busqsecue = m.busqsecue

m.campo = VarRead ()

@ 0, 1 SAY 'Dato a buscar:'
@ 0, 16 GET m._busqsecue PICTURE '@KS30'
READ

IF LastKey () != 27
	SKIP
	* LOCATE FOR At (Upper (AllTrim (m._busqsecue)), Upper (Eval (campo))) >= 1 REST

	* LOCATE FOR At (Upper (AllTrim (m._busqsecue)), Upper (Str (Eval (ident_imp[m.ubic])) + Eval (ident_exp[m.ubic]) + Vinc_Con())) >= 1 REST

	LOCATE FOR At (Upper (AllTrim (m._busqsecue)), Upper (Eval (m.exp_busc))) >= 1 REST

	IF !Found ()
		= Msg ('No se encontr� ning�n registro que concuerde con el dato solicitado')
		= Ir_Reg (m.reg_ant)
	ENDIF
ENDIF

RELEASE WINDOWS ventsecue

POP KEY

_jexitkey = 13
_JDBLCLICK = -1
=jkeyinit("U", ""," Buscando: ","")

RETURN .T.


*************************************************
FUNCTION Saltar
*************************************************
PARAMETERS modo
PRIVATE retorno
retorno = .T.
*SKIP
*IF Eof ()
*	GO TOP
*ENDIF
KEYBOARD '{dnarrow}'
IF m.modo = 1
	m.tecla = 'HOME'
ELSE
	m.tecla = 'CHOME'
ENDIF
RETURN retorno


*************************************************
FUNCTION Buscar
*************************************************
PRIVATE retorno
retorno = .T.
DO CASE
	CASE m.tecla = 'HOME'
		DO NewBusq
	CASE m.tecla = 'CHOME'
		DO BusqSecuen
ENDCASE
m.tecla = ''
RETURN retorno
