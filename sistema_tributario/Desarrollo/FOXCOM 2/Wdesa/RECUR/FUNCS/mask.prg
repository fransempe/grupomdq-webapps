*************************************************
FUNCTION MASK
*************************************************
* Autor : RODRIGO
* Enmascara un n�mero de imponible.
* Se hizo esta Fx para que los reportes no "molesten" con los arreglos
PARAMETERS tipo, nro
EXTERNAL ARRAY masc_imp

RETURN TRANSFORM(ALLT(STR(nro,10,0)), '@R ' + masc_imp[ASC(tipo)-64])
