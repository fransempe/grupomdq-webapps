*************************************************
*FUNCTION ftabmae
*************************************************
* Autor  : Elio
* Dise�o : Pachu
* Fecha  : 14-02-95
* 
* Par�metros: m.tipo_imp (C)= tipo de imponible
* 						@tabimp    (C)= tabla del imponible
*							@campimp   (C)= campos claves
*							@descimp   (C)= descripci�n de los tipos de imponibles
* 						msg        (L)= booleano para que muestre o n� mensaje
* 

PARAMETER m.tipo_imp, tabimp, campimp, descimp, msg
EXTERNAL ARRAY TAB_IMP, IDENT_IMP , DESC_TAB

IF SET ('DEBUG') = 'ON'
	=VerNuPar ('FTABMAE', PARAMETERS(), 5 )
	=VerTiPar ('FTABMAE','M.TIPO_IMP'   ,'C')
	=VerTiPar ('FTABMAE','TABIMP'   ,'C')
	=VerTiPar ('FTABMAE','CAMPIMP'   ,'C')
	=VerTiPar ('FTABMAE','DESCIMP'   ,'C')
	=VerTiPar ('FTABMAE','MSG'   ,'L')
ENDIF

PRIVATE retorno, m.nro_elem
retorno = .T.
m.nro_elem = ASC(m.tipo_imp) - 64
IF m.nro_elem < 0 OR m.nro_elem > 64
	retorno = .F.
	IF msg
		= msg('El Tipo de Imponible '+ m.tipo_imp + ' no es v�lido.')
	ENDIF
ELSE
	IF EMPTY(TRIM(tab_imp[m.nro_elem]))
		retorno = .F.
		IF msg
			= msg('El Tipo de Imponible ' + m.tipo_imp + ' no tiene asignada una tabla maestra')
		ENDIF
	ELSE
		IF EMPTY(TRIM(ident_imp[nro_elem]))
			retorno = .F.
			IF msg
				= msg('El Tipo de Imponible ' + m.tipo_imp + ' no tiene especificado el nombre del campo clave.')
			ENDIF
		ELSE
			tabimp = tab_imp[m.nro_elem]
			campimp = ident_imp[m.nro_elem]
			descimp = desc_tab[m.nro_elem]
		ENDIF
	ENDIF
ENDIF

RETURN retorno

