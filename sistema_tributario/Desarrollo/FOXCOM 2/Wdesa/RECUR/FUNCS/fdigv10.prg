*************************************************
* FDigV10
*************************************************
* Autor : Gabriel Zubieta
* Dise�o: Leo
* 
* Fecha : 28/03/95
* 
* Funcionamiento:
* Dado un digito y un ponderador devuelve el d�gito verificador
* correspondiente. 
* 
* 
* Par�metros:
* numero  -C- numero sobre el cual se quiere calcular el digito 
* pond    -C- mascara para ponderar el numero anterior. 
* 
* Modificaciones:
* 
PARAMETERS m.numero, m.pond
PRIVATE m.digver, m.len
m.digver = 0 
m.len = LEN (m.numero)
FOR i = 1 TO m.len
	dig = SUBSTR (m.numero, i, 1)
	IF m.dig > '0' AND m.dig <= '9'
		m.digver = m.digver + ((ASC (m.dig) - 48) * (ASC (SUBSTR (m.pond, i, 1)) - 48))
	ENDIF
ENDFOR
m.digver = m.digver % 10
IF m.digver != 0 
	m.digver = 10 - m.digver	
ENDIF
RETURN m.digver

