*************************************************
*FUNCTION Ir_Final
*************************************************
* Autor : Gabriel Zubieta
* Dise�o: Idem
* 
* Fecha : 13/03/95
* 
* Funcionamiento:
* Cant     - Vble. que marca cantidad de impresiones que se van realizando
* Cantidad - Vble. que marca cantidad a imprimir, fue ingresada en pantalla
*
PARAMETERS alias

IF (m.CANT >= m.cantidad)

	GO BOTTOM IN (alias)

ENDIF
RETURN ''
