*************************************************
* FUNCTION FAudRec
*************************************************
* Autor  : Rodrigo ( Sobre AUDCONTA() )
* Dise�o : ???
* Fecha  : 23/02/95
* 
* Funcionamiento:
* Realiza la auditor�a solicitada, seg�n el
* proceso que se le indique, y tomando la
* informaci�n desde el alias que se especifique.
* 
* Par�metros:
* proceso		-	(C) Proceso que se va a realizar.
*									Si se va a auditar un alta, baja o modi, los
*									procesos a auditar son:
*									ALTA  : Alta de un registro.
*									BAJA  : Baja o anulaci�n de un registro.
*									MODIA : Registro, antes de una modificaci�n.
*									MODID : Registro, despu�s de una modificaci�n.
* NomAlias	-	(C) Es el alias de la tabla desde la que se va a
*									hacer la auditor�a. Obviamente, no puede ser
*									un alias de cualquier tabla. Esto se verifica,
*									y si se detecta que no es un alias de la tabla
*									maestra, se arroja un error.
* 
* 								- Buscar <NEW_DBF> para completar con una tabla nueva
*
* [Obs] - (C) Observaciones
*
* Modificaciones :
*	Gabriel 04/03/95 - 09,30 Ag. Aud. de nro_plano y observaciones AUD_INM
*	Gabriel	04/03/95 - 11,00 Ag. Aud.	de Obs en AUD_COM y AUD_CONT
* Gabriel 06/03/95 - 10,30 Ag. Aud de Nro_empl, nro_titul, nro_empl, m2_esppub
*	Rodrigo 03/05/95 - 14:00 Ag. a la auditoria de Rec_Imp los campos del contribuyente
* Sato		15/11/96 - 14:00 Ag. Aud. de Veh�culos
*	Sato		19/11/96 - 10:00 Ag. Aud. de Cementerio
*	Carlos	22/05/98 - 11:30 Ag. Aud. de Gesti�n Legal (documentos y movimientos)
*
*************************************************
*	
*	SQLMODI-01
*		Usuario							: Tres Arroyos
*		Fecha								: 28/11/2000
*		Quien modific�			: Carlos
*		Detalle problema		: No ten�a Auditor�a de Condiciones Especiales.
*													
*		Programas tocados		: FAudRec.prg
*													
************************************************************************
*	
*	SQLMODI-02
*		Usuario							: Todos
*		Fecha								: 28/11/2000
*		Quien modific�			: Carlos
*		Detalle problema		: Se agreg� auditor�a para los campos nuevos.
*													
************************************************************************
*	
*	SQLMODI_03
*		Usuario							: San Nicol�s/Todos
*		Fecha								: 04/06/2002
*		Quien modific�			: Alejandro
*		Detalle correcci�n	: Se agreg� la auditor�a de UBIC_CEM, OCUPANT
*													y VINC_OCU
*													
************************************************************************
*	
*	SQLMODI_04
*		Usuario							: Todos
*		Fecha								: 29/04/2003
*		Quien modific�			: Alejandro
*		Detalle correcci�n	: Se agreg� la auditor�a de los campos nuevos de
*													VEHICULO.
*													
************************************************************************

PARAMETERS proceso, NomAlias, obs
PRIVATE m.retorno, tabla_mae, tabla_aud, select_bak, recno_bak
m.retorno = .T.

* =VerNuPar ('FAUDREC', PARAMETERS()	, 2 )
=VerTiPar ('FAUDREC','PROCESO'	,'C')
=VerTiPar ('FAUDREC','NOMALIAS'    ,'C')
* =VerTiPar ('FAUDREC','OBS'     ,'C')

IF ! USED (NomAlias)
	= msg_back ('','No se puede realizar auditor�a porque la tabla ' + m.NomAlias + ' No est� abierta. Funcion FAudRec ().')
	m.retorno = .F.
ENDIF

IF Type ('m.obs') <> 'C'
	m.obs = ''
ENDIF

IF m.retorno

	* Las dos pr�ximas l�neas, son para obtener el nombre f�sico de la
	* tabla. Si dbf () retorna 'c:\dbase\recur\dev_ccc.dbf', en
	* m.tabla_mae, queda almacenado 'DEV_CCC'.
	m.tabla_mae = SUBSTR (DBF (NomAlias), RAT ('\', DBF (NomAlias)) + 1, LEN (DBF (NomAlias)))
	m.tabla_mae = SUBSTR (m.tabla_mae, 1, AT ('.DBF', m.tabla_mae) - 1)
	m.proceso = ALLTRIM (UPPER (m.proceso))

	DO CASE
		CASE m.tabla_mae == 'CONCSDEV'
			m.tabla_aud = 'AUD_CD'
		CASE m.tabla_mae == 'CONC_IMP'
			m.tabla_aud = 'AUD_CDI'
		CASE m.tabla_mae == 'COMERCIO'
			m.tabla_aud = 'AUD_COM'
		CASE m.tabla_mae == 'CONTRIB'
			m.tabla_aud = 'AUD_CONT'
		CASE m.tabla_mae == 'INMUEBLE'
			m.tabla_aud = 'AUD_INM'
		CASE m.tabla_mae == 'OPER_CD'
			m.tabla_aud = 'AUD_OP'
		CASE m.tabla_mae == 'REC_IMP'
			m.tabla_aud = 'AUD_REIM'
		CASE m.tabla_mae == 'VINC_IMP'
			m.tabla_aud	= 'AUD_VI'
		CASE m.tabla_mae == 'VEHICULO'
			m.tabla_aud	= 'AUD_VEH'
		CASE m.tabla_mae == 'CEMENT'
			m.tabla_aud = 'AUD_CEM'
		CASE m.tabla_mae == 'LEG_DOC'
			m.tabla_aud = 'AUD_LDOC'
		CASE m.tabla_mae == 'LEG_MOV'
			m.tabla_aud = 'AUD_LMOV'

		CASE m.tabla_mae == 'REC_CC'
			m.tabla_aud = 'AUD_CC'

		* Comienzo SQLMODI-01
		CASE m.tabla_mae == 'COND_ESP'
			m.tabla_aud = 'AUD_COND'
		* Fin SQLMODI-01
		
		*	Comienzo SQLMODI_03
		CASE m.tabla_mae == 'UBIC_CEM'
			m.tabla_aud = 'AUD_UBCE'
		CASE m.tabla_mae == 'OCUPANT'
			m.tabla_aud = 'AUD_OCUP'				
		CASE m.tabla_mae == 'VINC_OCU'
			m.tabla_aud = 'AUD_VOCU'
		*	Fin SQLMODI_03
			
*				<NEW_DBF>
*				CASE m.tabla_mae == 'XXXX'
*					m.tabla_aud	=	'AUD_XXX'
		OTHERWISE
			= msg_back ('','El nombre del alias pasado como par�metro, ' + m.NomAlias + ', no corresponde a un alias de ninguna tabla que acepte auditor�a.')
			m.retorno = .F.
		ENDCASE
ENDIF

IF m.retorno
	IF EOF (NomAlias)
		= msg_back ('','Se trata de hacer auditor�a de una tabla que est� en EOF (). Funci�n AudConta (). Nombre del alias en EOF (), ' + m.NomAlias)
		m.retorno = .F.
	ENDIF
ENDIF

IF m.retorno

	= uset ('recur\' + m.tabla_aud)

	m.select_bak = SELECT ()
	SELECT (m.tabla_aud)
	m.recno_bak = RECNO (m.tabla_aud)

	= append ()

	REPLACE 	&tabla_aud..num_aud		WITH m.num_aud,;
						&tabla_aud..hora_proc	WITH SToT (SECONDS ())

	DO CASE

	CASE m.tabla_aud == 'AUD_CD'

		REPLACE aud_cd.proc_aud			WITH proceso,;
						aud_cd.recurso			WITH &NomAlias..recurso,;
						aud_cd.conc_dev			WITH &NomAlias..conc_dev,;
						aud_cd.desc_cd			WITH &NomAlias..desc_cd,;
						aud_cd.dsc_cd				WITH &NomAlias..dsc_cd,;
						aud_cd.conc_aux			WITH &NomAlias..conc_aux
						
		REPLACE aud_cd.conc_iva			WITH &NomAlias..conc_iva,;
						aud_cd.grav_iva			WITH &NomAlias..grav_iva,;
						aud_cd.grab_ccd			WITH &NomAlias..grab_ccd,;
						aud_cd.tipo_imp			WITH &NomAlias..tipo_imp,;
						aud_cd.vigen_cd			WITH &NomAlias..vigen_cd,;
						aud_cd.form_impr		WITH &NomAlias..form_impr
						
		REPLACE aud_cd.conc_cc			WITH &NomAlias..conc_cc,;
						aud_cd.dsc_val1ci		WITH &NomAlias..dsc_val1ci,;
						aud_cd.dsc_val2ci		WITH &NomAlias..dsc_val2ci

	CASE m.tabla_aud == 'AUD_CDI'
		REPLACE aud_cdi.proc_aud		WITH proceso,;
						aud_cdi.tipo_imp		WITH &NomAlias..tipo_imp,;
						aud_cdi.nro_imp			WITH &NomAlias..nro_imp,;
						aud_cdi.recurso			WITH &NomAlias..recurso,;
						aud_cdi.grupo_conc	WITH &NomAlias..grupo_conc,;
						aud_cdi.conc_dev		WITH &NomAlias..conc_dev
						
		REPLACE aud_cdi.conc_cc			WITH &NomAlias..conc_cc,;
						aud_cdi.val1_cdi		WITH &NomAlias..val1_cdi,;
						aud_cdi.val2_cdi		WITH &NomAlias..val2_cdi,;
						aud_cdi.cuota_inic	WITH &NomAlias..cuota_inic,;
						aud_cdi.anio_inic		WITH &NomAlias..anio_inic,;
						aud_cdi.cuota_fin		WITH &NomAlias..cuota_fin
						
		REPLACE aud_cdi.anio_fin		WITH &NomAlias..anio_fin,;
						aud_cdi.nro_cuotas	WITH &NomAlias..nro_cuotas

	CASE m.tabla_aud == 'AUD_COM'
		REPLACE aud_com.proc_aud		WITH proceso,;
						aud_com.nro_com			WITH &NomAlias..nro_com,;
						aud_com.dig_veri		WITH &NomAlias..dig_veri,;
						aud_com.nro_suc			WITH &NomAlias..nro_suc,;
						aud_com.nomb_com		WITH &NomAlias..nomb_com,;
						aud_com.nomb_fan		WITH &NomAlias..nomb_fan
						
		REPLACE aud_com.cod_post		WITH &NomAlias..cod_post,;
						aud_com.nomb_loc		WITH &NomAlias..nomb_loc,;
						aud_com.cod_calle		WITH &NomAlias..cod_calle,;
						aud_com.nomb_calle	WITH &NomAlias..nomb_calle,;
						aud_com.puerta			WITH &NomAlias..puerta,;
						aud_com.puertabis		WITH &NomAlias..puertabis
						
		REPLACE aud_com.piso				WITH &NomAlias..piso,;
						aud_com.depto				WITH &NomAlias..depto,;
						aud_com.fec_aper		WITH &NomAlias..fec_aper,;
						aud_com.exp_habil		WITH &NomAlias..exp_habil,;
						aud_com.fec_habil		WITH &NomAlias..fec_habil,;
						aud_com.fven_habil	WITH &NomAlias..fven_habil
						
		REPLACE aud_com.res_habil		WITH &NomAlias..res_habil,;
						aud_com.exp_cese		WITH &NomAlias..exp_cese,;
						aud_com.fec_cese		WITH &NomAlias..fec_cese,;
						aud_com.ing_brut		WITH &NomAlias..ing_brut,;
						aud_com.m2_esppub   WITH &NomAlias..m2_esppub,;
						aud_com.nro_titul   WITH &NomAlias..nro_titul

		REPLACE aud_com.nro_empl    WITH &NomAlias..nro_empl,;
						aud_com.mont_fact		WITH &NomAlias..mont_fact,;
						aud_com.mont_gan		WITH &NomAlias..mont_gan,;
						aud_com.mont_gast		WITH &NomAlias..mont_gast,;
						aud_com.mont_sueld	WITH &NomAlias..mont_sueld,;
						aud_com.mont_ddjj		WITH &NomAlias..mont_ddjj,;
						aud_com.int_cant		WITH &NomAlias..int_cant

		REPLACE	aud_com.clave_com		WITH &NomAlias..clave_com,;
						aud_com.auxiliar1 	WITH &NomAlias..auxiliar1,;
						aud_com.gran_cont		WITH &NomAlias..gran_cont,;
						aud_com.deleg				WITH &NomAlias..deleg,;
						aud_com.obs_com     WITH &NomAlias..obs_com,;
						aud_com.gest_com		WITH &NomAlias..gest_com

		REPLACE	aud_com.rubro1  		WITH &NomAlias..rubro1,;
						aud_com.rubro2  		WITH &NomAlias..rubro2,;
						aud_com.rubro3  		WITH &NomAlias..rubro3,;
						aud_com.rubro4  		WITH &NomAlias..rubro4,;
						aud_com.rubro5  		WITH &NomAlias..rubro5,;
						aud_com.rubro6  		WITH &NomAlias..rubro6
						
		REPLACE aud_com.rubro7  		WITH &NomAlias..rubro7,;
						aud_com.rubro8  		WITH &NomAlias..rubro8,;
						aud_com.rubro9  		WITH &NomAlias..rubro9,;
						aud_com.rubro10  		WITH &NomAlias..rubro10,;
						aud_com.SubRub1  		WITH &NomAlias..SubRub1,;
						aud_com.SubRub2  		WITH &NomAlias..SubRub2
						
		REPLACE aud_com.SubRub3  		WITH &NomAlias..SubRub3,;
						aud_com.SubRub4  		WITH &NomAlias..SubRub4,;
						aud_com.SubRub5  		WITH &NomAlias..SubRub5,;
						aud_com.SubRub6  		WITH &NomAlias..SubRub6,;
						aud_com.SubRub7  		WITH &NomAlias..SubRub7,;
						aud_com.SubRub8  		WITH &NomAlias..SubRub8
						
		REPLACE aud_com.SubRub9  		WITH &NomAlias..SubRub9,;
						aud_com.SubRub10  	WITH &NomAlias..SubRub10,;
						aud_com.cod_bajcm		WITH &NomAlias..cod_bajcm,;
						aud_com.fec_baja		WITH &NomAlias..fec_baja
						
		REPLACE aud_com.fec_rubr1 WITH &NomAlias..fec_rubr1
		REPLACE aud_com.fec_rubr2 WITH &NomAlias..fec_rubr2
		REPLACE aud_com.fec_rubr3 WITH &NomAlias..fec_rubr3
		REPLACE aud_com.fec_rubr4 WITH &NomAlias..fec_rubr4
		REPLACE aud_com.fec_rubr5 WITH &NomAlias..fec_rubr5
		REPLACE aud_com.fec_rubr6 WITH &NomAlias..fec_rubr6
		REPLACE aud_com.fec_rubr7 WITH &NomAlias..fec_rubr7
		REPLACE aud_com.fec_rubr8 WITH &NomAlias..fec_rubr8
		REPLACE aud_com.fec_rubr9 WITH &NomAlias..fec_rubr9
		REPLACE aud_com.fec_rubr10 WITH &NomAlias..fec_rubr10
		
		REPLACE aud_com.exp_habil1 WITH &NomAlias..exp_habil1
		REPLACE aud_com.exp_habil2 WITH &NomAlias..exp_habil2
		REPLACE aud_com.exp_habil3 WITH &NomAlias..exp_habil3
		REPLACE aud_com.exp_habil4 WITH &NomAlias..exp_habil4
		REPLACE aud_com.exp_habil5 WITH &NomAlias..exp_habil5
		REPLACE aud_com.exp_habil6 WITH &NomAlias..exp_habil6
		REPLACE aud_com.exp_habil7 WITH &NomAlias..exp_habil7
		REPLACE aud_com.exp_habil8 WITH &NomAlias..exp_habil8
		REPLACE aud_com.exp_habil9 WITH &NomAlias..exp_habil9
		REPLACE aud_com.exp_habi10 WITH &NomAlias..exp_habi10

		REPLACE aud_com.fec_bajr1 WITH &NomAlias..fec_bajr1
		REPLACE aud_com.fec_bajr2 WITH &NomAlias..fec_bajr2
		REPLACE aud_com.fec_bajr3 WITH &NomAlias..fec_bajr3
		REPLACE aud_com.fec_bajr4 WITH &NomAlias..fec_bajr4
		REPLACE aud_com.fec_bajr5 WITH &NomAlias..fec_bajr5
		REPLACE aud_com.fec_bajr6 WITH &NomAlias..fec_bajr6
		REPLACE aud_com.fec_bajr7 WITH &NomAlias..fec_bajr7
		REPLACE aud_com.fec_bajr8 WITH &NomAlias..fec_bajr8
		REPLACE aud_com.fec_bajr9 WITH &NomAlias..fec_bajr9
		REPLACE aud_com.fec_bajr10 WITH &NomAlias..fec_bajr10
		
		REPLACE aud_com.tipo_habil WITH &NomAlias..tipo_habil
				
	CASE m.tabla_aud == 'AUD_CONT'
		REPLACE aud_cont.proc_aud		WITH proceso,;
						aud_cont.cuit				WITH &NomAlias..cuit,;
						aud_cont.dig_veri		WITH &NomAlias..dig_veri,;
						aud_cont.nomb_cont	WITH &NomAlias..nomb_cont,;
						aud_cont.cod_post		WITH &NomAlias..cod_post,;
						aud_cont.nomb_loc		WITH &NomAlias..nomb_loc
						
		REPLACE aud_cont.cod_calle	WITH &NomAlias..cod_calle,;
						aud_cont.nomb_calle	WITH &NomAlias..nomb_calle,;
						aud_cont.puerta			WITH &NomAlias..puerta,;
						aud_cont.puertabis	WITH &NomAlias..puertabis,;
						aud_cont.piso				WITH &NomAlias..piso,;
						aud_cont.depto			WITH &NomAlias..depto
						
		REPLACE aud_cont.sit_iva		WITH &NomAlias..sit_iva,;
						aud_cont.obs_cont   WITH &NomAlias..obs_cont,;
						aud_cont.clave_cont	WITH &NomAlias..clave_cont,;
						aud_cont.auxiliar1	WITH &NomAlias..auxiliar1,;
						aud_cont.cod_bajcn	WITH &NomAlias..cod_bajcn,;
						aud_cont.fec_baja		WITH &NomAlias..fec_baja

		REPLACE	aud_cont.deleg			WITH &NomAlias..deleg


	CASE m.tabla_aud == 'AUD_INM'
		REPLACE aud_inm.proc_aud		WITH proceso,;
						aud_inm.nro_inm			WITH &NomAlias..nro_inm,;
						aud_inm.dig_veri		WITH &NomAlias..dig_veri,;
						aud_inm.circun			WITH &NomAlias..circun,;
						aud_inm.seccion			WITH &NomAlias..seccion,;
						aud_inm.numchacra		WITH &NomAlias..numchacra

		REPLACE aud_inm.letchacra		WITH &NomAlias..letchacra,;
						aud_inm.numquinta		WITH &NomAlias..numquinta,;
						aud_inm.letquinta		WITH &NomAlias..letquinta,;
						aud_inm.numfrac			WITH &NomAlias..numfrac,;
						aud_inm.letfrac			WITH &NomAlias..letfrac,;
						aud_inm.nummazna		WITH &NomAlias..nummazna

		REPLACE	aud_inm.letmazna		WITH &NomAlias..letmazna,;
						aud_inm.numparc			WITH &NomAlias..numparc,;
						aud_inm.letparc			WITH &NomAlias..letparc,;
						aud_inm.subparc			WITH &NomAlias..subparc,;
						aud_inm.codmazna		WITH &NomAlias..codmazna,;
						aud_inm.porc_dom		WITH &NomAlias..porc_dom
						
		REPLACE aud_inm.uni_func		WITH &NomAlias..uni_func,;
						aud_inm.poligono		WITH &NomAlias..poligono,;
						aud_inm.cod_post		WITH &NomAlias..cod_post,;
						aud_inm.nomb_loc		WITH &NomAlias..nomb_loc,;
						aud_inm.cod_calle		WITH &NomAlias..cod_calle,;
						aud_inm.nomb_calle	WITH &NomAlias..nomb_calle
						
		REPLACE aud_inm.puerta			WITH &NomAlias..puerta,;
						aud_inm.puertabis		WITH &NomAlias..puertabis,;
						aud_inm.piso				WITH &NomAlias..piso,;
						aud_inm.depto				WITH &NomAlias..depto,;
						aud_inm.rural_urb		WITH &NomAlias..rural_urb,;
						aud_inm.tipo_inm		WITH &NomAlias..tipo_inm

		REPLACE	aud_inm.uso_inm			WITH &NomAlias..uso_inm,;
						aud_inm.orig_inm		WITH &NomAlias..orig_inm,;
						aud_inm.anio_orig		WITH &NomAlias..anio_orig,;
						aud_inm.obser				WITH &NomAlias..obser,;
						aud_inm.caract			WITH &NomAlias..caract,;
						aud_inm.fech_reg		WITH &NomAlias..fech_reg

		REPLACE	aud_inm.insc_reg		WITH &NomAlias..insc_reg,;
						aud_inm.sup_ter			WITH &NomAlias..sup_ter,;
						aud_inm.sup_scub		WITH &NomAlias..sup_scub,;
						aud_inm.sup_cub			WITH &NomAlias..sup_cub,;
						aud_inm.met_fondo		WITH &NomAlias..met_fondo,;
						aud_inm.met_fte1		WITH &NomAlias..met_fte1

		REPLACE	aud_inm.met_fte2		WITH &NomAlias..met_fte2,;
						aud_inm.met_fte3		WITH &NomAlias..met_fte3,;
						aud_inm.met_fte4		WITH &NomAlias..met_fte4,;
						aud_inm.valter			WITH &NomAlias..valter,;
						aud_inm.valedif			WITH &NomAlias..valedif,;
						aud_inm.valmejo			WITH &NomAlias..valmejo

		REPLACE	aud_inm.valbasi			WITH &NomAlias..valbasi,;
						aud_inm.valfiscal		WITH &NomAlias..valfiscal,;
						aud_inm.pda_prov		WITH &NomAlias..pda_prov,;
						aud_inm.pdo_prov		WITH &NomAlias..pdo_prov,;
						aud_inm.dig_prov		WITH &NomAlias..dig_prov,;
						aud_inm.pdo_orig		WITH &NomAlias..pdo_orig

		REPLACE	aud_inm.pda_orig		WITH &NomAlias..pda_orig,;
						aud_inm.dig_orig		WITH &NomAlias..dig_orig,;
						aud_inm.cod_zona		WITH &NomAlias..cod_zona,;
						aud_inm.cod_zonaed	WITH &NomAlias..cod_zonaed,;
						aud_inm.deleg_inm		WITH &NomAlias..deleg_inm,;
						aud_inm.obs_inm     WITH &NomAlias..obs_inm

		REPLACE	aud_inm.fech_edif   WITH &NomAlias..fech_edif,;
						aud_inm.nro_plano   WITH &NomAlias..nro_plano,;
						aud_inm.clave_inm		WITH &NomAlias..clave_inm,;
						aud_inm.gran_cont		WITH &NomAlias..gran_cont,;
						aud_inm.auxiliar1		WITH &NomAlias..auxiliar1,;
						aud_inm.lagun_perm	WITH &NomAlias..lagun_perm

		REPLACE	aud_inm.fec_baja		WITH &NomAlias..fec_baja,;
						aud_inm.cod_bajin		WITH &NomAlias..cod_bajin

		REPLACE aud_inm.cod_alumb1	WITH &NomAlias..cod_alumb1,;
						aud_inm.cod_barr1		WITH &NomAlias..cod_barr1,;
						aud_inm.cod_resid1	WITH &NomAlias..cod_resid1,;
						aud_inm.cod_pavim1	WITH &NomAlias..cod_pavim1,;
						aud_inm.cod_agua1		WITH &NomAlias..cod_agua1,;
						aud_inm.cod_cloac1	WITH &NomAlias..cod_cloac1
						
		REPLACE aud_inm.cod_desag1	WITH &NomAlias..cod_desag1,;
						aud_inm.cat_calle1	WITH &NomAlias..cat_calle1,;
						aud_inm.cod_gas1		WITH &NomAlias..cod_gas1,;
						aud_inm.cod_esq1		WITH &NomAlias..cod_esq1,;
						aud_inm.cod_alumb2	WITH &NomAlias..cod_alumb2,;
						aud_inm.cod_barr2		WITH &NomAlias..cod_barr2
						
		REPLACE aud_inm.cod_resid2	WITH &NomAlias..cod_resid2,;
						aud_inm.cod_pavim2	WITH &NomAlias..cod_pavim2,;
						aud_inm.cod_agua2		WITH &NomAlias..cod_agua2,;
						aud_inm.cod_cloac2	WITH &NomAlias..cod_cloac2,;
						aud_inm.cod_desag2	WITH &NomAlias..cod_desag2,;
						aud_inm.cat_calle2	WITH &NomAlias..cat_calle2
						
		REPLACE aud_inm.cod_gas2		WITH &NomAlias..cod_gas2,;
						aud_inm.cod_esq2		WITH &NomAlias..cod_esq2,;
						aud_inm.cod_alumb3	WITH &NomAlias..cod_alumb3,;
						aud_inm.cod_barr3		WITH &NomAlias..cod_barr3,;
						aud_inm.cod_resid3	WITH &NomAlias..cod_resid3,;
						aud_inm.cod_pavim3	WITH &NomAlias..cod_pavim3
						
		REPLACE aud_inm.cod_agua3		WITH &NomAlias..cod_agua3,;
						aud_inm.cod_cloac3	WITH &NomAlias..cod_cloac3,;
						aud_inm.cod_desag3	WITH &NomAlias..cod_desag3,;
						aud_inm.cat_calle3	WITH &NomAlias..cat_calle3,;
						aud_inm.cod_gas3		WITH &NomAlias..cod_gas3,;
						aud_inm.cod_esq3		WITH &NomAlias..cod_esq3
						
		REPLACE aud_inm.cod_alumb4	WITH &NomAlias..cod_alumb4,;
						aud_inm.cod_barr4		WITH &NomAlias..cod_barr4,;
						aud_inm.cod_resid4	WITH &NomAlias..cod_resid4,;
						aud_inm.cod_pavim4	WITH &NomAlias..cod_pavim4,;
						aud_inm.cod_agua4		WITH &NomAlias..cod_agua4,;
						aud_inm.cod_cloac4	WITH &NomAlias..cod_cloac4
						
		REPLACE aud_inm.cod_desag4	WITH &NomAlias..cod_desag4,;
						aud_inm.cat_calle4	WITH &NomAlias..cat_calle4,;
						aud_inm.cod_gas4		WITH &NomAlias..cod_gas4,;
						aud_inm.cod_esq4		WITH &NomAlias..cod_esq4,;
						aud_inm.sup_edif1		WITH &NomAlias..sup_edif1,;
						aud_inm.cod_edif1		WITH &NomAlias..cod_edif1

		REPLACE	aud_inm.sup_edif2		WITH &NomAlias..sup_edif2,;
						aud_inm.cod_edif2		WITH &NomAlias..cod_edif2,;
						aud_inm.sup_edif3		WITH &NomAlias..sup_edif3,;
						aud_inm.cod_edif3		WITH &NomAlias..cod_edif3,;
						aud_inm.sup_edif4		WITH &NomAlias..sup_edif4,;
						aud_inm.cod_edif4		WITH &NomAlias..cod_edif4

		REPLACE	aud_inm.sup_edif5		WITH &NomAlias..sup_edif5,;
						aud_inm.cod_edif5		WITH &NomAlias..cod_edif5,;
						aud_inm.sup_edif6		WITH &NomAlias..sup_edif6,;
						aud_inm.cod_edif6		WITH &NomAlias..cod_edif6

		IF Type ('inmueble.met_fte5') <> 'U'
			REPLACE	aud_inm.met_fte5 WITH &NomAlias..met_fte5
			REPLACE	aud_inm.met_fte6 WITH &NomAlias..met_fte6
			REPLACE	aud_inm.met_fte7 WITH &NomAlias..met_fte7
			REPLACE	aud_inm.met_fte8 WITH &NomAlias..met_fte8
			REPLACE	aud_inm.met_fte9 WITH &NomAlias..met_fte9
			REPLACE	aud_inm.met_fte10 WITH &NomAlias..met_fte10
				
			REPLACE	aud_inm.cod_alumb5 WITH &NomAlias..cod_alumb5
			REPLACE	aud_inm.cod_alumb6 WITH &NomAlias..cod_alumb6
			REPLACE	aud_inm.cod_alumb7 WITH &NomAlias..cod_alumb7
			REPLACE	aud_inm.cod_alumb8 WITH &NomAlias..cod_alumb8
			REPLACE	aud_inm.cod_alumb9 WITH &NomAlias..cod_alumb9
			REPLACE	aud_inm.cod_alum10 WITH &NomAlias..cod_alum10
		
			REPLACE	aud_inm.cod_barr5 WITH &NomAlias..cod_barr5
			REPLACE	aud_inm.cod_barr6 WITH &NomAlias..cod_barr6
			REPLACE	aud_inm.cod_barr7 WITH &NomAlias..cod_barr7
			REPLACE	aud_inm.cod_barr8 WITH &NomAlias..cod_barr8
			REPLACE	aud_inm.cod_barr9 WITH &NomAlias..cod_barr9
			REPLACE	aud_inm.cod_barr10 WITH &NomAlias..cod_barr10
		
			REPLACE	aud_inm.cod_resid5 WITH &NomAlias..cod_resid5
			REPLACE	aud_inm.cod_resid6 WITH &NomAlias..cod_resid6
			REPLACE	aud_inm.cod_resid7 WITH &NomAlias..cod_resid7
			REPLACE	aud_inm.cod_resid8 WITH &NomAlias..cod_resid8
			REPLACE	aud_inm.cod_resid9 WITH &NomAlias..cod_resid9
			REPLACE	aud_inm.cod_resi10 WITH &NomAlias..cod_resi10
		
			REPLACE	aud_inm.cod_pavim5 WITH &NomAlias..cod_pavim5
			REPLACE	aud_inm.cod_pavim6 WITH &NomAlias..cod_pavim6
			REPLACE	aud_inm.cod_pavim7 WITH &NomAlias..cod_pavim7
			REPLACE	aud_inm.cod_pavim8 WITH &NomAlias..cod_pavim8
			REPLACE	aud_inm.cod_pavim9 WITH &NomAlias..cod_pavim9
			REPLACE	aud_inm.cod_pavi10 WITH &NomAlias..cod_pavi10
		
			REPLACE	aud_inm.cod_agua5 WITH &NomAlias..cod_agua5
			REPLACE	aud_inm.cod_agua6 WITH &NomAlias..cod_agua6
			REPLACE	aud_inm.cod_agua7 WITH &NomAlias..cod_agua7
			REPLACE	aud_inm.cod_agua8 WITH &NomAlias..cod_agua8
			REPLACE	aud_inm.cod_agua9 WITH &NomAlias..cod_agua9
			REPLACE	aud_inm.cod_agua10 WITH &NomAlias..cod_agua10
			
			REPLACE	aud_inm.cod_cloac5 WITH &NomAlias..cod_cloac5
			REPLACE	aud_inm.cod_cloac6 WITH &NomAlias..cod_cloac6
			REPLACE	aud_inm.cod_cloac7 WITH &NomAlias..cod_cloac7
			REPLACE	aud_inm.cod_cloac8 WITH &NomAlias..cod_cloac8
			REPLACE	aud_inm.cod_cloac9 WITH &NomAlias..cod_cloac9
			REPLACE	aud_inm.cod_cloa10 WITH &NomAlias..cod_cloa10
		
			REPLACE	aud_inm.cod_desag5 WITH &NomAlias..cod_desag5
			REPLACE	aud_inm.cod_desag6 WITH &NomAlias..cod_desag6
			REPLACE	aud_inm.cod_desag7 WITH &NomAlias..cod_desag7
			REPLACE	aud_inm.cod_desag8 WITH &NomAlias..cod_desag8
			REPLACE	aud_inm.cod_desag9 WITH &NomAlias..cod_desag9
			REPLACE	aud_inm.cod_desa10 WITH &NomAlias..cod_desa10
		
			REPLACE	aud_inm.cat_calle5 WITH &NomAlias..cat_calle5
			REPLACE	aud_inm.cat_calle6 WITH &NomAlias..cat_calle6
			REPLACE	aud_inm.cat_calle7 WITH &NomAlias..cat_calle7
			REPLACE	aud_inm.cat_calle8 WITH &NomAlias..cat_calle8
			REPLACE	aud_inm.cat_calle9 WITH &NomAlias..cat_calle9
			REPLACE	aud_inm.cat_call10 WITH &NomAlias..cat_call10
		
			REPLACE	aud_inm.cod_gas5 WITH &NomAlias..cod_gas5
			REPLACE	aud_inm.cod_gas6 WITH &NomAlias..cod_gas6
			REPLACE	aud_inm.cod_gas7 WITH &NomAlias..cod_gas7
			REPLACE	aud_inm.cod_gas8 WITH &NomAlias..cod_gas8
			REPLACE	aud_inm.cod_gas9 WITH &NomAlias..cod_gas9
			REPLACE	aud_inm.cod_gas10 WITH &NomAlias..cod_gas10
		
			REPLACE	aud_inm.cod_esq5 WITH &NomAlias..cod_esq5
			REPLACE	aud_inm.cod_esq6 WITH &NomAlias..cod_esq6
			REPLACE	aud_inm.cod_esq7 WITH &NomAlias..cod_esq7
			REPLACE	aud_inm.cod_esq8 WITH &NomAlias..cod_esq8
			REPLACE	aud_inm.cod_esq9 WITH &NomAlias..cod_esq9
			REPLACE	aud_inm.cod_esq10 WITH &NomAlias..cod_esq10
		
			REPLACE	aud_inm.puerta2 WITH &NomAlias..puerta2
			REPLACE	aud_inm.puerta3 WITH &NomAlias..puerta3
			REPLACE	aud_inm.puerta4 WITH &NomAlias..puerta4
			REPLACE	aud_inm.puerta5 WITH &NomAlias..puerta5
			REPLACE	aud_inm.puerta6 WITH &NomAlias..puerta6
			REPLACE	aud_inm.puerta7 WITH &NomAlias..puerta7
			REPLACE	aud_inm.puerta8 WITH &NomAlias..puerta8
			REPLACE	aud_inm.puerta9 WITH &NomAlias..puerta9
			REPLACE	aud_inm.puerta10 WITH &NomAlias..puerta10
		
			REPLACE	aud_inm.cod_calle2 WITH &NomAlias..cod_calle2
			REPLACE	aud_inm.cod_calle3 WITH &NomAlias..cod_calle3
			REPLACE	aud_inm.cod_calle4 WITH &NomAlias..cod_calle4
			REPLACE	aud_inm.cod_calle5 WITH &NomAlias..cod_calle5
			REPLACE	aud_inm.cod_calle6 WITH &NomAlias..cod_calle6
			REPLACE	aud_inm.cod_calle7 WITH &NomAlias..cod_calle7
			REPLACE	aud_inm.cod_calle8 WITH &NomAlias..cod_calle8
			REPLACE	aud_inm.cod_calle9 WITH &NomAlias..cod_calle9
			REPLACE	aud_inm.cod_call10 WITH &NomAlias..cod_call10
		
			REPLACE	aud_inm.vf_tipo WITH &NomAlias..vf_tipo
			REPLACE	aud_inm.vf_estado WITH &NomAlias..vf_estado
		ENDIF


		* Comienzo SQLMODI-02

		IF Type ('inmueble.val_anter') <> 'U'
			REPLACE aud_inm.val_anter		WITH &NomAlias..val_anter ;
							aud_inm.valter2			WITH &NomAlias..valter2 ;
							aud_inm.valbasi2		WITH &NomAlias..valbasi2 ;
							aud_inm.valmejo2		WITH &NomAlias..valmejo2 ;
							aud_inm.val_anter2	WITH &NomAlias..val_anter2 ;
							aud_inm.valfiscal2	WITH &NomAlias..valfiscal2 ;
							aud_inm.valedif2		WITH &NomAlias..valedif2 ;
							aud_inm.val_sup1		WITH &NomAlias..val_sup1 ;
							aud_inm.val_sup2		WITH &NomAlias..val_sup2 ;
							aud_inm.val_sup3		WITH &NomAlias..val_sup3 ;
							aud_inm.val_sup4		WITH &NomAlias..val_sup4 ;
							aud_inm.val_sup5		WITH &NomAlias..val_sup5 ;
							aud_inm.val_sup6		WITH &NomAlias..val_sup6 ;
							aud_inm.fec_sup1		WITH &NomAlias..fec_sup1 ;
							aud_inm.fec_sup2		WITH &NomAlias..fec_sup2 ;
							aud_inm.fec_sup3		WITH &NomAlias..fec_sup3 ;
							aud_inm.fec_sup4		WITH &NomAlias..fec_sup4 ;
							aud_inm.fec_sup5		WITH &NomAlias..fec_sup5 ;
							aud_inm.fec_sup6		WITH &NomAlias..fec_sup6 ;
							aud_inm.memo_imp		WITH &NomAlias..memo_imp ;
							aud_inm.cod_aux			WITH &NomAlias..cod_aux
		ENDIF

		* Fin SQLMODI-02


	CASE m.tabla_aud == 'AUD_OP'
		REPLACE aud_op.proc_aud			WITH proceso,;
						aud_op.recurso			WITH &NomAlias..recurso,;
						aud_op.conc_dev			WITH &NomAlias..conc_dev,;
						aud_op.operacion		WITH &NomAlias..operacion,;
						aud_op.desc_oper		WITH &NomAlias..desc_oper,;
						aud_op.tipo_dest		WITH &NomAlias..tipo_dest
						
		REPLACE aud_op.val_dest			WITH &NomAlias..val_dest,;
						aud_op.form_op			WITH &NomAlias..form_op,;
						aud_op.form_subs		WITH &NomAlias..form_subs

	CASE m.tabla_aud == 'AUD_REIM'
		REPLACE aud_reim.proc_aud		WITH proceso,;
						aud_reim.tipo_imp		WITH &NomAlias..tipo_imp,;
						aud_reim.nro_imp		WITH &NomAlias..nro_imp,;
						aud_reim.recurso		WITH &NomAlias..recurso,;
						aud_reim.remesa			WITH &NomAlias..remesa,;
						aud_reim.reparto		WITH &NomAlias..reparto
						
		REPLACE aud_reim.cod_post		WITH &NomAlias..cod_post,;
						aud_reim.nomb_loc		WITH &NomAlias..nomb_loc,;
						aud_reim.cod_calle	WITH &NomAlias..cod_calle,;
						aud_reim.nomb_calle	WITH &NomAlias..nomb_calle,;
						aud_reim.puerta			WITH &NomAlias..puerta,;
						aud_reim.puertabis	WITH &NomAlias..puertabis
						
		REPLACE aud_reim.piso				WITH &NomAlias..piso,;
						aud_reim.depto			WITH &NomAlias..depto,;
						aud_reim.cod_debaut	WITH &NomAlias..cod_debaut,;
						aud_reim.cta_debaut	WITH &NomAlias..cta_debaut,;
						aud_reim.fec_baja		WITH &NomAlias..fec_baja,;
						aud_reim.cod_bajri	WITH &NomAlias..cod_bajri

		REPLACE aud_reim.dom_envio	WITH &NomAlias..dom_envio

		IF Type ('&NomAlias..cuit') <> 'U'	
			REPLACE aud_reim.cuit WITH &NomAlias..cuit
			REPLACE aud_reim.dig_verc WITH &NomAlias..dig_verc
		ENDIF	

	CASE m.tabla_aud == 'AUD_VI'
		REPLACE aud_vi.proc_aud			WITH proceso,;
						aud_vi.tipo_imp			WITH &NomAlias..tipo_imp,;
						aud_vi.nro_imp			WITH &NomAlias..nro_imp,;
						aud_vi.tipo_impv		WITH &NomAlias..tipo_impv,;
						aud_vi.nro_impv			WITH &NomAlias..nro_impv,;
						aud_vi.tipo_vinc		WITH &NomAlias..tipo_vinc
						
		REPLACE aud_vi.porc_vinc		WITH &NomAlias..porc_vinc,;
						aud_vi.fec_altv			WITH &NomAlias..fec_altv,;
						aud_vi.fec_baja			WITH &NomAlias..fec_baja,;
						aud_vi.cod_bajv			WITH &NomAlias..cod_bajv,;
						aud_vi.obs_vinc			WITH &NomAlias..obs_vinc


	CASE m.tabla_aud == 'AUD_VEH'
		REPLACE aud_veh.proc_aud		WITH proceso,;
						aud_veh.nro_vehi		WITH &NomAlias..nro_vehi,;
						aud_veh.dig_veri		WITH &NomAlias..dig_veri
						
		REPLACE aud_veh.tipo_vehi		WITH &NomAlias..tipo_vehi,;
						aud_veh.marca				WITH &NomAlias..marca,;
						aud_veh.modelo			WITH &NomAlias..modelo,;
						aud_veh.anio_vehi		WITH &NomAlias..anio_vehi,;
						aud_veh.dominio			WITH &NomAlias..dominio,;
						aud_veh.motor				WITH &NomAlias..motor
						
		REPLACE	aud_veh.deleg				WITH &NomAlias..deleg,;
						aud_veh.chasis			WITH &NomAlias..chasis,;
						aud_veh.cilind			WITH &NomAlias..cilind,;
						aud_veh.obs					WITH &NomAlias..obs
												
		REPLACE aud_veh.cod_bajveh	WITH &NomAlias..cod_bajveh,;
						aud_veh.fec_baja		WITH &NomAlias..fec_baja

		IF Type ('aud_veh.cod_marca') = 'C'
			REPLACE	aud_veh.cod_marca		WITH &NomAlias..cod_marca ;
							aud_veh.cod_marcam	WITH &NomAlias..cod_marcam ;
							aud_veh.fec_alta		WITH &NomAlias..fec_alta ;
							aud_veh.certif_fab	WITH &NomAlias..certif_fab ;
							aud_veh.cod_aux			WITH &NomAlias..cod_aux
		ENDIF
		
		*	Comienzo SQLMODI_04
		IF Type ('aud_veh.cod_modelo') = 'C'
			REPLACE	aud_veh.cod_marcac	WITH &NomAlias..cod_marcac ;
							aud_veh.cod_modelo	WITH &NomAlias..cod_modelo ;
							aud_veh.domi_ante		WITH &NomAlias..domi_ante ;
							aud_veh.fecha_ddjj	WITH &NomAlias..fecha_ddjj ;
							aud_veh.uso_vehi		WITH &NomAlias..uso_vehi, ;
							aud_veh.peso				WITH &NomAlias..peso, ;
							aud_veh.carga				WITH &NomAlias..carga, ;
							aud_veh.cod_altveh	WITH &NomAlias..cod_altveh, ;
							aud_veh.cod_nacveh	WITH &NomAlias..cod_nacveh, ;
							aud_veh.cod_catveh	WITH &NomAlias..cod_catveh, ;
							aud_veh.seriemotor	WITH &NomAlias..seriemotor, ;
							aud_veh.tipo_comb		WITH &NomAlias..tipo_comb, ;
							aud_veh.valuacion		WITH &NomAlias..valuacion
		ENDIF
		*	Fin SQLMODI_04

	CASE m.tabla_aud == 'AUD_CEM'
		REPLACE aud_cem.proc_aud		WITH proceso,;
						aud_cem.nro_cta			WITH &NomAlias..nro_cta,;
						aud_cem.dig_veri		WITH &NomAlias..dig_veri,;
						aud_cem.nombre	    WITH &NomAlias..nombre,;
						aud_cem.tipo_doc		WITH &NomAlias..tipo_doc,;
						aud_cem.nro_doc		  WITH &NomAlias..nro_doc
			
		REPLACE	aud_cem.tipo_ubic		WITH &NomAlias..tipo_ubic,;
						aud_cem.sector  		WITH &NomAlias..sector,;
						aud_cem.manzana  		WITH &NomAlias..manzana,;
						aud_cem.lote				WITH &NomAlias..lote,;
						aud_cem.tablon			WITH &NomAlias..tablon,;
						aud_cem.nro_ubic		WITH &NomAlias..nro_ubic
																		
		REPLACE aud_cem.deleg     	WITH &NomAlias..deleg,;
						aud_cem.grupo_comp	WITH &NomAlias..grupo_comp,;
						aud_cem.nro_comp		WITH &NomAlias..nro_comp,;
						aud_cem.ini_arre		WITH &NomAlias..ini_arre,;
						aud_cem.fin_arre		WITH &NomAlias..fin_arre,;
						aud_cem.per_cobr		WITH &NomAlias..per_cobr
						
		REPLACE aud_cem.fec_udev		WITH &NomAlias..fec_udev,;
						aud_cem.fec_pdev		WITH &NomAlias..fec_pdev,;
						aud_cem.nro_adef		WITH &NomAlias..nro_adef,;
						aud_cem.sexo				WITH &NomAlias..sexo
						
		REPLACE	aud_cem.fec_nac 		WITH &NomAlias..fec_nac,;
						aud_cem.fec_ingr 		WITH &NomAlias..fec_ingr,;
						aud_cem.fec_defu 		WITH &NomAlias..fec_defu,;
						aud_cem.hor_defu 		WITH &NomAlias..hor_defu,;
						aud_cem.lug_defu 		WITH &NomAlias..lug_defu,;
						aud_cem.ocu_ppal 		WITH &NomAlias..ocu_ppal

		REPLACE	aud_cem.fec_inhum		WITH &NomAlias..fec_inhum,;
						aud_cem.trasl_orig	WITH &NomAlias..trasl_orig,;
						aud_cem.nacionalid  WITH &NomAlias..nacionalid

		REPLACE aud_cem.est_civil		WITH &NomAlias..est_civil,;
						aud_cem.profesion   WITH &NomAlias..profesion,;
						aud_cem.domi_ocup		WITH &NomAlias..domi_ocup,;
						aud_cem.causa_defu	WITH &NomAlias..causa_defu,;
						aud_cem.facultativ  WITH &NomAlias..facultativ
																		
		REPLACE	aud_cem.fec_baja		WITH &NomAlias..fec_baja,;
						aud_cem.cod_bajcem	WITH &NomAlias..cod_bajcem

	CASE m.tabla_aud == 'AUD_LDOC'
		REPLACE aud_ldoc.proc_aud		WITH proceso,;
						aud_ldoc.leg_tipdoc	WITH &NomAlias..leg_tipdoc,;
						aud_ldoc.leg_nrodoc	WITH &NomAlias..leg_nrodoc,;
						aud_ldoc.cuit				WITH &NomAlias..cuit,;
						aud_ldoc.dig_verc		WITH &NomAlias..dig_verc,;
						aud_ldoc.nomb_cont	WITH &NomAlias..nomb_cont,;
						aud_ldoc.cod_post		WITH &NomAlias..cod_post,;
						aud_ldoc.nomb_loc		WITH &NomAlias..nomb_loc,;
						aud_ldoc.cod_calle	WITH &NomAlias..cod_calle,;
						aud_ldoc.nomb_calle	WITH &NomAlias..nomb_calle,;
						aud_ldoc.puerta			WITH &NomAlias..puerta,;
						aud_ldoc.puertabis	WITH &NomAlias..puertabis

		REPLACE aud_ldoc.piso				WITH &NomAlias..piso,;
						aud_ldoc.depto			WITH &NomAlias..depto,;
						aud_ldoc.leg_inidoc	WITH &NomAlias..leg_inidoc,;
						aud_ldoc.leg_comple	WITH &NomAlias..leg_comple,;
						aud_ldoc.leg_grupo	WITH &NomAlias..leg_grupo,;
						aud_ldoc.leg_obsdoc	WITH &NomAlias..leg_obsdoc,;
						aud_ldoc.leg_caratu	WITH &NomAlias..leg_caratu,;
						aud_ldoc.leg_juzga	WITH &NomAlias..leg_juzga,;
						aud_ldoc.leg_secre	WITH &NomAlias..leg_secre,;
						aud_ldoc.leg_profde	WITH &NomAlias..leg_profde,;
						aud_ldoc.leg_estjud	WITH &NomAlias..leg_estjud,;
						aud_ldoc.leg_profco	WITH &NomAlias..leg_profco,;
						aud_ldoc.leg_estjuc	WITH &NomAlias..leg_estjuc,;
						aud_ldoc.leg_juez		WITH &NomAlias..leg_juez

	CASE m.tabla_aud == 'AUD_LMOV'
		REPLACE aud_lmov.proc_aud		WITH proceso,;
						aud_lmov.leg_tipdoc	WITH &NomAlias..leg_tipdoc,;
						aud_lmov.leg_nrodoc	WITH &NomAlias..leg_nrodoc,;
						aud_lmov.leg_nromov	WITH &NomAlias..leg_nromov,;
						aud_lmov.leg_fecmov	WITH &NomAlias..leg_fecmov,;
						aud_lmov.leg_sit		WITH &NomAlias..leg_sit,;
						aud_lmov.leg_estdoc	WITH &NomAlias..leg_estdoc,;
						aud_lmov.leg_apli		WITH &NomAlias..leg_apli

		REPLACE aud_lmov.leg_tipmov	WITH &NomAlias..leg_tipmov,;
						aud_lmov.leg_sitmov	WITH &NomAlias..leg_sitmov,;
						aud_lmov.leg_avispr	WITH &NomAlias..leg_avispr,;
						aud_lmov.leg_avispo	WITH &NomAlias..leg_avispo,;
						aud_lmov.leg_fecreg	WITH &NomAlias..leg_fecreg,;
						aud_lmov.leg_obsmov	WITH &NomAlias..leg_obsmov,;
						aud_lmov.leg_fecbaj	WITH &NomAlias..leg_fecbaj,;
						aud_lmov.leg_ofjust	WITH &NomAlias..leg_ofjust,;
						aud_lmov.leg_relev	WITH &NomAlias..leg_relev

	CASE m.tabla_aud == 'AUD_CC'
		REPLACE aud_cc.proc_aud		WITH proceso
		REPLACE aud_cc.obs			WITH m.obs
		REPLACE aud_cc.recurso		WITH &NomAlias..recurso
		REPLACE aud_cc.tipo_imp		WITH &NomAlias..tipo_imp
		REPLACE aud_cc.nro_imp		WITH &NomAlias..nro_imp
		REPLACE aud_cc.anio			WITH &NomAlias..anio
		REPLACE aud_cc.cuota		WITH &NomAlias..cuota
		REPLACE aud_cc.nro_mov		WITH &NomAlias..nro_mov
		REPLACE aud_cc.fecemi_mov	WITH &NomAlias..fecemi_mov
		REPLACE aud_cc.fecven_mov	WITH &NomAlias..fecven_mov
		REPLACE aud_cc.imp_mov		WITH &NomAlias..imp_mov
		REPLACE aud_cc.conc_cc		WITH &NomAlias..conc_cc
		REPLACE aud_cc.desc_mov		WITH &NomAlias..desc_mov
		REPLACE aud_cc.est_mov		WITH &NomAlias..est_mov
		REPLACE aud_cc.orig_mov		WITH &NomAlias..orig_mov
		REPLACE aud_cc.id_orig		WITH &NomAlias..id_orig
		REPLACE aud_cc.juicio		WITH &NomAlias..juicio
		REPLACE aud_cc.gest_leg		WITH &NomAlias..gest_leg
		REPLACE aud_cc.intimada		WITH &NomAlias..intimada
		REPLACE aud_cc.reconocida	WITH &NomAlias..reconocida
		REPLACE aud_cc.grupo_comp	WITH &NomAlias..grupo_comp
		REPLACE aud_cc.nro_comp		WITH &NomAlias..nro_comp
		REPLACE aud_cc.nro_plan		WITH &NomAlias..nro_plan


	* Comienzo SQLMODI-01
	CASE m.tabla_aud == 'AUD_COND'
		REPLACE proc_aud		WITH proceso ;
						tipo_imp		WITH &NomAlias..tipo_imp ;
						nro_imp			WITH &NomAlias..nro_imp ;
						nro_cond		WITH &NomAlias..nro_cond ;
						recurso			WITH &NomAlias..recurso ;
						anio_inic		WITH &NomAlias..anio_inic ;
						cuota_inic	WITH &NomAlias..cuota_inic ;
						anio_fin		WITH &NomAlias..anio_fin ;
						cuota_fin		WITH &NomAlias..cuota_fin ;
						fec_alta		WITH &NomAlias..fec_alta ;
						juicio			WITH &NomAlias..juicio ;
						gest_leg		WITH &NomAlias..gest_leg ;
						intimada		WITH &NomAlias..intimada ;
						reconocida	WITH &NomAlias..reconocida ;
						exp_cond		WITH &NomAlias..exp_cond ;
						detalle1		WITH &NomAlias..detalle1 ;
						detalle2		WITH &NomAlias..detalle2
	* Fin SQLMODI-01

		* SQLMODI_06
		REPLACE tipo_canc		WITH &NomAlias..tipo_canc

	*	Comienzo SQLMODI_03
	CASE m.tabla_aud == 'AUD_UBCE'
		REPLACE proc_aud		WITH proceso ;
						nro_cta			WITH &NomAlias..nro_cta ;
						dig_veri		WITH &NomAlias..dig_veri ;
						tipo_ubic		WITH &NomAlias..tipo_ubic ;
						nom_cem1		WITH &NomAlias..nom_cem1 ;
						nom_cem2		WITH &NomAlias..nom_cem2 ;
						nom_cem3		WITH &NomAlias..nom_cem3 ;
						nom_cem4		WITH &NomAlias..nom_cem4 ;
						nom_cem5		WITH &NomAlias..nom_cem5 ;
						nom_cem6		WITH &NomAlias..nom_cem6 ;
						nom_cem7		WITH &NomAlias..nom_cem7 ;
						nom_cem8		WITH &NomAlias..nom_cem8 ;
						nom_cem9		WITH &NomAlias..nom_cem9 ;
						des_catcem	WITH &NomAlias..des_catcem ;
						sup_ubic		WITH &NomAlias..sup_ubic ;
						deleg				WITH &NomAlias..deleg ;
						ini_arre		WITH &NomAlias..ini_arre ;
						fin_arre		WITH &NomAlias..fin_arre ;
						per_cobr		WITH &NomAlias..per_cobr ;
						obs_catcem	WITH &NomAlias..obs_catcem ;
						cod_bajubi	WITH &NomAlias..cod_bajubi ;
						fec_baja		WITH &NomAlias..fec_baja 
						
	CASE m.tabla_aud == 'AUD_OCUP'
		REPLACE proc_aud		WITH proceso ;
						tipo_doc		WITH &NomAlias..tipo_doc ;
						nro_doc			WITH &NomAlias..nro_doc ;
						nombre			WITH &NomAlias..nombre ;
						nro_adef		WITH &NomAlias..nro_adef ;
						sexo				WITH &NomAlias..sexo ;
						fec_nac			WITH &NomAlias..fec_nac ;
						fec_ingr		WITH &NomAlias..fec_ingr ;
						fec_defu		WITH &NomAlias..fec_defu ;
						hor_defu		WITH &NomAlias..hor_defu ;
						lug_defu		WITH &NomAlias..lug_defu ;
						fec_inhum		WITH &NomAlias..fec_inhum ;
						trasl_orig	WITH &NomAlias..trasl_orig ;
						nacionalid	WITH &NomAlias..nacionalid ;
						est_civil		WITH &NomAlias..est_civil ;
						profesion		WITH &NomAlias..profesion ;
						domi_ocup		WITH &NomAlias..domi_ocup ;
						causa_defu	WITH &NomAlias..causa_defu ;
						facultativ	WITH &NomAlias..facultativ ;
						doc_ingr		WITH &NomAlias..doc_ingr ;
						obs_cem			WITH &NomAlias..obs_cem ;
						cod_bajcem	WITH &NomAlias..cod_bajcem ;
						fec_baja		WITH &NomAlias..fec_baja 						
						
	CASE m.tabla_aud == 'AUD_VOCU'
		REPLACE proc_aud		WITH proceso ;
						nro_cta			WITH &NomAlias..nro_cta ;
						tipo_doc		WITH &NomAlias..tipo_doc ;
						nro_doc			WITH &NomAlias..nro_doc ;
						tip_vicem		WITH &NomAlias..tip_vicem ;
						fec_altv		WITH &NomAlias..fec_altv ;
						cod_altv		WITH &NomAlias..cod_altv ;
						fec_bajv		WITH &NomAlias..fec_bajv ;
						cod_bajv		WITH &NomAlias..cod_bajv ;
						obs_catcem	WITH &NomAlias..obs_catcem						
	*	Fin SQLMODI_03			

*	<NEW_DBF>
*	CASE m.tabla_mae == 'XXX'
*		REPLACE aud_xxx.cpo1 WITH &NomAlias..cpo1
*		REPLACE aud_xxx.cpo2 WITH &NomAlias..cpo2
*		. . . . . . . . . . . . . . . . . . . . . 
*		REPLACE aud_xxx.cpon WITH &NomAlias..cpon

	ENDCASE

	= ir_reg (m.recno_bak, m.tabla_aud)
	SELECT (m.select_bak)

ENDIF

RETURN m.retorno
