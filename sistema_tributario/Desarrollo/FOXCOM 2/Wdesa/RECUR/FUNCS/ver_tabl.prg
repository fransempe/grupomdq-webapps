*************************************************
* FUNCTION Ver_Tabl
*************************************************
* Autor : Gabriel Zubieta
* Dise�o: Gabriel Zubieta
* 
* Fecha : 12/04/95
* 
* Funcionamiento:
* Dado un path verifica que ah� esten las tablas dev_comp, dec_ccc y dev_cd
* 
* Par�metros:
* direct -C- Path Completo 
* 
* Modificaciones:
* 
PARAMETERS m.direct
PRIVATE retorno
retorno = .T.
m.direct = ALLT( m.direct )
IF SUBSTR( m.direct , LEN(m.direct) , 1 ) != '\'
	m.direct = m.direct + '\'
ENDIF
IF !FILE(m.direct + 'dev_comp.dbf')
	=msg('No existe la tabla de comprobantes de devengamientos '+ m.direct + 'DEV_COMP' + ' en el directorio correspondiente a la/s Tanda/s.')
	retorno = .F.
ENDIF
IF !FILE(m.direct + 'dev_ccc.dbf')
	=msg('No existe la tabla de conceptos de cuenta corriente devengados '+ m.direct + 'DEV_CCC' + ' en el directorio correspondiente a la/s Tanda/s.')
	retorno = .F.	
ENDIF
IF !FILE(m.direct + 'dev_cd.dbf')
	=msg('No existe la tabla de conceptos de devengamientos '+ m.direct + 'DEV_CD' + ' en el directorio correspondiente a la/s Tanda/s.')
	retorno = .F.
ENDIF
RETURN retorno
