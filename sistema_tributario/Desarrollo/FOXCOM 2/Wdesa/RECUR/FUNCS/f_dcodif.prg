*************************************************
FUNCTION F_DCODIF
*************************************************
* Funcionamiento:
* Dado un tipo de c�digo y un c�digo, extrae la desc. de la codificadora.
*		Si no halla el c�digo, retorna ''.
*
* Par�metros:
* 		M.TCOD (C 6): Tipo de C�digo  
* 		M.CODI (C 6): C�digo  
* 		M.CORTA (L) : Flag indicador si se quiere desc. corta. 
*
*	Ejemplos:
*				? F_DCODIF('TIPINM', 'EDIRU', .T.)  &&toma la desc. corta del
*																		tipo de inmueble Edificaci�n Rural
*				? F_DCODIF('GRUPOS', '17', .T.)  &&toma la desc. corta del grupo 17
*
*	Modificaciones:
*		- Ahora sirve para cualq. sistema (por default es Recursos). 19/nov/97
*************************************************
PARAM	m.tcod, m.codi, m.corta
PRIVATE retorno
retorno = ''

IF PARAM()=3  AND  TYPE('m.tcod')='C'  AND  ;
		TYPE('m.codi')='C'  AND  TYPE('m.corta')='L'
	
	IF ( TYPE('m.sistema')='C' )
		* El sistema SI existe
		DO CASE
		CASE m.sistema = 'ASUNT'
			retorno = DE_FIC( 'asunt' )
		CASE m.sistema = 'BIENES'
			retorno = DE_FICS( 'bienes' )
		CASE m.sistema = 'CONTA'
			retorno = DE_FICS( 'conta' )
		CASE m.sistema = 'RECUR'
			retorno = DE_FICS( 'recur' )
		CASE m.sistema = 'RECHUM'
			retorno = DE_FICA( 'rechum' )
		OTHERWISE
			* Si el sistema NO existe o NO est� entre los reconocidos, por default 
			* 	el considerado ser� Recursos.
			retorno = DE_FICS( 'recur' )
		ENDCASE
	ELSE
		* Si el sistema NO existe o NO est� entre los reconocidos, por default 
		* 	el considerado ser� Recursos.
		retorno = DE_FICS( 'recur' )
	ENDIF
		
	
ELSE
	=MSG('Error. Par�metros mal pasados. '+ ;
				'F_DCODIF  ( <tipo_c�digo_C1>, <c�digo_C2>, <desc_corta_L1> ).' )
ENDIF
RETURN retorno


*************************************************
FUNCTION DE_FIC
*************************************************
PARAM m.f_sist
PRIVATE retorno
PRIVATE m.curr_rec, m.en_uso, m.existe
retorno	= ''
m.en_uso= .T.

IF !USED('codific')
	m.en_uso = .F.
	=USET( m.f_sist + '\codific' )
ELSE
	m.curr_rec = RECNO('codific')
ENDIF
	
m.existe = SEEK( PADR(m.tcod,6)+ PADR(m.codi,8), 'codific')
IF m.existe
	IF m.corta
		m.retorno = codific.descab_cod
	ELSE
		m.retorno = codific.desc_cod
	ENDIF
ELSE
	m.retorno = ''
ENDIF

IF !m.en_uso
	USE IN codific
ELSE
	=IR_REG(m.curr_rec, 'codific')
ENDIF

RETURN retorno


*************************************************
FUNCTION DE_FICS
*************************************************
PARAM m.f_sist
PRIVATE retorno
PRIVATE m.curr_rec, m.en_uso, m.existe
retorno	= ''
m.en_uso= .T.

IF !USED('codifics')
	m.en_uso = .F.
	=USET( m.f_sist + '\codifics' )
ELSE
	m.curr_rec = RECNO('codifics')
ENDIF
	
m.existe = SEEK( PADR(m.tcod,6)+ PADR(m.codi,6), 'codifics')
IF m.existe
	IF m.corta
		m.retorno = codifics.dsc_cod
	ELSE
		m.retorno = codifics.desc_cod
	ENDIF
ELSE
	m.retorno = ''
ENDIF

IF !m.en_uso
	USE IN codifics
ELSE
	=IR_REG(m.curr_rec, 'codifics')
ENDIF

RETURN retorno


*************************************************
FUNCTION DE_FICA
*************************************************
PARAM m.f_sist
PRIVATE retorno
PRIVATE m.curr_rec, m.en_uso, m.existe
retorno	= ''
m.en_uso= .T.

IF !USED('codifica')
	m.en_uso = .F.
	=USET( m.f_sist + '\codifica' )
ELSE
	m.curr_rec = RECNO('codifica')
ENDIF
	
m.existe = SEEK( PADR(m.tcod,15)+ PADR(m.codi,8), 'codifica')
IF m.existe
	IF m.corta
		m.retorno = codifica.des_abrevi
	ELSE
		m.retorno = codifica.des_codigo
	ENDIF
ELSE
	m.retorno = ''
ENDIF

IF !m.en_uso
	USE IN codifica
ELSE
	=IR_REG(m.curr_rec, 'codifica')
ENDIF

RETURN retorno
