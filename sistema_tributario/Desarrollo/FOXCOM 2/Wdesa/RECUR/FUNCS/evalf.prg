*:*****************************************************************************
*:
*: Procedure file: G:\-DESA\RECUR\DEVEN\EVALF.PRG
*:
*:         System: Proceso de Devengamiento
*:         Author: Rodrigo
*:      Copyright (c) 1995, SICO
*:  Last modified: 17/04/95     10:40
*:
*:  Procs & Fncts: EVALF()
*:
*:          Calls: EMPTY()            (function  in ?)
*:               : ALLTRIM()          (function  in ?)
*:               : MESSAGE()          (function  in ?)
*:               : EVAL()             (function  in ?)
*:               : TYPE()             (function  in ?)
*:               : FMSGDEV()          (function  in ?)
*:
*:      Documented 15/09/95 at 13:20               FoxDoc  version 2.10f
*:*****************************************************************************
*************************************************
* FUNCTION EVALF
*************************************************
* Autor  : Rodrigo
* Dise�o : Pachu
* Fecha  : 21/03/95
*
* Funcionamiento:
* Dada una f�rmula y un tipo debe evaluarla y validarla contra el tipo
* devolviendo el valor evaluado
*
*
* Par�metros:
* formula -C- Fla para evaluar.
* Tipofla -C- Tipo que debe ser. Caracter, Numerica, Logica, etc.
*
parameters m.formula, m.tipofla
private m.nerror, m.exp

m.nerror 	= " "
if !empty(alltrim(m.formula) )
   on error m.nerror = message()
   m.exp 		= eval( alltrim(m.formula) )
   on error &onerror
   if type('m.exp') # m.tipofla
      m.txt_msg	=	'El tipo de dato de la expresi�n ' + m.formula + ' es inv�lido '
      =fmsgdev()
      do case
      case m.tipofla = 'C'
         m.exp	=	''
      case m.tipofla = 'N'
         m.exp	=	0
      case m.tipofla = 'L'
         m.exp	=	.t.
      case m.tipofla = 'D'
         m.exp	=	{}
      endcase
   endif
else
   do case
   case m.tipofla = 'C'
      m.exp	=	''
   case m.tipofla = 'N'
      m.exp	=	0
   case m.tipofla = 'L'
      m.exp	=	.t.
   case m.tipofla = 'D'
      m.exp	=	{}
   endcase
endif

if !(m.nerror == " " )
   m.txt_msg	= 'La Expresi�n es Inv�lida : ' + m.nerror
   =fmsgdev()
endif

return m.exp
*
*: EOF: EVALF.PRG
