*************************************************
* FUNCTION FACTUAL
*************************************************
* Autor : Leo
* Dise�o: Leo
* 
* Fecha : 29/12/97
* 
* Funcionamiento: calcula la actualizaci�n
* 
PARAMETERS importe, indice, fechaini, fechafin
PRIVATE coefic, anio, mes, dia, dia_m, anio_ant, mes_ant, mensaje

IF !USED ('INDICES')
	=USET ('RECUR\INDICES')
ENDIF

anio   = YEAR  (m.fechaini)
mes    = MONTH (m.fechaini)
dia    = DAY   (m.fechaini)
coefic = 0

IF SEEK (m.indice + STR (m.anio,4,0) + STR(m.mes,2,0), 'indices') AND ;
  indices.val_ind > 0
	m.coefic = 1 / indices.val_ind
	IF indices.porc_ind <> 0
		m.dias_m =	DAY (GOMONTH (CTOD ('01/' + STR (m.mes, 2) + '/' + ;
                          STR (m.anio, 4)), 1) - 1)
		* m.coefic = m.coefic * (1 + indices.porc_ind * (m.dias_m - m.dia)) / 3000
		m.coefic = m.coefic * (1 + (indices.porc_ind * (m.dias_m - m.dia) / 3000))
	ENDIF

	anio   = YEAR  (m.fechafin)
	mes    = MONTH (m.fechafin)
	dia    = DAY   (m.fechafin)
	IF m.mes > 1
		anio_ant = m.anio
		mes_ant  = m.mes - 1
	ELSE
		anio_ant = m.anio - 1
		mes_ant  = 12
	ENDIF

	IF SEEK (m.indice + STR (m.anio_ant,4,0) + STR(m.mes_ant,2,0),'indices')
		m.coefic = m.coefic * indices.val_ind
		IF SEEK (m.indice + STR (m.anio,4,0) + STR(m.mes,2,0), 'indices')
			IF indices.porc_ind <> 0
				* m.coefic = m.coefic * (1 + indices.porc_ind * m.dia) / 3000
				m.coefic = m.coefic * (1 + indices.porc_ind * m.dia / 3000)
			ENDIF
		ELSE
			mensaje = 'No est�n cargados los valores para el �ndice de actualizaci�n ' +;
				m.indice + ' para el mes ' + ALLT(STR(m.mes,2,0)) + ' del a�o ' +;
				STR(m.anio,4,0) + '. Verifique!'
			=FINAL(15, mensaje)
		ENDIF
	ELSE
		mensaje = 'No est�n cargados los valores para el �ndice de actualizaci�n ' +;
			m.indice + ' para el mes ' + ALLT(STR(m.mes_ant,2,0)) + ' del a�o ' +;
			STR(m.anio_ant,4,0) + '. Verifique!'
		=FINAL(15, mensaje)
	ENDIF
ELSE
	mensaje = 'No est�n cargados los valores para el �ndice de actualizaci�n ' +;
		m.indice + ' para el mes ' + ALLT(STR(m.mes,2,0)) + ' del a�o ' +;
		STR(m.anio,4,0) + '. Verifique!'
	=FINAL(15, mensaje)
ENDIF

IF m.coefic < 1
	m.coefic = 0
ELSE
	m.coefic = m.coefic - 1
ENDIF

RETURN ROUND (m.importe * m.coefic, 2)

