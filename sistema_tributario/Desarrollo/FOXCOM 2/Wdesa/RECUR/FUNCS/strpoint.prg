*************************************************
* FUNCTION StrPoint
*************************************************
* Autor : Carlos
* Dise�o: Carlos
* 
* Fecha : 17/04/1996
* 
* Funcionamiento:
* Esta funci�n se utiliza para pasar un n�mero
* a string, asegur�ndose que va con punto
* decimal y no con coma.
* 
* 
* Par�metros:
* (N) numero = N�mero a transformar en string.
* (N) longitud = Largo del string.
* (N) decimales = Cantidad de decimales
* 
* 
* Modificaciones:
* 
PARAMETERS numero, longitud, decimales
PRIVATE numstr, setpoint

m.setpoint = SET ('POINT')
SET POINT TO '.'
m.numstr = STR (m.numero, m.longitud, m.decimales)
SET POINT TO m.setpoint

RETURN m.numstr
