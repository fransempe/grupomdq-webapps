*************************************************
FUNCTION FIMPSDE
*************************************************
PARAMETERS m.tipimp, m.nro, m.msg
PRIVATE retorno, m.tipo_cod, nro_reg, m.tipo_imp, m.nro_imp
retorno 	= .T.
nro_reg		= RECNO('CODIFICS')
m.tipo_cod	= 'RECURS'
m.tipo_imp	= m.tipimp
m.nro_imp	= m.nro
IF SEEK(EVAL(exp_cod2),'CODIFICS')
	DO WHILE !EOF('codifics') AND codifics.tipo_cod = m.tipo_cod AND retorno
		retorno = VerCC(SUBSTR(codifics.codif,1,2))
		SKIP IN CODIFICS
	ENDDO
ENDIF
=IR_REG(nro_reg, 'CODIFICS')
RETURN retorno

*************************************************
FUNCTION VerCC
*************************************************
PARAMETERS m.rec
PRIVATE retorno, nro_reg2, m.recurso
m.recurso	= m.rec
retorno 	= .T.
nro_reg2	= RECNO('REC_CC')
SET KEY TO EVAL(exp_rec) IN REC_CC
GO TOP IN REC_CC
DO WHILE !EOF('REC_CC') AND retorno
	retorno = !((rec_cc.est_mov = 'NO' OR rec_cc.est_mov = 'DP') AND VAL(rec_cc.imp_mov) > 0)
	SKIP IN REC_CC
ENDDO
IF !retorno AND m.msg
	=MENS_AVI(m.sistema , "IMPONIBLE REGISTRA DEUDA")
ENDIF
SET KEY TO '' IN REC_CC
=IR_REG(nro_reg, 'REC_CC')
RETURN retorno
