****************************
* FUNCTION FDomEnv
****************************
PARAMETERS recurso
PRIVATE retorno, tab_dom
retorno = ''

IF m.programa = 'PIMPNOT'
	m.tipo_imp = not_impn.tipo_imp
	m.nro_imp = not_impn.nro_imp
ENDIF

IF !USED ('rec_imp')
	= UseT ('recur\rec_imp', 'primario')
ENDIF

IF SEEK (m.tipo_imp + STR (m.nro_imp, 10, 0) + m.recurso, 'rec_imp')
	DO CASE
		CASE rec_imp.dom_envio = 'E' 				
			tab_dom = 'rec_imp'
		CASE rec_imp.dom_envio = 'I'
			DO CASE
				CASE rec_imp.tipo_imp = 'I'
		  			tab_dom = 'inmueble'
					IF m.programa = 'PIMPNOT'
						= SEEK (STR (not_impn.nro_imp, 10, 0), 'inmueble')
					ENDIF
				CASE rec_imp.tipo_imp = 'C'
		  			tab_dom = 'comercio'
					IF m.programa = 'PIMPNOT'
						= SEEK (STR (not_impn.nro_imp, 10, 0), 'comercio')
					ENDIF
				CASE rec_imp.tipo_imp = 'V'
		  			tab_dom = 'vehiculo'
			ENDCASE
		OTHERWISE
			tab_dom = 'contrib'
			IF m.programa = 'PIMPNOT'
				= SEEK (STR (not_impn.cuit, 10, 0), 'contrib')
			ENDIF
	ENDCASE   

	retorno = ALLT (&tab_dom..cod_post) + ' ' + ALLT (&tab_dom..nomb_loc) + ' ' + ALLT (&tab_dom..nomb_calle) + ' ' + ALLT (&tab_dom..puerta) + ' Piso: ' + ALLT (&tab_dom..piso) + ' Depto.: ' + ALLT (&tab_dom..depto)
ENDIF

RETURN retorno