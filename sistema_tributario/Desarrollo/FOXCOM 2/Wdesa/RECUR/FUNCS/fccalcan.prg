***********************************
FUNCTION FCCALCAN
***********************************
PARAMETERS m.cod_pror
PRIVATE retorno, alias_ant
m.retorno = .T.

m.alias_ant = Alias ()

=USET ('RECUR\PROR_CUO')
SELECT PROR_CUO

* Buscar el �ltimo c�lculo cerrado o activo (no anulado)
PRIVATE nro_calc, est_calc
m.nro_calc = 0
m.est_calc = ''
=FLEERUCA (m.cod_pror, @m.nro_calc, @m.est_calc)

IF m.nro_calc > 0
	IF m.est_calc = 'ACTIV'
		
		* Si el c�lculo est� activo leerlo y ...
		WAIT "Cerrando c�lculo de costos unitarios..." WIND NOWAIT		
		=SEEK (m.cod_pror + STR (m.nro_calc, 3, 0), 'pror_val')
		
		* Recorrer la tabla de cuotas emitidas
		SELECT pror_cuo
		
		*	SQLMODI_05
		* SET KEY TO RANGE cod_pror, cod_pror IN pror_cuo
		SET KEY TO m.cod_pror IN pror_cuo		
		
		GO TOP IN pror_cuo
		
		PRIVATE tot_emit
		m.tot_emit = 0
		
		*	SQLMODI_05
		* DO WHILE !EOF ('pror_cuo') 
		DO WHILE !EOF ('pror_cuo') AND retorno
			IF m.nro_calc  = pror_cuo.nro_calc
				
				* Si el n�mero de c�lculo de la cuota coincide con la del costo
				m.tot_emit = pror_cuo.imp_sup + pror_cuo.imp_fte + pror_cuo.imp_val + pror_cuo.imp_mult
				
				IF (pror_cuo.gen_comp = 'N' OR pror_cuo.deb_cc = 'N') AND m.tot_emit > 0
					
					* Si (no fueron generados la cta. cte. o los comprobantes) y el total emitido es mayor que cero
					*	SQLMODI_05
					= Msg ('No se puede cerrar el C�lculo de Costos Unitarios activo.' + CHR (13) + CHR (13) + 'Razones: ' + IIF (pror_cuo.gen_comp = 'N', 'Comprobantes no generados. ', '') + IIF (pror_cuo.deb_cc = 'N', 'Cuotas no debitadas. ', '') ;
					+ CHR (13) + 'Total Emitido mayor que cero.')

					* Setear el flag para finalizar el recorrido de cuotas e indicar fin del proceso
					m.retorno = .F.
					
				ENDIF
			ENDIF
			
			SKIP IN pror_cuo
		ENDDO

		*	SQLMODI_05
		* SELECT pror_cuo
		* SET KEY TO RANGE 
		SET KEY TO '' IN pror_cuo
		
		IF retorno = .T.
			
			* Si el c�lculo anterior puede cerrarse pedir confirmaci�n al usuario
			IF sino ('� Confirma que desea cerrar el prorrateo anterior ?') = .T.

				* Setear el c�lculo anterior en cerrado
				SELECT pror_val
				REPLACE pror_val.est_val WITH 'CERRA'
				
			ELSE

				* No se puede procesar
				retorno = .F.
			ENDIF
			
		ENDIF
		
	ENDIF
ENDIF

SELECT (m.alias_ant)

RETURN retorno
