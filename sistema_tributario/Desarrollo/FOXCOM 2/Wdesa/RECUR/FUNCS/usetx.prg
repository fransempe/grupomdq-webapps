*************************************************
* FUNCTION UsetX
*************************************************
PARAMETERS ut_tabla
PRIVATE ut_pos, ut_stabla, ut_arch_tab, ut_arch_ind, ut_sistema, ut_stab_ver
PRIVATE retorno, on_err, que_error

que_error		=	.T.
retorno     = .T.
ut_sistema  = SUBSTR (ut_tabla, 1, RAT('\', ut_tabla) - 1)
ut_stabla   = SUBSTR (ut_tabla, RAT('\', ut_tabla) + 1)
ut_arch_tab = ut_tabla + '.dbf'
ut_stab_ver = ut_stabla
		
IF UPPER(ut_sistema) == 'CONTA' AND TYPE ('_EJERC') = 'C' AND !EMPTY (_EJERC)	
	ut_tabla    = _EJERC + '\' + ut_tabla
	ut_arch_tab = _EJERC + '\' + ut_arch_tab
ENDIF

 * db_conta
IF UPPER(ut_sistema) == 'CONTA' AND TYPE ('_DB_CONTA') = 'C' AND !EMPTY (_DB_CONTA)	
	ut_tabla    = _DB_CONTA + ut_tabla
	ut_arch_tab = _DB_CONTA + ut_arch_tab
ENDIF

 * db_recur
IF UPPER(ut_sistema) == 'RECUR' AND TYPE ('_DB_RECUR') = 'C' AND !EMPTY (_DB_RECUR)	
	ut_tabla    = _DB_RECUR + ut_tabla
	ut_arch_tab = _DB_RECUR + ut_arch_tab
ENDIF

IF !USED (ut_stabla)
	IF FILE (ut_arch_tab)
		on_err = ON('ERROR')
		ON ERROR DO USET_ERR WITH ERROR(), MESSAGE()
		que_error 	= .F.
		USE (ut_tabla) ALIAS (ut_stabla) AGAIN IN 0 EXCL
		ON ERROR &on_err
		IF !retorno
			IF que_error
				= FINAL (1003, ut_tabla)
			ELSE
				= FINAL (1002, ut_tabla)
			ENDIF
		ENDIF
	ELSE
		IF ! FILE (ut_arch_tab)
			= final (1000, ut_arch_tab)
		ENDIF
		retorno = .F.
	ENDIF
ENDIF

RETURN retorno

