*************************************************
* FUNCTION FVBorVlm
*************************************************
* Autor : Gabriel Zubieta
* Dise�o: Fernando
* Fecha : 27/02/95
* 
* Funcionamiento:
*  tipimp  -C- Tipo de Imponible
*  nro_imp -N- Nro de Imponible
*  emitmsg -L- Flag si emite mensaje.
*   
*
* Par�metros:
* Verifica que una vinculacion entre un imponible y un contribuyente
* esta en condiciones de ser dada de baja. 
* Supone que esta cargado en memoria el arreglo con los nombre de imponibles. 
*
* Modificaciones:
* 
PARAMETERS m.tipimp, m.nro, m.emitmsg
PRIVATE retorno

retorno = FImpSRec( m.tipimp , m.nro , .T. )
IF retorno
	retorno = FImpSde( m.tipimp , m.nro , .T.  )
ENDIF

IF !retorno AND m.emitmsg
	=mens_avi( m.sistema , "VINC. NO PUEDE DARSE DE BAJA" )	
ENDIF

RETURN retorno
*
