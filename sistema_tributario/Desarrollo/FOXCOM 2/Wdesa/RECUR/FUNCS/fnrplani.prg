*************************************************
* FUNCTION FNrPlani
*************************************************
* Autor : Cristian Carav�
* Dise�o: Cristian Carav�
* 
* Fecha : 04/04/95
* 
* Funcionamiento: Busca la fecha pasada como par�metro en la tabla 
*									Calenda. Si existe devuelve .T., as� como el Nro. de
*									Planilla (campo pla_ingre) y Nro. de Caja (campo 
*									nro_caja), en los par�metros pasados por referencia. 
*									Si no existe devuelve .F.
*
*		IMPORTANTE  : Se deber� tener definida en el programa que utilize la
*									funci�n, la expresi�n del �ndice Primario (exp_cal) de
*									la tabla Calenda. Tambi�n debe estar abierta la tabla.
* 
* Par�metros:	m.fecha	(D) : fecha a buscar.
* 						m.nro_plani	: nro. de planilla. (por referencia)
* 						m.nro_caja  : nro. de caja.     (por referencia)
* 
* Modificaciones:
* 
PARAMETERS m.fecha, m.nro_plani, m.nro_caja
PRIVATE retorno, cant_param
retorno    = .T.
cant_param = 0

cant_param = PARAMETERS ()
IF cant_param >=1
	=VERTIPAR ('FNrPlani', 'm.fecha', 'D')
			
	IF cant_param > 1 
		=VERTIPAR ('FNrPlani', 'm.nro_plani', 'N')
	ELSE
		m.nro_plani = 0
	ENDIF
			
	IF cant_param > 2
		=VERTIPAR ('FNrPlani', 'm.nro_caja', 'N')
	ELSE
		m.nro_caja = 0
	ENDIF
			
ELSE
	=VERNUPAR ('FNrPlani', cant_param, 1)
ENDIF

IF TYPE ('exp_cal') # 'C'
	=MSG ('No est� definida la variable (EXP_CAL), que contiene la'+;
				'expresi�n del �ndice.')
	retorno = .F.
ENDIF

IF retorno
	IF !USED ('CALENDA')
		retorno = USET ('CONTA\CALENDA')
	ENDIF
ENDIF

IF retorno
	IF SEEK (EVAL (exp_cal), 'CALENDA')
		m.nro_plani = calenda.pla_ingre
		m.nro_caja  = calenda.nro_caja
		retorno = .T.
	ELSE
		retorno = .F.
	ENDIF
ENDIF

RETURN retorno
