*************************************************
*FUNCTION FAbrVar
*************************************************
* Autor :ELIO
* Dise�o:
* 
* Fecha :07-02-95
* 
PARAMETER m.exclusivo
= VerTiPar('FABRVAR', 'exclusivo', 'L')
PRIVATE retorno, erro_aux, nerror, m.area_act, donde
retorno = .T.
donde = " "
m.area_act = " "
nerror = 0
erro_aux = ON('ERROR')
ON ERROR nerror = ERROR()
IF USED('variabs')
	m.area_act = SELECT()
	SELE variabs
ENDIF
IF EMPTY(m.area_act)
	donde = "IN 0 "
ENDIF
donde = donde + 'ORDER TAG primario '
IF m.exclusivo
	donde = donde + ' EXCL'
ENDIF

* SQLMODI01 - Comentado
*USE 'recur\variabs' &donde

* SQLMODI01 - Nuevo
IF Type ('_db_recur') = 'U'
	USE 'recur\variabs' &donde
ELSE
	USE _db_recur + 'recur\variabs' &donde
ENDIF


ON ERROR &erro_aux
IF nerror # 0
	=mens_avi( m.sistema , "TABLA VARIABLES EXCLUSIVA" )
	= final( 1 )
ENDIF
IF !EMPTY(m.area_act)
	SELE (m.area_act)
ENDIF

RETURN retorno

