*************************************************
* FUNCTION FDigImp
*************************************************
* Autor : Alejandro
* Dise�o: Alejandro
* 
* Fecha : 26/05/99
* 
* Funcionamiento:
* Obtiene el d�gito verificador de un imponible
* 
* Par�metros:
* tipo_imp -C- Tipo de Imponible
* nro_imp  -N- N�mero de Imponible
* 
PARAMETERS m.tipo_imp, m.nro_imp
PRIVATE dig_veri

m.dig_veri = 0

DO CASE
	CASE m.tipo_imp = 'I'
		= Seek (Str (m.nro_imp, 10, 0), 'inmueble')
		m.dig_veri = inmueble.dig_veri		
		
	CASE m.tipo_imp = 'C'
		= Seek (Str (m.nro_imp, 10, 0), 'comercio')
		m.dig_veri = comercio.dig_veri		
	
	CASE m.tipo_imp = 'O'
		= Seek (Str (m.nro_imp, 10, 0), 'cement')
		m.dig_veri = cement.dig_veri		
	
	CASE m.tipo_imp = 'E'
		= Seek (Str (m.nro_imp, 10, 0), 'ubic_cem')
		m.dig_veri = ubic_cem.dig_veri		
	
	CASE m.tipo_imp = 'N'
		= Seek (Str (m.nro_imp, 10, 0), 'contrib')
		m.dig_veri = contrib.dig_veri		
	
	CASE m.tipo_imp = 'V'
		= Seek (Str (m.nro_imp, 10, 0), 'vehiculo')
		m.dig_veri = vehiculo.dig_veri		

ENDCASE
	
RETURN m.dig_veri

