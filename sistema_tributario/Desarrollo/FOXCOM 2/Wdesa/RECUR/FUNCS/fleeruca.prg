******************************************
FUNCTION FLEERUCA
******************************************
PARAMETERS m.cod_pror, m.nro_calc, m.est_calc
PRIVATE retorno

PRIVATE area_ant, order
m.area_ant = ALIAS ()
m.order = ORDER ()

m.nro_calc = 0

SELECT pror_val
SET ORDER TO primario DESCENDING
SET KEY TO m.cod_pror
GO TOP IN pror_val

DO WHILE !EOF () AND m.nro_calc = 0
	IF pror_val.est_val <> 'ANUL'
		m.nro_calc = pror_val.nro_calc
		m.est_calc = pror_val.est_val
	ELSE
		SKIP
	ENDIF
ENDDO

SET KEY TO RANGE
SET ORDER TO primario ASCENDING

IF !EMPTY (m.area_ant)
	SELECT (m.area_ant)
ENDIF

SET ORDER TO (m.order)

IF m.nro_calc > 0
	retorno = .T.
ELSE
	retorno = .F.
ENDIF

RETURN retorno
