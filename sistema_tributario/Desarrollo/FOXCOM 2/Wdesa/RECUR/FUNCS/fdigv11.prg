*************************************************
* FUNCTION FDigV11
*************************************************
* Autor  : Gabriel Zubieta
* Dise�o : Pachu
* Fecha  : 07/02/95
* 
* Funcionamiento:
* Calcula el D�gito Verificador del N�mero dado
* Utiliza el Algoritmo del "Mod 11". 
*
*
* Par�metros:
*  m.numero -C- Numero sobre el cual se quiere calcular el d�gito verificador
* 
* 
* Modificaciones:
* 
PARAMETERS m.numero

=VerNuPar ('FDIGV11', PARAMETERS(), 1 )
=VerTiPar ('FDIGV11','NUMERO'     ,'C')

PRIVATE retorno, m.digver, m.modulo, m.largo, i

retorno = .T.
m.largo = LEN( m.numero )
m.digver = 0
m.modulo = 2

FOR i = m.largo TO 1 STEP -1
	IF isdigit( SUBSTR(m.numero,i,1) )
		m.digver = m.digver + ( VAL( SUBSTR(m.numero,i,1) ) * m.modulo )
	ENDIF
	m.modulo = m.modulo + 1
	IF m.modulo >= 8
		m.modulo = 2
	ENDIF
ENDFOR

m.digver = MOD( m.digver , 11 )

IF m.digver > 0
	m.digver = 11 - m.digver
ENDIF

IF m.digver = 10
	m.digver = 9
ENDIF

retorno = INT( m.digver )

RETURN retorno

