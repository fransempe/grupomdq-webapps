******************************************************
* FUNCTION FAudLega
******************************************************
* Auditoria para ABMC de Legajos
*

PARAMETERS accion
PRIVATE retorno, alias_ant
m.retorno = .T.
m.alias_ant = ALIAS()

IF m.retorno
	IF USED ('A_AUDLEG')
		USE IN A_AUDLEG
	ENDIF
	
	IF FILE(ALLTRIM(_DBASE)+'RECHUM\'+'\AUDLEGA.DBF') 
		IF !USED ("A_AUDLEG")
			USE ALLTRIM(_DBASE)+'RECHUM\'+'\AUDLEGA' IN 0 AGAIN ALIAS A_AUDLEG
	    SELECT A_AUDLEG
	    SET ORDER TO 1
			= APPEND()

			REPLACE num_aud WITH m.num_aud,;
				  fec_movim	WITH DATE(),;
				  hora_proc WITH STOT(SECONDS()),;
				   proc_aud	WITH m.accion,;
				 nro_legajo	WITH legajos.nro_legajo,;
				 fec_recono	WITH legajos.fec_recono,;
				  antig_ips	WITH legajos.antig_ips,;
				  antig_otr	WITH legajos.antig_otr,;
				 mes_recips	WITH legajos.mes_recips,;
				 mes_recotr	WITH legajos.mes_recotr
		ELSE
			* No puede abrir Audlega*
			m.retorno = .F. 			
		ENDIF
	ELSE
		* No existe tabla Audlega*
		m.retorno = .F. 
	ENDIF	

	IF !m.retorno
*		= Msg('Se encontraron problemas con las tablas de auditor�a')
	ENDIF
ENDIF

SELECT (m.alias_ant)
RETURN m.retorno
