*************************************************
FUNCTION BusNroFa
*************************************************
* Autor : Alejandro
* Dise�o: Alejandro
* 
* Fecha : 22/03/2000
* 
* Funcionamiento: Devuelve el n�mero de factura para una Orden de Pago
*                 Se usa desde la emisi�n de O.P.
* Par�metros:  N�mero de Orden de Pago
* 
* 
* 
* Modificaciones:
* 

PARAMETERS m.nro_or_pag
PRIVATE m.retorno, m.alias_old, m.order_old, m.reg_old
m.retorno = ""
m.alias_old = ALIAS()


IF VAL(m.tit_fact) > 0
	SELE FACT_OP
	m.order_old = ORDER()
	m.reg_old 	= RECNO()
	SET ORDER TO PRIMARIO

	SET KEY TO STR (m.nro_or_pag,5,0)
	GO TOP
	DO WHILE .NOT. EOF()
		IF IMP_PA_OP > 0
			m.retorno = NRO_FACT
		ENDIF	
		SKIP IN FACT_OP
	ENDDO
	SET KEY TO
	SET ORDER TO (m.order_old)
	=IR_REG (m.order_old)
	SELE (m.alias_old)
ELSE
	retorno = m.tit_fact
ENDIF
	
RETURN retorno

