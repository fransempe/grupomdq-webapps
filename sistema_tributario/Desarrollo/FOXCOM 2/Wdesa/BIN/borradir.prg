*************************************************
* FUNCTION BorraDir
*************************************************
* 
* Funcionamiento: Borra todos los archivos de un directorio y
* el directorio tambi�n. Es como el comando deltree, pero implementado
* en Fox.
* 
* OJO: CON ESTA FUNCION SE PUEDE BORRAR CUALQUIER COSA.
*      POR FAVOR UTILIZAR CON PRECAUCION.
* 
PARAMETER dir_a_borrar
PRIVATE retorno, directorio, nom_dir, nom_arch, i, cant_arch
retorno = .T.

m.dir_a_borrar = AllTrim (m.dir_a_borrar)

IF Right (m.dir_a_borrar, 1) = '\'
	m.dir_a_borrar = Left (m.dir_a_borrar, Len (m.dir_a_borrar) - 1)
ENDIF

IF !SiNo ('�Confirma Eliminar el directorio ' + m.dir_a_borrar + '?')
	retorno = .F.
	RETURN retorno
ENDIF

m.cant_arch = ADir (directorio, m.dir_a_borrar + '\*.*', 'D')

IF m.cant_arch = 0
	= Msg ('El directorio ' + m.dir_a_borrar + ' est� vac�o o no existe.')
	RETURN retorno
ENDIF

WAIT WIND 'Eliminando Directorio ...' NOWAIT

FOR m.i = 1 TO m.cant_arch
	IF Right (directorio [m.i, 5], 1) = 'D'
		IF directorio [m.i, 1] != '.'
			retorno = BorraDir (m.dir_a_borrar + '\' + directorio [m.i, 1])
			IF !retorno
				EXIT
			ENDIF
		ENDIF
	ELSE
		m.nom_arch = m.dir_a_borrar + '\' + directorio[m.i, 1]
		* = TTSAttrib (m.nom_arch, .F.)
		DELETE FILE &nom_arch
	ENDIF
ENDFOR

IF retorno
	!RD &dir_a_borrar
ENDIF

WAIT CLEAR

RETURN retorno
