*************************************************
* FUNCTION Alidj_Aya
*************************************************
PRIVATE alicuota

m.alicuota = 0

IF !USED ('recur\rubs_com')
	=UseT ('recur\rubs_com')
ENDIF

IF Seek (Str (c_comerc.nro_com, 10, 0), 'comercio') AND SEEK (STR (comercio.rubro1, 7, 0), 'rubs_com')
	m.alicuota = rubs_com.porc_iddjj 
ENDIF

RETURN m.alicuota