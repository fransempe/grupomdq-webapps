*************************************************
* 
* Funcionamiento:
* 
* Decodifica
*
* . tomo d�gito verificador
* . resto el d�gito verificador a cada d�gito
*   si el resultado es negativo, le sumo 10.
* . as� obtengo cada d�gito
* . los reordeno
* . obtengo la fecha sumando el resultado a {1/1/1980}
* 
* 
PARAMETERS m.longitud
PRIVATE retorno, digitos, numero, dig_ver, i

************************************************************************
*	
*	SQLMODI-01
*		Usuario							: Todos
*		Fecha								: 15/09/2000
*		Quien modifico			: Carlos
*		Detalle problema		: Los programas activados se expiraban antes
*													de tiempo.
*													
************************************************************************

retorno = {//}
DIMENSION digitos [6]
digitos[6] = m.longitud % 10
m.longitud = Int (m.longitud / 10)
digitos[5] = m.longitud % 10
m.longitud = Int (m.longitud / 10)
digitos[4] = m.longitud % 10
m.longitud = Int (m.longitud / 10)
digitos[3] = m.longitud % 10
m.longitud = Int (m.longitud / 10)
digitos[2] = m.longitud % 10
m.longitud = Int (m.longitud / 10)
digitos[1] = m.longitud % 10

m.numero = 0
FOR m.i = 1 TO 5
	digitos[m.i] = digitos[m.i] - digitos[6]
	IF digitos[m.i] < 0
		digitos[m.i] = 	digitos[m.i] + 10
	ENDIF
	m.numero = m.numero * 10
	m.numero = m.numero + digitos[m.i]
NEXT

m.dig_ver = Int (m.numero ^ 2) % 10
IF m.dig_ver = 0
	m.dig_ver = 9
ENDIF
IF digitos[6] != m.dig_ver
	m.numero = 0
ENDIF

IF m.numero > 30000 OR m.numero < 7500
	m.numero = 30000
ENDIF

*retorno = {01/01/1965} + m.numero
retorno = {^1965-01-01} + m.numero
IF retorno <= Date ()
	retorno = Date ()
ELSE
	* Comienzo SQLMODI-01
	* retorno = retorno + 1
	* Fin SQLMODI-01
ENDIF

RETURN retorno
