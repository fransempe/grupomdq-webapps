******************************************************
* FUNCTION ar_tm_op
******************************************************
* Funci�n especial para BARADERO para poder manejar el reporte
* esta funci�n se utiliza fuera del proyecto.
*
PRIVATE i, lin_imp, lin_ite, ped_memo, fal_imp, fal_ite, fal_ped
PRIVATE cant_imp, cant_ite, tot_items, primvez, ant_reg
PRIVATE tam_importe
*
* i        -N- Vble para impresion de lineas en blanco
* lin_imp  -N- Vble que controla cantidad de imput. que se imprimen. 
* lin_ite  -N- Vble que controla cantidad de items que se imprimen.
* fal_imp  -B- Flag que faltan imputaciones
* fal_ite  -B- Flag que faltan items.
* cant_imp -N- Indica la cantidad de imputaciones
* cant_ite -N- Indica la cantidad de items
* primvez  -B- Flag que indica si se imprimio el total de los items
* tam_importe -N- Define el tama�o de las l�neas de importe

tot_items = 0
cant_imp  = 26
cant_ite  = 3
fal_ite   = .T.
fal_imp   = .T.
lin_imp   = 0 
lin_ite   = 0 
subtot    = 0
valtotimp = 0
tot_ret   = 0
tot_ret_imp = 0
prim_ret  = .T.
tot_imp_imp = .F.
impor_cheq = ord_pag.importe_op
tam_importe = 71

* Variables agregadas por RODRIGO para generar 2 lineas de importe en letras
=IMPOR_LE(tam_importe)

*[*] Modificaci�n Nicol�s .Agregu� campo. c_nabrctap

* CREATE CURSOR tmp_imp ( linea C(96) )
CREATE CURSOR tmp_imp (	c_tipegre C(1), ;
												c_nroorcom C(5), ;
												c_finalid C(2),	;
											 	c_func_it C(2),	;
											 	c_rubro1 C(2), 	;
												c_rubro2 C(2),	;
												c_rubro3 C(2), 	;
												c_rest1 C(2),		;
												c_rest2 C(2), 	;
												c_rest3 C(2), 	;
												c_prog C(3), 		;
												c_activ C(2), 	;
												c_uniej C(5),		;
												c_nabrctap C(15),;
												c_ctadsc C(40), ;
												c_ctaimp C(20), ;
												c_ctarec C(16),;
												c_imp_op C(20), ;
												c_trans1 C(10), ;
												c_trans2 C(20), ;
												c_total C(20), ;
												c_nroctao C(4), ;
												c_nomctao C(35),;
												c_imp_op2 C(20),;
												c_descret C(15), ;
												c_impret C(20), ;
												c_impche C(20), ;
												c_totop C(20), ;
												c_suma C(96), ;
												c_pagar C(96))
SELECT tmp_imp
IF ALIAS() = 'tmp_imp'
	ZAP
ENDIF

SELECT c_ret
GO TOP IN c_ret
DO WHILE ! EOF ('c_ret')
	impor_cheq = impor_cheq - c_ret.valor	
	SKIP IN c_ret
ENDDO

COUNT TO tot_ret
tot_ret_imp = 2

GO TOP IN c_ret

ant_reg = RECNO ('ORPA_IM')
DO WHILE fal_ite OR fal_imp

	lin_imp = 0

* Linea comentada por RODRIGO para que haga bien el transporte
*	subtot = 0

	DO WHILE ! EOF('ORPA_IM') AND ;
	(ORPA_IM.nro_or_pag = ORD_PAG.nro_or_pag) AND ;
	( lin_imp < cant_imp ) AND ;
	fal_imp
		subtot = subtot + orpa_im.imp_imp_op
		valtotimp = valtotimp + orpa_im.imp_imp_op
		= lin_imput ()
		= push_lin ()
		lin_imp = lin_imp + 1
		SKIP IN ORPA_IM
	ENDDO
	
	IF fal_imp 
		IF ( lin_imp = cant_imp ) AND ! EOF ('ORPA_IM') AND ;
		(ORPA_IM.nro_or_pag = ORD_PAG.nro_or_pag)
			fal_imp = .T.
		ELSE
			fal_imp = .F.
		ENDIF
	ENDIF

	IF fal_imp
* <verif>
		= limp_vars ()
		m.c_trans1 = 'TRANSPORTE'
		m.c_trans2 = TRANS (subtot, '9,999,999,999,999.99')
		= push_lin ()
		* SPACE (52) + 'TRANSPORTE' + SPACE (3) + TRANS (subtot, '9,999,999,999,999.99') )
		= imp_blank (2)
	ELSE
	    *vfp
		*= imp_blank (28 - lin_imp )
		* aqui se imprimemn los blancos antes la impresion del total de la emi.pago!!
		= imp_blank (33 - lin_imp )
		*vfp
		IF tot_imp_imp = .F.
			= limp_vars ()
			m.c_total = TRANS (valtotimp, '9,999,999,999,999.99')
			= push_lin ()
			* 'tmp_imp', SPACE (65) + TRANS (valtotimp, '9,999,999,999,999.99') )
			tot_imp_imp = .T.
		ELSE
			= imp_blank (1)
		ENDIF
	ENDIF

    *
	*= imp_blank (11)
	= imp_blank (10)
	*			
	IF ! fal_imp AND ((tot_ret - tot_ret_imp) <= 3)
		= limp_vars ()
*[*] Modi Nicol�s Elimine la l�nea anterior, y la reemplace por la de abajo
*		m.c_pagar = 'P�guese a : ' + proveed.nomb_prov

		*vfp	
		*m.c_pagar = 'A librar de Cta. Bancaria ' + STR(ord_pag.nro_ctab,5,0)
		m.c_pagar = '                 ' + STR(ord_pag.nro_ctab,5,0)
		*vfp
		
 		= push_lin ()
		*m.c_pagar = 'Retira: ' + proveed.nomb_aut1
		m.c_pagar = '         ' 
 		= push_lin ()
		= imp_blank (3)
		m.c_pagar = ''

* Estas l�neas fueron reemplazadas por las de abajo (Rodrigo)
*		m.c_suma = 'La suma de pesos : ' +;
		PADR(PROPER(NUMLET(ord_pag.importe_op, .F.))+' ',71,'�')
*		= push_lin ()

*[*] Modi de Nicol�s. Esta L�nea fue reemplazada por la de abajo
 		m.c_suma = 'SON PESOS: ' + PROPER(m.imp1)
*		m.c_suma = PROPER(m.imp1)
		= push_lin ()
*		m.c_suma = '                   ' + PROPER(m.imp2)
		m.c_suma = PROPER(m.imp2)
		= push_lin ()
		= imp_blank (1)

	ELSE
		= imp_blank (2)
	ENDIF
	* * *
	* Pedro
	* = imp_blank (3)
	IF ! fal_imp
		= imp_blank (1)
	ELSE
		= imp_blank (2)
	ENDIF
	* * *

	IF ! fal_imp OR tot_ret > 5 && xxx para que ret en ult. hoja
	IF ((tot_ret - tot_ret_imp) <= 3)
		= limp_vars ()
		m.c_impche = trans (impor_cheq,'9,999,999,999,999.99')
		= push_lin ()
		= imp_blank (2)
	ELSE
		= imp_blank (3)
	ENDIF
	
	IF prim_ret
		prim_ret = .F.
		= limp_vars ()
		m.c_impret = trans (c_ret.valor, '9,999,999,999,999.99')
		= push_lin ()
		SKIP IN c_ret
		m.c_impret = trans (c_ret.valor, '9,999,999,999,999.99')
		= push_lin ()
		SKIP IN c_ret
	ELSE
		= imp_blank (1)
	ENDIF

	lin_ite = 0
	= limp_vars ()
	DO WHILE ! EOF('c_ret') AND ( lin_ite < cant_ite ) AND fal_ite
		m.c_descret = ''
		m.c_impret = ''
		m.c_descret = c_ret.desc
		m.c_impret = TRANS (c_ret.valor, '9,999,999,999,999.99')
		=push_lin()
		tot_ret_imp = tot_ret_imp + 1
		lin_ite = lin_ite + 1
		SKIP IN c_ret
	ENDDO
	IF ( lin_ite = cant_ite ) AND  ! EOF ('c_ret')
		fal_ite = .T.
	ELSE
		fal_ite = .F.
	ENDIF
*	fal_ite = ( lin_ite > cant_ite )

	= imp_blank (1)
	m.c_descret = ''
	m.c_impret = ''
	IF fal_ite
		m.c_totop = 'CONT. EN OTRA HOJA'
		= push_lin ()
	ELSE
** ACA

		*vfp
		*= imp_blank (3 - lin_ite )
		= imp_blank (2)
		*vfp
		
		
		m.c_totop = TRANS (ord_pag.importe_op, '9,999,999,999,999.99')
		= push_lin ()
	ENDIF
	ELSE
*		= imp_blank (10)
		= imp_blank (12)
	ENDIF && FIN XXX
	
	*vfp
	*= imp_blank (5)
	*vfp

ENDDO
= IR_REG (ant_reg, 'ORPA_IM')
RETURN .T.
*
