*************************************************
*FUNCTION LeyNueva
*************************************************
PRIVATE retorno,continuar,filtro,promedio
*EXTERNAL ARRAY matvari
retorno = 0
m.continuar = .T.

	=USET('RECHUM\LEY24714')
	SET ORDER TO primario IN ley24714
	*m.filtro = STR(m.anio,4,0)+ STR(m.mes,2,0)+ STR(concepto.cod_concepto,5,0)
	SET KEY TO STR(parametr.anio_haber,4,0)+ STR(parametr.mes_haber,2,0)+ STR(concepto.cod_concep,5,0) In ley24714
	GO TOP IN ley24714
	
	IF m.resultado <= 0
		IF m.resultado = 0 OR m.resultado = -1
			m.promedio = 0
		ELSE
			m.promedio = ABS(m.resultado)
		ENDIF
	ELSE
		m.promedio = m.resultado
	ENDIF
	
	IF Eof('ley24714')
		*matvari[6,3] = 0
		m.retorno = 0
	ENDIF
	
	DO WHILE !Eof('ley24714') AND m.continuar
		IF ABS(ley24714.prom_desde) <= m.promedio AND;
		  	 ABS(ley24714.prom_hasta) >= m.promedio
		   		m.continuar = .F.
		ENDIF
		IF !m.continuar
			*matvari[6,3] = ABS(ley24714.imp_ley)
			m.retorno = ABS(ley24714.imp_ley)
		ELSE
			*matvari[6,3] = 0
			m.retorno = 0
		ENDIF
		
		
		*Cambiar campo de agente con el promedio*
		m.alias_a = ALIAS()
		SELECT agentes
		REPLACE prom_24714	WITH	m.resultado
		IF !EMPTY(m.alias_a)
			SELECT (m.alias_a)
		ENDIF
		
		SKIP IN ley24714
	ENDDO
	
	SET KEY TO '' IN ley24714	


RETURN retorno
