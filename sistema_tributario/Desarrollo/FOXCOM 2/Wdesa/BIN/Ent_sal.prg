***********************************************************
* FUNCION Ent_Sal
***********************************************************
* Descripción: Dado el parámetro pasado se identifica si es
* una fichada de entrada o de salida.
*
* Parámetros: m.caracter   (c)
*             
*

PARAMETERS m.caracter
PRIVATE    retorno

IF m.caracter = ''
	m.retorno = 'N'
ELSE
	*** SETEAR SEGUN LO QUE VENGA EN EL ASCII DEL RELOJ**
	DO CASE
		CASE m.caracter = 'E'
			m.retorno = 'E'
		CASE m.caracter = 'S'	
			m.retorno = 'S'
		OTHERWISE
			m.retorno = m.caracter
	ENDCASE
ENDIF

RETURN m.retorno