*
* BuscDest()
*
* Busca la descipción abreviada del destino para imprimirla 
* en la O.C.
*

PARAMETERS cod_dest

PRIVATE retorno, ord_old, reg_old

retorno=''

SELE DESTINO

ord_old = ORDER()
reg_old = RECNO()

SET ORDER TO PRIMARIO

IF SEEK (STR(m.cod_dest,5,0))

	retorno =DESTINO.dsc_dest
	
ENDIF

SET ORDER TO  (ord_old)
GOTO reg_old

RETURN retorno	