******************************************
*FUNCTION Imping
*****************************************

* Esta funci�n se utiliza en PILAR
* para importar INGRESOS y DEPOSITOS
*
* Concepto de Dep�sito 	DEPOP
* Concepto de Ingreso 	INGGA - INGGC
*
*
* Proceso:
*
*	- Borrar los registros ya grabados para esa fecha
*
*   - Transfiere   
*

PRIVATE retorno, mes_ini, mes_fin, archivo, nombre, windice
m.retorno = .T.

m.mes_ini = MONTH(m.fec_ini)
m.mes_fin = MONTH(m.fec_fin)

FOR m=m.mes_ini TO m.mes_fin
	m.nombre="MES"+STRTRAN(STR(m,2), ' ', '0')	

	m.archivo = ALLTRIM(_DBASE)+ALLTRIM(_EJERC)+"\INGRESOS\"+nombre+".DBF"
	IF FILE (archivo)
		IF NOT USED(m.nombre)
			USE &archivo IN 0 EXCLU
			windice = SYS (3)
			SELE &nombre
			INDEX ON DTOC(FECHA_PRO,1) TAG windice
		ENDIF
	ELSE
		=MSG("No existe el archivo de Ingresos correspondiente al mes "+STRTRAN(STR(m,2), ' ', '0'))
		=MSG("Corrija el rango de fechas a procesar") 
	
		m.retorno = .F.

	ENDIF
NEXT m 

IF retorno
	=Procesar()
ENDIF	

RETURN m.retorno



*****
FUNCTION Procesar
*****

PRIVATE retorno, arch_err

m.retorno = .T.

IF SINO("Realiza la importaci�n de los Ingresos. ")


	m.arch_err = ALLTRIM(_DBASE)+ALLTRIM(_EJERC)+"\INGRESOS\errores"

	IF NOT USED("ERRORES")
		USE &arch_err IN 0 EXCLU
	ENDIF	

	SELE ERRORES
	DELE ALL
	
	=USET ('CONTA\RECURSOS')
	SCATTER MEMVAR BLANK

	=USET ('CONTA\OTR_CTAS')
	SCATTER MEMVAR BLANK

	=USET ('CONTA\MOV_REC')
	=USET ('CONTA\MOV_OTR')

	=USET ('CONTA\TIPO_MOV')
	SELE TIPO_MOV
	SCATTER MEMVAR BLANK

	=USET ('CONTA\CONCEPTS')
	SELE CONCEPTS
	SCATTER MEMVAR BLANK

	SELE MOV_REC
	SCATTER MEMVAR BLANK
	SET ORDER TO CAJAS	

	SELE MOV_OTR
	SCATTER MEMVAR BLANK
	SET ORDER TO CAJAS

	SELE CALENDA
	IF SEEK (DTOC(m.fec_ini,1))
		DO WHILE NOT EOF() AND calenda.fecha<=m.fec_fin
			WAIT WIND "Procesando fecha "+DTOC(calenda.fecha) NOWAIT
			m.nombre="MES"+STRTRAN(STR(MONTH(calenda.fecha),2), ' ', '0')	

			SELE &nombre
		
			SET KEY TO DTOC(calenda.fecha,1)
		
			GO TOP
			IF EOF()
				SELE ERRORES
				APPEND BLANK
				REPLACE fecha WITH calenda.fecha
				REPLACE descrip WITH "No existen datos para la fecha."
			ELSE
	
				SELE MOV_REC
				SET KEY TO DTOC(calenda.fecha,1)
				GO TOP
				DELE FOR COD_CONC="INGGA"

				SELE MOV_OTR
				SET KEY TO DTOC(calenda.fecha,1)
				GO TOP
				DELE FOR COD_CONC="INGGC" OR COD_CONC="DEPOP"
	
				
				SELE &nombre

				DO WHILE NOT EOF()
					
					m.importe = sum_import
					m.cta 	  = IDENT_CUE

					IF tipoc
						m.cuenta  = SUBSTR(IDENT_CUE,1,1)+;
									SUBSTR(IDENT_CUE,3,1)+;
									"000"+SUBSTR(IDENT_CUE,5,1)+;
								 	IIF (SUBSTR(IDENT_CUE,7,2)="  ","00",SUBSTR(IDENT_CUE,7,2))+;
									"00"+IIF (SUBSTR(IDENT_CUE,10,2)="  ","00",SUBSTR(IDENT_CUE,10,2))

						IF SEEK (m.cuenta, "RECURSOS")
							m.cod_conc = "INGGA"
			
							SELE MOV_REC
							APPEND BLANK
							REPL mov_rec.ord_rec    WITH ProxNumo ('Mov_rec', .T., .F., 1),;
							     mov_rec.cod_ctar   WITH m.cuenta,;
							     mov_rec.fec_rec    WITH calenda.fecha,;
							     mov_rec.importe    WITH m.importe,;
							     mov_rec.cod_conc   WITH m.cod_conc,;
							     mov_rec.pla_ingre  WITH calenda.pla_ingre,;
							     mov_rec.asiento    WITH 0,;
							     mov_rec.reng_as    WITH 0,;
							     mov_rec.nro_or_pag WITH 0,;
							     mov_rec.nro_cance  WITH 0,;
							     mov_rec.nro_prov   WITH 0,;
							     mov_rec.mayorizado WITH 'N',;
							     mov_rec.tip_regis  WITH BuscTip (m.cod_conc, 'tip_regis')
						ELSE
							SELE ERRORES
							APPEND BLANK
							REPLACE fecha WITH calenda.fecha
							REPLACE cuenta WITH m.cta
							REPLACE descrip WITH "Cuenta del C�lculo inexistente"
						ENDIF
					ELSE
						m.cuenta  = SUBSTR(IDENT_CUE,1,1)+;
									SUBSTR(IDENT_CUE,3,1)+;
								 	SUBSTR(IDENT_CUE,5,1)+;
								 	SUBSTR(IDENT_CUE,7,1)+;
									SUBSTR(IDENT_CUE,9,2)

						m.importe = sum_import

						m.cod_conc = "DEPOP"

						IF SEEK (m.cuenta, "OTR_CTAS")
							SELE MOV_OTR
							APPEND BLANK
							REPL mov_otr.ord_otr    WITH ProxNumo ('Mov_otr', .T., .F., 1),;
								 mov_otr.nro_ctao   WITH VAL(m.cuenta),;
								 mov_otr.fec_otr    WITH calenda.fecha,;
							     mov_otr.importe    WITH m.importe,;
							     mov_otr.cod_conc   WITH m.cod_conc,;
								 mov_otr.num_comp   WITH 0,;
								 mov_otr.asiento    WITH 0,;
								 mov_otr.reng_as    WITH 0,;
								 mov_otr.tip_egre   WITH '',;
								 mov_otr.nro_or_com WITH 0,;
								 mov_otr.nro_or_pag WITH 0,;
								 mov_otr.nro_cance  WITH 0,;
								 mov_otr.nro_prov   WITH 0,;
								 mov_otr.mayorizado WITH 'N',;
								 mov_otr.tip_regis  WITH BuscTip (m.cod_conc, 'tip_regis'),;
								 mov_otr.pla_ingre  WITH 0,;
								 mov_otr.cod_ingr	WITH 0
						ELSE
							SELE ERRORES
							APPEND BLANK
							REPLACE fecha WITH calenda.fecha
							REPLACE cuenta WITH m.cta
							REPLACE descrip WITH "Cuenta bancaria inexistente"		     
						ENDIF		 
					ENDIF
					SELE &nombre
					SKIP IN &nombre
				ENDDO
				
			ENDIF

			SELE CALENDA
		 	SKIP IN CALENDA
		ENDDO
	ENDIF	
	SELE ERRORES
	GO TOP
	IF NOT EOF()
		WAIT WIND "Han ocurrido errores en el proceso, que se listaran a continuaci�n."
*		BROW NOEDIT		
		=IMPRIME("IMP_ING")
	ELSE
		WAIT WIND "Proceso concluido."
	ENDIF
	WAIT CLEAR
	
ENDIF

RETURN m.retorno