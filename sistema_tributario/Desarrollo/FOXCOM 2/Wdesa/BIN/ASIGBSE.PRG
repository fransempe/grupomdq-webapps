*************************************************
* FUNCTION ASIGBSE
*************************************************
** Nombre del archivo de asignación de ctas para La Banda.

PRIVATE retorno, exp_cont
PRIVATE m.camp_ascii

retorno = ''

* Abro la Tabla con los campos ASCII
IF !USED('cpo_asc')
	=uset('asa\cpo_asc')
ENDIF

exp_cont = GETKEY('cpo_asc',.T.,3,'primario')

* Seteo el campo
m.camp_ascii = 'CODENTE'

IF SEEK(EVAL(exp_cont),'cpo_asc')
	IF CPO_ASC.tipo == 'C' 
		retorno = 'CTASBCO.' + PADL(ALLT(CPO_ASC.contenido),3,'0')
	ENDIF
ENDIF
RETURN retorno
