****************************************************
*FUNCTION DebInBSE
****************************************************
*Funci�n que retorna el nombre del archivo ASCII donde
*se leen los datos liquidados informados para el Bco. de Sgo. Estero

PRIVATE retorno
m.retorno = 'BSE_'+ PADL(m.mm,2,'0') +  PADL(SUBSTR(STR(m.aa,4,0),3,2),2,'0')  + '.TXT'
RETURN m.retorno