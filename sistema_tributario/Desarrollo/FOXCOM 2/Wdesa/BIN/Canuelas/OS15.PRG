*FUNCTION os15
* FUNCIONAMIENTO: suma los conceptos de ss sin descuentos
*

PRIVATE m.retorno, m.area_act, reg, XANIO, XCUOTA
*PARAMETER N_IMP

m.retorno  = .T.
m.area_act = SELECT()
SELECT DEV_CD
SET ORDER TO 1
GO TOP
XANIO=DEV_CD.ANIO
XCUOTA=DEV_CD.CUOTA

*set key to "OSANI"+"01/01"+"I"+ STR(3,10,0)+str(0,2,0)+"SS"+str(2001,4,0)+str(1,3,0) in dev_cd   
set key to dev_comp.remesa+dev_comp.tanda+"I"+STR(dev_comp.nro_imp,10,0)+str(grupo_conc,2,0)+"SS"+str(xanio,4,0)+str(xcuota,3,0) in dev_cd
sum imp_cd for CONC_DEV<>60 to m.totalito
set key to "" in dev_cd
GO TOP

*IF m.retorno
*   *m.retorno=SEEK(dev_comp.remesa+dev_comp.tanda+"I"+STR(dev_comp.nro_imp,10,0)+str(grupo_conc,2,0)+"SS"+str(xanio,4,0)+str(xcuota,3,0)+"NORM"+STR(46,4,0))
*   m.retorno=SEEK(STR(dev_comp.nro_imp,10,0)+STR(46,4,0))   
*   IF m.retorno=.t.
*      if dev_cd.imp_cd>=0
*          m.retorno=.f.
*      endif
*   endif
*ENDIF   

SELECT (m.area_act)
return m.TOTALITO

