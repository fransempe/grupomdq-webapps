* funcion XXCAS    SOLO CASARES
*
* FUNCIONAMIENTO: Lee un imponible en la tabla maestra correspondiente
*
* Parametros: m.tipo_imp (C): tipo de imponible
*             m.nro_imp  (N): nro de imponible
* 
PARAMETER m.tipo_imp, m.nro_imp
PRIVATE m.retorno, m.area_act

m.retorno  = .T.
m.area_act = SELECT()

IF m.tipo_imp='I'
   SELECT INMUEBLE
ELSE
   IF m.tipo_imp='C'
       SELECT COMERCIO
   ELSE
      IF m.tipo_imp='N'
           SELECT CONTRIB
      ELSE
          IF m.tipo_imp='O'
             SELECT CEMENT
          ELSE
             m.retorno=.F.
          ENDIF          
      ENDIF
   ENDIF
ENDIF

IF m.retorno
   m.retorno=SEEK(STR(m.nro_imp,10,0))
ENDIF
M.QUE=0
IF M.RETORNO
    DO CASE
       ** 2 h, MANZANAS 1 A 15
       ***********************
	   CASE INMUEBLE.CIRCUN=2  .AND. (ALLT(INMUEBLE.SECCION)="H" or ALLT(INMUEBLE.SECCION)="h")
		   m.QUE=INMUEBLE.NUMMAZNA
		   IF m.QUE>=1 AND m.QUE<=15
	   	 	    M.RETORNO=.T.
	   	   ENDIF
	   	   
	   ** 2 h, FRACIONES 1 Y 2
	   ***********************
	   CASE INMUEBLE.CIRCUN=2  .AND. (ALLT(INMUEBLE.SECCION)="H" or ALLT(INMUEBLE.SECCION)="h")
		   m.QUE=INMUEBLE.NUMFRAC
		   IF m.QUE>=1 AND m.QUE<=2
	   	 	    M.RETORNO=.T.
	   	   ENDIF
	   	   
	   OTHERWISE
   		   M.RETORNO=.F.
	ENDCASE
ENDIF      
    
SELECT (m.area_act)

return m.retorno

