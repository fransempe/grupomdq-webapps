*FUNCTION CAL
*
PARAMETERS cl, SGV, cuo
PRIVATE m.retorno
M.RETORNO=0
DO CASE
   CASE ALLT(cl)="P234" .AND. ALLT(SGV)="C_AGU"
        m.retorno=212+cuo
   CASE ALLT(cl)="P234" .AND. ALLT(SGV)="CA_SE"
        m.retorno=212+cuo
   CASE ALLT(cl)="P234" .AND. ALLT(SGV)="S_AGU"   
        m.retorno=(212+cuo)/2
   CASE ALLT(cl)="P234" .AND. ALLT(SGV)="SE_SE"
        m.retorno=(212+cuo)/2   
   CASE ALLT(cl)="P235" .AND. ALLT(SGV)="C_AGU"
        m.retorno=213+cuo
   CASE ALLT(cl)="P235" .AND. ALLT(SGV)="CA_SE"
        m.retorno=213+cuo
   CASE ALLT(cl)="P235" .AND. ALLT(SGV)="S_AGU"   
        m.retorno=(213+cuo)/2   
   CASE ALLT(cl)="P235" .AND. ALLT(SGV)="SE_SE"
        m.retorno=(213+cuo)/2     
ENDCASE        
return m.retorno

