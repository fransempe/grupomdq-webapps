FUNCTION Ar_Calle
PARAMETERS calle

*Funci�n para el armado del ascii de Preparaci�n de Solicitudes
*de Cr�dito autom�tico para Tresa
*Hay que reducir a la CALLE particular a 15 caracteres y si el
*n�mero es S/N setear en la posici�n 13-14-15 'S/N'

PRIVATE retorno
m.retorno = m.calle
IF ALLT(curslega.numero) = 'S/N'
	m.retorno = PADR(m.calle,12,' ') + 'S/N'
ENDIF
RETURN m.retorno