*PRUEBA
*#DEFINE _HIDDEN_INFO_ "\\SICO08\PRUEBA$\"
*MGP
*#DEFINE _HIDDEN_INFO_ "\\MGP007\EFI_INF$\"

*************************************************
* Inicio Sistema (VFP)
*************************************************
PARAMETERS sINIFile

IF .F.
	_REFOX_ = (9876543210)
	_REFOX_ = (9876543210)
ENDIF

PUBLIC _INIFILE, _SCAPTION, _SICOKEY, _SAFEMODE


* _SAFEMODE indica si se est� ocultando la ubicaci�n de la dbase y de efimuni en el servidor
* Se modifica en funci�n de la definici�n de la constante _HIDDEN_INFO_ 
* que contiene la ubicaci�n de los archivos _DBASE y _EFIMUNI, que contienen encriptados
* los directorios de dbase y sistema
#IFDEF _HIDDEN_INFO_
	_SAFEMODE = .T.
#ELSE
	_SAFEMODE = .F.
#ENDIF

* Clave para encriptaci�n
_SICOKEY = "_0YH*GU42&P" + CHR(255)

* Si no se pasa el archivo de inicializaci�n usa uno que est� en el mismo directorio
_INIFILE = IIF(!EMPTY(m.sINIFile),	m.sINIFile, ".\EFIMUNI.INI")
IF !FILE(_INIFILE)
	=MESSAGEBOX("No se encotr� el archivo de configuraci�n EFIMUNI.INI "+;
	IIF(!EMPTY(sINIFile),"(Pasado como par�metro)","")+CHR(13)+;
	"Informe al administrador",16, "Error de configuraci�n")
	RETURN .F.
ENDIF

SET DEBUG OFF
SET BLOCKSIZE TO 33
SET COLLATE TO 'SPANISH'
SET CONFIRM ON
SET CENTURY ON
SET DATE TO DMY
SET DELETED ON
SET EXACT OFF
SET EXCLUSIVE OFF
SET MARK TO '/'
SET MESSAGE TO 24 CENTER
SET NEAR ON
SET POINT TO '.'
SET PRINT OFF
SET REPROCESS TO 1
SET SAFETY OFF
SET SEPARATOR TO ','
SET CONSOLE OFF
SET DECIMALS TO 2
SET ESCAPE OFF
SET MULTILOCKS ON
SET NOTIFY ON
* SET REFRESH TO 1,1
SET REFRESH TO 0
SET SPACE ON
SET UDFPARMS TO VALUE
SET TALK OFF
SET STRICTDATE TO 0
SET BELL OFF

* VFP
SET LIBRARY TO C50 ADDITIVE
SET STATUS BAR OFF
*SET RESOURCE ON

* ### C/S
#IFDEF _NODEF
	! mode con rate=15 delay=1	
#ENDIF

* VFP : se pasa despu�s del SET PATH
*!*	path_proc = UPPER (GETENV('efimuni')) + 'BIN\EFIPROC'
*!*	* ### C/S
*!*	IF ! EMPTY(GETENV('EFIPROC'))
*!*		path_proc = GETENV('EFIPROC')
*!*	ENDIF
*!*	* SET PROCEDURE TO (path_proc)
*!*	SET PROCEDURE TO (path_proc+".APP")

SET SYSMENU TO

CONFINI()
IF !AMBIENTE()
	RETURN .F.
ENDIF


_SISTEMA = ""
_MODULO  = ""
SET DEFA TO (_EFIMUNI) + (_SISTEMA) + (_MODULO)
path_bin   = TRIM(_efimuni) + 'BIN'
path_dbase = TRIM(_dbase)
path_gral = path_dbase + ';' + path_bin
SET PATH TO (path_gral)

SET PROCEDURE TO EFIPROC.APP

* ### VFP
#IFDEF _NODEF
	path_lib = ALLTRIM(_efimuni) + 'BIN\JKEY'
	SET LIBRARY TO (path_lib) ADDITIVE
	path_lib = ALLTRIM(_efimuni) + 'BIN\NETWARE'
	SET LIBRARY TO (path_lib) ADDITIVE
#ENDIF

SET CONSOLE ON
ON SHUTDOWN DO SALE_VFP		&& ### VFP

_SCAPTION = "EFIMUNI - " + RTRIM(GET_PARA("ASA", "PARTIDO")) + " "
_Screen.Caption = _SCAPTION
_Screen.Icon = "EFIMUNI.ICO"
_Screen.WindowState = 2

OCUL_TBARS()

WAIT WIND "Cargando Sistema ..." NOWAIT NOCLEAR

SET CONSOLE OFF

#IFDEF _NODEF
	IF ! back_tts ()
		RETURN
	ENDIF
#ENDIF

* Verificar si Foxuser especificado es correcto
* Si Foxuser correcto
*   Copiar Foxuser de _EFIMUNI a Foxuser local
* Sino
*   MSG y salir con Return
* FinSi

*!*	PRIVATE m.arch_recu, i, m.proc_ok, m.march_recu, m.pat_recu
*!*	m.arch_recu = SET ('RESO',1)
*!*	DO CASE
*!*	CASE EMPTY (m.arch_recu)
*!*		PRIVATE m.msg_uno
*!*		m.msg_uno = 'No se especific� archivo de recursos FOXUSER.DBF o el mismo ya est� siendo utilizado por otro usuario. Especif�que un FOXUSER.DBF v�lido en su archivo de configuraci�n CONFIG.FP o, si no tiene CONFIG.FP propio, cree un subdirectorio FOX en C: (C:\FOX). '
*!*		m.msg_uno = m.msg_uno + 'Luego de realizar los cambios vuelva a entrar al SIFIM.'
*!*		= MSG (m.msg_uno)
*!*		RETURN
*!*	CASE ! FILE (m.arch_recu)
*!*		= MSG ('El archivo de recursos ' + m.arch_recu + ' especificado no existe. Especif�que un FOXUSER.DBF v�lido en su archivo de configuraci�n CONFIG.FP y vuelva a entrar al SIFIM.')
*!*		RETURN
*!*	ENDCASE
*!*	IF ! FILE (ALLTRIM (_EFIMUNI) + 'FOXUSER.DBF') OR ! FILE (ALLTRIM (_EFIMUNI) + 'FOXUSER.FPT')
*!*		= MSG ('No existe el archivo de recursos FOXUSER.DBF y/o FOXUSER.FPT en el subdirectorio ' + ALLTRIM (_efimuni) + '. No se podr� hacer copia autom�tica del archivo de recursos, de este �ltimo subdirectorio al ' + m.arch_recu + '.')
*!*	ELSE
*!*		IF (FDATE (ALLTRIM (_EFIMUNI) + 'FOXUSER.DBF') > FDATE (m.arch_recu) ) OR ;
*!*			( (FDATE (ALLTRIM (_EFIMUNI) + 'FOXUSER.DBF') = FDATE (m.arch_recu) ) AND ;
*!*			(FTIME (ALLTRIM (_EFIMUNI) + 'FOXUSER.DBF') > FTIME (m.arch_recu) ) )
*!*			m.pat_recu = SUBSTR (m.arch_recu, 1 ,RAT ('\',m.arch_recu))
*!*	*		IF SINO ('El archivo de recursos FOXUSER de ' + ALLT (_EFIMUNI) + ' es m�s nuevo que el de ' + m.pat_recu + CHR(13) + CHR(13) + '� Desea Copiarlo de ' + ALLT(_EFIMUNI) + ' a ' + m.pat_recu + ' ?')
*!*				m.proc_ok = .F.
*!*				FOR i=1 to 3
*!*					IF sin_uso (ALLTRIM (_EFIMUNI) + 'FOXUSER.DBF')
*!*						m.proc_ok = .T.
*!*						EXIT
*!*					ENDIF
*!*				ENDFOR
*!*				IF m.proc_ok
*!*					* El Foxuser es correcto -> Copiar Foxuser de _EFIMUNI a Foxuser local
*!*					*
*!*					m.march_recu = STUFF (m.arch_recu, LEN (ALLT (m.arch_recu))-2, 3, 'FPT')
*!*					SET RESOURCE OFF
*!*					COPY FILE ALLTRIM (_EFIMUNI) + 'FOXUSER.DBF' TO (m.arch_recu)
*!*					COPY FILE ALLTRIM (_EFIMUNI) + 'FOXUSER.FPT' TO (m.march_recu)
*!*					SET RESOURCE ON
*!*				ELSE
*!*					= MSG ('El archivo de recursos FOXUSER.DBF de '+ ALLTRIM (_EFIMUNI) + ' est� siendo usado por otro usuario. No se pudo hacer copia autom�tica del archivo de recursos.')
*!*				ENDIF
*!*	*		ENDIF
*!*		ENDIF
*!*	ENDIF


CLEAR


IF ! PEMSTATUS (_Screen, "imgLogo", 5)
	_Screen.AddObject("imgLogo", "Image")
ENDIF

WITH _Screen.imgLogo
	.Picture = 'SICO.BMP'
	.Top  = _Screen.Height/2 - .Height/2
	.Left = _Screen.Width /2 - .Width /2
	.Visible = .T.
ENDWITH

*!*	path_mac = path_bin + '\ANULAR'
*!*	RESTORE MACROS FROM (path_mac)		&& ### XXX

RESTORE MACROS FROM ("ANULAR")		&& ### XXX

*arch_help = ALLTRIM (_dbase) + 'ASA\AYUDA.DBF'
PUBLIC _cHelpKey,_cHelpCap
_cHelpKey = 'F1'
_cHelpCap = 'F1'
ON KEY LABEL F1 DO HELP_ME

PUBLIC _JEXITKEY, _JDBLCLICK
_JEXITKEY  = 13
_JDBLCLICK = -1

IF FILE(TRIM(_EFIMUNI)+'BIN\PROC_ACT.APP')
	PROC_ACT = 'PROC_ACT'
	DO &PROC_ACT
	PPWPP = TRIM(_EFIMUNI)+'BIN\PROC_ACT.APP'
	DELE FILE &PPWPP
ENDIF

ON ERROR DO errores WITH ERROR(), MESSAGE(), VARREAD(), PROGRAM(), LINENO()

* Desactivaci�n de Teclas
ON KEY LABEL F2 *
ON KEY LABEL F3 *
ON KEY LABEL F5 *
ON KEY LABEL F6 *
ON KEY LABEL F7 *
ON KEY LABEL F8 *
ON KEY LABEL F9 *
ON KEY LABEL F10 *
* ON KEY LABEL CTRL+] * && El Fox no toma la redefinici�n
* ON KEY LABEL CTRL+[ * && El Fox no toma la redefinici�n
ON KEY LABEL CTRL+I *
ON KEY LABEL CTRL+W *
ON KEY LABEL CTRL+J *
ON KEY LABEL CTRL+ENTER *
ON KEY LABEL CTRL+M *
ON KEY LABEL CTRL+END *
ON KEY LABEL CTRL+Q *
ON KEY LABEL ALT+Q DO CANCELAR IN INICIALV	&& ### VFP

* Activaci�n de teclas
*!*	ON KEY LABEL f4 DO VerAmbien
*!*	ON KEY LABEL 'CTRL+F12' DO DEBUG

sistema = ""
modulo  = ""
programa= ""
menu    = ""
tipo_inic = ""

SET CLASSLIB TO _EFIMUNI+'BIN\SICOCLAS.VCX'	&& ### VFP

WAIT CLEAR

DO FORM LOGIN
READ EVENTS
* El c�digo que sigue se pas� al form de login
*!*	IF param_usr ()
*!*		DO CASE
*!*		CASE tipo_inic = "MENU"
*!*			* prinmenu = sismenu (sistema, menu, 3)	### VFP
*!*			prinmenu = SISMENUV (sistema, menu, 3)
*!*			
*!*			* HIDE WINDOW COMMAND 	&& ### VFP
*!*			
*!*			sm_prim_vez = .T.
*!*			DO WHILE .T. AND (SET ('DEBUG') = 'OFF' OR sm_prim_vez)
*!*				* ACTIVATE POPUP (prinmenu)	### VFP
*!*				ACTIVATE MENU (prinmenu)
*!*				sm_prim_vez = .F.
*!*			ENDDO
*!*			
*!*			READ EVENTS
*!*		CASE tipo_inic = "PAD "
*!*			* DO sismenu with sistema, menu	### VFP
*!*			DO SISMENUV with sistema, menu
*!*		CASE tipo_inic = "PROG"
*!*	    	aejec = sistema + '\' + modulo + '\' + programa
*!*			WAIT CLEAR
*!*			DO &aejec
*!*		OTHERWISE
*!*			=MSG ("El usuario " + ALLTRIM (_USUARIO) + " no tiene seteada una opci�n de arranque correcta")
*!*		ENDCASE
*!*	ELSE
*!*		WAIT WINDOW "No hay configuraci�n para el Usuario"
*!*	ENDIF


*************************************************
FUNCTION param_usr
*************************************************
	=USET ('asa\usr')
	SELECT usr
	exp_usr = GETKEY ('usr', .T.)
	
	m.usuario = _USUARIO
	IF SEEK (EVAL (exp_usr)) AND ! EMPTY (EVAL (exp_usr))
		sistema = ALLT (sis_inic)
		modulo  = ALLT (mod_inic)
		programa= ALLT (prog_ini)
		menu    = ALLT (men_inic)
		tipo_inic = arranque
		USE
		RETURN .T.
	ELSE
		USE
		RETURN .F.
	ENDIF
	
*************************************************
FUNCTION confini
*************************************************
* Funcionamiento:
* Par�metros:

****** PROGRAMA PARA VERIFICAR CANTIDAD DE FILES EN EL CONFIG.SYS ******

arch = 100

DEFINE WINDOW infoconfig FROM 5,9 TO 18,69 DOUBLE SHADOW COLOR SCHEME 5

IF VAL(SYS(2010)) < arch

IF FILE('c:\config.sys')

	IF VAL(SYS(2010)) < arch
		ACTIVATE WINDOW infoconfig
		@ 1,2 SAY "     No son suficientes los FILES (archivos) en el      "
		@ 2,2 SAY "    archivo CONFIG.SYS para que funcione el sistema.    "
		@ 3,2 SAY "             Estos deben ser como m�nimo " + ALLTRIM(STR(arch)) + "."
		@ 6,2 SAY "             Desea realizar el cambio usted             "
		@ 7,2 SAY "          o que el Sistema lo haga autom�tico?          "
		@ 9,15 GET cambio PICTURE"@*HT \?\<Autom�tico;\<Manual" ;
			SIZE 1,12,3 ;
			DEFAULT 1 ;
			MESSAGE "< Autom�tico >:cambio autom�tico  -  < Manual >:usted realiza el cambio"
		READ CYCLE MODAL
		CLEAR
		DEACTIVATE WINDOW infoconfig
		
		IF cambio = 2
			
			=MSG ("Se ha elegido la opci�n de cambio manual, por lo tanto usted volver� al DOS donde deber� editar el archivo CONFIG.SYS y asegurarse de que aparezca en �ste la sentencia FILES = " + ALLTRIM(STR(arch)) + ".")

			QUIT
		ELSE
			IF FILE ('C:\CONFIG.SIC')
				DELETE FILE 'C:\CONFIG.SIC'
			ENDIF	
			RENAME 'C:\CONFIG.SYS' TO 'C:\CONFIG.SIC'
			wviejo = FOPEN ('C:\CONFIG.SIC')
			wnuevo = FCREATE ('C:\CONFIG.SYS')
			= FSEEK (wviejo,0,0)
			= FSEEK (wnuevo,0,0)
			DO WHILE !FEOF (wviejo)
				ls = FGETS (wviejo)
				IF ATC('files',ls) = 1
					ls = 'FILES = ' + ALLTRIM (STR (arch))
				ENDIF
				= FPUTS (wnuevo, ls)
			ENDDO
			= FCLOSE (wnuevo)
			= FCLOSE (wviejo)
			
			=MSG ("El Sistema ha modificado la cantidad de FILES necesarios en forma autom�tica. El CONFIG.SYS original se renombr� como C:\CONFIG.SIC .  Para que estas modificaciones entren en efecto elija <Acepta> y luego reinicie el sistema.")
			QUIT
			
		ENDIF
	ENDIF
ELSE
	=MSG ("El archivo C:\CONFIG.SYS no existe. Cuando haya salido del Sistema deber� crearlo con la sentencia FILES = " + ALLTRIM(STR(arch)) + ".")
	QUIT
ENDIF

ENDIF

RETURN


* Cancela la ejecuci�n del programa
PROCEDURE CANCELAR

* CLEAR ALL
ON KEY
SET SYSMENU TO DEFAULT
CLEAR MENUS
SET CLASSLIB TO
SET HELP TO
ON ERROR

CANCEL

RETURN

***



***************************************************
FUNCTION LOG_OUT
***************************************************
DEACTIVATE MENU BARRA
_SCREEN.Caption = _SCAPTION
SET SYSMENU TO
AR_ACTION('X')
DO FORM LOGIN
RETURN



*************************************************
FUNCTION ambiente
*************************************************
PUBLIC _dbase, _efimuni, _usuario, _wkstation, _dbasechk
PUBLIC _server, _EJERC
* Estas variables se utilizan para que aparezca una vez sola la ventana de impresi�n
PUBLIC _previmp, _curopc, _capimp, _finimp, _nomdrv, _copias, _nomarch
PUBLIC _si_v_imp, _tit_v_repo, _pantarch, _esc_ini, _esc_fin, _secuencia
PUBLIC _longpag, _nomrep, _nomimp
PUBLIC _sel_prn_nom		&& ### VFP
PRIVATE oReg

_previmp   = 'S'
_curopc    = 1
_capimp    = ''
_finimp    = ''
_nomdrv    = ''
_copias    = 1
_nomarch   = ''
_pantarch  = ''
_si_v_imp  = 'S'
_tit_v_repo= ''
_esc_ini   = ''
_esc_fin   = ''
_secuencia = ''
_longpag	 = 72
_nomrep		 = ''
_nomimp		 = ''
_sel_prn_nom = ''	&& ### VFP
	
oReg = NewObject("OldIniReg","Registry.prg")

IF _SAFEMODE = .T.
	IF FILE(_HIDDEN_INFO_ + "_DBASE")
		_DBASE = ENCRYPT(FILETOSTR(_HIDDEN_INFO_ + "_DBASE"), _SICOKEY)
	ELSE
		_DBASE = ""
	ENDIF
ELSE
	_DBASE = UPPER(GET_CFG('dbase'))
ENDIF
IF !DIRECTORY(_DBASE)
	=MESSAGEBOX("La ubicaci�n de la base de datos no es v�lida. "+CHR(13)+;
	"Informe al administrador",16, "Error de configuraci�n")
	RETURN .F.
ENDIF

IF _SAFEMODE = .T.
	IF FILE(_HIDDEN_INFO_ + "_DBASECHK")
		_DBASECHK = ENCRYPT(FILETOSTR(_HIDDEN_INFO_ + "_DBASECHK"), _SICOKEY)
	ELSE
		_DBASECHK = ""
	ENDIF
ELSE
	_DBASECHK = UPPER(GET_CFG('dbasechk'))
ENDIF
IF !DIRECTORY(_DBASECHK)
	=MESSAGEBOX("La ubicaci�n de la base de datos de chequeo no es v�lida. "+CHR(13)+;
	"Informe al administrador",16, "Error de configuraci�n")
	RETURN .F.
ENDIF

IF _SAFEMODE = .T.
	IF FILE(_HIDDEN_INFO_ + "_EFIMUNI")
		_EFIMUNI = ENCRYPT(FILETOSTR(_HIDDEN_INFO_ + "_EFIMUNI"), _SICOKEY)
	ELSE
		_EFIMUNI = ""
	ENDIF
ELSE
	_EFIMUNI = UPPER(GET_CFG('efimuni'))
ENDIF
IF !DIRECTORY(_EFIMUNI)
	=MESSAGEBOX("La ubicaci�n del sistema no es v�lida. "+CHR(13)+;
	"Informe al administrador",16, "Error de configuraci�n")
	RETURN .F.
ENDIF

_SERVER    = UPPER(GET_CFG('server'))
_EJERC     = UPPER(GET_CFG('EJERC'))
_USUARIO   = ""

_wkstation = ""
oReg.GetRegKey("ComputerName", @_wkstation,;
"System\CurrentControlSet\control\ComputerName\ComputerName", -2147483646)

PUBLIC _sistema, _modulo, _programa
_sistema   = UPPER (GET_CFG('sistema'))
_modulo    = UPPER (GET_CFG('modulo'))
_programa  = UPPER (GET_CFG('programa'))

RETURN .T.

*************************************************
FUNCTION GET_CFG
*************************************************
LPARAMETERS sVariable, sINILabel
LOCAL xRet

IF EMPTY(sINILabel)
	sINILabel = "Settings"
ENDIF

sIniFile = IIF(EMPTY(_INIFILE), ".\EFIMUNI.INI", _INIFILE)
IF TYPE("oReg") <> 'O'
	oReg = NewObject("OldIniReg","Registry.prg")
ENDIF
oReg.GetIniEntry(@xRet, sINILabel, sVariable, sIniFile)

IF EMPTY(xRet)
	xRet = GETENV(sVariable)
ENDIF

RETURN xRet


*************************************************
FUNCTION GETFORM
*************************************************
* Busca un form dentro de _SCREEN.Forms y devuelve una referencia al mismo o .NULL.
LPARAMETERS sFormName
LOCAL nPos, oRet

oRet = .NULL.
FOR nPos = 1 TO _SCREEN.FormCount
	IF UPPER(_SCREEN.Forms[nPos].Name) = UPPER(sFormName)
		oRet = _SCREEN.Forms[nPos]
		EXIT
	ENDIF
ENDFOR
RETURN oRet


*************************************************
FUNCTION AR_ACTION
*************************************************
* Realiza una acci�n sobre la barra de accesos r�pidos
LPARAMETERS sAction
LOCAL nPos, oRef

oRef = GETFORM("AccessBar")
DO CASE
CASE sAction = 'S'		&& Switch visible/no visible
	IF ISNULL(oRef)
		DO FORM Accesos
	ENDIF
CASE sAction = 'C'		&& Configurar
	IF ISNULL(oRef)
		DO FORM Accesos
		oRef = GETFORM("AccessBar")
	ENDIF
	oRef.AccessConfig
CASE sAction = 'X'		&& Cerrar
	IF !ISNULL(oRef)
		oRef.Release()
	ENDIF	
ENDCASE

RETURN