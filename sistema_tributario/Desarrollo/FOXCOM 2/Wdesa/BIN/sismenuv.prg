* Sismenu para VFP

PARAMETERS p_sistema, p_menu, tipo_menu

PRIVATE clave, clavep

IF PCOUNT() < 2
	WAIT WINDOW "Necesita dos par�metros para correr"
	RETURN
ENDIF

seek_again = .F.
IF TYPE ('_EFITEST') <> 'U'
	seek_again = .T.
ENDIF

IF PCOUNT() < 3
	tipo_menu = 0
ELSE
	IF TYPE ('tipo_menu') = 'C'
		PRIVATE on_err, error
		error  = .F.
		on_err = ON('ERROR')
		prmenu = ALLT (p_menu)
		ON ERROR error = .T.
		MOVE POPU (prmenu) TO 2,2
		ON ERROR &on_err
		if error = .F.
			IF TYPE('_usuario') = 'C' AND UPPER(_usuario) = 'EL_PACHU'
				ACTI POPU (prmenu) NOWAIT
				DO MUEVEMENU
			ELSE
				ACTI POPU (prmenu)
			ENDIF
			RETURN
		ENDIF
		ptit_menu = tipo_menu
		tipo_menu = 2
	ELSE
		tipo_menu = 1
	ENDIF
ENDIF

* deact popu all

PUBLIC m.done
m.done = .F.

p_sistema = PADR (UPPER(p_sistema),8)
p_menu    = PADR (UPPER(p_menu),10)
menu_prin = TRIM (p_menu)
m.menu_num = 0

gmenu_fila = 0 && fila de menues actual
gmenu_col  = 15 && col  de menues actual
*************************************************
* Con menu_fila,menu_col se mantiene la posici�n
* del menu dentro de cada llamada a la funci�n
* defipop, gmenu_fila y gmenu_col se usan para
* saber la posici�n actual globalmente.
*************************************************

clave  = 'PADR (m.sistema,8) + PADR (m.menu,10) + STR (m.menu_num,2)'
clavep = 'PADR (m.sistema,8) + PADR (m.menu,10)'

IF !USED ('SISMENU')
	=USET('asa\sismenu')
ENDIF
SELECT sismenu
SET ORDER TO PRIMARIO

WAIT CLEAR
WAIT WINDOW "Cargando Men�es ..." NOWAIT NOCLEAR

PRIVATE m.menu_fijo
m.menu_fijo = IIF (GET_PARA ("ASA", "MENU")=="S", .T., .F.)

tipo_menu = 0

IF tipo_menu = 0
	prmenu =defipad (p_sistema, p_menu)
	=defipop (p_sistema, p_menu)
	USE IN sismenu
	WAIT CLEAR
	RETURN (prmenu)
ELSE
	IF tipo_menu = 2
		gmenu_fila = 2
		gmenu_col  = 2
		menu_prin='NoExiste'
		DEFINE POPUP (prmenu) FROM 2,2 MARGIN RELATIVE
		
		defipop(p_sistema, p_menu)
		USE IN sismenu
		WAIT CLEAR
		ACTI POPU (prmenu)
	ELSE
		prmenu=defimenu (p_sistema, p_menu)
		USE IN sismenu
		WAIT CLEAR
		RETURN prmenu
	ENDIF
ENDIF

WAIT CLEAR
RETURN

*************************************************

*************************************************
FUNCTION defipad
*************************************************
	PARAMETERS m.sistema, m.menu
	EXTERNAL PROCEDURE LOG_OUT
	PRIV    m.sistema, m.menu, m.menu_num, encontro, clave_par
	PRIV    pmenu_hijo, ptit_menu, skip_for, tecla, pcomando, barpad

	PRIVATE m.menu_tran, m.menu_op

	m.menu_num = 0
	encontro = .F.

	SET SYSMENU TO
	* SET SYSMENU AUTOMATIC
	SET SYSMENU ON
	
	* barpad = '_MSYSMENU'
	barpad = 'BARRA'
	IF barpad <> '_MSYSMENU'
		* DEFINE MENU (barpad)
		DEFINE MENU (barpad) BAR
	ENDIF
	
	* Se cambi� el men� completo Varios por la opci�n de solo salir
	DEFINE PAD varios_in OF (barpad) PROMPT "\<Sistema" COLOR SCHEME 3
	ON     PAD varios_in OF (barpad) ACTIVATE POPUP varios_in
	DEFINE POPUP varios_in MARGIN RELATIVE
	DEFINE  BAR 1 OF varios_in PROMPT "\<Cerrar la sesi�n"
	DEFINE  BAR 2 OF varios_in PROMPT "\-"
	DEFINE  BAR 3 OF varios_in PROMPT "\<Salir de EFI-MUNI"
	ON SELECTION BAR 1 OF varios_in DO LOG_OUT
	ON SELECTION BAR 3 OF varios_in DO salir_menu with 0

	SEEK EVAL (clave)

	DO WHILE m.sistema = sistema and m.menu = menu AND !EOF()

		pmenu_hijo = TRIM (menu_hijo)
		ptit_menu  = TRIM (tit_menu)
		ptecla     = tecla
		IF !EMPTY (skip_for)
			pskip_for  = DEL_CHR (skip_for)
		ENDIF

		pmensaje   = DEL_CHR (mensaje)
		pnom_tecla = nom_tecla

		IF pmenu_hijo == ''
			SKIP
			LOOP
		ENDIF

		IF ! EMPTY (ptecla)
			deftecla = "KEY " + ptecla
			IF ! EMPTY (pnom_tecla)
				deftecla = deftecla + ",'" + pnom_tecla + "'"
			ENDIF
		ELSE
			deftecla = ""
		ENDIF

		ptit_menu = STRTRAN(ptit_menu, "Sistema de ")
		ptit_menu = STRTRAN(ptit_menu, "\<")
		
		DIMENSION menu_tran[06,2]
		menu_tran[01,1] = "Recursos Humanos"
		menu_tran[01,2] = "RR\<HH"
		menu_tran[02,1] = "Auditor�a y Segurid"
		menu_tran[02,2] = "\<Audit.y Seg."
		menu_tran[03,1] = "Gesti�n de Asuntos"
		menu_tran[03,2] = "\<Gest.Asuntos"
		menu_tran[04,1] = "Sistema Control de Integridad"
		menu_tran[04,2] = "Control \<Integridad"
		menu_tran[05,1] = "Administ. de Bienes"
		menu_tran[05,2] = "\<Admin.Bienes"
		menu_tran[06,1] = "Planific y Control"
		menu_tran[06,2] = "\<Planif.y Control"
			
		FOR m.menu_op = 1 TO ALEN(m.menu_tran,1)
			ptit_menu = ALLTRIM(STRTRAN(ptit_menu, m.menu_tran[m.menu_op,1], m.menu_tran[m.menu_op,2]))
		NEXT
		
		DEFINE PAD (pmenu_hijo) OF (barpad) PROMPT ptit_menu ;
			COLOR SCHEME 3 MESSAGE pmensaje &deftecla
			
		IF lanza = 'P'
			ON PAD (pmenu_hijo) OF (barpad) ACTIVATE POPUP (pmenu_hijo)

			DEFINE POPUP (pmenu_hijo) MARGIN RELATIVE
		ELSE
			if !EMPTY (pcomando)
				pcomando = DEL_CHR (comando)
				sm_pos_ext = RAT('.', pcomando)
				IF sm_pos_ext <> 0
					s_pcomando = SUBSTR (pcomando, 1, sm_pos_ext-1)
					pcomando = STUFF(pcomando, sm_pos_ext, 4, '')
				ELSE
					s_pcomando = pcomando
				ENDIF
				pos_do = ATC ('DO',pcomando)
				IF pos_do <> 0
					IF seek_again
						sa_comando = s_pcomando
						s_pcomando = ALLTRIM (_efitest) + ALLTRIM(substr(s_pcomando, pos_do+2)) + '.APP'
						IF ! FILE(s_pcomando)
							s_pcomando = ALLTRIM (_efimuni) + ALLTRIM(substr(sa_comando, pos_do+2)) + '.APP'							
							IF !FILE(s_pcomando)
								pcomando = 'WAIT WIND ' + '"' + 'No existe el programa: ' + s_pcomando + '"'
							ENDIF
						ENDIF
					ELSE
						s_pcomando = ALLTRIM (_efimuni) + ALLTRIM(substr(s_pcomando, pos_do+2)) + '.APP'
						IF ! FILE(s_pcomando)
							pcomando = 'WAIT WIND ' + '"' + 'No existe el programa: ' + s_pcomando + '"'
						ENDIF
					ENDIF
				ENDIF

				* ### VFP
				* m.pcomando = 'DO OPCION_MENU IN SISMENUV WITH ['+m.pcomando+']'
				m.pcomando = 'DO OPCION_MENU IN SISMENUV WITH ['+m.pcomando+'], '+IIF(m.menu_fijo, [.T.],[.F.])
				ON SELECTION PAD (pmenu_hijo) OF (barpad) (pcomando)
			ELSE
				pcomando = 'WAIT WIND "Opci�n definida en blanco en la tabla de men�es." '
				ON SELECTION PAD (pmenu_hijo) OF (barpad) (pcomando)
			ENDIF			
			* al salir del programa activa el f1 general para que este activo en el menu
			ON KEY LABEL F1 help
			*se cuelga programa al salir. verlo
			*SET HELPFILTER TO sistema = m.sistema

		ENDIF
		
		SKIP
		if  !encontro
			encontro = .T.
		ENDIF

		* ### XXX
		* EXIT
	ENDDO
	
	if !encontro
		WAIT WINDOW "No encontr� el PAD " + m.sistema + ' ' + m.menu
	ENDIF

	MENU_AYUDA()

	* ###> Mod01
	* if barpad <> '_MSYSMENU'
	* 	SHOW MENU (barpad)
	* 	ACTIVATE MENU (barpad)
	* ENDIF
	
	* Armo men� como default
	SET SYSMENU SAVE
	
	RETURN (barpad)


*************************************************
FUNCTION defipop
*************************************************
* Funcionamiento:
*  Define un popup desde la tabla sismenu.
*
* Par�metros:
*  sistema y menu
*
	PARAMETERS m.sistema, m.menu
	PRIV    m.menu_num, encontro
	PRIV    pmenu, pmenu_hijo, ptit_menu, pmenu_num, pcomando, ptecla, pnom_tecla, pskip_for, menu_fila, menu_col, menu_filai, menu_coli

	m.menu_num = 0
	encontro = .F.

	SEEK EVAL (clavep)

	* menu_filai y menu_coli mantiene la posici�n inicial
	menu_filai= gmenu_fila
	menu_coli = gmenu_col

	menu_fila = gmenu_fila
	menu_col  = gmenu_col + 2 && + 15  modi por leo 31/10

	* VFP
	IF ALLTRIM(m.sistema) = "CONTA" AND !(_EJERC $ _SCREEN.Caption)
		_SCREEN.Caption = _SCAPTION + "(Ejercicio Contable " + _EJERC + ")"
	ENDIF

	DO WHILE m.sistema = sistema and m.menu = menu and !EOF()

		pmenu      = TRIM (menu)
		pmenu_hijo = TRIM (menu_hijo)
		ptit_menu  = TRIM (tit_menu)
		pmenu_num  = menu_num
		ptecla     = tecla
		pnom_tecla = nom_tecla
		IF !EMPTY (skip_for)
			pskip_for  = DEL_CHR (skip_for)
		ELSE
			pskip_for = '.F.'
		ENDIF
		m.menu_num = menu_num
		m.mensaje  = DEL_CHR (mensaje)

		IF pmenu == menu_prin
			DO defipop with sis_hijo, menu_hijo
			SEEk EVAL (clave)
		ELSE
			IF ! EMPTY (ptecla)
				deftecla = "KEY " + ptecla
				IF ! EMPTY (pnom_tecla)
					deftecla = deftecla + ",'" + pnom_tecla + "'"
				ENDIF
			ELSE
				deftecla = ""
			ENDIF

			* menu_fila  = gmenu_fila
			* gmenu_fila = menu_fila
			gmenu_col  = menu_col
			
			* ### VFP
			DEFINE BAR pmenu_num OF (pmenu) PROMPT ptit_menu MESSAGE m.mensaje &deftecla ;
				SKIP FOR RDLEVEL() <> 0 COLOR SCHEME 4 && SKIP FOR &pskip_for

			IF lanza = 'P'
				IF TYPE ('pmenu_num')  == 'N' AND ;
					 TYPE ('pmenu')      == 'C' AND ;
					 TYPE ('pmenu_hijo') == 'C'

					ON BAR pmenu_num OF (pmenu) ACTIVATE POPUP (pmenu_hijo)
					gmenu_fila = menu_filai+1 && modi por leo 31/10

					DEFINE POPUP (pmenu_hijo) MARGIN RELATIVE 
					
					DO defipop with sis_hijo, menu_hijo
				
					SEEK EVAL (clave)
				ELSE
					=MSG ('Indefinici�n en la tabla de Men�es: ' + pmenu + '->' + ;
						pmenu_hijo )
				endif
			ELSE
				pcomando = DEL_CHR (comando)
				if !EMPTY (pcomando)
					sm_pos_ext = RAT('.', pcomando)
					IF sm_pos_ext <> 0
						s_pcomando = SUBSTR (pcomando, 1, sm_pos_ext-1)
						pcomando = STUFF(pcomando, sm_pos_ext, 4, '')
					ELSE
					    s_pcomando = pcomando
					ENDIF
					pos_do = ATC ('DO',pcomando)
					IF pos_do <> 0
						s_pcomando = ALLTRIM (_efimuni) + ALLTRIM(substr(s_pcomando, pos_do+2)) + '.APP'
						IF ! FILE(s_pcomando)
							pcomando = 'WAIT WIND ' + '"' + 'No existe el programa: ' + s_pcomando + '"'
						ENDIF
					ENDIF

					m.pcomando = 'DO OPCION_MENU IN SISMENUV WITH ['+m.pcomando+'], '+IIF(m.menu_fijo, [.T.],[.F.])
					ON SELECTION BAR pmenu_num OF (pmenu) &pcomando
				ELSE
					pcomando = 'WAIT WIND "Opci�n definida en blanco en la tabla de men�es." '
					ON SELECTION BAR pmenu_num OF (pmenu) &pcomando
				ENDIF
			ENDIF
		ENDIF
		gmenu_col  = menu_col

		SKIP
	ENDDO


	* restablece la posici�n inicial
*       gmenu_fila= menu_filai
	gmenu_col = menu_coli


*************************************************
FUNCTION defimenu
*************************************************

	PARAMETERS m.sistema, m.menu
	PRIV    encontro
	PRIV    prmenu, pmenu_hijo, ptit_menu, pcomando, pskip_for, items, m.menu_num

	items = 0
	m.menu_num = 0
	SEEK EVAL (clavep)

	* prmenu se define como el menu principal mas '_' para evitar colisiones con el _MSYSMENU
	prmenu = menu_prin + '_'

	DO WHILE m.sistema = sistema and m.menu = menu and !EOF()
		items = items + 1
		SKIP
	ENDDO

	m.menu_num = 0
	SEEK EVAL (clavep)

	* menu inicial puso en mayusculas gabriel 021195

	DEFINE POPUP (prmenu) FROM 1, 0 TO 2 + items+1,38 ;
		TITLE PADR ("MENU INICIAL", 33, " ") SCROLL MARGIN RELATIVE SHADOW COLOR SCHEME 4
		
	DO WHILE m.sistema = sistema and m.menu = menu and !EOF()

		pmenu_hijo = TRIM (menu_hijo)
		ptit_menu  = TRIM (tit_menu)
		ptecla     = tecla
		pnom_tecla = nom_tecla
		IF !EMPTY (skip_for)
			pskip_for  = DEL_CHR (skip_for)
		ELSE
			pskip_for = '.F.'
		ENDIF
		m.menu_num = menu_num

		IF EMPTY (mensaje)
			m.mensaje = " "
		ELSE
			m.mensaje = DEL_CHR (mensaje)
		ENDIF
		
										&& RDLEVEL() <> 0
		DEFINE BAR menu_num OF (prmenu) PROMPT ptit_menu SKIP FOR .F.  COLOR SCHEME 4 MESSAGE m.mensaje && SKIP FOR &pskip_for
		
		IF lanza = 'P'
			IF tipo_menu = 4
				ON BAR menu_num OF (prmenu) ACTIVATE POPUP (pmenu_hijo)
				* verif
				gmenu_fila = 2 && menu_num+2 modi por leo 31/10
				gmenu_col  = 2 && agregado por leo 31/10
			
				DEFINE POPUP (pmenu_hijo) RELATIVE
				DO defipop with sis_hijo, menu_hijo
				SEEK EVAL (clave)
			ELSE
				pcomando = 'DO SISMENUV with "' + sis_hijo + '","' + menu_hijo + '","' + ptit_menu + '"'
				ON SELECTION BAR menu_num OF (prmenu) &pcomando
			ENDIF
		ELSE
			M.COMANDO	= DEL_CHR (comando)
			if ! EMPTY(M.COMANDO)
				sm_pos_ext = RAT('.', M.comando)
				IF sm_pos_ext <> 0
					s_pcomando = SUBSTR (M.comando , 1, sm_pos_ext-1)
					pcomando = STUFF(M.comando, sm_pos_ext, 4, '')
				ELSE
					s_pcomando = M.comando
				ENDIF
				pos_do = ATC ('DO',pcomando)
				IF pos_do <> 0
					s_pcomando = ALLTRIM (_efimuni) + ALLTRIM(substr(s_pcomando, pos_do+2)) + '.APP'
					IF ! FILE(s_pcomando)
						pcomando = 'WAIT WIND ' + '"' + 'No existe el programa: ' + s_pcomando + '"'
					ENDIF
				ENDIF

				* ### VFP
				* m.pcomando = 'DO OPCION_MENU IN SISMENUV WITH ['+m.pcomando+']'
				m.pcomando = 'DO OPCION_MENU IN SISMENUV WITH ['+m.pcomando+'], '+IIF(m.menu_fijo, [.T.],[.F.])
				ON SELECTION BAR menu_num OF (prmenu) &pcomando
			ELSE
				pcomando = 'WAIT WIND "Opci�n definida en blanco en la tabla de men�es." '
				ON SELECTION BAR menu_num OF (prmenu) &pcomando
			ENDIF
		ENDIF
		SKIP

	ENDDO

	DEFINE  BAR items+2 OF (prmenu) PROMPT "\<Salir del Sistema" SKIP FOR RDLEVEL() <> 0 COLOR SCHEME 4
	ON SELE BAR items+2 OF (prmenu) DO salir_menu with 0 && ACTIVATE POPUP varios_in

	RETURN prmenu


***********************************************

*************************************************
FUNCTION MUEVEMENU
*************************************************
PRIVATE retorno, j, z, i
retorno = .T.
J = 12
Z = 40
FOR I = 1 TO 1000
	J = J + RAND () * 2 - 1
	Z = Z + RAND () * 5 - 2.5
	MOVE POPU (prmenu) TO J, Z
ENDF
RETURN retorno


*********************
FUNCTION ASTR (num)
*********************
RETURN (ALLTRIM(STR(m.num)))


****************************
FUNCTION STRZERO (num, largo)
****************************
RETURN PADL(ASTR(m.num), m.largo, "0")


* Ejecuta una opci�n de men�
*********************************************
PROCEDURE OPCION_MENU (m.opcion, m.menu_fijo)
*********************************************
LOCAL m.pop, m.bar

* &opcion
* SET SYSMENU TO DEFAULT

m.pop = POPUP()
m.bar = BAR()

DEACTIVATE MENU barra
SET SYSMENU OFF
&opcion
SET SYSMENU ON
SET SYSMENU TO DEFAULT

IF m.menu_fijo
	ACTIVATE POPUP (m.pop) BAR (m.bar)
ENDIF

RETURN


******************************************
FUNCTION MENU_AYUDA()
******************************************

PRIVATE m.selant
USET("ASA\SISTEMAS","PRIMARIO","SIS_TMP")

IF TYPE("SIS_TMP.HELP_FILE") <> 'U'
	m.selant = SELECT()
	SELECT SIS_TMP

	DEFINE PAD ayuda OF (barpad) PROMPT "A\<yuda"
	ON     PAD ayuda OF (barpad) ACTIVATE POPUP pad_ayuda
	DEFINE POPUP pad_ayuda MARGIN RELATIVE

	m.nropad = 1
	SCAN FOR !EMPTY(SIS_TMP.HELP_FILE)
		IF FILE(ALLTRIM(SIS_TMP.HELP_FILE) + ".HLP") OR FILE(ALLTRIM(SIS_TMP.HELP_FILE) + ".CHM")
			m.auxarg = ["] + ALLTRIM(SIS_TMP.SISTEMA) + ["]
			DEFINE BAR m.nropad OF pad_ayuda PROMPT ALLTRIM(SIS_TMP.DESCR_SIS)
			ON SELECTION BAR m.nropad OF pad_ayuda DO help_me WITH &auxarg
			m.nropad = m.nropad + 1
		ENDIF
	ENDSCAN

	IF !EMPTY(m.selant)
		SELECT (m.selant)
	ENDIF
	SET HELP ON
ENDIF

USE IN SIS_TMP
RETURN