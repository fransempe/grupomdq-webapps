*************************************************
FUNCTION TxtRegHs
*************************************************
* 
* Funcionamiento: Imprime una leyenda en el recibo
* de sueldos en funci�n de un valor en el campo
* multiprop�sito 3 de la tabla de agentes.
* 
* Es especial para Municipalidad de Azul.
* 
* 
PRIVATE retorno
retorno = ''

= Seek (Str (cagentes.nro_legajo, 6, 0) + Str (cagentes.nro_cargo, 3, 0), 'agentes')

DO CASE
	CASE agentes.multprop3 = 0
		retorno = ''
	CASE agentes.multprop3 = 20
		retorno = 'R�gimen horario = 40 hs. semanales'
	CASE agentes.multprop3 = 30
		retorno = 'R�gimen horario = 42 hs. alt./48 hs. corridas'
	CASE agentes.multprop3 = 50
		retorno = 'R�gimen horario = 48 hs. alternadas'
ENDCASE

RETURN retorno

