LPARAMETERS m.texto, m.passwd
LOCAL sText, sPass, nPos, sRet, sLib

m.texto = ALLTRIM(m.texto)
m.passwd = ALLTRIM(m.passwd)

sLib = SET("LIBRARY")
*SET LIBRARY TO CIPHER50 ADDITIVE
SET LIBRARY TO C50 ADDITIVE

sText = ALLTRIM(m.texto)
FOR nPos = LEN(ALLTRIM(m.texto)) TO 1 STEP -1
	sText = sText + SUBSTR(m.texto, nPos, 1)
ENDFOR
sPass = ALLTRIM(m.passwd)
FOR nPos = LEN(ALLTRIM(m.passwd)) TO 1 STEP -1
	sPass = sPass + SUBSTR(m.passwd, nPos, 1)
ENDFOR

sRet = ENCRYPT(sText,sPass)
SET LIBRARY TO &sLib
RETURN sRet
