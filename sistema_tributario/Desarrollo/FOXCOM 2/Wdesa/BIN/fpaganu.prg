*************************************************
* FUNCTION FPagAnu
*************************************************
* 
* Funcionamiento:
* Determina si un imponible pag� la cuota anual.
* 
PARAMETERS recurso, tipo_imp, nro_imp
PRIVATE retorno
retorno = .T.

IF !USED ('rec_ccc')
	UseT ('recur\rec_ccc')
ENDIF

IF SEEK (m.recurso + m.tipo_imp + STR (m.nro_imp, 10, 0) + STR (m.anio, 4, 0) + ' 13', 'rec_ccc')
	IF rec_ccc.est_mov = 'NO' AND VAL (rec_ccc.imp_mov) > 0
		retorno =.F.
	ENDIF
ENDIF

RETURN retorno
