

FUNCTION R1784Fil
*
* Función que determina si se debe retener
* por Resolución General 1784 de la AFIP 
* Regimen general del SUSS

PRIVATE retorno

IF  type ('proveed.ret_rg1784') = "C" AND proveed.ret_rg1784 = "S"
	retorno = .T.
ELSE
	retorno = .F.
ENDIF

RETURN retorno
