* Utilizo el Toolbar de control
#DEFINE TOOLBAR_CONTROL

* Utilizo la funci�n SINO en modo MESSAGEBOX
#DEFINE SINO_MSGB

* Utilizo el Form de Impresi�n
#DEFINE FORM_IMPRIME

* Utilizo el Form de Browse
#DEFINE FORM_BROWSE

* Utilizo el Form de MESSAGEBOX
#DEFINE FORM_MSGB


#INCLUDE "w:\-desa\lib\efiproc.h"


*!*	* Columnas de _SQL_TAB_INFO
*!*	#define TI_TABLA		1
*!*	#define TI_ORD_ACT		2
*!*	#define TI_TAG_ACT		3
*!*	#define TI_SQL_ACT		4
*!*	#define TI_INDICES		5
*!*	#define TI_COLUMNAS		30

EXTERNAL ARRAY _SQL_OPEN

IF .F.
	_REFOX_ = (9876543210)
	_REFOX_ = (9876543210)
ENDIF

* ###> Mod01: n vistas filtradas diferentes por cada orden
* ###> Mod02: Vuelta a una vista por cada orden (SQL Server 6.5)
* ###> Mod03: Ahora dos vistas por cada tabla del motor: 
*				una con conexi�n exclusiva y otra compartida
* ###> Mod04: Soporte de expresiones con "@" para interpretar literalmente y b�squeda parcial de campos clave

*

*!*	*************************************************
*!*	FUNCTION BusqSecu
*!*	*************************************************
*!*	* Autor : Carlos
*!*	* Dise�o: Carlos
*!*	* 
*!*	* Fecha : 20/08/98
*!*	* 
*!*	* Funcionamiento:
*!*	* Realiza una b�squeda secuencial sobre el browse del in_tabla.
*!*	* Es llamada por el in_tabla, al presionar la tecla Home.
*!*	* 
*!*	* 
*!*	PUBLIC _busqsecue
*!*	PRIVATE campo, busqsecue, reg_ant

*!*	* ### C/S
*!*	IF TYPE("nindice") # "C"
*!*		RETURN(.T.)		&& No estoy en un IN_TABLA
*!*	ENDIF

*!*	* Desactiva Jkey
*!*	IF ! EMPTY(nindice) 
*!*		=jkeycanc()
*!*	ENDIF

*!*	PUSH KEY
*!*	ON KEY

*!*	#IFNDEF FORM_BROWSE
*!*		DEFINE WINDOW ventsecue FROM 1, 30 TO 3, 77 IN SCREEN COLOR SCHEME 5
*!*		ACTIVATE WINDOW ventsecue
*!*	#ENDIF


*!*	reg_ant = RecNo ()
*!*	busqsecue = Eval (VarRead ())
*!*	IF Type ('m.busqsecue') != Type ('m._busqsecue')
*!*		_busqsecue = m.busqsecue
*!*	ELSE
*!*		IF Type ('m.busqsecue') = 'C'
*!*			IF Len (m.busqsecue) != Len (m._busqsecue)
*!*				_busqsecue = m.busqsecue
*!*			ENDIF
*!*		ENDIF
*!*	ENDIF

*!*	m.campo = VarRead ()
*!*	DO CASE
*!*		CASE Type ('_busqsecue') = 'N'
*!*			#IFNDEF FORM_BROWSE
*!*				@ 0, 1 SAY 'Dato a buscar:'
*!*				@ 0, 16 GET m._busqsecue PICTURE '@K'
*!*				READ
*!*			#ELSE
*!*				DO FORM BUSQSECU
*!*			#ENDIF

*!*			IF LastKey () != 27
*!*				SKIP
*!*				LOCATE FOR Eval (campo) = m._busqsecue REST
*!*				IF !Found ()
*!*					= Msg ('No se encontr� ning�n registro que concuerde con el dato solicitado')
*!*					= Ir_Reg (m.reg_ant)
*!*				ENDIF
*!*			ENDIF
*!*		CASE Type ('_busqsecue') = 'C'
*!*			#IFNDEF FORM_BROWSE
*!*				@ 0, 1 SAY 'Dato a buscar:'
*!*				@ 0, 16 GET m._busqsecue PICTURE '@KS30'
*!*				READ
*!*			#ELSE
*!*				DO FORM BUSQSECU
*!*			#ENDIF

*!*			IF LastKey () != 27
*!*				SKIP
*!*				LOCATE FOR At (Upper (AllTrim (m._busqsecue)), Upper (Eval (campo))) >= 1 REST
*!*				IF !Found ()
*!*					= Msg ('No se encontr� ning�n registro que concuerde con el dato solicitado')
*!*					= Ir_Reg (m.reg_ant)
*!*				ENDIF
*!*			ENDIF
*!*		CASE Type ('_busqsecue') = 'D'
*!*			#IFNDEF FORM_BROWSE
*!*				@ 0, 1 SAY 'Dato a buscar:'
*!*				@ 0, 16 GET m._busqsecue PICTURE '@K'
*!*				READ
*!*			#ELSE
*!*				DO FORM BUSQSECU
*!*			#ENDIF

*!*			IF LastKey () != 27
*!*				LOCATE FOR Eval (campo) = m._busqsecue REST
*!*				SKIP
*!*				IF !Found ()
*!*					= Msg ('No se encontr� ning�n registro que concuerde con el dato solicitado')
*!*					= Ir_Reg (m.reg_ant)
*!*				ENDIF
*!*			ENDIF
*!*	ENDCASE

*!*	#IFNDEF FORM_BROWSE
*!*		RELEASE WINDOWS ventsecue
*!*	#ENDIF

*!*	POP KEY

*!*	IF !EMPTY (nindice) 
*!*		_jexitkey = 13
*!*		_JDBLCLICK = -1
*!*		IF ! EMPTY (forbrow)
*!*			=jkeyinit("U", forbrow," Buscando: ","")
*!*		ELSE
*!*			=jkeyinit("U", ""," Buscando: ","")
*!*		ENDIF
*!*	ENDIF

*!*	RETURN .T.


*************************************************
FUNCTION BusqSecu
*************************************************
* 
* Funcionamiento:
* Realiza una b�squeda secuencial sobre el browse.
* Es llamada al presionar la tecla Home.
* 
* 
PUBLIC _busqsecue
PRIVATE campo, busqsecue, reg_ant, tipo

* Desactiva Jkey
=jkeycanc()

PUSH KEY
ON KEY

#IFNDEF FORM_BROWSE
	DEFINE WINDOW ventsecue FROM 1, 30 TO 3, 77 IN SCREEN COLOR SCHEME 5
	ACTIVATE WINDOW ventsecue
#ENDIF

m.reg_ant = RecNo ()
*!*	m.reg_ant = DBRECNO ( ) 
m.busqsecue = Space (20)

IF Type ('m._busqsecue') = 'C' AND Len (m._busqsecue) = 20
	m.busqsecue = m._busqsecue
ENDIF
m._busqsecue = m.busqsecue

m.campo = VarRead ()

#IFNDEF FORM_BROWSE
	@ 0, 1 SAY 'Dato a buscar:'
	@ 0, 16 GET m._busqsecue PICTURE '@KS30'
	READ
#ELSE
	DO FORM BusqSecu
#ENDIF

IF LastKey () != 27
*!*		IF ! _SQL_FLAG OR ! TABLA_SQL (ALIAS())
		SKIP
*!*			DBSKIP ( )
		* LOCATE FOR At (Upper (AllTrim (m._busqsecue)), Upper (Eval (campo))) >= 1 REST
		* LOCATE FOR At (Upper (AllTrim (m._busqsecue)), Upper (Str (Eval (ident_imp[m.ubic])) + Eval (ident_exp[m.ubic]) + Vinc_Con())) >= 1 REST

		m.exp_busc = IIF(VARTYPE(m.exp_busc) <> "C", m.campo, m.exp_busc)
		m.tipo = TYPE (m.exp_busc)

		DO CASE
		CASE m.tipo=="D"
			m.exp_busc = [ DTOC (] + m.exp_busc + [)]
		CASE m.tipo=="N"
			m.exp_busc = [ STR (] + m.exp_busc + [)]
		ENDCASE

		LOCATE FOR At (Upper (AllTrim (m._busqsecue)), Upper (Eval (m.exp_busc))) >= 1 REST

		IF !Found ()
			= Msg ('No se encontr� ning�n registro que concuerde con el dato solicitado')
			= Ir_Reg (m.reg_ant)
		ENDIF
*!*		ELSE
*!*			WITH _Screen.ActiveForm
*!*				* .grdFiltro (.GridColumn) = ""
*!*				m.campo = "cur_filt.filtro" + STRZERO (.GridColumn.ColumnOrder, 2)
*!*				REPLACE &campo WITH ALLTRIM(m._busqsecue)

*!*				WITH .grdFiltro.Columns (.GridColumn.ColumnOrder).Header1
*!*					IF ! EMPTY (m._busqsecue)
*!*						.Caption = "Contiene"
*!*						.ForeColor = RGB(0,0,255)
*!*					ELSE
*!*						.Caption = "Todos"
*!*						.ResetToDefault("ForeColor")
*!*					ENDIF
*!*				ENDWITH
*!*				
*!*				.GeneraSQL
*!*			ENDWITH
*!*		ENDIF
ENDIF


#IFNDEF FORM_BROWSE
	RELEASE WINDOWS ventsecue
#ENDIF

POP KEY

_jexitkey = 13
_JDBLCLICK = -1
=jkeyinit("U", ""," Buscando: ","")

RETURN .T.


*
*************************************************
FUNCTION tabvacia
*************************************************
* Autor: Carlos
* 
* Fecha: 05/10/94
* 
* Funcionamiento:
* Selecciona el alias de la tabla que se pasa como
* par�metro, hace un GO TOP, y si da EOF (),
* retorna .T., dado que implica que la tabla no
* tiene registros. Sino retorna .F.
* No se utilizar reccount, porque si tengo
* registros borrados, reccount los considera, y
* me puede devolver un valor que no quiero.
* 
* Par�metros:
* 
* tabla	= N - es el alias en el que se debe verificar si
*							la tabla est� vac�a.
* 
* Modificaciones:
* 

PARAMETERS m.tabla
PRIVATE retorno, nro_reg, alias_ant
retorno = .F.

IF EMPTY (m.tabla)
	=msg ('No existe el alias ' + m.tabla)
	retorno = .T.
ENDIF
IF !retorno
	alias_ant = ALIAS ()
	SELECT (m.tabla)
	nro_reg = RECNO ()
	GO TOP
	IF EOF ()
		retorno = .T.
	ELSE
		IF nro_reg <= RECCOUNT ()
*!*			IF nro_reg <= DBRECCOUNT()
			GO nro_reg
		ELSE
			* ### C/S	: Revisar para cliente-servidor
			GO BOTTOM
		ENDIF
	ENDIF
	IF ! EMPTY (alias_ant)
		SELECT (alias_ant)
	ENDIF
ENDIF
RETURN retorno
*************************************************
FUNCTION RELT
*
* Relaciona dos tablas comprobando si ya se en-
* contraban relacionadas.
* PARAMETROS:
*		CPO(C): Expresi�n por la cual se va a relacionar.
*		TABLA(C): Tabla a relacionar (Hija).
*		ONE_MUCH(L): Indicador de relaci�n 1 a N
*
* SYNTAX: =RELT( 'CPO','TABLA'[,ONE_MUCH] )						
*************************************************
PARAMETERS cpo, tabla, one_much
PRIVATE I

IF EMPTY(cpo) OR EMPTY(tabla)
	WAIT WIND 'Faltan parametros para llamar a la funci�n RELT'
	RETU
ENDIF

IF EMPTY (one_much)
	one_much = .F.
ENDIF

i = 1
DO WHIL i <= 225 AND !EMPTY( TARGET (i) )
	IF TARGET (i) = tabla
		RETURN
	ENDIF
	i = i + 1
ENDDO

* IF _SQL_FLAG AND CURSORGETPROP ("SourceType", m.tabla)==2
*!*	IF TABLA_SQL (m.tabla) AND !EMPTY(CURSORGETPROP ("SourceName", m.tabla))
*!*		* SET RELA TO SQL_REL(tabla, cpo) INTO &tabla ADDIT
*!*		m.cpo = [ SQL_REL ("] + tabla+[","] + m.cpo + [")]
*!*		SET RELA TO &cpo INTO &tabla ADDIT
*!*		RETURN
*!*	ENDIF

SET RELA TO &cpo INTO &tabla ADDIT
IF one_much
	SET SKIP TO &tabla
ENDIF

RETURN
*
********************************************
FUNCTION ULDIAMES
********************************************
* Funcionamiento : devuelve el �ltimo d�a del mes de una fecha pasada
* como par�metro.
*
* Par�metro :   wfecha :  (D) fecha
*
PARAMETERS wfecha
PRIVATE wmes, wanio, wpridia, retorno

wmes  = MONTH (wfecha)
wanio = YEAR (wfecha)

IF wmes + 1 = 13
	wmes = 1
	wanio = wanio + 1
ELSE
	wmes = wmes + 1
ENDIF

wpridia = '01/' + STR (wmes, 2) + '/' + STR (wanio, 4)

retorno = CTOD (wpridia) - 1

RETURN retorno



*************************************************
FUNCTION tab_vac
*************************************************
* Funcionamiento
*  Devuelve .T. si la tabla est� vac�a
*	 Devuelve .F. si la tabla NO est� vac�a. En este caso si se le pasa
*  un n�m de reg v�lido deja el puntero en este reg, si en cambio no
*	 se pasa un reg o el reg pasado es inv�lido el puntero queda en el 
*	 �ltimo reg 
*
* Par�metros
*	[reg]         : (N) (ninguno, queda en el actual) n�mero de registro  
*						      		en que se quiere posicionar el puntero.
* [tab_a_verif] : (C) (tabla activa) Nombre de la tabla de la que se quiere 
* 										saber si est� vac�a.
*************************************************

	parameterS reg, tab_a_verif

	IF TYPE('reg') <> 'N'
		reg = RECNO()
	ENDIF
	IF TYPE('tab_a_verif') <> 'C'
		tab_a_verif = ALIAS()
	ENDIF

	* IF ! _SQL_FLAG OR CURSORGETPROP("SourceType", tab_a_verif)<>2
*!*		IF ! TABLA_SQL (tab_a_verif)
		IF RECCOUNT(tab_a_verif) <> 0
			GO TOP IN (tab_a_verif)
			IF EOF(tab_a_verif)
				RETURN .T.
			ELSE
				IF !(reg > RECCOUNT() OR reg=0)
					GO reg IN (tab_a_verif)
				ELSE
					GO BOTTOM IN (tab_a_verif)
				ENDIF
				RETURN .F.
			ENDIF
		ELSE
			RETURN .T.
		ENDIF
*!*		ELSE
*!*			DBGORECNO (m.reg, m.tab_a_verif)
*!*			DO CASE
*!*			CASE DBEOF (m.tab_a_verif)
*!*				RETURN .F.
*!*			OTHERWISE
*!*				RETURN .T.
*!*			ENDCASE
*!*		ENDIF


*************************************************
FUNCTION rango
*************************************************
* Funcionamiento:
*	 Devuelve .T. si el rango es correcto
*  Devuelve .F. si el rango es incorrecto
*  Chequea que el valor (val_cpo) ingresado permanezca dentro del rango
*	indicado. Si el rango es incorrecto se muestra un mensaje y queda 
*	seleccionado el campo que se estaba editando. Si el rango es 
*	incorrecto y se presiona ESC no se chequea si el rango es v�lido.
*
* Par�metros:
*  val_cpo    = (C,N,D) valor del campo cuyo rango se valida ( con M. )
*  nom_cpo    = (C) nombre del campo para mensajes (Referencia del campo)
*  tipo_rango = (N) indica que es lo que se desea chequear;
		1 (01)  l�mite superior (solo los menores que);
		2 (10)  l�mite inferior (solo los mayores que);
		3 (11)  entre DOS valores;
		4 (100) excluyente
*  val_inf    = (C,N,D) valor inferior para casos 3 y 4
*											  Para caso 1 es l�mite superior
*											  Para caso 2 es l�mite inferior
*  [val_sup]  = (C,N,D) Para casos 1 y 2 no va
*											  valor superior para casos 3 y 4
*  [cant_deci] = (N)    Indica con la cantidad de decimales muestra el mensaje
*
* Modif.:
* Gabriel 220995 Agregue parametro cant_deci
**************************************************

	PARAMETERS val_cpo, nom_cpo, tipo_rango, val_inf, val_sup, m.cant_deci
	PRIVATE retorno, dsc_val_inf, dsc_val_inf
	retorno = .t.

	DO CASE
	CASE TYPE('val_cpo') = 'N'
		IF TYPE('val_inf') = 'C'
			val_inf = VAL(val_inf)
		ENDIF
		IF TYPE('val_sup') = 'C'
			val_sup = VAL(val_sup)
		ENDIF
	CASE TYPE('val_cpo') = 'D'
		IF TYPE('val_inf') = 'C'
			val_inf = CTOD(val_inf)
		ENDIF
		IF TYPE('val_sup') = 'C'
			val_sup = CTOD(val_sup)
		ENDIF
	ENDCASE	
	
	DO CASE 
	CASE TYPE('val_inf') = 'C'
		dsc_val_inf = val_inf
	CASE TYPE('val_inf') = 'N'
		IF val_inf / INT(val_inf) = 1
			dsc_val_inf = STR (val_inf,17,0)
		ELSE
			dsc_val_inf = STR (val_inf,17,IIF (TYPE ('m.cant_deci') = 'N', m.cant_deci, 2))
		ENDIF
	CASE TYPE('val_inf') = 'D'
		dsc_val_inf = DTOC(val_inf)
	ENDCASE

	DO CASE 
	CASE TYPE('val_sup') = 'C'
		dsc_val_sup = val_sup
	CASE TYPE('val_sup') = 'N'
		IF val_sup / INT(val_sup) = 1
			dsc_val_sup = STR(val_sup,17,0)
		ELSE
			dsc_val_sup = STR(val_sup,17,IIF (TYPE ('m.cant_deci') = 'N', m.cant_deci, 2))
		ENDIF
	CASE TYPE('val_sup') = 'D'
		dsc_val_sup = DTOC(val_sup)
	ENDCASE
			
	IF LASTKEY() <> 27
		DO CASE
		CASE tipo_rango = 1
			IF val_cpo > val_inf
				retorno = .F.
				= msg('RANGO ERRONEO:  '+nom_cpo+' debe ser menor o igual que '+ ALLTRIM(dsc_val_inf))			
			ENDIF
		CASE tipo_rango = 2
			IF val_cpo < val_inf
				retorno = .F.
				= msg('RANGO ERRONEO:  '+nom_cpo+' debe ser mayor o igual que ' + ALLTRIM(dsc_val_inf))
			ENDIF
		CASE tipo_rango = 3
			IF (val_cpo < val_inf) OR (val_cpo > val_sup)
				retorno = .F.
				= msg('RANGO ERRONEO:  '+nom_cpo+' debe estar entre ' + ALLTRIM(dsc_val_inf) + ' y ' + ALLTRIM(dsc_val_sup))
			ENDIF
		CASE tipo_rango = 4
			IF (val_cpo >= val_inf) AND (val_cpo <= val_sup)
				retorno = .F.
				= msg('RANGO ERRONEO:  '+nom_cpo+' debe ser menor que ' + ALLTRIM(dsc_val_inf) + ' y mayor que ' + ALLTRIM(dsc_val_sup))
			ENDIF
		ENDCASE
	ENDIF

	RETURN retorno


*************************************************
FUNCTION no_nulo
*************************************************
* Funcionamiento:
*  Devuelve .T. si el valor (nul_nom_cpo) de un campo es nulo
*	 Devuelve .F. si el valor (nul_nom_cpo) de un campo NO es nulo
*						En este caso se muestra un mensaje y queda 
*						seleccionado el campo 
* Par�metros
*  nul_nom_cpo    : (C,N,D,L) Valor del campo a chequear
*  nul_desc_cpo   : (C) Nombre del campo para mensaje 
*
**************************************************
PARAMETERS nul_nom_cpo, nul_desc_cpo
	IF TYPE('nul_desc_cpo') <> 'C'
		nul_desc_cpo = ''
	ENDIF	
	PRIVATE retorno
	retorno = .T.
	*IF EMPTY_NULL (nul_nom_cpo) AND LASTKEY() <> 27 AND 
	IF EMPTY(nul_nom_cpo) AND LASTKEY() <> 27 AND ;
	 TYPE ('nul_nom_cpo') <> 'N' AND TYPE ('nul_nom_cpo') <> 'L'
		= msg('El campo '+nul_desc_cpo+' no puede ser Nulo ')
		retorno = .F.
	ENDIF
	RETURN retorno



*************************************************
FUNCTION salir_menu
*************************************************

	PARAMETERS salida

	*POP MENU MENU
	m.done = .T.
	CLEAR READ ALL
	IF PARAMETERS () = 0 OR salida = 1
		SET SYSMENU NOSAVE
		SET SYSMENU TO DEFAULT
		CLEAR EVENTS	&& ### VFP
	ELSE
		QUIT
	ENDIF


*************************************************
FUNCTION uset
*************************************************
* Funcionamiento:
*  Abre una tabla en la primer �rea libre. Si es que ya est�
*	en uso setea el orden si es distinto al actual.
*	 No se selecciona la tabla que se abre.
*  Si el sistema es "conta" se cambia conta por _EJERC.
*
* Autor : ??
* Modif : Gabriel 16/12/94 ( agregue alias )
*					Rodrigo 08/07/95 ( Se agreg� el 4 par�metro )
*
* Par�metros:
*  ut_tabla    : (C) Nombre de la tabla que se quiere abrir 
*								 Puede incluir un path (ej.: 'sitema\tabla')
*  [ut_indice] : (C) ('nombre_tag_1') Indice por el cual se desea
* 							 ordenar la tabla a abrir.
*  [ut_tabla ] : (C) ('ordcom') Alias con que se quiere abrir una tabla 
*	 [ut_excl  ] : (L) Par�metro que indica si se quiere abrir la tabla en
*								 forma EXCLUSIVA.
*
*************************************************

PARAMETERS ut_tabla, ut_indice, ut_alias, ut_excl
PRIVATE ut_pos, ut_stabla, ut_arch_tab, ut_arch_ind, ut_sistema, ut_stab_ver
PRIVATE retorno, on_err, que_error

	* ### C/S
	* IF !TYPE("_SQL_DB")=="C"
	* 	PUBLIC _SQL_DB, _SQL_HND, _SQL_FLAG
	* 	_SQL_FLAG = .F.
	* 	_SQL_DB  = ""
	* 	_SQL_HND = 0
	* ENDIF
	
	* IF !EMPTY(_SQL_DB)
	* 	_SQL_FLAG = .T.
	*	RETURN (SQL_USET (ut_tabla, ut_indice, ut_alias, ut_excl))
	* ENDIF

*!*		IF _SQL_FLAG
*!*			RETURN (SQL_USET (ut_tabla, ut_indice, ut_alias, ut_excl))
*!*		ENDIF

	que_error		=	.T.
	retorno     = .T.
	ut_sistema  = SUBSTR (ut_tabla, 1, RAT('\', ut_tabla) - 1)
	ut_stabla   = SUBSTR (ut_tabla, RAT('\', ut_tabla) + 1)
	ut_arch_tab = ut_tabla + '.dbf'
	ut_arch_ind = ut_tabla + '.cdx'
	ut_stab_ver = ut_stabla 
		
	IF !EMPTY( ut_alias ) AND !( ut_alias == '' )
		ut_stabla = ut_alias
	ENDIF
	
	IF UPPER(ut_sistema) == 'CONTA' AND TYPE ('_EJERC') = 'C' AND !EMPTY (_EJERC)	
		ut_tabla    = _EJERC + '\' + ut_tabla
		ut_arch_tab = _EJERC + '\' + ut_arch_tab
		ut_arch_ind = _EJERC + '\' + ut_arch_ind	
	ENDIF

	IF !USED (ut_stabla)
		IF FILE (ut_arch_tab) AND FILE (ut_arch_ind)
			on_err = ON('ERROR')
			ON ERROR DO USET_ERR WITH ERROR(), MESSAGE()
			IF ut_excl
				que_error 	= .F.
				USE (ut_tabla) ALIAS (ut_stabla) AGAIN IN 0 EXCL
			ELSE
				que_error		=	.T.
				USE (ut_tabla) ALIAS (ut_stabla) AGAIN IN 0
			ENDIF
			ON ERROR &on_err
			IF !retorno
				IF que_error
					= FINAL (1003, ut_tabla)
				ELSE
					= FINAL (1002, ut_tabla)
				ENDIF
			ENDIF
		ELSE
			DO CASE
				CASE ! FILE (ut_arch_tab)
					= final (1000, ut_arch_tab)
				CASE ! FILE (ut_arch_ind)
					= final (1001, ut_arch_ind)
			ENDCASE
			retorno = .F.
		ENDIF
	ENDIF

	IF retorno
		IF TYPE('ut_indice') = 'C'
			IF ( tagno( ut_indice ,	ut_stab_ver, ut_stabla ) = 0 )
				IF EMPTY (ut_indice)
					SET ORDER TO primario IN (ut_stabla)
				ELSE
					= final (1004, ut_indice)
				ENDIF
			ELSE
				SET ORDER TO (ut_indice) IN (ut_stabla)
			ENDIF
		ELSE
			SET ORDER TO primario IN (ut_stabla)
		ENDIF
	ENDIF
	
* CEMII
	IF (Upper (AllTrim (ut_stabla))) == 'CEMENT'
		= Uset ('recur\ubic_cem')
	ENDIF
* CEMII_

	RETURN retorno

*************************************************
FUNCTION USET_ERR
*************************************************
* Autor : RODRIGO
* Captura los posibles errores que se puedan producir al abrir
* una tabla.
* NOTA: No poner PRIVATE retorno
PARAMETERS nro_err, msg_err

retorno = .T.
IF nro_err = 1705 OR UPPER(msg_err) = 'FILE ACCESS DENIED'
	retorno = .F.
ELSE
	=MSG('ERROR GRAVE al Intentar Abrir el Archivo '+UPPER(ut_tabla))
	ON ERROR &on_err
	&on_err
ENDIF

RETURN retorno
*

*************************************************
FUNCTION APPEND
*************************************************
* Funcionamiento
*  Si hay un registro marcado para borrar en la tabla
* 	actual lo lockea y lo recupera con un RECALL (no limpia el registro)
* 	y deja el puntero posicionado en ese registro. Si no hay
*		registros borrados para recuperar hace un APPEND BLANK
*		abitual y lockea el nuevo registro.
*	 Es �til para reutilizar registros borrados en cursores y 
*		en general.
*	 Si NO puede lockear el registro a recuperar o el nuevo devuelve .F.		
*		En caso contrario devuelve .F.

* Sin par�metros
*
* Ejemplo de uso:	IF APPEND()
*                 	UNLOCK
*                	ELSE
*                 	* Abort record add operation
*                	ENDIF
*
* Modif.:
* Gabriel 120995 cambie llamado a MSG por WAIT WIND "" NOWAIT
* Gabriel 130995 agregue utilidad al parametro que ten�a
*
**************************************************

PARAMETERS m.tabla
PRIVATE B,nerror,bmessage

*	SET DELETED OFF    && make deleted records visible
*	GOTO TOP && if there are any deleted records, its at top of file
*	SET DELETED ON     && return to our usual setting
*	IF DELETED() AND rlock()
*   Found a record to recycle and was able to lock it
*		RECALL
*		RETURN
*	ELSE
  * No record found to recycle (or lost a fight
  * over it with someone else), so create new.
  * First save current error handler and set up
  * to trap for any error. Usually we'll get the
  * error "File is in use by another" (108) if
  * someone else adds a record at the same instant.
  * In the event of an error, we retry at one second
  * intervals until we succeed or the user aborts.
  
		*tabla = ALIAS ()
		*IF PARAMETERS () = 1 AND USED (tabla)  
		*	SELECT (tabla)
		*ENDIF
		
		bmessage = .F.
		cehsave = ON('ERROR')
		nerror = 77777   && dummy value to prime the loop
		ON ERROR nerror = ERROR()
		DO WHILE nerror > 0
			nerror = 0

			APPEND BLANK
*!*				DBAPPBLANK()
			
			IF nerror > 0
      * You could get more elaborate here and check for
      * specific errors or hand off to your standard
      * error handler if it's an unexpected error number.
      * We are just assuming the problem is a conflict with
      * another user, and we allow the user to give up if
      * they get tired of waiting.
				*IF NOT bmessage
					*bmessage = .T.
        * This call to ALERT() doesn't wait for input; it
        * just displays the message and returns immediately
        * so we check for user abort. We use the bMessage
        * flag so the alert doesn't keep getting destroyed
        * and re-created.
					*=MSG ("Est�n agregando un registro. Espere a que termine.")
				*ENDIF
				
				WAIT WIND "NO Puede Agregar el Registro. Tabla en Uso. Con <ESC> deja de intentar"  NOWAIT
				CLEAR TYPEAHEAD

				IF INKEY(1) = 27
        * Avoid flooding the system with lock requests
        * by rechecking just once per second.
					ON ERROR &cehsave
					&cehsave
					RETURN .F.
				ENDIF
				
			ENDIF
		ENDDO
		ON ERROR &cehsave
*	ENDIF
RETURN .T. && lockr()


*************************************************
FUNCTION lockr
*************************************************
* Funcionamiento
*	Esta funci�n intenta lockear el registro actual de la tabla actual
* Se avisa con un mensaje si el registro no puede ser lockeado
*	 Devuelve .T. si se puede lockear 
*  Devuelve .F. si NO se puede lockear el registro
**************************************************
	IF RLOCK()
		RETURN .T.
	ENDIF
	PRIVATE ninkey
	=MSG ("Registro en tabla " + DBF() + " en uso...]" + CHR(13) + ;
		" Esperando para bloquear registro.")
	CLEAR TYPEAHEAD
	ninkey = INKEY(1)
	DO WHILE ninkey <> 27 AND (NOT RLOCK())
		ninkey = INKEY(1)
	ENDDO
	RETURN ninkey <> 27


*************************************************
FUNCTION basename
*************************************************

* Given a filename, possibly including a relative or fully qualified path,
* return the basename of the file (strip path & extension).

* Requires: FILENAME()

	parameter cfile
	PRIVATE nsep,cfile
	cfile = filename(cfile)
	nsep = AT('.',cfile)
	nsep = IIF(nsep = 0,LEN(cfile) + 1,nsep)
	RETURN LEFT(cfile,nsep - 1)

*************************************************
FUNCTION filename
*************************************************

* Given a filename, possibly including a relative or fully qualified path,
* return the filename (strip path).

* Requires: globals _cPathSep and _cDriveSep (OS specific path and
*           drive separators, respectively)

	parameter cfile

	DO CASE
		CASE RIGHT(cfile,1) $ _cpathsep + _cdrivesep
			RETURN ''
		CASE _cpathsep $ cfile
			RETURN SUBSTR(cfile,RAT(_cpathsep,cfile) + 1)
		CASE _cdrivesep $ cfile
			RETURN SUBSTR(cfile,AT(_cdrivesep,cfile) + 1)
		OTHERWISE
			RETURN cfile
	ENDCASE

**************************************************
FUNCTION browact
**************************************************
* Funci�n para actualizar la ventana de browse
* Desaparecen los campos borrados en el momento de boorrarlos
* ejemplo al llamar al Browse:
*   BROWSE VALID browact()

	PARAMETERS gs_nom_vent
	IF PARAMETERS() # 1
		gs_nom_vent = WONTOP()
	ENDIF
	IF DELETED()
		SHOW wind (gs_nom_vent) REFRESH
	ENDIF

*
**********************************
 FUNCTION to_nro
**********************************
PARAMETERS str
PRIVATE nro
RETURN VAL( STRTRAN( str , ' ' , '' ) )
*

***********************************
FUNCTION WW
***********************************
*
*
PARAM texto
WAIT WIND texto NOWAIT
RETURN ''
*

***********************************
FUNCTION lkey
***********************************
PRIVATE retorno

	retorno = LASTKEY()	
	RETURN retorno
*
*************************************************
FUNCTION AUTO_NO
*************************************************
* Funcionamiento: 
*					Proporciona un n�mero � c�digo autom�tico
* sugerido y puede asignarse a un determinado campo
*		Devuelve 	ERROR (-1) o un n�mero mayor que cero 
*	correspondiente al pr�ximo n�mero v�lido disponible.
*		Esta funci�n solo puede usarse en caso de Altas.
*
*
* Par�metros: 
*
* SISTEMA	(C)	: Sistema al cual pertenece la tabla
* TABLA		(C) : Tabla sobre la cual se autonumera
* CAMPO		(C) : Campo al que se va a autonumerar
* INDICE  (C) : Indice de la Tabla
* EXP_IND (C) : Expresion de Indice para busqueda parcial
* WARNING (L) : Con .T. muestra mensaje de error en Autonumeraci�n
*
* SYNTAXIS:    AUTO_NO (sistema, tabla, campo, indice, exp_ind, warning)
*
* Ejemplos:    
*		AUTO_NO ('CONTA','PROVEED','NRO_PROV')
*		AUTO_NO ('CONTA','TEL_PROV','CONT_TEL','PAR_REAL',STR(29,5,0)+'P',.T.)
*
*
* Este ejemplo NO es v�lido:
*		AUTO_NO ('CONTA','TEL_PROV','CONT_TEL','',STR(29,5,0)+'P')
*
* 										L E E R    ! ! ! !
*
* El indice y la exp_ind son campos vinculados NO pueden 
* usarse solos y garantizar un buen funcionamiento de la 
* funci�n.
*
***********************************************************


PARAMETERS sistema, tabla, campo, indice, exp_ind, warning
PRIVATE gs_curr_dbf, gs_curr_rec, autonum, max_val_cpo, encontro

IF !BETWEEN(PARAMETERS(),3,6)
	WAIT WIND "Cantidad erronea de parametros (3-6)"
	RETURN
ENDIF

IF EMPTY(warning)
	warning = .F.
ENDIF

gs_curr_dbf = SELE()
sistabla = sistema+'\'+tabla
=uset(sistabla)
SELE (tabla)

max_val_cpo = INT(10 ^ FSIZE (campo) - 1)

IF !EMPTY(indice)
	SET ORDER TO TAG (indice)
*!*		DBSETORDX (indice)
ENDIF
IF TYPE (campo) # 'N'
	WAIT WIND "AUTONUMERACION es solo para datos numericos"
	SELE (gs_curr_dbf)
	RETURN
ENDIF
IF !EMPTY(exp_ind)
	SET KEY TO (exp_ind)
*!*		DBSETKEY (exp_ind)
*!*	ELSE
*!*		DBSEEK ()
ENDIF
gs_curr_rec = RECNO()
GO BOTTOM

IF &campo # max_val_cpo
	autonum = &campo + 1
ELSE
	encontro = .T.
	GO TOP
	IF EMPTY(&campo)
		autonum = -1
	ELSE
		autonum = 0
	ENDIF
	DO WHIL !EOF() AND encontro
		autonum = autonum + 1
		IF autonum = &campo
			encontro = .T.
		ELSE
			encontro = .F.
		ENDIF
		SKIP
		IF EOF()
			IF warning
				=MSG('NO se puede Autogenerar el C�digo')
			ENDIF
			autonum = -1
		ENDIF
	ENDDO
ENDIF
= TAB_VAC (gs_curr_rec)
SET KEY TO
*!*	DBSETKEY()
SELE (gs_curr_dbf)

RETURN (autonum)
*************************************************
PROCEDURE VENTMSG
*************************************************

PARAMETERS 	mensaje, fil_ini, col_ini, fil_fin, col_fin

IF PARAMETERS() < 1
	?? CHR(7)
	SET NOTIFY ON
	WAIT WINDOW "Insuficientes Par�metros" NOWAIT
	SET NOTIFY OFF
	RETURN
ENDIF

IF TYPE('mensaje') <> "C"
	?? CHR(7)
	SET NOTIFY ON
	WAIT WINDOW "Par�metro Incorrecto" NOWAIT
	SET NOTIFY OFF
	RETURN
ELSE
	IF PARAMETERS() < 5 OR fil_ini < 0 OR fil_fin > 24 OR col_ini < 0 OR col_fin > 79 OR col_ini+3 > col_fin OR fil_ini + 2 > fil_fin
		fil_ini = 7
		fil_fin = 13
		col_ini = 19
		col_fin = 59
	ENDIF
ENDIF

DEFINE WINDOW procesando FROM fil_ini,col_ini TO fil_fin, col_fin IN SCREEN;
	TITLE " PROCESANDO ... " DOUBLE NOCLOSE FLOAT NOGROW NOMINIMIZE SHADOW;
	NOZOOM COLOR SCHEME 7
ACTIVATE WINDOW procesando NOSHOW

DO escribir_0 WITH mensaje

SHOW WINDOW procesando

*************************************************
PROCEDURE DECIRMSG
*************************************************

PARAMETERS 	mensaje, accion

IF PARAMETERS() < 1
	?? CHR(7)
	SET NOTIFY ON
	WAIT WINDOW "Insuficientes Par�metros" NOWAIT
	SET NOTIFY OFF
	RETURN
ELSE
	IF PARAMETERS() = 1
		accion = 0
	ENDIF
ENDIF

IF TYPE('mensaje') <> "C" OR accion < 0 OR accion > 2
	?? CHR(7)
	SET NOTIFY ON
	WAIT WINDOW "Par�metro Incorrecto" NOWAIT
	SET NOTIFY OFF
	RETURN
ENDIF

ACTIVATE WINDOW procesando
DO CASE
	CASE accion = 0
		DO escribir_0 WITH mensaje
	CASE accion = 1
		DO escribir_1 WITH mensaje
	CASE accion = 2
		DO escribir_2 WITH mensaje
ENDCASE


*************************************************
FUNCTION FINMSG
*************************************************

PARAMETERS 	accion, texto_1, texto_2 

PRIVATE correcto
correcto = .F.
IF PARAMETERS() < 1
	?? CHR(7)
	SET NOTIFY ON
	WAIT WINDOW "Insuficientes Par�metros" NOWAIT
	SET NOTIFY OFF
	RETURN
ENDIF

IF accion < 0 OR accion > 2
	?? CHR(7)
	SET NOTIFY ON
	WAIT WINDOW "Par�metro Incorrecto" NOWAIT
	SET NOTIFY OFF
	RETURN
ELSE
	DO CASE
		CASE accion=0
			correcto = .T.
		CASE accion=1 AND PARAMETERS()=2 AND TYPE('ALLTRIM(texto_1)')="C"
			correcto = .T.
		CASE accion=2 AND PARAMETERS()=3 AND TYPE('ALLTRIM(texto_1)')="C" AND TYPE('ALLTRIM(texto_2)')="C"
			correcto = .T.
		OTHERWISE
			?? CHR(7)
			SET NOTIFY ON
			WAIT WINDOW "Par�metro Incorrecto CASE" NOWAIT
			SET NOTIFY OFF
			RETURN
	ENDCASE
ENDIF

IF accion <> 0
	PRIVATE eleccion, fila, columna, largo, pantalla
	ACTIVATE WINDOW procesando
	SAVE SCREEN TO pantalla
	? ""
	fila = ROW()
	eleccion = 1
	DEFINE WINDOW procesando FROM WLROW('procesando'),WLCOL('procesando');
	TO WLROW('procesando')+WROW('procesando')+1, WLCOL('procesando') + WCOL('procesando')+1;
	IN SCREEN TITLE " PROCESO FINALIZADO " DOUBLE NOCLOSE FLOAT NOGROW NOMINIMIZE SHADOW;
	NOZOOM COLOR SCHEME 7
	ACTIVATE WINDOW procesando NOSHOW
	RESTORE SCREEN FROM pantalla
	
ENDIF
DO CASE
	CASE accion = 0
		DEACTIVATE WINDOW procesando
		RELEASE WINDOW procesando
		RETURN .T.
	CASE accion = 1
		columna = (WCOLS() - LEN(ALLTRIM(texto_1))-4)/2
		@ fila, columna GET eleccion PICTURE "@*HT \!\?\<"+texto_1	SIZE 1,LEN(ALLTRIM(texto_1))+2;
		VALID VALPRO() MESSAGE "Presione <Enter>, <Esc.> o <Barra Espaciadora> para continuar."
		READ CYCLE
		RETURN .T.
	CASE accion = 2
		IF LEN(ALLTRIM(texto_1)) < LEN(ALLTRIM(texto_2))
			largo = LEN(ALLTRIM(texto_2)) + 2
		ELSE
			largo = LEN(ALLTRIM(texto_1)) + 2
		ENDIF
		columna = (WCOLS() - (LARGO*2) - 2)/2
		@ fila, columna GET eleccion PICTURE "@*HT \!\<"+texto_1+";\?\<"+texto_2;
		SIZE 1,largo,2 DEFAULT 1 VALID VALPRO() MESSAGE "Seleccione una opci�n para continuar."
		READ CYCLE
		IF eleccion = 1
			RETURN .T.
		ELSE
			RETURN .F.
		ENDIF
ENDCASE

*************************************************
PROCEDURE escribir_0
*************************************************

PARAMETERS mensaje

IF PARAMETERS() <> 1
	? CHR(7)
	SET NOTIFY ON
	WAIT WINDOW "Par�metros Incorrectos"
	SET NOTIFY OFF
	RETURN .F.
ELSE
	IF !EMPTY(ALLTRIM(mensaje))
		?? mensaje
		? ""
	ENDIF
ENDIF

****************************************************************************
PROCEDURE escribir_1
PARAMETERS mensaje

IF PARAMETERS() <> 1
	? CHR(7)
	SET NOTIFY ON
	WAIT WINDOW "Par�metros Incorrectos"
	SET NOTIFY OFF
	RETURN .F.
ELSE
	IF !EMPTY(ALLTRIM(mensaje))
		?? mensaje
	ENDIF
ENDIF

****************************************************************************
PROCEDURE escribir_2
PARAMETERS mensaje
PRIVATE antfil, antcol

IF PARAMETERS() <> 1
	? CHR(7)
	SET NOTIFY ON
	WAIT WINDOW "Par�metros Incorrectos"
	SET NOTIFY OFF
	RETURN .F.
ELSE
	IF !EMPTY(ALLTRIM(mensaje))
		antfil = ROW()
		antcol = COL()
		?? mensaje
		@ antfil, antcol SAY ""
	ENDIF
ENDIF

FUNCTION VALPRO
	CLEAR READ	
	DEACTIVATE WINDOW procesando
	RELEASE WINDOW procesando
*************************************************
FUNCTION ULTCPOCL
*************************************************
* Autor: Rodrigo
* 
* Fecha: 07/10/94
* 
* Funcionamiento: Verifica si la ultima tecla pulsada fue ENTER de ser asi 
* verifica si el registro exista o no en la tabla; si existe se muestra un
* mensaje indicando que se ha ingresado una clave duplicada no permitiendo
* salir del campo. En caso de que la clave exista en la tabla se verifica
*	si se tienen permisos de consulta y edita permitiendo o no acceder al  
* registro solicitado.
*
* Par�metros: No tiene
* 
* Modificaciones:
* 

PRIVATE retorno,busca_cla, db_scr

retorno = .T.
busca_cla = 'busca_clave'
db_scr = 'db_scr'

IF LASTKEY() = 13

	IF &busca_cla()
		IF c_nuevo	
			= MSG('La clave ingresada ya existe. Ingrese una nueva clave.')
			retorno = .F.
		ELSE
			IF gl_cons
				IF _server = 'SICO02'
					retorno = NWVAL_CTRL('EDITA')
				ELSE
					IF gl_modi
						retorno = NWVAL_CTRL('EDITA')
					ELSE
						retorno = &DB_SCR(3,2)
					ENDIF
				ENDIF
			ELSE
				retorno = .F.
			ENDIF
		ENDIF
	ELSE
		retorno = NWVAL_CTRL('NUEVO')
	ENDIF

ENDIF

RETURN retorno
*
*************************************************
FUNCTION inicio
*************************************************
* Funcionamiento:
* Inicializa:
*		permisos
*		auditoria
*		programa
* 
* Par�metros:
* Debe ser invocada con SYS(16) 
*
* Falta :
* impresora
* backup
* HELPFILTER
* vector con tablas abiertas

PARAMETERS path_prog

CLEAR PROGRAM

	PUBLIC _gs_ayabm
	_gs_ayabm = 1

*!*	#IFDEF LOG_PERF
*!*		PUBLIC _LP_INIT, _LP_START, _LP_TEXT, _LP_LOG, _LP_INIT_LOG
*!*		
*!*		_LP_INIT_LOG = [ LOGFILE (REPLICATE("-",80)+CHR(13)+CHR(10)+BPROGRAM(1)+" "+DTOC(DATE())+" "+TIME()) ]
*!*		_LP_INIT = [ PRIVATE m.lp_sec, m.lp_text ]
*!*		_LP_START= [ m.lp_sec=SECONDS() ]
*!*		* _LP_TEXT = [ m.lp_text= ]
*!*		_LP_TEXT = [ m.lp_text ]
*!*		* _LP_LOG  = [ LOGFILE (m.lp_text+": "+ALLTRIM(STR(SECONDS()-m.lp_sec,8,2))) ]
*!*		* _LP_LOG  = [ LOGFILE (STR(SECONDS()-m.lp_sec,8,2)+": "+m.lp_text) ]
*!*		* _LP_LOG  = [ LOGFILE (STR(SECONDS()-m.lp_sec,8,2)+" ("+BPROGRAM(1)+"/"+BPROGRAM(2)+"): "+m.lp_text) ]
*!*		_LP_LOG  = [ LOGFILE (STR(SECONDS()-m.lp_sec,5,2)+" ("+BPROGRAM(1,3)+"): "+m.lp_text) ]
*!*	#ENDIF

*!*		INIT_LOG

	PRIVATE nivel_alta, nivel_baja, nivel_cons, nivel_acce, nivel_modi, ult_num_aud

* Si entr� desde el Fox no estar� seteado el ON ERROR
	IF ATC ("DO errores WITH ERROR()", ON ('ERROR')) = 0 AND ;
		SET ('DEBUG') = 'OFF' AND _SERVER != 'SICO'
		=MSG ("Debe ingresar al sistema con el comando: 'SIFIM'")
		QUIT
	ENDIF

	IF SET ('DEBUG') = 'OFF'
		PUSH KEY
	ENDIF
	
	IF SET ('DELE') = 'OFF'
		SET DELE ON
	ENDIF
	
	ON KEY LABEL 'F7'  DO ffuncs WITH 'F7'
	ON KEY LABEL 'F8'  DO ffuncs WITH 'F8'
	ON KEY LABEL 'F9'  DO ffuncs WITH 'F9'
	ON KEY LABEL 'F10' DO ffuncs WITH 'F10'
	ON KEY LABEL 'F11' DO ffuncs WITH 'F11'
	ON KEY LABEL 'F12' DO ffuncs WITH 'F12'

	*sentencia comentada por converion de VFP
	*ON KEY LABEL 'ALT+C' DO ACT_WIND WITH 'CALCULATOR'

	*sentencia agregada (1) por converion de VFP
	ON KEY LABEL 'ALT+C' !/N CALC.EXE

	*sentencia comentada por converion de VFP
	*ON KEY LABEL 'ALT+D' DO ACT_WIND WITH 'CALENDAR'
	* Se habilita debug en caso de que el server sea SICO

	IF _SERVER = 'SICO'
		deb_123='debug'
*		ON ERRO	&&### C/S: No puedo desactivar el handler de error en SQL
		PRIVATE pla_err_123, pas_ok_123
		pla_err_123 = 'PLA_ERR'
		pas_ok_123 = 'PAS_OK'
		ON KEY LABEL 'F4' DO VerAmbien
		ON KEY LABEL 'CTRL+F12' DO &deb_123
		ON KEY LABEL 'CTRL+F11' DO SICO02\VOL1:\-DESA\MAN_ERR\HOJA_ERR\&pla_err_123 WITH SYS(16)
		ON KEY LABEL 'CTRL+F9' DO SICO02\VOL1:\-DESA\BIND\&pas_ok_123
		* ON KEY LABEL 'ALT+C' DO ACT_WIND WITH 'CALCULATOR'
		ON KEY LABEL 'ALT+C' !/N CALC.EXE
		ON KEY LABEL 'ALT+D' DO ACT_WIND WITH 'CALENDAR'
	ELSE
		ON KEY LABEL 'CTRL+F12' DO DEBUG.APP
	ENDIF
 
	* Oculta todos los popup visibles
	HIDE POPUP ALL

	* Asigna valor de LASTKEY()
	KEYBOARD '{shift+f12}' CLEAR
	WAIT WIND ''

	WAIT WINDOW " Cargando Programa ... " NOWAIT 
	* <verif>
	ayuda = 1

	* Verificaci�n de nombre de programa actual pasado como par�metro
	IF TYPE ('path_prog') <> 'C' OR LEN (path_prog) < 17
		= FINAL (40)
	ENDIF

	gl_alta = .T.
	gl_modi = .T.
	gl_baja = .T.
	gl_cons = .T.
	gl_acce	= .T.

	* Almacena estados iniciales
	PRIVATE area_act, adtoa_used, usr_used, prog_used
	* Almacena �rea actual
	area_act = SELECT ()		

	**********************
	* Apertura de Tablas *
	**********************
	* Verifica si tabla de auditor�a est� usada
	IF USED ('adtoa')
		SET ORDE TO 1 IN ADTOA
*!*			DBSETORD(1, "ADTOA")
		adtoa_used = .T.
	ELSE
		= USET ('ASA\ADTOA')
		adtoa_used = .F.
	ENDIF
	* Verifica si tabla de usuarios est� usada
	IF USED ('usr')
		SET ORDER TO 1 IN USR
*!*			DBSETORD(1, "USR")
		usr_used = .T.
	ELSE
		= USET ('ASA\USR')
		usr_used = .F.
	ENDIF
	* Verifica si tabla de restricciones por usuario est� usada
	IF USED ('usr_rest')
		SET ORDER TO 1 IN USR_REST
*!*			DBSETORD(1, "USR_REST")
		usr_r_used = .T.
	ELSE
		= USET ('ASA\USR_REST')
		usr_r_used = .F.
	ENDIF
	* Verifica si tabla de programas est� usada
	IF USED ('prog')
		SET ORDER TO 1 IN PROG
*!*			DBSETORD(1, "PROG")
		prog_used = .T.
	ELSE
		= USET ('ASA\PROG')
		prog_used = .F.
	ENDIF

	* Establece el Programa
	_EFIMUNI = UPPER (ALLTRIM (_EFIMUNI))
	path_prog = UPPER (ALLTRIM (path_prog))

	* si no encuentra _EFIMUNI en el PATH es error
	IF TYPE('_EFITEST') = 'C' AND !EMPTY(_EFITEST)
		_EFITEST 	= ALLT(_EFITEST)
		path_prog = RIGHT (path_prog, LEN(path_prog) - RAT ('\',path_prog,3))
	ELSE
		IF ! (_EFIMUNI $ path_prog)
			=final (100)
		ELSE
			path_prog = SUBSTR (path_prog, AT (_EFIMUNI, path_prog) + LEN (_EFIMUNI))
		ENDIF
	ENDIF

	* saca sistema y modulo
	primer_bar = AT ('\', path_prog)
	segund_bar = AT ('\', path_prog, 2)
	m.sistema  = SUBSTR (path_prog, 1, primer_bar - 1)
	m.modulo   = SUBSTR (path_prog, primer_bar + 1, segund_bar - primer_bar - 1)
	m.programa = SUBSTR (path_prog, RAT ('\', path_prog) + 1)
	IF AT ('.', m.programa) > 0
		m.programa = SUBSTR (m.programa, 1, AT ('.', m.programa) - 1)
	ENDIF

	* ### XXX : Revisar este bloque para acomodar a funciones C/S

	* Establece el Usuario
	m.usuario = PADR (_USUARIO, 10)
*	exp_usr = GETKEY ('USR', .T., 1)
	IF ! SEEK (PADR (_USUARIO, 10), 'USR')
		IF 'APROGRA' $ UPPER (m.programa)
			GO TOP IN USR
			IF EOF ('USR')
				= ADD_SUPER ()
			ELSE
*				IF USR.usuario = "ADMIN"
*					SKIP 1
*					IF EOF ('USR')
*						= ADD_SUPER ()
*					ELSE
*						=final (10)
*					ENDIF
*				ELSE
					=final (10)
*				ENDIF
			ENDIF
		ELSE
			=final (10)
		ENDIF
	ENDIF		

*	* Establece el Programa
*	_EFIMUNI = UPPER (ALLTRIM (_EFIMUNI))
*	path_prog = UPPER (ALLTRIM (path_prog))
*	* si no encuentra _EFIMUNI en el PATH es error
*	IF ! (_EFIMUNI $ path_prog)
*		=final (100)
*	ELSE
*		path_prog = SUBSTR (path_prog, AT (_EFIMUNI, path_prog) + LEN (_EFIMUNI))
*	ENDIF

*	* saca sistema y modulo
*	primer_bar = AT ('\', path_prog)
*	segund_bar = AT ('\', path_prog, 2)
*	m.sistema  = SUBSTR (path_prog, 1, primer_bar - 1)
*	m.modulo   = SUBSTR (path_prog, primer_bar + 1, segund_bar - primer_bar - 1)
*	m.programa = SUBSTR (path_prog, RAT ('\', path_prog) + 1)
*	IF AT ('.', m.programa) > 0
*		m.programa = SUBSTR (m.programa, 1, AT ('.', m.programa) - 1)
*	ENDIF
	
	m.sistema  = PADR (m.sistema , 8)
	m.modulo   = PADR (m.modulo  , 8)
	m.programa = PADR (m.programa, 8)
	
	* Verifica si existe programa en tabla de programas
	exp_ind = getkey ('prog', .T., 3)
	IF ! SEEK (EVAL(exp_ind), 'PROG')
		IF 'APROGRA' $ UPPER (m.programa)
			SELE PROG
			APPEND BLANK
			REPLACE sistema WITH m.sistema ;
					modulo WITH m.modulo ;
					programa WITH m.programa ;
					descr_prog WITH 'Actualizaci�n de Programas' ;
					baja WITH 10 ;
					alta WITH 10 ;
					modif WITH 10 ;
					consult WITH 10 ;
					access WITH 10
		ELSE
			IF TYPE('_EFITEST') # 'C'
				=final (20)
			ENDIF
		ENDIF
	ENDIF
	
	m.cla_prog = prog.cla_prog
	
	* Verifica permisos para usuario por programa
	exp_ind = getkey ('USR_REST', .T., 4)
	IF SEEK (EVAL(exp_ind), 'USR_REST')
		nivel_alta = USR_REST.alta
		nivel_baja = USR_REST.baja
		nivel_modi = USR_REST.modif
		nivel_cons = USR_REST.consult
		nivel_acce = USR_REST.access
	ELSE
		nivel_alta = usr.alta
		nivel_baja = usr.baja
		nivel_modi = usr.modif
		nivel_cons = usr.consult
		nivel_acce = usr.access
	ENDIF

	* se compara el nivel del usuario con el del programa si es < es .F.
	gl_alta = nivel_alta >= prog.alta
	gl_modi = nivel_modi >= prog.modif
	gl_baja = nivel_baja >= prog.baja
	gl_cons = nivel_cons >= prog.consult

	* Establece auditoria
	* se intenta obtener el ultimo registro de adtoa
	* si no se puede lockear se queda en un bucle
	* <verif>
	* si no existe auditoria, que se hace ?	
	GO BOTTOM IN adtoa
*!*		DBGOBOTTOM ("adtoa")
	IF ! EOF ('ADTOA')
		IF ! RLOCK ('adtoa')
			WAIT WINDOW "Intentando bloquear auditor�a." + CHR (13) + ;
			" El usuario " + ALLTRIM (m.USUARIO) + " esta bloqueando el registro." NOWAIT
			
			DO WHILE ! RLOCK ('adtoa')
			ENDDO
			
			WAIT CLEAR
		ENDIF
		ult_num_aud = adtoa.cod_audit
	ELSE
		ult_num_aud = 0
	ENDIF	
	SELECT ADTOA
	IF ! APPEND ()
		=final (101)
	ENDIF

	num_aud = ult_num_aud + 1

	* ultimo numero de auditoria, si es cero avisa


	* ### C/S : Nombre de las variables
	
*!*						modif     with nivel_modif - prog.modif ,;
*!*						consult   with nivel_consult - prog.consult ,;
*!*						access    with nivel_access - prog.access ,;


	REPLACE cod_audit with num_aud ,;
					server    with _server ,;
					wrkstat   with _wkstation ,;
					usuario		WITH m.usuario ,;
					sistema   with m.sistema,;
					modulo    with m.modulo ,;
					programa  with m.programa,;
					baja      with nivel_baja - prog.baja ,;
					alta      with nivel_alta - prog.alta ,;
					modif     with nivel_modi - prog.modif ,;
					consult   with nivel_cons - prog.consult ,;
					access    with nivel_acce - prog.access ,;
					cod_tiprg with prog.cod_tiprg ,;
					fech_ingr with DATE () ,;
					hor_ingr  with INT (STOT (SECONDS ())) ,;
					fech_sal  with DATE () ,;
					hor_sal   with INT (STOT (SECONDS ())) ,;
					cod_err   with -1      

	UNLOCK

	IF nivel_acce < prog.access
		=final (30)		
	ENDIF

	*	Cierra tabla que no estaban abiertas previamente
	IF ! adtoa_used
		USE IN adtoa 
	ENDIF
	IF ! usr_used
		USE IN usr 
	ENDIF
	IF ! usr_r_used
		USE IN usr_rest
	ENDIF
	IF ! prog_used
		USE IN prog 
	ENDIF
	
*	IF TieneAyuda()	
	IF .T.
		ON KEY LABEL F1 DO HELP_ME
		IF TYPE ('nombre_ay') = "U"
			nombre_ay = ""
		ENDIF
		* SET HELPFILTER TO sistema = m.sistema AND nombre_ay = m.programa
	ELSE
		ON KEY LABEL F1 DO msgnohelp
	ENDIF
			
	* Setea variables del Programa
	sistema = m.sistema
	modulo  = m.modulo
	programa= m.programa
	usuario = m.usuario
	
	*	restablece la tabla anterior
	SELECT (area_act)

	DEACTIVATE MENU ALL

	WAIT CLEAR
	_si_v_imp = 'S'
	_nomrep		= ''
	
	PUBLIC _click_bot		&& ### VFP: Para soporte de rat�n en ventanas secundarias
	_click_bot = ""
	
*!*		IF _SQL_FLAG 			&& ### VFP: Para soporte de bloqueos en SQL
*!*			BLOQUEA ("*INICIO")
*!*			RELEASE _SQL_SESSION

*!*			SQLDISCONNECT(0)
*!*			** _SQL_HND = 0		### CNX
*!*			_SQL_HND = SQLCONNECT ("ConexionBD")
*!*			IF _SQL_HND < 1
*!*				WAIT WINDOW "No pudo completarse la conexi�n con el motor de base de datos"
*!*				CANCEL
*!*			ENDIF
*!*			_SQL_HND_TMP = 0
*!*			_SQL_DB_TMP = GEN_DB_TMP()
*!*			CLOSE TABLES ALL

*!*			IF VARTYPE(_SQL_OBJ)=="U"
*!*				_SQL_OBJ = CREATEOBJECT ("SQLDMO.SQLServer")
*!*				_SQL_OBJ.Connect ("SICO08", "sa", "")	&& ### XXX : C�digo no gen�rico, ver de parametrizar
*!*	*<>
*!*				GEN_SQL_COL()
*!*				* _SQL_OBJ_DB = _SQL_OBJ.Databases (DBGETPROP("ConexionBD", "Connection", "Database"))
*!*	*<>
*!*			ENDIF
*!*			DIMENSION _SQL_TAB_INFO [1]
*!*			_SQL_TAB_INFO [1] = .F.
*!*		ENDIF
	
	RETURN .T.

*************************************************
FUNCTION HELP_ME
*************************************************
PARAMETERS sis_help
PRIVATE retorno, helperr, errant
retorno = .T.
PUSH KEY
ON KEY LABEL F1 *
ON KEY LABEL F2 *
ON KEY LABEL F3 *
ON KEY LABEL F4 *
ON KEY LABEL F5 *
ON KEY LABEL F6 *

SET TOPIC ID TO
SET TOPIC TO

USET("ASA\SISTEMAS","PRIMARIO","SIS_TMP")
IF EMPTY(m.sis_help)
	USET("ASA\PROG","PRIMARIO","PRG_TMP")
	=SEEK(m.sistema,"SIS_TMP")
	IF SEEK(m.sistema + m.modulo + m.programa, "PRG_TMP")
		SET TOPIC ID TO PRG_TMP.HELP_ID
	ENDIF
	USE IN PRG_TMP
ELSE
	=SEEK(m.sis_help,"SIS_TMP")
ENDIF

m.helperr = .F.
m.errant = ON('ERROR')
ON ERROR m.helperr = .T.
SET HELP TO ALLTRIM(SIS_TMP.HELP_FILE) + ".CHM"
IF m.helperr
	m.helperr = .F.
	SET HELP TO ALLTRIM(SIS_TMP.HELP_FILE) + ".HLP"
ENDIF
ON ERROR &errant

IF !m.helperr
	HELP
ENDIF

USE IN SIS_TMP
POP KEY
RETURN retorno

*************************************************
FUNCTION add_super
*************************************************
SELECT USR
APPEND BLANK
REPLACE usuario WITH 'SUPERVISOR' ;
		nom_usr WITH 'Supervisor del Sistema' ;
		men_inic WITH 'CONFIGURAC' ;
		sis_inic WITH 'ASA' ;
		baja WITH 99 ;
		alta WITH 99 ;
		modif WITH 99 ;
		consult WITH 99 ;
		access WITH 99 ;
		arranque WITH MENU
= Final (11, 'Como no hab�a Usuarios dados de Alta ha sido a�adido un usuario con el nombre de SUPERVISOR. Debe loguearse como SUPERVISOR y volver a entrar al sistema para poder acceder.')

*************************************************
FUNCTION final
*************************************************
	PARAMETERS m.est_final, x_mensaje, adtoa_used
	PRIVATE retorno
	retorno = .T.
	*	auditoria
	*	cierra tablas desde el vector
	*	quit
	UNLOCK ALL
	IF PARAMETERS () > 0
		IF m.est_final > 0
			IF TYPE ('x_mensaje') = 'L'
				x_mensaje = ""
			ENDIF

			DO CASE
			CASE m.est_final = 1
			CASE m.est_final = 10
				=MSG ('Error 10: No se encuentra el usuario ' + ;
				ALLTRIM (m.usuario) + ' en las tablas de ASA')
			CASE m.est_final = 20
				=MSG ('Error 20: No se encuentra el programa ' + ;
				ALLTRIM (m.sistema) + '\' + ALLTRIM (m.modulo) + ;
				'\' + ALLTRIM (m.programa) + ' en las tablas de ASA')
			CASE m.est_final = 30
				=MSG ('Error 30: El usuario ' + ALLTRIM (m.usuario) + ;
				' no tiene acceso al programa ' + ;
				ALLTRIM (m.sistema) + '\' + ALLTRIM (m.modulo) + '\' + ;
				ALLTRIM (m.programa) )
			CASE m.est_final = 40
				=MSG ('Error 40: Se pas� mal el argumento del nombre del programa.')
			CASE m.est_final = 100
				=MSG ('Error 100: No se encuentra _EFIMUNI: ' + ALLTRIM (_efimuni) + ;
				' en el path ' + ALLTRIM (path_prog) )
			CASE m.est_final = 101
				=MSG ('Error 101: No se pudo a�adir un registro en la tabla de auditor�a')
			CASE m.est_final = 1000
				=MSG ('Error 1000 : Error al abrir archivo ' + ALLTRIM (x_mensaje) )
			CASE m.est_final = 1001
				=MSG ('Error 1001 : Error al abrir �ndice ' + ALLTRIM (x_mensaje) )
			CASE m.est_final = 1002
				=MSG ('Error 1002 : No se pudo abrir la tabla '+UPPER(ALLTRIM(x_mensaje))+' en forma EXCLUSIVA. Est� siendo utilizada en otra terminal.')
			CASE m.est_final = 1003
				=MSG ('Error 1003 : No se pudo abrir la tabla '+UPPER(ALLTRIM(x_mensaje))+'. Est� siendo utilizada en forma EXCLUSIVA en otra terminal.')
			CASE m.est_final = 1004
				=MSG ('Error 1004 : Error, NO existe el �ndice '+UPPER(ALLTRIM(x_mensaje))+'.')
			CASE m.est_final = 100001
				=MSG ('Error 100001 : NUMERO ERRONEO DE PARAMETROS' + CHR(13) +;
						   CHR(13) + 'La Funci�n ' + UPPER(nomb_fun) +;
						  ' debe ser invocada con ' + alltrim(str(nro_par,2,0)) +;
							' par�metros.')
			CASE m.est_final = 100002
				=MSG ('Error 100002 : TIPO ERRONEO DE PARAMETRO' + CHR(13) +;
						  CHR(13) + 'El par�metro ' + UPPER(nomb_par) +;
						  ' pasado a la Funci�n ' +	UPPER(nomb_fun) +;
						  ' debe ser de tipo ' + UPPER(nomb_tip) + '.')
			OTHERWISE
				=MSG ("Error nro : " + STR(m.est_final,6) + "  " + CHR (13) + x_mensaje)
			ENDCASE
		ENDIF
	ELSE
		m.est_final = 0
	ENDIF

*	IF SET('DEBUG') = 'OFF'
	IF !("SICO" $ _SERVER)
		= RLLBACK ()
	ENDIF
	
	IF TYPE ('num_aud') = 'N' AND num_aud > 0
		* Verifica si tabla de auditor�a est� usada
		IF USED ('adtoa')
			SET ORDE TO 1 IN ADTOA
*!*				DBSETORD(1, "ADTOA")
			adtoa_used = .T.
		ELSE
			= USET ('ASA\ADTOA')
			adtoa_used = .F.
		ENDIF
		exp_ind = getkey ('adtoa', .T., 1)
		m.cod_audit = num_aud
		IF SEEK (EVAL(exp_ind), 'ADTOA')
*!*			IF DBSEEK (exp_ind, 'ADTOA')
			SELECT ADTOA
			IF RLOCK ('ADTOA')
				REPLACE cod_err with m.est_final ,;
								fech_sal  with DATE () ,;
								hor_sal   with INT (STOT (SECONDS ()))
				UNLOCK
			ELSE
				WAIT WINDOW "No se pudo bloquear la auditor�a inicial."		
			ENDIF
		ELSE
			WAIT WINDOW "No se pudo encontrar la auditor�a inicial."
		ENDIF
		IF ! adtoa_used
			USE IN ADTOA		
		ENDIF
	ENDIF

	IF EMPTY (WONTOP())
		* @ 24,0 CLEAR	### C/S
	ENDIF 

	IF m.est_final > 0
		IF !("SICO" $ _SERVER)
			CLOSE TABLES ALL
			SET SYSMENU ON
			SET SYSMENU TO DEFA
			RETURN TO MASTER
		ELSE
			retorno = .F.
		ENDIF
	ENDIF
	
	RETURN retorno
	

*************************************************
FUNCTION VerNuPar
*************************************************
* Autor: FERNANDO
* 
* Fecha: 23/11/1994
* 
* Funcionamiento: Verifica que sea correcto el N�mero
* de Par�metros pasados a una Funci�n. Si es correcto
* devuelve TRUE; si no, emite un mensaje de error y
* aborta el programa.
* 
* Par�metros: nomb_fun (C) = nombre de la funci�n
* 						nro_parp (N) = n�mero de par�metros pasados a la funci�n
*							nro_par  (N) = n�mero correcto de par�metros de la funci�n
* 
*
PARAMETERS nomb_fun, nro_parp, nro_par

IF nro_parp <> nro_par
	DO final WITH 100001
ENDIF

RETURN .T.

*************************************************
FUNCTION VerTiPar
*************************************************
* Autor: FERNANDO
* 
* Fecha: 23/11/1994
* 
* Funcionamiento: Verifica que sea correcto el Tipo de
* un Par�metro pasado a una Funci�n. Si es correcto
* devuelve TRUE; si no, emite un mensaje de error y
* aborta el programa.
* 
* Par�metros: nomb_fun (C) = nombre de la funci�n
* 						nomb_par (C) = nombre del par�metro
*							tipo_par (C) = tipo correcto del par�metro
* 
*
PARAMETERS nomb_fun, nomb_par, tipo_par

IF (TYPE(nomb_par) <> tipo_par)
	DO CASE
		CASE tipo_par = 'C'
			nomb_tip = 'CHARACTER'
		CASE tipo_par = 'N'
			nomb_tip = 'NUMERIC'
		CASE tipo_par = 'D'
			nomb_tip = 'DATE'
		CASE tipo_par = 'L'
			nomb_tip = 'LOGICAL'
		CASE tipo_par = 'M'
			nomb_tip = 'MEMO'
		OTHERWISE
			nomb_tip = 'INDEFINIDO'
	ENDCASE

	DO final WITH 100002
ENDIF

RETURN .T.

*************************************************
FUNCTION VerAmbien
*************************************************
* Funcionamiento
*	 Muestra los seteos de las variables de ambiente si existen 
*************************************************
PUSH KEY 
= DEAC_TEC()
DO FORM FORM_AMB
POP KEY 
RETURN

************************
FUNCTION TieneAyuda
************************
PRIVATE retorno, ant_help
ant_help = SET( 'help' , 1 )
SET HELP OFF
=USET ('asa\ayuda')
retorno = SEEK( m.sistema + m.programa , 'ayuda' )
USE IN ayuda
* SET HELP TO &ant_help		### VFP
IF FILE(ant_help)
	SET HELP TO (ant_help)
	SET HELP ON
ENDIF
RETURN retorno
*

************************
FUNCTION msgnohelp
************************
ON KEY LABEL F1
=msg( 'Ayuda del Programa en Preparaci�n.' )
ON KEY LABEL F1 DO msgnohelp
RETURN
*


************************
FUNCTION FFuncs
************************
PARA funcion

DO CASE
CASE funcion = 'F7'
	IF FILE ('funcf7.app')
		func='funcf7'
		DO &func..app
	ENDI
CASE funcion = 'F8'
	IF FILE ('funcf8.app')
		func='funcf8'
		DO &func..app
	ENDI
CASE funcion = 'F9'
	IF FILE ('funcf9.app')
		func='funcf9'
		DO &func..app
	ENDI
CASE funcion = 'F10'
	IF FILE ('funcf10.app')
		func='funcf10'
		DO &func..app
	ENDI
CASE funcion = 'F11'
	IF FILE ('funcf11.app')
		func='funcf11'
		DO &func..app
	ENDI
CASE funcion = 'F12'
	IF FILE ('funcf12.app')
		func='funcf12'
		DO &func..app
	ENDI
ENDC
RETU
****************************
FUNCTION DTOSTR
****************************
* Autor : CRISTIAN
*
* Fecha : 7/11/94
*
* Ultima Modificaci�n : 7/11/94
*
* Convierte una fecha completa (dd/mm/aa) en una cadena en la que se indica
* el nombre del mes.
* 
*
* Ejemplo1 :    Llamada .... DTOSTR ({12/5/94}, .T.)
*
*									Resultado....'12 de Mayo de 1994'
*
* Ejemplo2 :    Llamada .... DTOSTR ({12/5/94}, .F.)
*
*									Resultado....'12 de Mayo'
* 
* Par�metros :      wfecha  (D)  : fecha a convertir.
*										con_anio (L) : flag que determina si se indica el anio.
*																	 .T. : en la cadena se indica el a�o.
*																	 .F. : en la cadena NO se indica el a�o.
*
***************************************************************************


PARAMETERS wfecha, con_anio
PRIVATE cadena, wdia, wmes, wanio, wnomes, retorno

retorno = .T.
cadena  = ''

IF SET('DEBUG') = 'ON'
	IF PARAMETER() < 2
		WAIT WIND 'El Nro. de par�metros pasados a DTOSTR () es incorrecto.'
		retorno = .F.
	ELSE
		IF TYPE('wfecha') # 'D' OR TYPE('con_anio') # 'L'
			WAIT WIND 'Tipo de par�metro incorrecto pasado a DTOSTR ().'
			retorno = .F.
		ENDIF
	ENDIF
ENDIF

wdia   = DAY (wfecha)
IF wdia = 0
	retorno = .F.
	??CHR(7)
	= msg('ERROR: Debe pasar una fecha a la funci�n DTOSTR()')
ENDIF
IF retorno

	wdia   = DAY (wfecha)
	wmes   = STR (MONTH (wfecha), 2)
*	wnomes = TI_CONTA ('MESESINT','descl','wmes')
	wnomes = tab_int( "MESESINT", 'DESCL', 'wmes','','CONTA')
	wanio  = YEAR (wfecha)

	cadena = ALLT (STR (wdia)) + ' de ' + ALLT (wnomes)

	IF con_anio
		cadena = cadena + ' de ' + ALLT (STR (wanio, 4))
	ENDIF
ENDIF

RETURN cadena
*
*************************************************
FUNCTION deac_tec
*************************************************
* Funcionamiento:
* 
* 
* 
* 
* Par�metros:
* 
* 
* 
PARAMETERS desactiva

IF TYPE ('desactiva') == 'L' 
	IF desactiva = .T.
		RETURN
	ENDIF
	ON KEY LABEL F5 DO deac_tec with .t.
	ON KEY LABEL F6 DO deac_tec with .t.
	ON KEY LABEL F3 DO deac_tec with .t.
	ON KEY LABEL F2 DO deac_tec with .t.
	ON KEY LABEL CTRL+A DO deac_tec with .t.
	ON KEY LABEL CTRL+S DO deac_tec with .t.
	ON KEY LABEL F4 DO deac_tec with .t.
	ON KEY LABEL F7 DO deac_tec with .t.
	ON KEY LABEL F8 DO deac_tec with .t.
	ON KEY LABEL F9 DO deac_tec with .t.
	ON KEY LABEL F10 DO deac_tec with .t.
	ON KEY LABEL F11 DO deac_tec with .t.
	ON KEY LABEL F12 DO deac_tec with .t.
	RETURN
ENDIF

IF NOT ('F5' $ desactiva)
	ON KEY LABEL F5 DO deac_tec with .t.
ENDIF
IF NOT ('F6' $ desactiva)
	ON KEY LABEL F6 DO deac_tec with .t.
ENDIF
IF NOT ('CTRL+A' $ desactiva)
	ON KEY LABEL CTRL+A DO deac_tec with .t.
ENDIF
IF NOT ('CTRL+S' $ desactiva)
	ON KEY LABEL CTRL+S DO deac_tec with .t.
ENDIF
IF NOT ('F3' $ desactiva)
	ON KEY LABEL F3 DO deac_tec with .t.
ENDIF
IF NOT ('F2' $ desactiva)
	ON KEY LABEL F2 DO deac_tec with .t.
ENDIF
IF NOT ('F4' $ desactiva)
	ON KEY LABEL F4 DO deac_tec with .t.
ENDIF
IF NOT ('F7' $ desactiva)
	ON KEY LABEL F7 DO deac_tec with .t.
ENDIF
IF NOT ('F8' $ desactiva)
	ON KEY LABEL F8 DO deac_tec with .t.
ENDIF
IF NOT ('F8' $ desactiva)
	ON KEY LABEL F8 DO deac_tec with .t.
ENDIF
IF NOT ('F9' $ desactiva)
	ON KEY LABEL F9 DO deac_tec with .t.
ENDIF
IF NOT ('F10' $ desactiva)
	ON KEY LABEL F10 DO deac_tec with .t.
ENDIF
IF NOT ('F11' $ desactiva)
	ON KEY LABEL F11 DO deac_tec with .t.
ENDIF
IF NOT ('F12' $ desactiva)
	ON KEY LABEL F12 DO deac_tec with .t.
ENDIF
***********************************************
 FUNCTION conv_str
***********************************************
PARAMETERS m.algo
PRIVATE ret

DO CASE
CASE TYPE( 'm.algo' ) = 'N'
	ret = STR( m.algo ) 
CASE TYPE( 'm.algo' ) = 'D'
    ret = DTOC( m.algo )
OTHERWISE
	ret = m.algo
ENDCASE

ret = ALLTRIM( ret )

RETURN ret 
*
*************************************************
FUNCTION es_num
*************************************************
* Autor:
* 
* Fecha:
* 
* Funcionamiento:
* 
* 
* 
* 
* Par�metros:
* 
* 
* 
* Modificaciones:
* 

PARAMETER cadena, muestra_mens
PRIVATE long_cad, car, retorno
cadena = ALLTRIM(cadena)
retorno = .T.
long_cad = LEN(cadena)
car = SUBSTR(cadena, 1, 1)
IF ! ISDIGIT(car) AND ! INLIST(car,'-','+')
	retorno = .F.
ENDIF
IF retorno AND long_cad > 1
	FOR i = 2 TO long_cad
		car = SUBSTR(cadena, i, 1)
		IF ! ISDIGIT(car) 
			retorno = .F.
			EXIT
		ENDIF
	ENDFOR
ENDIF
IF ! retorno AND muestra_mens
	=MSG('El dato no es num,rico')	
ENDIF		
RETURN retorno
*
*************************************************
FUNCTION ISLOGIC
*************************************************
* Funcionamiento:
* 								Valida si se ingresa una opcion
* l�gica ( S � N ). Sino es v�lida abre un POP-UP
* mostrando las opciones
* 
* Par�metros:
* 						CAMPO (C) : Nombre del campo a validar		
* 

PARAMETERS campo
PRIVATE menusito, retorno, linea

retorno = .T.
IF EMPTY(campo)
	WAIT WIND 'Error al pasar los Parametros. Funci�n ISLOGIC()'
	RETU
ENDIF

PUSH KEY 
ON KEY LABEL F1 *
ON KEY LABEL F2 *
ON KEY LABEL F3 *
ON KEY LABEL F4 *
ON KEY LABEL F5 *
ON KEY LABEL F6 *
IF LAST()#27
	IF (UPPER(&campo) # 'S' AND UPPER(&campo) # 'N') OR LAST() == -2

#IFNDEF SINO_MSGB

		linea = row()
		IF linea > 16
			linea = 16
		ENDIF
		DEFINE WINDOW menusito AT linea+3,col()+2 SIZE 2 ,12;
			IN SCREEN;
			NOCLOSE;
			NONE
		ACTI WIND menusito
		i = 0
		DIMENSION tab_sino (2)
		tab_sino (1) = ' SI'
		tab_sino (2) = ' NO'
		@0,0 MENU tab_sino, 2 TITLE 'Confirmaci�n'
		READ MENU TO i
		DEACT WIND menusito
		IF i = 0
			retorno = .F.
		ELSE
			&CAMPO = SUBSTR(tab_sino(i),2,1)
			retorno = .T.
		ENDIF

#ELSE
		i = MESSAGEBOX("Confirmaci�n", 3, "")

		DO CASE
		CASE i==6	&& Si
			&CAMPO = "S"
			retorno = .T.
		CASE i==7	&& No
			&CAMPO = "N"
			retorno = .T.
		OTHERWISE
			retorno = .F.
		ENDCASE
#ENDIF

	ENDIF
ENDIF

SHOW GET &CAMPO
POP KEY
RETU retorno
*
*************************************************
FUNCTION ACT_WIND
*************************************************
PARAMETERS vent
PRIVATE retorno, objeto
retorno = .T.
IF !EMPTY(WOUTPUT())
	objeto = _CUROBJ	
ENDIF
ACTIVATE WIND (vent) TOP
IF !EMPTY(WOUTPUT())
	* _CUROBJ = objeto		### VFP
	SET_CUROBJ (objeto)
ENDIF
	
RETURN retorno
*
***************************************
 FUNCTION get_nro
***************************************
PARAMETERS m.alias, m.nombre
PRIVATE  i, cant, enc, ret

i    = 1
cant = fcount( m.alias )
enc  = .F.
ret  = 0

DO WHILE ( i <= cant )  .AND. .NOT.enc
   IF FIELD(i) == UPPER( m.nombre )
     enc = .T.
     ret =  i 
   ENDIF
   i = i + 1
ENDDO

RETURN ret
*
*************************************************
FUNCTION msg
*************************************************
* Funcionamiento:
* 	Muestra un mensaje dentro de una ventana y para salir se debe 
*	 presionar una tecla
* Par�metros
*  [mensaje] : (C) ('Presione ENTER para continuar') Mensaje que se 
*							 quiere mostrar.
**************************************************

PARAMETERS m.mensaje, nowait
PUSH KEY
	=DEAC_TEC()

	PRIVATE m.mensaje, largomen, m.pos, m.pos1, largomemo, m.altura
	IF TYPE('m.mensaje') <> "C"
		m.mensaje = "Presione ENTER para continuar"
	ENDIF

	KEYBOARD '{shift+f12}' CLEAR
	WAIT WIND ''

	IF TYPE ('nowait') <> 'L'
		nowait = .T.
	ENDIF	

	largomemo = SET("MEMOWIDTH")
	m.altura = 1
	m.mensaje = ALLTRIM(m.mensaje)
	largomen = LEN(m.mensaje)
	IF largomen < 25
		largomen = 30
	ELSE
		IF largomen > 40
			SET MEMOWIDTH TO 40
			m.altura = MEMLINES(m.mensaje)
			largomen = 46
		ELSE
			largomen = largomen+6
		ENDIF
	ENDIF

	DEFINE WINDOW MSG_VENTMEN ;
		FROM	INT ( (SROW()-(6+m.altura)) / 2 ),;
					INT ( (SCOL()-largomen) / 2 );
		TO	INT ( (SROW()-(6+m.altura)) / 2 ) + (5+m.altura),;
				INT ( (SCOL()-largomen) / 2 ) +largomen-1 ;
		FLOAT ;
		NOCLOSE ;
		SHADOW ;
		DOUBLE ;
		COLOR SCHEME 7

	ACTIVATE WINDOW MSG_VENTMEN NOSHOW

	PRIVATE m.mensaje2	&& ### VFP
	m.mensaje2 = ""		&& ### VFP
	
	FOR i_msg=1 TO m.altura
		m.pos = (largomen - 2 - LEN (MLINE (m.mensaje, i_msg)) ) / 2
		@ i_msg, m.pos SAY MLINE (m.mensaje, i_msg)
		m.mensaje2 = m.mensaje2 + MLINE (m.mensaje, i_msg)+CHR(13)+CHR(10)
	ENDFOR 

	m.pos1 = (largomen - 2 - 8) / 2

	* ### C/S
*!*		IF ! nowait
*!*			@ m.altura+2, m.pos1 GET m.confirm ;
*!*				PICTURE "@*HN \!\?Acepta" ;
*!*				SIZE 1, 8, 3 ;
*!*				DEFAULT 1 ;
*!*				VALID msg_conf_valid() ;
*!*				MESSAGE "Seleccione Acepta para continuar"

*!*			ACTIVATE WINDOW MSG_VENTMEN
*!*			READ CYCLE MODAL TIMEOUT 600

*!*			RELEASE WINDOW MSG_VENTMEN
*!*		ENDIF

	RELEASE WINDOW MSG_VENTMEN
	* WAIT WIND m.mensaje2
	=MESSAGEBOX (m.mensaje2, "Consulta")

	SET MEMOWIDTH TO largomemo
	
	IF EMPTY ( WONTOP())
		* @ 24,0 CLEAR	### C/S
	ENDIF

	KEYBOARD '{shift+f12}' CLEAR
	WAIT WIND ''
	POP KEY
	
* FUNCTION msg_conf_valid   &&  m.confirm VALID
* 	CLEAR READ		### VFP

*


*************************************************
FUNCTION SKIPR
*************************************************
* Autor: RODRIGO
* 
* Fecha: 23/12/94
* 
* Funcionamiento: Saltea los repetidos cuando se hace un ABM
* que funciona con clave parcial.
* 
* Par�metros: NO TIENE
*
*	CONSIDERACIONES:
* 
*		Funciona solo si es usado desde un ABM GENERAL.
*		Esta funci�n MUEVE EL PUNTERO.!
* 
* Modificaciones:
*
 
PRIVATE retorno
LOCAL vfp_ctrl

retorno = .T.
m.vfp_ctrl = IIF(TYPE("m.campo_ctrl") = 'C', m.campo_ctrl, "")

*IF LASTKEY() = 1 OR VARREAD()='ANTERIOR' OR LASTKEY() = 19 OR VARREAD()='SIGUIENTE'
IF LASTKEY() = 1 OR VARREAD()='ANTERIOR' OR LASTKEY() = 19 OR VARREAD()='SIGUIENTE';
 OR vfp_ctrl $ "ANTERIOR|SIGUIENTE"
	SELECT (tab_mae)
	IF SEEK(EVAL(clave_mae),tab_mae)
*		IF LASTKEY() = 1 OR VARREAD()='ANTERIOR'
		IF LASTKEY() = 1 OR VARREAD()='ANTERIOR' OR vfp_ctrl == "ANTERIOR"
			SKIP -1
			IF BOF()
				GO TOP
				WAIT WINDOW 'Est� en el primer registro.' NOWAIT
			ENDIF
		ELSE
*			IF LASTKEY() = 19 OR VARREAD()='SIGUIENTE'
			IF LASTKEY() = 19 OR VARREAD()='SIGUIENTE' OR vfp_ctrl == "SIGUIENTE"
				=SEEK (EVAL(clave_mae)+'z',tab_mae)
				IF EOF()
					GO BOTTOM
					WAIT WINDOW 'Est� en el �ltimo registro' NOWAIT
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDIF
RETURN retorno
*
*************************************************
FUNCTION get_para
*************************************************
* Funcionamiento:
* Busca en el archivo de configuracion la variable por sistema
* Si el sistema es 'conta' y existe _EJERC se busca en el ejercicio
* 
* 
* Par�metros:
* sistema : al que pertenece la variable
* nomb_par : nombre del parametro que se busca

	PARAMETERS m.sistema, m.nomb_par
	PRIVATE m.v_ret, m.p_exp

*	IF LOWER (m.sistema) == 'conta' AND ;
*		TYPE ('_EJERC') = 'C' AND ;
*		!EMPTY (_EJERC)	
*			if ! uset (_EJERC + '\' + 'config')
*				RETURN ""
*			ENDIF
*	ELSE
*		IF ! uset('asa\config')
=USET ('asa\config')
*			RETURN ""
*		ENDIF
*	ENDIF

* Se pone la expresion del indice porque los campos que se pasan 
* por lo general no son de la base de datos.

m.exp = PADR (LOWER (m.sistema), 8) + PADR (UPPER (m.nomb_par), 15)
* IF ! _SQL_FLAG
* IF ! _SQL_FLAG OR CURSORGETPROP ("SourceType", "config")<>2

* ### XXX
*!*	m.p_exp = [ PADR (LOWER (m.sistema), 8) | PADR (UPPER (m.nomb_par), 15) ]

IF SEEK (m.exp, 'config')
*!*	IF DBSEEK (m.p_exp, 'config') 
	m.v_ret = config.cont_par
ELSE
	m.v_ret = ''
ENDIF

USE in config
RETURN m.v_ret
*

**********************************
 FUNCTION str_to
**********************************
PARAMETERS tipo_cpo , valor
PRIVATE ret

DO CASE
CASE tipo_cpo = 'N'
  ret = VAL( valor )
CASE tipo_cpo = 'D'
  ret = CTOD( valor )
OTHERWISE
  ret = valor
ENDCASE

RETURN ret
**************************************************
FUNCTION TAB_INT
*************************************************
*
* ULTIMA ACTUALIZACION	: 04/07/95
*		23/02 : Se le agrega el pedido VALIDL que retorna la descripci�n larga.
*						En la ventana de VALID Y VALIDL muetra como t�tulo la descrip.
*						de la tabla cabecera en lugar del tipo de C�d. interno.
*	
*		04/07 : 
*
* AUTOR		: Cristian
* DISE�O	: Fernando
*
* Funcionamiento:
* Las tablas internas se controlan por subsistema. Esta funci�n es com�n
* para los subsistemas : CONTA, RECUR, RECHUM, ASUNT, PYC.
* Controla el pedido y la selecci�n o chequeo de c�digos.

* Modificaciones
* GABRIEL 12/04/95 - 12:30   Cambie ALIAS por SELECT en walias
* Gabriel 150995 - 17:20 Agregue control de SHOW GETS
*
*
***********************************************************************
* 	Par�metros:
****************
** tip_cod	(C) Tipo de c�digo interno.
*
** acci�n	(C) define que quiere hacer con los datos.
*			Opciones :
*			CODIG =	se busca el cod_int en la tabla de c�digos internos; si
*				existe, se devuelve el mismo cod_int, sino devuelve " ".
*			DESCL =	se busca el cod_int; si existe, se devuelve la 
*				descripci�n larga, sino devuelve " ".
*			DESCC =	se busca el cod_int; si existe, devuelve la descripci�n 
*				corta, sino existe se devuelve " ".
*			POPUP =	se abre un popup para seleccionar un c�digo interno; si
*				se selecciona un c�digo se devuelve el cod_int 
*				correspondiente, sino devuelve " ".
*			VALID = se valida el cod_int en la tabla interna; si no 
*				existe, se ofrece una lista con las opciones correctas. Si 
*				se elige una opci�n se devuelve el cod_int y la descripci�n
*				corta, sino se elige devuelve " ".
*
*			VALIDL = se valida el cod_int en la tabla interna; si no 
*				existe, se ofrece una lista con las opciones correctas. Si 
*				se elige una opci�n se devuelve el cod_int y la descripci�n
*				larga, sino se elige devuelve " ".
*
** [cod_int]	(C) C�digo Interno. (para los pedidos CODIG, DESCL, DESCC y 
*				VALID se utiliza para validar la tabla).
*
** [@dsc_ab]	(C) Descripci�n abreviada.
*
** [sistema] (C) Nombre del sistema (CONTA, RECUR, LIQHAB, ASUNT, PYC). Si no se pasa 
*				este par�metro, se considera el sistema indicado en la variable de 
*				ambiente _sistema.
*
** [no_show] (B) Indica si hace SHOW GETS o no. no_show en falso HACE SHOW GETS
*
PARAMETER tip_cod, accion, cod_int, dsc_ab, tsistema, no_show
PRIVATE retorno, status, tab_tci, tab_ci, m.tipcodint, m.codint, wsistema,;
 cant_par, m.dscab_ci, walias

***  Inicializar Variables  *** 

*IF EMPTY (dsc_ab) OR PARAMETERS() <= 3
IF EMPTY (dsc_ab) OR PCOUNT() <= 3
	m.notiene = ''
	dsc_ab 		= 'm.notiene'
ENDIF

retorno  = ''
status   = .T.
tab_tci  = ''
tab_ci   = ''
wsistema = ''
m.tipcodint = SPACE (15)
m.codint    = SPACE (8)
m.dscab_ci  = SPACE (60)
cant_par    = 0
walias      = SELECT()

***  Fin Inicializaci�n  ***

******** Comienzo ***********

*cant_par = PARAMETERS ()
cant_par = PCOUNT()
status   = ChkParam (cant_par)
IF status
	= Ver_Vacio ()
	IF cant_par >= 3
		m.codint = UPPER (&cod_int)
	ENDIF
	tip_cod     = UPPER (tip_cod)
	accion      = UPPER (accion)
	m.tipcodint = PADR ( ALLT (tip_cod), 15, ' ')
	DO CASE
		CASE wsistema == 'CONTA'
			tab_tci = 'TI_CONT'
			tab_ci  = 'TI_CONTS'
		CASE wsistema == 'RECUR'
			tab_tci = 'TI_REC'
			tab_ci  = 'TI_RECS'
		CASE wsistema == 'RECHUM'
			tab_tci = 'TI_HAB'
			tab_ci  = 'TI_HABS'
		CASE wsistema == 'ASUNT'
			tab_tci = 'TI_ASU'
			tab_ci  = 'TI_ASUS'
		CASE wsistema == 'PYC'
			tab_tci = 'TI_PYC'
			tab_ci  = 'TI_PYCS'
		OTHERWISE
			= MSG ('ERROR. El sistema ' + ALLT (wsistema) + ' No existe.')
			status = .F.
	ENDCASE
	IF status
		=Abre_Dbf (tab_tci, tab_ci)
*!*			IF SEEK (m.tipcodint, tab_tci)
		IF SEEK (m.tipcodint, tab_tci, ORDER(tab_tci))
			DO CASE
				CASE accion == 'CODIG'
					IF cant_par >= 3
						IF SEEK (m.tipcodint + m.codint, tab_ci)
							retorno = m.codint
							IF cant_par >=4
								&dsc_ab = &tab_ci..dscab_ci
							ENDIF
						ENDIF
					ELSE
						DO final WITH 100000, 'NUMERO ERRONEO DE PARAMETROS' + CHR(13) + CHR(13) + 'Funci�n TAB_INT ().'
					ENDIF
					
				CASE accion == 'DESCL'
					IF cant_par >= 3
						IF SEEK (m.tipcodint + m.codint, tab_ci)
							retorno = &tab_ci..dsc_ci
							IF cant_par >=4
								&dsc_ab = &tab_ci..dscab_ci
							ENDIF
						ENDIF
					ELSE
						DO final WITH 100000, 'NUMERO ERRONEO DE PARAMETROS' + CHR(13) + CHR(13) + 'Funci�n TAB_INT ().'
					ENDIF
					
				CASE accion == 'DESCC'
					IF cant_par >= 3
						IF SEEK (m.tipcodint + m.codint, tab_ci)
							retorno = &tab_ci..dscab_ci
							IF cant_par >=4
								&dsc_ab = ''
							ENDIF
						ENDIF
					ELSE
						DO final WITH 100000, 'NUMERO ERRONEO DE PARAMETROS' + CHR(13) + CHR(13) + 'Funci�n TAB_INT ().'
					ENDIF
					
				CASE accion == 'POPUP'
					IF cant_par >= 2
						PRIVATE cant_ele, long_win, linea, long_max, i, titulo
						cant_ele = 0
						long_win = 0
						linea    = 0
						long_max = 0
						i        = 0
						titulo   = ''
						IF SEEK (m.tipcodint, tab_ci)
							DIMENSION opcion [30]
							cant_ele = 0
							long_max = LEN (&tab_ci..codint) + LEN (ALLT (&tab_ci..dsc_ci) ) + 3
							DO WHILE &tab_ci..tipcodint == m.tipcodint AND !EOF (tab_ci)
								IF long_max < LEN (&tab_ci..codint) + LEN (ALLT (&tab_ci..dsc_ci) ) + 3
									long_max = LEN (&tab_ci..codint) + LEN (ALLT (&tab_ci..dsc_ci) )+ 3
								ENDIF
								cant_ele   = cant_ele + 1
								opcion (cant_ele) = &tab_ci..codint + ': ' + ALLT (&tab_ci..dsc_ci)
								SKIP IN &tab_ci
							ENDDO						
							DIMENSION opcion [cant_ele]
							linea    = wrow()
							long_win = cant_ele
							IF (linea + cant_ele > 22) OR (linea < 1)
								linea    = 22 - cant_ele
								long_win = cant_ele
							ENDIF
							DEFINE WINDOW menusito AT linea,wcol()+2 SIZE long_win ,long_max;
								IN SCREEN;
								NOCLOSE;
								NONE
							ACTI WIND menusito
							i = 0
							titulo = ALLT (&tab_tci..dsc_tci)
							@ 0,0 MENU opcion, cant_ele TITLE titulo
							READ MENU TO i
							DEACT WIND menusito
							IF i = 0
								POP KEY
								RETU ''
							ENDIF
							wancho  = FSIZE ('codint', tab_ci)
							retorno = SUBS (opcion[i],1 ,wancho)
							POP KEY
						ENDIF
					ELSE
						DO final WITH 100000, 'NUMERO ERRONEO DE PARAMETROS' + CHR(13) + CHR(13) + 'Funci�n TAB_INT ().'
					ENDIF
					
				CASE accion == 'VALID'
					IF cant_par >= 3
						m.dscab_ci = &dsc_ab
						retorno = IN_TABLA (tab_ci+',m.codint', &tab_tci..dsc_tci, 'codint:H="COD.", dscab_ci:H="DESCRIPCION"','',.F.,.T.)
						IF retorno
							* ANTES DEL CAMBIO
							* &cod_int = m.codint
							* CAMBIO RENATO 27/04/95
							&cod_int = SUBSTR (m.codint,1,&tab_tci..carac_tci)
							IF cant_par >= 4
								&dsc_ab = m.dscab_ci
							ENDIF
							IF !no_show
								SHOW GETS OFF
								SHOW GET &cod_int
							ENDIF
						ENDIF
					ELSE
						DO final WITH 100000, 'NUMERO ERRONEO DE PARAMETROS' + CHR(13) + CHR(13) + 'Funci�n TAB_INT ().'
					ENDIF

				CASE accion == 'VALIDL'
					IF cant_par >= 3
						m.dsc_ci = &dsc_ab
						retorno = IN_TABLA (tab_ci+',m.codint', &tab_tci..dsc_tci, 'codint:H="COD.", dsc_ci:H="DESCRIPCION"','',.F.,.T.)
						IF retorno
							* ANTES DEL CAMBIO
							* &cod_int = m.codint
							* CAMBIO RENATO 27/04/95
							&cod_int = SUBSTR (m.codint,1,&tab_tci..carac_tci)
							IF cant_par >= 4
								&dsc_ab = m.dsc_ci
							ENDIF
							IF !no_show
								SHOW GETS OFF
								SHOW GET &cod_int
							ENDIF
						ENDIF
					ELSE
						DO final WITH 100000, 'NUMERO ERRONEO DE PARAMETROS' + CHR(13) + CHR(13) + 'Funci�n TAB_INT ().'
					ENDIF
					
				OTHERWISE
					DO final WITH 110000, 'ERROR. El tipo de Acci�n = ' + ALLT (accion) + ' es incorrecta.' + CHR(13) + CHR(13) + 'Funci�n TAB_INT ().'
			ENDCASE
		ELSE
			DO final WITH 110001, 'ERROR. El C�digo Interno = ' + ALLT (m.tipcodint) + ' no existe.' + CHR(13) + CHR(13) + 'Funci�n TAB_INT ().'
		ENDIF
	ENDIF
ENDIF

SELE (walias)

RETURN retorno

***********************************
FUNCTION ChkParam
***********************************
* Chequea la cantidad y el tipo de par�metros. Si la cantidad es 
* incorrecta, se retorna .F., sino .T.
*
PARAMETERS wcant_par
PRIVATE param_ok
param_ok = .T.

IF wcant_par >= 2
	IF wcant_par <= 4
		IF EMPTY (sistema)
			param_ok = .F.
			DO final WITH 110002, 'ERROR. No se encontr� sistema.' + CHR(13) + CHR(13) + 'Funci�n TAB_INT ().'
		ELSE
			wsistema = UPPER(ALLT(sistema))
		ENDIF
	ELSE
		wsistema = tsistema
	ENDIF
	IF param_ok
		=VerTiPar ('Tab_Int','TIP_COD','C')
		=VerTiPar ('Tab_Int','ACCION','C')
		IF wcant_par >=3 AND !EMPTY (M.COD_INT)
			=VerTiPar ('Tab_Int',M.COD_INT,'C')
		ENDIF
		IF wcant_par >=4 AND !EMPTY (DSC_AB)
			=VerTiPar ('Tab_Int',DSC_AB,'C')
		ENDIF
		IF wcant_par =5 AND !EMPTY (TSISTEMA)
			=VerTiPar ('Tab_Int','TSISTEMA','C')
		ENDIF
	ENDIF
ELSE
	DO final WITH 100000, 'NUMERO ERRONEO DE PARAMETROS' + CHR(13) + CHR(13) + 'Funci�n TAB_INT ().'
	param_ok = .F.
ENDIF
RETURN param_ok


***********************************
FUNCTION Abre_dbf
***********************************
* Si las tablas internas a usar no est�n abiertas, las abre.
*
PARAMETERS dbf_1, dbf_2
PRIVATE raiz

raiz = ALLT (_efimuni) + 'BIN\'

IF ! USED (dbf_1)
	SELE 0
	USE (raiz + dbf_1) ORDER PRIMARIO
ENDIF
IF ! USED (dbf_2)
	SELE 0
	USE (raiz + dbf_2) ORDER PRIMARIO
ENDIF

RETURN

************************
FUNCTION Ver_Vacio
************************
* Incializa en blanco ("") los par�metros que no se pasaron.
*
IF EMPTY (tip_cod)
	tip_cod = ''
ENDIF

IF EMPTY (accion)
	accion = ''
ENDIF

IF EMPTY (m.codint)
	m.codint = ''
ENDIF

RETURN
*

**************************************************
FUNCTION TI_CONTA
*************************************************
PARAMETERS dbf_int, pedido, valor, dsc_corta
PRIVATE retorno

IF PARA () < 4
	retorno = TAB_INT (dbf_int, pedido, valor)
ELSE
	retorno = TAB_INT (dbf_int, pedido, valor, @dsc_corta, 'CONTA')
ENDIF

RETURN retorno

*************************************************
FUNCTION VAL_GRAL
*************************************************
* 05/01/95 (Rodrigo y Renato) Se descomentaron las l�neas que limpiaban
* el BUFFER del teclado y se metieron dentro del IF que verificaba si la ultima tecla 
* presionada era un ESC, verificando que a este IF solo se haya ingresado con la condici�n
* que la �ltima tecla presionada haya sido distinta de ESC y no un boton tipo "ESC".

* Los select que estan de m�s dejarlos porque no afectan a la performance
* y mejoran la seguridad

PARAMETERS vcpo,vgraba,vagrup
PRIVATE vretorno, vexpresion, vtab_val, vtipo_cpo, vnom_grup
PRIVATE vvolver, vcpo2, vcurr_rec, valias, vlastkey, vclave
PRIVATE m.act_ctrl, m.obj		&& ### VFP

* PRIVATE m.fue_int	&& ### VFP
* m.fue_int = .F.

* ### VFP (Prevengo recurrencia)
* ###> Foco
IF TYPE("m.in_browlis")=="L"
	* Ya estoy dentro de VAL_GRAL
	RETURN(.T.)
ENDIF

* ###> Foco
*!*	IF _Screen.ActiveForm.ValidNest > 1
*!*		RETURN(.T.)
*!*	ELSE
*!*		_Screen.ActiveForm.ValidNest = _Screen.ActiveForm.ValidNest + 1
*!*	ENDIF

* ###> Foco
*!*	IF LASTKEY()==-2 AND "NWHOTKEY" $ UPPER (PROGRAM(PROGRAM(-1)-3))
*!*		RETURN (.T.)
*!*	ENDIF

* ###> Foco
*!*	IF _Screen.ActiveForm.LastValid = vcpo
*!*		_Screen.ActiveForm.LastValid = ""
*!*		RETURN (.T.)
*!*	ENDIF

* El campo est� deshabilitado, retorno sin m�s
* IF TYPE("_Screen.ActiveForm.ActiveControl")=="O" AND ! ;
* 	_Screen.ActiveForm.ActiveControl.Enabled

m.act_ctrl = .NULL.
IF TYPE("_Screen.ActiveForm.ActiveControl")=="O" 
	m.act_ctrl = _Screen.ActiveForm.ActiveControl
	WITH m.act_ctrl
		IF ! .Enabled
			RETURN (.T.)
		ENDIF

		IF UPPER (.BaseClass)=="COMMANDBUTTON"
			_click_bot = UPPER(STRTRAN(.Caption, "\<"))
			* WAIT WIND _click_bot NOWAIT
		ELSE
			_click_bot = ""
		ENDIF

		* Agregado para SQL: los campos DATE vac�os pasan a ser NULL
*!*			IF _SQL_FLAG AND UPPER (.BaseClass)=="TEXTBOX" AND ;
*!*				VARTYPE(.Value)=="D" AND EMPTY(.Value)

*!*				* WAIT WIND .Name+" a NULL" NOWAIT
*!*				* vcpo2 = .ControlSource 
*!*				* &vcpo2 = .NULL.
*!*				.Value = .NULL.
*!*				.Refresh
*!*			ENDIF
	ENDWITH
ENDIF

* IF !BETWEEN(PARAMETERS(),1,3)
IF !BETWEEN(PCOUNT(),1,3)
	WAIT WIND "Error en el pasaje de par�metros para la funci�n VAL_GRAL()"
	RETU
ENDIF

IF EMPTY(VGRABA)
	vgraba = .F.
ENDIF

IF EMPTY(VAGRUP)
	vagrup = .F.
ENDIF

vretorno  = .T.

est_error = .F.
vencontro = .T.

valias = SELE ()

vtab_val = WONTOP()
* WAIT WINDOW vtab_val NOWAIT      && ### VFP
IF vtab_val = 'CONTROLS' OR EMPTY(SELECT(vtab_val))
	vtab_val = vent_ppal
ENDIF
SELECT (vtab_val)

vcpo2 = PADR (UPPER(vcpo),10)
IF !SEEK (vcpo2+'V',vtab_val)
	vencontro = .F.
	vtipo_cpo = ''
ELSE
	vtipo_cpo = UPPER (tipo_cpo)
ENDIF

vlastkey = LASTKEY ()

* Con �sto arreglo el bug que ignoraba los botones de ignorar / retornar
* en las ventanas secundarias de elementos de lista

* IF vlastkey # 27 OR vtipo_cpo = 'ESC'		### VFP
IF vlastkey # 27 OR vtipo_cpo = 'ESC' OR _click_bot $ "IGNORAR/RETORNAR"

	IF vencontro
		vvolver		= volver
		vnom_grup = nom_grup
		vclave    = clave

		SELECT (vtab_val)
		DO WHIL !EOF(vtab_val) AND vretorno AND indica=='V' AND UPPER(campo)==vcpo2
			vcurr_rec = RECNO(vtab_val)

			IF !EMPTY (volver)
				vvolver	= volver
			ENDIF

			IF !(vgraba AND !val_graba)
				IF !EMPTY(funcion)
					vexpresion = funcion+'('+ DEL_CHR (contenido) +')'
					SELE (valias)
					vretorno = EVAL (vexpresion)
					SELE (vtab_val)
				ENDIF
			ENDIF
			
			SELE (vtab_val)
			GO vcurr_rec
			SKIP 
		ENDDO

		SELE (vtab_val)
		IF !vretorno 
			IF !EMPTY(vvolver)
				vvolver = ALLT (vvolver)
				* _CUROBJ = OBJNUM (m.&vvolver)		### VFP
				* SET_CUROBJ (OBJNUM (m.&vvolver))	&& Puede fallar en VFP, por eso el chequeo posterior
				SET_CUROBJ (OBJNUM2 (vvolver))	&& Puede fallar en VFP, por eso el chequeo posterior
			ELSE
				IF vagrup
					* _CUROBJ = OBJNUMSCR()		### VFP
					* SET_CUROBJ (OBJNUMSCR())
					m.obj = OBJNUMSCR()
					SET_CUROBJ (m.obj)
				ELSE
					IF !(grup_mem == UPPER(ALLTRIM(vnom_grup)) AND !EMPTY(grup_mem))
						IF UPPER(TRIM(vclave)) # 'CL' AND UPPER(ALLTRIM(vclave)) # 'CLU'
							* _CUROBJ = OBJNUM (m.&vcpo)	### VFP
							* SET_CUROBJ (OBJNUM (m.&vcpo))
							SET_CUROBJ (OBJNUM2 (vcpo))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF !vagrup AND !vgraba
				vretorno = .T.
			ENDIF
			est_error		= .T.
		ENDIF
	
		IF !EMPTY(vtipo_cpo) 
			IF UPPER (vtipo_cpo) = 'LST' OR UPPER (vtipo_cpo) = 'BOT'
				* _CUROBJ = OBJNUM (m.&vcpo)	### VFP
				* SET_CUROBJ (OBJNUM (m.&vcpo))
				SET_CUROBJ (OBJNUM2 (vcpo))
				vretorno = .T.
			ENDIF
		ENDIF

	ENDIF

	IF vtipo_cpo # 'ESC' AND LAST() = 27
		KEYBOARD '{shift+f12}' CLEAR
		WAIT WIND ''
	ENDIF
ENDIF

* Comienzo Alejandro 18/10/2006
IF !m.modulo = 'TESIN   '
	IF !USED ('val_cpos')
		UseT ('asa\val_cpos')
	ENDIF

	IF !USED ('campos')
		UseT ('asa\campos')
	ENDIF

	IF SEEK (m.sistema + m.modulo + m.programa + vcpo, 'val_cpos')
		IF val_cpos.obligat = 'S'
			DO CASE
				CASE TYPE ('&vcpo') = 'C'
					IF SEEK (m.sistema + vcpo, 'campos')
						vretorno = No_Nulo (&vcpo, campos.titulo)
					ELSE
						vretorno = No_Nulo (&vcpo, 'en edici�n')
					ENDIF
				CASE TYPE ('&vcpo') = 'N'
					IF SEEK (m.sistema + vcpo, 'campos')
						IF &vcpo = 0
							= Msg ('El campo ' + campos.titulo + ' no puede ser Nulo.')
							vretorno = .F.
						ENDIF
					ELSE
						IF &vcpo = 0
							= Msg ('El campo en edici�n no puede ser Nulo.')
							vretorno = .F.
						ENDIF
					ENDIF
			ENDCASE
		ENDIF
	ENDIF

	USE IN val_cpos
ENDIF
* Fin Alejandro 18/10/2006

SELE (valias)

* ### VFP
* IF m.fue_int AND vretorno
* 	* Si fu� un IN_TABLA y retorna .T. posiciono sobre el siguiente campo
* 	vretorno = 1
* ENDIF

* Agregado para SQL: valido campos DATE por limitaciones del motor
*!*	IF _SQL_FLAG AND ! ISNULL(m.act_ctrl) 
*!*		WITH m.act_ctrl
*!*			IF UPPER (.BaseClass)=="TEXTBOX" AND VARTYPE (.Value)=="D" AND ;
*!*				!EMPTY (.Value) AND .Value < {01/01/1800}

*!*				vretorno = .F.
*!*			ENDIF
*!*		ENDWITH
*!*	ENDIF

* ###> Foco
*!*	IF ! EMPTY (_Screen.ActiveForm.AbreF3)
*!*		_Screen.ActiveForm.AbreF3 = .F.
*!*	ENDIF

* ###> Foco
* _Screen.ActiveForm.ValidNest = _Screen.ActiveForm.ValidNest - 1

*!*	IF LASTKEY()==13
*!*		_Screen.ActiveForm.LastValid = vcpo
*!*	ELSE
*!*		_Screen.ActiveForm.LastValid = ""
*!*	ENDIF

RETURN vretorno


*************************************************
FUNCTION OBJNUMSCR
*************************************************
* Autor: RODRIGO
* 
* Fecha: 05/01/95
* 
* Funcionamiento: Busca , en caso de que un agrupado de error,
* el primer campo del grupo en la tabla de validaciones.
* 

PRIVATE numobj, i, n, flag, curr_rec
i			= 1
n			= ULTCPOPANT()
numobj 		= _CUROBJ
flag		= .T.
curr_rec	=	RECNO(vtab_val)

DO WHILE i <= n AND flag
	nomcpo = SUBSTR(OBJVAR(i),3)
	IF SEEK(nomcpo,vtab_val)
		IF !EMPTY(EVAL (vtab_val + '.nom_grup')) AND (ALLTRIM(UPPER(EVAL (vtab_val + '.nom_grup'))) == grup_mem)
			* numobj = OBJNUM2 (m.&nomcpo)
			numobj = OBJNUM2 (nomcpo)
			flag = .F.
		ENDIF
	ENDIF
	i = i + 1
ENDDO

=IR_REG(curr_rec, vtab_val)

RETURN numobj



* ### VFP : Setea el CUROBJ verificando que se cumpla
*!*	FUNCTION SET_CUROBJ (m.objnum)

*!*	LOCAL m.cur_obj, m.act_frm, m.ctrl, m.form

*!*	_CUROBJ = m.objnum
*!*	IF ! _CUROBJ==m.objnum 
*!*		m.cur_obj = 0
*!*		* m.act_frm = _Screen.ActiveForm.PageFrame1.Pages (1)
*!*		m.act_frm = _Screen.ActiveForm
*!*		m.form = _Screen.FormCount
*!*		DO WHILE EMPTY (m.act_frm.Name)
*!*			m.form = m.form - 1
*!*			m.act_frm = _Screen.Forms (m.form)
*!*		ENDDO
*!*		
*!*		* m.act_frm = m.act_frm.PageFrame1.Pages (1)
*!*		m.act_frm = m.act_frm.PageFrame1.Pages (m.act_frm.PageFrame1.PageCount)
*!*		FOR m.ctrl = 1 TO m.act_frm.ControlCount
*!*			
*!*			* IF PEMSTATUS (m.act_frm.Controls (m.ctrl), "ControlSource", 5) AND 
*!*			* LOWER (m.act_frm.Controls (m.ctrl).BaseClass)=="textbox" AND ;
*!*			* 	m.act_frm.Controls (m.ctrl).Style <> 1 AND
*!*			*	m.act_frm.Controls (m.ctrl).Visible AND ;
*!*			*	m.act_frm.Controls (m.ctrl).Enabled
*!*			
*!*			IF LOWER (m.act_frm.Controls (m.ctrl).BaseClass)=="textbox" AND ;
*!*			 	m.act_frm.Controls (m.ctrl).Style <> 1
*!*			 	
*!*				m.cur_obj = m.cur_obj + 1
*!*				IF m.cur_obj==m.objnum
*!*					* m.act_frm.Controls (m.ctrl).SetFocus
*!*					* _Screen.ActiveForm.LastActiveCtrl.NextFocus = m.ctrl
*!*					* _Screen.ActiveForm.NextFocus = _Screen.ActiveForm.PageFrame1.Pages (1).Controls (m.ctrl)
*!*					act_frm.Parent.Parent.NextFocus = act_frm.Controls (m.ctrl)
*!*					EXIT
*!*				ENDIF
*!*			ENDIF
*!*		NEXT
*!*	ENDIF

*!*	RETURN


FUNCTION SET_CUROBJ (m.objnum, m.frm)

LOCAL m.cur_obj, m.act_frm, m.ctrl, m.form, m.class
LOCAL m.arr_obj[1]
LOCAL m.hasNF

DIMENSION m.arr_obj [100, 2]

_CUROBJ = m.objnum
IF .T.		&& ! _CUROBJ==m.objnum 
	IF VARTYPE (m.frm) <> "O"
		m.act_frm = _Screen.ActiveForm
		m.form = _Screen.FormCount
		DO WHILE EMPTY (m.act_frm.Name)
			m.form = m.form - 1
			m.act_frm = _Screen.Forms (m.form)
		ENDDO

		IF PEMSTATUS(m.act_frm, "PageFrame1", 5)
			m.act_frm = m.act_frm.PageFrame1.Pages (m.act_frm.PageFrame1.PageCount)
		ENDIF
	ELSE
		m.act_frm = _Screen.ActiveForm
	ENDIF
	
	m.hasNF = PEMSTATUS(_Screen.ActiveForm, "NextFocus", 5)
	* IF m.act_frm.NextFocus <> .NULL.
	IF !(m.hasNF) OR;
	(TYPE ("_Screen.ActiveForm.NextFocus")=="L" AND ! ISNULL (_Screen.ActiveForm.NextFocus))
		RETURN
	ENDIF

	m.cur_obj = 0

	FOR m.ctrl = 1 TO m.act_frm.ControlCount
		WITH m.act_frm.Controls (m.ctrl)
			m.class = LOWER (.BaseClass)
			* IF .Visible AND .Enabled AND PEMSTATUS (m.act_frm.Controls (m.ctrl), "TabStop" ,5)
			* IF .Visible AND .Enabled AND .BaseClass

			IF .Visible AND (m.class=="commandgroup" OR TYPE ("m.act_frm.Controls (m.ctrl).TabStop")=="L")
				IF m.class=="textbox" AND .Style==1
					* Elimino textboxs compatibles con @ SAY
					LOOP
				ENDIF

				m.cur_obj = m.cur_obj + 1
				m.arr_obj [m.cur_obj, 1] = .TabIndex		&& Orden de tabulaci�n
				m.arr_obj [m.cur_obj, 2] = m.ctrl			&& Nro control
			ENDIF
		ENDWITH
	NEXT

	IF EMPTY(m.cur_obj)
		RETURN
	ENDIF

	DIMENSION m.arr_obj [m.cur_obj, 2]
	ASORT (m.arr_obj, 1)

*!*		m.log = .F.
*!*		IF m.log
*!*			FOR m.ctrl = 1 TO ALEN (m.arr_obj, 1)
*!*				LOGFILE (STR(m.ctrl,2)+": "+ m.act_frm.Controls (m.arr_obj[m.ctrl,2]).Name, "ORDER.LOG")
*!*			NEXT
*!*			LOGFILE ("", "ORDER.LOG")
*!*			m.ctrl = 1
*!*			DO WHILE ! EMPTY (OBJVAR(m.ctrl))
*!*				LOGFILE (STR(m.ctrl,2)+": "+OBJVAR(m.ctrl), "ORDER.LOG")
*!*				m.ctrl = m.ctrl + 1
*!*			ENDDO
*!*			LOGFILE (REPLICATE("-",20), "ORDER.LOG")
*!*		ENDIF

	* Reviso de no pasarme de m�nimos y m�ximos del array
	m.objnum = MAX(m.objnum, 1)
	m.objnum = MIN(m.objnum, ALEN (m.arr_obj, 1))

	* act_frm.Parent.Parent.NextFocus = m.act_frm.Controls (m.arr_obj [m.objnum, 2])
	m.class = UPPER (m.act_frm.Controls (m.arr_obj [m.objnum, 2]).BaseClass)
	* IF UPPER (m.act_frm.Controls (m.arr_obj [m.objnum, 2]).BaseClass) <> "COMMANDGROUP"
	DO CASE 
	CASE m.class=="COMMANDGROUP"
		act_frm.Parent.Parent.NextFocus = m.act_frm.Controls (m.arr_obj [m.objnum, 2]).Buttons (1)
	CASE m.class=="PAGEFRAME"
		IF PEMSTATUS (m.act_frm.Controls (m.arr_obj [m.objnum, 2]), "LastActivePage", 5)
			WITH m.act_frm.Controls (m.arr_obj [m.objnum, 2])
				act_frm.Parent.Parent.NextFocus = .Pages (.LastActivePage).Controls(.LastActiveControl)
			ENDWITH
		ELSE
			act_frm.Parent.Parent.NextFocus = m.act_frm.Controls (m.arr_obj [m.objnum, 2]).Pages (1).Controls(1)
		ENDIF
	OTHERWISE
		act_frm.Parent.Parent.NextFocus = m.act_frm.Controls (m.arr_obj [m.objnum, 2])
	ENDCASE
ENDIF

RETURN


*************************************************
FUNCTION errores
*************************************************
* Funcionamiento:
* Llamada
* SELECT 225
* IF FILE("ERRORES.DBF")
* 	USE
* 	USE ERRORES
* 	ON ERROR DO errores WITH ERROR(), MESSAGE(), VARREAD(), PROGRAM(), LINENO(), MESSAGE(1)
* ENDIF
* 

PARAMETERS nError, sError, vvar, sPrograma, nLinea, sLinea
PRIVATE princ_prog, m.pos
LOCAL sCallStack

* VFP
*!*		FOR m.pos = PROGRAM(-1) TO 1 STEP -1
*!*			sError = sError + CHR(13) + PROGRAM(m.pos)
*!*		NEXT
sCallStack = "Programa : " && + sPrograma
FOR m.pos = PROGRAM(-1) TO 1 STEP -1
	sCallStack = sCallStack + CHR(13) + PROGRAM(m.pos)
NEXT

* VFP
*!*		ON ERROR
*!*		ERROR sError, nError
DO FORM ERRORMSG WITH nError, sError, nLinea, sLinea, sCallStack
CLOSE TABLES ALL
SET SYSMENU ON
SET SYSMENU TO DEFA
FOR pos = TXNLEVEL() TO 1 STEP -1
	ROLLBACK
ENDFOR
RETURN TO MASTER

* VFP
*!*	IF TYPE ('m.programa') = 'C'
*!*		princ_prog = m.programa
*!*	ELSE
*!*		princ_prog = ''
*!*	ENDIF
*!*	PUSH KEY
*!*	ON KEY

*!*	* Lista de errores por los que no se debe dejar constancia
*!*	*  108:
*!*	*  125:
*!*	*   30:
*!*	* 1001:

*!*	* Restablece la tansaccion corriente
*!*	*=RLLBACK()

*!*	KEYBOARD '{shift+f12}' CLEAR
*!*	WAIT WIND ''

*!*	IF	(nError) <> 108 AND ;
*!*			(nError <> 125) AND ;
*!*			(nError <> 30)  AND ;
*!*			(nError <> 1001)

*!*	*		IF SET('DEBUG') = 'ON'
*!*				*** Define ventana para errores

*!*				* DEFINE WINDOW errores FROM 5,5 TO 20,75	;	### VFP
*!*				* 	TITLE " Control de Errores " ;
*!*				* 	DOUBLE SHADOW FLOAT GROW ;
*!*				* 	MINIMIZE COLOR SCHEME 7 

*!*				DEFINE WINDOW errores FROM 5,5 TO 25, 110 ;
*!*					NAME frmErr ;
*!*					TITLE " Control de Errores " ;
*!*					DOUBLE SHADOW FLOAT GROW ;
*!*					MINIMIZE COLOR SCHEME 7 ;
*!*					FONT "Courier New",8
*!*	*			ACTIVATE WINDOW errores
*!*	*		ENDIF

*!*			frmErr.AutoCenter = .T.

*!*			* Agrego Gabriel 011195
*!*			PRIVATE dim_amb, m.size_app, m.date_app, m.time_app, m.atrs_app, m.path
*!*			m.path = ''
*!*			STORE '' TO m.size_app, m.date_app, m.time_app, m.atrs_app
*!*			IF TYPE ('sistema') # 'U' AND TYPE ('modulo') # 'U'
*!*				DIME dim_amb[1,5]
*!*				m.path = ALLT (_EFIMUNI) + '\' + ALLT (sistema) + '\' + ALLT(modulo) + '\' + ALLT(princ_prog) + '.APP'
*!*				= ADIR (dim_amb, m.path)
*!*				IF TYPE ('dim_amb[1,2]') = 'N'
*!*					m.size_app = STR (dim_amb[1,2], 6)
*!*					m.date_app = DTOC (dim_amb[1,3])
*!*					m.time_app = dim_amb[1,4]
*!*					m.atrs_app = dim_amb[1,5]
*!*				ENDIF
*!*			ENDIF
*!*			
*!*			actarea = SELECT()
*!*			IF !USED('errores')
*!*				=USET ('asa\errores')
*!*			ENDIF

*!*			IF TYPE ('m.num_aud') = 'U'
*!*				num_aud = 0
*!*			ENDIF

*!*			SELECT errores
*!*			APPEND BLANK
*!*			REPLACE cerror     WITH nError,;
*!*					nerror     WITH sError,;
*!*				  var        WITH vvar,;
*!*				  programa   WITH sPrograma,;
*!*				  linea      WITH vlinea,;
*!*					fecha      WITH DATE(),;
*!*					hora 	   WITH TIME(),;
*!*					prog_princ WITH m.path ,;
*!*					cod_audit  WITH num_aud

*!*			IF FIELD (13) = 'SIZE_APP'
*!*				REPL size_app  WITH m.size_app,;
*!*						date_app   WITH m.date_app,;
*!*						time_app   WITH m.time_app,;
*!*						atrs_app   WITH m.atrs_app
*!*			ENDIF
*!*								
*!*			IF TYPE('_server') <> 'U'
*!*				REPLACE server WITH _server						
*!*			ENDIF
*!*			IF TYPE('_usuario') <> 'U'
*!*				REPLACE user WITH _usuario						
*!*			ENDIF
*!*			IF TYPE('_wkstation') <> 'U'
*!*				REPLACE wkstation WITH _wkstation						
*!*			ENDIF
*!*			SELECT (actarea)

*!*	*		IF SET('DEBUG') = 'ON'
*!*				ACTIVATE WINDOW errores NOSHOW
*!*				CLEAR
*!*				*** Muestra datos del error
*!*				? " Usuario ----------> "
*!*				IF TYPE('_usuario') <> 'U'
*!*					?? _usuario						
*!*				ENDIF
*!*				? " Server -----------> "
*!*				IF TYPE('_server') <> 'U'
*!*					?? _server						
*!*				ENDIF
*!*				? " Work Station -----> "
*!*				IF TYPE('_wkstation') <> 'U'
*!*					?? _wkstation						
*!*				ENDIF
*!*				?
*!*				? " N�mero de error --> ", ALLTRIM(STR(nError))
*!*				? " Texto ------------> ", sError
*!*				? 
*!*				? " Variable editada -> ", vvar
*!*				? " Prog.Principal ---> ", princ_prog + " " + m.size_app + " " + m.date_app + " " + m.time_app + " " + m.atrs_app
*!*				? " Programa ---------> ", sPrograma
*!*				? " Linea ------------> ", ALLTRIM(STR(vlinea))
*!*	*			?
*!*	*			? " Pulse una tecla para continuar"
*!*	*			= INKEY(0)
*!*	**********************************************
*!*			PRIVATE confirm

*!*			confirm = 1
*!*			* @ 12,6 GET m.confirm ### C/S
*!*			* 	PICTURE "@*HN \!\<Imprimir; I\<nformaci�n; \?\<Acepta"
*!*			@ WROWS()-2,6 GET m.confirm ;
*!*				PICTURE "@*HN \!\<Imprimir; I\<nformacion; \?\<Acepta";
*!*				SIZE 1, 8, 2 ;
*!*				DEFAULT 1 ;
*!*				VALID msg_error() ;
*!*				MESSAGE "Permite imprimir el mensaje de error y continuar"
*!*	************************************************
*!*				SHOW WINDOW errores
*!*				READ CYCLE
*!*				RELEASE WINDOW errores
*!*	*		ENDIF
*!*	ENDIF
*!*	POP KEY
*!*	=FINAL(1)
*!*	RETURN
*!*	*

*********************************************
FUNCTION msg_error   &&  m.confirm VALID
*********************************************
	DO CASE
		CASE m.confirm = 1
			IF _server='SICO'
				* No imprime
				WAIT WIND TIMEOUT .5 ;
						'Esta desactivada la impresi�n para este servidor.' 
				CLEAR READ
			ELSE
				=print_error()		
				*CLEAR READ
			ENDIF

		CASE m.confirm = 2
			PRIVATE debug
			debug = 'debug'
			=&DEBUG ()
			*CLEAR READ
			
		CASE m.confirm = 3
			CLEAR READ
			
	ENDCASE
	* _CUROBJ = 1	### VFP
	SET_CUROBJ (1)
	RETURN .F.
	
*************************************************
FUNCTION print_error	
*************************************************
	* Funcionamiento:
	* 
	* 
	* 
	* 
	* Par�metros:
	* 
	* 
	* 
	
IF PRINTSTATUS()	
SET PRINTER ON
SET CONSOLE OFF
* set proc to c:\fox\efiproc

wdia     = str(day (date () ),2)
wmes     = ALLTRIM(str(month (date () ),2))
*wnom_mes = ti_conta('MESESINT','descl','wmes')
wnom_mes = tab_int( 'MESESINT', 'descl', 'wmes','','CONTA' )
wanio    = STR( YEAR (DATE ()), 4)

? "Sistema EFI-MUNI"
? "________________________________________________________________________"
?
? "                                                  " 
*?? wdia 
*?? " de "
*?? wnom_mes 
*?? " de "
*?? wanio
??DATE()
??",   "
?? TIME()
?
? "Atenci�n Gerencia de Clientes"
?
? "SICO Servicios Inform�ticos S.A."
?
? "FAX: (023) 75-6897"
?
?
? "      Le remitimos una copia de un error surgido en el Sistema EFI-MUNI"
?
?
			? " Usuario ----------> "
			IF TYPE('_usuario') <> 'U'
				?? _usuario						
			ENDIF
			? " Server -----------> "
			IF TYPE('_server') <> 'U'
				?? _server						
			ENDIF
			? " Work Station -----> "
			IF TYPE('_wkstation') <> 'U'
				?? _wkstation						
			ENDIF
			?
			? " Numero de error --> ", ALLTRIM(STR(nError))
			? " Texto ------------> ", sError
			? 
			? " Variable editada -> ", vvar
			? " Prog.Principal ---> ", princ_prog + " " + m.size_app + " " + m.date_app + " " + m.time_app + " " + m.atrs_app
			? " Programa ---------> ", sPrograma 
			? " Linea ------------> ", ALLTRIM(STR(vlinea))
?			
?
? "Municipalidad: .................."
?
? "Fax:           .................."

EJECT PAGE
SET CONSOLE ON
SET PRINTER OFF	
ELSE
	=MSG ('La impresora no esta preparada')
ENDIF
*
*******************************************
FUNCTION DEL_CHR
*
* Funcion que Limpia cosas extra�as de memos
* y vbles. de tipo caracter.
* Gabriel - 16/08/94
* Previsto :
* Memos : CHR(13)-CHR(10)-TAB-BLANCO
* Char  : TAB-BLANCO
*******************************************
PARAMETERS EXP
PRIVATE R_EXP

R_EXP = EXP
R_EXP = STRTRAN( R_EXP , CHR(10) , "" )
R_EXP = STRTRAN( R_EXP , CHR(13) , "" )
R_EXP = STRTRAN( R_EXP , CHR(09) , "" )


RETURN ALLTRIM( R_EXP )
*

**************************************************
FUNCTION TTOS
**************************************************
* Funcionamiento
*  Pasa un tiempo en formato HHMMSS a segundos
*  Si el valor de tiempo es incorrecto devuelve -1 
*
* Par�metros:
*  tiempo  : (N) Tiempo en formato HHMMSS 		
*  [warn]  : (N) (0) 0 = no avisa si est� mal el tiempo ingresado
*										 1 = avisa si est� mal el tiempo ingresado
*	 [es_dia]: (N) (0) 0 = Si permite mayor cantidad de horas que tiene un dia
*										 1 = Si NO permite mayor cantidad de horas que tiene un dia
* 
**************************************************

PARAMETERS Tiempo, Warn, es_dia
PRIVATE Horas, Minutos, Segundos, CantSegundos

IF EMPTY(es_dia)
	es_dia = 0
ENDIF

IF PARAMETERS () < 2
	Warn = 0
ENDIF

IF PARAMETERS () < 1
	Tiempo = 0
ENDIF

Segundos = MOD (Tiempo, 100)
Minutos  = MOD (INT (Tiempo / 100), 100)
Horas    = INT (Tiempo / 10000)

IF Segundos < 0 .OR. Segundos > 59
	IF Warn # 0
		=msg( 'Cantidad de Segundos Incorrecta. ' +;
			ALLTRIM (STR (Segundos)) + ' segundos ')
	ENDIF
	CantSegundos = -1
ELSE
	IF Minutos < 0 .OR. Minutos > 59
		IF Warn # 0
			=msg('Cantidad de Minutos Incorrecta. ' +;
				ALLTRIM (STR (Minutos)) +	' minutos ')
		ENDIF
		CantSegundos = -1
	ELSE
		IF Horas < 0 OR (Horas > 23 AND es_dia = 1)
			IF Warn # 0
				=msg( 'Cantidad de Horas Incorrecta. ' +;
				     	ALLTRIM (STR (Horas)) +	' horas ' )
			ENDIF
			CantSegundos = -1
		ELSE
			CantSegundos = Horas * 3600 + Minutos * 60 + Segundos
		ENDIF
	ENDIF
ENDIF	

RETURN CantSegundos


**************************************************
FUNCTION STOT
**************************************************
* Funcionamiento
*  Pasa una cantidad de segundos a formato HHMMSS 
*  Si el valor de segundos es negativo devuelve 0 
*
* Par�metros:
*  CantSegundos : (N) Cantidad de segundos  		
**************************************************

PARAMETERS CantSegundos
PRIVATE Horas, Minutos, Segundos, Tiempo

IF PARAMETERS () < 1
	CantSegundos = 0
ENDIF

IF CantSegundos < 0
	CantSegundos = 0
ENDIF

Segundos = MOD (CantSegundos, 60)
Minutos  = INT (MOD (CantSegundos, 3600) / 60)
Horas    = INT (CantSegundos / 3600)

Tiempo = Horas * 10000 + Minutos * 100 + Segundos

RETURN Tiempo


**************************************************
FUNCTION TTOM
**************************************************
* Funcionamiento
*  Pasa un tiempo en formato HHMM a Minutos
*  Si el valor de tiempo es incorrecto devuelve -1 
*
* Par�metros:
*  tiempo  : (N) Tiempo en formato HHMM
*  [warn]  : (N) (0) 0 = no avisa si est� mal el tiempo ingresado
*										 1 = avisa si est� mal el tiempo ingresado
*	 [es_dia]: (N) (0) 0 = Si permite mayor cantidad de horas que tiene un dia
*										 1 = Si NO permite mayor cantidad de horas que tiene un dia
*
**************************************************

PARAMETERS Tiempo, Warn, es_dia
PRIVATE Horas, Minutos, CantMinutos

IF EMPTY(es_dia)
	es_dia = 0
ENDIF

IF PARAMETERS () < 2
	Warn = 0
ENDIF

IF PARAMETERS () < 1
	Tiempo = 0
ENDIF

Minutos  = MOD (Tiempo, 100)
Horas    = INT (Tiempo / 100)

IF Minutos < 0 .OR. Minutos > 59
	IF Warn # 0
		=msg( 'Cantidad de Minutos Incorrecta. ' +;
			ALLTRIM (STR (Minutos)) +	' minutos ' )
	ENDIF
	CantMinutos = -1
ELSE
	IF Horas < 0 OR (Horas > 23 AND es_dia = 1)
		IF Warn # 0
			=msg( 'Cantidad de Horas Incorrecta. ' +;
   				ALLTRIM (STR (Horas)) +	' horas ' )
		ENDIF
		CantMinutos = -1
	ELSE
		CantMinutos = Horas * 60 + Minutos
	ENDIF
ENDIF	

RETURN CantMinutos


**************************************************
FUNCTION MTOT
**************************************************
* Funcionamiento
*  Pasa una cantidad de minutos a formato HHMM
*  Si el valor de minutos es negativo devuelve 0 
*
* Par�metros:
*  Cantminutos : (N) Cantidad de minutos  		
**************************************************

PARAMETERS Cantminutos
PRIVATE Horas, Minutos, Tiempo

IF PARAMETERS () < 1
	Cantminutos = 0
ENDIF

IF Cantminutos < 0
	Cantminutos = 0
ENDIF

Minutos  = MOD (Cantminutos, 60)
Horas    = INT (Cantminutos / 60)

Tiempo = Horas * 100 + Minutos

RETURN Tiempo

*************************************************
FUNCTION in_tabla
*************************************************
*
* Funcionamiento
*	 Chequea que el �ltimo campo de la clave de la tabla contra la cual
*		se debe validar exista.
*	 Si el campo no existe se visualiza una lista de opciones
*			Con ESC se sale sin seleccionar nada (devuelve .F.)
*			Con ENTER se sale seleccionando un valor (devuelve .T.)
*	 Si el campo existe se actualizan los campos de descripci�n
*			Llamamos campos de descripci�n a los campos que se pasan el la
*			expresi�n (expresi�n) y que no son parte de la clave de la 
*			tabla (tabla). (devuelve .T.)
*
* Par�metros:
*
*  tabla       : (C) nombre de la tabla contra la cual se valida
*
*  [titulo]    : (C) ('Lista de opciones') t�tulo de la ventana 
*                con lista de opciones
*  [expresion] : (C) (todos los campos con el nombre del campo
*                como cabecera) expresi�n para el browse
*  [indice]    : (C) ('nombre_tag_1') nombre del tag por el cual se 
*                quiere buscar en la tabla contra la cual se valida
*  [no_valida] : (L) (.F.) .F.= valida  .T.= no valida y se llama 
*																				la lista de opciones �nicamente				
*																				si se presion� F3.
*  [no_show]   : (L,C,N,D) (.F.) si no se pasa el par�metro o es .F.
*								 se hacen dentro de la funci�n SCATTER de todos los
*								 campos y un SHOW GETS   
*
* Ejemplo de llamada
*
*  =IN_TABLA('<nom_tabla>','<tit_ventana>','<nom_cpo_1>:H="<desc_cpo_1>",
*  <nom_cpo_2>:H="<desc_cpo_2>",....','<nom_tag>','<no_valida>',<no_show>)
*
* Aviso : en esta funcion, para mejorar la performance no se tienen en
*         cuenta algunas normas de SICO. Ejemplo: se retorna de cualquier
*					punto de modo de no anidar demasiados IFs
*************************************************

PARAMETERS ntabla, ntitulo, nexpr, nindice, no_valida, no_show, cond_for
PRIVATE wexp_ind, baseact, actarea, ordact,;
	actreg, retorno, palabra, i, pos_coma, ;
	int_cant_ind, int_nom_cpos, int_val_cpos, pal, ilastkey, existe, get_indice

retorno = .T.

* ### VFP (Indicador de que entr� en IN_TABLA)
m.fue_int = .T.
					
**  por ESC o no_valida sin F3 retorna
ilastkey = LAST ()
get_indice = .F.

* ###> Foco
*!*	IF ! EMPTY (_Screen.ActiveForm.AbreF3)
*!*		_Screen.ActiveForm.AbreF3 = .F.
*!*		ilastkey = -2
*!*	ENDIF

IF ilastkey = -1
	ilastkey = -2
	get_indice= .T.
ENDIF

IF ilastkey = -2 
	PUSH KEY 
	= DEAC_TEC()
ELSE
	IF ilastkey = 27 OR no_valida 
		RETURN retorno
	ENDIF
ENDIF

**	Busca si hay un campo en ntabla
pos_coma = AT (',',ntabla)
IF pos_coma > 0
	in_aux_cpo = UPPER (SUBSTR (ntabla, pos_coma+1))
	ntabla = SUBSTR (ntabla, 1, pos_coma-1)
ELSE
	IF '(' $ VARREAD()
		in_aux_cpo = VARREAD()
	ELSE
		in_aux_cpo = 'M.'+VARREAD()
	ENDIF
ENDIF

* ### VFP
IF in_aux_cpo=="M."
	RETURN retorno
ENDIF

**  Si esta vacio y no es numerico retorna
IF EMPTY (EVAL (in_aux_cpo)) AND TYPE(in_aux_cpo) <> 'N' AND ilastkey <> -2
	RETURN retorno
ENDIF

**  Establece parametros
IF TYPE('no_show') <> 'L'
	no_show = .T.
ENDIF	

IF TYPE('nindice') <> 'C'
	nindice = ''
ENDIF

IF AT ('\', ntabla) > 0
	tabla_co = ntabla
	ntabla   = substr (ntabla, AT ('\', ntabla) + 1)
ENDIF

actarea  = SELECT()
baseused = .T.

IF ! USED(ntabla)
	=USET (tabla_co)
	baseused = .F.
ENDIF

SELECT (ntabla)
ant_skip = SET('SKIP')
SET SKIP TO
actreg = RECNO()
ordact = ORDER()

* IF EMPTY(_SQL_DB)
* IF ! _SQL_FLAG
* IF ! _SQL_FLAG OR CURSORGETPROP ("SourceType", ntabla)<>2
*!*	IF ! TABLA_SQL (ntabla)
	IF EMPTY(nindice)
		SET ORDER TO 1 
		nindice = TAG(1)
	ELSE
		SET ORDER TO (nindice)
	ENDIF

	**  arma indice
	wexp_ind = getkey (ntabla, .T., 0 , nindice)
*!*	ELSE
*!*		**  arma indice

*!*		IF EMPTY(nindice)
*!*			DBSETORDX (1)
*!*			nindice = 1
*!*		ELSE
*!*			DBSETORDX (nindice)
*!*		ENDIF
*!*		
*!*		* wexp_ind = SQL_GETKEY (ntabla, .T., 0 , 1)
*!*		wexp_ind = SQL_GETKEY (ntabla, .T., 0 , nindice)
*!*	ENDIF


**  Si no es F3 y lo encuentra y no_show = .F. hace SCATTER y nada mas
**  para mejorar la performance del caso mas comun

m.existe = SEEK (EVAL (wexp_ind))
*!*	DO CASE
* CASE EMPTY(_SQL_DB)
* CASE ! _SQL_FLAG
* CASE ! _SQL_FLAG OR CURSORGETPROP ("SourceType", ntabla)<>2
*!*	CASE ! TABLA_SQL (ntabla)
*!*		m.existe = SEEK (EVAL (wexp_ind))
*!*	CASE ilastkey <> -2
*!*		* ### Aparentemente no funcionaba para seeks con campos ""
*!*		* m.existe = SQL_SEEK (&wexp_ind )
*!*		m.existe = DBSEEK (wexp_ind)
*!*	OTHERWISE
*!*		m.existe = .T.
*!*	ENDCASE

IF ilastkey <> -2 
	retorno = m.existe
	IF retorno AND no_show = .F.
		SCATTER MEMVAR				
	ENDIF
ENDIF

**  si es F3 o no encontro hace todo esto.

IF ilastkey = -2 OR retorno = .F. OR no_show
	* ###> Foco
	* _Screen.ActiveForm.DioF3 = .T.

	* IF ! _SQL_FLAG OR CURSORGETPROP ("SourceType", ntabla)<>2
*!*		IF ! TABLA_SQL (ntabla)
		cant_ind = OCCURS('+',wexp_ind) + 1
*!*		ELSE
*!*			cant_ind = OCCURS(',',wexp_ind) + 1
*!*		ENDIF
	
	DIMENSION list_ind (cant_ind)
	list_ind = ''
	DO arma_ind

	**	Verifica si existen las variables (s�lo en desarrollo) 
	**  Si no existe retorna
	IF SET('DEBUG') = 'ON'
		FOR i = 1 TO cant_ind
			IF TYPE(list_ind[i]) = 'U'
				WAIT WINDOW ' Variable ' + list_ind[i] + ' no definida '
				RETURN .T.
			ENDIF
		ENDFOR
	ENDIF

	**  Arma vectores de campos y titulos
	IF TYPE('nexpr') <> 'C' OR EMPTY(nexpr)
		nexpr = ''
	
		cant_val = FCOUNT()
		DIMENSION list_val(cant_val), list_tit(cant_val)
	
		FOR j=1 TO cant_val
			nexpr = nexpr + FIELD(j) + ','
			list_val[j] = FIELD(j)
			list_tit[j] = FIELD(j)
		ENDFOR
		nexpr =  SUBSTR (nexpr, 1, LEN (ALLTRIM(nexpr)) - 1)
	ELSE
		nexpr = ALLTRIM(nexpr)
		cant_val = OCCURS(',',nexpr) + 1
		
		DIMENSION list_val(cant_val), list_tit(cant_val)
		list_val = ''
		list_tit = ''
	
		DO arma_val
	ENDIF

	IF ilastkey = -2 
		retorno = browlis2 ()
	ELSE
		IF retorno
			IF no_show 
				=scat_campos()
			ENDIF
		ELSE
			IF ! EMPTY (EVAL (in_aux_cpo)) OR TYPE(in_aux_cpo) = 'N'
				= MSG('El dato ingresado no est� entre las opciones permitidas')
				* = MSG('El dato ingresado no est� entre las opciones permitidas'+CHR(13)+CHR(10)+;
				*	'Utilice F3 para consultar las opciones disponibles')
				* retorno = browlis2 ()
				* No puedo llamar a browlis2 desde un VALID
				* retorno = .F.

				* ###> Foco (estaba s�lo el MSG)
				SET_CUROBJ (_CUROBJ)
				* _Screen.ActiveForm.AbreF3 = .T.
				* KEYBOARD "{ENTER}" PLAIN
				* _Screen.ActiveForm.DioF3 = .F.
				
				* _Screen.ActiveForm.AbreF3 = [ IN_TABLA (] + ;
				* 	QUOTE_STR (ntabla) +[,] +;
				* 	QUOTE_STR (ntitulo) + [,] +;
				* 	QUOTE_STR (nindice) + [,] +;
				* 	QUOTE_STR (no_valida) + [,] +;
				* 	QUOTE_STR (no_show) + [,] +;
				* 	QUOTE_STR (cond_for) + [)]
			ELSE
				IF no_show
					=blank_campos()
				ELSE
					=blank_nocla()
				ENDIF
				retorno = .T.
			ENDIF
		ENDIF
	ENDIF
ENDIF

**	reestablece entorno
IF ! EMPTY(ant_skip)
	SET SKIP TO (ant_skip)
ENDIF
IF baseused 
	SET ORDER TO (ordact)
	* DBSETORD (ordact)
	IF !retorno
		= IR_REG (actreg)
	ENDIF
ELSE
	USE IN (ntabla)
ENDIF

SELECT (actarea)

IF ! no_show
	SHOW GETS OFF
	SHOW GET &in_aux_cpo
ENDIF

IF ilastkey = -2 
	IF LASTKEY() = 27 
		KEYBOARD '{shift+f12}' CLEAR
		WAIT WIND ''
	ELSE
		* KEYBOARD '{ENTER}'	&& ### VFP
		* Anula el lastkey de val_gral
		
		* _CUROBJ = _CUROBJ+1
		* _Screen.ActiveForm.ThisFormset.AFINALI_.PageFrame1.Page1.txtFinalidad1.Valid
		* _Screen.ActiveForm.PageFrame1.Page1.txtFinalidad1.Valid
		* _Screen.ActiveForm.SetAll("Enabled", .T.)
		
		* m.next_obj = _CUROBJ + 1
		* _Screen.ActiveForm.LastActiveCtrl.Valid
		* SET_CUROBJ (m.next_obj)

		* ###> Foco (original)
		_Screen.ActiveForm.DioF3 = .T.
		
		vlastkey = 13
	ENDIF		
	POP KEY
ENDIF

RETURN retorno




**************************************************
FUNCTION browlis2
**************************************************

PRIVATE notiant, i, j, ancho, antordpri, anttabla, ancho_val,;
	ancho_tit, forbrow, distinto, retorno, int_val_cpos

PRIVATE m.in_browlis	&& VFP : prevengo recurrencia
m.in_browlis = .T.

retorno = .T.

IF TYPE('ntitulo') <> 'C' OR ntitulo == ''
	ntitulo = ' Lista de opciones '
ENDIF

#IFNDEF FORM_BROWSE

	* Fija ancho de ventana de lista de opciones
	ancho_val = 0
	ancho_tit = 0
	ancho = 1

	* Calcula ancho de ventana browse
	FOR i = 1 TO cant_val             
		ancho_val = FSIZE (list_val[i])
		ancho_tit = LEN (list_tit[i])
		IF ancho_val >= ancho_tit
			ancho = ancho + ancho_val + 1
		ELSE
			ancho = ancho + ancho_tit + 1
		ENDIF
	ENDFOR
	IF ancho < LEN(ntitulo)
		ancho = LEN(ntitulo)
	ENDIF
	IF ancho > 74
		ancho = 74
	ENDIF


	DEFINE WINDOW vent FROM 4,(76-ancho) TO 22,77 IN SCREEN DOUBLE FLOAT SHADOW COLOR SCHEME 10

#ENDIF


* cant_ind: cantidad de campos clave del �ndice
* list_ind: array de     ""

* cant_val: cantidad de campos a mostrar en la expresi�n de browse
* list_val: lista de     ""

cpos_en_expr = 0
FOR i=1 TO cant_ind
	FOR j=1 TO cant_val
		IF list_ind[i] == "M."+list_val[j]
			cpos_en_expr = cpos_en_expr + 1
			EXIT
		ENDIF
	ENDFOR
ENDFOR

* IF EMPTY(_SQL_DB)
* IF ! _SQL_FLAG
* IF ! _SQL_FLAG OR CURSORGETPROP ("SourceType", ntabla)<>2
*!*	IF ! TABLA_SQL (ntabla)
	IF cpos_en_expr = cant_ind
		forbrow = ""
	ELSE
		forbrow = getkey (ntabla, .T., cant_ind-cpos_en_expr, nindice)
		forbrow = EVAL (forbrow)
	ENDIF				

	IF ! EMPTY (forbrow)
		SET KEY TO forbrow
		IF !m.existe
			IF SEEK (forbrow)
				m.existe = .T.
			ENDIF
		ENDIF
		IF get_indice
			WAIT WIND 'No se puede cambiar el acceso a la tabla: ' + ntabla + '.'
		ENDIF
	ENDIF

*!*	ELSE
*!*		IF cpos_en_expr = cant_ind
*!*			forbrow = ""
*!*		ELSE
*!*			forbrow = SQL_GETKEY (ntabla, .F., cant_ind-cpos_en_expr, nindice)
*!*		ENDIF

*!*		* LOGFILE("FORBROW: "+forbrow)

*!*		IF !EMPTY (forbrow)
*!*			IF !m.existe
*!*				IF SQL_LOCATE (SQL_GETKEY(ntabla, 3, cant_ind-cpos_en_expr)) > 0
*!*					m.existe = .T.
*!*				ENDIF
*!*			ENDIF
*!*			* IF get_indice
*!*			* 	WAIT WIND 'No se puede cambiar el acceso a la tabla: ' + ntabla + '.'
*!*			* ENDIF
*!*		ENDIF
*!*	ENDIF
		
IF EMPTY(forbrow)
	IF !m.existe
		m.existe = ! TABVACIA (ntabla)
		GO TOP IN (ntabla)
	ENDIF
	IF get_indice
		nindice = selec_ind ()
		IF !EMPTY (nindice)
			SET ORDER TO nindice
		ENDIF
		GO TOP IN (ntabla)
	ENDIF
ENDIF

IF m.existe
	IF !EMPTY (nindice) 
		_jexitkey = 13
		_JDBLCLICK = -1
		IF ! EMPTY (forbrow)
			=jkeyinit("U", forbrow," Buscando: ","")
		ELSE
			=jkeyinit("U", ""," Buscando: ","")
		ENDIF
	ENDIF

#IFNDEF FORM_BROWSE

	ON KEY LABEL home DO BusqSecu

	* ### VFP
	PRIVATE lEnter 
	lEnter = .F.
	ON KEY LABEL Enter DO PulsoEnter

	* Muestra lista de opciones
	IF TYPE ('m.cond_for') = 'C'
		BROWSE FIELDS &nexpr NOAPPEND NODELETE ;
			NOEDIT REST TITLE ntitulo ;
			FOR EVAL (m.cond_for) ;
			WINDOW vent TIMEOUT 90
	ELSE
		BROWSE FIELDS &nexpr NOAPPEND NODELETE ;
			NOEDIT REST TITLE ntitulo ;
			WINDOW vent TIMEOUT 90
	ENDIF

	ON KEY LABEL home
	ON KEY LABEL Enter	&& ### VFP

#ELSE
	PRIVATE lEnter , m.alias, m.tabla
	lEnter = .F.

	* IF _SQL_FLAG
	* 	* Reabro la vista completa de la tabla
	* 	SQL_SET_ORD (1)
	* 	SQL_SEEK ()
	* ENDIF

	* IF !EMPTY(forbrow)
	* 	IF VARTYPE(m.cond_for)=="C"
	* 		m.cond_for = m.cond_for + [ AND ] + forbrow
	* 	ELSE
	* 		m.cond_for = forbrow
	* 	ENDIF
	* ENDIF

	IF TYPE ('m.cond_for') = 'C'
		* IF EMPTY(_SQL_DB)
		* IF ! _SQL_FLAG
		* IF ! _SQL_FLAG OR CURSORGETPROP ("SourceType", ntabla)<>2
*!*			IF ! TABLA_SQL (ntabla)
			DO FORM BROWLIS2 WITH ntitulo, nexpr, m.cond_for
*!*			ELSE
*!*				* DO FORM BROWLIS4 WITH ntitulo, nexpr, forbrow, m.cond_for
*!*				DO FORM BROWLIS3 WITH ntitulo, nexpr, forbrow, m.cond_for
*!*			ENDIF
	ELSE
		* IF EMPTY(_SQL_DB)
		* IF ! _SQL_FLAG
		* IF ! _SQL_FLAG OR CURSORGETPROP ("SourceType", ntabla)<>2
*!*			IF ! TABLA_SQL (ntabla)
			DO FORM BROWLIS2 WITH ntitulo, nexpr
*!*			ELSE
*!*				* DO FORM BROWLIS4 WITH ntitulo, nexpr, forbrow
*!*				DO FORM BROWLIS3 WITH ntitulo, nexpr, forbrow
*!*			ENDIF
	ENDIF
	
	* SET SYSMENU TO DEFAULT	&& Repongo el men� porque al entrar a un form
								&& modal desaparece
#ENDIF
	
	PRIVATE keyantbrow
	keyantbrow = LASTKEY()

	* ### VFP
	IF lEnter
		keyantbrow = 13
	ENDIF

	* Desactiva Jkey
	IF ! EMPTY(nindice) 
		=jkeycanc()
	ENDIF

	IF keyantbrow = 13 
		IF no_show
			=scat_campos()
		ELSE
			SCATTER MEMVAR 
			SHOW GETS OFF
			SHOW GET &in_aux_cpo
		ENDIF
		retorno = .T.                
* 	 Agregado por RODRIGO el 13/07/95
*		IF keyantbrow = 13
			KEYBOARD '{shift+f12}' CLEAR
			WAIT WIND ''
*		ENDIF

	ELSE
		* IF EMPTY(_SQL_DB)
		* IF ! _SQL_FLAG
		* IF ! _SQL_FLAG OR CURSORGETPROP ("SourceType", ntabla)<>2
*!*			IF ! TABLA_SQL (ntabla)
			IF ! SEEK (EVAL (wexp_ind))
				IF no_show
					=blank_campos()
				ELSE
					=blank_nocla()
				ENDIF
				retorno = .F.
			ELSE
				retorno = .T.  
			ENDIF
*!*			ELSE
*!*				IF no_show
*!*					=blank_campos()
*!*				ELSE
*!*					=blank_nocla()
*!*				ENDIF
*!*				retorno = .F.
*!*			ENDIF
	ENDIF
ELSE
	retorno = .F.
	= MSG ('No hay opciones disponibles')
ENDIF

* IF EMPTY(_SQL_DB)
* IF ! _SQL_FLAG
* IF ! _SQL_FLAG OR CURSORGETPROP ("SourceType", ntabla)<>2
*!*	IF ! TABLA_SQL (ntabla)
	SET KEY TO
*!*	ENDIF

#IFNDEF FORM_BROWSE
	RELEASE WINDOW vent
	DEACTIVATE WINDOW vent
#ENDIF

RETURN retorno


* ### VFP
FUNCTION PulsoEnter
lEnter = .T.

* PUBLIC _LASTKEY
* _LASTKEY = 13

KEYBOARD "{CTRL-W}" PLAIN

RETURN



*************************************************
FUNCTION arma_val      
*************************************************
* Busca campos en la funcion IN de Val_Tabla

PRIVATE pos1, pos2, i, car, len_cad

car  = ''
pos1 = 1
pos2 = 1
cont_comillas = 1
len_cad = LEN (nexpr)

DO WHILE  pos1 < len_cad

	car  = SUBSTR(nexpr,pos1,1)
	DO WHILE (car <> ':') AND ((car <> ',') AND (car <> ' ')) AND ;
			pos1 < len_cad + 1
		list_val[pos2] = list_val[pos2] + car
		pos1 = pos1 + 1
		car  = SUBSTR(nexpr,pos1,1)
	ENDDO
	list_val[pos2] = UPPER (list_val[pos2])

	pos_tit = pos1
	car_tit = car

	DO WHILE car_tit == ' '
		pos_tit = pos_tit + 1
		car_tit = SUBSTR(nexpr, pos_tit, 1)
	ENDDO

	IF car_tit = ':'
		pos_tit = AT('"', nexpr, cont_comillas)+1
		car_tit = SUBSTR(nexpr, pos_tit, 1)
		DO WHILE car_tit <> '"' AND pos_tit < len_cad + 1
			list_tit[pos2] = list_tit[pos2] + car_tit
			pos_tit = pos_tit + 1
			car_tit = SUBSTR(nexpr, pos_tit, 1)
		ENDDO
		cont_comillas = cont_comillas + 2
	ELSE
		list_tit[pos2] = list_val[pos2]
	ENDIF

	IF (pos2 < cant_val)
		pos1 = RAT(',', nexpr, cant_val - pos2) + 1
		DO WHILE SUBSTR(nexpr, pos1, 1) = ' '
			pos1 = pos1 + 1
		ENDDO
		pos2 = pos2 + 1
	ELSE
		EXIT 
	ENDIF

ENDDO

RETURN

*************************************************
FUNCTION arma_ind      
*************************************************
* Busca campos que componen el indice

PRIVATE pos1, pos2, aux_list_ind, len_cad
pos1 = 1
pos2 = 1
len_cad = LEN(wexp_ind)

DO WHILE pos1 <= len_cad AND pos2 <= cant_ind
	aux_list_ind = strword(wexp_ind, @pos1)
	IF !EMPTY(aux_list_ind)
		list_ind[pos2] = aux_list_ind
		pos2 = pos2 + 1
	ENDIF
ENDDO

RETURN
*

*************************************************
FUNCTION scat_campos
*************************************************
IF no_show
	IF TYPE (list_ind[cant_ind]) <> 'U'
		ult_cpo_ind = ntabla + SUBSTR (list_ind[cant_ind], 2)
		&list_ind[cant_ind] = EVAL (ult_cpo_ind)
	ENDIF
	FOR i=1 TO cant_val
		distinto = .F.
		FOR j=1 TO cant_ind
			IF "M."+list_val[i] <> list_ind[j]
				distinto = .T.
				EXIT
			ENDIF
		ENDFOR
		IF distinto
			br_nom_cpo = 'M.'+list_val[i]
			dsc_cpo = ntabla + '.' + list_val[i]
			IF TYPE (br_nom_cpo) <> 'U'
				&br_nom_cpo = EVAL (dsc_cpo)
			ENDIF
		ENDIF
	ENDFOR
ENDIF
RETURN retorno


*************************************************
FUNCTION blank_campos
*************************************************
FOR i=1 TO cant_val
	distinto = .F.
	FOR j=1 TO cant_ind
		IF "M."+list_val[i] <> list_ind[j]
			distinto = .T.
			EXIT
		ENDIF
	ENDFOR
	IF distinto
		br_nom_cpo = 'M.'+list_val[i]
		IF TYPE (br_nom_cpo) <> 'U'
			DO CASE
			CASE TYPE (br_nom_cpo) = 'C'
				&br_nom_cpo = SPACE(LEN(br_nom_cpo))
			CASE TYPE (br_nom_cpo) = 'D'										
				&br_nom_cpo = {}
			CASE TYPE (br_nom_cpo) = 'N'
				&br_nom_cpo = 0
			ENDCASE
		ENDIF
	ENDIF
ENDFOR


*************************************************
FUNCTION blank_nocla
*************************************************
PRIVATE int_val_cpos

DIMENSION int_val_cpos(cant_ind)
FOR I=1 TO cant_ind
	int_val_cpos[i] = EVAL (list_ind[i])
ENDFOR
SCATTER MEMVAR BLANK
FOR I=1 TO cant_ind
	&list_ind[i] = int_val_cpos[i]
ENDFOR


*************************************************
FUNCTION selec_ind
*************************************************

PRIVATE lista_ind, i_ind, ind_aux
DIME lista_ind[10]

FOR i_ind = 1 TO 10
    m.ind_aux = TAG(i_ind)
    IF ! (m.ind_aux == "")
        lista_ind[i_ind] = m.ind_aux    
    ELSE
        EXIT
    ENDIF
ENDFOR

i_ind = i_ind - 1

DO CASE
CASE i_ind > 1
	DEFI WIND wlista_ind AT 2,2 SIZE i_ind,20 IN SCREEN NOCLO NONE
	ACTI WIND wlista_ind
	@ 0,0 MENU lista_ind, i_ind TITLE "Seleccione Indice"
	READ MENU TO i_ind
	DEAC WIND wlista_ind

	IF i_ind < 1
		i_ind = 1
	ENDIF

CASE i = 1
	WAIT WIND 'La tabla de acceso solo tiene un indice. No se puede seleccionar.'
CASE i < 1
	WAIT WIND 'La tabla de acceso no tiene indices. No se puede seleccionar.'
ENDCASE

RETURN i_ind

**********************************
  FUNCTION get_alia
**********************************
PARAMETERS nombre
PRIVATE al
*,barra

*barra = AT( '\' , nombre )
*IF ( barra <> 0 )

al = SUBSTR( nombre , AT( '\' , nombre ) + 1 )

*ELSE
*   al = nombre
*ENDIF

RETURN al
*************************************************
FUNCTION getkey
*************************************************
* Funcionamiento
*		Devuelve una expresi�n de un �ndice con m. o no seg�n 
*		(con_eme) sea .T. o .F.
*
* Par�metros:
*		tab_de_expr  : (C) nombre de la tabla de la cual se quiere 
*									 la expresi�n del �ndice.
*		[m.con_eme]  : (L) (.F.) si es .t. me devuelve los campos 
*									 con M. adelante
* 	[cant_camp]  : (N) (todos los campos de la clave) cantidad de campos 
*									 de izq. a der. del �ndice que ser�n devueltos en 
*									 la expresi�n.
* 	[m.ind_expr] : (C) ('nombre_tag_1') cadena es el nombre del tag 
*									 del �ndice cuya expresi�n se quiere obtener.
*
* Ejemplo de uso y llamada:
*  expr = GETKEY ('tabla',.T.,2,'ptabla')
*	 =SEEK (&expr)
*  En expr se almacena una cadena del tipo:
*	 ' str(m.cod_tab,3,0) + m.nom_tabla '
**************************************************

PARAMETERS tab_de_expr, m.con_eme, cant_camp, m.ind_expr 

PRIVATE baseused, nro_tag, pos, ind_aux

IF type('tab_de_expr') <> 'C'
	WAIT WINDOW ' Par�metros Inv�lidos ' NOWAIT
	RETURN .F.
ENDIF
IF type('cant_camp') <> 'N'
	cant_camp = 0
ENDIF
* IF type('m.ind_expr') <> 'C'
IF ! type('m.ind_expr') $ 'CN'
	m.ind_expr = ''
ENDIF

* ### C/S
* IF _SQL_FLAG AND CURSORGETPROP("SourceType", tab_de_expr)==2
*!*	IF TABLA_SQL (tab_de_expr)
*!*		* Es una vista remota, utilizo la emulaci�n GETKEY para SQL 
*!*		m.ind_expr = IIF(EMPTY(m.ind_expr), 1, m.ind_expr)
*!*		RETURN ( SQL_GETKEY ( tab_de_expr, m.con_eme, cant_camp, m.ind_expr ))
*!*	ENDIF

**	Guarda estado actual
baseused = .T.
IF ! USED(tab_de_expr)
	=USET (tab_de_expr)
	baseused = .F.
ENDIF

IF empty(m.ind_expr)
	nro_tag = 1
ELSE
	nro_tag = 1
	m.ind_expr = UPPER(m.ind_expr)
	m.ind_aux  = TAG (nro_tag, m.tab_de_expr)
	
	DO WHILE !(m.ind_aux == m.ind_expr) AND !(m.ind_aux == '')
		nro_tag = nro_tag + 1
		m.ind_aux  = TAG (nro_tag, m.tab_de_expr)
	ENDDO

	IF !(m.ind_aux == m.ind_expr)
		=MSG(' Indice ' + m.ind_expr + ' inexistente en la tabla '+UPPER(tab_de_expr))
		RETURN .f.
	ENDIF
ENDIF

m.expr  = SYS(14, nro_tag, tab_de_expr)
pos   = 0
IF cant_camp > 0
	pos   = AT('+', m.expr, cant_camp)
ENDIF
IF pos <> 0
	m.expr = LEFT(m.expr,pos-1)
ENDIF

IF m.con_eme
	m.expr = add_m(m.expr)
ENDIF

**	reestablece entorno
IF baseused = .F.
	USE IN (ntabla)
ENDIF

RETURN m.expr

****************
FUNCTION add_m

PARAMETERS cad

PRIVATE i,exp,pal,long,car

i = 1
exp = ''
pal = ''

long = LEN(cad)

do while i <= long 
	car = substr(cad,i,1)
	if	(car <> ' ') AND (car <> '+') AND (car <> '(') AND (car <> ')') AND (car <> ',')
		pal = pal+car
	else
		if !empty(pal) 
			if	!(pal='LEFT' OR pal='RIGHT' OR pal='STR' OR pal='UPPER' OR ;
					pal='ALLTRIM' OR pal='DTOC' OR pal='DTOS' OR ISDIGIT(pal))
				pal = 'M.'+pal
				exp = exp+pal
				exp = exp+car
			else
				exp = exp+pal
				exp = exp+car
			endif
			pal = ''
		else
			exp = exp+car
		endif
	endif
	i = i+1
enddo

if !empty(pal) 
	if	!(pal='LEFT' OR pal='RIGHT' OR pal='STR' OR pal='UPPER' OR ;
		pal='ALLTRIM' OR pal='DTOC' OR pal='DTOS' OR ;
		LEFT (pal, 1) >= '0' AND LEFT (pal, 1) <= '9')
		pal = 'M.'+pal
		exp = exp+pal
	endif
	pal = ''
endif

RETURN exp
*

*************************************************
FUNCTION WHE_GRAL
*************************************************
* Los select que estan de m�s dejarlos porque no afectan a la performance
* y mejoran la seguridad


PARAMETERS wcpo
PRIVATE wretorno, wexpresion, wno_grup, ;
	wtab_whe, wreg_whe, wencontro, wes_lista, walias, wlastkey

IF PARAMETERS() # 1
	WAIT WIND "Mal los parametros para la funcion WHE_GRAL()"
	RETU
ENDIF

wretorno   = .T.
wencontro  = .T.
wno_grup   = .F.
wlastkey = LASTKEY ()

walias = SELE ()

wtab_whe = WONTOP()
IF wtab_whe = 'CONTROLS'
	wtab_whe = vent_ppal
ENDIF

IF EMPTY(SELECT(wtab_whe))
	RETURN
ENDIF

SELE (wtab_whe)

wcpo = PADR(wcpo, 10)
wes_lista = .F.
IF SEEK (wcpo + 'W', wtab_whe)
	IF tipo_cpo = 'LST'
		wes_lista = .T.
	ENDIF
ELSE
	wencontro = .F.
ENDIF

SELE (wtab_whe)
IF wLASTKEY # 27 OR wes_lista
	IF wencontro
		wreg_whe    = recno (wtab_whe)

		IF !EMPTY(grup_mem)
			IF !(grup_mem == UPPE( ALLTRIM(nom_grup)))
				wretorno = VAL_GRAL(grup_mem, .F., .T.)
			ELSE
				wno_grup = .T.
			ENDIF
		ENDIF
		SELE (wtab_whe)
		GO wreg_whe
	ELSE
		IF !EMPTY(grup_mem)
			wretorno = VAL_GRAL(grup_mem, .F., .T.)
		ELSE
			wno_grup = .T.
		ENDIF
	ENDIF
ELSE
	grup_mem = ''
ENDIF

SELE (wtab_whe)
IF wretorno
	IF ! wno_grup
		IF wencontro
			grup_mem = UPPE(ALLTRIM(nom_grup))
		ELSE
			grup_mem = ''
		ENDIF
	ENDIF
	IF WONTOP() <> 'controls' AND wlastkey # 27
		gs_num_cpo   = _CUROBJ
		* gs_valor_cpo = EVAL ('m.'+ wcpo)	### VFP
		IF !EMPTY(wcpo)
	        ***sentecias agregadas (3) por cesar para conversion de vfp	
		    if upper(wcpo)='EL_EJERCIC'
               wcpo = 'EL_EJERCICIO'
		    endif
		    ***fin de las sentencias agregadas para la conversion.

			gs_valor_cpo = EVAL ('m.'+ wcpo)
		ELSE
			gs_valor_cpo = .F.
		ENDIF
	ENDIF
	IF wencontro
		wretorno=PART2()	 	
	ENDIF
ELSE
	IF wlastkey # 27
		gs_num_cpo   = _CUROBJ
		gs_valor_cpo = EVAL ('m.'+ wcpo)
*		_CUROBJ	= gs_num_cpo
		wretorno = .T.
	ENDIF
ENDIF

*IF wretorno AND wlastkey # 27
*	KEYBOARD '{shift+f12}' CLEAR
*	WAIT WIND ''
*ENDIF

SELE (walias)

* ### VFP
IF wretorno AND TYPE (wcpo)=="C"
	* Para emular FPD26 asigno el MaxWidth de los TextBoxes con char para no permitir editar m�s all�
	* del tama�o del campo
	PRIVATE m.niv_act, m.nivel, m.prog, m.form, m.obj

	* m.acum = ""
	m.niv_act = PROGRAM(-1)

	FOR m.nivel = m.niv_act-1 TO 1 STEP -1
		* m.acum = m.acum + PROGRAM (m.nivel) + CHR(13)+CHR(10)
		m.prog = PROGRAM (m.nivel)
		IF ".WHEN" $ m.prog
			m.nivel = RAT(".WHEN", m.prog)
			m.prog = LEFT (m.prog, m.nivel-1)

			m.nivel = AT(".", m.prog)
			m.prog  = SUBSTR (m.prog, m.nivel+1)	&& Elimino el nivel del FormSet
			m.nivel = AT(".", m.prog)
			m.form  = FormID (LEFT (m.prog, m.nivel-1))

			m.nivel = AT(".PAGEFRAME1", m.prog)
			* m.prog  = "_Screen.ActiveForm" + SUBSTR (m.prog, m.nivel)
			m.prog  = "_Screen.Forms (" + ALLTRIM(STR(m.form)) + ")" + SUBSTR (m.prog, m.nivel)
			
			IF ! "." $ wcpo		&& Si no tiene alias asumo m.wcpo
				wcpo = "m." + wcpo
			ENDIF
			
			m.obj = &prog
			IF UPPER (obj.BaseClass)=="TEXTBOX"
				* &prog..MaxLength = LEN (EVAL (wcpo))
				obj.MaxLength = LEN (EVAL (wcpo))
			ENDIF
			
			EXIT
		ENDIF
	NEXT
ENDIF

RETURN wretorno

*************************************************
FUNCTION PART2
*************************************************
PRIVATE wretorno
wretorno = .T.

SELE (wtab_whe)
DO WHIL !EOF(WTAB_WHE) AND wretorno AND indica='W' AND wcpo == campo
	wreg_whe    = recno (wtab_whe)
	
	IF !ISBLANK (funcion)
		wexpresion		= funcion+'('+ DEL_CHR (contenido) +')'
		wretorno 		= EVAL (wexpresion)
	ENDIF

	SELE (wtab_whe)
	GO wreg_whe
	SKIP
ENDDO
RETURN wretorno
FUNCTION sin_uso
PARAMETER nomarch
* nomarch : debe incluir disco y camino
* Devuelve .T. si nomarch existe y est� libre
PRIVATE retorno, mane_arch
mane_arch = FOPEN (nomarch) 
IF mane_arch = -1
	retorno = .F.
ELSE
	= FCLOSE (mane_arch)
	retorno = .T.
ENDIF
RETURN retorno
***************************************************
FUNCTION VAL_GRAB
*************************************************
* Autor: RODRIGO
* 
* Fecha: 08/10/94
* 
* Funcionamiento: Valida los campos de la DBF interna (DBF_GRAL) en la secuencia
* en que se encuentran ubicados en la pantalla. Dependiendo el parametro que se 
* le pase valida TODOS los campos, los campos CLAVES Y los campos NO CLAVES.
* 
* Par�metros: GQUE_VAL		(C) : Permite solo estos valores:
*																											T: Valida TODOS los campos
*																											C: Valida solo CLAVES 
* 																										D: Valida solo NO CLAVES
* 
* Modificaciones: 14/08/1994 
*									Se cambiaron los nombres de variables, y el manejo de las
*	Tablas de Validaciones que causaban problemas al interactuar con los pro-
*	gramas.
*
*	06/01/95	(RODRIGO) El seteo de la variable grup_mem se hacia en todos lados cuando
* deber�a hacerse solo si "corresponde". Un "if retorno" al salir de la parte que valida 
* grupos.
*
 
PARAMETERS gque_val
PRIVATE gretorno, gi, guna_vez,gn, gcorresp, gcpo, gnom_grup, gclave

IF PARAMETERS() # 1
	WAIT WIND "Error al pasar los parametros de la funcion VAL_GRAB"
	RETU .F.
ELSE
	gque_val = UPPER (gque_val)
	IF gque_val = 'T' OR gque_val = 'D' OR gque_val = 'C'
		gretorno = .T.
	ELSE
		WAIT WIND "Error al pasar los parametros de la funcion VAL_GRAB"
		RETU .F.
	ENDIF
ENDIF

IF LAST () = 27
	KEYBOARD '{shift+f12}' CLEAR
	WAIT WIND ''
ENDIF

gi 				= 1
guna_vez 	= .T.
gn 				= UltCpoPant()

gtab_gra	= WONTOP()
IF gtab_gra = 'CONTROLS'
	gi       = 15
	gtab_gra = vent_ppal
ENDIF

DO WHILE (gi <= gn ) AND gretorno
	
	gcpo = SUBSTR(OBJVAR (gi),3,10)
	gcpo = PADR(ALLTRIM(UPPER(gcpo)),10)
	* IF SEEK (gcpo+'V', gtab_gra)	### C/S
	IF SEEK (gcpo+'V', gtab_gra, "PRIMARIO")
		gnom_grup = EVAL (gtab_gra + '.nom_grup')
		gclave		= EVAL (gtab_gra + '.clave')
		IF guna_vez
			guna_vez = .F.
		ELSE
			IF !(grup_mem == UPPER(ALLTRIM(gnom_grup)))
				IF !EMPTY(grup_mem)
					gretorno = VAL_GRAL(grup_mem,.T.,.T.)
				ENDIF
			ENDIF
		ENDIF
		IF gretorno
			corresp		= .F.
			DO CASE
	
			CASE gque_val = 'T'
				corresp = .T.
	
			CASE gque_val = 'D'		
				IF gclave # 'CL ' AND gclave # 'CLU'
					corresp = .T.
				ENDIF
	
			CASE gque_val = 'C'
				IF gclave = 'CL ' OR gclave = 'CLU'
					corresp = .T.
				ENDIF

			OTHERWISE
				WAIT WIND "Error en el pedido: " +gque_val+" de VAL_GRAB"
				gretorno = .F.

			ENDCASE
		
			IF corresp
				gretorno 	= VAL_GRAL (gcpo, .T.)
				
				* IF !EMPTY(_SQL_DB) AND !gretorno
*!*					IF _SQL_FLAG AND !gretorno
*!*						* Fuerzo a posicionarme en el objeto que di� error
*!*						* _CUROBJ = gi		### C/S
*!*						SET_CUROBJ (gi)
*!*					ENDIF
				
*				grup_mem  = UPPER(ALLTRIM(EVAL (gtab_gra + '.nom_grup')))
				grup_mem  = UPPER(ALLTRIM(gnom_grup))
			ENDIF
		ENDIF
	ENDIF
	gi = gi + 1
ENDDO

RETURN gretorno
*


*

**********************************
FUNCTION get_cant
**********************************
PARAMETERS m.tip_imp
PRIVATE m.ret

DO CASE
CASE m.tip_imp = 1
 m.ret = 7
CASE m.tip_imp = 2 
 m.ret = 3
CASE m.tip_imp = 3
 m.ret = 4
ENDCASE

RETURN m.ret

*

*************************************************
FUNCTION StrWord
*************************************************
* Funcionamiento:
*  Devuelve la palabra de una cadena comenzando en una posicion.
*  Devuelve en el segundo par�metro la posici�n del fin de la pal. devuelta
*  Permite tomar todas las palabras de una cadena si se llama sucesivamente
*  , la primera vez la posicion es 1.
*
*  Par�metros:
*  cad_a_pal      : (C) cadena de la cual se extraen las palabras
*  pos_inicial    : (N) posici�n a partir de la cual se devuelve 
*										la palabra. Si se pasa este par�metro por referencia
*										devuelve la posici�n en la que termina la palabra
*  [sin_m]        : (L) (.F.)  .F.= devuelve palabras con m. 
*  [letras_solas] : (L) (.F.)  .F.= No devuelve letras solas
*************************************************
*
*	PS
*		recibe cadena pos_inicial
*
*		palabra ''
*		long cadena
*		i = pos_inicial
*		toma caracter
*
*		mientras i <= long
*			si finpalabra y palabra no vacia
*				finpalabra = .F.
*			finsi
*
*			case letra o '_'
*				anexa a palabra
*			case numero y palabra no vacia
*				anexa a palabra
*			case numero
*			case '+' o '-' o ')'
*				finpalabra = .T.
*			case '('
*				palabra = ''
*			fincase
*
*			si finpalabra y palabra no vacia
*				salir
*			finsi
*			i = i + 1
*			si i <= long
*				toma caracter
*			finsi
*		finmientras
*		pos_inicial = i
*		retorna palabra
	
* EJEMPLO
*  USE bind\abmgral.scx
*  LOCATE FOR ( (objtype = 1) AND (objcode = 63))
*  IF FOUND ()
*	 long = len (proccode)
*	 i = 1
*	 cadena = proccode
*	 DO while i <= long
*	 	? strword (cadena, @i,.t.,.T.)
*	 ENDDO
*  ENDIF

PARAMETERS cad_a_pal, pos_inicial, sin_m, letras_solas

	private i, palabra, longitud, caracter, fin_pal
	i        =  pos_inicial
	palabra  = ''
	long     = LEN (cad_a_pal)
	fin_pal  = .F.
	i =  pos_inicial
	IF i <= long
		caracter = substr (cad_a_pal, i, 1)
	ENDIF
	DO WHILE i <= long
 		DO CASE
		CASE ISALPHA (caracter) OR caracter = '_' 
			palabra = palabra + caracter
		CASE ISDIGIT (caracter) AND !EMPTY (palabra)
			palabra = palabra + caracter
		CASE caracter = '.' AND UPPER (substr (cad_a_pal, i-1, 1)) = 'M' AND ! sin_m 
			palabra = palabra + caracter
		OTHERWISE
			fin_pal = .T.
		ENDCASE
		
		IF (caracter = CHR(9) OR caracter = CHR(32)) 
			DO WHILE i < long
				caracter = substr (cad_a_pal, i+1, 1)
				IF caracter = CHR(9) OR caracter = CHR(32) 	
					i = i + 1
				ELSE
					EXIT 
				ENDIF
			ENDDO
		ENDIF

		IF caracter = '(' 
			palabra = ''
		ENDIF
	
		IF fin_pal 
			IF LEN(palabra) = 1 AND ! letras_solas 
				palabra = ''
			ENDIF
			IF !EMPTY (palabra)
				EXIT
			ELSE
				fin_pal = .F.
			ENDIF
		ENDIF
		i = i + 1
		IF i <= long
			caracter = substr (cad_a_pal, i, 1)
		ENDIF
	ENDDO

  pos_inicial = i
	RETURN palabra
	*
	*************************************************
FUNCTION UltCpoPant
*************************************************
* Funcionamiento:
*
* Devuelve el n�mero de objeto del �ltimo campo
* de la pantalla.
*
* Par�metros:
*
* No tiene.
*


PRIVATE on_error, error, i, ult_cpo

on_error = ON ('ERROR')

ON ERROR error = .T.

i = 1
error = .F.
ult_cpo = ''

* ### VFP
*!*	do while !error
*!*		= objvar (i)
*!*		IF !error AND !EMPTY (OBJVAR (i))
*!*			ult_cpo = objvar (i)
*!*		ENDIF
*!*		i = i + 1
*!*	enddo

DO WHILE !EMPTY(OBJVAR(i))
	ult_cpo = OBJVAR(i)
	* WAIT WIND STR(i)+":"+ult_cpo
	i = i + 1
ENDDO

ON ERROR &on_error

IF AT ('(', ult_cpo) > 0
	ult_cpo = SUBSTR (ult_cpo, AT ('M.', ult_cpo) + 2)
ENDIF

* WAIT WIND "Ultimo: "+ult_cpo

* return OBJNUM( &ult_cpo )		### VFP
return OBJNUM2(ult_cpo)
*


**************************************************
FUNCTION SINO
**************************************************
* Funcionamiento:
* 	Muestra un mensaje dentro de una ventana y pide una 
* confirmaci�n para continuar. 
*		Se usa para pedir confirmaciones.(ej.: BORRA ? )
*
*		Devuelve .T. si se selecciona < Si >
*		Devuelve .F. si se selecciona < No >
*
* Par�metros
*  [mensaje] : (C) ('Su elecci�n . . .?') Mensaje que se 
*							 quiere mostrar.
**************************************************



* SINO seleccion de opciones SI / NO

PARAMETERS m.mensaje
PUSH KEY
=DEAC_TEC()
PRIVATE m.mensaje, largomen, m.pos, m.pos1, largomemo, m.altura, m_mline
PRIVATE i

IF LAST () = 27
	KEYBOARD '{shift+f12}' CLEAR
	WAIT WIND ''
ENDIF

IF TYPE('m.mensaje') = "L"
	m.mensaje = "Su elecci�n . . . ?"
ENDIF

largomemo = SET("MEMOWIDTH")
m.altura = 1
m.mensaje = ALLTRIM(m.mensaje)
largomen = LEN(m.mensaje)

IF largomen < 25
	largomen = 30
ELSE
	IF largomen > 40
		SET MEMOWIDTH TO 40
		m.altura = MEMLINES(m.mensaje)				
		largomen = 46
	ELSE
		largomen = largomen+6
	ENDIF
ENDIF

#IFDEF SINO_MSGB
	m.sino = MESSAGEBOX(m.mensaje, 3, "Consulta")
	m.sino = IIF(m.sino==6, 1, 0)
#ELSE
	IF NOT WEXIST("_qdv0ttc1u")
		DEFINE WINDOW _qdv0ttc1u ;
			FROM INT((SROW()-(6+m.altura))/2),INT((SCOL()-largomen)/2) ;
			TO INT((SROW()-(6+m.altura))/2)+(5+m.altura),INT((SCOL()-largomen)/2)+largomen-1 ;
			FLOAT ;
			NOCLOSE ;
			SHADOW ;
			DOUBLE ;
			COLOR SCHEME 7
	ENDIF

	#REGION 1

	IF WVISIBLE("_qdv0ttc1u")
		ACTIVATE WINDOW _qdv0ttc1u SAME
	ELSE
		ACTIVATE WINDOW _qdv0ttc1u NOSHOW
	ENDIF

	m_mline = _mline
	FOR i=1 TO m.altura
		m.pos = (largomen-2-LEN(MLINE(m.mensaje,i)))/2
		@ i,m.pos SAY MLINE(m.mensaje,i)
	ENDFOR
	_mline = m_mline

	m.pos1 = (largomen-2-19)/2
	@ m.altura+2,m.pos1 GET m.sino ;
		PICTURE "@*HN \!\<Si;\?\<No" ;
		SIZE 1,8,3 ;
		DEFAULT 1 ;
		VALID _qdv0tterb() ;
		MESSAGE "Si: confirma      -      No: anula"

	IF NOT WVISIBLE("_qdv0ttc1u")
		ACTIVATE WINDOW _qdv0ttc1u
	ENDIF

	READ CYCLE MODAL
	RELEASE WINDOW _qdv0ttc1u

	if wontop() == ''
		@ 24,0 CLEAR
	endif
#ENDIF

SET MEMOWIDTH TO largomemo

KEYBOARD '{shift+f12}' CLEAR
WAIT WIND ''
POP KEY
IF m.sino = 1
	RETURN .T.
ELSE
	RETURN .F.
ENDIF
	
FUNCTION _qdv0tterb     &&  m.sino VALID
	#REGION 1
	CLEAR READ


	
*
*************************************************
FUNCTION Msg_Back
*************************************************
* Autor : Gabriel
* Dise�o: Gabriel
* 
* Fecha : 12/6/95
* 
* Funcionamiento:
* Dado un alias y un mensaje si se le indica puede deslockear una tabla,
* todas las tablas o ninguna 
* Ademas el mensaje puede ser de mens_avi que se indica con el 4to param.
* Esta fx debe ser invocada cuando hay una transaccion activa.
* Si Ud. Tiene en el proceso superior un vble. M.ROLLB esta se pondra 
* en .T. si ha realizado la finalizacion de transaccion. Si al iniciar
* el proceso la inicializa con .F. al final puede saber la situacion
*
* Par�metros:
* alias   -C- alias sobre la cual se quiere desloquear. 
* msg     -C- Mensaje ha ser emitido.
* [unall] -L- Flag para indicar si Desloquea todas las tablas
* [mavi]  -L- Flag para indicar que el mensaje es de mens_avi
*  
*
* Modificaciones:
* 
PARAMETERS m.alias, m.msg, m.unall, m.mavi

IF PARAMETERS() < 2
	=msg('Debe pasar como m�nimo 2 par�metros a la Fx Msg_Back, alias y mensaje')
	RETURN ''
ENDIF

IF !EMPTY( m.alias )
	UNLOCK IN m.alias
ENDIF

IF m.unall
	UNLOCK ALL
ENDIF

=RLLBACK()
m.ROLLB = .T.

IF !EMPTY( m.msg )
	IF !m.mavi
		= msg( m.msg )	
	ELSE
		=mens_avi( m.sistema , m.msg )
	ENDIF
ENDIF

RETURN ''
*

*************************************************
FUNCTION subraya
*************************************************
PARAMETER cadena
cadena = ALLTRIM(cadena)
RETURN REPLICATE('-', LEN(cadena))
*
*************************************************
FUNCTION APATH
*************************************************
* Autor: RODRIGO 
* 
* Fecha: 10/11/94
* 
* Funcionamiento: Agrega o saca un path a la variable de ambiente PATH
* 
* Par�metros		: EXP_PATH	(C) : Expresion a agregar o sacar al PATH
* (full version)	ACCION		(C)	:	Accion a realizar (+) o (-)
* 
* 
* Modificaciones:
* 
PARAMETERS exp_path, accion
PRIVATE aux1

IF EMPTY(accion)
	accion = '+'
ENDIF

exp_path = UPPER (exp_path)

aux1 = SET('PATH')
DO CASE 
CASE accion = '+'
	IF RIGHT (exp_path,1) = '\'
		aux1 = exp_path+';'+aux1
	ELSE
		aux1 = exp_path+'\;'+aux1
	ENDIF
CASE accion = '-'
	IF RIGHT (exp_path,1) # '\'
		exp_path = exp_path+'\'
	ENDIF
	apartir = RAT(exp_path,aux1)
	IF apartir # 0
		aux1		= STUFF (aux1, apartir, LEN(exp_path)+1, '') && +1 POR EL ';'
	ENDIF
ENDCASE
SET PATH TO &aux1
? SET ('PATH')

RETURN
*
*************************************************
FUNCTION HAY_SEEK
*************************************************
* Funcionamiento: Se usa para validar la existencia de un
* 								dato en otra tabla. Ya sea en el momento 
* 								de borrar o grabar algun registro.
*
* Par�metros:
*					TABLA 		(C)	:	Tabla sobre la cual se va a validar
*														con SISTEMA.
* 					CANT_CPOS (N)	:	Cantidad de campos del �ndice.
* 					NOM_TAG		(C)	:	Nombre del �ndice de la tabla a validar.
*
* NOTA: No ser�a necesario validar la existencia de 
* la tabla e indice si ya lo hace USET()
*


PARAMETERS tabla, cant_cpos, nom_tag
PRIVATE gs_curr_dbf, gs_puntero, retorno, exp_ind, usada

retorno = .T.
usada	= .T.
IF !BETWEEN(PARAMETERS(),1,3)
	WAIT WINDOW "Faltan parametros para la Funci�n HAY_SEEK()"
	retorno = .F.
ENDIF

IF RAT('\',tabla) # 0
	IF !USED(tabla)
		=USET(tabla)
		usada = .F.
	ENDIF
	tabla   = SUBSTR (tabla, RAT('\',tabla) + 1)
ELSE
	IF !USED(tabla)
		=MSG('La tabla NO esta abierta. Debe pasar el par�metro con SISTEMA')
		retorno = .F.
	ENDIF
ENDIF

IF retorno
	gs_curr_dbf = ALIAS()
	gs_puntero	= RECNO()
	gs_nom_tag = TAG()
	SELE (tabla)
	IF !EMPTY(nom_tag)
		SET ORDER TO TAG (nom_tag)
	ENDIF
	exp_ind = GETKEY (tabla, .T. , cant_cpos , nom_tag)
	SEEK &exp_ind
	IF FOUND()
		retorno = .T.
	ELSE
		retorno = .F.
	ENDIF
	SET ORDER TO TAG (gs_nom_tag)
	IF usada
		=TAB_VAC(gs_puntero)
		SELE (gs_curr_dbf)
	ELSE
		USE
	ENDIF
ENDIF


RETURN retorno
*************************************************
FUNCTION GETSLIST
*************************************************
* Autor:Leonardo
* 
* Fecha:16/10/94
* 
* Funcionamiento:
* Elimina la llamada a errores por problemas con la lista
* al ejecutar la linea SHOW GETS
* 
* Par�metros: parametro con el comando a pasar al SHOW GETS
* 
PARAMETERS show_gets

IF TYPE ('show_gets') # 'C'
	show_gets = ''
ENDIF

show_gets = UPPER(show_gets)

IF TYPE ('l_lista(1,1)') = 'C'
	DIMENSION l_lista (ALEN (l_lista, 1), ALEN (l_lista, 2))
	FOR i = 1 TO ALEN (l_lista, 1)
		IF RECCOUNT (l_lista (i, 2)) = 0
			SHOW GET &l_lista(i,1)
		ENDIF
	ENDFOR
ENDIF

SHOW GETS &SHOW_GETS

RETURN .T.

*

*************************************************
FUNCTION GETLIST
*************************************************
* Autor:Leonardo
* 
* Fecha:16/10/94
* 
* Funcionamiento:
* Elimina la llamada a errores por problemas con la lista
* al ejecutar la linea SHOW GET
* 
* Par�metros: parametro con el comando a pasar al SHOW GET
* 
PARAMETERS show_get

IF TYPE ('show_get') # 'C'
	show_get = ''
ENDIF

show_get = UPPER(show_get)

IF TYPE ('l_lista(1,1)') = 'C'
	DIMENSION l_lista (ALEN (l_lista, 1), ALEN (l_lista, 2))
	FOR i = 1 TO ALEN (l_lista, 1)
		IF l_lista (i, 1) $ show_get
			IF RECCOUNT (l_lista (i, 2)) = 0
*!*				IF DBRECCOUNT (l_lista (i, 2)) = 0
				SHOW GET &l_lista(i,1)
				EXIT
			ENDIF
		ENDIF
	ENDFOR
ENDIF

SHOW GET &SHOW_GET

RETURN .T.

***********************************************
FUNCTION diahabil
***********************************************
*
*	Esta funci�n sirve para evaluar, dada una fecha, si corresponde a un
* d�a laborable o no, y en este �ltimo caso, tener la posibilidad de 
* determinar por que no lo es.
* Autor        : Miguel 05/05/94
*
* Modificaci�n : Gabriel 10/12/94
*
****************************************************************************
PARAMETERS m.fecha, flag, satflag

********************* DESCRIPCION DE PARAMETROS ****************************
*
*	fecha = fecha a evaluar
*			tipo fecha
*
*	flag  = dato que devuelve la funci�n
*			se aclara abajo cuales son los datos que devuelve		
*			valores 0, 1, 2
*			tipo num,rico (opcional)
*
*	satflag = si es s�bado, tomarlo o no como laborable
*			  .T. = s�bado laborable
*			  .F. = s�bado no laborable
*			  tipo booleano (opcional)
*
****************** DESCRIPCION DEL PARAMETRO FLAG **************************
*
* El par�metro flag (num,rico) indica que valor devuelve la funci�n.
*
*	0 = devuelve .T. si el d�a es laborable
*		devuelve .F. si el d�a es no laborable
*
*	1 = devuelve:
*			0 = d�a h�bil laborable normal
*			1 = d�a no h�bil no laborable normal por ser fin de semana
*				(el s�bado depende del par�metro satflag)
*			2 = d�a feriado pero laborable
*			3 = d�a feriado no laborable (feriado normal)
*			4 = d�a h�bil normal no laborable
*			5 = d�a no h�bil normal (fin de semana) laborable
*
*	2 = devuelve una cadena descriptiva seg�n el caso correspondiente.
*		para los casos:
*			0 = "D�a laborable"
*			1 = "S�bado" � "Domingo"  seg�n corresponda
*			2 a 5 = La cadena que esta contenida en el campo descripci�n
*					del registro correspondiente en la tabla feriados
*
****************************************************************************


******************** VERIFICACION DE PARAMETROS ****************************

DO CASE
	CASE PARAMETERS() < 1
		SET NOTIFY ON
		WAIT WINDOW	 "N�mero insuficiente de par�metros ..." NOWAIT
		SET NOTIFY OFF
		?? CHR(7)
		RETURN .F.				
	CASE PARAMETERS() = 1
		flag = 0
		satflag = .F.
	CASE PARAMETERS() = 2	
		satflag = .F.
ENDCASE

IF flag < 0 OR flag > 2
	flag = 0
ENDIF

****************************************************************************


* SEEK RIGH("0"+ALLTRIM(STR(DAY(fecha))),2)+RIGH("0"+ALLTRIM(STR(MONTH(fecha))),2)+RIGH("0"+ALLTRIM(STR(YEAR(fecha))),2)
* SEEK DTOC (FECHA,1)
* SUSP
IF SEEK( DTOC (m.fecha,1) , 'feriados' )
	DO CASE
		CASE feriados.feria = 'S' AND feriados.labor = 'S'   && Feriado Laborable
			DO CASE
				CASE flag = 0
					RETURN .T.
				CASE flag = 1
					RETURN 2
				CASE flag = 2
					RETURN IIF (ISBLANK (feriados.desc_fer), "Sin Descripci�n", feriados.desc_fer)
			ENDCASE
	
		CASE feriados.feria = 'S' AND feriados.labor = 'N'   && Feriado No Laborable
			DO CASE
				CASE flag = 0
					RETURN .F.
				CASE flag = 1
					RETURN 3
				CASE flag = 2
					RETURN IIF (ISBLANK (feriados.desc_fer), "Sin Descripci�n", feriados.desc_fer)
			ENDCASE

		CASE feriados.feria = 'N' AND feriados.labor = 'S'   && No Habil Laborable
			DO CASE
				CASE flag = 0
					RETURN .T.
				CASE flag = 1
					RETURN 5
				CASE flag = 2
					RETURN IIF (ISBLANK (feriados.desc_fer), "Sin Descripci�n", feriados.desc_fer)
			ENDCASE

		CASE feriados.feria = 'N' AND feriados.labor = 'N'   && Habil No Feriado No Laborable 
			DO CASE
				CASE flag = 0
					RETURN .F.
				CASE flag = 1
					RETURN 4
				CASE flag = 2
					RETURN IIF (ISBLANK (feriados.desc_fer), "Sin Descripci�n", feriados.desc_fer)
			ENDCASE

	ENDCASE
ELSE
	*	SEEK RIGH("0"+ALLTRIM(STR(DAY(fecha))),2)+RIGH("0"+ALLTRIM(STR(MONTH(fecha))),2)+"00"
	FECHAC = CTOD (ALLTRIM(STR(DAY(m.fecha)))+'/'+ALLTRIM(STR(MONTH(m.fecha)))+"/00")
	* SEEK DTOC (FECHAC,1)
	
	IF SEEK( DTOC( FECHAC , 1 ) , 'feriados' )		&& Feriado No Laborable 
		DO CASE
			CASE flag = 0
				RETURN .F.
			CASE flag = 1
				RETURN 3
			CASE flag = 2
				RETURN IIF (ISBLANK (feriados.desc_fer), "Sin Descripci�n", feriados.desc_fer)
		ENDCASE
	ELSE
		IF DOW(m.fecha) = 1 OR (DOW(m.fecha) = 7 AND !satflag)
			DO CASE						&& D�a de Fin de Semana No Laborable
				CASE flag = 0			&& Segun el flag de S�bado Laborable
					RETURN .F.
				CASE flag = 1
					RETURN 1
				CASE flag = 2	
					IF DOW(m.fecha) = 1	
						RETURN "Domingo"
					ELSE
						RETURN "S�bado"
					ENDIF
			ENDCASE
		ELSE
			DO CASE						&& D�a Normal Laborable
				CASE flag = 0
					RETURN .T.
				CASE flag = 1
					RETURN 0
				CASE flag = 2
					RETURN "D�a laborable"
			ENDCASE
		ENDIF
	ENDIF
ENDIF

RETURN .F.
*************************************************
FUNCTION EVALMEMO
*************************************************
* Autor : Rodrigo
* Dise�o: Rodrigo
* Dada una f�rmula y un tipo debe evaluarla y validarla contra el tipo
* devolviendo el valor evaluado (Se captura el ONERROR)
*
* Par�metros:
* formula -C- Fla para evaluar.
* Tipofla -C- Tipo que debe ser. Caracter, Numerica, Logica, etc.
*
* NOTA:
*				Se deben setear estas variables al principio del programa	
*				principal con:
*
*											ONERROR 		: ON('error')
*											[LINE2LINE] : (Opcional) Ejecuta un Prg. Se setea 
*																		con cualquier valor. (C,N,L,Etc.)		
*											[EVAL_ERROR]:	(Opcional) Es el retorno de la Fx.(L)
*
*	MODIFICACION: RODRIGO 12/07/95 .- Se agregaron 2 variables a la Fx
*	La variable LINE2LINE que si esta seteada con un valor antes de 
* que la Fx sea llamada realiza una ejecuci�n del programa linea a 
* linea en el memo a evaluar. Tambien se agreg� la variable EVAL_ERROR
* que tambien debe ser definida antes de llamar a la Fx y devuelve el
* retorno. Se modific� el mensaje de error para que indique el no. de
* linea en la que se produjo el error.
*
PARAMETERS m.formula, m.tipofla, msg
PRIVATE m.nerror, m.exp, ancho, i, msg_error

ancho = SET ('MEMO')
msg_error = ''
SET MEMO TO 256

i					= 0
m.nerror 	= " "
IF !EMPTY(ALLTRIM(m.formula) )
	ON ERROR DO CAPT_ERR WITH message(), i
	m.exp 		= EVAL_FX( ALLTRIM(m.formula) )
	on error &onerror
	IF TYPE('m.exp') # m.tipofla
		IF msg
			=MSG('El tipo de dato de la expresi�n ' + m.formula + ' es inv�lido ')
		ENDIF
		DO CASE
		CASE m.tipofla = 'C'
			m.exp	=	''
		CASE m.tipofla = 'N'
			m.exp	=	0
		CASE m.tipofla = 'L'
			m.exp	=	.T.
		CASE m.tipofla = 'D'
			m.exp	=	{}
		ENDCASE
	ENDIF
ELSE
	DO CASE
	CASE m.tipofla = 'C'
		m.exp	=	''
	CASE m.tipofla = 'N'
		m.exp	=	0
	CASE m.tipofla = 'L'
		m.exp	=	.T.
	CASE m.tipofla = 'D'
		m.exp	=	{}
	ENDCASE
ENDIF

IF !(m.nerror == " " )
	IF msg
		=MSG(msg_error)
  ENDIF
	eval_error = .T.
ELSE
	eval_error = .F.
ENDIF

SET MEMO TO (ancho)

RETURN m.exp

*************************************************
FUNCTION EVAL_FX
*************************************************
* Autor : Leo
* Dise�o: Leo
* 
* Fecha : 17/3/95
* 
* Par�metros: texto campo memo a evaluar
* 
PARAMETERS texto
PRIVATE resultado, lineas, linea, chk

chk		 = TYPE('LINE2LINE')
lineas = MEMLINES (texto)
_MLINE = 0
FOR i = 1 TO lineas
	linea = MLINE (texto, 1, _MLINE)
	IF chk # 'U'
		&linea
	ELSE
		resultado = EVAL (linea)
	ENDIF
ENDFOR

IF chk # 'U'
	RETURN 
ELSE
	RETURN resultado
ENDIF

*************************************************
FUNCTION CAPT_ERR
*************************************************
PARAMETERS ms , li
m.nerror  = ms
msg_error = 'Expresi�n Inv�lida: '+m.nerror+' en la linea N '+ALLT(STR(li,4))
RETURN
*************************************************
FUNCTION Mens_Avi
*************************************************
* Funcionamiento:
* Se le pasan como par�metros el nombre del sistema del programa
* que la llam�, y el c�d. del aviso. Seg�n la gravedad del aviso
* muestra un mensaje sencillo � con detalles.
* 
* Par�metros:
* 	m_sis  = nombre del sistema.
* 	m_avis = c�digo del aviso.
* 
PARAMETERS m_sis, m_avis
PRIVATE retorno, m_alias, m_reg_ant, m_mensa, w_descr, w_dsc, m.reto

PUSH KEY
=DEAC_TEC()

KEYBOARD '{shift+f12}' CLEAR
WAIT WIND ''

retorno   = .T.
m_alias   = ALIAS()
m_reg_ant = RECNO()
*!*	m_reg_ant = DBRECNO()
w_descr   = ''
w_dsc     = ''
m_sis     = PADR (m_sis, 8, ' ')

=uset('asa\mens_avi')
select mens_avi
IF SEEK(m_sis + m_avis)
	w_descr = del_chr( mens_avi.desc_avis )
	IF mens_avi.grav_avis < 5
		=MSG (&w_descr)		
	ELSE			
		**SENTENCIAS COMENTADAS POR VFP
*!*		m_mensa = ' Subsistema : '+ ALLTRIM(m_sis) + CHR(13) + CHR(13) + '  Aviso : '+ ;
*!*		 		ALLTRIM( mens_avi.cod_avis ) + CHR(13) + CHR(13) + &w_descr	
		** FIN DE SENTENCIAS COMENTADAS

		**SENTENCIAS AGREGADAS POR VFP
		IF AT('+',w_descr)> 0
			m_mensa = ' Subsistema : '+ ALLTRIM(m_sis) + CHR(13) + CHR(13) + '  Aviso : '+ ;
			 		  ALLTRIM( mens_avi.cod_avis ) + CHR(13) + CHR(13) + &w_descr	
		ELSE
			m_mensa = ' Subsistema : '+ ALLTRIM(m_sis) + CHR(13) + CHR(13) + '  Aviso : '+ ;
					  ALLTRIM( mens_avi.cod_avis ) + CHR(13) + CHR(13) + w_descr	
		ENDIF
		** FIN DE SENTENCIAS AGREGADAS POR VFP

		=Vent_Exp(m_mensa)
	ENDIF
	m.reto = .T.
	
ELSE
	=MSG ('No se encuentra Mensaje de Aviso ' + CHR(13) + m_avis + '.' +;
			  CHR(13) + 'Informe a SICO.')
	m.reto = .F.
ENDIF

=msale()
	
KEYBOARD '{shift+f12}' CLEAR
WAIT WIND ''
POP KEY
	
RETURN m.reto
*


*************************************************
FUNCTION msale
*************************************************
* Funcionamiento:
* Cierra las tablas que se abrieron dentro de la funci�n y 
* restaura, si hay, la tabla en uso antes del llamado.
* 
* 
* Par�metros:
* 
*	no tiene. 
* 
	USE IN mens_avi
	IF !EMPTY(m_alias)
		* SELECT &m_alias	### VFP
		SELECT (m_alias)
		=tab_vac(m_reg_ant)
	ENDIF


*************************************************
FUNCTION Vent_Exp
*************************************************
* Funcionamiento:
* Se llama para los c�d. de aviso mayores que 5. Si elige opci�n
* Explicaci�n se mostrar� una explicaci�n del aviso. Si elige la 
* opcion Indicaciones se mostrar� la/s posibles soluciones.
* 
* Par�metros:
* 
* 
* 
PARAMETERS m.mensaje, nowait

	PRIVATE m.mensaje, largomen, m.pos, m.pos1, largomemo, m.altura
	IF TYPE('m.mensaje') <> "C"
		m.mensaje = "Presione ENTER para continuar"
	ENDIF

	IF TYPE ('nowait') <> 'L'
		nowait = .T.
	ENDIF	

	#IFNDEF FORM_MSGBXX

	largomemo = SET("MEMOWIDTH")
	m.altura = 1
	m.mensaje = ALLTRIM(m.mensaje)
	largomen = LEN(m.mensaje)
	IF largomen < 25
		largomen = 30
	ELSE
		IF largomen > 40
			SET MEMOWIDTH TO 40
			m.altura = MEMLINES(m.mensaje)
			largomen = 46
		ELSE
			largomen = largomen+6
		ENDIF
	ENDIF

	largomen = largomen*1.2		&& ### VFP

	* DEFINE WINDOW M_VENTMEN ;
	* 	FROM	INT ( (SROW()-(6+m.altura)) / 2 ),;
	* 				INT ( (SCOL()-largomen) / 2 );
	* 	TO	INT ( (SROW()-(6+m.altura)) / 2 ) + (5+m.altura),;
	* 			INT ( (SCOL()-largomen) / 2 ) +largomen-1+3 	### VFP

	DEFINE WINDOW M_VENTMEN ;
		FROM	INT ( (SROW()-(6+m.altura)) / 2 ),;
					INT ( (SCOL()-largomen) / 2 );
		TO INT ( (SROW()*1.1-(6+m.altura)) / 2 ) + (5+m.altura),;
				INT ( (SCOL()-largomen) / 2 ) +largomen-1+3 ;
		FLOAT ;
		NOCLOSE ;
		SHADOW ;
		DOUBLE ;
		COLOR SCHEME 7 ;
		FONT "Courier New",8

	ACTIVATE WINDOW M_VENTMEN NOSHOW

	FOR i_msg=1 TO m.altura
		* m.pos = (largomen - 2 - LEN (MLINE (m.mensaje, i_msg)) ) / 2	&& ### VFP
		m.pos = WCOLS()/2-LEN(MLINE (m.mensaje, i_msg))/2
		@ i_msg, m.pos SAY MLINE (m.mensaje, i_msg)
	ENDFOR 

*	m.pos1 = (largomen - 2 - 43) / 2
	IF ! nowait
		DO CASE	
			CASE !EMPTY(ALLTRIM (mens_avi.expl_avis)) AND !EMPTY(ALLTRIM (mens_avi.sol_avis))
				* m.pos1 = 1	###> VFP
				m.pos1 = WCOLS() / 2 - LEN("Acepta  Explicaci�n  Indicaciones") / 2
				@ m.altura+2, m.pos1 GET m.confirm ;
					PICTURE "@*HN \!\?\<Acepta; \<Explicaci�n ; \<Indicaciones " ;
					SIZE 1, 12, 1 ;
					DEFAULT 1 ;
					VALID msg_conf_valid() ;
					MESSAGE "Acepta  Continuar, Explicaci�n  M�s informaci�n, Indicaciones  Soluciones"

			CASE EMPTY(ALLTRIM (mens_avi.expl_avis)) AND !EMPTY(ALLTRIM (mens_avi.sol_avis))
				* m.pos1 = 9	###> VFP
				m.pos1 = WCOLS() / 2 - LEN("Acepta  Indicaciones") / 2
				@ m.altura+2, m.pos1 GET m.confirm ;
					PICTURE "@*HN \!\?\<Acepta; \<Indicaciones " ;
					SIZE 1, 12, 1 ;
					DEFAULT 1 ;
					VALID msg_conf_valid() ;
					MESSAGE "Acepta  Continuar, Indicaciones  Soluciones"

			CASE !EMPTY(ALLTRIM (mens_avi.expl_avis)) AND EMPTY(ALLTRIM (mens_avi.sol_avis))
				* m.pos1 = 9	###> VFP
				m.pos1 = WCOLS() / 2 - LEN("Acepta  Explicaci�n") / 2
				@ m.altura+2, m.pos1 GET m.confirm ;
					PICTURE "@*HN \!\?\<Acepta; \<Explicaci�n " ;
					SIZE 1, 12, 1 ;
					DEFAULT 1 ;
					VALID msg_conf_valid() ;
					MESSAGE "Acepta  Continuar, Explicaci�n  M�s informaci�n"

			CASE EMPTY(ALLTRIM (mens_avi.expl_avis)) AND EMPTY(ALLTRIM (mens_avi.sol_avis))
				* m.pos1 = (largomen - 7) / 2	###> VFP
				m.pos1 = WCOLS() / 2 - LEN("Acepta") / 2
				@ m.altura+2, m.pos1 GET m.confirm ;
					PICTURE "@*HN \!\?\<Acepta" ;
					SIZE 1, 8, 1 ;
					DEFAULT 1 ;
					VALID msg_conf_valid() ;
					MESSAGE "Acepta  Continuar"

		ENDCASE
		

		ACTIVATE WINDOW M_VENTMEN
*		READ CYCLE MODAL TIMEOUT 15
*		READ CYCLE 
*   comente linea anterior por MODAL
		READ CYCLE MODAL
		
		RELEASE WINDOW M_VENTMEN

	ENDIF


	SET MEMOWIDTH TO largomemo
	
	IF EMPTY ( WONTOP())
		@ 24,0 CLEAR
	ENDIF

	KEYBOARD '{shift+f12}' CLEAR
	WAIT WIND ''
	
	#ELSE
	
		m.confirm = 1
		IF ! nowait
			DO CASE	
			CASE !EMPTY(ALLTRIM (mens_avi.expl_avis)) AND !EMPTY(ALLTRIM (mens_avi.sol_avis))
				DO FORM MSGBOX WITH m.mensaje, "\<Acepta; \<Explicaci�n ; \<Indicaciones"
			CASE EMPTY(ALLTRIM (mens_avi.expl_avis)) AND !EMPTY(ALLTRIM (mens_avi.sol_avis))
				DO FORM MSGBOX WITH m.mensaje, "\<Acepta; \<Indicaciones"
			CASE !EMPTY(ALLTRIM (mens_avi.expl_avis)) AND EMPTY(ALLTRIM (mens_avi.sol_avis))
				DO FORM MSGBOX WITH m.mensaje, "\<Acepta; \<Explicaci�n "
			CASE EMPTY(ALLTRIM (mens_avi.expl_avis)) AND EMPTY(ALLTRIM (mens_avi.sol_avis))
				DO FORM MSGBOX WITH m.mensaje, "\<Acepta"
			ENDCASE
		ENDIF
	
	#ENDIF
	

*********************************************
FUNCTION msg_conf_valid   &&  m.confirm VALID
************************
	DO CASE
		CASE m.confirm = 1
			#IFNDEF FORM_MSGBXX
				CLEAR READ
			#ELSE
				* _Screen.ActiveForm.Release
				* FormActivo.Release		&&// Privada del click del form
			#ENDIF
			
		CASE m.confirm = 2
			IF EMPTY(ALLTRIM (mens_avi.expl_avis))
				IF !EMPTY(ALLTRIM (mens_avi.sol_avis))
					=MSG (ALLTRIM (mens_avi.sol_avis))
				ENDIF
			ELSE	
				=MSG (ALLTRIM (mens_avi.expl_avis))
			ENDIF	
			RETURN .F.
		CASE m.confirm = 3
			IF !EMPTY(ALLTRIM (mens_avi.sol_avis))
				=MSG (ALLTRIM (mens_avi.sol_avis))
			ENDIF	
			RETURN .F.
	ENDCASE
*

*************************************************
FUNCTION HAY_LOC
*************************************************
* Funcionamiento:
* 
* 
* Par�metros:	TABLA	 	(C)	: Tabla (con sistema) contra la cual 
* 							se valida.
* 				EXPR_FOR	(C)	: Expresi�n a buscar con LOCATE FOR
* 				EXPR_WHI	(C)	: Expresi�n a buscar con LOCATE WHILE
* 
* Modif : Gabriel 02-01-95
*
PARAMETERS tabla, expr_for, expr_whi
PRIVATE gs_curr_dbf, gs_puntero, retorno, usada

retorno = .T.
usada	= .T.

IF !BETWEEN(PARAMETERS(),2,3)
	WAIT WINDOW "Faltan parametros para la Funci�n HAY_LOC ()"
	retorno = .F.
ENDIF

IF RAT('\',tabla) # 0
	IF !USED(tabla)
		=USET(tabla)
		usada = .F.
	ENDIF
	tabla   = SUBSTR (tabla, RAT('\',tabla) + 1)
ELSE
	IF !USED(tabla)
		=MSG('La tabla NO esta abierta. Debe pasar el par�metro con SISTEMA')
		retorno = .F.
	ENDIF
ENDIF

IF retorno
	gs_curr_dbf = SELECT ()
	gs_puntero	= RECNO ()
*!*		gs_puntero	= DBRECNO ()
	
	SELECT (tabla)
	
*!*		IF !usada AND TABLA_SQL (m.tabla)	&& ### XXX
*!*			DBSEEK ()
*!*		ENDIF
	
	IF EMPTY (expr_whi)
		LOCATE FOR &expr_for
	ELSE	
		LOCATE FOR &expr_for WHILE &expr_whi
	ENDIF
	IF FOUND ()
		retorno = .T.
	ELSE
		retorno = .F.
	ENDIF
	IF !usada
		USE 
	ENDIF
	
	SELECT (gs_curr_dbf)	
	=ir_reg (gs_puntero)
ENDIF

RETURN retorno
**************************************************
FUNCTION JUST_CHR
*************************************************
* Autor : RODRIGO
* Dise�o: RODRIGO
PARAMETERS cadena, caract
PRIVATE retorno, occ, dif, i, cad_res, occ2, bk_agreg
retorno = .T.
cadena  = TRIM (cadena)
occ 		= OCCURS(' ',cadena)
occ2		= occ
dif 		= caract - LEN(cadena)
cad_res	=	cadena
bk_agreg= 0
IF occ > 0 AND dif >= 0
	FOR I = 1 to occ
		blanco 	= AT(' ',cadena,i)
		cant_bk	= CEILING(dif/occ2)
		cad_res	= STUFF(cad_res,blanco+bk_agreg,0,SPAC(cant_bk))
		bk_agreg= bk_agreg + cant_bk 
		dif			= dif - cant_bk
		occ2		=	occ2 - 1 
	ENDFOR
ENDIF
RETURN cad_res
*
*************************************************
FUNCTION FABRTAB
*************************************************
* Autor :	RODRIGO
* Dise�o: PACHU
PARAMETERS direct, tabla, indice, alias

*=VerNuPar ('FABRTAB', PARAMETERS()	, 2 )
 =VerTiPar ('FABRTAB', 'DIRECT'			,'C')
 =VerTiPar ('FABRTAB', 'TABLA'			,'C')
*=VerTiPar ('FABRTAB', 'INDICE'			,'C')
*=VerTiPar ('FABRTAB', 'ALIAS'			,'C')

PRIVATE retorno

IF !EMPTY(direct)
	IF RIGHT (direct,1) # '\'
		direct = direct + '\'
	ENDIF
ELSE
	direct = _SISTEMA + '\'	
ENDIF

retorno = USET(direct+tabla, indice, alias)

RETURN retorno
*
*************************************************
FUNCTION ir_reg
*************************************************
* Funcionamiento
*  Devuelve .F. si no pudo posicionar el nro. de registro indicado.
*	 Devuelve .T. si lo pudo posicionar . En este caso si se le pasa
*  un n�m de reg v�lido deja el puntero en este reg, si en cambio no
*	 se pasa un reg o el reg pasado es inv�lido el puntero queda en el 
*	 reg en que se encontraba.
*
* Par�metros
*	[reg]         : (N) (ninguno, queda en el actual) n�mero de registro  
*						      		en que se quiere posicionar el puntero.
* [tab_a_verif] : (C) (tabla activa) Nombre de la tabla de la que se quiere 
* 										saber si est� vac�a.
*************************************************

	PARAMETERS reg, tab_a_verif
	PRIVATE ant_reg

	IF TYPE('tab_a_verif') <> 'C'
		tab_a_verif = ALIAS()
	ENDIF

	* ### C/S
	* IF _SQL_FLAG AND CURSORGETPROP ("SourceType", m.tab_a_verif)==2
*!*		IF TABLA_SQL (m.tab_a_verif)
*!*			RETURN (SQL_IR_REG (m.reg, m.tab_a_verif))
*!*		ENDIF

	IF USED(tab_a_verif)
		IF TYPE('reg') <> 'N'	
			reg = RECNO(tab_a_verif)
		ENDIF		
		IF EOF(tab_a_verif)
			ant_reg = 0
		ELSE
			ant_reg = RECNO(tab_a_verif)
		ENDIF		
		IF RECCOUNT(tab_a_verif) <> 0
*!*			IF DBRECCOUNT(tab_a_verif) <> 0
			GO TOP IN (tab_a_verif)
			IF EOF(tab_a_verif)
				RETURN .F.
			ELSE
				IF !(reg > RECCOUNT(tab_a_verif) OR reg <= 0)
*!*					IF !(reg > DBRECCOUNT(tab_a_verif) OR reg <= 0)
					GO reg IN (tab_a_verif)
					IF DELETED(tab_a_verif)
						IF ant_reg <> 0
							GO ant_reg IN (tab_a_verif)
						ELSE
							GO BOTT IN (tab_a_verif)
						ENDIF
						RETURN .F.
					ELSE	
						RETURN .T.
					ENDIF
				ELSE
					IF ant_reg <> 0
						GO ant_reg IN (tab_a_verif)
					ELSE
						GO BOTT IN (tab_a_verif)
					ENDIF
					RETURN .F.
				ENDIF
			ENDIF
		ELSE
			RETURN .F.
		ENDIF
	ELSE
		WAIT WINDOW NOWAIT 'Alias '+tab_a_verif+ ', no existe'
		RETURN .F.
	ENDIF	
	*************************************************
FUNCTION DELREC
*************************************************
* Funcionamiento:
* Borra el registro corriente del area actual
* poniendo en el primer campo del indice un
* 0, "", o fecha_nula segun el tipo del campo
* para que no se encuentre con KEYMATCH

	PRIVATE u_campo
	DELETE
*	u_campo = strword (SYS (14, 1), 1)

*	DO CASE
*	CASE TYPE (u_campo) = 'C'
*		REPLACE &u_campo WITH ""
*	CASE TYPE (u_campo) = 'N'
*		REPLACE &u_campo WITH 0
*	CASE TYPE (u_campo) = 'D'
*		REPLACE &u_campo WITH CTOD ('  /  /  ')
*	ENDCASE
*
*       �����������������������������������������������������������
*       �                                                         �
*       � 11/01/1999           IMPRIME3.SPR              10:20:17 �
*       �                                                         �
*       ����������������������������������������������������������
*       �                                                         �
*       � Author's Name                                           �
*       �                                                         �
*       � Copyright (c) 1999 Company Name                         �
*       � Address                                                 �
*       � City,     Zip                                           �
*       �                                                         �
*       � Description:                                            �
*       � This program was automatically generated by GENSCRN.    �
*       �                                                         �
*       O����������������������������������������������������������


*       �����������������������������������������������������������
*       �                                                         �
*       �         IMPRIME3/MS-DOS Setup Code - SECTION 1          �
*       �                                                         �
*       O����������������������������������������������������������
*


FUNCTION imprime

PARAMETERS p_reporte, p_expr_repo, p_tip_imp

#REGION 1


* Guarda �rea en la que se realiza el reporte
PRIVATE area_repo, m.reporte, seleccmul

area_repo = SELECT ()
* Asignar Nombre completo del reporte
p_reporte = substr (p_reporte, rat ('\', p_reporte) + 1)
IF '.' $ p_reporte
	p_reporte = substr (p_reporte, 1 , at ('.', p_reporte) -1)
ENDIF
m.reporte = UPPER (ALLT (p_reporte))
repo_arch = ALLT (m.sistema) + '\' + ALLT (m.modulo) + '\' + ;
m.reporte + '.FRX '

*Chequeo de par�metros
IF TYPE ('p_tip_imp') <> 'N' OR ! INLIST (p_tip_imp,0,1,2,3)
	p_tip_imp = 3
ENDIF
IF TYPE ('p_expr_repo') = 'L'
	p_expr_repo = ' '
ELSE
	p_expr_repo = p_expr_repo + ' '
ENDIF

* Verificaci�n de muestra de ventana o no
* La primera vez que se lanza un reporte desde un programa
* deber�a entrar siempre con _SI_V_IMP = 'S'

IF TYPE ('_SI_V_IMP') <> 'C' OR ! INLIST (_SI_V_IMP,'N','S')
	_SI_V_IMP = 'S'
ENDIF

* Modif por RODRIGO 28/06/95
IF (p_tip_imp = 0) OR (p_tip_imp = 1)
	IF TYPE ('_SI_V_IMP') <> 'C' OR ! INLIST (_SI_V_IMP,'N','S')
		_SI_V_IMP = 'N'
	ENDIF
ENDIF

* Inicializaciones de variables (s�lo defensivamente)
m.previmp    	= 'S'
m.curopc     	= 1
m.capimp     	= ''
m.finimp     	= ''
m.nomdrv     	= ''
m.copias     	= 1
m.nomarch    	= ''
m.si_v_imp   	= 'N'
m.tit_v_repo 	= ''
m.tipoimp			=	''
m.nomdrvlog 	= ''
* m.esc_ini   = ''
* m.esc_fin   = ''
* m.secuencia = ''
m.sel_tip	  = 1
seleccmul     = 'N'

m.sel_prn	  = 1		&& ### VFP
m.sel_prn_nom = ""
m.acepta      = 1

* Inicializaci�n de la Matriz de Tipos de Archivos
* La matriz tiene el siguiente formato:
*               Descripci�n del Tipo
*       Extensi�n del archivo
*               Sentencia del "COPY TO", Ej. 'DELIMITED WITH TAB' o Sentencia o funci�n para los Tipos definidos por el usuario

*               Tipo de formato (Sentencia SQL o Funci�n)

DIMENSION formatos [14,4]

formatos [1,1] = 'Reporte a Archivo'
formatos [2,1] = 'Base de Datos (DBF)'
formatos [3,1] = 'Delimitado por Tabs'
formatos [4,1] = 'Delimitado por Espacios'
formatos [5,1] = 'Delimitado por Comas'
formatos [6,1] = 'System Data Format (SDF)'
formatos [7,1] = 'Symbolic Link Format (SYLK)'
formatos [8,1] = 'Data Interchange Format (DIF)'
formatos [9,1] = 'Microsoft Excel (XLS)'
formatos [10,1] = 'Microsoft Multiplan (4.01)(MOD)'
formatos [11,1] = 'Symphony (1.0) (WRK)'
formatos [12,1] = 'Symphony (1.1/1.2)(WR1)'
formatos [13,1] = 'Lotus 1-2-3 (1A)(WKS)'
formatos [14,1] = 'Lotus 1-2-3 (2.x)(WK1)'

*Extensiones de los archivos
formatos [1,2] = '.PRN'
formatos [2,2] = '.DBF'
formatos [3,2] = '.ASC'
formatos [4,2] = '.ASC'
formatos [5,2] = '.ASC'
formatos [6,2] = '.SDF'
formatos [7,2] = '.ASC'
formatos [8,2] = '.DIF'
formatos [9,2] = '.XLS'
formatos [10,2] = '.MOD'
formatos [11,2] = '.WRK'
formatos [12,2] = '.WR1'
formatos [13,2] = '.WKS'
formatos [14,2] = '.WK1'

*Cl�usula COPY TO de los archivos
formatos [1,3] = ''
formatos [2,3] = ''
formatos [3,3] = 'DELIMITED WITH TAB'
formatos [4,3] = 'DELIMITED WITH ","'
formatos [5,3] = 'DELIMITED WITH BLANK'
formatos [6,3] = 'SDF'
formatos [7,3] = 'SYLK'
formatos [8,3] = 'DIF'
formatos [9,3] = 'XLS'
formatos [10,3] = 'MOD'
formatos [11,3] = 'WRK'
formatos [12,3] = 'WR1'
formatos [13,3] = 'WKS'
formatos [14,3] = 'WK1'

PUSH KEY
= deac_tec()
ON KEY LABEL F3 DO valcpos WITH VARREAD ()

*********************************************************
* Apertura de Tablas e �ndices e inicializaci�n de      *
* variables de campos.                                  *
* Si no se indica indice en USET se utiliza el primario *
*********************************************************
PRIVATE cl_rpts, cl_rpt_form, cl_impre, cl_drivers, cl_repwks, cl_prog
STORE .T. TO cl_rpts, cl_rpt_form, cl_impre, cl_drivers, cl_repwks, cl_prog
IF USED ('RPTS')
	cl_rpts = .F.
	SET ORDER TO 1 IN RPTS
ELSE
	= USET ('ASA\RPTS')
ENDIF
IF USED ('RPT_FORM')
        cl_rpts = .F.
        SET ORDER TO 1 IN RPT_FORM
ELSE
        = USET ('ASA\RPT_FORM')
ENDIF
IF USED ('IMPRE')
	cl_impre = .F.
	SET ORDER TO 1 IN IMPRE
ELSE
	= USET ('ASA\IMPRE')
ENDIF
IF USED ('DRIVERS')
	cl_drivers = .F.
	SET ORDER TO 1 IN DRIVERS
ELSE
	= USET ('ASA\DRIVERS')
ENDIF
IF USED ('REPWKS')
	cl_repwks = .F.
	SET ORDER TO 1 IN REPWKS
ELSE
	= USET ('ASA\REPWKS')
ENDIF
IF USED ('PROG')
	cl_prog = .F.
	SET ORDER TO 1 IN PROG
ELSE
	= USET ('ASA\PROG')
ENDIF

* Creaci�n de tabla temporal para lista de opciones
PRIVATE gs_tmp_dbf
gs_tmp_dbf = '_' + RIGHT (SYS(2015),7)
SELECT 0
* CREATE TABLE (gs_tmp_dbf) (NOMIMP C(15), DSCIMP C(30), TIPOIMP C(10) )
CREATE TABLE (gs_tmp_dbf) FREE (NOMIMP C(15), DSCIMP C(30), TIPOIMP C(10) )
INDEX ON NOMIMP TAG (gs_tmp_dbf)

* Variables Privadas
PRIVATE retorno, repo_arch, m.actlongpag, ant_area
retorno = .T.

* Expresiones de b�squeda para tablas
PRIVATE expr_drivers, expr_rpts, expr_repwks, expr_impre, expr_prog, expr_rpt_form
expr_rpts    = GETKEY ('RPTS', .T., 4)
expr_repwks  = GETKEY ('REPWKS', .T., 6)
expr_impre   = GETKEY ('IMPRE', .T., 1)
expr_drivers = GETKEY ('DRIVERS', .T., 2)
expr_prog    = GETKEY ('PROG', .T., 3)
expr_rpt_form= GETKEY ('RPT_FORM', .T., 4)

IF SEEK (EVAL(expr_rpts), 'RPTS')
	* Carga tabla temporal con opciones
	m.nomdrvlog = RPTS.nomdrvlog
*	IF _SI_V_IMP = 'S'
*		m.si_v_imp  = RPTS.salevent && <nueva>
*	ELSE
*		m.si_v_imp = 'N'
*	ENDIF
	GO TOP IN DRIVERS
	DO WHILE !EOF ('DRIVERS')
		IF ( DRIVERS.nomdrvlog = m.nomdrvlog )
			GO TOP IN IMPRE	
			DO WHILE !EOF ('IMPRE')
				IF (IMPRE.tipoimp = DRIVERS.tipoimp)
					SELE (gs_tmp_dbf)
					APPE BLANK
					REPLACE nomimp  WITH IMPRE.nomimp ,;
									tipoimp WITH LEFT (IMPRE.tipoimp,10) ,;
									dscimp  WITH LEFT (IMPRE.dscimp,30)
				ENDIF						
				SKIP IN IMPRE
			ENDDO
		ENDIF
		SKIP IN DRIVERS
	ENDDO
	
	* Abrir reporte y tomar m.longpag
	*ant_area = SELECT ()
	*SELECT 0
	*USE (repo_arch)	
	*LOCATE FOR objcode = 53
	*IF FOUND ()
	*	m.actlongpag = height
	*ELSE
	*	m.actlongpag = 1
	*ENDIF
	*USE
	*SELECT (ant_area)
	* Toma siempre de la tabla rpts.longpag, Gabriel 211095
	
	m.server  = _server
	m.wrkstat = _wkstation
	
	IF TYPE('_nomrep') # 'C'
		_nomrep = RPTS.reporte
	ENDIF
	
	IF TYPE('_nomimp') = 'C' AND ALLT(RPTS.reporte) == ALLT(_nomrep)
		m.nomimp = _nomimp
	ELSE
		*
		* Este seek no funcionaba porque las variables reporte, server y
		* workstat no ten�an la longitud correcta.
		*
		* IF SEEK (EVAL(expr_repwks), 'REPWKS')

		IF SEEK (m.sistema + m.modulo + m.programa + PadR (m.reporte, 8) + PadR (m.server, 10) + PadR (m.wrkstat, 10), 'REPWKS')

			m.nomimp = REPWKS.nomimp
		ELSE
			m.nomimp = RPTS.nomimp
		ENDIF
	ENDIF
	
	IF (TYPE ('_finimp') = 'C') AND ;
	(TYPE ('_capimp') = 'C') AND ;
	(TYPE ('_esc_ini') = 'C') AND ;
	(TYPE ('_esc_fin') = 'C') AND ;
	(TYPE ('_secuencia') = 'C') AND ALLT(RPTS.reporte) == ALLT(_nomrep)
		m.capimp     = _capimp
		m.finimp     = _finimp
		m.esc_ini    = _esc_ini
		m.esc_fin    = _esc_fin
		m.secuencia  = _secuencia
	ELSE
		IF SEEK (EVAL(expr_impre), 'IMPRE')
			m.capimp = DEL_CHR (IMPRE.capimp)
			IF TYPE ('IMPRE.esc_ini')   <> 'U' AND ;
		  	 TYPE ('IMPRE.esc_fin')   <> 'U' AND ;
			   TYPE ('IMPRE.secuencia') <> 'U'
				m.esc_ini   = ALLT (IMPRE.esc_ini)
				m.esc_fin   = ALLT (IMPRE.esc_fin)
				m.secuencia = ALLT (IMPRE.secuencia)
			ELSE	
				m.esc_ini = ''
				m.esc_fin = ''
				m.secuencia = ''
			ENDIF			
			* verificar que exista cpo finimp en tabla IMPRE
			IF TYPE ('IMPRE.finimp') <> 'U'
				m.finimp = DEL_CHR (IMPRE.finimp)
				IF EMPTY (m.finimp) AND ! EMPTY (m.capimp)
					m.finimp = 'ENDCAP'
				ENDIF
			ELSE
				m.finimp = ''
			ENDIF
			m.tipoimp = IMPRE.tipoimp
		ELSE
			m.nomimp    = SPACE (LEN (m.nomimp))
			m.tipoimp   = SPACE (LEN (IMPRE.tipoimp))
			m.capimp    = SPACE (LEN (IMPRE.capimp))
			m.finimp    = ''
			m.esc_ini   = ''
			m.esc_fin   = ''
			m.secuencia = ''
		ENDIF
	ENDIF
	
	IF TYPE('_nomdrv') = 'C' AND ALLT(RPTS.reporte) == ALLT(_nomrep)
		m.nomdrv = _nomdrv
	ELSE
		IF SEEK (EVAL(expr_drivers), 'DRIVERS')
			m.nomdrv = DRIVERS.nomdrv
		ELSE
			m.nomdrv = SPACE (LEN (DRIVERS.nomdrv))
		ENDIF
	ENDIF	
	
	SELECT RPTS
	m.nomarch= rpts.nomarch
	m.grabopc = 'N'
	
	IF RPTS.lpagfija = 'S'
		m.lpagfija = 'Fija'
	ELSE
		m.lpagfija = ''
	ENDIF

	IF TYPE('_copias') = 'N' AND ALLT(RPTS.reporte) == ALLT(_nomrep)
		m.copias = _copias
	ELSE
		m.copias = rpts.copias
		IF EMPTY (m.copias) OR m.copias < 1
			m.copias = 1
		ELSE
			m.copias = INT (m.copias)
		ENDIF
	ENDIF
	
	IF TYPE('_curopc') = 'N' AND ALLT(RPTS.reporte) == ALLT(_nomrep)
		m.curopc 	 = _curopc
	ELSE
    m.curopc = rpts.curopc
    IF ! INLIST (m.curopc, 'I', 'A', 'P','B')
      m.curopc = 1
		ELSE
			DO CASE
			CASE m.curopc = 'I'
				m.curopc = 1
			CASE m.curopc = 'P'
        m.curopc = 2
      CASE m.curopc = 'B'
        m.curopc = 3
      CASE m.curopc = 'A'
        m.curopc = 4
      ENDCASE
      IF m.curopc = 4
        = VALCPOS ('NOMARCH', .T.)
			ENDIF
		ENDIF
  ENDIF

  IF TYPE('_longpag') = 'N' AND ALLT(RPTS.reporte) == ALLT(_nomrep)
		m.longpag = _longpag
	ELSE
		m.longpag= rpts.longpag
		*IF m.longpag < 1
		*	m.longpag = INT (m.actlongpag)
		*ELSE
		*	m.longpag = INT (m.longpag)
		*ENDIF
		* Toma siempre de rpts, en actlongpag no hay nada ahora, Gabriel 211095
	ENDIF
	
	* Se agrega para guardar el valor de m.longpag, sera comparado despues de
	* la ventana, Gabriel 211095
	PRIVATE m.longant
	m.longant = m.longpag
	
		
	IF TYPE('_previmp') = 'C' AND ALLT(RPTS.reporte) == ALLT(_nomrep)
		m.previmp = _previmp
	ELSE
		m.previmp= rpts.previmp
		IF ! INLIST (m.previmp, 'S', 'N')
			m.previmp = 'N'
		ENDIF	
	ENDIF

	IF ALLT(RPTS.reporte) == ALLT(_nomrep)
		m.si_v_imp = _SI_V_IMP
	ELSE
		_SI_V_IMP	 = 'S'
		m.si_v_imp = 'N'
	ENDIF

  *[]
  * Busco si est� definido del Nombre de Reporte, y busco Tipo segun la extension
  IF !EMPTY (m.nomarch) AND ('.' $ m.nomarch)
    m.posic = ASCAN (formatos, ALLTRIM(SUBSTR (m.nomarch, (AT('.', m.nomarch)))))
    IF m.posic != 0
      m.sel_tip = ASUBSCRIPT (formatos, m.posic,1)
      SHOW GET m.sel_tip
    ENDIF
  ENDIF
  *[]

ELSE
	retorno = .F.
	= MSG_BACK ('', 'El reporte ' + ALLT (m.reporte) + ;
	' no est� en la tabla de reportes. Agreguelo y reintente por favor ...', .T.)
ENDIF

IF retorno
	IF EMPTY (RPTS.dscrepo)
		m.tit_v_repo = ' IMPRESION '
	ELSE	
		m.tit_v_repo = ' ' + ALLTRIM (RPTS.dscrepo) + ' '
	ENDIF
ENDIF

* Modif por Rodrigo el 29/06/95	 (Se agrego esta condicion)
IF RPTS.salevent = 'S' AND _SI_V_IMP = 'S' AND retorno
	IF SEEK (EVAL(expr_prog), 'PROG') AND (! EMPTY (PROG.descr_prog))
		m.nomprog = ALLTRIM (PROG.descr_prog)
	ELSE
		m.nomprog = ALLTRIM (m.programa)
	ENDIF
	
	* Defensivo
	IF TYPE ('m.SI_V_IMP') <> 'C' OR ! INLIST (m.SI_V_IMP,'N','S')
		m.SI_V_IMP = 'N'
	ENDIF
	* m.SI_V_IMP = 'N'
	* se comento por gabriel 24/06/94. si se cambia la 1era vez que quede.
	
* Leo la tabla rpt_form buscando formatos definidos por el usuario,
* y los agrego al final del vector del popup de tipos de archivo

  PRIVATE m.old_alias, m.ind
  m.old_alias = ALIAS()
  SELECT RPT_FORM
  SET KEY TO &expr_rpt_form
  GO TOP
  m.ind = 14
  DO WHILE !EOF()
    * Si no est� definido el formato, no lo agrego a la lista
    IF !EMPTY(RPT_FORM.def_format)
      m.ind = m.ind+1
      DIMENSION formatos [m.ind,4]
      formatos[m.ind,1]=RPT_FORM.nom_format
      formatos[m.ind,2]='.txt'
      formatos[m.ind,3]=RPT_FORM.def_format
      formatos[m.ind,4]=RPT_FORM.tipo_forma
    ENDIF
    SKIP
  ENDDO
  SELECT (old_alias)

	* Aqu� sigue la definici�n del Generador de Fox para ventanas y READ

#IFNDEF FORM_IMPRIME

#REGION 0
REGIONAL m.currarea, m.talkstat, m.compstat

IF SET("TALK") = "ON"
	SET TALK OFF
	m.talkstat = "ON"
ELSE
	m.talkstat = "OFF"
ENDIF
m.compstat = SET("COMPATIBLE")
SET COMPATIBLE FOXPLUS

*       �����������������������������������������������������������
*       �                                                         �
*       �                MS-DOS Window definitions                �
*       �                                                         �
*       O����������������������������������������������������������
*

IF NOT WEXIST("_s1k0m5o6w")
	DEFINE WINDOW _s1k0m5o6w ;
		FROM INT((SROW()-16)/2),INT((SCOL()-65)/2) ;
		TO INT((SROW()-16)/2)+15,INT((SCOL()-65)/2)+64 ;
		TITLE m.tit_v_repo ;
		FLOAT ;
		NOCLOSE ;
		SHADOW ;
		NOMINIMIZE ;
		COLOR SCHEME 1
ENDIF


*       �����������������������������������������������������������
*       �                                                         �
*       �              IMPRIME3/MS-DOS Screen Layout              �
*       �                                                         �
*       O����������������������������������������������������������
*

#REGION 1
IF WVISIBLE("_s1k0m5o6w")
	ACTIVATE WINDOW _s1k0m5o6w SAME
ELSE
	ACTIVATE WINDOW _s1k0m5o6w NOSHOW
ENDIF
@ 2,1 SAY "Destino:" ;
	SIZE 1,8, 0
@ 1,0 TO 1,62
@ 5,26 SAY "Copias:" ;
	SIZE 1,7, 0
@ 5,40 SAY "Vista Preliminar:" ;
	SIZE 1,17, 0
@ 10,1 SAY "Longitud de P�gina          :" ;
	SIZE 1,29, 0
@ 11,1 SAY "Grabar Opciones             :" ;
	SIZE 1,29, 0
@ 12,1 SAY "Solicita por cada impresi�n :" ;
	SIZE 1,29, 0
@ 0,1 SAY "Programa :" ;
	SIZE 1,10, 0
@ 8,1 SAY "Tipo de Archivo:" ;
	SIZE 1,16, 0
@ 13,1 SAY "Selecciona registros        :" ;
	SIZE 1,29, 0
@ 0,13 SAY m.nomprog ;
	SIZE 1,30
@ 3,2 GET m.curopc ;
	PICTURE "@*RVN Impresora;Reporte a Pantalla;Visualizar Datos;Archivo" ;
	SIZE 1,22,0 ;
	DEFAULT 1 ;
	VALID VALCPOS ( VARREAD ())
@ 3,26 GET m.nomimp ;
	SIZE 1,15 ;
	DEFAULT " " ;
	PICTURE "@K" ;
	WHEN m.curopc = 1 ;
	VALID VALCPOS ( VARREAD ()) ;
	MESSAGE 'Nombre de una impresora del sistema'
@ 3,47 SAY m.tipoimp ;
	SIZE 1,15
@ 4,26 SAY m.nomdrv ;
	SIZE 1,20
@ 4,47 SAY m.nomdrvlog ;
	SIZE 1,15
@ 5,34 GET m.copias ;
	SIZE 1,2 ;
	DEFAULT 0 ;
	PICTURE "@KZ" ;
	WHEN W_CUROPC() ;
	VALID VALCPOS ( VARREAD ()) ;
	MESSAGE 'Cantidad de copias a imprimir'
@ 5,58 GET m.previmp ;
	SIZE 1,1 ;
	DEFAULT " " ;
	PICTURE "@M N,S" ;
	WHEN W_CUROPC() ;
	MESSAGE 'S: Vista preeliminar antes de Impresi�n  -  N: SIN vista preeliminar'
@ 6,26 GET m.nomarch ;
	SIZE 1,29 ;
	DEFAULT " " ;
	PICTURE "@!K" ;
	WHEN m.curopc = 4 ;
	VALID VALCPOS ( VARREAD ()) ;
	MESSAGE 'Nombre de archivo donde se direcciona la impresi�n'
@ 7,26 GET m.sel_tip ;
	PICTURE "@^" ;
	FROM formatos ;
	SIZE 3,33 ;
	RANGE 1 ;
	DEFAULT 1 ;
	VALID VALCPOS ( VARREAD ()) ;
	DISABLE ;
	COLOR SCHEME 1, 2
@ 10,32 GET m.longpag ;
	SIZE 1,3 ;
	DEFAULT 0 ;
	PICTURE "@KZ" ;
	WHEN EMPTY (M.LPAGFIJA) ;
	VALID VALCPOS ( VARREAD ()) ;
	MESSAGE 'Longitud de p�gina del reporte en l�neas'
@ 10,37 SAY m.lpagfija ;
	SIZE 1,10
@ 11,32 GET m.grabopc ;
	SIZE 1,1 ;
	DEFAULT " " ;
	PICTURE "@M N,S" ;
	MESSAGE 'S: Graba modificaciones en ventana de impresi�n  -  N: no graba modificaciones'
@ 12,32 GET m.si_v_imp ;
	SIZE 1,1 ;
	DEFAULT " " ;
	PICTURE "@M N,S" ;
	MESSAGE 'S: Muestra ventana para cada impresi�n del programa  -  N: No muestra ventana'
@ 13,32 GET m.seleccmul ;
	SIZE 1,1 ;
	DEFAULT " " ;
	PICTURE "@M N,S" ;
	WHEN rpts.previmp = 'S' ;
	MESSAGE 'S: Permite seleccionar registros a imprimir  -  N: no permite seleccionar registros'
@ 13,51 GET m.acepta ;
	PICTURE "@*HN \?Aceptar" ;
	SIZE 1,11,1 ;
	DEFAULT 1 ;
	WHEN W_ACEPTA() ;
	VALID VALCPOS ( VARREAD ()) ;
	MESSAGE 'Inicia impresi�n del reporte'

IF NOT WVISIBLE("_s1k0m5o6w")
	ACTIVATE WINDOW _s1k0m5o6w
ENDIF


*       �����������������������������������������������������������
*       �                                                         �
*       �    MS-DOSREAD contains clauses from SCREEN imprime3     �
*       �                                                         �
*       O����������������������������������������������������������
*

READ CYCLE MODAL ;
	WHEN _s1k0m6lyl() ;
	SHOW _s1k0m6lyo() ;
	TIMEOUT 600

RELEASE WINDOW _s1k0m5o6w

#REGION 0
IF m.talkstat = "ON"
	SET TALK ON
ENDIF
IF m.compstat = "ON"
	SET COMPATIBLE ON
ENDIF

#ELSE
	* Imprime4 actualiza retorno
	DO FORM IMPRIME4
	_sel_prn_nom = m.sel_prn_nom	&& ### VFP
	
	* DO FORM IMPRIME3
	* m.sel_prn_nom = BUSCA_LINEA (tipoimp.expr, "DEVICE=")
	* _sel_prn_nom = m.sel_prn_nom
#ENDIF

*       �����������������������������������������������������������
*       �                                                         �
*       �              IMPRIME3/MS-DOS Cleanup Code               �
*       �                                                         �
*       O����������������������������������������������������������
*

#REGION 1
ENDIF

* _sel_prn_nom = m.sel_prn_nom	&& ### VFP

=FIN_REPORT(retorno)

*************************************************


*       �����������������������������������������������������������
*       �                                                         �
*       �  IMPRIME3/MS-DOS Supporting Procedures and Functions    �
*       �                                                         �
*       O����������������������������������������������������������
*

#REGION 1
FUNCTION FIN_REPORT
*************************************************
PARAMETERS report_ok

POP KEY
* Borrado de tabla temporal
SELE (gs_tmp_dbf)
USE
IF FILE (gs_tmp_dbf+'.DBF')
	DELETE FILE (gs_tmp_dbf+'.DBF')
ENDIF
IF FILE (gs_tmp_dbf+'.CDX')
	DELETE FILE (gs_tmp_dbf+'.CDX')
ENDIF

IF report_ok
	* Grabar cambios en tabla RPTS
	IF RPTS.salevent = 'S' AND m.grabopc = 'S'
		SELECT RPTS
		DO CASE
		CASE m.curopc = 1
			REPLACE RPTS.curopc WITH 'I'
		CASE m.curopc = 2
			REPLACE RPTS.curopc WITH 'P'
		CASE m.curopc = 3
			REPLACE RPTS.curopc WITH 'B'
		CASE m.curopc = 4
			REPLACE RPTS.curopc WITH 'A'
		ENDCASE		
		GATHER MEMVAR FIELDS copias, longpag, nomarch, previmp
		IF SEEK (EVAL(expr_repwks), 'REPWKS')
			SELECT REPWKS
			GATHER MEMVAR FIELDS nomimp
		ELSE
			SELECT RPTS
			GATHER MEMVAR FIELDS nomimp
		ENDIF
	ENDIF

	* Impresi�n
	= Impr ()
ENDIF

* Cierre de Tablas
IF cl_rpts
	USE IN RPTS
ENDIF
IF cl_impre
	USE IN IMPRE
ENDIF
IF cl_drivers
	USE IN DRIVERS
ENDIF
IF cl_repwks
	USE IN REPWKS
ENDIF
IF cl_prog
	USE IN PROG
ENDIF


SELECT (area_repo)

* ### VFP
IF VARTYPE(m.repo_arch)=="C" AND !EMPTY(m.repo_arch) AND report_ok
	* WAIT WIND m.repo_arch
	PRIVATE errh, dio_err
	m.errh = ON("ERROR")
	ON ERROR dio_err = .T.
	* ##### OJO
	IF TYPE ("_RPT_DEBUG")=="L" AND _RPT_DEBUG
		COPY FILE (STRTRAN(m.repo_arch, "FRX", "FR*")) TO C:\TEMP\REPORTE.*
	ENDIF
	
	IF USED(m.repo_arch)
		USE IN (m.repo_arch) && ### XXX Alejandro - 16/01/2003
	ENDIF
	m.repo_temp = STRTRAN(m.repo_arch, "FRX", "FR*")	&& ### XXX Alejandro - 16/01/2003
	* DELETE FILE (STRTRAN(m.repo_arch, "FRX", "FR*"))	### XXX Alejandro - 16/01/2003
	DELETE FILE (m.repo_temp)	&& ### XXX Alejandro - 16/01/2003
	ON ERROR &errh
	* LOGFILE (m.repo_arch + ": " + BPROGRAM(2,5))	
ENDIF


RETURN report_ok


* Retorna el n�mero indicado en formato interleave para c�digo de barras
FUNCTION NUM_A_INTERL (m.num)

LOCAL m.pos, m.par, m.ret

IF INT(LEN(m.num)/2)*2 <> LEN(m.num)
	m.num = "0"+m.num
ENDIF

m.ret = ""

FOR m.pos = 1 TO LEN(m.num) STEP 2
	m.par = VAL(SUBSTR(m.num, m.pos, 2))
	
	DO CASE
	CASE m.par <= 91
		m.ret = m.ret+CHR(35+m.par)
	CASE m.par==92
		m.ret = m.ret+CHR(196)
	CASE m.par==93
		m.ret = m.ret+CHR(197)
	CASE m.par==94
		m.ret = m.ret+CHR(199)
	CASE m.par==95
		m.ret = m.ret+CHR(201)
	CASE m.par==96
		m.ret = m.ret+CHR(209)
	CASE m.par==97
		m.ret = m.ret+CHR(214)
	CASE m.par==98
		m.ret = m.ret+CHR(220)
	CASE m.par==99
		m.ret = m.ret+CHR(225)
	ENDCASE	
NEXT

RETURN "!"+m.ret+["]


* Retorna el sistema operativo host
FUNCTION OS_ID

LOCAL m.os, m.pos

m.os = STRTRAN(UPPER(OS())," ")
m.pos = AT(".", m.os)
IF m.pos > 0
	m.os = LEFT(m.os, m.pos-1)
ENDIF

RETURN (m.os)
	

#define OS_SIGN	"SIST_OP"


FUNCTION EXPR_OS_ID (m.expr)

LOCAL m.pos, m.expr_os

m.pos = AT(OS_SIGN, m.expr)
m.expr_os = ""
IF m.pos > 0
	m.expr_os = SUBSTR(m.expr, m.pos)
	m.pos = AT("=", m.expr_os)
	m.expr_os = STRTRAN(SUBSTR(m.expr_os, m.pos+1), " ")
ENDIF

RETURN (m.expr_os)



* Retorna el sistema operativo de un tipo de impresora
* Se agrega el tipo de impresora para soporte de m�ltiples S.O.
FUNCTION CHEQ_OS_ID (m.expr, m.tipoimp)

LOCAL m.retorno, m.expr_os

m.retorno = .T.

* Compatibilidad con el modo de ejecuci�n anterior
IF PCOUNT() = 1
	m.expr_os = EXPR_OS_ID(m.expr)
	IF !EMPTY(m.expr_os) AND m.expr_os <> OS_ID()
		=MSG ("El tipo de impresora seleccionado no corresponde al sistema operativo de su PC ("+OS()+")")
		retorno = .F.
	ENDIF
ELSE
	m.expr_os = OS_ID()
	IF !SEEK(m.tipoimp + m.expr_os, "DRIVERS", "IMP_OS")
		=MSG ("El tipo de impresora seleccionado no corresponde al sistema operativo de su PC ("+OS()+")")
		retorno = .F.
	ENDIF
ENDIF

RETURN (m.retorno)



****************************************
FUNCTION W_ACEPTA
****************************************
PRIVATE retorno
retorno = .T.
*Habilito o deshabilito selecci�n de formato
IF m.curopc = 4
        SHOW GET m.sel_tip ENABLE
ELSE
        SHOW GET m.sel_tip DISABLE
ENDIF

RETURN retorno


****************************************
FUNCTION valcpos
****************************************
PARAMETERS nomcpo, desdeafuera
PRIVATE pos_pto

IF m.curopc == 4
        SHOW GET m.sel_tip ENABLE
ELSE
        SHOW GET m.sel_tip DISABLE
ENDIF

DO CASE

CASE nomcpo = 'CUROPC'
	IF m.curopc == 4
		SHOW GET m.sel_tip ENABLE
	ELSE
		SHOW GET m.sel_tip DISABLE
	ENDIF
	
#IFDEF FORM_IMPRIME
	IF m.curopc == 1
		SHOW GET m.sel_prn ENABLE
	ELSE
		SHOW GET m.sel_prn DISABLE
	ENDIF
#ENDIF	

CASE nomcpo == 'SEL_TIP'
  IF EMPTY(m.nomarch)
    m.nomarch = m.reporte + '.TXT'
  ENDIF
  IF m.sel_tip >= 1 and m.sel_tip < 15
    *Saco la extension del archivo (si tiene)
    pos_pto=AT('.',m.nomarch)
    IF pos_pto != 0
      m.nomarch = SUBSTR(m.nomarch,1,pos_pto-1)
    ENDIF
    m.nomarch = PADR (m.nomarch+formatos[m.sel_tip,2], 50)
    SHOW GET m.nomarch
  ENDIF
CASE nomcpo == 'NOMIMP'
	IF IN_TABLA (gs_tmp_dbf,'Impresoras',' nomimp :h= "Nombre", tipoimp :h= "Tipo", dscimp :h= "Descripci�n" ')
		= SEEK (EVAL(expr_impre), 'IMPRE')
		m.capimp = DEL_CHR (IMPRE.capimp)
		IF TYPE ('IMPRE.finimp') <> 'U'
			m.finimp = DEL_CHR (IMPRE.finimp)
			IF EMPTY (m.finimp) AND ! EMPTY (m.capimp)
				m.finimp = 'ENDCAP'
			ENDIF
		ELSE
			m.finimp = ''
		ENDIF
		m.tipoimp = IMPRE.tipoimp
		IF TYPE ('IMPRE.esc_ini')<>'U' AND TYPE ('IMPRE.esc_fin')<>'U' AND TYPE ('IMPRE.secuencia')<>'U'
			m.esc_ini   = ALLT (IMPRE.esc_ini)
			m.esc_fin   = ALLT (IMPRE.esc_fin)
			m.secuencia = ALLT (IMPRE.secuencia)
		ELSE
			m.esc_ini   = ''
			m.esc_fin   = ''
			m.secuencia = ''
		ENDIF			
		IF SEEK (EVAL(expr_drivers), 'DRIVERS')
			m.nomdrv = DRIVERS.nomdrv
		ELSE
			= MSG ('No se encontr� Driver para la Impresora especificada')
			m.nomdrv = SPACE (LEN (DRIVERS.nomdrv))
		ENDIF
	ELSE
		m.nomimp    = SPACE (LEN (m.nomimp))
		m.tipoimp   = SPACE (LEN (IMPRE.tipoimp))
		m.capimp    = SPACE (LEN (IMPRE.capimp))
		m.nomdrv    = SPACE (LEN (DRIVERS.nomdrv))
		m.finimp    = ''
		m.esc_ini   = ''
		m.esc_fin   = ''
		m.secuencia = ''
		= MSG ('No se encontr� la Impresora especificada')
	ENDIF

CASE nomcpo == 'COPIAS'	
	IF EMPTY (m.copias) OR m.copias < 1
		= MSG ('La cantidad de copias debe ser mayor o igual que 1')
		m.copias = 1
	ELSE
		m.copias = INT (m.copias)
	ENDIF
	SHOW GET m.copias

CASE nomcpo == 'NOMARCH'
	IF (ALLT (UPPER (m.nomarch)) <> ALLT (UPPER (m.reporte)) + '.TXT')
		* Si no tiene ext. le agrega ext. punto TXT
		IF ! '.' $ m.nomarch
			m.nomarch = PADR (ALLT (m.nomarch) + '.TXT', LEN (m.nomarch) )
		ENDIF
		* Verificar si ya existe el archivo
		IF FILE (m.nomarch)
			IF ! SINO ('El archivo especificado ya existe. "� Desea sobreescribirlo ?')
				m.nomarch = PADR (m.reporte + '.TXT', LEN (m.nomarch))
			ENDIF
		ELSE
			m.hand_arch = FCREATE ( ALLT (m.nomarch) )
			* Verificar si el camino es correcto
			IF m.hand_arch = -1
				= MSG ('Camino de Directorio incorrecto. El archivo especificado no puede ser creado')
				m.nomarch = PADR (m.reporte + '.TXT', LEN (m.nomarch))
			ELSE
				= FCLOSE (m.hand_arch)
				DELETE FILE ( ALLT (m.nomarch) )
			ENDIF
		ENDIF
	ENDIF
	IF ! desdeafuera
		SHOW GET m.nomarch
	ENDIF

CASE nomcpo == 'LONGPAG'
	IF m.longpag < 1
		= MSG ('La longitud de la p�gina debe ser mayor o igual que uno.')
		m.longpag = INT (m.actlongpag)
	ELSE
		m.longpag = INT (m.longpag)
	ENDIF
	SHOW GET m.longpag

CASE nomcpo == 'ACEPTA'
	m.retorno = (m.acepta==1)		&& Retorno del llamador (si es 1 devuelve .T., si 2 .F.)
	CLEAR READ

CASE nomcpo == 'PREVIMP'
	= ISLOGIC ('m.previmp')

CASE nomcpo == 'GRABOPC'
	= ISLOGIC ('m.grabopc')

CASE nomcpo == 'SI_V_IMP'
	= ISLOGIC ('m.SI_V_IMP')

ENDCASE

RETURN



*************************************************
FUNCTION impr
*************************************************
* Autor: Renato M. Rossello
* Fecha: 24/11/94
* Funcionamiento: Imprime un reporte seg�n los datos
*   especificados eb ventana de impresi�n
	
	PRIVATE capturar, aux_nom_arch, fincaptura
	PRIVATE m.pos, m.sel, m.font, m.fontsz, m.factorh,;
		m.nom_impr, m.pos2, m.expr, m.rpt_ok		&& ### VFP
	
	* Mensaje durante la impresi�n
	IF (ALLT (m.tit_v_repo) == 'IMPRESION')
		WAIT WIND 'Imprimiendo' NOWAIT
	ELSE
		WAIT WIND 'Imprimiendo ' + m.tit_v_repo NOWAIT
	ENDIF
	
	* AND (INT (m.actlongpag) <> m.longpag)
	* Estaba incluida en la condicion siguiente, pero al sacar la asig. de
	* actlongpag no va, en cambio se agrego la 2da para ver si se modifica
	* el valor de la tabla, en ese caso se actualizara el reporte.
	* Gabriel 211095
	
	* ### VFP : Se cambi� el bloque por el bloque de abajo
	* IF (m.SI_V_IMP = 'S') AND (m.longant # m.longpag)
	* 	ant_area = SELECT ()
	* 	SELECT 0
	* 	USE (repo_arch)	
	* 	LOCATE FOR objcode = 53
	* 	IF FOUND ()
	* 		REPLACE height WITH m.longpag
	* 	ENDIF
	* 	USE
	* 	SELECT (ant_area)
	* ENDIF
		
	IF .T.
		m.sel_prn_nom = _sel_prn_nom	&& ### VFP
		
		ant_area = SELECT ()
		SELECT 0
		USE (repo_arch)

		* Comienzo Alejandro
		reporte_original = repo_arch
		LOCATE FOR platform = 'DOS' AND objtype = 1 AND objcode = 53
		IF FOUND ()
			_ASCIICOLS = width
			_ASCIIROWS = height
		ELSE
			_ASCIICOLS = 280
			_ASCIIROWS = 63
		ENDIF
		* Fin Alejandro		

		* Temporal
		* VFP
		* m.repo_nue = SUBSTR(SYS(2015), 3, 8)+".FRX"
		m.repo_nue = ADDBS(SYS(2023)) + SUBSTR(SYS(2015), 3, 8)+".FRX"
		
		* COPY TO (m.repo_nue) WITH CDX
		* USE
		
		m.repo_arch = STRTRAN (DBF(), "FRX", "FR*")
		USE
		COPY FILE (m.repo_arch) TO (STRTRAN(m.repo_nue, ".FRX", ".*"))
		*		

		m.repo_arch = ["] + m.repo_nue + ["]
		USE &repo_arch

		IF (m.SI_V_IMP = 'S') AND (m.longant # m.longpag)
			LOCATE FOR objcode = 53
			IF FOUND ()
				REPLACE height WITH m.longpag
			ENDIF
		ENDIF

		m.rpt_ok = .F.
		LOCATE FOR "RPT_OK" $ UPPER (comment)
		m.rpt_ok = FOUND()

		USET("ASA\tipoimp")

		IF TYPE("tipoimp.cpi10")=="C"
			m.sel = SELECT()

			IF m.curopc==2
				* Busco el driver para impresi�n por pantalla
				m.sel_prn_nom = "PANTALLA "
			ENDIF

			SELECT tipoimp
			m.nom_impr = m.sel_prn_nom

			* Para soporte NT
			IF LEFT(m.nom_impr, 1)=="\"
				m.pos = RAT("\", m.nom_impr)
				m.nom_impr = SUBSTR(m.nom_impr, m.pos+1)
			ENDIF

			m.pos = AT(" ", m.nom_impr)
			IF m.pos = 0
				m.pos = LEN(m.nom_impr)
			ENDIF
			m.pos = MIN (m.pos, LEN(tipoimp.tipoimp))

			IF m.pos > 0
				* LOCATE FOR tipoimp=LEFT(m.sel_prn_nom, m.pos-1)
				LOCATE FOR tipoimp = LEFT(m.nom_impr, m.pos)
				
				DO CASE
				CASE EOF()
					WAIT WIND "Error: tipo de impresora "+LEFT(m.nom_impr, m.pos-1)+" no definida"
					USE
					SELECT (ant_area)
					RETURN
				CASE ALLTRIM(UPPER(tipoimp.tipoimp)) <> "PANTALLA" AND !CHEQ_OS_ID (tipoimp.expr, tipoimp.tipoimp)
					USE
					SELECT (ant_area)
					RETURN
				CASE EMPTY(tipoimp.cpi10)
					* Nada
				OTHERWISE
					m.fontsz = 10
					* m.font = AT("CPI", UPPER(drivers.nomdrvlog))
					* m.font = AT("CPI", UPPER(m.nomdrvlog))	### XXX Alejandro 04-06-2001
					m.font = AT("CPI", STRTRAN (UPPER(m.nomdrvlog), ' ', ''))
					
					IF m.font > 0
						* m.fontsz = VAL2 (SUBSTR(drivers.nomdrvlog, m.font-2, 2))
						m.fontsz = VAL_NUM (SUBSTR(m.nomdrvlog, m.font-2, 2))
					ENDIF
					
					m.fontsz = ALLTRIM(STR(m.fontsz))
					m.factorh = 1
					
					* IF m.curopc = 2		&&// Pantalla
					* 	DO CASE
					* 	CASE m.fontsz=="10"
					* 		m.font = "Courier New,10"
					* 	CASE m.fontsz=="12"
					* 		* m.font = "Courier New,8"
					* 		m.font = "Courier New,9"
					* 	* CASE m.fontsz=="16"
					* 	OTHERWISE
					* 		* m.font = "Courier New,7"
					* 		m.font = "Sans Serif,8"
					* 	ENDCASE
					* ELSE
					* 	* m.font = RTRIM(tipoimp.font)+" "+tipoimp.cpi10
					* 	* m.font = RTRIM(tipoimp.cpi12)
					* 	m.font = RTRIM ( EVAL ([tipoimp.cpi] + m.fontsz))
					* 
					* 	* Pas� abajo
					* 	* DO CASE
					* 	* CASE m.fontsz=="10"
					* 	* CASE m.fontsz=="12"
					* 	* 	m.factorh = 1.15
					* 	* CASE m.fontsz=="16"
					* 	* 	m.factorh = 0.8
					* 	* ENDCASE
					* ENDIF
					
					* IF m.curopc==2
					* 	* Busco el driver para impresi�n por pantalla
					* 	SEEK "PANTALLA" ORDER primario IN tipoimp
					* ENDIF
					
					m.font = RTRIM ( EVAL ([tipoimp.cpi] + m.fontsz))

					DO CASE
					CASE m.rpt_ok		&& Reporte ya modificado para VFP
						m.factorh = 1
					CASE m.fontsz=="10"
					CASE m.fontsz=="12"
						m.factorh = 1.15
					CASE m.fontsz=="16"
						* m.factorh = 0.8
						m.factorh = 0.79
					ENDCASE
					
					* m.pos = AT(",", m.font)
					SELECT (m.sel)

					* ###> Ahora dejo el tama�o y la orientaci�n de la p�gina original
					IF rpts.longpag==2
						GO TOP
						m.expr = tipoimp.expr
						IF "ORIENTATION" $ expr
							m.expr_o = MLINE(expr, ATCLINE("ORIENTATION", expr))
							m.expr_r = MLINE(m.expr, ATCLINE("ORIENTATION", m.expr))
							m.expr = STRTRAN(m.expr, m.expr_r, m.expr_o)
						ENDIF
						
						IF "PAPERSIZE" $ expr
							m.expr_o = MLINE(expr, ATCLINE("PAPERSIZE", expr))
							m.expr_r = MLINE(m.expr, ATCLINE("PAPERSIZE", m.expr))
							m.expr = STRTRAN(m.expr, m.expr_r, m.expr_o)
						ENDIF
					ELSE
						m.expr = tipoimp.expr
					ENDIF

					m.pos = AT(",", m.font)
					
					* Arreglo fonts de labels y textboxs
					REPLACE ;
						fontface WITH RTRIM(LEFT(m.font, m.pos-1)) ,;
						fontsize WITH VAL(SUBSTR(m.font, m.pos+1)) ,;
						mode     WITH 1 ,;
						offset   WITH 0 ;
						;
						FOR INLIST(objtype, 5, 8) AND !comment=="BarCode" AND fontface # 'Arial'	&& Alejandro

					* Corrijo el formato en los labels
					REPLACE ALL ;
						fontstyle WITH STYLE_2_BIT (style) ;
						FOR objtype==5 ;
						AND ! EMPTY (style)
						
					* Seteo configuraci�n de impresora y p�gina
					REPLACE ;
						expr WITH m.expr,;		&& tipoimp.expr ,
						tag  WITH drivers.tag  ,; && tag  WITH tipoimp.tag  (para m�lt. S.O.)
						tag2 WITH drivers.tag2 ,; && tag2 WITH tipoimp.tag2  (para m�lt. S.O.)
						vpos WITH tipoimp.vpos ,;
						hpos WITH tipoimp.hpos ,;
						height WITH tipoimp.height ,;
						width  WITH tipoimp.width  ;
						;						
						FOR objcode==53
					*	vpos WITH tipoimp.vpos + IIF (m.curopc==2,1440,0) 

					* Reposiciono horizontalmente
					REPLACE ALL ;
						hpos  WITH hpos  * m.factorh ,;
						width WITH CEILING (width * (m.factorh + IIF (INLIST (objtype, 5, 8), 0.1, 0)))
						
					*	width WITH width * (m.factorh + IIF(INLIST(objtype, 5, 8), 0.2, 0))
					*	width WITH width * m.factorh
					*	width WITH width * IIF(INLIST(objtype, 5, 8), MAX(m.factorh, 1), m.factorh)
				ENDCASE

			ENDIF

			USE
			SELECT (ant_area)
		ENDIF
	ENDIF
	* ### VFP (hasta ac�)

	* Selecciona �rea en la que se realizar� el reporte
	SELECT (area_repo)

	IF m.seleccmul = 'S'
		PRIV i3_ejec
		i3_ejec = 'selmult'
		IF &i3_ejec (SYS (14,1))
			IF EMPTY (m.p_expr_repo)
				p_expr_repo = 'FOR selmult.seleccion = "S"'
			ELSE
				p_expr_repo = p_expr_repo + 'AND selmult.seleccion = "S"'
			ENDIF
		ENDIF
	ENDIF

	_pcopies = 1
	SET SEPARATOR TO '.'
	SET POINT TO ','
	DO CASE
	CASE m.curopc = 1  && Impresora.
		* _pdsetup = m.nomdrv	### VFP
		_pdsetup = ""
		m.pos = RAT(" (", m.sel_prn_nom)
		IF m.pos > 0
			m.sel_prn_nom = LEFT(m.sel_prn_nom, m.pos-1)
		ENDIF

		SET PRINTER TO NAME (m.sel_prn_nom)
	    
		_pcopies = m.copias
		IF (! EMPTY (m.capimp)) AND (INLIST (p_tip_imp, 2, 3) ) && ;
			AND ( (! EMPTY (m.finimp)) OR !(_capimp == m.capimp) )

			capturar = '! ' + m.capimp
			* &capturar		### VFP
	    ENDIF

		IF m.previmp = 'S' AND p_tip_imp <> 1
			REPORT FORM &repo_arch &p_expr_repo PREV
		ENDIF
		IF p_tip_imp <> 1
			FOR I = 1 TO _pcopies
				REPORT FORM &repo_arch &p_expr_repo NOEJECT NOCONSOLE TO PRINTER
			ENDFOR
		ENDIF
		IF INLIST (p_tip_imp, 1, 3) AND ! EMPTY (m.finimp)
			fincaptura = '! ' + m.finimp
			*	&fincaptura		### VFP
		ENDIF
	CASE m.curopc = 2 && Pantalla
		_PDSETUP = ''
		IF INLIST (p_tip_imp, 2, 3)
			_pantarch = '_' + RIGHT (SYS(2015),7) + '.TXT'
			IF ! EMPTY (_pantarch) && defensivo
				&& Modif. por Cesar para VFP
				* REPORT FORM &repo_arch &p_expr_repo NOCONSOLE TO FILE &_pantarch
				* HIDE WINDOWS ALL
				* ACTIVATE SCREEN
				* REPORT FORM &repo_arch &p_expr_repo PREVIEW IN SCREEN
				* SHOW WINDOWS ALL

				* DEFINE WINDOW Prev FROM 0,0 TO SROWS()-2,SCOLS()-2 NAME frmPrev
				* frmPrev.Height = _Screen.Height-40
				* frmPrev.Width  = _Screen.Width-40
				* frmPrev.WindowState = 2
				* ACTIVATE WINDOW Prev
				* REPORT FORM &repo_arch &p_expr_repo PREVIEW WINDOW Prev
				* RELEASE WINDOW Prev

				* REPORT FORM &repo_arch &p_expr_repo PREVIEW IN SCREEN
				DO FORM RPT_PREV
			ENDIF
		ENDIF
		IF p_tip_imp = 0			
			* IF ! EMPTY (_pantarch) && defensivo	### VFP
			IF VARTYPE(_pantarch)<>"U" AND ! EMPTY (_pantarch) && defensivo
				* REPORT FORM &repo_arch &p_expr_repo NOCONSOLE TO FILE &_pantarch ADDITIVE
				DO FORM RPT_PREV
			ENDIF
		ENDIF
		IF INLIST (p_tip_imp, 1, 3)
			WAIT CLEAR
			IF ! EMPTY (_pantarch) && defensivo
			    && Modif. por cesar para VFP (comente las 4 instrucciones siguientes)
				*DEFI WIND vent_prev FROM 1,0 TO 24,79 TITLE m.tit_v_repo FOOT ' Para recorrer reporte use:   PageUp PageDown - ESC: sale '
				*MODI FILE (_pantarch) WINDOW vent_prev NOEDIT
				*RELEASE WIND vent_prev
				*DELETE FILE (_pantarch)
			ENDIF
		ENDIF
	
		m.copias = 1
		m.previmp= 'N'
		SHOW GET m.copias
		SHOW GET m.previmp
	CASE m.curopc = 3 && Browse

#IFNDEF FORM_BROWSE
		_PDSETUP = ''
		DEFI WIND vent_prev FROM 1,0 TO 24,79 TITLE m.tit_v_repo FOOT ' Para recorrer reporte use:   PageUp PageDown - ESC: sale '
		BROWSE WINDOW vent_prev NOEDIT NORMAL
		RELEASE WIND vent_prev
#ELSE
		DO FORM BROWLIS2 WITH m.tit_v_repo
#ENDIF

		m.copias = 1
		m.previmp= 'N'
		SHOW GET m.copias
		SHOW GET m.previmp

	CASE m.curopc = 4 && Archivo
		_PDSETUP = ''
		_ASCIICOLS = 163
		_ASCIIROWS = 63	
		IF EMPTY (m.nomarch)
			IF m.sel_tip < 15
				m.nomarch = UPPER (ALLT (p_reporte)) + formatos[m.sel_tip,2]
			ELSE
				m.nomarch = UPPER (ALLT (p_reporte)) + '.TXT'
			ENDIF
		ENDIF
		aux_nom_arch = m.nomarch
		IF m.sel_tip = 1 && Reporte a Archivo
			IF INLIST (p_tip_imp, 2, 3)
				* REPORT FORM &repo_arch &p_expr_repo NOCONSOLE TO FILE &aux_nom_arch	### XXX Alejandro
				REPORT FORM &reporte_original &p_expr_repo NOCONSOLE TO FILE &aux_nom_arch ASCII
			ENDIF
			IF p_tip_imp = 0
				* REPORT FORM &repo_arch &p_expr_repo NOCONSOLE TO FILE &aux_nom_arch ADDITIVE	### XXX Alejandro
				REPORT FORM &reporte_original &p_expr_repo NOCONSOLE TO FILE &aux_nom_arch ASCII ADDITIVE
			ENDIF
		ELSE
			IF m.sel_tip < 15 && Formatos Predefinidos de Fox
				IF m.sel_tip = 2 && DBFs
					COPY TO &aux_nom_arch
				ELSE
					aux_formato = formatos[m.sel_tip,3]
					COPY TO &aux_nom_arch TYPE &aux_formato
				ENDIF
			ELSE && Formatos definidos por el Usuario
				IF ALLTRIM (formatos[m.sel_tip,4]) == 'SQL'
					m.cur_alias = ALIAS()
					m.sentencia = formatos[m.sel_tip,3]
					*Ejecuto SQL y envio el resultado al cursor __temp__
					SELECT &sentencia from &cur_alias INTO CURSOR __temp__
					* Copio contenido del cursor al archivo indicado por el usuario
					SELECT __temp__
					COPY TO &aux_nom_arch TYPE SDF
					SELECT (cur_alias)
				ELSE
					*Si es una funci�n definida por el usuario, directamante evaluo la expresi�n
					m.sentencia = formatos[m.sel_tip,3]
					&sentencia
				ENDIF
				*Ejecuta sentencia SQL .con
			ENDIF
		ENDIF
	ENDCASE

	IF m.seleccmul = 'S' AND !EMPTY (SYS (14,1))
		SET RELATION OFF INTO selmult
	ENDIF

	IF INLIST (p_tip_imp, 1, 3)
		WAIT CLEAR
	ENDIF
	SET SEPARATOR TO ','
	SET POINT TO '.'
	* asignar variables locales a p�blicas
	_previmp   = m.previmp
	_curopc    = m.curopc
	_capimp    = m.capimp
	_finimp    = m.finimp
	_nomdrv    = m.nomdrv
	_copias    = m.copias
	_nomarch   = m.nomarch
	_si_v_imp  = m.si_v_imp
	_tit_v_repo= m.tit_v_repo
	_esc_ini   = m.esc_ini
	_esc_fin   = m.esc_fin
	_secuencia = m.secuencia
	_longpag	 = m.longpag
	_nomrep 	 = RPTS.reporte
	_nomimp		 = m.nomimp
RETURN


* Pasa el estilo de formato string a bit
FUNCTION STYLE_2_BIT (m.estilo)

LOCAL m.ret

m.ret = 0

IF "B" $ m.estilo
	m.ret = m.ret+1
ENDIF

IF "I" $ m.estilo
	m.ret = m.ret+2
ENDIF

RETURN m.ret


* Retorna una l�nea de un campo memo con una expresi�n en particular
FUNCTION BUSCA_LINEA (m.str, m.busca)

#define CRLF CHR(13)+CHR(10)

LOCAL m.pos

m.str = CRLF+m.str+CRLF
m.pos = AT(CRLF+m.busca, m.str)

IF EMPTY(m.pos)
	m.str = ""
ELSE
	m.str = SUBSTR(m.str, m.pos+1)
	m.pos = AT(CRLF, m.str)
	m.str = LEFT(m.str, m.pos-1)
	m.str = SUBSTR(m.str, m.pos+1)
ENDIF

RETURN (m.str)



*************************************************
FUNCTION W_CUROPC
*************************************************
PRIVATE retorno
retorno = .T.

IF m.curopc # 1
	m.copias = 1
	m.previmp= 'N'
	SHOW GET m.copias
	SHOW GET m.previmp
	retorno = .F.
ENDIF

RETURN retorno


*       �����������������������������������������������������������
*       �                                                         �
*       � _S1K0M6LYL           Read Level When                    �
*       �                                                         �
*       � Function Origin:                                        �
*       �                                                         �
*       �                                                         �
*       � From Platform:       MS-DOS                             �
*       � From Screen:         IMPRIME3                           �
*       � Called By:           READ Statement                     �
*       � Snippet Number:      1                                  �
*       �                                                         �
*       O����������������������������������������������������������
*
FUNCTION _s1k0m6lyl     && Read Level When
*
* When Code from screen: IMPRIME3
*
#REGION 1
* _curobj = objnum (m.acepta)	### VFP
* SET_CUROBJ = objnum (m.acepta)		### VFP
SET_CUROBJ (objnum2 ("acepta"))

*
*Habilito o deshabilito selecci�n de formato
IF m.curopc = 4
  SHOW GET m.sel_tip ENABLE
ELSE
  SHOW GET m.sel_tip DISABLE
ENDIF




*       �����������������������������������������������������������
*       �                                                         �
*       � _S1K0M6LYO           Read Level Show                    �
*       �                                                         �
*       � Function Origin:                                        �
*       �                                                         �
*       �                                                         �
*       � From Platform:       MS-DOS                             �
*       � From Screen:         IMPRIME3                           �
*       � Called By:           READ Statement                     �
*       � Snippet Number:      2                                  �
*       �                                                         �
*       O����������������������������������������������������������
*
FUNCTION _s1k0m6lyo     && Read Level Show
PRIVATE currwind
STORE WOUTPUT() TO currwind
*
* Show Code from screen: IMPRIME3
*
#REGION 1
IF SYS(2016) = "_S1K0M5O6W" OR SYS(2016) = "*"
	ACTIVATE WINDOW _s1k0m5o6w SAME
	@ 0,13 SAY m.nomprog ;
		SIZE 1,30, 0
	@ 3,47 SAY m.tipoimp ;
		SIZE 1,15, 0
	@ 4,26 SAY m.nomdrv ;
		SIZE 1,20, 0
	@ 4,47 SAY m.nomdrvlog ;
		SIZE 1,15, 0
ENDIF
IF NOT EMPTY(currwind)
	ACTIVATE WINDOW (currwind) SAME
ENDIF*       �����������������������������������������������������������
*       �                                                         �
*       � 27/10/1997            NEWCTRL.SPR              14:44:44 �
*       �                                                         �
*       ����������������������������������������������������������
*       �                                                         �
*       � Author's Name                                           �
*       �                                                         �
*       � Copyright (c) 1997 Company Name                         �
*       � Address                                                 �
*       � City,     Zip                                           �
*       �                                                         �
*       � Description:                                            �
*       � This program was automatically generated by GENSCRN.    �
*       �                                                         �
*       O����������������������������������������������������������


*       �����������������������������������������������������������
*       �                                                         �
*       �          NEWCTRL/MS-DOS Setup Code - SECTION 1          �
*       �                                                         �
*       O����������������������������������������������������������
*


#IFDEF TOOLBAR_CONTROL

FUNCTION NEWCTRL

LOCAL m.ctrl

m.ctrl = FormID(m.vent_ppal)

IF EMPTY(m.ctrl)
	RETURN (.T.)
ENDIF

IF "U" $ VARTYPE(_VENT_CTRL)
	PUBLIC _VENT_CTRL, _VENT_CONT

	_VENT_CTRL = ""
	_VENT_CONT = 0
ELSE
	RELEASE _VENT_CTRL
ENDIF

* _Screen.Forms(FormID(m.vent_ppal)).NewCtrl = .T.
_Screen.Forms(m.ctrl).NewCtrl = .T.

_VENT_CONT = _VENT_CONT+1
_VENT_CTRL = CREATEOBJECT("sAbmToolBar")

*!*	* Actualizo los tooltips
*!*	FOR m.ctrl = 1 TO _VENT_CTRL.ControlCount
*!*		WITH _VENT_CTRL.Controls (m.ctrl)
*!*			IF UPPER(.BaseClass)=="COMMANDBUTTON"
*!*				.ToolTipText = STRTRAN(.ToolTipText, "@x", desc_tab_mae)
*!*			ENDIF
*!*		ENDWITH
*!*	NEXT

_VENT_CTRL.Dock(2)
_VENT_CTRL.Visible = .T.

RETURN .T.


*       �����������������������������������������������������������
*       �                                                         �
*       �               NEWCTRL/MS-DOS Cleanup Code               �
*       �                                                         �
*       O����������������������������������������������������������
*

#REGION 1
*************************************************
* act_control
*************************************************
NOTE        a  ON        a OFF      INVISIBLE ;
	Nuevo    !ed y !nu    ed o  nu    !alta     ;
	Edita    !ed y !nu    ed o  nu    !modi     ;
	Graba     ed o  nu   !ed y !nu    !alta y !modi;
	Borra        ed          nu       !baja     ;
	Cancela   ed o  nu   !ed y !nu              ;
	Dem�s    !ed y !nu    ed o nu     !consul   ;
	c_anterior almacena el ultimo estado, si se ;
	trata de hacer el mismo retorna enseguida

***************************************************


*       �����������������������������������������������������������
*       �                                                         �
*       �   NEWCTRL/MS-DOS Supporting Procedures and Functions    �
*       �                                                         �
*       O����������������������������������������������������������
*


FUNCTION nwact_ctrl
***************************************************

PRIVATE gs_act_vent, grupo1, grupo2

* _VENT_CTRL = _Screen.Forms(FormID("Controls")).PageFrame1.Page1
* m.grupo1 = _VENT_CTRL.cmgProc
* m.grupo2 = _VENT_CTRL.cmgBusq

#DEFINE BTN_NUEVO		1
#DEFINE BTN_EDITA		2
#DEFINE BTN_GRABA		3
#DEFINE BTN_BORRA		4
#DEFINE BTN_CANCELA		5
#DEFINE BTN_SALE		6
#DEFINE BTN_AYUDA   	7

#DEFINE BTN_PRIMERO  	9
#DEFINE BTN_ANTERIOR	10
#DEFINE BTN_SIGUIENTE   11
#DEFINE BTN_ULTIMO   	12
#DEFINE BTN_BUSCA   	13

IF c_edita or c_nuevo
	IF c_anterior = 1
		RETURN .T.
	ENDIF

	* SHOW GET nuevo      disabled
	* SHOW GET edita      disabled
	* SHOW GET cancela    enabled
	_VENT_CTRL.Controls[BTN_NUEVO  ].Enabled = .F.
	_VENT_CTRL.Controls[BTN_EDITA  ].Enabled = .F.
	_VENT_CTRL.Controls[BTN_CANCELA].Enabled = .T.

	IF c_edita
	 	IF gl_modi and gl_cons
			* SHOW GET graba  enabled
			_VENT_CTRL.Controls[BTN_GRABA].Enabled = .T.
		ENDIF
	 	IF gl_baja and gl_cons
			* SHOW GET borra      enable
			_VENT_CTRL.Controls[BTN_BORRA].Enabled = .T.
		ENDIF
	ENDIF

	* SHOW GET siguiente  disabled
	* SHOW GET anterior   disabled
	* SHOW GET primero    disabled
	* SHOW GET ultimo     disabled
	* SHOW GET busca      disabled
	* SHOW GET sale       disabled
	* SHOW GET orden      disabled

	_VENT_CTRL.Controls[BTN_SIGUIENTE].Enabled = .F.
	_VENT_CTRL.Controls[BTN_ANTERIOR ].Enabled = .F.
	_VENT_CTRL.Controls[BTN_PRIMERO  ].Enabled = .F.
	_VENT_CTRL.Controls[BTN_ULTIMO   ].Enabled = .F.
	_VENT_CTRL.Controls[BTN_BUSCA    ].Enabled = .F.
	_VENT_CTRL.Controls[BTN_SALE     ].Enabled = .F.

	c_anterior = 1
ELSE
	IF c_anterior = 0
		return .t.
	ENDIF

	IF gl_alta
	  	* SHOW GET nuevo    enabled
		_VENT_CTRL.Controls[BTN_NUEVO].Enabled = .T.
	ENDIF

	IF (gl_modi OR gl_baja) and gl_cons
		* SHOW GET edita    enabled
		_VENT_CTRL.Controls[BTN_EDITA].Enabled = .T.
	ENDIF

	* SHOW GET graba      disabled
	* SHOW GET cancela    disabled
	* SHOW GET borra 	  disabled
	* SHOW GET sale       enabled
	_VENT_CTRL.Controls[BTN_GRABA  ].Enabled = .F.
	_VENT_CTRL.Controls[BTN_CANCELA].Enabled = .F.
	_VENT_CTRL.Controls[BTN_BORRA  ].Enabled = .F.
	_VENT_CTRL.Controls[BTN_SALE   ].Enabled = .T.

	IF gl_cons
	  	* SHOW GET siguiente  enabled
	  	* SHOW GET anterior   enabled
	  	* SHOW GET primero    enabled
	  	* SHOW GET ultimo     enabled
	  	* SHOW GET busca      enabled
	  	* SHOW GET orden      enabled
		_VENT_CTRL.Controls[BTN_SIGUIENTE].Enabled = .T.
		_VENT_CTRL.Controls[BTN_ANTERIOR ].Enabled = .T.
		_VENT_CTRL.Controls[BTN_PRIMERO  ].Enabled = .T.
		_VENT_CTRL.Controls[BTN_ULTIMO   ].Enabled = .T.
		_VENT_CTRL.Controls[BTN_BUSCA    ].Enabled = .T.
	ENDIF

	c_anterior = 0
ENDIF

*!*	gs_act_vent = ''
*!*	IF WOUTPUT() <> 'CONTROLS'
*!*		gs_act_vent = WOUTPUT()
*!*		ACTIVATE WINDOW controls SAME
*!*	ENDIF

*!*	* SET COLOR TO w+/n

*!*	DO CASE
*!*	CASE c_edita
*!*		* @ 20,4 SAY " Editando  "
*!*		_VENT_CTRL.lblAccion.Caption = "<< Editando"
*!*	CASE c_nuevo
*!*		* @ 20,4 SAY " Agregando "
*!*		_VENT_CTRL.lblAccion.Caption = "<< Agregando"
*!*	OTHERWISE
*!*		* @ 20,4 SAY "Consultando"
*!*		_VENT_CTRL.lblAccion.Caption = "<< Consultando"
*!*	ENDCASE

*!*	SET COLOR TO
*!*	IF !EMPTY(gs_act_vent)
*!*		ACTIVATE WINDOW (gs_act_vent) SAME
*!*	ENDIF

RETURN .T.


#ELSE

#REGION 1

FUNCTION newctrl	&&// ### VFP
KEYBOARD '{shift+f12}' CLEAR
WAIT WIND ''
DIMENSION des_tag (ALEN (des_tag))
IF TYPE('des_tag(1)') = 'C' ;
	AND des_tag(1) = "Ninguno"
	des_tag(1) = "Ingreso"
ENDIF

#REGION 0
REGIONAL m.currarea, m.talkstat, m.compstat

IF SET("TALK") = "ON"
	SET TALK OFF
	m.talkstat = "ON"
ELSE
	m.talkstat = "OFF"
ENDIF
m.compstat = SET("COMPATIBLE")
SET COMPATIBLE FOXPLUS

*       �����������������������������������������������������������
*       �                                                         �
*       �                MS-DOS Window definitions                �
*       �                                                         �
*       O����������������������������������������������������������
*

IF NOT WEXIST("controls") ;
	OR UPPER(WTITLE("CONTROLS")) == "CONTROLS.PJX" ;
	OR UPPER(WTITLE("CONTROLS")) == "CONTROLS.SCX" ;
	OR UPPER(WTITLE("CONTROLS")) == "CONTROLS.MNX" ;
	OR UPPER(WTITLE("CONTROLS")) == "CONTROLS.PRG" ;
	OR UPPER(WTITLE("CONTROLS")) == "CONTROLS.FRX" ;
	OR UPPER(WTITLE("CONTROLS")) == "CONTROLS.QPR"

*!*		DEFINE WINDOW controls ;	&& ### VFP
*!*			FROM 1, 63 ;
*!*			TO 23,79 

	DEFINE WINDOW controls ;
		FROM 1, SCOLS()-20 ;
		TO 23,SCOLS()-3 ;
		TITLE " CONTROLES " ;
		FLOAT ;
		NOCLOSE ;
		SHADOW ;
		NOMINIMIZE ;
		DOUBLE ;
		COLOR SCHEME 1
ENDIF


*       �����������������������������������������������������������
*       �                                                         �
*       �              NEWCTRL/MS-DOS Screen Layout               �
*       �                                                         �
*       O����������������������������������������������������������
*

#REGION 1
IF WVISIBLE("controls")
	ACTIVATE WINDOW controls SAME
ELSE
	ACTIVATE WINDOW controls NOSHOW
ENDIF
@ 20,1 SAY "" ;
	SIZE 1,2, 0
@ 0,1 TO 8,13
@ 9,1 TO 15,13
@ 0,3 SAY " Proceso " ;
	SIZE 1,9, 0
@ 9,2 SAY " B�squedas " ;
	SIZE 1,11, 0
@ 1,2 GET m.nuevo ;
	PICTURE "@*VN Ag\<rega" ;
	SIZE 1,11,3 ;
	DEFAULT 1 ;
	WHEN whe_gral(varread()) ;
	VALID nwval_ctrl(varread()) ;
	DISABLE ;
	MESSAGE 'Agrega un nuevo registro de ' + desc_tab_mae
@ 2,2 GET m.edita ;
	PICTURE "@*HN \<Edita" ;
	SIZE 1,11,1 ;
	DEFAULT 1 ;
	WHEN whe_gral(varread()) ;
	VALID nwval_ctrl(varread()) ;
	DISABLE ;
	MESSAGE 'Modifica el registro actual de ' + desc_tab_mae
@ 3,2 GET m.graba ;
	PICTURE "@*HN \<ProcesaF5" ;
	SIZE 1,11,1 ;
	DEFAULT 1 ;
	WHEN whe_gral(varread()) ;
	VALID nwval_ctrl(varread()) ;
	DISABLE ;
	MESSAGE 'Graba los datos del registro actual de ' + desc_tab_mae
@ 4,2 GET m.borra ;
	PICTURE "@*HN B\<orra  F6" ;
	SIZE 1,11,1 ;
	DEFAULT 1 ;
	WHEN whe_gral(varread()) ;
	VALID nwval_ctrl(varread()) ;
	DISABLE ;
	MESSAGE 'Borra el registro actual de ' + desc_tab_mae
@ 5,2 GET m.cancela ;
	PICTURE "@*HN \?\<Cancela" ;
	SIZE 1,11,1 ;
	DEFAULT 1 ;
	WHEN whe_gral(varread()) ;
	VALID nwval_ctrl(varread()) ;
	DISABLE ;
	MESSAGE 'Ignora cambios realizados en el registro de ' + desc_tab_mae
@ 6,2 GET m.sale ;
	PICTURE "@*HN \?Sa\<le  Esc" ;
	SIZE 1,11,2 ;
	DEFAULT 1 ;
	WHEN whe_gral(varread()) ;
	VALID nwval_ctrl(varread()) ;
	MESSAGE 'Termina y Sale de la ventana de ' + tit_ventana
@ 7,2 GET m._gs_ayabm ;
	PICTURE "@*HN A\<yuda" ;
	SIZE 1,11,1 ;
	DEFAULT 1 ;
	WHEN whe_gral(varread()) ;
	VALID nwval_ctrl(varread()) ;
	MESSAGE 'Ayuda del programa para Alta, Bajas y Modificaciones (ABM) General'
@ 10,2 GET m.siguiente ;
	PICTURE "@*HN \<Sigte. ^S" ;
	SIZE 1,11,1 ;
	DEFAULT 1 ;
	WHEN whe_gral(varread()) ;
	VALID nwval_ctrl(varread()) ;
	DISABLE ;
	MESSAGE 'Visualiza el registro de ' + desc_tab_mae + ' siguiente al actual'
@ 11,2 GET m.anterior ;
	PICTURE "@*HN \<Anter. ^A" ;
	SIZE 1,11,1 ;
	DEFAULT 1 ;
	WHEN whe_gral(varread()) ;
	VALID nwval_ctrl(varread()) ;
	DISABLE ;
	MESSAGE 'Visualiza el registro de ' + desc_tab_mae + ' anterior al actual'
@ 12,2 GET m.primero ;
	PICTURE "@*HN \<Primero" ;
	SIZE 1,11,1 ;
	DEFAULT 1 ;
	WHEN whe_gral(varread()) ;
	VALID nwval_ctrl(varread()) ;
	DISABLE ;
	MESSAGE 'Visualiza el primer registro de ' + desc_tab_mae
@ 13,2 GET m.ultimo ;
	PICTURE "@*HN \<Ultimo" ;
	SIZE 1,11,1 ;
	DEFAULT 1 ;
	WHEN whe_gral(varread()) ;
	VALID nwval_ctrl(varread()) ;
	DISABLE ;
	MESSAGE 'Visualiza el �ltimo registro de ' + desc_tab_mae
@ 14,2 GET m.busca ;
	PICTURE "@*HN \<Busca" ;
	SIZE 1,11,1 ;
	DEFAULT 1 ;
	WHEN whe_gral(varread()) ;
	VALID nwval_ctrl(varread()) ;
	DISABLE ;
	MESSAGE 'Busca un registro de ' + desc_tab_mae + ' determinado'

* ### VFP : No funciona. Revisar
*!*	@ 17,0 GET m.orden ;
*!*		PICTURE "@^" ;
*!*		FROM des_tag ;
*!*		SIZE 3,15 ;
*!*		DEFAULT 1 ;
*!*		WHEN whe_gral(varread()) ;
*!*		VALID nwval_ctrl(varread()) ;
*!*		DISABLE ;
*!*		MESSAGE 'Selecciona el Orden en el cual se ver�n los registros de ' + desc_tab_mae ;
*!*		COLOR SCHEME 1, 2

* La l�nea que causa el error de sintaxis es la l�nea FROM des_tag,
* se prob� de cambiar el FROM con opciones directamente en PICTURE / FORMAT pero
* el VFP parece ignorarlas (queda en formato de un GET com�n)
	
@ 20,1 GET m.actiwin ;
	PICTURE "@*IHN " ;
	SIZE 1,2,1 ;
	DEFAULT 0 ;
	WHEN whe_gral(varread()) ;
	VALID nwval_ctrl(varread()) ;
	MESSAGE 'Activar la ventana de ' + tit_ventana
@ 16,1 SAY "Ordenado por:" ;
	SIZE 1,13, 0

IF NOT WVISIBLE("controls")
	ACTIVATE WINDOW controls
ENDIF

#REGION 0
IF m.talkstat = "ON"
	SET TALK ON
ENDIF
IF m.compstat = "ON"
	SET COMPATIBLE ON
ENDIF

#REGION 1


FUNCTION nwact_ctrl
***************************************************
PRIVATE gs_act_vent

IF c_edita or c_nuevo
	IF c_anterior = 1
		return .t.
	ENDIF

	SHOW GET nuevo      disabled
	SHOW GET edita      disabled
	SHOW GET cancela    enabled
	IF c_edita
	 	IF gl_modi and gl_cons
			SHOW GET graba  enabled
		ENDIF
	 	IF gl_baja and gl_cons
			SHOW GET borra      enable
		ENDIF
	ENDIF
	SHOW GET siguiente  disabled
	SHOW GET anterior   disabled
	SHOW GET primero    disabled
	SHOW GET ultimo     disabled
	SHOW GET busca      disabled
	SHOW GET sale       disabled
	SHOW GET orden      disabled

	c_anterior = 1
ELSE
	IF c_anterior = 0
		return .t.
	ENDIF

	IF gl_alta
		SHOW GET nuevo    enabled
	ENDIF

	IF (gl_modi OR gl_baja) and gl_cons
		SHOW GET edita    enabled
	ENDIF

	SHOW GET graba      disabled
	SHOW GET cancela    disabled
	SHOW GET borra 			disabled
	SHOW GET sale       enabled

	IF gl_cons
	  	SHOW GET siguiente  enabled
	  	SHOW GET anterior   enabled
	  	SHOW GET primero    enabled
	  	SHOW GET ultimo     enabled
	  	SHOW GET busca      enabled
	  	SHOW GET orden      enabled
	ENDIF

	c_anterior = 0
ENDIF

gs_act_vent = ''
IF WOUTPUT() <> 'CONTROLS'
	gs_act_vent = WOUTPUT()
	ACTIVATE WINDOW controls SAME
ENDIF
SET COLOR TO w+/n
DO CASE
CASE c_edita
	@ 20,4 SAY " Editando  "
CASE c_nuevo
	@ 20,4 SAY " Agregando "
OTHERWISE
	@ 20,4 SAY "Consultando"
ENDCASE
SET COLOR TO
IF !EMPTY(gs_act_vent)
	ACTIVATE WINDOW (gs_act_vent) SAME
ENDIF

RETURN .T.

#ENDIF




*************************************************
FUNCTION nwval_ctrl
*************************************************
PARAMETERS m.campo_ctrl

	PRIVATE m.campo_ctrl, db_scr, vf_graba, vf_nuevo, vf_cancela, ;
		vf_borra, vf_edita, vf_busca_part, vf_busca_clave, ;
		vf_sale, acti_campos, val_grab
	* DIMENSION cpos_clave (ALEN (cpos_clave))	### VFP
	DIMENSION cpos_claves (ALEN (cpos_claves))
	DIMENSION tag_name   (ALEN (tag_name))

	db_scr         = 'db_scr'
	vf_busca_clave = 'busca_clave'

	retorno = .f.

	DO CASE
	CASE m.campo_ctrl = 'NUEVO'
		IF gl_alta
			hay_clave = .t.
			retorno = .T.
			IF ! &vf_busca_clave()
				val_grab = 'val_grab'
				IF val_grab('C')
					vf_nuevo= 'nuevo'
					IF &vf_nuevo()
						c_nuevo = .t.
						= &db_scr (2,1)

						#IFDEF TOOLBAR_CONTROL
							_VENT_CTRL.Controls[BTN_GRABA].Enabled = .T.
						#ELSE
							SHOW GET m.graba ENABLE
						#ENDIF

						ACTIVATE WINDOW &vent_ppal
						* _CUROBJ = OBJNUM(m.&cpos_claves[1])	### VFP
						SET_CUROBJ ( OBJNUM2 (cpos_claves[1] ))
					ELSE
						retorno = .F.
					ENDIF
				ELSE
					* IF _CUROBJ = OBJNUM(m.nuevo)		### VFP
					IF _CUROBJ = OBJNUM2("nuevo")
						= MSG('No se puede Agregar porque la clave es incorrecta.')
						= &db_scr(3,0)
						* _CUROBJ = OBJNUM( m.&cpos_claves[1] )	### VFP
						SET_CUROBJ ( OBJNUM2 ( cpos_claves[1] ) )
					ELSE
						retorno = .F.
					ENDIF
				ENDIF
			ELSE
				= MSG('No se puede Agregar porque ya existe la clave.')
				= &db_scr (3,0)
				ACTIVATE WINDOW &vent_ppal
				* _CUROBJ = OBJNUM( m.&cpos_claves[1] )	### VFP
				SET_CUROBJ ( OBJNUM2 ( cpos_claves[1] ))
			ENDIF
			IF retorno
				c_nuevo = .t.
				ON KEY LABEL CTRL+S DO deac_tec with .t.
				ON KEY LABEL CTRL+A DO deac_tec with .t.
			ENDIF
		ELSE
			=MSG('No se puede Agregar porque no tiene permiso.')
			retorno = .F.
		ENDIF

	CASE m.campo_ctrl = 'EDITA'
		IF (gl_modi OR gl_baja) AND gl_cons
			IF &vf_busca_clave ()
				vf_edita= 'edita'
				IF &vf_edita()
					c_edita = .t.
					=&db_scr(2,3)
					ACTIVATE WINDOW &vent_ppal
					* _CUROBJ = OBJNUM(m.&cpos_claves[1])		### VFP
					SET_CUROBJ ( OBJNUM2 ( cpos_claves[1]) )
					ON KEY LABEL CTRL+S DO deac_tec with .t.
					ON KEY LABEL CTRL+A DO deac_tec with .t.
					retorno = .T.
				ELSE
					retorno = .F.
				ENDIF
			ELSE
				= MSG('No se puede Editar porque no existe la clave.')
				retorno = .F.
			ENDIF
		ELSE
			=MSG('No se puede Consultar o Editar porque no tiene permiso.')
			retorno = .F.
		ENDIF

	CASE m.campo_ctrl = 'GRABA'
		retorno = .T.
		IF type('zona_claves') = 'U'
			zona_claves = .F.
		ENDIF

		IF ! zona_claves AND gl_modi
			IF c_nuevo
				IF &vf_busca_clave ()
					=MSG ('Se agreg� un registro con esta clave en otra terminal. No se puede agregar el registro actual.')
					retorno = .F.
				ELSE
					val_grab = 'val_grab'
					IF val_grab('C')
						PRIVATE vvs_nuevo, vvs_entro, vvs_titvent
						vvs_nuevo = 'vs_nuevo'
						vvs_entro = 'vs_entro'
						vvs_titvent= ' vs_titvent'
						
						long_vent = ALEN(&vvs_nuevo)
						ii = 1
						DO WHILE retorno AND ii <= long_vent
							IF &vvs_nuevo(ii) = 1 AND &vvs_entro(ii) <> 1
								= MSG ( 'FALTA ENTRAR EN LA VENTANA '+&vvs_titvent(ii))
								retorno = .F.
							ELSE
								ii = ii + 1
							ENDIF
						ENDDO
					ELSE
						retorno = .F.
					ENDIF
				ENDIF
			ELSE
				retorno = c_edita
			ENDIF

			IF retorno
				val_grab = 'val_grab'
				IF val_grab('D')
					PUSH KEY
					= DEAC_TEC()
*					KEYBOARD '{shift+f12}' CLEAR
*					WAIT WIND ''
					IF TYPE ('conf_graba') <> 'L' OR conf_graba
						retorno = sino ("� Desea Grabar el Registro ?")
					ELSE
						retorno = .T.
					ENDIF

					POP KEY

					IF c_nuevo
						IF &vf_busca_clave ()
							=MSG ('Se agreg� un registro con esta clave en otra terminal. No se puede agregar el registro actual.')
							retorno = .F.
						ELSE
							val_grab = 'val_grab'
							IF ! val_grab('C')
								retorno = .F.
							ENDIF
						ENDIF
					ENDIF
					
					IF retorno
						vf_graba = 'graba'
						retorno = &vf_graba()
						IF retorno
							FLUSH
							
							* IF _SQL_FLAG	&& ### C/S
							* 	TABLEUPDATE(1)
							* ENDIF
							
							WAIT WINDOW 'Registro grabado.' NOWAIT
							acti_campos = 'acti_campos'
							=&acti_campos(1,0)
							IF c_nuevo
* Comentado por Leo el 5/9/95
*								_CUROBJ = OBJNUM(m.nuevo)
								* _CUROBJ = OBJNUM(m.&cpos_clave[1]) ### VFP
								* _CUROBJ = OBJNUM(m.&cpos_claves[1])	### VFP
								SET_CUROBJ ( OBJNUM2 ( cpos_claves[1]) )
								c_nuevo = .f.
							ELSE
								* _CUROBJ = OBJNUM(m.&cpos_clave[1]) ### VFP
								* _CUROBJ = OBJNUM(m.&cpos_claves[1])	### VFP
								SET_CUROBJ ( OBJNUM2 (cpos_claves[1]) )
								c_edita = .f.
							ENDIF
							* Agrego Gabriel 031095
							ON KEY LABEL CTRL+S DO nwhotkeys WITH "SIGUIENTE"
							ON KEY LABEL CTRL+A DO nwhotkeys WITH "ANTERIOR"
						ENDIF
					ELSE
						* _CUROBJ = gs_num_cpo	### VFP
						SET_CUROBJ ( gs_num_cpo )
						RETORNO = .f.
					ENDIF	
				ENDIF
			ENDIF
		ELSE
			retorno = .F.
		ENDIF

	CASE m.campo_ctrl = 'BORRA'
		IF gl_baja AND gl_cons AND c_edita AND ! EOF(tab_mae)
			PUSH KEY
			= DEAC_TEC()
			IF TYPE ('conf_borra') <> 'L' OR conf_borra
				retorno = SINO ("� Desea Borrar el Registro ?")
			ELSE
				retorno = .T.
			ENDIF

			POP KEY

			IF retorno
				IF &vf_busca_clave()
					vf_borra = 'borra'
					retorno = &vf_borra()
					IF retorno
						IF DELETED ()
							WAIT WINDOW 'Registro borrado.' NOWAIT
							SKIP IN (tab_mae)
							* DBSKIP (tab_mae)
							IF EOF (tab_mae)
							* IF DBEOF (tab_mae)
								SKIP -1 IN (tab_mae)
								* DBGOBOTTOM (tab_mae)
								IF BOF (tab_mae)
								* IF DBBOF (tab_mae)
									prox_reg = 0
								ELSE
									prox_reg = RECNO(tab_mae)
									* prox_reg = DBRECNO(tab_mae)
								ENDIF
							ELSE
								prox_reg = RECNO(tab_mae)
								* prox_reg = DBRECNO(tab_mae)
							ENDIF
							IF prox_reg <> 0
								= IR_REG (prox_reg, tab_mae)
								IF gl_cons
									= &db_scr(3,2)
								ELSE
									= &db_scr(3,0)
								ENDIF
							ELSE
								= &db_scr(1,0)					
							ENDIF
						ELSE
							WAIT WINDOW 'Registro dado de baja' NOWAIT
							IF gl_cons
								= &db_scr(3,2)
							ELSE
								= &db_scr(3,0)
							ENDIF
						ENDIF
						grup_mem = ''
						* _CUROBJ = OBJNUM(m.&cpos_claves[1])	### VFP
						SET_CUROBJ ( OBJNUM2 (cpos_claves[1]) )
						c_edita = .F.
						ON KEY LABEL CTRL+S DO nwhotkeys WITH "SIGUIENTE"
						ON KEY LABEL CTRL+A DO nwhotkeys WITH "ANTERIOR"
					ENDIF
				ELSE
					= MSG('No se puede Borrar porque el registro no existe.')
					* _CUROBJ = gs_num_cpo		### VFP
					SET_CUROBJ ( gs_num_cpo )
					retorno = .F.
				ENDIF
			ELSE
				* _CUROBJ = gs_num_cpo	### VFP
				SET_CUROBJ ( gs_num_cpo )
			ENDIF
		ELSE
			retorno = .F.
		ENDIF

	CASE m.campo_ctrl = 'CANCELA'
		KEYBOARD '{shift+f12}' CLEAR
		WAIT WIND ''
		
		PUSH KEY
		= DEAC_TEC()
		IF TYPE ('conf_cancela') <> 'L' OR conf_cancela
			if c_nuevo
				retorno = sino ("� Cancela Agregar un nuevo registro ?")
				* ### C/S
				* Al dar de alta una clave y cancelar, la vista queda nula (sin registros), 
				* lo cual no sucede con xBase. Para SQL la mando al �ltimo registro
*!*					IF _SQL_FLAG
*!*						DBGOBOTTOM (tab_mae)
*!*					ENDIF
			else
				retorno = sino ("� Cancela la Edici�n ?")
			endif			
		ELSE
			retorno = .T.
		ENDIF
		POP KEY		
		IF retorno
			vf_cancela = 'cancela'
			retorno = &vf_cancela()
			IF retorno
				IF gl_cons
					=&db_scr(3,2)
				ELSE
					=&db_scr(3,0)
				ENDIF
				IF c_nuevo
					* _CUROBJ = OBJNUM(m.nuevo)		&& ### VFP
					* SET_CUROBJ ( OBJNUM2 ("nuevo"))	&& XXX : No corresponde foco para toolbar
					SET_CUROBJ ( OBJNUM2 (cpos_claves[1]) )
					c_nuevo = .f.
				ELSE
					* _CUROBJ = OBJNUM(m.&cpos_claves[1])	### VFP
					SET_CUROBJ ( OBJNUM2 (cpos_claves[1]) )
					c_edita = .f.
				ENDIF
				ON KEY LABEL CTRL+S DO nwhotkeys WITH "SIGUIENTE"
				ON KEY LABEL CTRL+A DO nwhotkeys WITH "ANTERIOR"
			ELSE
				retorno = .F.
			ENDIF
		ELSE
			* _CUROBJ = gs_num_cpo		### VFP
			SET_CUROBJ ( gs_num_cpo )
			retorno = .F.
		ENDIF

	CASE m.campo_ctrl = 'SALE'
		KEYBOARD '{shift+f12}' CLEAR
		WAIT WIND ''
		
		IF TYPE ('conf_sale') <> 'L' OR conf_sale
			retorno = sino ("� Desea salir del proceso ?")
		ELSE
			retorno = .T.
		ENDIF

		IF retorno
			vf_sale = 'sale'
			retorno = &vf_sale()
			IF retorno
				=deac_tec()	
				ON KEY LABEL f4 DO VerAmbien
		 		CLEAR READ
				* CLEAR WINDOWS ALL
				* WAIT WIND "Unload"
		 		* _FORM_ACT.Release
				retorno = .T.
		 	ENDIF
		ELSE
			* _CUROBJ = gs_num_cpo		### VFP
			SET_CUROBJ ( gs_num_cpo )
		ENDIF

	CASE m.campo_ctrl = '_GS_AYABM'
		path_ay_pant = _efimuni + 'BIN\ABM_GRAL.HLP'
		IF FILE(path_ay_pant) 		
			DEFI WIND vent_ayuda FROM 3,5 TO 22,74 TITLE ' Ayuda general del ABMC ' FOOT ' Para moverse en la ayuda utilice:   PageUp PageDown '
			MODI FILE (path_ay_pant) WIND vent_ayuda NOEDIT
			RELEASE WIND vent_ayuda
			retorno = .T.
		ELSE
			retorno = .F.
		ENDIF
		* _CUROBJ = OBJNUM (_GS_AYABM)	### VFP
		SET_CUROBJ ( OBJNUM2 ("_GS_AYABM") )

	CASE m.campo_ctrl = 'SIGUIENTE'
		PRIVATE ant_tecla
		ant_tecla = 0
		IF LASTKEY () <> 19
			* _CUROBJ = OBJNUM(m.siguiente)	### VFP
			SET_CUROBJ ( OBJNUM2 ("siguiente") )
		ELSE
			ant_tecla = 19
		ENDIF

		WAIT CLEAR
		retorno = .T.
		SELECT (tab_mae)
*		= &vf_busca_clave ()
		
		IF EOF()
			GO BOTT
			IF EOF()
*!*			IF DBEOF()
*!*				DBGOBOTTOM()
*!*				IF DBEOF()
				WAIT WINDOW 'Est� en el �ltimo registro.' NOWAIT
				retorno = .F.
			ELSE
				retorno = &db_scr(3,2)
			ENDIF
		ELSE
			SKIP 1
			IF EOF()
				SKIP -1
*!*				DBSKIP(1)
*!*				IF DBEOF()
*!*					DBGOBOTTOM()
				WAIT WINDOW 'Est� en el �ltimo registro.' NOWAIT
			ENDIF
			retorno = &db_scr(3,2)
		ENDIF

	CASE m.campo_ctrl = 'ANTERIOR'
		PRIVATE ant_tecla
		ant_tecla = 0

		SELECT (tab_mae)
		IF LASTKEY () <> 1
			* _CUROBJ = OBJNUM(m.anterior)	### VFP
			SET_CUROBJ ( OBJNUM2 ("anterior") )
		ELSE
			ant_tecla = 1
		ENDIF

		WAIT CLEAR
		retorno = .T.
*		= &vf_busca_clave ()

		IF BOF()
			GO TOP
			IF BOF()
*!*			IF DBBOF()
*!*				DBGOTOP()
*!*				IF DBBOF()
				WAIT WINDOW 'Est� en el primer registro.' NOWAIT
				retorno = .F.
			ELSE
				retorno = &db_scr(3,2)
			ENDIF
		ELSE
			SKIP -1
			IF BOF()
				GO TOP
*!*				DBSKIP (-1)
*!*				IF DBBOF()
*!*					DBGOTOP()
				WAIT WINDOW 'Est� en el primer registro.' NOWAIT
			ENDIF
			retorno = &db_scr(3,2)
		ENDIF

	CASE m.campo_ctrl = 'PRIMERO'
		SELECT (tab_mae)
		* _CUROBJ = OBJNUM(m.primero)	### VFP
		SET_CUROBJ ( OBJNUM2 ("primero") )

		GO TOP
		IF BOF()
*!*			DBGOTOP()
*!*			IF DBBOF()
			retorno = .F.
		ELSE
			retorno=&db_scr(3,2)
		ENDIF

	CASE m.campo_ctrl = 'ULTIMO'
		SELECT (tab_mae)
		* _CUROBJ = OBJNUM(m.ultimo)	### VFP
		SET_CUROBJ ( OBJNUM2 ("ultimo") )
		GO BOTTOM
		IF EOF()
*!*			DBGOBOTTOM()
*!*			IF DBEOF()
			retorno = .F.
		ELSE
			retorno=&db_scr(3,2)
		ENDIF

	CASE m.campo_ctrl = 'BUSCA'
		IF TYPE ('hay_busca') <> 'L' OR ! hay_busca
			retorno = nwbusca()
		ELSE
			vf_busca_part= 'busca_part'
			retorno = &vf_Busca_Part ()
		ENDIF
	
		* _CUROBJ = OBJNUM(m.busca)	### VFP
		SET_CUROBJ ( OBJNUM2 ("busca") )
		if retorno
			retorno = &db_scr(3,2)
		ENDIF
		
	CASE m.campo_ctrl = 'ORDEN'
		* Corregido por RODRIGO el 15/02/95 
		SET ORDER TO (tag_name[m.orden]) IN (tab_mae)
		retorno = .T.
	
	CASE m.campo_ctrl = 'ACTIWIN'
		ACTIVATE WINDOW &vent_ppal
		* _CUROBJ = OBJNUM(m.&cpos_clave[1])	### VFP
		* _CUROBJ = OBJNUM(m.&cpos_claves[1])	### VFP
		SET_CUROBJ ( OBJNUM2 (cpos_claves[1]) )
		retorno = .T.
	ENDCASE


	#IFDEF TOOLBAR_CONTROL
		* _VENT_CTRL.Refresh
		NWACT_CTRL()
	#ELSE
		SHOW GETS WINDOW controls
	#ENDIF
	
	RETURN retorno
	

*************************************************
FUNCTION nwacti_tecl
*************************************************
	PRIVATE vf_val_cpos

	IF gl_cons
*		ON KEY LABEL F3     DO val_gral  WITH (UPPER (VARREAD()))
		* IF EMPTY(_SQL_DB)
*!*			IF EMPTY(_SQL_FLAG)
			ON KEY LABEL F2     DO nwhotkeys WITH "F2F3"
*!*			ENDIF
		ON KEY LABEL F3     DO nwhotkeys WITH "F2F3"
		ON KEY LABEL CTRL+S DO nwhotkeys WITH "SIGUIENTE"
		ON KEY LABEL CTRL+A DO nwhotkeys WITH "ANTERIOR"
	ENDIF
	
	IF (gl_modi AND gl_cons) OR gl_alta
    ON KEY LABEL f5 DO nwhotkeys with "GRABA"
  ENDIF

  IF gl_baja AND gl_cons
    ON KEY LABEL f6 DO nwhotkeys with "BORRA"
  ENDIF

	ON KEY LABEL f4 DO VerAmbien
	

*************************************************
FUNCTION nwhotkeys
*************************************************
	PARAMETERS boton
	
	PUSH KEY
	ON KEY LABEL F5 DO deac_tec with .t.
	ON KEY LABEL F6 DO deac_tec with .t.
	ON KEY LABEL F3 DO deac_tec with .t.
	ON KEY LABEL F2 DO deac_tec with .t.
	ON KEY LABEL CTRL+A DO deac_tec with .t.
	ON KEY LABEL CTRL+S DO deac_tec with .t.
	ON KEY LABEL F4 DO deac_tec with .t.

	PRIVATE retorno
	retorno = .T.

	DO CASE
	CASE boton = "F2F3"
		* ###> Foco 
		* ### VFP : Mejor no habilitar 2 instancias concurrentes del Valid del campo,
		*           fuerzo la validaci�n al perder el foco del campo
		
		retorno = val_gral(VARREAD())
		* _Screen.ActiveForm.AbreF3 = .T.
		* KEYBOARD "{ENTER}" PLAIN
	CASE boton = "GRABA"
		retorno = whe_gral("GRABA")
		IF retorno
			retorno = nwval_ctrl("GRABA")
		ENDIF
	CASE boton = "BORRA"
		retorno = nwval_ctrl("BORRA")
	CASE boton = "SIGUIENTE"
		retorno = nwval_ctrl("SIGUIENTE")
	CASE boton = "ANTERIOR"
		retorno = nwval_ctrl("ANTERIOR")
	ENDCASE

	POP KEY

	IF retorno
		ON KEY LABEL CTRL+S DO nwhotkeys WITH "SIGUIENTE"
		ON KEY LABEL CTRL+A DO nwhotkeys WITH "ANTERIOR"
	ENDIF
		
	RETURN retorno


*************************************************
FUNCTION nwbusca
*************************************************

	PRIVATE retorno

	PUSH KEY
	= DEAC_TEC()

	retorno = .T.
	SELECT (tab_mae)
  UNLOCK
  *	verif completar el browlist con los .... campos de la pantalla
	*	retorno = brow_list

#IFNDEF FORM_BROWSE
	IF EMPTY(ORDER())
		ON KEY LABEL ENTER KEYBOARD '{CTRL+W}'
	ELSE
		_jexitkey = 13
		_JDBLCLICK= -1
		= jkeyinit("U", "", " Buscando: ", "")
	ENDIF

	IF TYPE ('expr_busca') <> 'C' OR EMPTY (expr_busca)
		BROWSE WINDOW vent_busca NOEDIT NOAPPEND NODELETE TIMEOUT 600
	ELSE
		BROWSE FIELDS &expr_busca WINDOW vent_busca NOEDIT ;
			NOAPPEND NODELETE TIMEOUT 600
	ENDIF

	IF EMPTY(ORDER())
		ON KEY LABEL ENTER
	ELSE
		=jkeycanc()
*		RELEASE LIBRARY jkey
	ENDIF
#ELSE
	IF TYPE ('expr_busca') <> 'C' OR EMPTY (expr_busca)
		DO FORM BROWLIS2
	ELSE
		DO FORM BROWLIS2 WITH "", m.expr_busca
	ENDIF
#ENDIF

	POP KEY

	RETURN retorno
	*
	

*** ### C/S


* Funci�n LASTKEY modificada para soporte de F3
FUNCTION LASTKEY2

*!*	IF TYPE ("_Screen.ActiveForm.AbreF3")=="L" AND _Screen.ActiveForm.AbreF3
*!*		RETURN -2
*!*	ELSE
*!*		RETURN LASTKEY()
*!*	ENDIF

RETURN LASTKEY()


* Referencias a funciones de NETWARE.LIB
FUNCTION RLLBACK

* RETURN (1)

LOCAL m.alias, m.area
PRIVATE m.arr_hnd

*!*	IF ! _SQL_FLAG
	IF TXNLEVEL() > 0
		ROLLBACK
	ENDIF
*!*	ELSE
*!*		DECLARE m.arr_hnd [1]
*!*		STORE -1 TO arr_hnd

*!*		* SQLSETPROP (_SQL_HND, "Transaction", 1)		&& // Transacciones a modalidad autom�tica
*!*		* SQLROLLBACK (_SQL_HND)		&& Anulo la transacci�n de la conexi�n base
*!*		* SQLEXEC(_SQL_HND, [ROLLBACK TRANSACTION])		### CNX
*!*		SQL_EXEC(_SQL_HND, [ROLLBACK TRANSACTION])
*!*		
*!*		* Desenlazo las sesiones de datos
*!*		FOR m.area = 1 TO 250
*!*			m.alias = ALIAS (m.area)
*!*			IF !EMPTY(m.alias) AND TABLA_SQL (m.alias)
*!*				SQL_BIND (m.alias, .T.)
*!*			ENDIF
*!*		NEXT

*!*		RELEASE _SQL_SESSION
*!*	ENDIF

RETURN (1)


FUNCTION COMMIT

LOCAL m.alias, m.area
PRIVATE m.arr_hnd

* RETURN (1)

*!*	IF ! _SQL_FLAG
	IF TXNLEVEL() > 0
		END TRANSACTION
	ENDIF
*!*	ELSE
*!*		DECLARE m.arr_hnd [1]
*!*		STORE -1 TO arr_hnd

*!*		* SQLSETPROP (_SQL_HND, "Transaction", 1)		&& // Transacciones a modalidad autom�tica
*!*		* SQLCOMMIT (_SQL_HND)	&& Confirmo la transacci�n de la conexi�n base
*!*		* SQLEXEC (_SQL_HND, [ COMMIT TRANSACTION ])	&& Confirmo la transacci�n de la conexi�n base		### CNX
*!*		SQL_EXEC (_SQL_HND, [ COMMIT TRANSACTION ])	&& Confirmo la transacci�n de la conexi�n base

*!*		* Desenlazo las sesiones de datos
*!*		FOR m.area = 1 TO 250
*!*			m.alias = ALIAS (m.area)
*!*			IF !EMPTY(m.alias) AND TABLA_SQL (m.alias)
*!*				SQL_BIND (m.alias, .T.)
*!*			ENDIF
*!*		NEXT

*!*		RELEASE _SQL_SESSION
*!*	ENDIF

RETURN (1)


FUNCTION BEGIN_TRAN

=BEGINTRAN()

RETURN


FUNCTION BEGINTRAN

LOCAL m.area, m.alias
PRIVATE m.arr_hnd

* RETURN 
*!*	IF ! _SQL_FLAG
	IF TXNLEVEL() > 0
		=MSG("Atenci�n: Ya est� dentro de una transacci�n ("+PROGRAM(2)+"). Informe a SICO")
	ELSE
		BEGIN TRANSACTION
	ENDIF
*!*	ELSE
*!*		DECLARE m.arr_hnd [1]
*!*		STORE -1 TO arr_hnd

*!*		* Enlazo las sesiones de datos
*!*		FOR m.area = 1 TO 250
*!*			m.alias = ALIAS (m.area)
*!*			IF !EMPTY(m.alias) AND TABLA_SQL (m.alias)
*!*				SQL_BIND (m.alias)
*!*			ENDIF
*!*		NEXT

*!*		* SQLSETPROP (_SQL_HND, "Transaction", 2)		&& // Transacciones a modalidad manual
*!*		* SQLEXEC (_SQL_HND, [BEGIN TRANSACTION])		&& Inicio la transacci�n de la conexi�n base		### CNX
*!*		SQL_EXEC (_SQL_HND, [BEGIN TRANSACTION])		&& Inicio la transacci�n de la conexi�n base
*!*	ENDIF

RETURN



FUNCTION TRANCOUNT

LOCAL m.sel, m.ret

m.ret = 0

*!*	IF ! _SQL_FLAG
	m.ret = TXNLEVEL()
*!*	ELSE
*!*		m.sel = SELECT()
*!*		* IF SQLEXEC (_SQL_HND, [SELECT @@TRANCOUNT AS trancount], "sql_tmp") > 0		### CNX
*!*		IF SQL_EXEC (_SQL_HND, [SELECT @@TRANCOUNT AS trancount], "sql_tmp") > 0
*!*			m.ret = sql_tmp.trancount
*!*		ENDIF
*!*		SELECT (m.sel)
*!*	ENDIF

RETURN (m.ret)



* Referencias a funciones de JKEY.LIB
FUNCTION jkeycanc()
RETURN

FUNCTION jkeyinit (xPar1, xPar2, xPar3, xPar4, xPar5)
RETURN .T.



* Agregados a #INSERTS de los screens
PROCEDURE I_NWSETUP
* WAIT WINDOW PROGRAM()
RETURN

PROCEDURE I_NWCLEANUP
* WAIT WINDOW PROGRAM()
RETURN

PROCEDURE I_ACTICPOS
* WAIT WINDOW PROGRAM()
RETURN

PROCEDURE I_LCLEAN
* WAIT WINDOW PROGRAM()
RETURN

PROCEDURE I_VECT_EST
* WAIT WINDOW PROGRAM()
RETURN

PROCEDURE I_DBSCR1
* WAIT WINDOW PROGRAM()
RETURN

PROCEDURE I_DBSCR2
* WAIT WINDOW PROGRAM()
RETURN

PROCEDURE I_DBSCR3
* WAIT WINDOW PROGRAM()
RETURN

PROCEDURE I_DBSCR4
* WAIT WINDOW PROGRAM()
RETURN



***********************************************************
* Funciones para soporte VFP

* Retorna el n�mero de form dado su nombre
FUNCTION FormID (nombre)

LOCAL id, retorna

m.nombre = UPPER(m.nombre)
m.retorna = 0
FOR m.id = 1 TO _Screen.FormCount
	IF UPPER(_Screen.Forms(m.id).Name)==m.nombre
		m.retorna = m.id
		EXIT
	ENDIF
NEXT

RETURN (m.retorna)



* Oculta toolbars
FUNCTION OCUL_TBARS

PRIVATE toolbars, toolbar
DIMENSION toolbars[11]

m.toolbars[01] = TB_STANDARD_LOC
m.toolbars[02] = TB_LAYOUT_LOC
m.toolbars[03] = TB_QUERY_LOC
m.toolbars[04] = TB_VIEWDESIGNER_LOC
m.toolbars[05] = TB_COLORPALETTE_LOC
m.toolbars[06] = TB_FORMCONTROLS_LOC
m.toolbars[07] = TB_DATADESIGNER_LOC
m.toolbars[08] = TB_REPODESIGNER_LOC
m.toolbars[09] = TB_REPOCONTROLS_LOC
m.toolbars[10] = TB_PRINTPREVIEW_LOC
m.toolbars[11] = TB_FORMDESIGNER_LOC

FOR m.toolbar = 1 TO ALEN(m.toolbars,1)
   IF WEXIST(m.toolbars[m.toolbar,1]) AND ;
      WVISIBLE(m.toolbars[m.toolbar,1]) 

      HIDE WINDOW (m.toolbars[m.toolbar,1])
   ENDIF
NEXT

RETURN



* Convierte una expresi�n BROWSE FIELDS a un grid
* Ej:
* 	'codif:H="COD.", desc_cod:H="DESCRIPCION"'
*

FUNCTION BROW_A_GRID (m.expr_brow, m.obj_grid)

LOCAL m.posi1, m.posi2, m.campo, m.en_string, m.caract, m.brow_var, m.brow_val
LOCAL m.brow_R, m.brow_A, m.brow_V, m.brow_P, m.brow_B, m.brow_H, m.brow_W
LOCAL m.nro_col, m.ancho, m.factor, m.ancho_max

m.nro_col = 0
m.ancho   = 0
m.obj_grid.ColumnCount = 0

m.expr_brow = STRTRAN(m.expr_brow, CHR(9), " ")

DO WHILE !EMPTY (m.expr_brow)
	STORE "" TO m.brow_R, m.brow_A, m.brow_V, m.brow_P, m.brow_B, m.brow_H, m.brow_W
	
	m.posi1 = AT(":", m.expr_brow)
	IF EMPTY(m.posi1)
		m.posi1 = LEN(m.expr_brow)+1
	ENDIF
	m.campo = ALLTRIM (LEFT(m.expr_brow, m.posi1-1))

	m.posi2 = AT("=", m.campo)		&& Elimino expresi�n izq. en campos calculados
	IF m.posi2 > 0
		m.campo = SUBSTR(m.campo, m.posi2+1)
	ENDIF

	m.expr_brow = SUBSTR(m.expr_brow, m.posi1)
	m.en_string = ""
	m.brow_var  = ""
	m.brow_val  = ""

	m.posi1 = 1
	DO WHILE m.posi1 <= LEN(m.expr_brow)+1
	
		m.posi2 = m.posi1+1
		m.caract = SUBSTR(m.expr_brow, m.posi1, 1)
		
		DO CASE
		CASE m.caract $ [()]	&& ###> Foco
			* Trato par�ntesis como cadena
			IF m.caract=="("
				m.en_string = m.en_string + m.caract
			ELSE
				m.en_string = LEFT(m.en_string, LEN(m.en_string)-1)
			ENDIF
			
			IF !EMPTY(m.brow_val)
				m.brow_val = m.brow_val + m.caract
			ENDIF
		CASE m.caract $ ['"]
			* Abro o cierro cadena
			DO CASE
			CASE RIGHT(m.en_string,1)==m.caract
				* Cierro cadena
				m.en_string = LEFT(m.en_string, LEN(m.en_string)-1)
			CASE !EMPTY(m.en_string)
				* Ya estoy en cadena, sigo
			OTHERWISE
				* Abro
				m.en_string = m.en_string + m.caract
			ENDCASE
			
			IF !EMPTY(m.brow_val)
				m.brow_val = m.brow_val + m.caract
			ENDIF
		CASE !EMPTY(m.en_string)
			* Estoy en cadena, nada
			IF !EMPTY(m.brow_val)
				m.brow_val = m.brow_val + m.caract
			ENDIF
		CASE m.caract==":"
			IF !EMPTY(m.brow_var)
				m.brow_var = ALLTRIM(SUBSTR(m.brow_var,2))
				m.brow_&brow_var = m.brow_val
			ENDIF
		
			m.brow_var = ":"
			m.brow_val = ""
			* IF SUBSTR (m.expr_brow,2,1) $ "0123456789"
			* 	m.brow_val = "0"
			* ENDIF		
		CASE m.caract $ "0123456789" AND EMPTY (m.brow_val) AND m.brow_var==":"
			m.brow_var = ":A"
			m.brow_val = m.caract

		CASE m.caract=="=" AND !EMPTY(m.brow_var)
			m.brow_val = ":"
		
		CASE m.caract=="," OR m.posi1 > LEN(m.expr_brow)
			IF !EMPTY(m.brow_var)
				m.brow_var = ALLTRIM(SUBSTR(m.brow_var,2))
				m.brow_&brow_var = m.brow_val
			ENDIF

			* Termino campo actual
			* ? m.campo
			* ? "		m.brow_R : ", m.brow_R
			* ? "		m.brow_A : ", m.brow_A
			* ? "		m.brow_V : ", m.brow_V
			* ? "		m.brow_P : ", m.brow_P
			* ? "		m.brow_B : ", m.brow_B
			* ? "		m.brow_H : ", m.brow_H
			* ? "		m.brow_W : ", m.brow_W
			* ? REPLICATE("-", 40)

			m.brow_H = SUBSTR(m.brow_H,2)
			
			m.nro_col = m.nro_col+1
			WITH m.obj_grid
				IF !EMPTY(m.brow_H)
					m.brow_H = EVAL(m.brow_H)
				ENDIF

				* .AddColumn(m.nro_col)
				.AddObject("Column" + ALLTRIM (STR (m.nro_col)), "BrowColumn")
				.Columns (m.nro_col).ControlSource = m.campo
				.Columns (m.nro_col).Header1.Caption = m.brow_H
				.Columns (m.nro_col).ReadOnly  = .T.
				* .Columns (m.nro_col).Resizable = .F.
				.Columns (m.nro_col).Movable   = .F.

				IF EMPTY(VAL(m.brow_A))
					m.brow_A = FSIZE (m.campo)
					* IF EMPTY(m.brow_A)
					* 	m.brow_A = XLEN (EVAL(m.campo))
					* ENDIF
					DO CASE
					CASE EMPTY(m.brow_A)
						m.brow_A = XLEN (EVAL(m.campo))
					CASE m.brow_A==8
						* si es un date paso a 10
						IF TYPE(m.campo)=="D"
							m.brow_A = 10
						ENDIF
					ENDCASE
				ELSE
					m.brow_A = VAL(m.brow_A)
				ENDIF
				
				m.brow_A = MAX(m.brow_A, LEN(m.brow_H))
				.Columns (m.nro_col).Width = m.brow_A*7.5+4
				m.ancho = m.ancho + .Columns (m.nro_col).Width
			ENDWITH
			
			m.expr_brow = SUBSTR(m.expr_brow, m.posi1+1)
			EXIT
		OTHERWISE
			* Acumulo
			DO CASE
			CASE !EMPTY(m.brow_val)
				m.brow_val = m.brow_val+m.caract
			CASE !EMPTY(m.brow_var)
				m.brow_var = m.brow_var+m.caract
			ENDCASE
		ENDCASE
	
		m.posi1 = m.posi2
	ENDDO
ENDDO

WITH m.obj_grid
	* m.ancho = m.ancho+30	&& De la scrollbar
	m.ancho = m.ancho + IIF(.ScrollBars <> 0, 30, 14)	&& De la scrollbar
	m.ancho = MAX(m.ancho,180)
	
	* .Parent.Width = MIN (m.ancho+.Left*2,590)
	.Parent.Width = MIN (m.ancho + .Left * 2, _Screen.Width - 50)
	.Parent.Width = MAX (.Parent.Width, 280)

	* Si no entra todo en la pantalla achico un poco las columnas
	m.ancho_max = .Parent.Width - .Left * 2 - IIF(.ScrollBars <> 0, 10, 3) 	&& De la scrollbar
	IF m.ancho > m.ancho_max
		m.factor = m.ancho_max / m.ancho
		FOR m.nro_col = 1 TO .ColumnCount
			.Columns (m.nro_col).Width = .Columns (m.nro_col).Width * m.factor
		NEXT
		m.ancho = m.ancho_max + IIF(.ScrollBars <> 0, 10, 3)		&& Scrollbar
	ENDIF
	
	.Parent.Resize

	.Width = m.ancho
	.SetAll("Visible", .T.)
ENDWITH

RETURN


* Retorna el largo de un par�metro
FUNCTION XLEN (m.par)

LOCAL m.tipo

m.tipo = TYPE("m.par")

DO CASE
CASE m.tipo=="C"
	RETURN (LEN(m.par))
CASE m.tipo=="D"
	RETURN (10)		&& dd/mm/aaaa
CASE m.tipo=="N"
	RETURN (10)		&& 9999999.99
OTHERWISE
	RETURN (1)
ENDCASE

RETURN



* AT extendido para evitar b�squeda dentro de cadenas de texto
*!*	FUNCTION XAT (m.find, m.str)

*!*	LOCAL m.arr, m.pos, m.cnt
*!*	DIMENSION m.arr[1]

*!*	m.str = PARSE_LIT (m.str, @m.arr)
*!*	m.pos  = AT(m.find, m.str)

*!*	IF m.pos > 0
*!*		m.str = LEFT (m.str, m.pos)
*!*		m.pos  = AT("@@@", m.str)
*!*		m.cnt  = 0
*!*		DO WHILE m.pos > 0
*!*			m.cnt = m.cnt + 1
*!*			m.str = STUFF (m.str, m.pos, 3, m.arr [m.cnt])
*!*			m.pos  = AT("@@@", m.str)
*!*		ENDDO
*!*		m.pos = LEN (m.str)
*!*	ENDIF

*!*	RETURN m.pos



* Retorna el VAL del par�metro eliminando todos los caracteres no num�ricos
FUNCTION VAL_NUM (m.str)

LOCAL m.pos, m.ch

m.pos = 1
DO WHILE m.pos <= LEN(m.str)
	m.ch = SUBSTR(m.str, m.pos, 1)
	
	DO CASE
	CASE m.ch $ "0123456789."
		m.pos = m.pos + 1
	OTHERWISE
		m.str = STUFF(m.str, m.pos, 1, "")
	ENDCASE
ENDDO

RETURN VAL(m.str)


* Retorna el n�mero enviado como un string completado a la izquierda con "0"s
FUNCTION STRZERO (m.num, m.len)
RETURN PADL(LTRIM(STR(m.num, m.len)), m.len, "0")



* Push y pop de un stack
FUNCTION STACK_PUSH (m.valor)

LOCAL m.pos

IF VARTYPE(_STACK_ARR)=="U"
	PUBLIC _STACK_ARR [2]
	m.pos = 2
ELSE
	m.pos = ALEN(_STACK_ARR, 1) + 1
	DIMENSION _STACK_ARR [m.pos]
ENDIF

_STACK_ARR [m.pos] = m.valor

RETURN



FUNCTION STACK_POP

LOCAL m.ret

m.pos = ALEN(_STACK_ARR, 1)
m.ret = _STACK_ARR [m.pos]

DIMENSION _STACK_ARR [m.pos-1]

RETURN (m.ret)




****
* funcion: Agrega un campo mas a un arreglo de campos <aArray>
*          que se creo con una sentencia afields anteriormente con una base. 
*
* autor: cesar.
*
* 		   
* parametros:   aArray: nombre del arreglo donde se insertara un campo mas.
*				sCamp1: nombre de campo, tipo caracter
*             	sCamp2: tipo del campo , tipo caracter (longitud 1)
*             	nCamp3: longitud del campo, tipo numerico (longitud 1)
*             	nCamp4: longitud de decimales, tipo numerico (longitud 1)
*
* fecha: 17-6-1999
****
PROCEDURE AgregarCampo(aArray,sCamp1,sCamp2,nCamp3,nCamp4)
local i

DIMENSION aArray [ALen (aArray,1) + 1,alen(aArray,2)]  &&redimensionar con una fila mas.

for i=1 to 16							&& 16 columnas se agregan

    aArray (ALen (aArray,1) ,i) = ""	&& rellenar el nuevo campo con espacios vacios
										&& necesarios.	
endfor

&& agregar los valores de nombre de campo,tipo,longitud, decimales.
aArray [ALen (aArray,1),1] = sCamp1		
aArray [ALen (aArray,1),2] = sCamp2
aArray [ALen (aArray,1),3] = nCamp3
aArray [ALen (aArray,1),4] = nCamp4

RETURN 



* Sale de VFP?
FUNCTION SALE_VFP

IF EMPTY (RDLEVEL())
	IF MESSAGEBOX("� Sale de EFIMUNI ?", 4+32, "Consulta")==6
		QUIT
	ENDIF
ELSE
	MESSAGEBOX("Debe salir del programa actual", 48, "Aviso")
ENDIF

RETURN



* Sirve para grabar informaci�n sobre un archivo de .LOG
FUNCTION LOGFILE (m.str, m.file)

LOCAL m.hnd

m.file = IIF(EMPTY(m.file), "OUT.LOG", m.file)

m.hnd = FOPEN(m.file,2)
IF m.hnd < 1
	m.hnd = FCREATE(m.file)
ELSE
	FSEEK (m.hnd, 0, 2)		&& Posiciono sobre el final del archivo
ENDIF

FPUTS (m.hnd, m.str)

FCLOSE (m.hnd)

RETURN


* Retorna el PROGRAM hacia atr�s (0=nivel actual, 1=anterior, etc.)
FUNCTION BPROGRAM (m.nivel, m.cant)

LOCAL m.prog_act, m.prog_niv, m.test_niv, m.retorna

m.prog_act = PROGRAM()
m.nivel = m.nivel+1			&&// Elimino este nivel
m.cant  = IIF(EMPTY(m.cant), 1, m.cant)

m.test_niv = PROGRAM(-1)

m.retorna = ""
m.nivel = MAX (m.test_niv-m.nivel-m.cant+1, 1)

FOR m.prog_niv = m.test_niv TO m.nivel STEP -1
	m.prog_act = PROGRAM(m.prog_niv)
	m.retorna = IIF(EMPTY(m.retorna), "", m.retorna+"/")+m.prog_act
NEXT

RETURN m.retorna




* Formatea una fecha para hacer una b�squeda con LIKE 
FUNCTION DATE_A_LIKE (m.fecha)

LOCAL m.dia, m.mes, m.anio, m.fecha1, m.ret

m.fecha = IIF(VARTYPE(m.fecha)=="D", DTOC(m.fecha), m.fecha)

m.dia  = VAL(SUBSTR(m.fecha, 1, 2))
m.mes  = VAL(SUBSTR(m.fecha, 4, 2))
m.anio = SUBSTR(m.fecha, 7)

IF EMPTY (m.anio)
	m.anio = ""
ELSE
	m.fecha1 = CTOD ("01/01/" + m.anio)		&& Por el siglo
	m.anio = YEAR (m.fecha1)
ENDIF

m.ret = ""
m.ret = m.ret + IIF(!EMPTY(m.dia) , STRZERO(m.dia,2) , "__") + "/"
m.ret = m.ret + IIF(!EMPTY(m.mes) , STRZERO(m.mes,2) , "__") + "/"
m.ret = m.ret + IIF(!EMPTY(m.anio), STRZERO(m.anio,4) , "____")

RETURN (m.ret)



* Retorna la primera variable utilizada en una expresi�n
*  Ej: VAR_EXPR([ STR (nro_inm,5) ])	=> 	"nro_inm"
FUNCTION VAR_EXPR (m.expr)
LOCAL m.acum, m.car

m.acum = ""

FOR m.pos = 1 TO LEN(m.expr)
	m.car = UPPER (SUBSTR(m.expr, m.pos, 1))
	DO CASE
	CASE BETWEEN (m.car, "A", "Z") OR m.car $ "_"
		m.acum = m.acum + m.car
	CASE BETWEEN (m.car, "0", "9") OR m.car $ "_"
		IF !EMPTY(m.acum)
			m.acum = m.acum + m.car
		ENDIF
	CASE m.car=="("
		m.acum = ""
	CASE m.car $ ")+," AND !EMPTY(m.acum)
		EXIT
	ENDCASE
NEXT

RETURN (ALLTRIM (m.acum))




* Devuelve el par�metro enviado en formato string entrecomillado (FORMATO FOX)
* para evaluar con EVAL
*  Ej:
*   QUOTE_STR (DATE())		=> "{01/01/1999}"
*   QUOTE_STR ('1')   		=> "'1'"
*   QUOTE_STR (5)     		=> "5"

FUNCTION QUOTE_STR (m.expr)

LOCAL m.tipo
m.tipo = VARTYPE(m.expr)

DO CASE
CASE m.tipo=="C"
	RETURN ([']+m.expr+['])
CASE m.tipo $ "DT"
	RETURN ([{]+DTOC(m.expr)+[}])
CASE m.tipo=="N"
	RETURN (ALLTRIM(STR(m.expr, 20, 5)))
CASE m.tipo=="L"
	RETURN (IIF(m.expr, [.T.], [.F.]))
ENDCASE
RETURN


* Retorna el par�metro enviado como string
FUNCTION XSTR (m.expr, m.dtoc)

LOCAL m.tipo
m.tipo = VARTYPE(m.expr)

DO CASE
CASE m.tipo=="C"
	RETURN (m.expr)
CASE m.tipo=="D"
	RETURN (IIF(m.dtoc, DTOC(m.expr), DTOS(m.expr)))
CASE m.tipo=="N"
	RETURN (STR(m.expr, 20, 5))
CASE m.tipo=="L"
	RETURN (IIF(m.expr, [.T.], [.F.]))
CASE m.tipo=="X"
	RETURN (SPACE(8))
ENDCASE

RETURN



FUNCTION CTOBIN2 (m.num)

LOCAL m.pos, m.res, m.mult, m.val

m.res = 0
m.mult = 1
FOR m.pos = LEN(m.num) TO 1 STEP -1
	m.val = ASC(SUBSTR(m.num, m.pos, 1)) * m.mult
	m.res = m.res + m.val

	m.mult = m.mult * 256
NEXT

RETURN (m.res)


* Retorna el OBJNUM real de una variable (la funci�n OBJNUM de VFP funciona mal)
FUNCTION OBJNUM2 
PARAMETERS m.var

LOCAL m.ctrl, m.obj

m.var = ALLTRIM (UPPER (m.var))
m.ctrl = AT ("M.", m.var)
IF EMPTY (m.ctrl)
	m.var = "M." + m.var
ENDIF

m.ctrl = 1
m.obj = UPPER (OBJVAR (m.ctrl))
DO WHILE ! EMPTY (m.obj)
	IF m.obj==m.var
		RETURN (m.ctrl)
	ENDIF
	m.ctrl = m.ctrl + 1
	m.obj = UPPER (OBJVAR (m.ctrl))
ENDDO

RETURN (OBJNUM (&var ))



* Funciona como SET VIEW para xBase, en SQL no hace nada
FUNCTION DBSETVIEW (m.view)
SET VIEW TO (m.view)
RETURN


* Definici�n de clases

* Defino la clase para utilizar las columnas en el Browse
DEFINE CLASS BrowColumn AS Column
	PROCEDURE Init
		This.FontName = "Courier New"
		This.FontSize = 8

		This.Header1.FontName = This.FontName
		This.Header1.FontSize = This.FontSize

		This.Text1.FontName = This.FontName
		This.Text1.FontSize = This.FontSize
	ENDPROC
	
	PROCEDURE Resize
		LOCAL m.colwidth, m.leftwidth, m.minwidth, m.cols, m.filtro
		
		m.minwidth = 30 						&& Ancho m�nimo de la columna
		m.cols     = This.Parent.ColumnCount	&& Cantidad de columnas
		m.maxwidth = This.Parent.Width - 16		&& Ancho m�ximo de todas las columnas

		m.colwidth = 0
		m.leftwidth = 0
		m.filtro = (VARTYPE (ThisForm.grdFiltro)=="O")
		
		FOR m.col = 1 TO m.cols
			m.width = This.Parent.Columns (m.col).Width
			m.colwidth = m.colwidth + m.width
			* m.leftwidth = m.leftwidth + IIF(m.col < This.ColumnOrder, m.width, 0)
		NEXT

		* Achico las columnas de la derecha de la actual si corresponde
		m.col = m.cols
		DO WHILE m.colwidth > m.maxwidth AND m.col > This.ColumnOrder
			m.colwidth = m.colwidth - This.Parent.Columns (m.col).Width
			m.width = MAX (m.minwidth, m.maxwidth - m.colwidth)
			m.colwidth = m.colwidth + m.width

			This.Parent.Columns (m.col).Width = m.width
			IF m.filtro
				ThisForm.grdFiltro.Columns (m.col).Width = m.width
			ENDIF
			m.col = m.col - 1
		ENDDO

		* La columna no puede ser menor al ancho m�nimo
		m.colwidth = m.colwidth - This.Width
		m.width = MAX (This.Width, m.minwidth)
		* m.colwidth = m.colwidth + m.width

		* Me paso del ancho m�ximo en todas las columnas?
		IF m.colwidth + m.width > m.maxwidth
			* m.colwidth = m.colwidth - m.width
			* m.width    = m.colwidth + m.width - m.maxwidth
			m.width    = m.width - (m.colwidth + m.width - m.maxwidth)
			* m.colwidth = m.colwidth + m.width
		ENDIF

		m.colwidth = m.colwidth + m.width

		This.Width = m.width
		IF m.filtro
			ThisForm.grdFiltro.Columns (This.ColumnOrder).Width = This.Width
		ENDIF

		IF m.colwidth < m.maxwidth
			m.width = This.Parent.Columns (m.cols).Width + m.maxwidth - m.colwidth
			This.Parent.Columns (m.cols).Width = m.width
			IF m.filtro
				ThisForm.grdFiltro.Columns (m.cols).Width = m.width
			ENDIF
		ENDIF
	ENDPROC
ENDDEFINE
***


*************************
FUNCTION DOS_Val 
*************************
PARAMETERS cadena
PRIVATE numero, punto

m.punto = SET ('POINT')
SET POINT TO '.'
numero = VAL (cadena)
SET POINT TO (m.punto)

RETURN numero