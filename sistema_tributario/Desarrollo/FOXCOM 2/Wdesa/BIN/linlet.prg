*****************************************************
*FUNCTION LinLet
*****************************************************
PARAMETERS texto, num_lin, long_lin
PRIVATE retorno
m.retorno = ' '
DIMENSION renglon(20)
=IniRenglon()
m.pos = m.long_lin
m.ini = 1
m.i   = 1
IF m.pos >= LEN(m.texto)
	m.lin = m.texto
ENDIF
DO WHILE m.pos < LEN(m.texto) AND m.pos > 0 
	m.lin = SUBS(m.texto,m.ini,m.long_lin)
	m.fin = .F.
	DO WHILE !m.fin AND m.pos >0
		IF SUBS(m.lin,m.ini + m.pos - 1,1) = ' '
			m.fin = .T.
			m.lin = SUBS(m.texto,m.ini,m.pos)
			m.ini = m.ini + m.pos
			m.pos = m.long_lin
		ELSE
			IF SUBS(m.texto,m.ini + m.pos,1) = ' '
				m.fin = .T.
				m.lin = SUBS(m.texto,m.ini,m.pos)
				m.ini = m.ini + m.pos + 1
				m.pos = m.long_lin
				 
			ELSE
				m.pos = m.pos - 1
			ENDIF
		ENDIF
	ENDDO
	IF m.fin
		renglon[m.i] = m.lin
		m.i = m.i + 1
	ENDIF
ENDDO
renglon[m.i] = m.lin
RETURN renglon[num_lin]

*****************************************************
FUNCTION IniRenglon
*****************************************************
PRIVATE retorno, aux
m.retorno = .T.
FOR m.aux = 1 TO 10
	renglon[m.aux] = ' '
ENDFOR
RETURN m.retorno
