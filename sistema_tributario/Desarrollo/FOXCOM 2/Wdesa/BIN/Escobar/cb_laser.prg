*************************************************
FUNCTION CB_Laser
*************************************************
* 
* Funcionamiento:
* Recibe como par�metro un nro. de comprobante en formato
* 0100000481573
* Donde 01 es el grupo
* 4815 es el nro. de comprobantes
* 73 es el d�g. verificador
*
* El c�digo de barras I2of5 se arma:
* Chr (33) - Inicial
* Desde el n�mero 00 hasta el 91, se toma de los caracteres
* ASCII 35 a 126 respectivamente. Pero los dem�s est�n representados
* por los siguientes caracteres ASCII:
* 92 = 196
* 93 = 197
* 94 = 199
* 95 = 201
* 96 = 209
* 97 = 214
* 98 = 220
* 99 = 225
* Chr (34) - Final
*
PARAMETERS nro_comp
PRIVATE barras, i, numero
m.nro_comp = m.nro_comp + '0'
m.barras = ''
FOR m.i = 1 TO LEN(m.nro_comp) / 2
	m.numero = Val (SubStr (m.nro_comp, (m.i * 2) - 1, 2))
	IF m.numero <= 91
		m.barras = m.barras + Chr (Val (SubStr (m.nro_comp, (m.i * 2) - 1, 2)) + 35)
	ELSE
		DO CASE
			CASE m.numero = 92
				m.barras = m.barras + Chr (196)
			CASE m.numero = 93
				m.barras = m.barras + Chr (197)
			CASE m.numero = 94
				m.barras = m.barras + Chr (199)
			CASE m.numero = 95
				m.barras = m.barras + Chr (201)
			CASE m.numero = 96
				m.barras = m.barras + Chr (209)
			CASE m.numero = 97
				m.barras = m.barras + Chr (214)
			CASE m.numero = 98
				m.barras = m.barras + Chr (220)
			CASE m.numero = 99
				m.barras = m.barras + Chr (225)
		ENDCASE
	ENDIF
ENDFOR
m.barras = '!' + m.barras + '"'
RETURN m.barras
