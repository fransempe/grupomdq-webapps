************************************************* 
* FUNCTION NTOC
*************************************************
* Autor : Nicol�s 
* Fecha : (15/01/98)
* 
* Funcionamiento: Devuelve una Cadena formateado con la cantidad de
* 				        decimales solicitada.Completa con el caracter
*									especificado.
*
* Ejemplo : N�mero  = 111,1
*				1 :	long    = 5
*						deci		= 0	
*           car     = '0' -> retorno = '' ERROR Menor cant de decimales
*                                         pedidos que los que existen.
*
*				2 :	long    = 5
*						deci		= 2	
*           car     = '0' -> retorno = '11110'
*            								 3 enteros �������� 2 decimales
*
*				3 :	long    = 5
*						deci		= 1	
*           car     = ' ' -> retorno = ' 1111'
*            								  3 enteros �������� 2 decimales
*
* Par�metros: nro    N    : Cadena con el n�mero sin comas o puntos.
*             long   N    : Longitud de la cadena de salida.
*			  			deci   N    : Cantidad de decimales.	  
*            [car]   C(1) : Caracter que completa la cadena. Por defecto
* 													es ' '.
PARAMETERS nro,long,deci,car
PRIVATE retorno,entero,decimal
retorno = 0
IF PARAMETERS() < 3
	=Msg('La cantidad de Par�metros de la Funci�n CTON() es incorrecta.')
ELSE
	IF EMPTY(car)
		car = ' '
	ENDIF
  
	decimal = SUBSTR(STR(nro - INT(nro),deci + 1,deci),2,deci)
	entero  = ALLT(STR(INT(nro)))
	
	IF  deci >= LEN(decimal)
		IF  (long-deci) >= LEN(entero)
			decimal = PADR(decimal,deci,car)
			entero  = PADL(entero,long-deci,car)
			retorno = ALLT(entero) + ALLT(decimal)
		ELSE
			= Msg('La cantidad de caracteres enteros que solicita es menor que la que posee el n�mero. Funci�n NTOC().')		
		ENDIF
	ELSE
		= Msg('La cantidad de decimales que solicita es menor que la que posee el n�mero. Funci�n NTOC().')
	ENDIF
ENDIF
RETURN retorno
