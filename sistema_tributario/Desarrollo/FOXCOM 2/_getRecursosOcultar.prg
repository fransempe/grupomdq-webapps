***********************************************************************************
*	Método _getRecursosOcultar:  
***********************************************************************************
* 	Date:          28/02/2014
* 	Author:        Juan
* 	Design:        Juan
***********************************************************************************
*	Funcionamiento:
*		Busca en la tabla de configuración del sistema el parámetro que indica si
*		se debe ocultar totalmente (no ver desde grilla) uno o varios recursos
***********************************************************************************
PROCEDURE _getRecursosOcultar() as string
	PRIVATE strParametro as String
	PRIVATE strIndex as String
	
	
	IF (!USED ('config')) THEN
		=USET ('asa\config')
	ENDIF
	SELECT config
	SET ORDER TO PRIMARIO
	
		
	strParametro = ''
	strIndex = PADR('RECUR', 8, ' ') + 'WEB_RECUR_NOVER'
	IF (SEEK(strIndex)) THEN
		strParametro = ALLTRIM(config.cont_par)		
	ENDIF
	
	
	RETURN ALLTRIM(strParametro)
ENDPROC