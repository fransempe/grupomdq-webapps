PROCEDURE _actualizarVisita(p_sFecha as String, p_sHora as String, p_sTipoImponible as String, p_nNroImponible as Number) as String
 	PRIVATE sResultado as String
 	PRIVATE dFecha as Date
	PRIVATE sIndex as String


	IF (!USED('web_vis')) THEN
		=UseT('recur\web_vis')
	ENDIF
	SELECT web_vis
	SCATTER MEMVAR BLANK

	dFecha = CTOD(p_sFecha)
	sIndex = DTOC(dFecha, 1) + ALLTRIM(p_sHora) + ALLTRIM(p_sTipoImponible) + STR(p_nNroImponible, 10, 0)
	SET ORDER TO primario
	IF (SEEK(sIndex)) THEN	
		SELECT web_vis
		REPLACE FECHA	 	WITH dFecha
		REPLACE HORA	 	WITH p_sHora
		REPLACE TIPO_IMP 	WITH p_sTipoImponible			
		REPLACE NRO_IMP 	WITH p_nNroImponible			
		REPLACE IMPRIMIO 	WITH 'S'			
		sResultado = 'OK'				
	ELSE	
		sResultado = 'visita inexistente'		
	ENDIF
	
	RETURN sResultado
ENDPROC