PROCEDURE _getDatoByImponible2(p_impTipo as String, p_impNro as number) as String
 	PRIVATE dato as String

	IF (!USED('inmueble')) THEN
		=UseT('recur\inmueble')
	ENDIF	
	IF (!USED('comercio')) THEN
		=UseT('recur\comercio')
	ENDIF
	IF (!USED('vehiculo')) THEN
		=UseT('recur\vehiculo')
	ENDIF
		
	
	dato = ''
	DO CASE 
		CASE UPPER(ALLTRIM(p_impTipo)) = 'I'
			SELECT inmueble
			SET ORDER TO primario IN inmueble
			IF (SEEK(STR(p_impNro, 10, 0), 'inmueble')) THEN
				dato = ''
			ENDIF
			
			
		CASE UPPER(ALLTRIM(p_impTipo)) = 'C'		
			SELECT comercio
			SET ORDER TO primario IN comercio
			IF (SEEK(STR(p_impNro, 10, 0), 'comercio')) THEN						
				dato = ''
			ENDIF
		
		
		CASE UPPER(ALLTRIM(p_impTipo)) = 'V'
			SELECT vehiculo
			SET ORDER TO primario IN vehiculo
			IF (SEEK(STR(p_impNro, 10, 0), 'vehiculo')) THEN
				dato = dato + ALLTRIM(vehiculo.dominio)
			ENDIF
	ENDCASE

	
	RETURN dato
ENDPROC