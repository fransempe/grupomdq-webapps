
PROCEDURE _ObtenerDatosMunicipalidad()
	PRIVATE mDatos as String
	PRIVATE mNombre as String
	PRIVATE mDireccion as String
	PRIVATE mTelefono as String
	PRIVATE mMail as String


	mNombre = ALLTRIM(Get_Para('ASA','PARTIDO'))
	mDireccion = ALLTRIM(Get_Para('ASA','DOMICILIO'))
	mTelefono = ALLTRIM(Get_Para('ASA','NROTELRECWEB'))
	mMail = ALLTRIM(Get_Para('ASA','DIRMAILRECWEB'))


	mDatos = ''
	mDatos = mDatos + Indent(1) + '<MUNICIPALIDAD>' + _S_
	mDatos = mDatos + Indent(2) + '<MUNICIPALIDAD_NOMBRE>' + TRIM(mNombre) + '</MUNICIPALIDAD_NOMBRE>' + _S_
	mDatos = mDatos + Indent(2) + '<MUNICIPALIDAD_DIRECCION>' + TRIM(mDireccion) + '</MUNICIPALIDAD_DIRECCION>' + _S_		
	mDatos = mDatos + Indent(2) + '<MUNICIPALIDAD_TELEFONO>' + TRIM(mTelefono) + '</MUNICIPALIDAD_TELEFONO>' + _S_
	mDatos = mDatos + Indent(2) + '<MUNICIPALIDAD_MAIL>' + TRIM(mMail) + '</MUNICIPALIDAD_MAIL>' + _S_		
	mDatos = mDatos + Indent(1) + '</MUNICIPALIDAD>' + _S_
	
	RETURN TRIM(mDatos)
ENDPROC