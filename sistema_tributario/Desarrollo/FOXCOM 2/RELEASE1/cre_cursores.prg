CREATE CURSOR CompAVenc		(				;
					grupo_comp N(2),		;
					nro_comp N(9),			;
					dv_comp N(2),			;
					venc N(1),				;
					fecha_venc D,			;
					detalle_comp C(60),		;
					tot_ori N(15,2),		;
					tot_actint N(15,2),		;
					tot_iva N(15,2),		;
					tot_comp N(15,2)		)
INDEX ON DTOC(fecha_venc,1) + STR(grupo_comp,2,0) + STR(nro_comp,9,0) TAG primario

CREATE CURSOR CompAVencReng		(			;
					grupo_comp N(2),		;
					nro_comp N(9),			;
					reng_comp N(3),			;
					recurso C(2),			;
					anio N(4),				;
					cuota N(3),				;
					nro_mov N(9)			)
INDEX ON STR(grupo_comp,2,0) + STR(nro_comp,9,0) + STR(reng_comp,3,0) TAG primario

CREATE CURSOR CuotasVenc		(			;
					recurso C(2),			;
					anio N(4),				;
					cuota N(3),				;
					nro_mov N(9),			;
					fecven_mov D,			;
					dsc_ccc C(50),			;
					de_plan C(2),			;
					cond_esp C(60),			;
					imp_ori N(15,2),		;
					imp_actint N(15,2),		;
					imp_iva N(15,2),		;
					imp_tot N(15,2)			)
INDEX ON recurso + STR(anio,4,0) + STR(cuota,3,0) + STR(nro_mov,9,0) TAG primario

CREATE CURSOR RefRecursos		(			;
					recurso C(2),			;
					desc_recurso C(60)		)
INDEX ON recurso TAG primario
