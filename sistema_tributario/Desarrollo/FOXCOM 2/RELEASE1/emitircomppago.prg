PROCEDURE  	EmitirCompPago
PARAMETERS  P_ArchComp

AuxHuboError= .T.

*crear archivo XML
AuxArchComp = AuxArchcomp + Indent(1) + '<IMPONIBLE>' + _S_
	

IF AuxhuboError  
	m.cuit1=0
	m.cuit2=0
	m.nombretitular =''
	m.nombreresppago=''
	
	=FCONTVIN(comprob.tipo_imp,comprob.nro_imp, .F.,.F.@m.cuit,@m.cuit2)
	IF m.cuit1 > 0
		IF SEEK (STR(m.cuit1,10,0),'contrib')
			m.nombretitular= contrib.nomb_cont
		ENDIF
	ENDIF
	IF m.cuit2 > 0
		IF SEEK (STR(m.cuit2,10,0),'contrib')
			m.nombreresppago= contrib.nomb_cont
		ENDIF
	ENDIF
	
	*agregar al archivo xml datos generales de la cuenta
	AuxArchComp = AuxArchcomp + Indent(2) +'<TIPOIMPONIBLE>'+P_archcomp.tipoImponible+'</TIPOIMPONIBLE>'+ _S_
	AuxArchComp = AuxArchcomp + Indent(2) +'<NROIMPONIBLE>'+STR(P_archcomp.NroImponible,10,0)+'</NROIMPONIBLE>'+ _S_
	AuxArchComp = AuxArchcomp + Indent(2) +'<TITULAR>'+ALLTRIM(m.nombretitular)+'</TITULAR>' +_S_
	AuxArchComp = AuxArchcomp + Indent(2) +'<DOMICILIO>'+ALLTRIM(contrib.nomb_calle)+' '+ALLTRIM(contrib.puerta)+' '+ALLT(contrib.piso)+' '+ALLT(contrib.depto)+'</DOMICILIO>'+ _S_
	AuxArchComp = AuxArchcomp + Indent(2) +'<LOCALIDAD>'+'('+ ALLTRIM(contrib.cod_post)+')'+ALLTRIM(contrib.nom_loc)+</LOCALIDAD>' +_S_
	AuxArchComp = AuxArchcomp + Indent(1) + '</IMPONIBLE>' + _S_
	
	DO WHILE NOT EOF ('P_ArchComp')
		IF SEEK( STR(P_Archcomp.grupocomp,2,0) + STR(P_Archcomp.nro_comp,9,0),'comprob')		
			SELECT COMPROB
			AuxArchComp = AuxArchcomp + Indent(1) +'<COMPROBANTES>'+ _S_
			AuxArchComp = + AuxArchcomp + Indent(2) +'<COMPROBANTE>' + _S_
			AuxArchComp = AuxArchcomp +Indent(3) + '<NROCOMP>'+PADL(ALLTRIM(STR(grupo_comp,2,0)),2,'0')+'/'+ ;
						PADL(ALLTRIM(STR(nro_comp,9,0)),9,'0')+'/'+PADL(ALLTRIM(STR(dv_comp,2,0)),2,'0')+'</NROCOMP>'+_S_
			AuxArchComp = AuxArchcomp + Indent(3) +'<FECHAEMISION>'+DTOC(fecemicomp)+'</FECHAEMISION>'+_S_
	    	AuxArchComp = AuxArchcomp + Indent(3) +'<FECHAVENC1>'+DTOC(fec1_comp)+'</FECHAVENC1>'+_S_
	    	AuxArchComp = AuxArchcomp + Indent(3) +'<IMPTOTORIGEN>'+STR(tot_ori,15,2)+'</IMPTOTORIGEN>'+_S_
	    	AuxArchComp = AuxArchcomp + Indent(3) +'<IMPRECARGOS1>'+STR(tot_act+tot_int,15,2)+'</IMPRECARGOS1>'+_S_
	    	AuxArchComp = AuxArchcomp + Indent(3) +'<IMPTOTAL1>'+STR(tot_comp,15,2)+'</IMPTOTAL1>'+_S_
	    	AuxArchComp = AuxArchcomp + Indent(3) +'<FECHAVENC2>'+DTOC(fec2_comp)+'</FECHAVENC2>'+_S_
	    	AuxArchComp = AuxArchcomp + Indent(3) +'<IMPRECARGOS2>'+STR(tot2_act+tot2_int,15,2)+'</IMPRECARGOS2>'+_S_
	    	AuxArchComp = AuxArchcomp + Indent(3) +'<IMPTOTAL2>'+STR(tot_ori+tot2_act+tot2_int,15,2)+'</IMPTOTAL2>'+_S_
	    	AuxArchComp = AuxArchcomp + Indent(3) +'<FECHAVENC3>'+DTOC(fec3_comp)+'</FECHAVENC3>'+_S_
	    	AuxArchComp = AuxArchcomp + Indent(3) +'<IMPRECARGOS3>'+STR(tot3_act+tot3_int,15,2)+'</IMPRECARGOS3>'+_S_
	    	AuxArchComp = AuxArchcomp + Indent(3) +'<IMPTOTAL3>'+STR(tot_ori+tot3_act+tot3_int,15,2)+'</IMPTOTAL3>'+_S_
		
			SET KEY TO STR(comprob.grupo_comp,2,0)+ STR(comprob.nro_comp,9,0) IN  comp_ren
			GO TOP IN comp_ren
			DO WHILE NOT EOF('comp_ren')
				SELECT comp_ren
				IF SEEK (recurso+conc_cc,'concs_cc')
					AuxDescConc= concs_cc.dsc_ccc
				ENDIF
				AuxCodPlan = ''
				IF comp_ren.nro_plan > 0
					IF SEEK(comprob.tipo_imp+comprob.nro_imp+comp_ren.nro_plan,'pla_imp')
						AuxCodPlan=pla_imp.cod_plan
					ENDIF
				ENDIF
				AuxArchComp = AuxArchcomp +Indent(3)+'<RENGLONES>'+_S_
				AuxArchComp = AuxArchcomp + Indent(4)++'<RENGLON>'+_S_
				AuxArchComp = AuxArchcomp + Indent(5)+'<RECURSO>'+comp_ren.recurso+'</RECURSO>'+_S_
				AuxArchComp = AuxArchcomp +Indent(5)+'<CUOTA>'+comp_ren.cuota+'</CUOTA>'+_S_
				AuxArchComp = AuxArchcomp +Indent(5)+'<ANIO>'+STR(comp_ren.anio,4,0)+'</ANIO>'+_S_
				AuxArchComp = AuxArchcomp +Indent(5) +'<CONCEPTO>'+ALLTRIM(AuxDescConc)+'</CONCEPTO>'+_S_
				AuxArchComp = AuxArchcomp +Indent(5) +'<CODPLAN>'+ALLTRIM(AuxCodPlan)+'</CODPLAN>'+_S_
				AuxArchComp = AuxArchcomp +Indent(5) +'<IMPORIGENRENG>'+STR(comp_reng.imp_reng,15,6)+'</IMPORIGENRENG>'+_S_
				AuxArchComp = AuxArchcomp +Indent(5) +'<IMPRECARGOSRENG>'+STR(comp_ren.actualiza1+comp_ren.intereses1,13,2)+'</IMPRECARGOSRENG>'+_S_
				AuxArchComp = AuxArchcomp +Indent(5) +'<IMPTOTALRENG>'+STR(comp_ren.imp_reng+comp_ren.actualiza1+comp_ren.intereses1,13,2)+'</RECURSO>'+_S_
		    	AuxArchComp = AuxArchcomp +Indent(4) + '</RENGLON>'+_S_
				AuxArchComp = AuxArchcomp +Indent(3) + '</RENGLONES>'+_S_
				AuxArchComp = AuxArchcomp +Indent(2) + '</COMPROBANTE>'+_S_
				AuxArchComp = AuxArchcomp +Indent(1) + '</COMPROBANTES>'+_S_
				SKIP IN comp_ren
			ENDDO
			SET KEY TO '' IN comp_ren
		ENDIF
		SKIP IN P_ArchComp
	ENDDO
ENDPROC		



