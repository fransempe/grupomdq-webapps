*************************************************
FUNCTION Mens_Avi
*************************************************
* Funcionamiento:
* Se le pasan como par�metros el nombre del sistema del programa
* que la llam�, y el c�d. del aviso. Seg�n la gravedad del aviso
* muestra un mensaje sencillo � con detalles.
* 
* Par�metros:
* 	m_sis  = nombre del sistema.
* 	m_avis = c�digo del aviso.
* 
PARAMETERS m_sis, m_avis
PRIVATE retorno, m_alias, m_reg_ant, m_mensa, w_descr, w_dsc, m.reto

retorno   = .T.
	
RETURN m.reto


*************************************************
FUNCTION msg
*************************************************
* Funcionamiento:
* 	Muestra un mensaje dentro de una ventana y para salir se debe 
*	 presionar una tecla
* Par�metros
*  [mensaje] : (C) ('Presione ENTER para continuar') Mensaje que se 
*							 quiere mostrar.
**************************************************
PARAMETERS m.mensaje, nowait

RETURN .T.	

*************************************************
FUNCTION final
*************************************************
	PARAMETERS m.est_final, x_mensaje, adtoa_used
	PRIVATE retorno
	retorno = .T.
	
	RETURN 'FUNCION "FINAL" NO SOPORTADA EN LA WEB'
	

*************************************************
FUNCTION VerNuPar
*************************************************
* Autor: FERNANDO
* 
* Fecha: 23/11/1994
* 
* Funcionamiento: Verifica que sea correcto el N�mero
* de Par�metros pasados a una Funci�n. Si es correcto
* devuelve TRUE; si no, emite un mensaje de error y
* aborta el programa.
* 
* Par�metros: nomb_fun (C) = nombre de la funci�n
* 						nro_parp (N) = n�mero de par�metros pasados a la funci�n
*							nro_par  (N) = n�mero correcto de par�metros de la funci�n
* 
*
PARAMETERS nomb_fun, nro_parp, nro_par

IF nro_parp <> nro_par
	DO final WITH 100001
ENDIF

RETURN .T.

*************************************************
FUNCTION VerTiPar
*************************************************
* Autor: FERNANDO
* 
* Fecha: 23/11/1994
* 
* Funcionamiento: Verifica que sea correcto el Tipo de
* un Par�metro pasado a una Funci�n. Si es correcto
* devuelve TRUE; si no, emite un mensaje de error y
* aborta el programa.
* 
* Par�metros: nomb_fun (C) = nombre de la funci�n
* 						nomb_par (C) = nombre del par�metro
*							tipo_par (C) = tipo correcto del par�metro
* 
*
PARAMETERS nomb_fun, nomb_par, tipo_par

IF (TYPE(nomb_par) <> tipo_par)
	DO CASE
		CASE tipo_par = 'C'
			nomb_tip = 'CHARACTER'
		CASE tipo_par = 'N'
			nomb_tip = 'NUMERIC'
		CASE tipo_par = 'D'
			nomb_tip = 'DATE'
		CASE tipo_par = 'L'
			nomb_tip = 'LOGICAL'
		CASE tipo_par = 'M'
			nomb_tip = 'MEMO'
		OTHERWISE
			nomb_tip = 'INDEFINIDO'
	ENDCASE

	DO final WITH 100002
ENDIF

RETURN .T.

*************************************************
FUNCTION uset
*************************************************
* Funcionamiento:
*  Abre una tabla en la primer �rea libre. Si es que ya est�
*	en uso setea el orden si es distinto al actual.
*	 No se selecciona la tabla que se abre.
*  Si el sistema es "conta" se cambia conta por _EJERC.
*
* Autor : ??
* Modif : Gabriel 16/12/94 ( agregue alias )
*					Rodrigo 08/07/95 ( Se agreg� el 4 par�metro )
*
* Par�metros:
*  ut_tabla    : (C) Nombre de la tabla que se quiere abrir 
*								 Puede incluir un path (ej.: 'sitema\tabla')
*  [ut_indice] : (C) ('nombre_tag_1') Indice por el cual se desea
* 							 ordenar la tabla a abrir.
*  [ut_tabla ] : (C) ('ordcom') Alias con que se quiere abrir una tabla 
*	 [ut_excl  ] : (L) Par�metro que indica si se quiere abrir la tabla en
*								 forma EXCLUSIVA.
*
*************************************************

PARAMETERS ut_tabla, ut_indice, ut_alias, ut_excl
PRIVATE ut_pos, ut_stabla, ut_arch_tab, ut_arch_ind, ut_sistema, ut_stab_ver
PRIVATE retorno, on_err, que_error

	* ### C/S
	* IF !TYPE("_SQL_DB")=="C"
	* 	PUBLIC _SQL_DB, _SQL_HND, _SQL_FLAG
	* 	_SQL_FLAG = .F.
	* 	_SQL_DB  = ""
	* 	_SQL_HND = 0
	* ENDIF
	
	* IF !EMPTY(_SQL_DB)
	* 	_SQL_FLAG = .T.
	*	RETURN (SQL_USET (ut_tabla, ut_indice, ut_alias, ut_excl))
	* ENDIF

*!*		IF _SQL_FLAG
*!*			RETURN (SQL_USET (ut_tabla, ut_indice, ut_alias, ut_excl))
*!*		ENDIF

	que_error		=	.T.
	retorno     = .T.
	ut_sistema  = SUBSTR (ut_tabla, 1, RAT('\', ut_tabla) - 1)
	ut_stabla   = SUBSTR (ut_tabla, RAT('\', ut_tabla) + 1)
	ut_arch_tab = ut_tabla + '.dbf'
	ut_arch_ind = ut_tabla + '.cdx'
	ut_stab_ver = ut_stabla 
		
	IF !EMPTY( ut_alias ) AND !( ut_alias == '' )
		ut_stabla = ut_alias
	ENDIF
	
	IF UPPER(ut_sistema) == 'CONTA' AND TYPE ('_EJERC') = 'C' AND !EMPTY (_EJERC)	
		ut_tabla    = _EJERC + '\' + ut_tabla
		ut_arch_tab = _EJERC + '\' + ut_arch_tab
		ut_arch_ind = _EJERC + '\' + ut_arch_ind	
	ENDIF

	IF !USED (ut_stabla)
		IF FILE (ut_arch_tab) AND FILE (ut_arch_ind)
			on_err = ON('ERROR')
			ON ERROR RETURN .F.
			IF ut_excl
				que_error 	= .F.
				USE (ut_tabla) ALIAS (ut_stabla) AGAIN IN 0 EXCL
			ELSE
				que_error		=	.T.
				USE (ut_tabla) ALIAS (ut_stabla) AGAIN IN 0
			ENDIF
			ON ERROR &on_err
			IF !retorno
				IF que_error
					= FINAL (1003, ut_tabla)
				ELSE
					= FINAL (1002, ut_tabla)
				ENDIF
			ENDIF
		ELSE
			DO CASE
				CASE ! FILE (ut_arch_tab)
					= final (1000, ut_arch_tab)
				CASE ! FILE (ut_arch_ind)
					= final (1001, ut_arch_ind)
			ENDCASE
			retorno = .F.
		ENDIF
	ENDIF

	IF retorno
		IF TYPE('ut_indice') = 'C'
			IF ( tagno( ut_indice ,	ut_stab_ver, ut_stabla ) = 0 )
				IF EMPTY (ut_indice)
					SET ORDER TO primario IN (ut_stabla)
				ELSE
					= final (1004, ut_indice)
				ENDIF
			ELSE
				SET ORDER TO (ut_indice) IN (ut_stabla)
			ENDIF
		ELSE
			SET ORDER TO primario IN (ut_stabla)
		ENDIF
	ENDIF
	
* CEMII
	IF (Upper (AllTrim (ut_stabla))) == 'CEMENT'
		= Uset ('recur\ubic_cem')
	ENDIF
* CEMII_

	RETURN retorno

************************************************************************************


*******************************************
FUNCTION DEL_CHR
*
* Funcion que Limpia cosas extra�as de memos
* y vbles. de tipo caracter.
* Gabriel - 16/08/94
* Previsto :
* Memos : CHR(13)-CHR(10)-TAB-BLANCO
* Char  : TAB-BLANCO
*******************************************
PARAMETERS EXP
PRIVATE R_EXP

R_EXP = EXP
R_EXP = STRTRAN( R_EXP , CHR(10) , "" )
R_EXP = STRTRAN( R_EXP , CHR(13) , "" )
R_EXP = STRTRAN( R_EXP , CHR(09) , "" )


RETURN ALLTRIM( R_EXP )


*****************
FUNCTION RLLBACK
*****************
* RETURN (1)

LOCAL m.alias, m.area
PRIVATE m.arr_hnd

*!*	IF ! _SQL_FLAG
	IF TXNLEVEL() > 0
		ROLLBACK
	ENDIF
*!*	ELSE
*!*		DECLARE m.arr_hnd [1]
*!*		STORE -1 TO arr_hnd

*!*		* SQLSETPROP (_SQL_HND, "Transaction", 1)		&& // Transacciones a modalidad autom�tica
*!*		* SQLROLLBACK (_SQL_HND)		&& Anulo la transacci�n de la conexi�n base
*!*		* SQLEXEC(_SQL_HND, [ROLLBACK TRANSACTION])		### CNX
*!*		SQL_EXEC(_SQL_HND, [ROLLBACK TRANSACTION])
*!*		
*!*		* Desenlazo las sesiones de datos
*!*		FOR m.area = 1 TO 250
*!*			m.alias = ALIAS (m.area)
*!*			IF !EMPTY(m.alias) AND TABLA_SQL (m.alias)
*!*				SQL_BIND (m.alias, .T.)
*!*			ENDIF
*!*		NEXT

*!*		RELEASE _SQL_SESSION
*!*	ENDIF

RETURN (1)

*************************************************
FUNCTION getkey
*************************************************
* Funcionamiento
*		Devuelve una expresi�n de un �ndice con m. o no seg�n 
*		(con_eme) sea .T. o .F.
*
* Par�metros:
*		tab_de_expr  : (C) nombre de la tabla de la cual se quiere 
*									 la expresi�n del �ndice.
*		[m.con_eme]  : (L) (.F.) si es .t. me devuelve los campos 
*									 con M. adelante
* 	[cant_camp]  : (N) (todos los campos de la clave) cantidad de campos 
*									 de izq. a der. del �ndice que ser�n devueltos en 
*									 la expresi�n.
* 	[m.ind_expr] : (C) ('nombre_tag_1') cadena es el nombre del tag 
*									 del �ndice cuya expresi�n se quiere obtener.
*
* Ejemplo de uso y llamada:
*  expr = GETKEY ('tabla',.T.,2,'ptabla')
*	 =SEEK (&expr)
*  En expr se almacena una cadena del tipo:
*	 ' str(m.cod_tab,3,0) + m.nom_tabla '
**************************************************

PARAMETERS tab_de_expr, m.con_eme, cant_camp, m.ind_expr 

PRIVATE baseused, nro_tag, pos, ind_aux

IF type('tab_de_expr') <> 'C'
	WAIT WINDOW ' Par�metros Inv�lidos ' NOWAIT
	RETURN .F.
ENDIF
IF type('cant_camp') <> 'N'
	cant_camp = 0
ENDIF
* IF type('m.ind_expr') <> 'C'
IF ! type('m.ind_expr') $ 'CN'
	m.ind_expr = ''
ENDIF

* ### C/S
* IF _SQL_FLAG AND CURSORGETPROP("SourceType", tab_de_expr)==2
*!*	IF TABLA_SQL (tab_de_expr)
*!*		* Es una vista remota, utilizo la emulaci�n GETKEY para SQL 
*!*		m.ind_expr = IIF(EMPTY(m.ind_expr), 1, m.ind_expr)
*!*		RETURN ( SQL_GETKEY ( tab_de_expr, m.con_eme, cant_camp, m.ind_expr ))
*!*	ENDIF

**	Guarda estado actual
baseused = .T.
IF ! USED(tab_de_expr)
	=USET (tab_de_expr)
	baseused = .F.
ENDIF

IF empty(m.ind_expr)
	nro_tag = 1
ELSE
	nro_tag = 1
	m.ind_expr = UPPER(m.ind_expr)
	m.ind_aux  = TAG (nro_tag, m.tab_de_expr)
	
	DO WHILE !(m.ind_aux == m.ind_expr) AND !(m.ind_aux == '')
		nro_tag = nro_tag + 1
		m.ind_aux  = TAG (nro_tag, m.tab_de_expr)
	ENDDO

	IF !(m.ind_aux == m.ind_expr)
		*MSG(' Indice ' + m.ind_expr + ' inexistente en la tabla '+UPPER(tab_de_expr))
		RETURN .f.
	ENDIF
ENDIF

m.expr  = SYS(14, nro_tag, tab_de_expr)
pos   = 0
IF cant_camp > 0
	pos   = AT('+', m.expr, cant_camp)
ENDIF
IF pos <> 0
	m.expr = LEFT(m.expr,pos-1)
ENDIF

IF m.con_eme
	m.expr = add_m(m.expr)
ENDIF

**	reestablece entorno
IF baseused = .F.
	USE IN (ntabla)
ENDIF

RETURN m.expr

**************************************************
FUNCTION STOT
**************************************************
* Funcionamiento
*  Pasa una cantidad de segundos a formato HHMMSS 
*  Si el valor de segundos es negativo devuelve 0 
*
* Par�metros:
*  CantSegundos : (N) Cantidad de segundos  		
**************************************************

PARAMETERS CantSegundos
PRIVATE Horas, Minutos, Segundos, Tiempo

IF PARAMETERS () < 1
	CantSegundos = 0
ENDIF

IF CantSegundos < 0
	CantSegundos = 0
ENDIF

Segundos = MOD (CantSegundos, 60)
Minutos  = INT (MOD (CantSegundos, 3600) / 60)
Horas    = INT (CantSegundos / 3600)

Tiempo = Horas * 10000 + Minutos * 100 + Segundos

RETURN Tiempo



****************
FUNCTION add_m
****************
PARAMETERS cad

PRIVATE i,exp,pal,long,car

i = 1
exp = ''
pal = ''

long = LEN(cad)

do while i <= long 
	car = substr(cad,i,1)
	if	(car <> ' ') AND (car <> '+') AND (car <> '(') AND (car <> ')') AND (car <> ',')
		pal = pal+car
	else
		if !empty(pal) 
			if	!(pal='LEFT' OR pal='RIGHT' OR pal='STR' OR pal='UPPER' OR ;
					pal='ALLTRIM' OR pal='DTOC' OR pal='DTOS' OR ISDIGIT(pal))
				pal = 'M.'+pal
				exp = exp+pal
				exp = exp+car
			else
				exp = exp+pal
				exp = exp+car
			endif
			pal = ''
		else
			exp = exp+car
		endif
	endif
	i = i+1
enddo

if !empty(pal) 
	if	!(pal='LEFT' OR pal='RIGHT' OR pal='STR' OR pal='UPPER' OR ;
		pal='ALLTRIM' OR pal='DTOC' OR pal='DTOS' OR ;
		LEFT (pal, 1) >= '0' AND LEFT (pal, 1) <= '9')
		pal = 'M.'+pal
		exp = exp+pal
	endif
	pal = ''
endif

RETURN exp
*


*************************************************
FUNCTION get_para
*************************************************
* Funcionamiento:
* Busca en el archivo de configuracion la variable por sistema
* Si el sistema es 'conta' y existe _EJERC se busca en el ejercicio
* 
* 
* Par�metros:
* sistema : al que pertenece la variable
* nomb_par : nombre del parametro que se busca

	PARAMETERS m.sistema, m.nomb_par
	PRIVATE m.v_ret, m.p_exp

*	IF LOWER (m.sistema) == 'conta' AND ;
*		TYPE ('_EJERC') = 'C' AND ;
*		!EMPTY (_EJERC)	
*			if ! uset (_EJERC + '\' + 'config')
*				RETURN ""
*			ENDIF
*	ELSE
*		IF ! uset('asa\config')
=USET ('asa\config')
*			RETURN ""
*		ENDIF
*	ENDIF

* Se pone la expresion del indice porque los campos que se pasan 
* por lo general no son de la base de datos.

m.exp = PADR (LOWER (m.sistema), 8) + PADR (UPPER (m.nomb_par), 15)
* IF ! _SQL_FLAG
* IF ! _SQL_FLAG OR CURSORGETPROP ("SourceType", "config")<>2

* ### XXX
*!*	m.p_exp = [ PADR (LOWER (m.sistema), 8) | PADR (UPPER (m.nomb_par), 15) ]

IF SEEK (m.exp, 'config')
*!*	IF DBSEEK (m.p_exp, 'config') 
	m.v_ret = config.cont_par
ELSE
	m.v_ret = ''
ENDIF

USE in config
RETURN m.v_ret


*************************************************
FUNCTION ir_reg
*************************************************
* Funcionamiento
*  Devuelve .F. si no pudo posicionar el nro. de registro indicado.
*	 Devuelve .T. si lo pudo posicionar . En este caso si se le pasa
*  un n�m de reg v�lido deja el puntero en este reg, si en cambio no
*	 se pasa un reg o el reg pasado es inv�lido el puntero queda en el 
*	 reg en que se encontraba.
*
* Par�metros
*	[reg]         : (N) (ninguno, queda en el actual) n�mero de registro  
*						      		en que se quiere posicionar el puntero.
* [tab_a_verif] : (C) (tabla activa) Nombre de la tabla de la que se quiere 
* 										saber si est� vac�a.
*************************************************

	PARAMETERS reg, tab_a_verif
	PRIVATE ant_reg

	IF TYPE('tab_a_verif') <> 'C'
		tab_a_verif = ALIAS()
	ENDIF

	* ### C/S
	* IF _SQL_FLAG AND CURSORGETPROP ("SourceType", m.tab_a_verif)==2
*!*		IF TABLA_SQL (m.tab_a_verif)
*!*			RETURN (SQL_IR_REG (m.reg, m.tab_a_verif))
*!*		ENDIF

	IF USED(tab_a_verif)
		IF TYPE('reg') <> 'N'	
			reg = RECNO(tab_a_verif)
		ENDIF		
		IF EOF(tab_a_verif)
			ant_reg = 0
		ELSE
			ant_reg = RECNO(tab_a_verif)
		ENDIF		
		IF RECCOUNT(tab_a_verif) <> 0
*!*			IF DBRECCOUNT(tab_a_verif) <> 0
			GO TOP IN (tab_a_verif)
			IF EOF(tab_a_verif)
				RETURN .F.
			ELSE
				IF !(reg > RECCOUNT(tab_a_verif) OR reg <= 0)
*!*					IF !(reg > DBRECCOUNT(tab_a_verif) OR reg <= 0)
					GO reg IN (tab_a_verif)
					IF DELETED(tab_a_verif)
						IF ant_reg <> 0
							GO ant_reg IN (tab_a_verif)
						ELSE
							GO BOTT IN (tab_a_verif)
						ENDIF
						RETURN .F.
					ELSE	
						RETURN .T.
					ENDIF
				ELSE
					IF ant_reg <> 0
						GO ant_reg IN (tab_a_verif)
					ELSE
						GO BOTT IN (tab_a_verif)
					ENDIF
					RETURN .F.
				ENDIF
			ENDIF
		ELSE
			RETURN .F.
		ENDIF
	ELSE
		*AIT WINDOW NOWAIT 'Alias '+tab_a_verif+ ', no existe'
		RETURN .F.
	ENDIF	



**************************************************
FUNCTION SINO
**************************************************
* Funcionamiento:
* 	Muestra un mensaje dentro de una ventana y pide una 
* confirmaci�n para continuar. 
*		Se usa para pedir confirmaciones.(ej.: BORRA ? )
*
*		Devuelve .T. si se selecciona < Si >
*		Devuelve .F. si se selecciona < No >
*
* Par�metros
*  [mensaje] : (C) ('Su elecci�n . . .?') Mensaje que se 
*							 quiere mostrar.
**************************************************



* SINO seleccion de opciones SI / NO

PARAMETERS m.mensaje

RETURN 'FUNCION "SINO" NO SOPORTADA EN LA WEB'