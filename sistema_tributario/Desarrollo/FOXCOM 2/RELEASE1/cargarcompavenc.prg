PROCEDURE CargarCompAVenc
	PRIVATE AuxTodoOK, AuxNroVenc

	AuxNroVenc = 0
	AuxTodoOK = .T.

	SELECT comprob
	SET ORDER TO imponib
	SET KEY TO P_TipoImponible + STR(P_NroImponible,10,0) IN comprob
	GO TOP IN comprob
	DO WHILE NOT EOF('comprob')
		* Omitir ciertos tipos de comprobantes
		IF comprob.tipo_comp # 'DE' AND comprob.tipo_comp # 'PP' AND comprob.tipo_comp # 'ND'
			SKIP IN comprob
			LOOP
		ENDIF
		
		IF comprob.est_comp = 'NO'
			AuxNroVenc = 0
			IF comprob.fec1_comp >= DATE()
				AuxNroVenc = 1
			ELSE
				IF comprob.fec2_comp >= DATE()
					AuxNroVenc = 2
				ELSE
					IF comprob.fec3_comp >= DATE()
						AuxNroVenc = 3
					ENDIF
				ENDIF
			ENDIF
			
			IF AuxNroVenc > 0
				SELECT CompAVenc
				APPEND BLANK
				REPLACE CompAVenc.grupo_comp	WITH comprob.grupo_comp,	;
						CompAVenc.nro_comp		WITH comprob.nro_comp,		;
						CompAVenc.dv_comp		WITH comprob.dv_comp,		;
						CompAVenc.venc			WITH AuxNroVenc,			;
						CompAVenc.tot_ori		WITH comprob.tot_ori
				DO SetDetalleComp
				
				DO CASE
					CASE AuxNroVenc = 1
						REPLACE CompAVenc.fecha_venc	WITH comprob.fec1_comp,						;
								CompAVenc.tot_actint	WITH comprob.tot_act + comprob.tot_int,		;
								CompAVenc.tot_iva		WITH comprob.tot_iva1 + comprob.tot_iva2,	;
								CompAVenc.tot_comp		WITH comprob.tot_comp
					
					CASE AuxNroVenc = 2
						REPLACE CompAVenc.fecha_venc	WITH comprob.fec2_comp,						;
								CompAVenc.tot_actint	WITH comprob.tot2_act + comprob.tot2_int,		;
								CompAVenc.tot_iva		WITH comprob.tot2_iva1 + comprob.tot2_iva2,	;
								CompAVenc.tot_comp		WITH comprob.tot_ori + CompAVenc.tot_actint + CompAVenc.tot_iva
					
					CASE AuxNroVenc = 3
						REPLACE CompAVenc.fecha_venc	WITH comprob.fec3_comp,						;
								CompAVenc.tot_actint	WITH comprob.tot3_act + comprob.tot3_int,		;
								CompAVenc.tot_iva		WITH comprob.tot3_iva1 + comprob.tot3_iva2,	;
								CompAVenc.tot_comp		WITH comprob.tot_ori + CompAVenc.tot_actint + CompAVenc.tot_iva
				ENDCASE
				
				SET KEY TO STR(comprob.grupo_comp,2,0) + STR(comprob.nro_comp,9,0) IN comp_ren
				GO TOP IN comp_ren
				DO WHILE NOT EOF('comp_ren')
					SELECT CompAVencReng
					APPEND BLANK
					REPLACE CompAVencReng.grupo_comp	WITH comp_ren.grupo_comp,	;
							CompAVencReng.nro_comp		WITH comp_ren.nro_comp,		;
							CompAVencReng.reng_comp		WITH comp_ren.reng_comp,	;
							CompAVencReng.recurso		WITH comp_ren.recurso,		;
							CompAVencReng.anio			WITH comp_ren.anio,			;
							CompAVencReng.cuota			WITH comp_ren.cuota,		;
							CompAVencReng.nro_mov		WITH comp_ren.nro_mov
							
					* Si el recurso no est� inclu�do en el cursor de recursos para Referencias, agregarlo.
					* Leer CODIFICS en este momento para obtener la descripci�n del recurso ser�a cr�tico
					*	porque los movimientos de cuenta corriente se leen a partir de CODIFICS. Por eso
					*	es que la obtenci�n de la descripci�n de los recursos se hace al final.
					IF NOT SEEK(comp_ren.recurso,'RefRecursos')
						SELECT RefRecursos
						APPEND BLANK
						Replace RefRecursos.recurso WITH comp_ren.recurso
					ENDIF
					SKIP IN comp_ren
				ENDDO
				SET KEY TO '' IN comp_ren
			ENDIF
		ENDIF
		SKIP IN comprob
	ENDDO
	SET KEY TO '' IN comprob
	RETURN AuxTodoOK
ENDPROC

********************************************************************
PROCEDURE SetDetalleComp
********************************************************************
	PRIVATE AuxDetalleComp, AuxCantReng, AuxDescConc
	
	AuxDetalleComp = ''
	AuxCantReng = 0
	AuxDescConc = ''
		
	SET KEY TO STR(comprob.grupo_comp,2,0) + STR(comprob.nro_comp,9,0) IN comp_ren
	GO TOP IN comp_ren
	DO WHILE NOT EOF('comp_ren')
		AuxCantReng = AuxCantReng + 1
		IF AuxCantReng = 1
			AuxDescConc = ''
			IF SEEK(comp_ren.recurso + comp_ren.conc_cc,'concs_cc')
				AuxDescConc = Concs_cc.dsc_ccc
			ENDIF
			AuxDetalleComp = ALLTRIM(comp_ren.recurso) + ' ' + ALLTRIM(STR(comp_ren.cuota,3,0)) + ;
				'/' + ALLTRIM(STR(comp_ren.anio,4,0)) + ' ' + ALLTRIM(AuxDescConc)
		ELSE
			AuxDetalleComp = AuxDetalleComp + 'y otros'
			EXIT
		ENDIF
		SKIP IN comp_ren
	ENDDO
	REPLACE CompAVenc.detalle_comp WITH AuxDetalleComp
ENDPROC