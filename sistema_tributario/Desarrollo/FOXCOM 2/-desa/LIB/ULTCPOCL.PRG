*************************************************
FUNCTION ULTCPOCL
*************************************************
* Autor: Rodrigo
* 
* Fecha: 07/10/94
* 
* Funcionamiento: Verifica si la ultima tecla pulsada fue ENTER de ser asi 
* verifica si el registro exista o no en la tabla; si existe se muestra un
* mensaje indicando que se ha ingresado una clave duplicada no permitiendo
* salir del campo. En caso de que la clave exista en la tabla se verifica
*	si se tienen permisos de consulta y edita permitiendo o no acceder al  
* registro solicitado.
*
* Par metros: No tiene
* 
* Modificaciones:
* 

PRIVATE retorno,busca_cla, db_scr

retorno = .T.
busca_cla = 'busca_clave'
db_scr = 'db_scr'

IF LASTKEY() = 13

	IF &busca_cla()
		IF c_nuevo	
			= MSG('La clave ingresada ya existe. Ingrese una nueva clave.')
			retorno = .F.
		ELSE
			IF gl_cons
				IF _server = 'SICO'
					retorno = NWVAL_CTRL('EDITA')
				ELSE
					IF gl_modi
						retorno = NWVAL_CTRL('EDITA')
					ELSE
						retorno = &DB_SCR(3,2)
					ENDIF
				ENDIF
			ELSE
				retorno = .F.
			ENDIF
		ENDIF
	ELSE
		retorno = NWVAL_CTRL('NUEVO')
	ENDIF

ENDIF

RETURN retorno
*
