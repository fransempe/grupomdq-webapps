*************************************************
FUNCTION get_para
*************************************************
* Funcionamiento:
* Busca en el archivo de configuracion la variable por sistema
* Si el sistema es 'conta' y existe _EJERC se busca en el ejercicio
* 
* 
* Par metros:
* sistema : al que pertenece la variable
* nomb_par : nombre del parametro que se busca

	PARAMETERS m.sistema, m.nomb_par
	PRIVATE m.v_ret, m.exp

*	IF LOWER (m.sistema) == 'conta' AND ;
*		TYPE ('_EJERC') = 'C' AND ;
*		!EMPTY (_EJERC)	
*			if ! uset (_EJERC + '\' + 'config')
*				RETURN ""
*			ENDIF
*	ELSE
*		IF ! uset('asa\config')
=USET ('asa\config')
*			RETURN ""
*		ENDIF
*	ENDIF

* Se pone la expresion del indice porque los campos que se pasan 
* por lo general no son de la base de datos.
m.exp = PADR (LOWER (m.sistema), 8) + PADR (UPPER (m.nomb_par), 15)

IF SEEK (m.exp, 'config') 
	m.v_ret = config.cont_par
ELSE
	m.v_ret = ''
ENDIF

USE in config
RETURN m.v_ret
*
