*************************************************
FUNCTION VAL_GRAL
*************************************************
* 05/01/95 (Rodrigo y Renato) Se descomentaron las l�neas que limpiaban
* el BUFFER del teclado y se metieron dentro del IF que verificaba si la ultima tecla 
* presionada era un ESC, verificando que a este IF solo se haya ingresado con la condici�n
* que la �ltima tecla presionada haya sido distinta de ESC y no un boton tipo "ESC".

* Los select que estan de m�s dejarlos porque no afectan a la performance
* y mejoran la seguridad

PARAMETERS vcpo,vgraba,vagrup
PRIVATE vretorno, vexpresion, vtab_val, vtipo_cpo, vnom_grup
PRIVATE vvolver, vcpo2, vcurr_rec, valias, vlastkey, vclave

IF !BETWEEN(PARAMETERS(),1,3)
	WAIT WIND "Error en el pasaje de par�metros para la funci�n VAL_GRAL()"
	RETU
ENDIF

IF EMPTY(VGRABA)
	vgraba = .F.
ENDIF

IF EMPTY(VAGRUP)
	vagrup = .F.
ENDIF

vretorno  = .T.

est_error = .F.
vencontro = .T.

valias = SELE ()

vtab_val = WONTOP()
IF vtab_val = 'CONTROLS'
	vtab_val = vent_ppal
ENDIF
SELECT (vtab_val)

vcpo2 = PADR (UPPER(vcpo),10)
IF !SEEK (vcpo2+'V',vtab_val)
	vencontro = .F.
	vtipo_cpo = ''
ELSE
	vtipo_cpo = UPPER (tipo_cpo)
ENDIF

vlastkey = LASTKEY ()

IF vlastkey#27 OR vtipo_cpo = 'ESC'

	IF vencontro
		vvolver		= volver
		vnom_grup = nom_grup
		vclave    = clave

		SELECT (vtab_val)
		DO WHIL !EOF(vtab_val) AND vretorno AND indica=='V' AND UPPER(campo)==vcpo2
			vcurr_rec = RECNO(vtab_val)

			IF !EMPTY (volver)
				vvolver		= volver
			ENDIF

			IF !(vgraba AND !val_graba)
				IF !EMPTY(funcion)
					vexpresion		= funcion+'('+ DEL_CHR (contenido) +')'
					SELE (valias)
					vretorno 			= EVAL (vexpresion)
					SELE (vtab_val)
				ENDIF
			ENDIF
			
			SELE (vtab_val)
			GO vcurr_rec
			SKIP 
		ENDDO

		SELE (vtab_val)
		IF !vretorno 
			IF !EMPTY(vvolver)
				vvolver = ALLT (vvolver)
				_CUROBJ = OBJNUM (m.&vvolver)
			ELSE
				IF vagrup
					_CUROBJ = OBJNUMSCR()
				ELSE
					IF !(grup_mem == UPPER(ALLTRIM(vnom_grup)) AND !EMPTY(grup_mem))
						IF UPPER(TRIM(vclave)) # 'CL' AND UPPER(ALLTRIM(vclave)) # 'CLU'
							_CUROBJ = OBJNUM (m.&vcpo)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF !vagrup AND !vgraba
				vretorno = .T.
			ENDIF
			est_error		= .T.
		ENDIF
	
		IF !EMPTY(vtipo_cpo) 
			IF UPPER (vtipo_cpo) = 'LST' OR UPPER (vtipo_cpo) = 'BOT'
				_CUROBJ = OBJNUM (m.&vcpo)
				vretorno = .T.
			ENDIF
		ENDIF

	ENDIF

	IF vtipo_cpo # 'ESC' AND LAST() = 27
		KEYBOARD '{shift+f12}' CLEAR
		WAIT WIND ''
	ENDIF
ENDIF

SELE (valias)

RETURN vretorno


*************************************************
FUNCTION OBJNUMSCR
*************************************************
* Autor: RODRIGO
* 
* Fecha: 05/01/95
* 
* Funcionamiento: Busca , en caso de que un agrupado de error,
* el primer campo del grupo en la tabla de validaciones.
* 

PRIVATE numobj, i, n, flag, curr_rec
i					= 1
n					= ULTCPOPANT()
numobj 		= _CUROBJ
flag		 	= .T.
curr_rec	=	RECNO(vtab_val)

DO WHILE i <= n AND flag
	nomcpo = SUBSTR(OBJVAR(i),3)
	IF SEEK(nomcpo,vtab_val)
		IF !EMPTY(EVAL (vtab_val + '.nom_grup')) AND (ALLTRIM(UPPER(EVAL (vtab_val + '.nom_grup'))) == grup_mem)
			numobj = OBJNUM(m.&nomcpo)
			flag = .F.
		ENDIF
	ENDIF
	i = i + 1
ENDDO

=IR_REG(curr_rec, vtab_val)

RETURN numobj

