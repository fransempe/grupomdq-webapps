*************************************************
FUNCTION StrWord
*************************************************
* Funcionamiento:
*  Devuelve la palabra de una cadena comenzando en una posicion.
*  Devuelve en el segundo parámetro la posición del fin de la pal. devuelta
*  Permite tomar todas las palabras de una cadena si se llama sucesivamente
*  , la primera vez la posicion es 1.
*
*  Parámetros:
*  cad_a_pal      : (C) cadena de la cual se extraen las palabras
*  pos_inicial    : (N) posición a partir de la cual se devuelve 
*										la palabra. Si se pasa este parámetro por referencia
*										devuelve la posición en la que termina la palabra
*  [sin_m]        : (L) (.F.)  .F.= devuelve palabras con m. 
*  [letras_solas] : (L) (.F.)  .F.= No devuelve letras solas
*************************************************
*
*	PS
*		recibe cadena pos_inicial
*
*		palabra ''
*		long cadena
*		i = pos_inicial
*		toma caracter
*
*		mientras i <= long
*			si finpalabra y palabra no vacia
*				finpalabra = .F.
*			finsi
*
*			case letra o '_'
*				anexa a palabra
*			case numero y palabra no vacia
*				anexa a palabra
*			case numero
*			case '+' o '-' o ')'
*				finpalabra = .T.
*			case '('
*				palabra = ''
*			fincase
*
*			si finpalabra y palabra no vacia
*				salir
*			finsi
*			i = i + 1
*			si i <= long
*				toma caracter
*			finsi
*		finmientras
*		pos_inicial = i
*		retorna palabra
	
* EJEMPLO
*  USE bind\abmgral.scx
*  LOCATE FOR ( (objtype = 1) AND (objcode = 63))
*  IF FOUND ()
*	 long = len (proccode)
*	 i = 1
*	 cadena = proccode
*	 DO while i <= long
*	 	? strword (cadena, @i,.t.,.T.)
*	 ENDDO
*  ENDIF

PARAMETERS cad_a_pal, pos_inicial, sin_m, letras_solas

	private i, palabra, longitud, caracter, fin_pal
	i        =  pos_inicial
	palabra  = ''
	long     = LEN (cad_a_pal)
	fin_pal  = .F.
	i =  pos_inicial
	IF i <= long
		caracter = substr (cad_a_pal, i, 1)
	ENDIF
	DO WHILE i <= long
 		DO CASE
		CASE ISALPHA (caracter) OR caracter = '_' 
			palabra = palabra + caracter
		CASE ISDIGIT (caracter) AND !EMPTY (palabra)
			palabra = palabra + caracter
		CASE caracter = '.' AND UPPER (substr (cad_a_pal, i-1, 1)) = 'M' AND ! sin_m 
			palabra = palabra + caracter
		OTHERWISE
			fin_pal = .T.
		ENDCASE
		
		IF (caracter = CHR(9) OR caracter = CHR(32)) 
			DO WHILE i < long
				caracter = substr (cad_a_pal, i+1, 1)
				IF caracter = CHR(9) OR caracter = CHR(32) 	
					i = i + 1
				ELSE
					EXIT 
				ENDIF
			ENDDO
		ENDIF

		IF caracter = '(' 
			palabra = ''
		ENDIF
	
		IF fin_pal 
			IF LEN(palabra) = 1 AND ! letras_solas 
				palabra = ''
			ENDIF
			IF !EMPTY (palabra)
				EXIT
			ELSE
				fin_pal = .F.
			ENDIF
		ENDIF
		i = i + 1
		IF i <= long
			caracter = substr (cad_a_pal, i, 1)
		ENDIF
	ENDDO

  pos_inicial = i
	RETURN palabra
	*
	