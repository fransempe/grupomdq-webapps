*************************************************
FUNCTION getkey
*************************************************
* Funcionamiento
*		Devuelve una expresi�n de un �ndice con m. o no seg�n 
*		(con_eme) sea .T. o .F.
*
* Par�metros:
*		tab_de_expr  : (C) nombre de la tabla de la cual se quiere 
*									 la expresi�n del �ndice.
*		[m.con_eme]  : (L) (.F.) si es .t. me devuelve los campos 
*									 con M. adelante
* 	[cant_camp]  : (N) (todos los campos de la clave) cantidad de campos 
*									 de izq. a der. del �ndice que ser�n devueltos en 
*									 la expresi�n.
* 	[m.ind_expr] : (C) ('nombre_tag_1') cadena es el nombre del tag 
*									 del �ndice cuya expresi�n se quiere obtener.
*
* Ejemplo de uso y llamada:
*  expr = GETKEY ('tabla',.T.,2,'ptabla')
*	 =SEEK (&expr)
*  En expr se almacena una cadena del tipo:
*	 ' str(m.cod_tab,3,0) + m.nom_tabla '
**************************************************

PARAMETERS tab_de_expr, m.con_eme, cant_camp, m.ind_expr 

PRIVATE baseused, nro_tag, pos, ind_aux

IF type('tab_de_expr') <> 'C'
	WAIT WINDOW ' Par�metros Inv�lidos ' NOWAIT
	RETURN .F.
ENDIF
IF type('cant_camp') <> 'N'
	cant_camp = 0
ENDIF
IF type('m.ind_expr') <> 'C'
	m.ind_expr = ''
ENDIF

**	Guarda estado actual
baseused = .T.
IF ! USED(tab_de_expr)
	=USET (tab_de_expr)
	baseused = .F.
ENDIF

IF empty(m.ind_expr)
	nro_tag = 1
ELSE
	nro_tag = 1
	m.ind_expr = UPPER(m.ind_expr)
	m.ind_aux  = TAG (nro_tag, m.tab_de_expr)
	
	DO WHILE !(m.ind_aux == m.ind_expr) AND !(m.ind_aux == '')
		nro_tag = nro_tag + 1
		m.ind_aux  = TAG (nro_tag, m.tab_de_expr)
	ENDDO

	IF !(m.ind_aux == m.ind_expr)
		=MSG(' Indice ' + m.ind_expr + ' inexistente en la tabla '+UPPER(tab_de_expr))
		RETURN .f.
	ENDIF
ENDIF

expr  = SYS(14, nro_tag, tab_de_expr)
pos   = 0
IF cant_camp > 0
	pos   = AT('+', expr, cant_camp)
ENDIF
IF pos <> 0
	expr = LEFT(expr,pos-1)
ENDIF

IF m.con_eme
	expr = add_m(expr)
ENDIF

**	reestablece entorno
IF baseused = .F.
	USE IN (ntabla)
ENDIF

RETURN expr

****************
FUNCTION add_m

PARAMETERS cad

PRIVATE i,exp,pal,long,car

i = 1
exp = ''
pal = ''

long = LEN(cad)

do while i <= long 
	car = substr(cad,i,1)
	if	(car <> ' ') AND (car <> '+') AND (car <> '(') AND (car <> ')') AND (car <> ','))
		pal = pal+car
	else
		if !empty(pal) 
			if	!(pal='LEFT' OR pal='RIGHT' OR pal='STR' OR pal='UPPER' OR ;
					pal='ALLTRIM' OR pal='DTOC' OR pal='DTOS' OR ISDIGIT(pal))
				pal = 'M.'+pal
				exp = exp+pal
				exp = exp+car
			else
				exp = exp+pal
				exp = exp+car
			endif
			pal = ''
		else
			exp = exp+car
		endif
	endif
	i = i+1
enddo

if !empty(pal) 
	if	!(pal='LEFT' OR pal='RIGHT' OR pal='STR' OR pal='UPPER' OR ;
		pal='ALLTRIM' OR pal='DTOC' OR pal='DTOS' OR ;
		LEFT (pal, 1) >= '0' AND LEFT (pal, 1) <= '9')
		pal = 'M.'+pal
		exp = exp+pal
	endif
	pal = ''
endif

RETURN exp
*

