*************************************************
* FUNCTION FLeeDom
*************************************************
* Dise�o : Pachu
* Fecha  : 09/03/10
* 
* Funcionamiento:
* Dado un dominio, obtiene el nro. de veh�culo correspondiente.
* 
* Par�metros:
* In  P_Dominio  -C-
* Out NroVehic   -N- 
*
*
* Modificaciones:
* 
PARAMETERS P_Dominio
PRIVATE retorno

retorno = .T. 

IF NOT USED('vehiculo')
  =USET('recur\vehiculo')
ENDIF
SELECT vehiculo
*SET ORDER TO DOMINIO IN vehiculo
SET ORDER TO DOMINIO


LOCATE FOR STRTRAN(DOMINIO, '-', '') = ALLTRIM(P_Dominio)
IF (!EOF('vehiculo')) THEN
	retorno = vehiculo.nro_vehi
ELSE
	retorno = 0
ENDIF

*IF SEEK(ALLT(P_Dominio), 'vehiculo')
*	retorno = vehiculo.nro_vehi
*ELSE
*	retorno = 0
*ENDIF

*SET ORDER TO PRIMARIO IN VEHICULO
RETURN STR(retorno)
