 PROCEDURE _ObtRecursosConPagoElect() as String
    PRIVATE mString as String

    IF (!USED ('rec_inf')) THEN
      =USET ('recur\rec_inf')
    ENDIF
    
    mString = ''
    GO TOP IN rec_inf
    DO WHILE !EOF('rec_inf')
      IF rec_inf.env_link = 'S'
        IF EMPTY(mString)
          mString = ALLT(rec_inf.recurso)
        ELSE
          mString = ALLT(mString) + '|' + ALLT(rec_inf.recurso)
        ENDIF
      ENDIF
      SKIP IN rec_inf
    ENDDO

    RETURN mString
 ENDPROC
