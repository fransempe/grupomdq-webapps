PROCEDURE AgregCompAArchSalida (AuxArchCompGral as String)
	PRIVATE mDescripcionRecurso as String
	PRIVATE mSeek as String
	
	
	IF NOT USED('codifics')
		=USET('recur\codifics')
	ENDIF
	
	AuxImpTotOrig1 = 0
	AuxImpTotOrig2 = 0
	AuxImpTotOrig3 = 0
	
	AuxArchCompGral = AuxArchCompGral + Indent(2) + '<COMPROBANTE>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<NROCOMP>' + PADL(ALLT(STR(comprob.grupo_comp,2,0)),2,'0') + '/' + PADL(ALLT(STR(comprob.nro_comp,9,0)),9,'0') + '/' + PADL(ALLT(STR(comprob.dv_comp,2,0)),2,'0') + '</NROCOMP>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<FECHA_EMISION>' + IIF(NOT EMPTY(comprob.fecemicomp),DTOC(comprob.fecemicomp),'') + '</FECHA_EMISION>' + _S_
			
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<RENGLONES>' + _S_
	SET KEY TO STR(comprob.grupo_comp,2,0) + STR(comprob.nro_comp,9,0) IN comp_ren
	GO TOP IN comp_ren
	DO WHILE NOT EOF('comp_ren')

	  IF TYPE('comp_ren.venc_conc') = 'U' OR comp_ren.venc_conc = 0 OR comprob.fec1_comp >= DATE() OR;
	   (!EMPTY(comprob.fec2_comp) AND comprob.fec2_comp >= DATE() AND comp_ren.venc_conc >= 2) OR;
	   (!EMPTY(comprob.fec3_comp) AND comprob.fec3_comp >= DATE() AND comp_ren.venc_conc >= 3)

		AuxDescConc = ''
		IF SEEK(comp_ren.recurso+comp_ren.conc_cc,'concs_cc')
			AuxDescConc = concs_cc.dsc_ccc
		ENDIF
		AuxCodPlan = ''
		IF comp_ren.nro_plan > 0
			IF SEEK(comprob.tipo_imp+STR(comprob.nro_imp,10,0)+STR(comp_ren.nro_plan,3,0),'pla_imp')
				AuxCodPlan = pla_imp.cod_plan
			ENDIF
		ENDIF
		
		
		DO CASE			
			CASE TYPE('comp_ren.venc_conc') = 'U' OR comp_ren.venc_conc = 0
				AuxImpTotOrig1 = AuxImpTotOrig1 + ROUND(comp_ren.imp_reng,2)
				IF NOT EMPTY(comprob.fec2_comp)
					AuxImpTotOrig2 = AuxImpTotOrig2 + ROUND(comp_ren.imp_reng,2)
				ENDIF
				IF NOT EMPTY(comprob.fec3_comp)
					AuxImpTotOrig3 = AuxImpTotOrig3 + ROUND(comp_ren.imp_reng,2)
				ENDIF
			CASE comp_ren.venc_conc = 1
				AuxImpTotOrig1 = AuxImpTotOrig1 + ROUND(comp_ren.imp_reng,2)
			CASE comp_ren.venc_conc = 2
				IF NOT EMPTY(comprob.fec2_comp)
					AuxImpTotOrig2 = AuxImpTotOrig2 + ROUND(comp_ren.imp_reng,2)
				ENDIF
			CASE comp_ren.venc_conc = 3
				IF NOT EMPTY(comprob.fec3_comp)
					AuxImpTotOrig3 = AuxImpTotOrig3 + ROUND(comp_ren.imp_reng,2)
				ENDIF
		ENDCASE
		IF TYPE('comp_ren.venc_conc') = 'U' OR (comp_ren.venc_conc = 0) OR (comp_ren.venc_conc = 1)
			AuxArchCompGral = AuxArchCompGral + Indent(4) + '<RENGLON>' + _S_
			
			
			
			* Obtengo la descripcion del recurso
			mDescripcionRecurso = ''
			mSeek = PADR('RECURS', 6) + PADR(ALLTRIM(comp_ren.recurso), 6)
			IF (SEEK(ALLTRIM(mSeek), 'codifics')) THEN
				mDescripcionRecurso = ALLTRIM(codifics.Desc_cod)
			ENDIF
					
			
			AuxArchCompGral = AuxArchCompGral + Indent(5) + '<RECURSO>' + ALLT(comp_ren.recurso) + ' - ' + ALLTRIM(mDescripcionRecurso) + '</RECURSO>' + _S_										
			AuxArchCompGral = AuxArchCompGral + Indent(5) + '<ANIO>' + ALLT(STR(comp_ren.anio,4,0)) + '</ANIO>' + _S_
			AuxArchCompGral = AuxArchCompGral + Indent(5) + '<CUOTA>' + ALLT(STR(comp_ren.cuota,3,0)) + '</CUOTA>' + _S_
			AuxArchCompGral = AuxArchCompGral + Indent(5) + '<CONCEPTO>' + ALLT(AuxDescConc) + '</CONCEPTO>' + _S_
			AuxArchCompGral = AuxArchCompGral + Indent(5) + '<CODPLAN>' + ALLT(AuxCodPlan) + '</CODPLAN>' + _S_
			AuxArchCompGral = AuxArchCompGral + Indent(5) + '<IMPORIGENRENG>' + ALLT(STR(comp_ren.imp_reng,15,2)) + '</IMPORIGENRENG>' + _S_
			
			
			* Modificado por Juan 15/06
			* Pedido por Ariel para la municipalidad de tapalque
			* Requisito: No muestra correctamente los datos al segundo vencimiento
			* Solucion: La dll no estaba devolviendo estos valores; Por esto agrego un "IF" preguntando en orden 
			* cronologico que fecha de vencimiento esta vencida, una vez encontrada entro en esa rama de "IF" para
			* asignar los datos:
			* Esta es la sintaxis que remplace:
			*	AuxArchCompGral = AuxArchCompGral + Indent(5) + '<IMPRECARGOSRENG>' + ALLT(STR(comp_ren.actualiza1 + comp_ren.intereses1,15,2)) + '</IMPRECARGOSRENG>' + _S_
			*	AuxArchCompGral = AuxArchCompGral + Indent(5) + '<IMPTOTALRENG>' + ALLT(STR(comp_ren.imp_reng + comp_ren.actualiza1 + comp_ren.intereses1,15,2)) + '</IMPTOTALRENG>' + _S_
								
			
			* Le asigno los valores al primer vencimiento
			IF comprob.fec1_comp >= DATE()
				AuxArchCompGral = AuxArchCompGral + Indent(5) + '<IMPRECARGOSRENG>' + ALLT(STR(comp_ren.actualiza1 + comp_ren.intereses1,15,2)) + '</IMPRECARGOSRENG>' + _S_
				AuxArchCompGral = AuxArchCompGral + Indent(5) + '<IMPTOTALRENG>' + ALLT(STR(comp_ren.imp_reng + comp_ren.actualiza1 + comp_ren.intereses1,15,2)) + '</IMPTOTALRENG>' + _S_
			ELSE
			
				* Le asigno los valores al segundo vencimiento
				IF comprob.fec2_comp >= DATE()				
					AuxArchCompGral = AuxArchCompGral + Indent(5) + '<IMPRECARGOSRENG>' + ALLT(STR(comp_ren.actualiza2 + comp_ren.intereses2,15,2)) + '</IMPRECARGOSRENG>' + _S_
					AuxArchCompGral = AuxArchCompGral + Indent(5) + '<IMPTOTALRENG>' + ALLT(STR(comp_ren.imp_reng + comp_ren.actualiza2 + comp_ren.intereses2,15,2)) + '</IMPTOTALRENG>' + _S_				
					
				ELSE				
					* Le asigno los valores al tercer vencimiento
					IF comprob.fec3_comp >= DATE()
						AuxArchCompGral = AuxArchCompGral + Indent(5) + '<IMPRECARGOSRENG>' + ALLT(STR(comp_ren.actualiza3 + comp_ren.intereses3,15,2)) + '</IMPRECARGOSRENG>' + _S_
						AuxArchCompGral = AuxArchCompGral + Indent(5) + '<IMPTOTALRENG>' + ALLT(STR(comp_ren.imp_reng + comp_ren.actualiza3 + comp_ren.intereses3,15,2)) + '</IMPTOTALRENG>' + _S_
					ENDIF
				ENDIF
			ENDIF
			
			
			
			
			AuxArchCompGral = AuxArchCompGral + Indent(4) + '</RENGLON>' + _S_
		ENDIF
           ENDIF
	   SKIP IN comp_ren
	ENDDO
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '</RENGLONES>' + _S_
	SET KEY TO '' IN comp_ren
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<FECHA_VENCIMIENTO_1>' + IIF(NOT EMPTY(comprob.fec1_comp),DTOC(comprob.fec1_comp),'') + '</FECHA_VENCIMIENTO_1>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_TOTAL_ORIGEN1>' + ALLT(STR(AuxImpTotOrig1,15,2)) + '</IMPORTE_TOTAL_ORIGEN1>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_RECARGO_1>' + ALLT(STR(comprob.tot_act + comprob.tot_int,15,2)) + '</IMPORTE_RECARGO_1>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_TOTAL_1>' + ALLT(STR(comprob.tot_comp,15,2)) + '</IMPORTE_TOTAL_1>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<FECHA_VENCIMIENTO_2>' + IIF(NOT EMPTY(comprob.fec2_comp),DTOC(comprob.fec2_comp),'') + '</FECHA_VENCIMIENTO_2>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_TOTAL_ORIGEN2>' + ALLT(STR(AuxImpTotOrig2,15,2)) + '</IMPORTE_TOTAL_ORIGEN2>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_RECARGO_2>' + IIF(NOT EMPTY(comprob.fec2_comp),ALLT(STR(comprob.tot2_act + comprob.tot2_int,15,2)),'0.00') + '</IMPORTE_RECARGO_2>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_TOTAL_2>' + IIF(NOT EMPTY(comprob.fec2_comp),ALLT(STR(AuxImpTotOrig2 + comprob.tot2_act + comprob.tot2_int,15,2)),'0.00') + '</IMPORTE_TOTAL_2>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<FECHA_VENCIMIENTO_3>' + IIF(NOT EMPTY(comprob.fec3_comp),DTOC(comprob.fec3_comp),'') + '</FECHA_VENCIMIENTO_3>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_TOTAL_ORIGEN3>' + ALLT(STR(AuxImpTotOrig3,15,2)) + '</IMPORTE_TOTAL_ORIGEN3>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_RECARGO_3>' + IIF(NOT EMPTY(comprob.fec3_comp),ALLT(STR(comprob.tot3_act + comprob.tot3_int,15,2)),'0.00') + '</IMPORTE_RECARGO_3>' + _S_
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<IMPORTE_TOTAL_3>' + IIF(NOT EMPTY(comprob.fec3_comp),ALLT(STR(AuxImpTotOrig3 + comprob.tot3_act + comprob.tot3_int,15,2)),'0.00') + '</IMPORTE_TOTAL_3>' + _S_
	
	AuxStringCodBarra = ''
	
	IF ALLTRIM(_DATOSREPLICADOS) = 'S' AND comprob.grupo_comp = AuxGCompWeb 	
	  FCODBARRA = 'FCBWebW()'
	ELSE
	  FCODBARRA = 'FCBWeb()'
	ENDIF

	*_fcbweb_ = "'" + ALLTRIM(_SISTEMA) + ALLTRIM(m.sistema) + "\" + ALLTRIM(m.modulo) + "\FCBWEB.PRG'"
	_fcbwebpath_ = "'" + ALLTRIM(_SISTEMA) + ALLTRIM(m.sistema) + "\" + ALLTRIM(m.modulo) + "\'"
	_oldpath = SET("Path")
	SET PATH TO &_fcbwebpath_
	
	
	*IF FILE(&_fcbweb_)
	IF FILE('FCBWEB.FXP')
		*SET PROCEDURE TO &_fcbweb_
		AuxStringCodBarra = &FCODBARRA
		*SET PROCEDURE TO MINIEFIPROC
		*ELSE
		*AuxStringCodBarra = 'holaaaaaa' + "'" + ALLTRIM(_SISTEMA) + ALLTRIM(m.sistema) + "\" + ALLTRIM(m.modulo) + "\'"
	ENDIF
	
	SET PATH TO &_oldpath
	
	
	
	AuxArchCompGral = AuxArchCompGral + Indent(3) + '<STRINGCODBARRA>' + AuxStringCodBarra + '</STRINGCODBARRA>' + _S_	
	AuxArchCompGral = AuxArchCompGral + Indent(2) + '</COMPROBANTE>' + _S_
ENDPROC