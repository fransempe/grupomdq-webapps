PROCEDURE _getVisitasCantidad() as String
	PRIVATE nCantidad as number

	IF (!USED('web_vis')) THEN
		=UseT('recur\web_vis')
	ENDIF
	SELECT web_vis
	SCATTER MEMVAR BLANK
	
	nCantidad = 0
	COUNT TO nCantidad

	RETURN ALLTRIM(STR(nCantidad))
ENDPROC