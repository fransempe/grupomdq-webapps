PROCEDURE _getUsaLoginAdmin() as String
	PRIVATE strUsaLoginAdmin as String	
	PRIVATE strIndex as String
	PRIVATE strParametro as String
	
	
	IF (!USED ('config')) THEN
		=USET ('asa\config')
	ENDIF
	SELECT config
	SET ORDER TO Primario IN config
		 
	
	strParametro = ''
	strUsaLoginAdmin = 'FALSE'
	strIndex = PADR('ASA', 8, ' ') + 'WEB_LOGIN_ADMIN'
	IF (SEEK(strIndex, 'config')) THEN
		strParametro = ALLTRIM(config.cont_par)
		IF (!EMPTY(strParametro)) THEN
			IF (ALLTRIM(strParametro) = 'S') THEN
				strUsaLoginAdmin = 'TRUE'
			ELSE 
				strUsaLoginAdmin = 'FALSE'
			ENDIF
		ENDIF
	ENDIF
	
	
	RETURN ALLTRIM(strUsaLoginAdmin)
ENDPROC
