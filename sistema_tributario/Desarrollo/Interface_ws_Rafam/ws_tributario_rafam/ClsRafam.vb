﻿
'DB ORACLE
Imports System.Data.OleDb
Imports System.Data.OracleClient


Public Class ClsRafam

#Region "Variables"

    Private mConexion As OracleConnection
    Private mAdaptador As New OracleDataAdapter
    Private mComando As New OracleCommand()
    Private mCadenaConexion As String
    Private mRutaFisicaDirectorio As String


#End Region

#Region "Contructores"
    Public Sub New(ByVal mRutaFisica As String)
        mConexion = Nothing
        mCadenaConexion = ""
        mRutaFisicaDirectorio = mRutaFisica.ToString

        SetearCadenaConexion()
    End Sub

    Public Function Abrir(ByVal mStringConexion As String) As Boolean

        mCadenaConexion = mStringConexion.tostring
        If (mCadenaConexion.Tostring <> "") Then
            Return True
        Else
            Return False
        End If

    End Function
#End Region

#Region "Funciones Privadas"

    Private Function SetearCadenaConexion() As String
        Dim dsConfiguracion As New DataSet

        dsConfiguracion.ReadXml(mRutaFisicaDirectorio.ToString & "\Configuracion.gmdq")
        mCadenaConexion = ObtenerCadenaConexion(dsConfiguracion.Tables("CadenaConexion"))
        dsConfiguracion = Nothing

        Return mCadenaConexion.ToString
    End Function

    Private Function ObtenerCadenaConexion(ByVal dtCadenaConexion As DataTable) As String
        Dim mCadenaConexion As String

        mCadenaConexion = " Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)" & _
                          " (HOST=" & ModSeguridad.DesEncriptar(dtCadenaConexion.Rows(0).Item(0).ToString) & ")" & _
                          " (PORT=" & ModSeguridad.DesEncriptar(dtCadenaConexion.Rows(0).Item(1).ToString) & "))) " & _
                          " (CONNECT_DATA=(SERVER=" & ModSeguridad.DesEncriptar(dtCadenaConexion.Rows(0).Item(2).ToString) & ")" & _
                          " (SERVICE_NAME=" & ModSeguridad.DesEncriptar(dtCadenaConexion.Rows(0).Item(3).ToString) & "))); " & _
                          " User Id=" & ModSeguridad.DesEncriptar(dtCadenaConexion.Rows(0).Item(4).ToString) & ";" & _
                          " Password=" & ModSeguridad.DesEncriptar(dtCadenaConexion.Rows(0).Item(5).ToString) & ";"

        Return mCadenaConexion.ToString
    End Function

    Private Function ObtenerTipoCuenta(ByVal mTipoCuenta As String) As String
        Select Case mTipoCuenta.ToUpper
            Case "I" : Return "INMUEBLE"
            Case "C" : Return "COMERCIO"
            Case "E", "O" : Return "CEMENTERIO"
            Case "V", "R" : Return "VEHÍCULO"
            Case Else : Return ""
        End Select
    End Function

#End Region

#Region "Funciones WS"

    Public Function VerRuta_CadenaConexion() As String
        Dim mCadenaConexion As String
        mCadenaConexion = ""
        mCadenaConexion = SetearCadenaConexion()
        Return mCadenaConexion
    End Function

#Region "Limpiar SESSION"

    'Esta Funcion Limpia los registros de la Tabla ING_TMP_CC cuando un USUARIO cierra o Caduca su SESSION.
    Public Function LimpiarDatosSession(ByVal mNroUsuario As Integer) As Boolean
        Dim mOK As Boolean

        Try

            'Variable Bandera
            mOK = False

            'Instancio Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()


            'Instancio el Comando
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.LIMPIAR_DATOS_SESSION"
            mComando.CommandType = CommandType.StoredProcedure


            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("mNroUsuario", OracleType.Number)).Value = mNroUsuario


            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()
            mConexion.Close()

            mOK = True
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If
            Call GenerarLog(ex)
            mOK = False
        Finally
            mComando = Nothing
            mConexion = Nothing
        End Try

        Return mOK
    End Function

#End Region

#Region "Consultar Deuda"

    Public Function ConsultarDeuda(ByVal mTipoCuenta As String, ByVal mNroCuenta As Long, ByVal mUsuarioObtenido As Integer) As String
        Dim mNombreCuenta As String
        Dim mNombreApellidoContribuyente As String
        Dim mNroUsuario As Integer
        Dim dsDatosDeuda As DataSet
        Dim mXML As String

        Try


            'Si el (UsuarioObtenido = -1) no Genero la deuda en la TABLA TEMPORAL ING_TMP_CC y no Actualizo la tabla de USUARIOS
            If (mUsuarioObtenido = -1) Then

                'Genero la Deuda en la Tabla Temporal
                mNroUsuario = GenerarDeuda(mTipoCuenta, mNroCuenta)
            Else
                mNroUsuario = mUsuarioObtenido
            End If



            If (mNroUsuario <> -1) Then
                'Obtengo los datos de la Tabla Temporal
                If (mNroUsuario <> -1) Then
                    dsDatosDeuda = ObtenerDeuda(mNroUsuario)
                End If



                'Obtengo el Nombre de la Cuenta
                mNombreCuenta = ObtenerTipoCuenta(mTipoCuenta)


                'Obtengo el Nombre y el Apellido del Contribuyente
                mNombreApellidoContribuyente = ObtenerNombreContribuyente(mTipoCuenta, mNroCuenta)


                'Genero el XML con la DEUDA del CONTRIBUYENTE
                mXML = ObtenerXMLConsultarDeuda(dsDatosDeuda, mNroUsuario, mNombreCuenta, mNroCuenta, mNombreApellidoContribuyente)


            Else
                mXML = "Usuario Sin Asignar"
            End If


            Return mXML.ToString

        Catch ex As Exception
            Return "Error"
        End Try

    End Function

    Private Function GenerarDeuda(ByVal mTipoCuenta As String, ByVal mNroCuenta As Long) As Integer
        Dim mSQL As String = ""
        Dim mNroUsuario As Integer


        Try

            'Instancio Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()


            'Instancio el Comando
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.GENERAR_DEUDA"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("mNroCuenta", OracleType.Number)).Value = mNroCuenta
            mComando.Parameters.Add(New OracleParameter("mTipoCuenta", OracleType.Char)).Value = mTipoCuenta.ToString
            mComando.Parameters.Add(New OracleParameter("mNroUsuario", OracleType.Number)).Direction = ParameterDirection.Output
            mComando.Parameters.Add(New OracleParameter("mCantidadRegistros", OracleType.Number)).Direction = ParameterDirection.Output


            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Obtengo el Nro. de Usuario con el que se genero la deuda
            mNroUsuario = -1
            mNroUsuario = mComando.Parameters.Item("mNroUsuario").Value.ToString
            mConexion.Close()



            Return mNroUsuario
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            mConexion = Nothing
            Call GenerarLog(ex)
            Return -1
        End Try

    End Function

    Private Function ObtenerDeuda(ByVal mNroUsuario As Integer) As DataSet
        Dim dsDatos As DataSet
        Dim mSQL As String = ""


        Try
            'Instancio Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.OBTENER_DEUDA"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("mNroUsuario", OracleType.Number)).Value = mNroUsuario
            mComando.Parameters.Add(New OracleParameter("mCursorRecursos", OracleType.Cursor)).Direction = ParameterDirection.Output
            mComando.Parameters.Add(New OracleParameter("mCursorDeudaVencida", OracleType.Cursor)).Direction = ParameterDirection.Output
            mComando.Parameters.Add(New OracleParameter("mCursorTotalDeudaVencida", OracleType.Cursor)).Direction = ParameterDirection.Output
            mComando.Parameters.Add(New OracleParameter("mCursorDeudaProxima", OracleType.Cursor)).Direction = ParameterDirection.Output
            mComando.Parameters.Add(New OracleParameter("mCursorDeudaOtra", OracleType.Cursor)).Direction = ParameterDirection.Output


            'Ejecuto el Comando y Obtengo los datos en Dataset
            dsDatos = New DataSet
            mAdaptador = New OracleDataAdapter(mComando)
            mAdaptador.Fill(dsDatos, "DATOS")
            mConexion.Close()



            'Asigno Nombre a las etiquetas
            dsDatos.Tables(0).TableName = "Recursos"
            dsDatos.Tables(1).TableName = "DeudaVencida"
            dsDatos.Tables(2).TableName = "TotalDeudaVencida"
            dsDatos.Tables(3).TableName = "DeudaProxima"
            dsDatos.Tables(4).TableName = "DeudaOtra"
            dsDatos.DataSetName = "Consulta"


            Return dsDatos
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            mConexion = Nothing
            Call GenerarLog(ex)
            Return Nothing
        End Try

    End Function

    Private Function ObtenerNombreContribuyente(ByVal mTipoCuenta As String, ByVal mNroCuenta As Long) As String
        Dim mDataSet As New DataSet
        Dim mSQL As String = ""

        Try

            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.OBTENER_NOMBRE_CONTRIBUYENTE"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("mNroCuenta", OracleType.Number)).Value = mNroCuenta
            mComando.Parameters.Add(New OracleParameter("mTipoCuenta", OracleType.Char)).Value = mTipoCuenta.ToString
            mComando.Parameters.Add(New OracleParameter("mNombreContribuyente", OracleType.VarChar, 100)).Direction = ParameterDirection.ReturnValue



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Analizo el Resultado
            If (IsDBNull(mComando.Parameters.Item("mNombreContribuyente").Value)) Then
                Return "NULL"
            Else
                Return mComando.Parameters.Item("mNombreContribuyente").Value.ToString
            End If

            mComando = Nothing
            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            mConexion = Nothing
            Call GenerarLog(ex)
            Return "Error"
        End Try

    End Function

    Private Function ObtenerXMLConsultarDeuda(ByVal dsDatos As DataSet, ByVal mNroUsuario As Integer, ByVal mNombreCuenta As String, _
                                              ByVal mNroCuenta As String, ByVal mNombreApellidoContribuyente As String) As String
        Dim mXML As String
        Dim i As Integer
        Dim mXMLDatosMunicipalidad As String
        Dim mImporteOrigen As Double
        Dim mImporteRecargo As Double
        Dim mImporteTotal As Double
        Dim mNroComprobante As String
        Dim mNota As String
        Dim TieneDeuda As Boolean



        mXML = "<?xml version=""1.0"" encoding=""UTF-8""?> " & vbCrLf & _
               "<VFPDATA> " & vbCrLf & _
                   "<DATOSCUENTA> " & vbCrLf & _
                       "<TIPOIMPONIBLE>" & mNombreCuenta.ToString & "</TIPOIMPONIBLE> " & vbCrLf & _
                       "<NROIMPONIBLE>" & mNroCuenta.ToString & "</NROIMPONIBLE> " & vbCrLf & _
                       "<TITULAR>" & mNombreApellidoContribuyente.ToString & "</TITULAR> " & vbCrLf & _
                       "<CLAVE>" & mNroUsuario.ToString & "</CLAVE> " & vbCrLf & _
                   "</DATOSCUENTA> " & vbCrLf & _
                   "<DETALLEDEUDAVENCIDA> " & vbCrLf


        TieneDeuda = False
        mImporteOrigen = 0
        mImporteRecargo = 0
        mImporteTotal = 0




        For i = 0 To dsDatos.Tables("DeudaVencida").Rows.Count - 1

            mXML = mXML & "<REGISTRO_DETALLEDEUDAVENCIDA> " & vbCrLf & _
                                "<DDV_NROMOV>" & dsDatos.Tables("DeudaVencida").Rows(i).Item("DDV_NROMOV").ToString & "</DDV_NROMOV> " & vbCrLf & _
                                "<DDV_REC>" & dsDatos.Tables("DeudaVencida").Rows(i).Item("DDV_REC").ToString & "</DDV_REC> " & vbCrLf & _
                                "<DDV_ANIO>" & dsDatos.Tables("DeudaVencida").Rows(i).Item("DDV_ANIO").ToString & "</DDV_ANIO> " & vbCrLf & _
                                "<DDV_CUOTA>" & dsDatos.Tables("DeudaVencida").Rows(i).Item("DDV_CUOTA").ToString & "</DDV_CUOTA> " & vbCrLf & _
                                "<DDV_CONCEPTO>" & dsDatos.Tables("DeudaVencida").Rows(i).Item("DDV_CONCEPTO").ToString & "</DDV_CONCEPTO> " & vbCrLf & _
                                "<DDV_DEPLAN>" & dsDatos.Tables("DeudaVencida").Rows(i).Item("DDV_DEPLAN").ToString & "</DDV_DEPLAN> " & vbCrLf & _
                                "<DDV_FECHAVENC>" & Format(CDate(dsDatos.Tables("DeudaVencida").Rows(i).Item("DDV_FECHAVENC").ToString), "dd/MM/yyyy") & "</DDV_FECHAVENC> " & vbCrLf & _
                                "<DDV_CONDESPECIAL>" & dsDatos.Tables("DeudaVencida").Rows(i).Item("DDV_CONDESPECIAL").ToString & "</DDV_CONDESPECIAL> " & vbCrLf & _
                                "<DDV_IMPORTEORIGEN>" & dsDatos.Tables("DeudaVencida").Rows(i).Item("DDV_IMPORTEORIGEN").ToString & "</DDV_IMPORTEORIGEN> " & vbCrLf & _
                                "<DDV_IMPORTERECARGO>" & dsDatos.Tables("DeudaVencida").Rows(i).Item("DDV_IMPORTERECARGO").ToString & "</DDV_IMPORTERECARGO> " & vbCrLf & _
                                "<DDV_IMPORTETOTAL>" & dsDatos.Tables("DeudaVencida").Rows(i).Item("DDV_IMPORTETOTAL").ToString & "</DDV_IMPORTETOTAL> " & vbCrLf & _
                           "</REGISTRO_DETALLEDEUDAVENCIDA> " & vbCrLf

            TieneDeuda = True
        Next






        mXML = mXML & "</DETALLEDEUDAVENCIDA> " & vbCrLf & _
             "<TOTALESDEUDAVENCIDA> " & vbCrLf & _
                 "<REGISTRO_TOTALESDEUDAVENCIDA> " & vbCrLf & _
                     "<TDV_TEXTO>Total de Deuda Vencida</TDV_TEXTO> " & vbCrLf & _
                     "<TDV_IMPORTEORIGEN>" & dsDatos.Tables("TotalDeudaVencida").Rows(0).Item("DDV_TOTAL_IMPORTEORIGEN").ToString & "</TDV_IMPORTEORIGEN> " & vbCrLf & _
                     "<TDV_IMPORTERECARGO>" & dsDatos.Tables("TotalDeudaVencida").Rows(0).Item("DDV_TOTAL_IMPORTERECARGO").ToString & "</TDV_IMPORTERECARGO> " & vbCrLf & _
                     "<TDV_IMPORTETOTAL>" & dsDatos.Tables("TotalDeudaVencida").Rows(0).Item("DDV_TOTAL_IMPORTETOTAL").ToString & "</TDV_IMPORTETOTAL> " & vbCrLf & _
                 "</REGISTRO_TOTALESDEUDAVENCIDA> " & vbCrLf & _
             "</TOTALESDEUDAVENCIDA> " & vbCrLf



        'Detalle de los Movimientos Proximos a Vencer dentro de los 60 dias
        mImporteOrigen = 0
        mImporteRecargo = 0
        mImporteTotal = 0
        mNroComprobante = ""
        If (dsDatos.Tables("DeudaProxima").Rows.Count = 0) Then
            mXML = mXML & "<DETALLEPROXCOMPAVENC> " & vbCrLf & _
                          "</DETALLEPROXCOMPAVENC> " & vbCrLf
        Else

            mXML = mXML & "<DETALLEPROXCOMPAVENC> " & vbCrLf
            For i = 0 To dsDatos.Tables("DeudaProxima").Rows.Count - 1

                mNroComprobante = dsDatos.Tables("DeudaProxima").Rows(i).Item("TIPO_COMPROB").ToString & "/" & dsDatos.Tables("DeudaProxima").Rows(i).Item("NRO_COMPROB").ToString & "/" & dsDatos.Tables("DeudaProxima").Rows(i).Item("DIGITO_VER").ToString
                mXML = mXML & "<REGISTRO_DETALLEPROXCOMPAVENC> " & vbCrLf & _
                                    "<DPC_COMPROBANTE>" & mNroComprobante.ToString & "</DPC_COMPROBANTE> " & vbCrLf & _
                                    "<DPC_DETALLE>" & dsDatos.Tables("DeudaProxima").Rows(i).Item("DESCRIPCION").ToString & "</DPC_DETALLE> " & vbCrLf & _
                                    "<DPC_VENC>1</DPC_VENC> " & vbCrLf & _
                                    "<DPC_FECHAVENC>" & Format(CDate(dsDatos.Tables("DeudaProxima").Rows(i).Item("FECHA_VTO").ToString), "dd/MM/yyyy") & "</DPC_FECHAVENC> " & vbCrLf & _
                                    "<DPC_IMPORTEORIGEN>" & dsDatos.Tables("DeudaProxima").Rows(i).Item("MONTO_ORIGEN").ToString & "</DPC_IMPORTEORIGEN> " & vbCrLf & _
                                    "<DPC_IMPORTERECARGO>" & dsDatos.Tables("DeudaProxima").Rows(i).Item("MONTO_INT").ToString & "</DPC_IMPORTERECARGO> " & vbCrLf & _
                                    "<DPC_IMPORTETOTAL>" & dsDatos.Tables("DeudaProxima").Rows(i).Item("MONTO_TOTAL").ToString & "</DPC_IMPORTETOTAL> " & vbCrLf & _
                              "</REGISTRO_DETALLEPROXCOMPAVENC> " & vbCrLf


                TieneDeuda = True
            Next
            mXML = mXML & "</DETALLEPROXCOMPAVENC> " & vbCrLf
        End If




        mImporteOrigen = 0
        mImporteRecargo = 0
        mImporteTotal = 0
        mNroComprobante = ""
        If (dsDatos.Tables("DeudaOtra").Rows.Count = 0) Then
            mXML = mXML & "<DETALLEOTROSCOMPAVENC> " & vbCrLf & _
                        "</DETALLEOTROSCOMPAVENC> " & vbCrLf
        Else


            mXML = mXML & "<DETALLEOTROSCOMPAVENC> " & vbCrLf
            For i = 0 To dsDatos.Tables("DeudaOtra").Rows.Count - 1

                mNroComprobante = dsDatos.Tables("DeudaOtra").Rows(i).Item("TIPO_COMPROB").ToString & "/" & dsDatos.Tables("DeudaOtra").Rows(i).Item("NRO_COMPROB").ToString & "/" & dsDatos.Tables("DeudaOtra").Rows(i).Item("DIGITO_VER").ToString
                mXML = mXML & "<REGISTRO_DETALLEOTROSCOMPAVENC> " & vbCrLf & _
                                   "<COMPROBANTE>" & mNroComprobante.ToString & "</COMPROBANTE> " & vbCrLf & _
                                   "<DOC_DETALLE>" & dsDatos.Tables("DeudaOtra").Rows(i).Item("DESCRIPCION").ToString & "</DOC_DETALLE> " & vbCrLf & _
                                   "<DOC_VENC>1</DOC_VENC> " & vbCrLf & _
                                   "<DOC_FECHAVENC>" & Format(CDate(dsDatos.Tables("DeudaOtra").Rows(i).Item("FECHA_VTO").ToString), "dd/MM/yyyy") & "</DOC_FECHAVENC> " & vbCrLf & _
                                   "<DOC_IMPORTEORIGEN>" & dsDatos.Tables("DeudaOtra").Rows(i).Item("MONTO_ORIGEN").ToString & "</DOC_IMPORTEORIGEN> " & vbCrLf & _
                                   "<DOC_IMPORTERECARGO>" & dsDatos.Tables("DeudaOtra").Rows(i).Item("MONTO_INT").ToString & "</DOC_IMPORTERECARGO> " & vbCrLf & _
                                   "<DOC_IMPORTETOTAL>" & dsDatos.Tables("DeudaOtra").Rows(i).Item("MONTO_TOTAL").ToString & "</DOC_IMPORTETOTAL> " & vbCrLf & _
                              "</REGISTRO_DETALLEOTROSCOMPAVENC>"


                TieneDeuda = True
            Next
            mXML = mXML & "</DETALLEOTROSCOMPAVENC> " & vbCrLf
        End If



        'Recursos Involucrados
        mXML = mXML & "<REFRECURSOS> " & vbCrLf
        For i = 0 To dsDatos.Tables("Recursos").Rows.Count - 1
            mXML = mXML & "<REGISTRO_REFRECURSOS> " & vbCrLf & _
                                "<REC_CODIGO>" & dsDatos.Tables("Recursos").Rows(i).Item("CODIGO_RECURSO").ToString & "</REC_CODIGO> " & vbCrLf & _
                                "<REC_DESCRIPCION>" & dsDatos.Tables("Recursos").Rows(i).Item("DESCRIPCION").ToString & "</REC_DESCRIPCION> " & vbCrLf & _
                          "</REGISTRO_REFRECURSOS> " & vbCrLf
        Next
        mXML = mXML & "</REFRECURSOS> " & vbCrLf



        'Si el Nombre del Contribuyente trae mensaje es porque la CUENTA NO EXISTE O NO TIENE DEUDA
        mNota = ""
        If (mNombreApellidoContribuyente.ToString = "Cuenta Inexistente") Then
            mNota = "<MENSAJE>" & mNombreApellidoContribuyente.ToString & "</MENSAJE>"

            'Si la Cuenta No existe Libero el Usuario WEB Asignado
            Call LimpiarDatosSession(mNroUsuario)
        End If


        'Si el Contribuyente Existe pero no tiene deuda
        If Not (TieneDeuda) Then
            mNota = "<MENSAJE>La cuenta no registra deuda al " & Format(Now, "dd/MM/yyyy") & ".</MENSAJE>"

            'Libero el Usuario WEB Asignado
            Call LimpiarDatosSession(mNroUsuario)
        End If




        'Obtengo los Datos de la Municipalidad
        mXMLDatosMunicipalidad = ObtenerEtiquetasXMLDatosMunicipalidad()
        mXML = mXML & mXMLDatosMunicipalidad.ToString & vbCrLf & _
             "<NOTAS> " & _
                 "<REGISTRO_NOTAS>" & mNota.ToString & "</REGISTRO_NOTAS> " & vbCrLf & _
             "</NOTAS> " & vbCrLf & _
             "<TIEMPODEPROCESAMIENTO> " & vbCrLf & _
                 "<TIEMPO>0.00</TIEMPO> " & vbCrLf & _
             "</TIEMPODEPROCESAMIENTO> " & vbCrLf & _
         "</VFPDATA> "



        Return mXML.ToString
    End Function



    Public Function ObtenerFechaActuaWeb() As String
        Dim mDataSet As New DataSet
        Dim mSQL As String = ""

        Try

            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.OBTENER_FECHA_ACTUA_WEB"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida            
            mComando.Parameters.Add(New OracleParameter("mFecha", OracleType.VarChar, 12)).Direction = ParameterDirection.ReturnValue



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Analizo el Resultado
            If (IsDBNull(mComando.Parameters.Item("mFecha").Value)) Then
                Return "NULL"
            Else
                Return mComando.Parameters.Item("mFecha").Value.ToString
            End If

            mComando = Nothing
            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            mConexion = Nothing
            Call GenerarLog(ex)
            Return "Error"
        End Try

    End Function



#End Region

#Region "Emitir Comprobantes"


    Private Function generarXML(ByVal p_strCadena As String) As String
        Dim datos() As String
        Dim comprobantes() As String
        Dim strXML As String


        'Divido el string entre el usuario y los comprobantes y genero la etiqueta del usuario
        strXML = ""
        datos = p_strCadena.Split("#")
        strXML = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCrLf
        strXML = strXML & "<VFPDATA>" & vbCrLf
        strXML = strXML & "<USUARIO><NRO_USUARIO>" & datos(0).Trim & "</NRO_USUARIO></USUARIO>"


        'Recorro los comprobantes (arranco desde 1 porque el 0 es el usuario)
        For i = 1 To datos.Length - 1

            'Divido los datos del comprobante
            comprobantes = datos(i).Split("|")
            strXML = strXML & "<DETALLEPERIODOSSELECCIONADOS>" & vbCrLf & _
                                "<NRO_RECURSO>" & comprobantes(0).Trim & "</NRO_RECURSO>" & vbCrLf & _
                                "<TIPO_COMPROBANTE>" & comprobantes(1).Trim & "</TIPO_COMPROBANTE>" & vbCrLf & _
                                "<NRO_COMPROBANTE>" & comprobantes(2).Trim & "</NRO_COMPROBANTE>" & vbCrLf & _
                          "</DETALLEPERIODOSSELECCIONADOS>" & vbCrLf

        Next
        strXML = strXML & "</VFPDATA>"

        Return strXML.Trim
    End Function





    'Esta Funcion es llamada desde el WEB SERVICES para generar el comprobante
    Public Function EmitirComprobantes(ByVal strCadena As String, ByVal mComprobantesVencidos As Boolean) As String
        Dim mOK As Boolean
        Dim dsDatosXML As DataSet
        Dim mNroUsuario As Integer
        Dim mRecurso As String = ""
        Dim mTipo_Y_NroComprobante As String = ""
        Dim strXML As String
        Try

            crearLog(strCadena)

            'Genero el XML
            strXML = Me.generarXML(strCadena.Trim)

            'Guardo el XML(string)en una variable
            dsDatosXML = New DataSet
            dsDatosXML.ReadXml(New IO.StringReader(strXML.Trim))
            
            'Extraigo los Datos del XML (Hubo problemas con este método, en monte no puede borrar el archivo xml temporal)
            'dsDatosXML = ObtenerDatosXML(strXML)

            If (dsDatosXML IsNot Nothing) Then
                mNroUsuario = dsDatosXML.Tables("USUARIO").Rows(0).Item("NRO_USUARIO").ToString
                mRecurso = dsDatosXML.Tables("DETALLEPERIODOSSELECCIONADOS").Rows(0).Item("NRO_RECURSO").ToString

                'Obtengo los Registros Seleccionados
                mTipo_Y_NroComprobante = ObtenerComprobantesSeleccionados(dsDatosXML)
            End If


            'Marco los registros seleccionados en la tabla temporal
            mOK = False
            If (mTipo_Y_NroComprobante.ToString <> "") Then
                mOK = MarcarRegistrosSeleccionados(mNroUsuario, mRecurso, mTipo_Y_NroComprobante)
            End If



            'Emito Los Comprobantes
            If (mOK) Then

                If (mComprobantesVencidos) Then
                    strXML = EmitirCompPagoDeudaVenc(mNroUsuario, mRecurso)

                Else
                    strXML = EmitirCompPagoDeudaNoVenc(mNroUsuario)
                End If


                crearLog(strXML)

                If (strXML.ToString = "") Then
                    Return "ERROR EMITIR"
                End If

            Else
                strXML = "ERROR SELECCION"
            End If

            Return strXML.ToString
        Catch ex As Exception
            Me.LogTXT(ex)
            Return "ERROR"
        End Try

    End Function

    'Esta Function Devuelve los Datos de un XML en un DATASET
    'Private Function ObtenerDatosXML(ByVal mXML As String) As DataSet
    '    Dim mArchivo As String
    '    Dim dsDatos As DataSet


    '    Try
    '        'Busque si existe el Directorio, SINO lo CREO
    '        If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisicaDirectorio.ToString & "datos_temporales")) Then
    '            My.Computer.FileSystem.CreateDirectory(mRutaFisicaDirectorio.ToString & "datos_temporales")
    '        End If

    '        'Genero el Nombre y la Ubicacion del Archivo Temporal
    '        mArchivo = mRutaFisicaDirectorio.ToString & "datos_temporales\" & Format(Now, "ddMMyyyyHHmmss") & ".xml"


    '        'Creo el Archivo Temporal
    '        Dim mCrearXML As New IO.StreamWriter(mArchivo.ToString)
    '        mCrearXML.WriteLine(mXML.ToString)
    '        mCrearXML.Close()


    '        'Obtengo los Datos del XML
    '        dsDatos = New DataSet
    '        dsDatos.ReadXml(mArchivo.ToString)


    '        'Elimino el Archivo Temporal (Da error en monte, no lo puede borrar por tema de permisos (?) )
    '        'My.Computer.FileSystem.DeleteFile(mArchivo.ToString) 



    '        Return dsDatos
    '    Catch ex As Exception
    '        Call GenerarLog(ex)
    '        Return Nothing
    '    End Try

    '    ''Elimino el Archivo Temporal (Da error en monte, no lo puede borrar por tema de permisos (?) )
    '    ''Acá se intenta hacer un catch del error.
    '    'My.Computer.FileSystem.DeleteFile(mArchivo.ToString)

    '    'Catch ex As Exception
    '    '    Call GenerarLog(ex)
    '    'Finally
    '    '    Return dsDatos
    '    'End Try

    'End Function

    'Esta Funcion me Devuelve los pares TIPO_COMPROB y NRO_COMPRO para marcar en la TABLA TEMPORAL ING_TMP_CC los registros Seleccionados
    Private Function ObtenerComprobantesSeleccionados(ByVal dsDatosXML As DataSet) As String
        Dim mTipo_Y_NroComprobante As String


        Try
            mTipo_Y_NroComprobante = ""
            For i = 0 To dsDatosXML.Tables("DETALLEPERIODOSSELECCIONADOS").Rows.Count - 1

                'Primera Vez
                If (i = 0) Then
                    mTipo_Y_NroComprobante = mTipo_Y_NroComprobante & _
                                             "( " & dsDatosXML.Tables("DETALLEPERIODOSSELECCIONADOS").Rows(i).Item("TIPO_COMPROBANTE").ToString() & _
                                             ", " & dsDatosXML.Tables("DETALLEPERIODOSSELECCIONADOS").Rows(i).Item("NRO_COMPROBANTE").ToString() & ")"


                Else
                    mTipo_Y_NroComprobante = mTipo_Y_NroComprobante & _
                                             ", ( " & dsDatosXML.Tables("DETALLEPERIODOSSELECCIONADOS").Rows(i).Item("TIPO_COMPROBANTE").ToString() & _
                                             ", " & dsDatosXML.Tables("DETALLEPERIODOSSELECCIONADOS").Rows(i).Item("NRO_COMPROBANTE").ToString() & ")"
                End If
            Next


            Return mTipo_Y_NroComprobante.ToString
        Catch ex As Exception
            Return ""
        End Try

    End Function

    'Esta Funcion Marca en la TABLA TEMPORAL ING_TMP_CC los registros seleccionados desde la WEB
    Private Function MarcarRegistrosSeleccionados(ByVal mNroUsuario As Integer, ByVal mRecurso As String, _
                                                 ByVal mTipo_Y_NroComprobante As String) As Boolean
        Dim mOK As String

        Try

            'Instancio Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()


            'Instancio el Comando
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.MARCAR_REGISTROS_SELECCIONADOS"
            mComando.CommandType = CommandType.StoredProcedure


            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("mNroUsuario", OracleType.Number)).Value = mNroUsuario
            mComando.Parameters.Add(New OracleParameter("mRecurso", OracleType.VarChar)).Value = mRecurso.ToString
            mComando.Parameters.Add(New OracleParameter("mTipo_Y_NroComprobante", OracleType.VarChar)).Value = mTipo_Y_NroComprobante.ToString
            mComando.Parameters.Add(New OracleParameter("mDatosOK", OracleType.VarChar, 5)).Direction = ParameterDirection.ReturnValue




            'Ejecuto el Store
            mComando.ExecuteNonQuery()
            mOK = mComando.Parameters.Item("mDatosOK").Value.ToString()
            mConexion.Close()

        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If
            Call GenerarLog(ex)
            mOK = "ERROR"
        Finally
            mComando = Nothing
            mConexion = Nothing
        End Try


        Return CBool(mOK.ToString = "OK")
    End Function

    'Esta Funcion Genera un NUEVO COMPROBANTE en la TABLA ING_COMPR_CAB_CC y Devuelve un XML con los Registros Seleccionados
    Private Function EmitirCompPagoDeudaVenc(ByVal mNroUsuario As Integer, ByVal mRecurso As String) As String
        Dim mXML As String

        Try

            'Instancio Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()


            'Instancio el Comando
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_PROCS.WEB_EmitirCompPagoDeudaVenc"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("P_AuxArchComp", OracleType.Clob)).Direction = ParameterDirection.Output
            mComando.Parameters.Add(New OracleParameter("P_Usuario", OracleType.Number)).Value = mNroUsuario
            mComando.Parameters.Add(New OracleParameter("P_Recurso", OracleType.VarChar)).Value = mRecurso.ToString



            'Ejecuto el Store
            mComando.ExecuteNonQuery()


            'Obtengo el XML
            mXML = ""
            'mXML = mComando.Parameters.Item("P_AuxArchComp").Value.ToString
            mXML = DirectCast(DirectCast(mComando.Parameters.Item("P_AuxArchComp").Value, System.Object), System.Data.OracleClient.OracleLob).Value.ToString


            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If
            Call GenerarLog(ex)
            mXML = ""
        Finally
            mComando = Nothing
            mConexion = Nothing
        End Try

        Return mXML.ToString
    End Function

    'Esta Funcion Devuelve un XML con los Registros Seleccionados
    Private Function EmitirCompPagoDeudaNoVenc(ByVal mNroUsuario As Integer)
        Dim mXML As String


        Try
            'Instancio Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()


            'Instancio el Comando
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.EMITIR_COMPROB_NO_VENCIDO"
            mComando.CommandType = CommandType.StoredProcedure


            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("mNroUsuario", OracleType.Number)).Value = mNroUsuario
            mComando.Parameters.Add(New OracleParameter("mXML", OracleType.Clob)).Direction = ParameterDirection.ReturnValue


            'Ejecuto el Store
            mComando.ExecuteNonQuery()


            'Obtengo el XML
            mXML = ""
            mXML = DirectCast(DirectCast(mComando.Parameters.Item("mXML").Value, System.Object), System.Data.OracleClient.OracleLob).Value.ToString


            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If
            Call GenerarLog(ex)
            mXML = ""
        Finally
            mComando = Nothing
            mConexion = Nothing
        End Try

        Return mXML.ToString
    End Function

#End Region

#Region "Datos Municipalidad"

    Public Function ObtenerTelefonoYMail() As String
        Return ObtenerDatosMunicipalidad()
    End Function

    Private Function DatosMunicipalidad() As DataSet
        Dim dsDatos As New DataSet
        Dim mSQL As String = ""

        Try

            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.OBTENER_DATOS_MUNICIPALIDAD"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("mDatosMunicipalidad", OracleType.Cursor)).Direction = ParameterDirection.ReturnValue


            'Ejecuto el Comando y Obtengo los datos en Dataset
            mAdaptador = New OracleDataAdapter(mComando)
            mAdaptador.Fill(dsDatos, "DATOS")
            mConexion.Close()





            Return dsDatos
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            mConexion = Nothing
            Call GenerarLog(ex)
            Return Nothing
        End Try

    End Function

    Private Function ObtenerDatosMunicipalidad() As String
        Dim mXML As String
        Dim mXMLDatosMunicipalidad As String


        'Obtengo los Datos de la Municpalidad
        mXMLDatosMunicipalidad = ObtenerEtiquetasXMLDatosMunicipalidad()

        'Estructura XML
        mXML = "<?xml version=""1.0"" encoding=""UTF-8""?> " & vbCrLf & _
                    "<VFPDATA> " & vbCrLf & _
                        mXMLDatosMunicipalidad.ToString & vbCrLf & _
                        "<ERROR> " & vbCrLf & _
                        "</ERROR> " & vbCrLf & _
                        "<TIEMPODEPROCESAMIENTO> " & vbCrLf & _
                            "<TIEMPO>0</TIEMPO> " & vbCrLf & _
                        "</TIEMPODEPROCESAMIENTO> " & vbCrLf & _
                    "</VFPDATA> "

        Return mXML.ToString
    End Function

    Private Function ObtenerEtiquetasXMLDatosMunicipalidad() As String
        Dim dsDatos As DataSet
        Dim mXML As String

        Dim municipalidadDireccion As String = ""
        Dim municipalidadTelefono As String = ""
        Dim municipalidadMail As String = ""
        Dim municipalidadWeb As String = ""




        'Obtengo los Datos de la Munucipalidad
        dsDatos = DatosMunicipalidad()
        If (dsDatos IsNot Nothing) Then

            For Each dr As DataRow In dsDatos.Tables(0).Rows
                Select Case dr.Item("NOMBRE_PARAMETRO").ToString.Trim
                    Case "WEB_DIRECCION_MUNI" : municipalidadDireccion = dr.Item("DATO").ToString.Trim
                    Case "WEB_NROTELRECWEB" : municipalidadTelefono = dr.Item("DATO").ToString.Trim
                    Case "WEB_DIRMAILRECWEB" : municipalidadMail = dr.Item("DATO").ToString.Trim
                    Case "WEB_PAGINA" : municipalidadWeb = dr.Item("DATO").ToString.Trim
                End Select
            Next

            mXML = "<MUNICIPALIDAD>" & vbCrLf & _
                        "<MUNICIPALIDAD_DIRECCION>" & municipalidadDireccion.Trim & "</MUNICIPALIDAD_DIRECCION> " & vbCrLf & _
                        "<MUNICIPALIDAD_TELEFONO>" & municipalidadTelefono.Trim & "</MUNICIPALIDAD_TELEFONO> " & vbCrLf & _
                        "<MUNICIPALIDAD_MAIL>" & municipalidadMail.Trim & "</MUNICIPALIDAD_MAIL> " & vbCrLf & _
                        "<MUNICIPALIDAD_WEB>" & municipalidadWeb.Trim & "</MUNICIPALIDAD_WEB> " & vbCrLf & _
                   "</MUNICIPALIDAD>"

        Else

            mXML = "<MUNICIPALIDAD>" & vbCrLf & _
                        "<MUNICIPALIDAD_TELEFONO></MUNICIPALIDAD_TELEFONO> " & vbCrLf & _
                        "<MUNICIPALIDAD_MAIL></MUNICIPALIDAD_MAIL> " & vbCrLf & _
                   "</MUNICIPALIDAD>"

        End If

        Return mXML.ToString
    End Function

#End Region

#Region "Obtener Datos Contribuyente"

    ' Esta Funcion Devuelve un string, con los datos del CONTRIBUYENTE 
    Public Function ObtenerDatosContribuyente(ByVal mNroUsuario As Integer) As String


        Try
            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.OBTENER_DATOS_CONTRIBUYENTE"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("mNroUsuario", OracleType.Number)).Value = mNroUsuario
            mComando.Parameters.Add(New OracleParameter("mCadenaDatosContribuyente", OracleType.VarChar, 500)).Direction = ParameterDirection.ReturnValue



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Analizo el Resultado
            If (IsDBNull(mComando.Parameters.Item("mCadenaDatosContribuyente").Value)) Then
                Return "|||"
            Else
                Return mComando.Parameters.Item("mCadenaDatosContribuyente").Value.ToString
            End If

            mComando = Nothing
            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            mConexion = Nothing
            Call GenerarLog(ex)
            Return "Error"
        End Try
    End Function

#End Region

#Region "Obtener Codigo Municipalidad"

    Public Function ObtenerCodigoMunicipalidad() As String


        Try
            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.OBTENER_CODIGO_MUNICIPALIDAD"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("mCodigoMunicipalidad", OracleType.VarChar, 5)).Direction = ParameterDirection.ReturnValue



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Analizo el Resultado
            If (IsDBNull(mComando.Parameters.Item("mCodigoMunicipalidad").Value)) Then
                Return ""
            Else
                Return mComando.Parameters.Item("mCodigoMunicipalidad").Value.ToString
            End If

            mComando = Nothing
            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            mConexion = Nothing
            Call GenerarLog(ex)
            Return "Error"
        End Try
    End Function

#End Region

#Region "Obtener Rodado Por Dominio"
    ' Esta Funcion Devuelve un string, con los datos del CONTRIBUYENTE 
    Public Function ObtenerRodadoPorDominio(ByVal mDominio As String) As String
        Dim mNroRodado_AUX As String

        Try

            mNroRodado_AUX = "0"

            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.OBTENER_RODADO_POR_DOMINIO"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("mDominio", OracleType.VarChar)).Value = mDominio.ToString.Trim
            mComando.Parameters.Add(New OracleParameter("mNroRodado", OracleType.Number)).Direction = ParameterDirection.ReturnValue



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Analizo el Resultado
            If Not (IsDBNull(mComando.Parameters.Item("mNroRodado").Value)) Then
                mNroRodado_AUX = mComando.Parameters.Item("mNroRodado").Value.ToString
            End If


            mComando = Nothing
            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            mConexion = Nothing
            Call GenerarLog(ex)
            mNroRodado_AUX = "0"
        End Try


        Return mNroRodado_AUX.ToString.Trim
    End Function

#End Region

#Region "Obtener LOGO"

    ' Esta Funcion Devuelve un array de byte con LOGO
    Public Function ObtenerLogo() As DataSet
        Dim dsdatosLogo As DataSet


        Try
            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.OBTENER_LOGO"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("mCadenaDatosContribuyente", OracleType.Cursor)).Direction = ParameterDirection.ReturnValue


            'Obtener Logo
            dsdatosLogo = New DataSet
            mAdaptador = New OracleDataAdapter(mComando)
            mAdaptador.Fill(dsdatosLogo, "DATOS")
            mConexion.Close()




            mComando = Nothing
            mConexion.Close()

            Return dsdatosLogo
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            mConexion = Nothing
            Call GenerarLog(ex)
            Return Nothing
        End Try
    End Function

#End Region

#Region "Visitas"

    'Este método agrega una visita en la base de datos
    Public Function agregarVisita(ByVal p_strFecha As String, ByVal p_strHora As String, ByVal p_strTipoImponible As String, ByVal p_lngNroImponible As Long) As String
        Dim strAgregarVisitaOK_aux As String

        Try

            strAgregarVisitaOK_aux = "ERROR"

            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.AGREGAR_VISITA"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("p_varFecha", OracleType.VarChar)).Value = p_strFecha.Trim
            mComando.Parameters.Add(New OracleParameter("p_varHora", OracleType.VarChar)).Value = p_strHora.Trim
            mComando.Parameters.Add(New OracleParameter("p_chrTipoImponible", OracleType.Char)).Value = p_strTipoImponible.Trim
            mComando.Parameters.Add(New OracleParameter("p_numNroImponible", OracleType.Number)).Value = p_lngNroImponible
            mComando.Parameters.Add(New OracleParameter("strAgregarVisitaOK", OracleType.VarChar, 10)).Direction = ParameterDirection.ReturnValue



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Analizo el Resultado
            If Not (IsDBNull(mComando.Parameters.Item("strAgregarVisitaOK").Value)) Then
                strAgregarVisitaOK_aux = mComando.Parameters.Item("strAgregarVisitaOK").Value.ToString.Trim
            End If


            mComando = Nothing
            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            mConexion = Nothing
            Call GenerarLog(ex)
            strAgregarVisitaOK_aux = ""
        End Try


        Return strAgregarVisitaOK_aux.Trim
    End Function


    'Este método actualiza una visita en la base de datos, actualizar significa pasar el estado "imprimio" a "S"
    Public Function actualizarVisita(ByVal p_strFecha As String, ByVal p_strHora As String, ByVal p_strTipoImponible As String, ByVal p_lngNroImponible As Long) As String
        Dim strActualizarVisitaOK_aux As String

        Try

            strActualizarVisitaOK_aux = "ERROR"

            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.ACTUALIZAR_VISITA"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("p_varFecha", OracleType.VarChar)).Value = p_strFecha.Trim
            mComando.Parameters.Add(New OracleParameter("p_varHora", OracleType.VarChar)).Value = p_strHora.Trim
            mComando.Parameters.Add(New OracleParameter("p_chrTipoImponible", OracleType.Char)).Value = p_strTipoImponible.Trim
            mComando.Parameters.Add(New OracleParameter("p_numNroImponible", OracleType.Number)).Value = p_lngNroImponible
            mComando.Parameters.Add(New OracleParameter("strActualizarVisitaOK", OracleType.VarChar, 10)).Direction = ParameterDirection.ReturnValue



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Analizo el Resultado
            If Not (IsDBNull(mComando.Parameters.Item("strActualizarVisitaOK").Value)) Then
                strActualizarVisitaOK_aux = mComando.Parameters.Item("strActualizarVisitaOK").Value.ToString.Trim
            End If


            mComando = Nothing
            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            mConexion = Nothing
            Call GenerarLog(ex)
            strActualizarVisitaOK_aux = ""
        End Try


        Return strActualizarVisitaOK_aux.Trim
    End Function


    'Esta funcion devuelve el numero total de visitas a la Web 
    Public Function getVisitasCantidad() As String
        Dim strCantidad_aux As String

        Try

            strCantidad_aux = "0"

            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.GET_VISITAS_CANTIDAD"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida        
            mComando.Parameters.Add(New OracleParameter("strCantidad_aux", OracleType.VarChar, 10)).Direction = ParameterDirection.ReturnValue



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Analizo el Resultado
            If Not (IsDBNull(mComando.Parameters.Item("strCantidad_aux").Value)) Then
                strCantidad_aux = mComando.Parameters.Item("strCantidad_aux").Value.ToString.Trim
            End If


            mComando = Nothing
            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            mConexion = Nothing
            Call GenerarLog(ex)
            strCantidad_aux = "0"
        End Try


        Return strCantidad_aux.Trim
    End Function


    'Esta funcion devuelve la cantidad de visitas a la Web agrupada por tipo de imponible y si imprimio o no 
    Public Function getVisitasCantidadDetalle(ByVal p_strFechaDesde As String, ByVal p_strFechaHasta As String) As String
        Dim strDetalle_aux As String

        Try

            strDetalle_aux = ""

            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.GET_VISITAS_CANTIDAD_DETALLE"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida        
            mComando.Parameters.Add(New OracleParameter("p_varFechaDesde", OracleType.VarChar)).Value = p_strFechaDesde.Trim
            mComando.Parameters.Add(New OracleParameter("p_varFechaHasta", OracleType.VarChar)).Value = p_strFechaHasta.Trim
            mComando.Parameters.Add(New OracleParameter("strDetalle_aux", OracleType.VarChar, 4000)).Direction = ParameterDirection.ReturnValue



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Analizo el Resultado
            If Not (IsDBNull(mComando.Parameters.Item("strDetalle_aux").Value)) Then
                strDetalle_aux = mComando.Parameters.Item("strDetalle_aux").Value.ToString.Trim
            End If


            mComando = Nothing
            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            mConexion = Nothing
            Call GenerarLog(ex)
            strDetalle_aux = ""
        End Try


        Return strDetalle_aux.Trim
    End Function


#End Region

#Region "Obtener Tipos de Imponible"

    ' Esta Funcion Devuelve un string, con los tipos de IMPONIBLES 
    Public Function ObtenerTiposImponible() As String


        Try
            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.OBTENER_TIPOS_IMPONIBLE"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("mTipos", OracleType.VarChar, 500)).Direction = ParameterDirection.ReturnValue



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Analizo el Resultado
            If (IsDBNull(mComando.Parameters.Item("mTipos").Value)) Then
                Return ""
            Else
                Return mComando.Parameters.Item("mTipos").Value.ToString
            End If

            mComando = Nothing
            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            mConexion = Nothing
            Call GenerarLog(ex)
            Return "Error"
        End Try
    End Function

#End Region


#End Region

#Region "Genero LOG"
    Private Sub GenerarLog(ByVal mError As Exception)

        'Genero un txt con el error 
        Call LogTXT(mError)

    End Sub

    Private Sub LogTXT(ByVal mError As Exception)

        If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisicaDirectorio.ToString & "log")) Then
            My.Computer.FileSystem.CreateDirectory(mRutaFisicaDirectorio.ToString & "log")
        End If



        Dim mLog As New IO.StreamWriter(mRutaFisicaDirectorio.ToString & "log\log" & Format(Now, "ddMMyyyyHHmmss") & ".txt")
        Dim i As Integer
        Dim mCadenaError(0 To 4) As String



        'Armo el Log
        mCadenaError(0) = "FECHA: " & Format(Now, "dd/MM/yy hh:mm:ss")
        mCadenaError(1) = "MENSAJE: " & mError.Message.ToString
        mCadenaError(2) = "SOURCE: " & mError.Source.ToString
        mCadenaError(3) = "TARGETSITE: " & mError.TargetSite.ToString
        mCadenaError(4) = "STACKTRACE (Ruta): " & mError.StackTrace.ToString


        'Escribo el Log
        mLog.WriteLine("Descripcion de ERROR:")
        mLog.WriteLine("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
        For i = 0 To 4
            mLog.WriteLine(mCadenaError(i).ToString)
        Next
        mLog.WriteLine("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")


        mLog.Close()
    End Sub


    Private Sub crearLog(ByVal mensaje As String)
        If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisicaDirectorio.ToString & "log")) Then
            My.Computer.FileSystem.CreateDirectory(mRutaFisicaDirectorio.ToString & "log")
        End If
        Dim mLog As New IO.StreamWriter(mRutaFisicaDirectorio.ToString & "log\mensaje" & Format(Now, "ddMMyyyyHHmmss") & ".txt")

        'Escribo el Log
        mLog.WriteLine(mensaje)



        mLog.Close()
    End Sub

#End Region

End Class

