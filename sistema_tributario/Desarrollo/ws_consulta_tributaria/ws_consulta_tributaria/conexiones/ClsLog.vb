﻿Public Class Clslog


#Region "Genero LOG"
    Public Sub GenerarLog(ByVal mError As Exception, ByVal mRutaFisica As String)
        Dim mDescripcionError As String = ""


        'Genero un txt con el error y Obtengo el Detalle para mandarlo por mail
        mDescripcionError = LogTXT(mError, mRutaFisica.ToString)

        'Envio el Mail
        Call Mail(mDescripcionError.ToString)



    End Sub


    Private Function LogTXT(ByVal mError As Exception, ByVal mRutaFisica As String) As String
        Dim mCadena As String = ""


        Try

            If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString & "log")) Then
                My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString & "log")
            End If


      
            'Armo el Log
            mCadena = "EQUIPO: " & My.Computer.Name.ToString.ToUpper & vbCrLf & _
                      "FECHA: " & Format(Now, "dd/MM/yy hh:mm:ss") & vbCrLf & _
                      "MENSAJE: " & mError.Message.ToString & vbCrLf & _
                      "SOURCE: " & mError.Source.ToString & vbCrLf & _
                      "TARGETSITE: " & mError.TargetSite.ToString & vbCrLf & _
                      "STACKTRACE (Ruta): " & mError.StackTrace.ToString


            Dim mLog As New IO.StreamWriter(mRutaFisica.ToString & "log\log" & Format(Now, "ddMMyyyyHHmmss") & ".txt")
            mLog.Write(mCadena.ToString)
            mLog.Close()

        Catch ex As Exception
        End Try


        Return mCadena.ToString
    End Function


    Private Sub Mail(ByVal mDetalleError As String)


        Try
            Dim correo As New System.Net.Mail.MailMessage()
            correo.To.Add("sistemas.gmdq@gmail.com")
            correo.From = New System.Net.Mail.MailAddress("sistemas.gmdq@gmail.com", "Sistema Web Tributaria", System.Text.Encoding.UTF8)
            correo.Subject = "Sistema Web Tributaria.-"
            correo.SubjectEncoding = System.Text.Encoding.UTF8
            correo.Body = mDetalleError.ToString
            correo.BodyEncoding = System.Text.Encoding.UTF8
            correo.IsBodyHtml = False
            correo.Priority = System.Net.Mail.MailPriority.Normal


            Dim smtp As New System.Net.Mail.SmtpClient
            smtp.Credentials = New System.Net.NetworkCredential("sistemas.gmdq@gmail.com", "s1i2s3t3e4m5a6s7")
            smtp.Port = 587
            smtp.Host = "smtp.gmail.com"
            smtp.EnableSsl = True
            smtp.Send(correo)

        Catch ex As System.Net.Mail.SmtpException
        End Try



        'Try
        'Dim mMail As New MailMessage
        '    SmtpMail.SmtpServer = "mail.ilogiko.com"


        '    mMail.From = "ilogiko@ilogiko.com"
        '    mMail.To = "juan@ilogiko.com"
        '    mMail.Subject = "Sistema Web Tributario.-"



        '    Le asigno el cuerpo del Mail con el error detectado
        '    mMail.Body = mDetalleError.ToString



        '    Envio el Mail
        '    SmtpMail.Send(mMail)


        '    SmtpMail.SmtpServer = "smtp.gmail.com"


        '    mMail.From = "sistemas.gmdq@gmail.com"
        '    mMail.To = "sistemas.gmdq@gmail.com"
        '    mMail.Subject = "Sistema Web Tributario.-"



        '    Le asigno el cuerpo del Mail con el error detectado
        '    mMail.Body = mDetalleError.ToString



        '    Envio el Mail
        '    SmtpMail.Send(mMail)


        'Catch ex As Exception
        'End Try

    End Sub

#End Region

End Class