﻿Public Partial Class webfrmconsulta_b
    Inherits System.Web.UI.Page


#Region "Variables"

    Private mWS As web_tributaria.ws_consulta_tributaria.Service1
    Private mLog As Clslog
    Private mXml As String

#End Region

#Region "Eventos Formulario"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not (IsPostBack) Then



            txtdominio.Visible = False
            'id_td_titulo_dominio.Visible = False
            id_tr_dominio.Visible = False


            Call CargarDatosMunicipalidad()

            'Obtener Nro de Cuenta por Dominio
            txtdominio.Attributes.Add("Onchange", "javascript:solo_mayusculas(this);")
            btn_obtener_cuenta.Attributes.Add("onclick", "javascript:return validar_dominio();")
            btn_obtener_cuenta.Attributes.Add("onmouseover", "javascript:boton_dominio_onmouseover(this);")
            btn_obtener_cuenta.Attributes.Add("onmouseout", "javascript:boton_dominio_onmouseout(this);")
            cmbtipo_cuentas.Attributes.Add("onchange", "javascript:redimensionar_div();")


            link_consultar_deuda.Attributes.Add("onclick", "javascript:return validar_datos();")
            txtcaptcha.Attributes.Add("onkeydown", "javascript:consultar_deuda(this, event);")
            

            lblversion.Text = "Versión " & ClsTools.mVersion.ToString.Trim
            txtnro_cuenta.Focus()
        Else
            Call LimpiarControles()
        End If

    End Sub

    Protected Sub link_consultar_deuda_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_consultar_deuda.Click
        Dim dsDatos As DataSet

        Try
            If (Validar()) Then


                'Limpio la Session por las dudas que el usuario este logueado y quiera volver a loguearse
                Call LimpiarSession(False)


                'Consulto si el Contribuyente tiene DEUDA
                mWS = New ws_consulta_tributaria.Service1
                mXml = mWS.ConsultarDeuda(cmbtipo_cuentas.Text, CLng(txtnro_cuenta.Text.Trim), -1)
                mWS = Nothing


                'Obtengo los Datos
                dsDatos = New DataSet
                dsDatos = ObtenerDatosXML()


                'Si la cuenta No Existe no tiene Deuda lo muestro el mensaje
                If (dsDatos.Tables("REGISTRO_NOTAS") IsNot Nothing) Then
                    Call GenerarMensajeCuenta(dsDatos)
                    Exit Sub
                End If


                'Asigno Variables de Session
                Session("tipo_cuenta") = cmbtipo_cuentas.Text
                Session("nro_cuenta") = txtnro_cuenta.Text.Trim
                Session("nro_usuario") = dsDatos.Tables("DATOSCUENTA").Rows(0).Item("CLAVE").ToString



                Session("dominio") = ""
                If (Session("tipo_cuenta").ToString.Trim = "Vehículo") Then
                    Session("dominio") = txtdominio.Text.Trim
                End If


                'Cargo los Datos de la Municipalidad 
                Call CargarDatosMunicipalidad()

                Response.Redirect("webfrmlistado.aspx", False)
            Else

                Call CrearAuditoria()
                Call LimpiarSession(True)
            End If


        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)
        End Try
    End Sub

#End Region

#Region "Procedimientos y Funciones"

    Private Function Validar() As Boolean

        'Nro. Cuenta
        If (txtnro_cuenta.Text <> "") Then
            If Not (IsNumeric(txtnro_cuenta.Text)) Then
                Return False
            End If
        Else
            Return False
        End If



        verificador_captcha.ValidateCaptcha(txtcaptcha.Text.Trim())
        If Not (verificador_captcha.UserValidated) Then
            lblmensaje.Text = "Código de Verificación Incorrecto."
            txtcaptcha.BackColor = Drawing.Color.Red
            txtcaptcha.ForeColor = Drawing.Color.White
            txtcaptcha.Text = ""
            txtcaptcha.Focus()

            Return False
        End If


        Return True
    End Function


    Private Sub GenerarMensajeCuenta(ByVal dsdatos As DataSet)

        Select Case dsdatos.Tables("REGISTRO_NOTAS").Rows(0).Item("MENSAJE").ToString
            Case "Cuenta Inexistente"
                lblmensaje.Text = "Cuenta Inexistente."
                txtnro_cuenta.BackColor = Drawing.Color.Red
                txtnro_cuenta.ForeColor = Drawing.Color.White
            Case "La cuenta no registra deuda al " & Format(Now, "dd/MM/yyyy") & "."
                lblmensaje.Text = "La Cuenta No Registra Deuda."
        End Select


        txtcaptcha.Text = ""
        txtnro_cuenta.Focus()
    End Sub

#End Region

#Region "LOG"

    Private Function ObtenerDetalleWEB() As String
        Dim mInfoLog As String = ""
        Try

            For Each mNombreVariable In Request.ServerVariables
                mInfoLog = mInfoLog & mNombreVariable.ToString & ": " & Request.ServerVariables(mNombreVariable.ToString)
                mInfoLog = mInfoLog & vbCrLf
            Next

            Return mInfoLog.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function
#End Region

#Region "Auditoria"

    Private Sub CrearAuditoria()
        Dim mRutaFisica As String
        mRutaFisica = Request.ServerVariables("APPL_PHYSICAL_PATH").ToString & "auditoria"

        'Controlo la Cantidad de Archivos
        Call CantidadLog(1000, mRutaFisica.ToString)

        'Genero el TXT
        Call GenerarLogAuditoria(mRutaFisica.ToString)
    End Sub

    Private Sub CantidadLog(ByVal mCantidadLog As Integer, ByVal mRutaFisica As String)

        If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString)) Then
            My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString)
        End If


        Dim mArchivos As System.Collections.ObjectModel.ReadOnlyCollection(Of String)
        Dim mNombreCarpeta As String = ""
        mArchivos = My.Computer.FileSystem.GetFiles(mRutaFisica.ToString)



        Dim c As Integer
        Dim arreglo(mArchivos.Count() - 1) As String
        For c = 0 To UBound(arreglo, 1)
            arreglo(c) = mArchivos(c)
        Next


        If mCantidadLog = 0 Then
            Dim k As Integer
            For k = 0 To (UBound(arreglo, 1)) - 1
                mNombreCarpeta = My.Computer.FileSystem.GetName(Trim(arreglo(k).ToString))
                My.Computer.FileSystem.DeleteFile(Trim(arreglo(k).ToString))
            Next


        Else
            If mArchivos.Count > mCantidadLog Then
                Dim i As Integer
                For i = UBound(arreglo, 1) - (mCantidadLog) To 0 Step -1
                    mNombreCarpeta = My.Computer.FileSystem.GetName(arreglo(i))
                    My.Computer.FileSystem.DeleteFile(Trim(arreglo(i).ToString))
                Next
            End If
        End If


    End Sub

    Private Sub GenerarLogAuditoria(ByVal mRutaFisica As String)
        Dim mCadena As String


        If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString)) Then
            My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString)
        End If


        'Armo el Log
        Dim mLog As New IO.StreamWriter(mRutaFisica.ToString & "\auditorialog" & Format(Now, "ddMMyyyyHHmmss") & ".txt")
        mCadena = "FECHA: " & Format(Now, "dd/MM/yy hh:mm:ss") & "|"
        mCadena = mCadena & "IP: " & Request.ServerVariables("REMOTE_ADDR").ToString() & "|"
        mCadena = mCadena & "HOST: " & Request.ServerVariables("REMOTE_HOST").ToString() & "|"
        mCadena = mCadena & "PUERTO: " & Request.ServerVariables("REMOTE_PORT").ToString()

        mLog.WriteLine(mCadena.ToString)


        mLog.WriteLine("")
        mLog.WriteLine("-- DATOS INGRESADOS --")


        mLog.WriteLine("NRO. CUENTA:        " & txtnro_cuenta.Text)
        'mLog.WriteLine("TIPO CUENTA:        " & cmbtipo_cuenta.Text)

        mLog.Close()

    End Sub

#End Region


    Private Sub CargarDatosMunicipalidad()
        Dim dsdatos As DataSet

        Try

            mWS = New ws_consulta_tributaria.Service1
            mXml = mWS.ObtenerDatosMunicipalidad()
            mWS = Nothing

            dsdatos = New DataSet
            dsdatos = ObtenerDatosXML()
            If (dsdatos.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_TELEFONO").ToString = "") Then
                lbltelefono.Text = "00-000000"
            Else
                lbltelefono.Text = dsdatos.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_TELEFONO").ToString
            End If

            If (dsdatos.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_MAIL").ToString = "") Then
                lblmail.Text = "mail@mail.com.ar"
            Else
                lblmail.Text = dsdatos.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_MAIL").ToString
            End If
            dsdatos = Nothing

            Session("municipalidad_telefono") = lbltelefono.Text
            Session("municipalidad_mail") = lblmail.Text

        Catch ex As Exception
            dsdatos = Nothing
        End Try
    End Sub


    Private Function ObtenerDatosXML() As DataSet
        Dim mRutaFisica As String
        Dim mArchivo As String
        Dim dsDatos As DataSet

        Try

            mRutaFisica = Request.ServerVariables("APPL_PHYSICAL_PATH").ToString

            If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString & "datos_temporales")) Then
                My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString & "datos_temporales")
            End If

            mArchivo = mRutaFisica.ToString & "datos_temporales\" & Format(Now, "ddMMyyyyHHmmss") & ".xml"


            Dim mCrearXML As New IO.StreamWriter(mArchivo.ToString)
            mCrearXML.WriteLine(mXml.ToString)
            mCrearXML.Close()

            dsDatos = New DataSet
            dsDatos.ReadXml(mArchivo.ToString)

            My.Computer.FileSystem.DeleteFile(mArchivo.ToString)


            Return dsDatos
        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)
            Return Nothing
        End Try
    End Function


    Private Sub LimpiarControles()
        txtnro_cuenta.BackColor = Drawing.Color.White
        txtnro_cuenta.ForeColor = Drawing.Color.Black
        txtcaptcha.BackColor = Drawing.Color.White
        txtcaptcha.ForeColor = Drawing.Color.Black
    End Sub

#Region "Limpiar Session"

    Private Sub LimpiarSession(ByVal mDatosInvalidos As Boolean)
        Dim mNroUsuario As Integer


        Try

            'Limpio en la Base de Datos
            If (Session("tipo_conexion") IsNot Nothing) Then
                If (Application("tipo_conexion").ToString = "RAFAM") Then
                    If (Session("nro_usuario") IsNot Nothing) Then
                        If (Session("nro_usuario").ToString <> "") Then

                            mNroUsuario = CInt(Session("nro_usuario"))

                            mWS = New ws_consulta_tributaria.Service1
                            mWS.LimpiarDatosSession(mNroUsuario)
                            mWS = Nothing

                            Application("cant_usuarios") = CInt(Application("cant_usuarios")) - 1
                        End If
                    End If
                End If
            End If


            'Si los datos son Invalidos borro cualquier Session
            If (mDatosInvalidos) Then
                Session.RemoveAll()
                Session.Clear()
                Session.Abandon()
            End If


        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
        End Try

    End Sub

#End Region

    Protected Sub btn_obtener_cuenta_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_obtener_cuenta.Click
        Dim mNroCuenta_AUX As String
        Dim mDominio_AUX As String

        Try

            'Obtengo el Nro de Cuenta en base a un Tipo de Cuenta y un dominio
            mNroCuenta_AUX = ""
            mDominio_AUX = txtdominio.Text.Trim
            txtnro_cuenta.Text = "Cargando ..."
            If (mDominio_AUX.ToString.Trim <> "") Then
                mWS = New ws_consulta_tributaria.Service1


                'Busco la patente con todos caracteres juntos
                mNroCuenta_AUX = mWS.ObtenerNroCuentaPorPatente(mDominio_AUX.ToString.Trim)


                'Si el dominio ingresado es de los nuevos busco con la cadena segmentada
                If (mNroCuenta_AUX.ToString.Trim = "0") And (mDominio_AUX.ToString.Trim.Length = 6) Then

                    'Busco la patente con un espacio
                    mDominio_AUX = txtdominio.Text.ToString.Trim.Substring(0, 3) & " " & txtdominio.Text.ToString.Trim.Substring(3, 3)
                    mNroCuenta_AUX = mWS.ObtenerNroCuentaPorPatente(mDominio_AUX.ToString.Trim)
                End If



                If (mNroCuenta_AUX.ToString = "0") Then
                    lblmensaje.Text = "Dominio Inexistente."
                    txtnro_cuenta.Text = ""
                Else
                    lblmensaje.Text = ""
                    txtnro_cuenta.Text = mNroCuenta_AUX.ToString.Trim
                End If
                txtcaptcha.Focus()
            Else
                txtnro_cuenta.Text = ""
                txtdominio.Focus()
            End If

        Catch ex As Exception
            txtnro_cuenta.Text = ""
        Finally
            mWS = Nothing
        End Try

    End Sub

    Protected Sub cmbtipo_cuentas1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbtipo_cuentas.SelectedIndexChanged

        txtnro_cuenta.Text = ""
        If (cmbtipo_cuentas.Text = "Vehículo") Then
            txtdominio.Text = ""
            txtdominio.Visible = True
            id_tr_dominio.Visible = True
            div_contenedor.Style.Add("height", "410")
            txtdominio.Focus()
        Else
            txtdominio.Visible = False
            id_tr_dominio.Visible = False
            div_contenedor.Style.Add("height", "365")
        End If

    End Sub

End Class