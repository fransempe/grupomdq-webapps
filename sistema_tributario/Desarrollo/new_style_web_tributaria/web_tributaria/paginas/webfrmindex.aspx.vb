﻿Option Explicit On
Option Strict On

Imports System.Drawing.Color
Imports System.Drawing.Imaging
Imports System.Drawing
Imports System.IO
Imports web_tributaria.ws_consulta_tributaria.Service1


Partial Public Class webfrmindex
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mRutaFisica As String
        Dim mNivel As String



        'Obtengo la Ruta del archivo de Configuracion
        mRutaFisica = Server.MapPath("./").ToString
    


        'Obtengo el Tipo de conexion
        Application("tipo_conexion") = "Ninguna"
        Application("tipo_conexion") = System.Configuration.ConfigurationManager.AppSettings("TipoConexion").ToString.Trim


        'Obtengo el Nivel de Seguridad
        mNivel = "ALTO"
        mNivel = System.Configuration.ConfigurationManager.AppSettings("SeguridadLogin").ToString.Trim


        'Redirijo segun el Nivel
        Select Case mNivel.ToString.Trim
            Case "Bajo" : Response.Redirect("webfrmconsulta_b.aspx", False)
            Case "Medio" : Response.Redirect("webfrmconsulta_m.aspx", False)
            Case "Alto" : Response.Redirect("webfrmconsultab_a.aspx", False)
            Case "Mantenimiento" : Response.Redirect("webfrmmantenimiento.aspx", False)
        End Select



        'Me muevo un nivel para Atras
        mRutaFisica = Server.MapPath("../").ToString

        'Borro los Temporales
        Call LimpiarTemporales(mRutaFisica.ToString & "datos_temporales")
        Call LimpiarTemporales(mRutaFisica.ToString & "comprobantespdf")
    End Sub


    Private Sub LimpiarTemporales(ByVal mRutaFisica As String)
        Dim mDirectorio As DirectoryInfo
        Dim mArchivosInfo As FileInfo()
        Dim mArchivoBandera As String
        Dim mFechaLimpieza As Date


        Try

            mArchivoBandera = mRutaFisica.ToString & "\limpiar.txt"
            If (My.Computer.FileSystem.FileExists(mArchivoBandera.ToString)) Then
                mFechaLimpieza = CDate(My.Computer.FileSystem.ReadAllText(mArchivoBandera.ToString))
            Else
                mFechaLimpieza = Now.AddDays(-1)
            End If


            If (mFechaLimpieza <> CDate(Format(Now, "dd/MM/yyyy"))) Then
                mDirectorio = New DirectoryInfo(mRutaFisica.ToString)
                mArchivosInfo = mDirectorio.GetFiles()
                For Each mArchivo As FileInfo In mArchivosInfo
                    If (Format(mArchivo.CreationTime, "dd/MM/yyyy") < Format(Now, "dd/MM/yyyy")) Then
                        My.Computer.FileSystem.DeleteFile(mArchivo.FullName.ToString)
                    End If
                Next



                'Actualizo el archivo Bandera
                My.Computer.FileSystem.WriteAllText(mArchivoBandera.ToString, Format(Now, "dd/MM/yyyy"), False)
            End If


        Catch ex As Exception
        End Try
    End Sub


End Class