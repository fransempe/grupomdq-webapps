﻿Option Explicit On


Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel


<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Service1
    Inherits System.Web.Services.WebService

#Region "Variables"

    Private ObjConexion As Object
    Private ObjConfiguracion As clsConfiguracion


    Private Enum eConexiones
        mERROR = -1
        RAFAM = 0
        FOX = 1
    End Enum

#End Region

#Region "Funciones Privadas"

    'GenerarLog:
    'Esta funcion genera un log al momento que se genera un error
    Private Sub GenerarLog(ByVal pError As Exception)
        Me.ObjConfiguracion = New clsConfiguracion
        Me.ObjConfiguracion.GenerarLog(pError)
        Me.ObjConfiguracion = Nothing
    End Sub


    'SeteoConexion:
    'Esta funcion Instancia la clave Conexion segun el tipo de conexion seteada
    Private Function SeteoConexion() As eConexiones
        Dim mTipoConexion As String


        Try

            'Instancio el Objeto Conexion dependiendo del tipo de conexion seteado
            mTipoConexion = ""                                                                                   
            mTipoConexion = System.Configuration.ConfigurationManager.AppSettings("Tipo_Conexion").ToString.Trim
            If (mTipoConexion.ToString = eConexiones.RAFAM.ToString) Then
                Me.ObjConexion = New clsConexionRAFAM()
                'Else
                'Me.ObjConexion = New clsConexionFOX()
            End If

        Catch ex As Exception
            Call Me.GenerarLog(ex)
            Return -1
        End Try


        Return 1
    End Function

#End Region

#Region "Funciones Publicas"

    'Login:
    'Esta funcion valida el logueo y devuelve el Nro de Comercio.
    <WebMethod(Description:="Rafam: Esta funcion valida el logueo y devuelve el Nro de Comercio")> _
    Public Function Login(ByVal pCuit As String, ByVal pClave As String) As String
        Dim mResultado As String


        Try

            mResultado = ""
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.Cuit = pCuit.ToString.Trim
            Me.ObjConexion.Clave = pClave.ToString.Trim
            mResultado = Me.ObjConexion.Login()
            Me.ObjConexion = Nothing


        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mResultado = "ERROR"
        Finally
            Me.ObjConexion = Nothing
        End Try


        Return mResultado.ToString.Trim
    End Function


    'LoginRAFAM
    'Esta funcion nos devuelve un boolean como resultado del login de un usuario de rafam
    <WebMethod(Description:="Rafam: Esta funcion nos devuelve un boolean como resultado del login de un usuario de rafam")> _
    Public Function LoginRAFAM(ByVal pUsuarioRAFAM As String, ByVal pClaveRAFAM As String) As Boolean
        Dim mLogin_RAFAM As Boolean

        Try

            mLogin_RAFAM = False
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.UsuarioRAFAM = pUsuarioRAFAM.ToString.Trim
            Me.ObjConexion.ClaveRAFAM = pClaveRAFAM.ToString.Trim
            mLogin_RAFAM = Me.ObjConexion.LoginRafam()
            ObjConexion = Nothing

        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mLogin_RAFAM = False
        Finally
            Me.ObjConexion = Nothing
        End Try


        Return mLogin_RAFAM
    End Function



    ''ObtenerDatosMunicipalidad:
    ''Esta funcion devuelve los datos de la municipalidad que van al pie de la web    
    '<WebMethod(Description:="Fox: Esta funcion devuelve los datos de la municipalidad que van al pie de la web")> _
    'Public Function ObtenerDatosMunicipalidad() As String
    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.ObtenerDatosMunicipalidad()
    '    Else
    '        Return ""
    '    End If
    'End Function


    ''ObtenerDatosComercio:
    ''Esta funcion devuelve los datos del comercio y del contribuyente en base a un Nro de Comercio
    '<WebMethod(Description:="Fox: Esta funcion devuelve los datos del comercio y del contribuyente en base a un Nro de Comercio")> _
    'Public Function ObtenerDatosComercio(ByVal pNroComercio As String) As String
    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then

    '        Me.ObjConexion.NroComercio = pNroComercio.ToString.Trim
    '        Return Me.ObjConexion.ObtenerDatosComercio()
    '    Else
    '        Return ""
    '    End If
    'End Function


    'BuscarComercio:
    ''Esta funcion realiza la busqueda de un comercio
    '<WebMethod(Description:="Fox: Esta funcion realiza la busqueda de un comercio")> _
    'Public Function BuscarComercio(ByVal pDato As String, ByVal pTipoBusqueda As String) As String

    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then

    '        Me.ObjConexion.NroComercio = pDato.ToString.Trim
    '        Me.ObjConexion.TipoBusqueda = pTipoBusqueda.ToString.Trim
    '        Return Me.ObjConexion.BuscarComercio()
    '    Else
    '        Return ""
    '    End If
    'End Function



    'CrearComercioWeb:
    'Esta funcion crea un usuario Web junto con los ccomercios relacionados y nos devuelve su clave generada automaticamente
    <WebMethod(Description:="Rafam: Esta funcion crea un usuario Web junto con los ccomercios relacionados y nos devuelve su clave generada automaticamente")> _
    Public Function CreateUserWeb(ByVal pDataManager As String, ByVal pDataTrade As String) As String
        Dim mKey_AUX As String

        Try

            mKey_AUX = ""
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.DataManager = pDataManager.ToString.Trim
            Me.ObjConexion.DataTrade = pDataTrade.ToString.Trim
            mKey_AUX = Me.ObjConexion.CreateUserWeb()


        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mKey_AUX = ""
        Finally
            Me.ObjConexion = Nothing
        End Try


        Return mKey_AUX.ToString.Trim
    End Function


    'EditUserWeb:
    'Esta funcion edita los datos y los comercios relacionados al usuario Web
    <WebMethod(Description:="Rafam: Esta funcion edita los datos y los comercios relacionados al usuario Web")> _
    Public Function EditUserWeb(ByVal pDataManager As String, ByVal pDataTrade As String) As Boolean
        Dim mOK As Boolean

        Try

            mOK = False
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.DataManager = pDataManager.ToString.Trim
            Me.ObjConexion.DataTrade = pDataTrade.ToString.Trim
            mOK = Me.ObjConexion.EditUserWeb()


        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mOK = False
        Finally
            Me.ObjConexion = Nothing
        End Try


        Return mOK
    End Function


    'ClearKey:
    'Esta funcion borra la clave del usuario Web y vuelve a generar una nueva de forma automatica
    <WebMethod(Description:="Rafam: Esta funcion borra la clave del usuario Web y vuelve a generar una nueva de forma automatica")> _
    Public Function ClearKey(ByVal pCUIT As String) As String
        Dim mKey_AUX As String

        Try

            mKey_AUX = ""
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.Cuit = pCUIT.Trim
            mKey_AUX = Me.ObjConexion.ClearKey()


        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mKey_AUX = ""
        Finally
            Me.ObjConexion = Nothing
        End Try


        Return mKey_AUX.ToString.Trim
    End Function


    'ObtainDataManager:
    'Esta funcion devuelve los datos del representante
    <WebMethod(Description:="Rafam: Esta funcion devuelve los datos del representante")> _
    Public Function ObtainDataManager(ByVal pCuit As String) As DataSet
        Dim dsData As DataSet

        Try

            dsData = New DataSet
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.Cuit = pCuit.ToString.Trim
            dsData = Me.ObjConexion.ObtainDataManager()


        Catch ex As Exception
            Call Me.GenerarLog(ex)
            dsData = Nothing
        Finally
            Me.ObjConexion = Nothing
        End Try


        Return dsData
    End Function



    'EditKey:
    'This function edit the key of the User
    <WebMethod(Description:="Rafam: Esta funcion guarda la nueva clave escrita por el usuario Web pisando la generada automaticamente")> _
    Public Function EditKey(ByVal pCUIT As String, ByVal pKeyManager As String, ByVal pNew_KeyManager As String) As String
        Dim mEdit_OK As String


        Try

            mEdit_OK = ""
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.CUIT = pCUIT.ToString.Trim
            Me.ObjConexion.KeyManager = pKeyManager.ToString.Trim
            Me.ObjConexion.New_KeyManager = pNew_KeyManager.ToString.Trim
            mEdit_OK = Me.ObjConexion.EditKey()


        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mEdit_OK = ""
        Finally
            Me.ObjConexion = Nothing
        End Try


        Return mEdit_OK.ToString.Trim
    End Function


    'DownUsers:
    'This function edit the key of the User
    <WebMethod(Description:="Rafam: Esta funcion suspende a al usuario Web")> _
     Public Function DownUsers(ByVal pCUIT As String, ByVal pMotiveDown As String) As String
        Dim mDown_OK As String


        Try

            mDown_OK = ""
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.CUIT = pCUIT.ToString.Trim
            Me.ObjConexion.Motive_Down = pMotiveDown.ToString.Trim
            mDown_OK = Me.ObjConexion.DownUsers()


        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mDown_OK = ""
        Finally
            Me.ObjConexion = Nothing
        End Try


        Return mDown_OK.ToString.Trim
    End Function



    'UpUsers:
    'This function edit the key of the User
    <WebMethod(Description:="Rafam: Esta funcion habilita al usuario Web")> _
    Public Function UpUsers(ByVal pCUIT As String) As String
        Dim mUp_OK As String


        Try

            mUp_OK = ""
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.CUIT = pCUIT.ToString.Trim
            mUp_OK = Me.ObjConexion.UpUsers()


        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mUp_OK = ""
        Finally
            Me.ObjConexion = Nothing
        End Try


        Return mUp_OK.ToString.Trim
    End Function




    'ExisteComercio
    'Esta funcion nos devuelve un boolean confirmando si existe un comercio en la tabla de comercios web 
    <WebMethod(Description:="Fox: Esta funcion devuelve un valor boolen si existe el comercio")> _
    Public Function ExisteComercio(ByVal pNroComercio As Long) As String
        Dim mCUIT As String

        Try

            mCUIT = ""
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.NroComercio = pNroComercio
            mCUIT = Me.ObjConexion.ExisteComercio()

        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mCUIT = ""
        Finally
            Me.ObjConexion = Nothing
        End Try


        Return mCUIT.ToString.Trim
    End Function




    'ObtainCTACTE
    'Esta funcion devuelve la cta. cte. de un comercio
    '<WebMethod(Description:="Fox: Esta funcion devuelve la cta. cte. de un comercio")> _
    'Public Function ObtainCTACTE(ByVal pNroComercio As Long) As String

    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then

    '        'Seteo Property
    '        Me.ObjConexion.NroComercio = pNroComercio.ToString.Trim
    '        Return Me.ObjConexion.ObtainCTACTE()
    '    Else
    '        Return ""
    '    End If

    'End Function


    'ObtainListDDJJ
    'This function returns a list of the presented DDJJ
    '<WebMethod(Description:="Fox: Este método devuelve el listado de declaraciones juradas de los últimos dos años para un comercio")> _
    'Public Function getListadoDDJJByComercio(ByVal p_numNroComercio As Long) As String
    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.getListadoDDJJByComercio(p_numNroComercio)
    '    Else
    '        Return ""
    '    End If

    'End Function



    'ObtainDetailDDJJ:
    'This function returns the detail of only a DDJJ
    '<WebMethod(Description:="Fox: Esta funcion devuelve el detalle de un movimiento de la lista de DDJJ de un comercio")> _
    'Public Function ObtainDetailDDJJ(ByVal pNumberComercio As Integer, ByVal pYear As Integer, ByVal pQuota As Integer) As String
    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then

    '        'Seteo Property
    '        Me.ObjConexion.NroComercio = pNumberComercio.ToString.Trim
    '        Me.ObjConexion.Year = pYear
    '        Me.ObjConexion.Quota = pQuota

    '        Return Me.ObjConexion.ObtainDetailDDJJ()
    '    Else
    '        Return ""
    '    End If
    'End Function



    'ObtainRubros:
    'This function returns the rubros of the comercio
    '<WebMethod(Description:="Fox: Esta funcion los rubros relacionados a un comercio")> _
    'Public Function ObtainRubros(ByVal pNumberComercio As Integer) As String
    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then

    '        'Seteo Property
    '        Me.ObjConexion.NroComercio = pNumberComercio.ToString.Trim
    '        Return Me.ObjConexion.ObtainRubros()
    '    Else
    '        Return ""
    '    End If
    'End Function




    'ObtainRubros:
    ''This function returns the rubros of the comercio
    '<WebMethod(Description:="Fox: Esta funcion genera el comprobante tanto vencido como no vencido")> _
    'Public Function ToEmitVouchers(ByVal pXMLVouchers As String, ByVal pExpiredVouchers As Boolean) As String
    '    If (SeteoConexion() <> eConexiones.mERROR) Then

    '        'Seteo Property
    '        Me.ObjConexion.XMLVouchers = pXMLVouchers.ToString.Trim
    '        Me.ObjConexion.ExpiredVouchers = pExpiredVouchers.ToString.Trim
    '        Return Me.ObjConexion.ToEmitVouchers()
    '    Else
    '        Return ""
    '    End If
    'End Function




    'ObtainNumberRecursoTISH:
    ''This function returns the number of the recurso TISH
    '<WebMethod(Description:="Fox: Esta funcion devuelve el numero de recuros utilizado para el TISH")> _
    'Public Function ObtainNumberRecursoTISH() As String
    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.ObtainNumberRecursoTISH()
    '    Else
    '        Return ""
    '    End If
    'End Function



    'ObtainPeriodsTISH: 
    'This function returns the periods that the municipalidad was using to receive the tish
    '<WebMethod(Description:="Fox: Esta funcion devuelve la lista de periodos del TISH")> _
    'Public Function ObtainPeriodsTISH() As String
    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.ObtainPeriodsTISH()
    '    Else
    '        Return ""
    '    End If
    'End Function



    'Exist_DDJJ: 
    'This function says to us if the ddjj exists
    '<WebMethod(Description:="Fox: Esta funcion devuelve un valor boolen si exite la DDJJ")> _
    'Public Function Exist_DDJJ(ByVal pNumberComercio As Integer, ByVal pYear As Integer, ByVal pQuota As Integer) As Boolean

    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Me.ObjConexion.NroComercio = pNumberComercio.ToString.Trim
    '        Me.ObjConexion.Year = pYear
    '        Me.ObjConexion.Quota = pQuota
    '        Return Me.ObjConexion.Exist_DDJJ()
    '    Else
    '        Return ""
    '    End If
    'End Function





    'AddDDJJ: 
    'This function a new DDJJ adds
    '<WebMethod(Description:="Fox: Esta funcion graba una nueva DDJJ")> _
    'Public Function AddDDJJ(ByVal pRecurso As String, ByVal pCuit As String, _
    '                        ByVal pNumberComercio As Integer, _
    '                        ByVal pYear As Integer, ByVal pQuota As Integer, _
    '                        ByVal pXML_AddDDJJ As String) As String


    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then

    '        Me.ObjConexion.Recurso = pRecurso.ToString.Trim
    '        Me.ObjConexion.Cuit = pCuit.ToString.Trim
    '        Me.ObjConexion.NroComercio = pNumberComercio.ToString.Trim
    '        Me.ObjConexion.Year = pYear
    '        Me.ObjConexion.Quota = pQuota
    '        Me.ObjConexion.XML_AddDDJJ = pXML_AddDDJJ.ToString.Trim
    '        Return Me.ObjConexion.AddDDJJ()
    '    Else
    '        Return ""
    '    End If
    'End Function


    'ObtainNumberVoucher:
    'This function returns the group of the voucher and the number of the voucher 
    '<WebMethod(Description:="Fox: Esta funcion devuelve el numero del grupo de comprobantes")> _
    'Public Function ObtainNumberVoucher(ByVal pRecurso As String, ByVal pNumberComercio As Long, _
    '                                     ByVal pYear As Integer, ByVal pQuota As Integer) As String


    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then

    '        Me.ObjConexion.Recurso = pRecurso.ToString.Trim
    '        Me.ObjConexion.NroComercio = pNumberComercio.ToString.Trim
    '        Me.ObjConexion.Year = pYear
    '        Me.ObjConexion.Quota = pQuota
    '        Return Me.ObjConexion.ObtainNumberVoucher()
    '    Else
    '        Return ""
    '    End If
    'End Function




    'getListadoDeComercios:
    'Este método devuelve un listado de comercios 
    '<WebMethod(Description:="Fox: Este método devuelve los datos de los comercios ingresados. [Los números se ingresas separados por pipe ('|')]")> _
    'Public Function getListadoDeComercios(ByVal p_strComercios As String) As String
    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.getListadoDeComercios(p_strComercios.Trim())
    '    Else
    '        Return ""
    '    End If
    'End Function


    'obtainListTrades:
    'Este método devuelve un listado de comercios 
    '<WebMethod(Description:="Fox: Este método devuelve los datos de los comercios ingresados. [Los números se ingresas separados por pipe ('|')]")> _
    'Public Function obtainListTrades(ByVal p_strComercios As String) As String
    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.obtainListTrades(p_strComercios.Trim())
    '    Else
    '        Return ""
    '    End If
    'End Function


    'ObtainDateOfExpiry:
    'This function add  expiry days 
    '<WebMethod(Description:="Fox: Esta funcion devuelve la fecha de vencimiento")> _
    'Public Function ObtainDateOfExpiry() As String

    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.ObtainDateOfExpiry()
    '    Else
    '        Return ""
    '    End If

    'End Function




    'Obtengo los vencimientos de un período particular:
    '<WebMethod(Description:="Fox: Este método devuelve las fechas de vencimiento de un perío en particular")> _
    'Public Function getFechasVtoPeriodo(ByVal p_strAnio As String, ByVal p_strCuota As String) As String

    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.getFechasVtoPeriodo(p_strAnio.Trim(), p_strCuota.Trim())
    '    Else
    '        Return ""
    '    End If

    'End Function



#End Region






    'LoginRAFAM
    'Esta funcion nos devuelve un boolean como resultado del login de un usuario de rafam
    <WebMethod()> _
    Public Function LoginRAFAM2(ByVal pUsuarioRAFAM As String, ByVal pClaveRAFAM As String) As String
        Dim mLogin_RAFAM As String

        Try

            mLogin_RAFAM = ""
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.UsuarioRAFAM = pUsuarioRAFAM.ToString.Trim
            Me.ObjConexion.ClaveRAFAM = pClaveRAFAM.ToString.Trim
            mLogin_RAFAM = Me.ObjConexion.LoginRafam2()
            ObjConexion = Nothing

        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mLogin_RAFAM = "Error: " & ex.ToString
        Finally
            Me.ObjConexion = Nothing
        End Try


        Return mLogin_RAFAM
    End Function



    ''ObtainMunicipalidadData:
    ''Esta funcion devuelve los datos de la municipalidad[Direccion - Tel. - Mail - Web]
    '<WebMethod(Description:="FOX - Rafam: Devuelve los datos de la municipalidad[Direccion - Tel. - Mail - Web]")> _
    'Public Function ObtainMunicipalidadData() As String

    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.ObtainMunicipalidadData()
    '    Else
    '        Return ""
    '    End If

    'End Function




    'UserWebExist:
    'Esta funcion devuelve un valor boolean dependiendo de la existencia del usuario Web
    <WebMethod(Description:="Rafam: Esta funcion devuelve un valor boolean dependiendo de la existencia del usuario Web")> _
    Public Function UserWebExist(ByVal pCuit As String) As Boolean

        Me.ObjConexion = New clsConexionRAFAM()
        Me.ObjConexion.Cuit = pCuit.Trim
        Return Me.ObjConexion.UserWebExist()

    End Function


    ''AddDateExpiry:
    ''Esta funcion agrega las fechas de vencimiento para los periodos a declarar
    '<WebMethod(Description:="FOX: Esta funcion agrega las fechas de vencimiento para los periodos a declarar")> _
    'Public Function AddDateExpiry(ByVal pYear As Integer, ByVal pQuota As Integer, _
    '                              ByVal pDateQuota1 As String, ByVal pDateQuota2 As String, ByVal pDate_p_DDJJ As String) As String

    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.AddDateExpiry(pYear, pQuota, pDateQuota1.Trim, pDateQuota2.Trim, pDate_p_DDJJ.Trim)
    '    Else
    '        Return ""
    '    End If

    'End Function


    ''ObtainList_DateExpiry:
    ''Esta funcion devuelve los peridos de vencimiento para las DDJJ
    '<WebMethod(Description:="FOX: Esta funcion devuelve los peridos de vencimiento para las DDJJ")> _
    'Public Function ObtainList_DateExpiry(ByVal pYear As Integer) As String

    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.ObtainList_DateExpiry(pYear)
    '    Else
    '        Return ""
    '    End If

    'End Function


    ''EditDateExpiry:
    ''This function returns 
    '<WebMethod(Description:="FOX: Esta funcion edita un periodo de vencimiento de DDJJ")> _
    'Public Function EditDateExpiry(ByVal pYear As Integer, ByVal pQuota As Integer, _
    '                               ByVal pDateQuota1 As String, ByVal pDateQuota2 As String, ByVal pDate_p_DDJJ As String) As String

    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.EditDateExpiry(pYear, pQuota, pDateQuota1.Trim, pDateQuota2.Trim, pDate_p_DDJJ.Trim)
    '    Else
    '        Return ""
    '    End If

    'End Function


    ''Fec_Vto_CTACTE_Exist:
    ''This function returns 
    '<WebMethod(Description:="FOX: Esta funcion indica si el período se encuentra en al Cta. Cte.")> _
    'Public Function Fec_Vto_CTACTE_Exist(ByVal pYear As Integer, ByVal pQuota As Integer) As String

    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.Fec_Vto_CTACTE_Exist(pYear, pQuota)
    '    Else
    '        Return ""
    '    End If

    'End Function



    ''DeleteDateExpiry:
    ''This function returns 
    '<WebMethod(Description:="FOX: Esta funcion elimina un vencimiento de DDJJ que no se encuentre en la Cta. Cte.")> _
    'Public Function DeleteDateExpiry(ByVal pYear As Integer, ByVal pQuota As Integer) As String

    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.DeleteDateExpiry(pYear, pQuota)
    '    Else
    '        Return ""
    '    End If

    'End Function


    ''ObtainDescriptionResourceTISH:
    ''Esta funcion devuelve la descripcion del recursos utilzado en el TISH
    '<WebMethod(Description:="FOX: Esta funcion devuelve la descripcion del recursos utilzado en el TISH")> _
    'Public Function ObtainDescriptionResourceTISH() As String

    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.ObtainDescriptionResourceTISH()
    '    Else
    '        Return ""
    '    End If

    'End Function



    ''VerificarParametros:
    '<WebMethod(Description:="FOX: Esta funcion devuelve todos los parametros que necesito para usar el sistema")> _
    'Public Function VerificarParametros(ByVal pVerParametros As Boolean) As String

    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.VerificarParametros(pVerParametros)
    '    Else
    '        Return ""
    '    End If

    'End Function


    ''VerificarParametros:
    '<WebMethod(Description:="FOX: Esta funcion devuelve la fecha a la cual se calcularan los intereses")> _
    'Public Function ObtenerFechaActuaWeb() As String
    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.ObtenerFechaActuaWeb()
    '    Else
    '        Return ""
    '    End If
    'End Function


    'getArticle:
    '<WebMethod(Description:="FOX: Esta funcion devuelve el articulo que se va a mostrar en la carga de una nueva DDJJ")> _
    'Public Function getArticle() As String
    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.getArticle()
    '    Else
    '        Return ""
    '    End If
    'End Function



    'getEmployeesUsing:
    '<WebMethod(Description:="FOX: Esta funcion Checkea si usan el valor cantidad empleados para la DDJJ")> _
    'Public Function getEmployeesUsing() As String
    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.getEmployeesUsing()
    '    Else
    '        Return ""
    '    End If
    'End Function



    ''getRubros:
    '<WebMethod(Description:="FOX: Este método me devuelve el listado de rubros")> _
    'Public Function getRubros() As String
    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.getRubros()
    '    Else
    '        Return ""
    '    End If
    'End Function


    'getRubrosByComercio:
    '<WebMethod(Description:="FOX: Este método me devuelve los rubros a asociados a un comercio")> _
    'Public Function getRubrosByComercio(ByVal p_strNroComercio As String) As String
    '    If (Me.SeteoConexion() <> eConexiones.mERROR) Then
    '        Return Me.ObjConexion.getRubrosByComercio(p_strNroComercio.Trim())
    '    Else
    '        Return ""
    '    End If
    'End Function

    'Obtener_Datos_Municipalidad:
    'Esta funcion devuelve los datos de municipalidad
    <WebMethod(Description:="RAFAM: Este método devuelve los datos de la muni")> _
    Public Function Obtener_Datos_Municipalidad() As DataSet

        Me.ObjConexion = New clsConexionRAFAM()
        Return Me.ObjConexion.Obtener_Datos_Municipalidad()

    End Function

    'Alta de Fechas de vencimiento:
    <WebMethod(Description:="RAFAM: Este metodo da de alta las fechas de vencimiento")> _
    Public Function Alta_Fecha_Vencimiento(ByVal pDataFechaVencimiento As String) As String
        Dim mOK As String

        Try
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.DataFechaVencimiento = pDataFechaVencimiento.ToString
            mOK = Me.ObjConexion.Alta_Fecha_Vencimiento()

        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mOK = Nothing
        Finally
            Me.ObjConexion = Nothing
        End Try

        Return mOK

    End Function

    'Editor de Fechas de vencimiento:
    <WebMethod(Description:="RAFAM: Este metodo edita las fechas de vencimiento")> _
    Public Function Edita_Fecha_Vencimiento(ByVal pDataFechaVencimiento As String) As String
        Dim mOK As String

        Try
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.DataFechaVencimiento = pDataFechaVencimiento.ToString
            mOK = Me.ObjConexion.Edita_Fecha_Vencimiento()

        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mOK = Nothing
        Finally
            Me.ObjConexion = Nothing
        End Try

        Return mOK

    End Function

    'Borrado de Fechas de vencimiento:
    <WebMethod(Description:="RAFAM: Este metodo borra las fechas de vencimiento")> _
    Public Function Borra_Fecha_Vencimiento(ByVal pDataFechaVencimiento As String) As String
        Dim mOK As String

        Try
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.DataFechaVencimiento = pDataFechaVencimiento.ToString
            mOK = Me.ObjConexion.Borra_Fecha_Vencimiento()

        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mOK = Nothing
        Finally
            Me.ObjConexion = Nothing
        End Try

        Return mOK

    End Function

    'Obtener_Datos_Comercio:
    'Esta funcion devuelve los datos de comercio
    <WebMethod(Description:="RAFAM: Este metodo devuelve los datos del comercio")> _
    Public Function Obtener_Datos_Comercio(ByVal pNroComercio As Integer) As String
        Dim dsDatos As String

        Try
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.NroComercio = pNroComercio
            dsDatos = Me.ObjConexion.Obtener_Datos_Comercio()

        Catch ex As Exception
            Call Me.GenerarLog(ex)
            dsDatos = Nothing
        Finally
            Me.ObjConexion = Nothing
        End Try

        Return dsDatos

    End Function


    'Esta funcion devuelve el XML con las fechas de vencimiento por parametro
    <WebMethod(Description:="RAFAM: Este metodo devuelve la fecha de vencimiento POR PARÁMETRO.")> _
    Public Function Obtener_Fecha_Vencimiento(ByVal pRecurso As Integer, ByVal pAnio As Integer, ByVal pCuota As Integer) As String
        Dim dsDatos As String

        Try
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.Recurso = pRecurso
            Me.ObjConexion.Anio = pAnio
            Me.ObjConexion.Cuota = pCuota

            dsDatos = Me.ObjConexion.Obtener_Fecha_Vencimiento()

        Catch ex As Exception
            Call Me.GenerarLog(ex)
            dsDatos = Nothing
        Finally
            Me.ObjConexion = Nothing
        End Try

        Return dsDatos

    End Function
    'Esta funcion devuelve el XML con las fechas de vencimiento por parametro
    <WebMethod(Description:="RAFAM: Este metodo devuelve el listado XML de fechas de vencimientos.")> _
    Public Function Obtener_Fechas_Vencimientos() As String
        Dim dsDatos As String

        Try
            Me.ObjConexion = New clsConexionRAFAM()

            dsDatos = Me.ObjConexion.Obtener_Fechas_Vencimientos()

        Catch ex As Exception
            Call Me.GenerarLog(ex)
            dsDatos = Nothing
        Finally
            Me.ObjConexion = Nothing
        End Try

        Return dsDatos

    End Function

    'Alta de DDJJ:
    'Esta funcion da de alta las ddjj
    <WebMethod(Description:="RAFAM: Este metodo da de alta la DDJJ")> _
    Public Function Alta_DDJJ(ByVal pDataDDJJ As String, ByVal pFechaPresentacion As String) As String
        Dim mOK As String

        Try
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.DataDDJJ = pDataDDJJ.ToString.Trim
            Me.ObjConexion.FechaPresentacion = pFechaPresentacion.ToString.Trim

            mOK = Me.ObjConexion.Alta_DDJJ()

        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mOK = Nothing
        Finally
            Me.ObjConexion = Nothing
        End Try

        Return mOK

    End Function

    'Existe DDJJ:
    'Esta funcion confirma si la DDJJ existe en la tabla o no.
    <WebMethod(Description:="RAFAM: Este metodo avisa por parametro si existe o no la DDJJ: 0-No Existe , 1-Existe, -1-Error. ")> _
    Public Function Existe_DDJJ(ByVal pNroComercio As Integer, ByVal pAnio As Integer, ByVal pCuota As Integer) As String
        Dim mOK As String

        Try
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.NroComercio = pNroComercio
            Me.ObjConexion.Anio = pAnio
            Me.ObjConexion.Cuota = pCuota
            mOK = Me.ObjConexion.Existe_DDJJ()

        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mOK = Nothing
        Finally
            Me.ObjConexion = Nothing
        End Try

        Return mOK

    End Function

    'Obtener Detalle Rubro:
    'Esta Funcion trae los rubros y los detalles y datos a partir de un nro de comercio.
    <WebMethod(Description:="RAFAM: Obtenemos la lista de los rubros y los detalles y datos a partir de un número de comercio ")> _
    Public Function Obtener_Datos_Rubro(ByVal pNroComercio As Integer) As String
        Dim mXML As String

        Try
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.NroComercio = pNroComercio

            mXML = Me.ObjConexion.Obtener_Datos_Rubro()

        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mXML = Nothing
        Finally
            Me.ObjConexion = Nothing
        End Try

        Return mXML

    End Function

    'Obtener DDJJ:
    'Esta Funcion trae el XML de las ddjj a partir de un nro de comercio.
    <WebMethod(Description:="RAFAM: Obtenemos la lista de las DDJJ y datos a partir de un número de comercio ")> _
    Public Function Obtener_DDJJ(ByVal pNroComercio As Integer) As String
        Dim mXML As String

        Try
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.NroComercio = pNroComercio

            mXML = Me.ObjConexion.Obtener_DDJJ()
        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mXML = Nothing
        Finally
            Me.ObjConexion = Nothing
        End Try

        Return mXML

    End Function
    'Obtener DDJJ:
    'Esta Funcion trae el XML de las ddjj a partir de un nro de comercio, Mes, anio.
    <WebMethod(Description:="RAFAM: Obtenemos la lista de las DDJJ y datos a partir de un número de comercio, Mes y Anio ")> _
    Public Function Obtener_DDJJ_Constancia(ByVal pNroComercio As Integer, ByVal pMes As Integer, ByVal pAnio As Integer) As String
        Dim mXML As String

        Try
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.NroComercio = pNroComercio
            Me.ObjConexion.Mes = pMes
            Me.ObjConexion.Anio = pAnio


            mXML = Me.ObjConexion.Obtener_DDJJ_Constancia()
        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mXML = Nothing
        Finally
            Me.ObjConexion = Nothing
        End Try

        Return mXML

    End Function
    'Obtener Datos Contribuyentes:
    'Esta Funcion trae el XML de los datos de contribuyente a partir del cuit.
    <WebMethod(Description:="RAFAM: Obtenemos los datos de contribuyente a partir de un cuit. ")> _
    Public Function Obtener_Datos_Contribuyente(ByVal pCuit As String) As String
        Dim mXML As String

        Try
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.Cuit = pCuit.ToString.Trim

            mXML = Me.ObjConexion.Obtener_Datos_Contribuyente()
        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mXML = Nothing
        Finally
            Me.ObjConexion = Nothing
        End Try

        Return mXML

    End Function

    'Comercio vinculado:
    'Esta Funcion devuelve si EXISTE o si NO EXISTE la vinculación entre un comercio y el contribuyente.
    <WebMethod(Description:="RAFAM: devuelve si EXISTE o si NO EXISTE la vinculación entre un comercio y el contribuyente.  ")> _
    Public Function Comercio_Vinculado(ByVal pNroComercio As Long, ByVal pNroContrib As Long) As String
        Dim mXML As String

        Try
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.NroComercio = pNroComercio
            Me.ObjConexion.NroContrib = pNroContrib


            mXML = Me.ObjConexion.Comercio_Vinculado()
        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mXML = Nothing
        Finally
            Me.ObjConexion = Nothing
        End Try

        Return mXML

    End Function


    'LOGIN ADMIN RAFAM :
    'Esta Funcion devuelve 1 SI ES ADMIN DE RAFAM O 0 SI ES OPERADOR.
    <WebMethod(Description:="RAFAM: Esta Funcion devuelve admin SI ES ADMINISTRADOR DE RAFAM U OPERADOR 0 SI ES OPERADOR.  ")> _
    Public Function Login_Rafam_Admin(ByVal pUsuarioRafam As String) As String
        Dim mXML As String

        Try
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.UsuarioRAFAM = pUsuarioRafam


            mXML = Me.ObjConexion.Login_Admin_Rafam
        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mXML = Nothing
        Finally
            Me.ObjConexion = Nothing
        End Try

        Return mXML

    End Function

    <WebMethod(Description:="RAFAM: Se obtiene el comprobante NO VENCIDO.  ")> _
    Public Function Obtener_Datos_Comprobante(ByVal pTipoComprobante As Long, ByVal pNroComprobante As Long) As String
        Dim mXML As String

        Try
            Me.ObjConexion = New clsConexionRAFAM()
            Me.ObjConexion.TipoComprobante = pTipoComprobante
            Me.ObjConexion.NroComprobante = pNroComprobante


            mXML = Me.ObjConexion.Obtener_Datos_Comprobante()
        Catch ex As Exception
            Call Me.GenerarLog(ex)
            mXML = Nothing
        Finally
            Me.ObjConexion = Nothing
        End Try

        Return mXML

    End Function


End Class