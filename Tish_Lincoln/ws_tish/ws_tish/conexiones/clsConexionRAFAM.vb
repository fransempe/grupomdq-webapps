﻿Option Explicit On
Option Strict On


Public Class clsConexionRAFAM
    Inherits clsConfiguracion

#Region "Variables"

    Private mDLL As ws_tish_rafam.ClsRafam
    Private mXML As String



    Private mCuit As String
    Private mClave As String


    Private mUsuarioRAFAM As String
    Private mClaveRAFAM As String


    Private mNroComercio As Long
    Private mNroContrib As Long
    Private mContribuyenteCuit As String


    Private mKeyManager As String
    Private mNew_KeyManager As String

    Private mDataManager As String
    Private mDataTrade As String
    Private mDataFechaVencimiento As String
    Private mMotive_Down As String
    Private mRecurso As Integer
    Private mAnio As Integer
    Private mMes As Integer
    Private mCuota As Integer
    Private mDataDDJJ As String
    Private mFechaPresentacion As String
    Private mTipoComprobante As Integer
    Private mNroComprobante As Integer

#End Region


#Region "Contructores"

    Public Sub New()

        Me.mXML = ""

        Me.mCuit = ""
        Me.mClave = ""

        Me.mUsuarioRAFAM = ""
        Me.mClaveRAFAM = ""

        Me.mNroComercio = 0
        Me.mNroContrib = 0

        Me.mContribuyenteCuit = ""

        Me.mKeyManager = ""
        Me.mNew_KeyManager = ""

        Me.mDataManager = ""
        Me.mDataTrade = ""


        Me.mMotive_Down = ""

        Me.mRecurso = 0
        Me.mAnio = 0
        Me.mMes = 0
        Me.mCuota = 0

        Me.mDataDDJJ = ""

        Me.mFechaPresentacion = ""
        Call Me.Limpiar()
    End Sub

    Protected Overrides Sub Finalize()

        Me.mXML = ""

        Me.mCuit = ""
        Me.mClave = ""

        Me.mUsuarioRAFAM = ""
        Me.mClaveRAFAM = ""

        Me.mNroComercio = 0
        Me.mContribuyenteCuit = ""
        mTipoComprobante = 0
        mNroComprobante = 0

        Me.mKeyManager = ""
        Me.mNew_KeyManager = ""

        Me.mDataManager = ""
        Me.mDataTrade = ""
        Me.mDataFechaVencimiento = ""
        Me.mMotive_Down = ""


        Call Me.Limpiar()

        MyBase.Finalize()
    End Sub
#End Region



#Region "Propertys"

    Public WriteOnly Property Cuit() As String
        Set(ByVal value As String)
            Me.mCuit = value.ToString.Trim()
        End Set
    End Property


    Public WriteOnly Property Clave() As String
        Set(ByVal value As String)
            Me.mClave = value.ToString.Trim()
        End Set
    End Property


    Public WriteOnly Property UsuarioRAFAM() As String
        Set(ByVal value As String)
            Me.mUsuarioRAFAM = value.ToString.Trim()
        End Set
    End Property


    Public WriteOnly Property ClaveRAFAM() As String
        Set(ByVal value As String)
            Me.mClaveRAFAM = value.ToString.Trim()
        End Set
    End Property


    Public WriteOnly Property NroComercio() As Long
        Set(ByVal value As Long)
            Me.mNroComercio = value
        End Set
    End Property

    Public WriteOnly Property NroContrib() As Long
        Set(ByVal value As Long)
            Me.mNroContrib = value
        End Set
    End Property


    Public WriteOnly Property ContribuyenteCuit() As String
        Set(ByVal value As String)
            Me.mContribuyenteCuit = value.ToString.Trim
        End Set
    End Property


    Public WriteOnly Property KeyManager() As String
        Set(ByVal value As String)
            Me.mKeyManager = value.ToString.Trim
        End Set
    End Property



    Public WriteOnly Property New_KeyManager() As String
        Set(ByVal value As String)
            Me.mNew_KeyManager = value.ToString.Trim
        End Set
    End Property

    Public WriteOnly Property DataManager() As String
        Set(ByVal value As String)
            Me.mDataManager = value.ToString.Trim
        End Set
    End Property

    Public WriteOnly Property DataTrade() As String
        Set(ByVal value As String)
            Me.mDataTrade = value.ToString.Trim
        End Set
    End Property


    Public WriteOnly Property Motive_Down() As String
        Set(ByVal value As String)
            Me.mMotive_Down = value.ToString.Trim
        End Set
    End Property

    Public WriteOnly Property DataFechaVencimiento() As String
        Set(ByVal value As String)
            Me.mDataFechaVencimiento = value.ToString.Trim
        End Set
    End Property

    Public WriteOnly Property Recurso() As Integer
        Set(ByVal value As Integer)
            Me.mRecurso = value
        End Set
    End Property

    Public WriteOnly Property Mes() As Integer
        Set(ByVal value As Integer)
            Me.mMes = value
        End Set
    End Property

    Public WriteOnly Property Anio() As Integer
        Set(ByVal value As Integer)
            Me.mAnio = value
        End Set
    End Property

    Public WriteOnly Property TipoComprobante() As Integer
        Set(ByVal value As Integer)
            Me.mTipoComprobante = value
        End Set
    End Property

    Public WriteOnly Property NroComprobante() As Integer
        Set(ByVal value As Integer)
            Me.mNroComprobante = value
        End Set
    End Property

    Public WriteOnly Property Cuota() As Integer
        Set(ByVal value As Integer)
            Me.mCuota = value
        End Set
    End Property

    Public WriteOnly Property DataDDJJ() As String
        Set(ByVal value As String)
            Me.mDataDDJJ = value.ToString.Trim
        End Set
    End Property

    Public WriteOnly Property FechaPresentacion() As String
        Set(ByVal value As String)
            Me.mFechaPresentacion = value.ToString.Trim
        End Set
    End Property
#End Region

#Region "Funciones Privadas"

    'Limpiar:
    'Esta funcion limpia y elimina todo lo que este en memoria
    Private Sub Limpiar()
        Me.mDLL = Nothing
        System.GC.Collect()
        System.GC.WaitForPendingFinalizers()
    End Sub
#End Region

#Region "Funciones Publicas"

    Public Function Login() As String
        Dim mNroComercio As String

        Try

            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            mNroComercio = ""
            If (mDLL.Conectar()) Then
                mNroComercio = mDLL.Login(mCuit.ToString.Trim, mClave.ToString.Trim)
            End If


        Catch ex As Exception

            Call MyBase.GenerarLog(ex)
            mNroComercio = "ERROR"

        Finally
            mDLL = Nothing
        End Try


        Return mNroComercio.ToString.Trim
    End Function


    'LoginRAFAM
    'Esta funcion nos devuelve un boolean como resultado del login de un usuario de rafam
    Public Function LoginRAFAM() As Boolean
        Dim mLogin_RAFAM As Boolean

        Try

            'Datos de conexion
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            'Datos del Usuario de rafam
            mDLL.UsuarioRAFAM = Me.mUsuarioRAFAM.ToString
            mDLL.ClaveRAFAM = Me.mClaveRAFAM.ToString


            mLogin_RAFAM = False
            mLogin_RAFAM = mDLL.LoginUsuariosRAFAM

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
            mLogin_RAFAM = False
        Finally
            mDLL = Nothing
        End Try


        Return mLogin_RAFAM
    End Function



    'CrearComercioWeb
    'Esta funcion nos devuelve un boolean como resultado del login de un usuario de rafam
    Public Function CreateUserWeb() As String
        Dim mClave As String

        Try

            'Datos de conexion
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString

            mClave = ""
            If (mDLL.Conectar()) Then
                mDLL.DataManager = Me.mDataManager.ToString.Trim
                mDLL.DataTrades = Me.mDataTrade.ToString.Trim
                mClave = mDLL.CreateUserWeb()
            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
            mClave = ""
        Finally
            mDLL = Nothing
        End Try


        Return mClave.ToString.Trim
    End Function


    'EditUserWeb
    'Esta funcion nos devuelve un boolean como resultado del login de un usuario de rafam
    Public Function EditUserWeb() As Boolean
        Dim mOK As Boolean

        Try

            'Datos de conexion
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString

            mOK = False
            If (mDLL.Conectar()) Then
                mDLL.DataManager = Me.mDataManager.ToString.Trim
                mDLL.DataTrades = Me.mDataTrade.ToString.Trim
                mOK = mDLL.EditUserWeb()
            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
            mOK = False
        Finally
            mDLL = Nothing
        End Try


        Return mOK
    End Function


    'CrearComercioWeb
    'Esta funcion nos devuelve un boolean como resultado del login de un usuario de rafam
    Public Function ClearKey() As String
        Dim mClave As String

        Try

            'Datos de conexion
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString

            mClave = ""
            If (mDLL.Conectar()) Then
                mDLL.Cuit = Me.mCuit.Trim
                mClave = mDLL.ClearKey()
            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
            mClave = ""
        Finally
            mDLL = Nothing
        End Try


        Return mClave.ToString.Trim
    End Function


    'CrearComercioWeb
    'Esta funcion nos devuelve un boolean como resultado del login de un usuario de rafam
    Public Function ObtainDataManager() As DataSet
        Dim dsData As DataSet

        Try

            'Datos de conexion
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString

            dsData = New DataSet
            If (mDLL.Conectar()) Then
                mDLL.Cuit = Me.mCuit.Trim
                dsData = mDLL.ObtainDataManager()
            End If


        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
            dsData = Nothing
        Finally
            mDLL = Nothing
        End Try


        Return dsData
    End Function


    'EditKey
    'This function edit the key of the User WEB
    Public Function EditKey() As String
        Dim mEdit_OK As String

        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString

            mEdit_OK = ""
            If (mDLL.Conectar()) Then
                mDLL.Cuit = Me.mCuit.Trim
                mDLL.KeyManager = Me.mKeyManager.ToString
                mDLL.New_KeyManager = Me.mNew_KeyManager.ToString
                mEdit_OK = mDLL.EditKey()
            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
            mEdit_OK = ""
        Finally
            mDLL = Nothing
        End Try


        Return mEdit_OK.ToString.Trim
    End Function


    'DownUsers
    'This function edit the key of the User WEB
    Public Function DownUsers() As String
        Dim mDown_OK As String

        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString

            mDown_OK = ""
            If (mDLL.Conectar()) Then
                mDLL.Cuit = Me.mCuit.Trim
                mDLL.Motive_Down = Me.mMotive_Down.ToString.Trim                
                mDown_OK = mDLL.DownUsers()
            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
            mDown_OK = ""
        Finally
            mDLL = Nothing
        End Try


        Return mDown_OK.ToString.Trim
    End Function


    'UpUsers
    'This function edit the key of the User WEB
    Public Function UpUsers() As String
        Dim mUp_OK As String

        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString

            mUp_OK = ""
            If (mDLL.Conectar()) Then
                mDLL.Cuit = Me.mCuit.Trim
                mUp_OK = mDLL.UpUsers()
            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
            mUp_OK = ""
        Finally
            mDLL = Nothing
        End Try


        Return mUp_OK.ToString.Trim
    End Function



    'ExisteComercio
    'Esta funcion nos devuelve un boolean confirmando si existe un comercio en la tabla de comercios web 
    Public Function ExisteComercio() As String
        Dim mCUIT As String

        Try

            'Datos de conexion
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString

            mCUIT = ""
            If (mDLL.Conectar()) Then
                mDLL.NroComercio = Me.mNroComercio
                mCUIT = mDLL.ExisteComercio()
            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
            mCUIT = ""
        Finally
            mDLL = Nothing
        End Try


        Return mCUIT.ToString.Trim
    End Function



    'LoginRAFAM
    'Esta funcion nos devuelve un boolean como resultado del login de un usuario de rafam
    Public Function LoginRAFAM2() As String
        Dim mLogin_RAFAM As String

        Try

            'Datos de conexion
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            'Datos del Usuario de rafam
            mDLL.UsuarioRAFAM = Me.mUsuarioRAFAM.ToString
            mDLL.ClaveRAFAM = Me.mClaveRAFAM.ToString


            mLogin_RAFAM = ""
            mLogin_RAFAM = mDLL.LoginUsuariosRAFAM2

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
            mLogin_RAFAM = ex.ToString
        Finally
            mDLL = Nothing
        End Try


        Return mLogin_RAFAM
    End Function



    'UserWebExist
    'Esta funcion nos devuelve un boolean como resultado de la existencia del usuario Web
    Public Function UserWebExist() As Boolean
        Dim mExist As Boolean

        Try

            'Datos de conexion
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString

            mExist = False
            If (mDLL.Conectar()) Then
                mDLL.Cuit = Me.mCuit.ToString.Trim                
                mExist = mDLL.UserWebExist()
            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
            mExist = False
        Finally
            mDLL = Nothing
        End Try


        Return mExist
    End Function


#End Region


    Public Function Obtener_Datos_Municipalidad() As DataSet
        Dim mDatos As DataSet

        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString

            If (mDLL.Conectar()) Then
                mDatos = mDLL.ObtenerDatosMunicipalidad
            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
        Finally
            mDLL = Nothing
        End Try


        Return mDatos
    End Function

    Public Function Obtener_Datos_Comercio() As String
        Dim dsDatos As String

        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            If (mDLL.Conectar()) Then
                mDLL.NroComercio = Me.mNroComercio
                dsDatos = mDLL.Obtener_Datos_Comercio()
            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
        Finally
            mDLL = Nothing
        End Try


        Return dsDatos
    End Function


    Public Function Alta_Fecha_Vencimiento() As String
        Dim mOK As String = ""
        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            If (mDLL.Conectar()) Then
                mDLL.DataFechaVencimiento = Me.mDataFechaVencimiento
                mOK = mDLL.AltaFechaVencimiento()
            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
        Finally
            mDLL = Nothing
        End Try


        Return mOK
    End Function


    Public Function Obtener_Fecha_Vencimiento() As String
        Dim mOK As String = ""
        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            If (mDLL.Conectar()) Then
                mDLL.Recurso = Me.mRecurso
                mDLL.Anio = Me.mAnio
                mDLL.Cuota = Me.mCuota

                mOK = mDLL.Obtener_Fecha_Vencimiento
            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
        Finally
            mDLL = Nothing
        End Try


        Return mOK
    End Function

    Public Function Obtener_Fechas_Vencimientos() As String
        Dim mOK As String = ""
        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            If (mDLL.Conectar()) Then

                mOK = mDLL.Obtener_Fechas_Vencimientos
            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
        Finally
            mDLL = Nothing
        End Try


        Return mOK
    End Function
    Public Function Alta_DDJJ() As String
        Dim mOK As String = ""
        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            If (mDLL.Conectar()) Then
                mDLL.Data_DDJJ = mDataDDJJ.ToString.Trim
                mDLL.FechaPresentacion = mFechaPresentacion.ToString.Trim
                mOK = mDLL.Alta_DDJJ()
            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
        Finally
            mDLL = Nothing
        End Try


        Return mOK
    End Function

    Public Function Existe_DDJJ() As String
        Dim mOK As String = ""
        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            If (mDLL.Conectar()) Then
                mDLL.NroComercio = Me.mNroComercio
                mDLL.Anio = Me.mAnio
                mDLL.Cuota = Me.mCuota
                mOK = mDLL.Existe_DDJJ()
            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
        Finally
            mDLL = Nothing
        End Try


        Return mOK
    End Function

    Public Function Obtener_Datos_Rubro() As String
        Dim mXML As String = ""
        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            If (mDLL.Conectar()) Then
                mDLL.NroComercio = Me.mNroComercio
                mDLL.Anio = Me.mAnio
                mDLL.Cuota = Me.mCuota
                mXML = mDLL.Obtener_Datos_Rubro()

            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
        Finally
            mDLL = Nothing
        End Try


        Return mXML
    End Function

    Public Function Obtener_DDJJ() As String
        Dim mXML As String = ""
        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            If (mDLL.Conectar()) Then
                mDLL.NroComercio = Me.mNroComercio
                mXML = mDLL.Obtener_DDJJ()

            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
        Finally
            mDLL = Nothing
        End Try


        Return mXML
    End Function

    Public Function Obtener_DDJJ_Constancia() As String
        Dim mXML As String = ""
        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            If (mDLL.Conectar()) Then
                mDLL.NroComercio = Me.mNroComercio
                mDLL.Mes = Me.mMes
                mDLL.Anio = Me.mAnio


                mXML = mDLL.Obtener_DDJJ_Constancia()

            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
        Finally
            mDLL = Nothing
        End Try


        Return mXML
    End Function

    Public Function Edita_Fecha_Vencimiento() As String
        Dim mOK As String = ""
        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            If (mDLL.Conectar()) Then
                mDLL.DataFechaVencimiento = Me.mDataFechaVencimiento
                mOK = mDLL.EditaFechaVencimiento()
            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
        Finally
            mDLL = Nothing
        End Try


        Return mOK
    End Function

    Public Function Borra_Fecha_Vencimiento() As String
        Dim mOK As String = ""
        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            If (mDLL.Conectar()) Then
                mDLL.DataFechaVencimiento = Me.mDataFechaVencimiento
                mOK = mDLL.BorraFechaVencimiento()
            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
        Finally
            mDLL = Nothing
        End Try


        Return mOK
    End Function

    Public Function Obtener_Datos_Contribuyente() As String
        Dim mXML As String = ""
        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            If (mDLL.Conectar()) Then
                mDLL.Cuit = Me.mCuit
                mXML = mDLL.Obtener_Datos_Contribuyente()

            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
        Finally
            mDLL = Nothing
        End Try


        Return mXML
    End Function

    Public Function Comercio_Vinculado() As String
        Dim mXML As String = ""
        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            If (mDLL.Conectar()) Then
                mDLL.NroComercio = Me.mNroComercio
                mDLL.NroContrib = Me.mNroContrib
                mXML = mDLL.Comercio_Vinculado()

            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
        Finally
            mDLL = Nothing
        End Try


        Return mXML
    End Function


    Public Function Login_Admin_Rafam() As String
        Dim mXML As String = ""
        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            If (mDLL.Conectar()) Then
                mDLL.UsuarioRAFAM = mUsuarioRAFAM

                mXML = mDLL.Login_Admin_Rafam

            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
        Finally
            mDLL = Nothing
        End Try


        Return mXML
    End Function

    Public Function Obtener_Datos_Comprobante() As String
        Dim mXML As String = ""
        Try

            'data the connection
            mDLL = New ws_tish_rafam.ClsRafam
            mDLL.Host = MyBase.Conexion_Host.ToString
            mDLL.Puerto = MyBase.Conexion_Puerto.ToString
            mDLL.Servidor = MyBase.Conexion_Servidor.ToString
            mDLL.Servicio = MyBase.Conexion_Servicio.ToString


            If (mDLL.Conectar()) Then
                mDLL.TipoComprobante = Me.mTipoComprobante
                mDLL.NroComprobante = Me.mNroComprobante
                mXML = mDLL.Obtener_Datos_Comprobante.ToString

            End If

        Catch ex As Exception
            Call MyBase.GenerarLog(ex)
        Finally
            mDLL = Nothing
        End Try


        Return mXML
    End Function

End Class
