<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmconsulta_b.aspx.vb" Inherits="web_tributaria.webfrmconsulta_b" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc1" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<link  href="../css/generica.css" rel="stylesheet" type="text/css" />
<link  href="../css/mensajes.css" rel="stylesheet" type="text/css" />
<link  href="../css/botones.css" rel="stylesheet" type="text/css" />
<script src="../js/funciones.js" type="text/javascript"></script>


 <!-- Virtual keyboard -->       
        <script type="text/javascript" src="../keyboard/keyboard.js" charset="UTF-8"></script>
        <link rel="stylesheet" href="../keyboard/keyboard.css" type="text/css" />

<script type="text/javascript">
    function dominioenabled() {
        if (window.document.getElementById('RdbVehiculo').checked) {
            window.document.getElementById('id_tr_dominio').style.display='' ;
        } else {
            window.document.getElementById('id_tr_dominio').style.display = 'none';
       }
        
    
    }

    function validar_datos() {
        
        //Cadena Vacia
        if (control_vacio(window.document.getElementById('txtnro_cuenta'),'N�mero de Cuenta:\nDebe Ingresar un N�mero de Cuenta')) {
           return false;
        }
        
       //Validar Cadena Ingresada Nro. de Cuenta
        if (!(validar_cadena_solo_numeros(window.document.getElementById('txtnro_cuenta'),'N�mero de Cuenta'))) {
            return false;
        }       
 
     
        //Cadena Vacia
        if (control_vacio(window.document.getElementById('txtcaptcha'),'Debe Ingresar el Codigo de Confirmacion')) {          
            return false;
        }


        if (!validar_cadena(window.document.getElementById('txtcaptcha'),'Codigo de Verificaci�n')) {          
            return false;
        }

        
        procesando_datos();
        
        
    return true;
    }
    
    
    function ver(){
    	document.getElementById('tabla').style.visibility = 'hidden';
	    //document.getElementById('tabla').style.opacity = 1;
	    
    }

    function procesando_datos() {
        var _html;
        var _height_tabla_login;        
        var _height;
        
        
        _height_tabla_login = parseInt(window.document.getElementById('tabla_login').style.height.replace('px',''));                         
        _height = _height_tabla_login + 16 + 13 ;      
        
        
    	_html = '<div id="loading" align="center" style="height:' + _height+ 'px;">'+
    	            '<table border= "0" style="padding-top:50px;">' +
                        '<tr>' +
                            '<td align="center" style="width:20%;"><img id="img_load" alt="" src="../imagenes/loading.gif" /></td>' +
                            '<td align="left" style="width:80%;">' + 'Aguarde, se est�n verificando los datos ingresados ....' +'</td>' +
                        '</tr>' +
                    '</table>' +
                '</div>';




    	window.document.getElementById('tabla_login').style.display = 'none';



    	var control_label = document.getElementById('lblmensaje');
    	if (control_label != null) {
    	    window.document.getElementById('lblmensaje').style.display = 'none';
    	    window.document.getElementById('btnaceptar').style.display='none';
    	}

    	var control_div = document.getElementById('div_mensaje');
    	if (control_div != null) {
    	    window.document.getElementById('div_mensaje').style.display = 'none';
    	}
        window.document.getElementById('div_mensaje_procesando_datos').style.display = 'block';
        window.document.getElementById('div_mensaje_procesando_datos').innerHTML = _html;
    return true;        
    }



    function completar_ceros_derecha(control, cantidad_ceros, caracter) {
        var dato = control.value.toString();
        var id_control = control.id;
        
        
        while(dato.length < cantidad_ceros) {
            dato = caracter + dato;
        }    
        window.document.getElementById(id_control).value = dato;
    }
    
    
    
    function consultar_deuda(val, ev){

        if (window.event) {
            var key = window.event.keyCode;
        } else {
            var key = ev.which;
        }
        
      
        if (key == 13) {    
            if (validar_datos()){
                __doPostBack("link_consultar_deuda",'');
            }
        }
    }

    function carga_inicial(){
        var control_label = document.getElementById('lblmensaje');
        var control_div


        if (document.formulario.lblmensaje != undefined) {        
            if (control_label.innerHTML == 'OK') {
                document.getElementById('div_mensaje').style.display = 'none';
            } else {
                document.getElementById('div_mensaje').style.display = 'block';
            }
        }
        
    }    
    


    // Esta funcion redimensiona el div_contenedor segun la opcion seleccionada
    function redimensionar_div() {
     
        if (document.getElementById('cmbtipo_cuentas').value == 'Veh�culo') {
           document.getElementById('txtdominio').disabled=false;

        } else {  
            document.getElementById('txtdominio').disabled=true;    
        }
    }


    //Esta funcion pasa el dominio ingresado a letras mayusculas
    function solo_mayusculas(control) {
        control.value=control.value.toUpperCase()
    }
    
    
    //Esta funcion valida la cadena ingresada como dominio
    function validar_dominio(){   
        return validar_cadena_dominio(window.document.getElementById('txtdominio'))
    }
    
    //Asigna estilo al boton que obtiene la cuenta por dominio
    function boton_dominio_onmouseover(boton){
        boton.className = 'boton_dominio_mouseover';
    }
    
    //Asigna estilo al boton que obtiene la cuenta por dominio
    function boton_dominio_onmouseout(boton){
        boton.className = 'boton_dominio_mouseout';
    }
    

</script>



<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Sistema tributario :: Login</title>
    <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />



<link href="../css/webfrmconsulta_b.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            #footer
            {
                height: 0px;
            }
                        
            .columna_titulo{
            	width: 90%;
            	text-align:center;
            	font-size:2em;
            }
            
            .columna_dato{
            	width: 60%;
            	text-align:center;                  	          
            }
            
            .textbox {
                 text-align:center;
                 margin:2px;
            }
            
            
            .textbox_captcha {
                 text-align:center;
                  margin-bottom:2px;
            }
            
                        
                        
           .titulo_tabla{
	            background-image: url('../imagenes/barra_cabecera.png');
	            color:#FFFFFF;
	            font-size:12px;
	            text-align:center;
	            border-left:1px solid #FFFFFF;
	            height:40px;
            }
                             
            
            .style7
            {
            	font-weight:bolder;
                width: 700px;
                text-align: center;
                font-size:medium;
            }
            
            .style5
            {
            	
            	text-align:center;
            	width:100px;
            	}
                       
             .style-dominio-cuenta
             {
             	font-weight:bolder;
             	text-align:center;
             	font-size:medium;
             	width:300px;
             	}                
            
        </style>
        
        
        
        
    	        
</head>

<body onload="carga_inicial()">

    <form id="formulario" class="form"  runat="server">
        <div id="div_main" class="main" style="min-height:540px;">
        
            <div id="header">  
            
            
		        <div class="div_font" style="padding:0px 5px 0px 5px;">
                    <a href="#" title="Usar fuente menor" class="link_font" onclick="assign_font('-');">
                    A-</a> 
                </div>
                
                <div class="div_font">
		            <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">
                    A</a> 
                </div>		            
               
               <div style="padding:0px 5px 0px 5px;">	
			        
		        </div>
                                 
                <span id="logo-text">Terminal de Consulta Tributaria :: Ingreso</span>  
                <h2 id="slogan">Municipalidad de <asp:Label ID="lblmunicipalidad_name" runat="server" Text=""></asp:Label></h2>			
                    
            </div>

      
 <asp:ScriptManager ID="ScriptManager1" runat="server"> </asp:ScriptManager>
        
        
        <div id="div_contenedor" class="content" runat="server">
		    <div class="barra">
                <strong>&nbsp;CONSULTA DE DEUDA Y EMISION DE COMPROBANTES</strong>
            </div>
                <div class="recomendaciones">
                     &nbsp;* Ingrese el tipo y n�mero de cuenta (sin guiones, barras ni d�gito 
                     verificador).<br />
                     &nbsp;* Seleccione tipo de cuenta <strong><em>INMUEBLE</em></strong> para deudas 
                     relacionadas con tasas del tipo Alumbrado, Barrido y Limipieza, Servicios 
                     Urbanos y Servicios Sanitarios. <br />
                            &nbsp;* Seleccione tipo de cuenta <strong><em>COMERCIO</em></strong> para 
                     deudas relacionadas con Inspecci�n de Seguridad e Higiene.<br />
                            &nbsp;* Seleccione tipo de cuenta <strong><em>CEMENTERIO</em></strong> para 
                     deudas relacionadas con Cementerios.<br />
                            &nbsp;* Seleccione tipo de cuenta <em><strong>VEHICULO</strong></em> para 
                     deudas relacionadas con Impuesto Automotor o Patentes de Rodados Menores.
                 </div>
                            
                 <div class="barra"></div>
                    <div id ="tabla" style="width: 850px;  padding:10px 0px 10px 0px; height: 296px;" align="center";>
                         <div id = "ajax">
                             <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional"><ContentTemplate>
                                       <div id="mensaje" style="padding-bottom:5px;">
		                                   <div id="div_mensaje" class="Globo GlbRed" style="width:45%;" runat="server" visible="False">
		                                        <asp:Label ID="lblmensaje" runat="server" Visible="False" Font-Size="Large"></asp:Label>
		                                        <br/>
		                                        <asp:Button ID="btnaceptar" runat="server" Text="Aceptar" style ="margin-top:5px;"/>
		                                   </div>           
                                        </div>
                                    
                                    
                                        <div id="div_mensaje_procesando_datos"></div>
                                    
                                        <div id="div_login" runat="server">                                                       
                                            <table id="tabla_login" border="1" cellspacing="10"                                      
                                            style="border-style: double; background-color: #FFFFFF; color: #000000;" 
                                            align="center">
                                                
                                                    <td class="style7">Tipo de Cuenta</td>
                                                        <td class="style5">Inmueble
                                                            <asp:RadioButton class="RDB" ID="RdbInmueble" runat="server" GroupName="TipoCuenta" 
                                                                Text="" Checked="True" onclick="dominioenabled()"/>
                                                        </td>
                                                        <td class="style5">Comercio
                                                            <asp:RadioButton class="RDB" ID="RdbComercio" runat="server" GroupName="TipoCuenta"
                                                                Text=""  onclick="dominioenabled()"/>
                                                        </td>
                                                        
                                                        <td class="style5">Veh�culo
                                                            <asp:RadioButton class="RDB" ID="RdbVehiculo" runat="server" GroupName="TipoCuenta" 
                                                                Text=""  onclick="dominioenabled()"/>
                                                        </td>
                                                        <td class="style5">Cementerio  
                                                            <asp:RadioButton class="RDB" ID="RdbCementerio" runat="server" GroupName="TipoCuenta" 
                                                                Text=""  onclick="dominioenabled()" Font-Strikeout="False"/>
                                                        </td>
                                               </table>     
                                                
                                                
                                               <table id="tabla_login1" border="1" cellspacing="10" style="border-style: double; background-color: #FFFFFF; color: #000000;" 
                                                align="center"> 
                                                    <tr id="id_tr_dominio" runat="server" style="display: none">
                                                       
                                                        <td class="style-dominio-cuenta"  runat="server">Dominio</td>
                                                            
                                                        <td class="style-dominio-cuenta">
                                                        
                                                        <asp:TextBox ID="txtdominio" runat="server" Width="100px"
                                                                MaxLength="10" 
                                                                ToolTip="Debe ingresar su domino sin espacios, guiones o barras."></asp:TextBox>
                                                                                                    
                                                        <asp:Button ID="btn_obtener_cuenta" runat="server" 
                                                                Text="Obtener Cuenta" ToolTip="Cargar una cuenta en base al dominio cargado." 
                                                                CssClass="boton_dominio_mouseout" Width="180px"/>
                                                                         
                                                                        
                                                        </td>         
                                                    </tr>
                                                          
                                                    
                                                        <td class="style-dominio-cuenta">N�mero de Cuenta</td>
                                                            <td class="style-dominio-cuenta">
                                                            <asp:TextBox ID="txtnro_cuenta" runat="server" MaxLength="10" Width="180px"></asp:TextBox>
                                                                <caption>
                                                                    <asp:Label ID="lblCargando" runat="server" BackColor="#CCCCCC" Text="Aguarde, se est�n consultando los datos..." style="display:none; font-size:2em;"  ></asp:Label>
                                                                    &nbsp;
                                                                </caption>
                                                            </td>
                                                    
                                                </table>
                                        </div>
                                   <asp:LinkButton Id="link_consultar_deuda" runat="server" CssClass="link" Font-Size="X-Large">[ConsultarDeuda]</asp:LinkButton>             
                          </ContentTemplate>  

                                                </asp:UpdatePanel>       
                         
                         
                        </div>       
                    </div>    
                                
                                
                                
                              <!-- <div style="height: 15px; font-weight: bold; font-size: small; text-align: center; font-family: 'Courier New', Courier, 'espacio sencillo';" 
                    class="barra">NOTA:</div>
                                <div align="center">&nbsp; - Los per�odos de deuda aqu� listados pueden no reflejar los 
                                    pagos realizados recientemente. Ante cualquier duda relacionada con su estado de 
                                    deuda&nbsp; p�ngase en contacto con la Municipalidad a trav�s del tel�fono 
                                    <strong><asp:Label ID="lbltelefono" runat="server" Text="TELEFONO"></asp:Label></strong> 
                                    &nbsp;o la direcci�n de correo electr�nico 
                                    <strong><asp:Label ID="lblmail" runat="server" Text="MAIL"></asp:Label></strong>
                                    .</div>
	                            
	                                <div align="center">
                                        &nbsp; - Para un correcto funcionamiento en la Emisi�n de Comprobantes se le aconseja 
                                        tener actualizado el Acrobat Reader. Para actualizar a la �ltima versi�n    
                                        <a href="http://www.adobe.com/es/products/acrobat/readstep2.html" 
                                            target="_blank" class="link">[clic aqu�]</a>.
                                    </div>
                                    
	                        </div>
                            
	                        <div id='div_visitas' 
                style="padding: 20px 5px 0px 0px; text-align:right; height: 90px;">
	                            	                            
	                            <a href="webfrmvisitas.aspx" class="link" title="Ver detalle de visitas">
	                            <strong style="font-size:larger;"><asp:Label ID="lblcontador" runat="server" 
                                    Text="Visitas: 00000012" Font-Size="Medium"></asp:Label></strong>	                            
	                            </a>	                            
	                            <br />
	                            
	                            <p style="margin:1px 3px 1px 0px">
	                                <a id="loginAdmin"  href="webfrmlogin_admin.aspx" class="link" title="ingresar como administrador" runat="server">
                                    [Ingresar como administrador]</a>
	                                <label id="label_saludo_admin" runat="server" visible="false">Usuario 
                                    Administrador</label>
	                            </p>
	                                                        
	                            <a href="http://www.grupomdq.com" target="_blank" class="link">[Desarrollado por 
                                Grupo MDQ]</a>
	                            <br />
	                             <label style="padding-right:3px;">
                                    <asp:Label ID="lblversion" runat="server"></asp:Label>
                                </label>
                                -->
	            </div>             
            </div> 
        </form>
    </body>
</html>


