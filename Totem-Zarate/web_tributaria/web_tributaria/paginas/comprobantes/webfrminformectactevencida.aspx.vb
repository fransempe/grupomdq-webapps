﻿Option Explicit On
Option Strict On

Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Text


'Web Services
Imports web_tributaria.ws_consulta_tributaria.Service1

Partial Public Class WebForm1
    Inherits System.Web.UI.Page

#Region "Variables"

    Private Structure SLogo
        Dim LogoOrganismo As Byte()
        Dim RenglonOrganismo As String
        Dim RenglonNombreOrganismo As String
    End Structure

    Private mPDF As ClsCrearListadoPDF
    Private mLog As Clslog
    Private mWS As web_tributaria.ws_consulta_tributaria.Service1
    Private mLogo As SLogo

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dsDatos As DataSet
        Dim dtgrilla_cta_cte_vencida As DataTable
        Dim dttotales_grilla_cta_cte_vencida As DataTable

        Dim mRutaFisica As String
        Dim mNombreArchivoComprobante As String
        Dim mArchivo As String



        Try

            If Not (IsPostBack) Then
                If (Session("tipo_cuenta") IsNot Nothing) And (Session("nro_cuenta") IsNot Nothing) Then


                    'Creo Directorio y Archivo en Disco
                    mRutaFisica = Request.ServerVariables("APPL_PHYSICAL_PATH").ToString & "comprobantespdf"
                    If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString)) Then
                        My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString)
                    End If
                    mNombreArchivoComprobante = Session("tipo_cuenta").ToString & Session("nro_cuenta").ToString & Format(Now, "HHmmss")
                    mArchivo = mRutaFisica & "\" & mNombreArchivoComprobante & ".pdf"


                    'Creo y Seteo el Nombre del Logo dependiendo de la Conexion
                    If (Application("tipo_conexion").ToString = "RAFAM") Then
                        Call GenerarLogo()
                    End If



                    'Genero el PDF
                    mPDF = New ClsCrearListadoPDF
                    mPDF.TipoConexion = Application("tipo_conexion").ToString


                    'Estos Datos son para agregar al LOGO si esta Conectado a RAFAM
                    If (Application("tipo_conexion").ToString = "RAFAM") Then
                        mPDF.Organismo = mLogo.RenglonOrganismo.ToString
                        mPDF.NombreOrganismo = mLogo.RenglonNombreOrganismo.ToString
                    End If

                    mPDF.DatosContribuyente = Session("datos_contribuyente").ToString.Split(CChar("|"))
                    mPDF.RutaFisica = mArchivo.ToString
                    dsDatos = Nothing
                    dsDatos = New DataSet


                    'Instancio los DATATABLE para poder asignar los DATATABLE que tengo en VARIABLES de SESSION
                    dtgrilla_cta_cte_vencida = New DataTable
                    dttotales_grilla_cta_cte_vencida = New DataTable
                    dtgrilla_cta_cte_vencida = CType(Session("grilla_cta_cte_vencida"), DataTable).Copy
                    dttotales_grilla_cta_cte_vencida = CType(Session("totales_grilla_cta_cte_vencida"), DataTable).Copy


                    'Agrego los Datatable as DataSet
                    dsDatos.Tables.Add(dtgrilla_cta_cte_vencida)
                    dsDatos.Tables.Add(dttotales_grilla_cta_cte_vencida)

                    mPDF.DatosMunicipalidad = Me.ObtenerDatosMunicipalidad()
                    mPDF.Datos = dsDatos

                    'Asigno los datos del rodado
                    If (Session("datosRodado") IsNot Nothing) Then
                        mPDF.DatosRodado = CType(Session("datosRodado"), DataTable)
                    End If

                    mPDF.CrearPDF()

                    'Este código es para imprimir directamente sin cuadros de diálogo.
                    'Dim psi As System.Diagnostics.ProcessStartInfo = New System.Diagnostics.ProcessStartInfo()
                    'psi.UseShellExecute = True
                    'psi.Verb = "print"
                    'psi.FileName = mArchivo.ToString
                    'psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden
                    'psi.ErrorDialog = False
                    'psi.Arguments = "/p"
                    'Dim p As System.Diagnostics.Process = System.Diagnostics.Process.Start(psi)
                    'p.WaitForInputIdle()

                    ' Si esta configurado para que el pdf salga con el cuadro de díalogo de impresión, redirecciono a la pagina "webfrmpdf.aspx"
                    If (CBool(System.Configuration.ConfigurationManager.AppSettings("CuadroDialogoImpresion").ToString.Trim)) Then
                        Session("pdf") = mNombreArchivoComprobante.Trim()
                        Response.Redirect("../webfrmindex.aspx", False)

                    Else

                        'Muestro el Comprobante por pantalla
                        Response.Expires = 0
                        Response.Buffer = True
                        Response.Clear()
                        Response.ContentType = "application/pdf"
                        Response.TransmitFile(mArchivo.ToString)
                        Response.Flush()
                    End If
                Else
                    Call LimpiarSession()
                End If
                End If


        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
            Response.Redirect("../webfrmerror.aspx", False)
        End Try

    End Sub


#Region "LOG"

    Private Function ObtenerDetalleWEB() As String
        Dim mInfoLog As String = ""
        Try

            For Each mNombreVariable In Request.ServerVariables
                mInfoLog = mInfoLog & mNombreVariable.ToString & ": " & Request.ServerVariables(mNombreVariable.ToString)
                mInfoLog = mInfoLog & vbCrLf
            Next

            Return mInfoLog.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function
#End Region

    Private Sub LimpiarSession()
        Session.RemoveAll()
        Session.Clear()
        Session.Abandon()
        Response.Redirect("../webfrmindex.aspx", False)
    End Sub


    Private Sub GenerarLogo()
        Dim dsdatosLogo As DataSet
        Dim mCadenaBytes As IO.MemoryStream
        Dim mLogoImagen As Image

        Try

            'Obtengo el LOGO
            mWS = New ws_consulta_tributaria.Service1
            dsdatosLogo = mWS.ObtenerLogo
            mWS = Nothing


            'Obtengo los Datos para generar el Logo
            mLogo.RenglonOrganismo = ""
            mLogo.RenglonNombreOrganismo = ""
            mLogo.LogoOrganismo = CType(dsdatosLogo.Tables(0).Rows(0).Item("BMP"), Byte())
            mLogo.RenglonOrganismo = dsdatosLogo.Tables(0).Rows(0).Item("RPTORGANISMO").ToString
            mLogo.RenglonNombreOrganismo = dsdatosLogo.Tables(0).Rows(0).Item("RPTNOMBREORG").ToString


            'Genero un Imagen a partir del Array de Byte y lo guardo en disco
            mCadenaBytes = New IO.MemoryStream(mLogo.LogoOrganismo)
            mLogoImagen = Image.FromStream(mCadenaBytes)
            mLogoImagen.Save("C:\logo_rafam.jpg")


        Catch ex As Exception
            mLogo.RenglonOrganismo = ""
            mLogo.RenglonNombreOrganismo = ""
        End Try

    End Sub


    Private Function ObtenerDatosMunicipalidad() As DataSet
        Dim dsDatos As DataSet
        Dim mXML As String

        Try

            'Obtengo los datos de la municipalidad
            mWS = New ws_consulta_tributaria.Service1
            mXML = mWS.ObtenerDatosMunicipalidad()
            mWS = Nothing


            dsDatos = New DataSet
            dsDatos = ClsTools.ObtainDataXML(mXML.ToString.Trim)

            Return dsDatos
        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
            Response.Redirect("../webfrmerror.aspx", False)
            Return Nothing
        End Try

    End Function

End Class