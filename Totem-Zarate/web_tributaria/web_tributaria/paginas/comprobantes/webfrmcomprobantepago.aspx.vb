﻿Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Text


'Web Services
Imports web_tributaria.ws_consulta_tributaria.Service1


Partial Public Class webfrmcomprobantepago
    Inherits System.Web.UI.Page

#Region "Variables"

    Private Structure SLogo
        Dim LogoOrganismo As Byte()
        Dim RenglonOrganismo As String
        Dim RenglonNombreOrganismo As String
    End Structure

    Private mPDF As ClsCrearComprobantePDF
    Private mLog As Clslog
    Private mWS As web_tributaria.ws_consulta_tributaria.Service1
    Private mLogo As SLogo

#End Region

#Region "Eventos Formulario"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dsDatos As DataSet
        Dim mRutaFisica As String
        Dim mNombreArchivoComprobante As String
        Dim mArchivo As String
        Dim mCodigoMunicipalidad As String


        Try

            '  If Not (IsPostBack) Then
            If (Session("tipo_cuenta") IsNot Nothing) And (Session("nro_cuenta") IsNot Nothing) Then
                If (Session("xml_comprobantes") IsNot Nothing) Then

                    'Obtengo los datos
                    dsDatos = New DataSet
                    dsDatos.ReadXml(New IO.StringReader(Session("xml_comprobantes")))


                    'Creo Directorio y Archivo en Disco
                    mRutaFisica = Request.ServerVariables("APPL_PHYSICAL_PATH").ToString & "comprobantespdf"
                    If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString)) Then
                        My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString)
                    End If
                    mNombreArchivoComprobante = Session("tipo_cuenta").ToString & Session("nro_cuenta").ToString & Format(Now, "HHmmss")
                    mArchivo = mRutaFisica & "\" & mNombreArchivoComprobante & ".pdf"


                    'Creo y Seteo el Nombre del Logo dependiendo de la Conexion
                    mCodigoMunicipalidad = ""
                    If (Application("tipo_conexion").ToString = "RAFAM") Then
                        Call GenerarLogo()
                        mCodigoMunicipalidad = ObtenerCodigoMunicipalidad()
                    End If


                    'Genero el PDF
                    mPDF = New ClsCrearComprobantePDF

                    mPDF.TipoConexion = Application("tipo_conexion").ToString
                    mPDF.Dominio = Session("dominio").ToString.Trim

                    'Estos Datos son para agregar al LOGO si esta Conectado a RAFAM
                    If (Application("tipo_conexion").ToString = "RAFAM") Then
                        mPDF.Organismo = mLogo.RenglonOrganismo.ToString
                        mPDF.NombreOrganismo = mLogo.RenglonNombreOrganismo.ToString
                    End If


                    'Este Dato es utilizado si estoy conectado a RAFAM, es usado para GENERAR el BARCODE39
                    mPDF.CodigoMunicipalidad = mCodigoMunicipalidad.ToString


                    mPDF.RutaFisica = mArchivo.ToString
                    mPDF.DatosMunicipalidad = Me.ObtenerDatosMunicipalidad()
                    mPDF.Datos = dsDatos

                    'Asigno los datos del rodado
                    If (Session("datosRodado") IsNot Nothing) Then
                        mPDF.DatosRodado = CType(Session("datosRodado"), DataTable)
                    End If



                    mPDF.CrearPDF()



                    ' Si esta configurado para que el pdf salga con el cuadro de díalogo de impresión, redirecciono a la pagina "webfrmpdf.aspx"
                    If (CBool(System.Configuration.ConfigurationManager.AppSettings("CuadroDialogoImpresion").ToString.Trim)) Then
                        Session("pdf") = mNombreArchivoComprobante.Trim()
                        Response.Redirect("../webfrmindex.aspx", False)

                    Else
                        'Muestro el Comprobante por pantalla
                        Response.Expires = 0
                        Response.Buffer = True
                        Response.Clear()
                        Response.ContentType = "application/pdf"
                        Response.TransmitFile(mArchivo.ToString)
                        Response.Flush()
                    End If


                End If
                Else
                    Call LimpiarSession()
                End If

        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
            Response.Redirect("../webfrmerror.aspx", False)
        End Try

    End Sub

#End Region

#Region "procedimientos y Funciones"



    Private Sub LimpiarSession()
        Session.RemoveAll()
        Session.Clear()
        Session.Abandon()
        Response.Redirect("../webfrmindex.aspx", False)
    End Sub

    Private Function ObtenerTipoCuenta(ByVal mTipoCuenta As String) As String
        Select Case mTipoCuenta
            Case "I" : Return "INMUEBLE"
            Case "C" : Return "COMERCIO"
            Case "E", "O" : Return "CEMENTERIO"
            Case "V" : Return "VEHICULOS"
            Case Else : Return ""
        End Select
    End Function

#End Region

#Region "LOG"

    Private Function ObtenerDetalleWEB() As String
        Dim mInfoLog As String = ""
        Try

            For Each mNombreVariable In Request.ServerVariables
                mInfoLog = mInfoLog & mNombreVariable.ToString & ": " & Request.ServerVariables(mNombreVariable.ToString)
                mInfoLog = mInfoLog & vbCrLf
            Next

            Return mInfoLog.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function
#End Region

    Private Function ObtenerDatoConfig(ByVal mNombreNodo As String, ByVal mNombreDato As String) As String
        Dim mRutaFisica As String
        Dim dsConfiguracion As DataSet
        Dim ObjSeguridad As ClsSeguridad
        Dim mNombreMunicipio As String


        Try

            'Obtengo la Ruta del archivo de Configuracion
            mRutaFisica = Server.MapPath("Configuracion.gmdq").ToString.Replace("Configuracion.gmdq", Nothing)
            mRutaFisica = mRutaFisica.ToString.Replace("\comprobantes", Nothing)
            dsConfiguracion = New DataSet
            dsConfiguracion.ReadXml(mRutaFisica.ToString & "Configuracion.gmdq")


            'Obtengo el Dato del archivo configuracion
            mNombreMunicipio = ""
            ObjSeguridad = New ClsSeguridad
            mNombreMunicipio = ObjSeguridad.DesEncriptar(dsConfiguracion.Tables(mNombreNodo.ToString).Rows(0).Item(mNombreDato.ToString).ToString)



            'Libero Memoria
            ObjSeguridad = Nothing
            dsConfiguracion = Nothing


            Return mNombreMunicipio.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function


    Private Sub GenerarLogo()
        Dim dsdatosLogo As DataSet
        Dim mCadenaBytes As IO.MemoryStream
        Dim mLogoImagen As Image

        Try

            'Obtengo el LOGO
            mWS = New ws_consulta_tributaria.Service1
            dsdatosLogo = mWS.ObtenerLogo
            mWS = Nothing


            'Obtengo los Datos para generar el Logo
            mLogo.RenglonOrganismo = ""
            mLogo.RenglonNombreOrganismo = ""
            mLogo.LogoOrganismo = dsdatosLogo.Tables(0).Rows(0).Item("BMP")
            mLogo.RenglonOrganismo = dsdatosLogo.Tables(0).Rows(0).Item("RPTORGANISMO").ToString
            mLogo.RenglonNombreOrganismo = dsdatosLogo.Tables(0).Rows(0).Item("RPTNOMBREORG").ToString


            'Genero un Imagen a partir del Array de Byte y lo guardo en disco
            mCadenaBytes = New IO.MemoryStream(mLogo.LogoOrganismo)
            mLogoImagen = Image.FromStream(mCadenaBytes)
            mLogoImagen.Save("C:\logo_rafam.jpg")


        Catch ex As Exception
            mLogo.RenglonOrganismo = "Nombre No Obtenido"
            mLogo.RenglonNombreOrganismo = "Nombre No Obtenido"

            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
        End Try

    End Sub

    Private Function ObtenerCodigoMunicipalidad() As String
        Dim mCodigoMunicipalidad As String

        Try

            'Obtengo Nombre Municpalidad
            mCodigoMunicipalidad = ""
            mWS = New ws_consulta_tributaria.Service1
            mCodigoMunicipalidad = mWS.ObtenerCodigoMunicipalidad
            mWS = Nothing

            Return mCodigoMunicipalidad.ToString.Trim
        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
            Response.Redirect("../webfrmerror.aspx", False)
            Return ""
        End Try

    End Function


    Private Function ObtenerDatosMunicipalidad() As DataSet
        Dim dsDatos As DataSet
        Dim mXML As String

        Try

            'Obtengo los datos de la municipalidad
            mWS = New ws_consulta_tributaria.Service1
            mXML = mWS.ObtenerDatosMunicipalidad()
            mWS = Nothing


            dsDatos = New DataSet
            dsDatos = ClsTools.ObtainDataXML(mXML.ToString.Trim)

            Return dsDatos
        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
            Response.Redirect("../webfrmerror.aspx", False)
            Return Nothing
        End Try

    End Function

End Class