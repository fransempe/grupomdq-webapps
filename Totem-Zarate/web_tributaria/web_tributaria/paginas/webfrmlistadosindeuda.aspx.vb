﻿Public Partial Class webfrmlistadosindeuda
    Inherits System.Web.UI.Page

    Private mLog As Clslog

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try


            'Eventos JavaScript
            Me.link_cerrar_session.Attributes.Add("onclick", "javascript:return cerrar_session();")
            Me.link_imprimir.Attributes.Add("onclick", "javascript:return aguarde();")


            Me.lblmunicipalidad_name.Text = System.Configuration.ConfigurationManager.AppSettings("Nombre_Municipalidad").ToString.Trim

            If (Session("municipalidad_telefono") IsNot Nothing) Then
                Me.lbltelefono.Text = Session("municipalidad_telefono").ToString.Trim
            Else
                Me.lbltelefono.Text = "Tel. Municipio"
            End If

            If (Session("municipalidad_mail") IsNot Nothing) Then
                Me.lblmail.Text = Session("municipalidad_mail").ToString.Trim
            Else
                Me.lblmail.Text = "Email Municipio"
            End If


            Me.lblmensaje.Text = "La cuenta no registra deuda al " & Format(Now, "dd/MM/yyyy") & "."

        Catch ex As Exception
            Me.mLog = New Clslog
            Me.mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            Me.mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)
        End Try
    End Sub


#Region "LOG"

    Private Function ObtenerDetalleWEB() As String
        Dim mInfoLog As String = ""
        Try

            For Each mNombreVariable In Request.ServerVariables
                mInfoLog = mInfoLog & mNombreVariable.ToString & ": " & Request.ServerVariables(mNombreVariable.ToString)
                mInfoLog = mInfoLog & vbCrLf
            Next

            Return mInfoLog.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function
#End Region


    Protected Sub link_cerrar_session_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_cerrar_session.Click
        Call Me.LimpiarSession()
    End Sub

    Private Sub LimpiarSession()

        Session.RemoveAll()
        Session.Clear()
        Session.Abandon()
        Response.Redirect("webfrmindex.aspx", False)

    End Sub

     
    Protected Sub link_imprimir_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_imprimir.Click
        Response.Redirect("comprobantes/webfrminformectactevencida_sindeuda.aspx", False)
    End Sub
End Class