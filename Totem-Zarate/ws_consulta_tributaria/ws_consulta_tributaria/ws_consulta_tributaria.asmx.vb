﻿Option Explicit On


Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Data.OracleClient



' Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la siguiente línea.
' <System.Web.Script.Services.ScriptService()> _

<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Service1
    Inherits System.Web.Services.WebService


#Region "Variables"

    Private ObjConexion As Object
    Private mLog As Clslog
    Private mTipoConexion As String

#End Region


#Region "Funciones Privadas"

    'Esta funcion setea el tipo de conexion que va a usar el sistema
    Private Function SeteoConexion() As Boolean
        Dim mRutaFisica As String = ""
        Dim mTipoConexion As String = ""
        Dim mConexion_OK As Boolean


        Try

            mConexion_OK = False
            mTipoConexion = System.Configuration.ConfigurationManager.AppSettings("Tipo_Conexion").ToString.Trim

            'Obtengo la Ruta del archivo de Configuracion
            mRutaFisica = Server.MapPath("./").ToString.Trim

            If (mTipoConexion.ToString.Trim = "RAFAM") Then
                ObjConexion = New ClsConexionRAFAM(mRutaFisica.ToString)
            Else
                ObjConexion = New ClsConexionFOX(mRutaFisica.ToString)
            End If


            mConexion_OK = True
        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, mRutaFisica.ToString)
            mLog = Nothing
            mConexion_OK = False
        End Try


        Return mConexion_OK
    End Function

    'Esta funcion devuelte la letra del tipo de cuenta
    Private Function ObtenerTipoCuenta(ByVal mTipoCuenta As String) As String
        Select Case mTipoCuenta
            Case "Inmueble" : Return "I"
            Case "Comercio" : Return "C"
            Case "Cementerio" : Return "E"
            Case "Vehículo"

                mTipoConexion = System.Configuration.ConfigurationManager.AppSettings("Tipo_Conexion").ToString.Trim
                If (mTipoConexion.ToString = "RAFAM") Then
                    Return "R"
                Else
                    Return "V"
                End If
            Case Else : Return ""
        End Select
    End Function

#End Region



#Region "Funciones Publicas"

    'Esta funcion realiza un test de conexion a la base de datos
    <WebMethod(Description:="FOX: Esta funcion realiza un test de conexion a la base de datos")> _
    Public Function ProbarConexion() As String
        If (Me.SeteoConexion()) Then
            Return ObjConexion.ProbarConexion()
        Else
            Return ""
        End If
    End Function


    'Esta funcion devuelve la ruta que tiene seteada para leer los datos o 
    'si es RAFAM la cadena de conexion
    <WebMethod(Description:="Esta funcion devuelve la ruta que tiene seteada para leer los datos o si es RAFAM la cadena de conexion")> _
    Public Function VerRuta_CadenaConexion() As String
        If (Me.SeteoConexion()) Then
            Return ObjConexion.VerRuta_CadenaConexion()
        Else
            Return ""
        End If
    End Function



    <WebMethod()> _
   Public Function Login(ByVal mTipoCuenta As String, ByVal mNroCuenta As Long, _
                                  ByVal mNroGrupo1 As Integer, ByVal mNroComprobante1 As Integer, ByVal mDigitoVerificador1 As Integer, _
                                  ByVal mNroGrupo2 As Integer, ByVal mNroComprobante2 As Integer, ByVal mDigitoVerificador2 As Integer) As Integer

        mTipoCuenta = ObtenerTipoCuenta(mTipoCuenta.ToString)
        If (Me.SeteoConexion()) Then
            Return ObjConexion.Login(mTipoCuenta, mNroCuenta, _
                                     mNroGrupo1, mNroComprobante1, mDigitoVerificador1, _
                                     mNroGrupo2, mNroComprobante2, mDigitoVerificador2)
        Else
            Return 0
        End If
    End Function


    'La Variable "mNroUsuario" solo es utilizada en el caso de RAFAM, esta variable es utilizada como bandera
    <WebMethod(Description:="'La Variable 'mNroUsuario' solo es utilizada en el caso de RAFAM, esta variable es utilizada como bandera")> _
    Public Function ConsultarDeuda(ByVal mTipoCuenta As String, ByVal mNroCuenta As Long, ByVal mNroUsuario As Integer) As String
        If (Me.SeteoConexion()) Then
            mTipoCuenta = ObtenerTipoCuenta(mTipoCuenta.ToString)
            Return ObjConexion.ConsultarDeuda(mTipoCuenta, mNroCuenta, mNroUsuario)
        Else
            Return ""
        End If
    End Function

    <WebMethod(Description:="Emite los comprobantes tanto vencidos como no vencidos dependiendo del segundo parametro")> _
    Public Function EmitirComprobantes(ByVal mXMLComprobantes As String, ByVal mComprobantesVencidos As Boolean) As String
        If (Me.SeteoConexion()) Then
            Return ObjConexion.EmitirComprobantes(mXMLComprobantes, mComprobantesVencidos)
        Else
            Return ""
        End If
    End Function

    <WebMethod()> _
   Public Function LimpiarDatosSession(ByVal mNroUsuario As Integer) As Boolean
        If (Me.SeteoConexion()) Then
            Return ObjConexion.LimpiarDatosSession(mNroUsuario)
        Else
            Return False
        End If
    End Function



    <WebMethod(Description:="ORACLE: Devuelve los datos del contribuyente")> _
    Public Function ObtenerDatosContribuyente_RAFAM(ByVal mNroUsuario As Integer) As String
        If (Me.SeteoConexion()) Then
            Return ObjConexion.ObtenerDatosContribuyente(mNroUsuario)
        Else
            Return False
        End If
    End Function

    <WebMethod(Description:="FOX: Devuelve los datos del contribuyente")> _
     Public Function ObtenerDatosContribuyente_FOX(ByVal pTipoImponible As String, ByVal pNroImponible As String) As String
        If (Me.SeteoConexion()) Then
            Return ObjConexion.ObtenerDatosContribuyente(pTipoImponible, pNroImponible)
        Else
            Return False
        End If
    End Function

    <WebMethod(Description:="Devuelve el logo de la municipalidad para la impresion de los comprobantes")> _
    Public Function ObtenerLogo() As DataSet
        If (Me.SeteoConexion()) Then
            Return ObjConexion.ObtenerLogo()
        Else
            Return Nothing
        End If
    End Function

    <WebMethod()> _
    Public Function ObtenerCodigoMunicipalidad() As String
        If (Me.SeteoConexion()) Then
            Return ObjConexion.ObtenerCodigoMunicipalidad()
        Else
            Return Nothing
        End If
    End Function



    <WebMethod(Description:="ORACLE: Realiza una prueba de conexion contra una base de datos de Oracle")> _
  Public Function prueba(ByVal mHost As String, ByVal mServer As String, ByVal mServicio As String) As String
        Dim mCadenaConexion As String
        Dim mConexion As OracleConnection



        mCadenaConexion = "Data Source=(DESCRIPTION=" _
                              + "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" & mHost.ToString.Trim & ")(PORT=1521)))" _
                              + "(CONNECT_DATA=(SERVER=" & mServer.ToString.Trim & ")(SERVICE_NAME=" & mServicio.ToString.Trim & ")));" _
                              + "User Id=OWNER_RAFAM;Password=OWNERDBA;"


        mConexion = New OracleConnection
        mConexion.ConnectionString = mCadenaConexion.ToString

        Try


            mConexion.Open()
            mConexion.Close()
            Return "OK"

        Catch ex As Exception
            Return ex.Message.ToString
        End Try
    End Function


    'Esta Funcion me Devuelve el Número de Cuenta en Base a un Dominio
    <WebMethod(Description:="FOX: Esta Funcion me Devuelve el Número de Cuenta en Base a un Dominio")> _
    Public Function ObtenerNroCuentaPorPatente(ByVal mDominio As String) As String
        If (Me.SeteoConexion()) Then
            Return ObjConexion.ObtenerNroCuentaPorPatente(mDominio.ToString.Trim)
        Else
            Return ""
        End If
    End Function



    'Esta Funcion me Devuelve los datos de la municipalidad
    <WebMethod(Description:="Esta Funcion me Devuelve los datos de la municipalidad")> _
    Public Function ObtenerDatosMunicipalidad() As String

        If (Me.SeteoConexion()) Then
            Return ObjConexion.ObtenerDatosMunicipalidad()
        Else
            Return ""
        End If
    End Function



    'Esta Funcion valida que esten todos los parametros creados y definidos
    <WebMethod(Description:="FOX: Esta Funcion valida que esten todos los parametros creados y definidos")> _
    Public Function VerificarParametros(ByVal pVerParametros As Boolean) As String
        If (Me.SeteoConexion()) Then
            Return ObjConexion.VerificarParametros(pVerParametros)
        Else
            Return ""
        End If
    End Function



    'Esta Funcion Devuelve la fecha a la cual se calcularan los intereses
    <WebMethod(Description:="Esta Funcion Devuelve la fecha a la cual se calcularan los intereses")> _
    Public Function ObtenerFechaActuaWeb() As String

        If (Me.SeteoConexion()) Then
            Return ObjConexion.ObtenerFechaActuaWeb()
        Else
            Return ""
        End If
    End Function

    <WebMethod()> _
    Public Function ObtenerFechaVTOWebCalculada() As String

        If (Me.SeteoConexion()) Then
            Return ObjConexion.ObtenerFechaVTOWebCalculada()
        Else
            Return ""
        End If
    End Function

    <WebMethod(Description:="FOX: CreateLog")> _
    Public Function CreateLog(ByVal p_sPath As String, ByVal p_sMensaje As String) As String

        If (Me.SeteoConexion()) Then
            Return ObjConexion.CreateLog(p_sPath.Trim, p_sMensaje.Trim)
        Else
            Return ""
        End If
    End Function

    <WebMethod(Description:="Esta función registra una visita al sitio")> _
    Public Function agregarVisita(ByVal p_sFecha As String, ByVal p_sHora As String, ByVal p_sTipoImponible As String, ByVal p_lNroImponible As Long) As String
        Dim sTipoImponibleAUX As String

        sTipoImponibleAUX = Me.ObtenerTipoCuenta(p_sTipoImponible.Trim)
        If (Me.SeteoConexion()) Then
            Return ObjConexion.agregarVisita(p_sFecha.Trim, p_sHora.Trim, sTipoImponibleAUX.Trim, p_lNroImponible)
        Else
            Return ""
        End If
    End Function

    <WebMethod(Description:="Esta función actualiza el estado de una visita")> _
    Public Function actualizarVisita(ByVal p_sFecha As String, ByVal p_sHora As String, ByVal p_sTipoImponible As String, ByVal p_lNroImponible As Long) As String
        Dim sTipoImponibleAUX As String

        sTipoImponibleAUX = Me.ObtenerTipoCuenta(p_sTipoImponible.Trim)
        If (Me.SeteoConexion()) Then
            Return ObjConexion.actualizarVisita(p_sFecha.Trim, p_sHora.Trim, sTipoImponibleAUX.Trim, p_lNroImponible)
        Else
            Return ""
        End If
    End Function

    <WebMethod(Description:="Esta función devuelve el total de visitas realizadas")> _
    Public Function getVisitasCantidad() As String
        If (Me.SeteoConexion()) Then
            Return ObjConexion.getVisitasCantidad()
        Else
            Return ""
        End If
    End Function

    <WebMethod(Description:="Esta función devuelve las visitas agrupadas por tipo de imponible y divido por imprimio o no")> _
    Public Function getVisitasCantidadDetalle(ByVal p_sFechaDesde As String, ByVal p_sFechaHasta As String) As String
        If (Me.SeteoConexion()) Then
            Return ObjConexion.getVisitasCantidadDetalle(p_sFechaDesde.Trim(), p_sFechaHasta.Trim())
        Else
            Return ""
        End If
    End Function


    <WebMethod(Description:="FOX: getParametroExcluirRecurso")> _
    Public Function getParametroExcluirRecurso() As String
        If (Me.SeteoConexion()) Then
            Return ObjConexion.getParametroExcluirRecurso()
        Else
            Return ""
        End If
    End Function

    <WebMethod(Description:="FOX: ObtenerTiposImponible")> _
    Public Function getObtenerTiposImponible() As String
        If (Me.SeteoConexion()) Then
            Return ObjConexion.getObtenerTiposImponible()
        Else
            Return ""
        End If
    End Function

    <WebMethod(Description:="RAFAM: ObtenerTiposImponible")> _
    Public Function ObtenerTiposImponible() As String
        If (Me.SeteoConexion()) Then
            Return ObjConexion.ObtenerTiposImponible()
        Else
            Return ""
        End If

    End Function

    <WebMethod(Description:="FOX: getUsaLoginAdmin")> _
    Public Function usaLoginAdmin() As String
        If (Me.SeteoConexion()) Then
            Return ObjConexion.usaLoginAdmin()
        Else
            Return ""
        End If
    End Function


    'Esta Funcion devuelve si el imponible está o no bloqueado.
    <WebMethod(Description:="FOX: Esta Funcion devuelve si el imponible esta o no Bloqueado")> _
    Public Function ObtenerImponibleBloqueado(ByVal pTipoImponible As String, ByVal pNroImponible As Long) As String
        If (Me.SeteoConexion()) Then
            Return ObjConexion.ObtenerImponibleBloqueado(pTipoImponible, pNroImponible)
        Else
            Return ""
        End If
    End Function

    'Esta Funcion Devuelve el parámetro de imprimir o no el codigo de barras para COMERCIOS en Zárate
    <WebMethod(Description:="Esta Funcion Devuelve el parametro (S o N) para imprimir o no el cod bar de Comercios en Zárate")> _
    Public Function ObtenerParametroImprimeCodBar() As String

        If (Me.SeteoConexion()) Then
            Return ObjConexion.ObtenerParametroImprimeCodBar()
        Else
            Return ""
        End If
    End Function

#End Region


End Class