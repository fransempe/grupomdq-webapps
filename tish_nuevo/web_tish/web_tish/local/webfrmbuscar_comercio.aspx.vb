﻿Option Explicit On



Partial Public Class webfrmbuscar_comercio
    Inherits System.Web.UI.Page

#Region "Variables"
    Private mWS As ws_tish.Service1
    Private mXML As String


    Private dtDatos As DataTable
    Private dtDatosOriginal As DataTable
    Private mFila As Integer

    Dim dsDatosEventosGrilla As DataTable
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not (IsPostBack) Then

            Call ValidarSession()
            Call CargarPie()



            'I assign the events javascript
            txtbuscar.Attributes.Add("onfocus", "javascript:event_focus(this);")
            txtbuscar.Attributes.Add("onblur", "javascript:event_focus(this);")


            'This procedure loads the grilla with a row in white
            Call LoadGrilla_RecordBlank()


        Else
            'Call LimpiarControles()
        End If
    End Sub


    Private Sub btnbuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnbuscar.Click
        'Call Buscar(txtbuscar.Text.Trim)

        Dim dsDatos As DataSet
        Dim mDatos_XML As String


        Try


            lblmensaje_busqueda.visible = True


            'Obtengo los datos
            mWS = New ws_tish.Service1
            mDatos_XML = mWS.BuscarComercio(txtbuscar.Text.Trim, cmbtipo_busqueda.Text.Trim)
            mWS = Nothing


            'creo el XML en disco para poder leerlo
            dsDatos = New DataSet
            dsDatos = clsTools.ObtainDataXML(mDatos_XML.ToString)




            'Si el dataset no esta vacio asigno los datos
            mFila = 0
            If (clsTools.HasData(dsDatos)) Then
                If (dsDatos.Tables(0).Rows(0).Item("COMERCIO_ESTADO").ToString.Trim = "OK") Then
                    dsDatosEventosGrilla = dsDatos.Tables(0)
                    dtDatosOriginal = dsDatos.Tables(0)
                    Session("grilla_original") = dtDatosOriginal
                    Session("grilla_actual") = dtDatosOriginal
                    gvcomercios.DataSource = dsDatos.Tables(0)
                    gvcomercios.DataBind()

                Else

                    'This procedure loads the grilla with a row in white
                    Call LoadGrilla_RecordBlank()

                End If

            Else

                'This procedure loads the grilla with a row in white
                Call LoadGrilla_RecordBlank()
            End If



        Catch ex As Exception
            mWS = Nothing
        End Try
    End Sub




    'Filtra la grilla buscando por el texto en el campo txtFiltar
    Public Sub Buscar(ByVal mCadena As String)
        Dim mFila As Integer
        Dim mColumna As Integer
        Dim dtDatosFiltrados As DataTable




        Dim dtoriginal As DataTable
        dtoriginal = New DataTable
        dtoriginal = Session("grilla_original").copy




        'Obtengo los Datos de la Grilla para filtrarlos
        dtDatosFiltrados = New DataTable
        dtDatosFiltrados = Session("grilla_actual").copy
        dtDatosFiltrados.Rows.Clear()


        'Busco la Cadena
        For mFila = 0 To dtoriginal.Rows.Count - 1
            For mColumna = 0 To dtDatosFiltrados.Columns.Count - 1
                If InStr(dtoriginal.Rows(mFila).Item(mColumna).ToString.ToUpper, mCadena.ToUpper, CompareMethod.Text) > 0 Then

                    'Guardo la Fila si hay alguna Coincidencia
                    dtDatosFiltrados.Rows.Add(dtoriginal.Rows(mFila).ItemArray)
                    Exit For
                End If
            Next
        Next


        'Return dtDatosFiltrados
        'Asigno los Datos Filtrados a la Grilla
        dsDatosEventosGrilla = dtDatosFiltrados
        Session("grilla_actual") = dtDatosFiltrados
        gvcomercios.DataSource = dtDatosFiltrados
        gvcomercios.DataBind()
        'Call RefrescarGrilla()
    End Sub

    Private Sub gvproveedores_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvcomercios.RowCommand
        Dim dsFilas As DataTable

        dsFilas = New DataTable
        dsFilas = Session("grilla_actual")

        Session("codigo_proveedor") = dsFilas.Rows(e.CommandArgument.ToString).Item("COMERCIO_NUMERO").ToString
        Response.Redirect("webfrmgenerar_clave.aspx", False)
    End Sub


    Private Sub gvproveedores_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvcomercios.RowDataBound
        Dim mCodigoProveedor As String




        If (e.Row.RowType = DataControlRowType.DataRow) Then

            'Obtengo el Tipo Y Nro de Comprobantes
            mCodigoProveedor = dsDatosEventosGrilla.Rows(mFila).Item("COMERCIO_NUMERO").ToString
            mFila = mFila + 1


            e.Row.Cells(3).Attributes.Add("onclick", "javascript:return seleccionado(" & mCodigoProveedor.ToString & ");")
        End If
    End Sub


    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If

    End Sub


    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("nombre_usuario") IsNot Nothing) Then
                If (Session("nombre_usuario").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        If (mLogin_OK) Then
            lblnombre_usuario.Text = Session("nombre_usuario").ToString.Trim
        Else
            Response.Redirect("webfrmlogin_rafam.aspx", False)
        End If

    End Function


    'LoadGrilla_RecordBlank:
    'This procedure loads the grilla with a row in white
    Private Sub LoadGrilla_RecordBlank()
        Dim dsDatos As DataSet
        Dim dtDatos As DataTable
        Dim mFila As DataRow


        Try

            'I create the columns
            dtDatos = New DataTable
            mFila = dtDatos.NewRow
            dtDatos.Columns.Add("COMERCIO_NUMERO")
            dtDatos.Columns.Add("COMERCIO_NOMBRE")
            dtDatos.Columns.Add("COMERCIO_DOMICILIO")
            dtDatos.Columns.Add("CONTRIBUYENTE_CUIT")


            'I believe the first record
            mFila = dtDatos.NewRow
            mFila("COMERCIO_NUMERO") = ""
            mFila("COMERCIO_NOMBRE") = ""
            mFila("COMERCIO_DOMICILIO") = ""
            mFila("CONTRIBUYENTE_CUIT") = ""
            dtDatos.Rows.Add(mFila)


            'I fill the dataset
            dsDatos = New DataSet
            dsDatos.Tables.Add(dtDatos)


            'I fill the grilla
            dsDatosEventosGrilla = dsDatos.Tables(0)
            dtDatosOriginal = dsDatos.Tables(0)
            Session("grilla_original") = dtDatosOriginal
            Session("grilla_actual") = dtDatosOriginal
            gvcomercios.DataSource = dsDatos.Tables(0)
            gvcomercios.DataBind()


            'I clear  the link of the first record
            For i = 0 To gvcomercios.Rows.Count - 1
                gvcomercios.Rows(i).Cells(4).Controls.Clear()
            Next

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvcomercios_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvcomercios.SelectedIndexChanged

    End Sub
End Class