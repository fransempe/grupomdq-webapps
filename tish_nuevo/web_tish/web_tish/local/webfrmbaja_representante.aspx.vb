﻿Option Explicit On
Option Strict On


Partial Public Class webfrmbaja_representante
    Inherits System.Web.UI.Page


#Region "Variables"
    Private mWS As ws_tish.Service1
    Private mXML As String
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mEventTarget As String
        Dim mEventArgument As String

        If Not (IsPostBack) Then

            Me.txtcuit.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtcuit.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtcuit.Attributes.Add("onkeydown", "javascript:return load_manager_key(this, event);")
            Me.btndown_user.Attributes.Add("onclick", "javascript:return down_user();")

            Call Me.SloganMuniicpalidad()
            Call Me.ValidarSession()
            Call Me.CargarPie()

            Me.txtcuit.Focus()

        Else



            mEventTarget = Request.Params.Get("__EVENTTARGET").ToString.Trim
            mEventArgument = Request.Params.Get("__EVENTARGUMENT").ToString.Trim
            If (mEventTarget IsNot Nothing) And (mEventArgument IsNot Nothing) Then
                If (mEventTarget.ToString.Trim = "down") Then
                    Call Me.DownUsers(mEventArgument.ToString.Trim)
                End If
            End If
        End If
    End Sub

    Private Sub SloganMuniicpalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema TISH."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema TISH."
        End If

    End Sub


    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If

        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub


    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("nombre_usuario") IsNot Nothing) Then
                If (Session("nombre_usuario").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        If (mLogin_OK) Then
            lblname_user.Text = Session("user_name").ToString.Trim
        Else
            Response.Redirect("webfrmlogin_rafam.aspx", False)
        End If

    End Function

    'Protected Sub btn_load_manager_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_load_manager.Click
    '    Call ObtainTrade()
    'End Sub

    Private Sub ObtainTrade()
        Dim dsDataManager As DataSet


        Try

            'I obtain the data of the trade
            mWS = New ws_tish.Service1
            dsDataManager = mWS.ObtainDataManager(txtcuit.Text.Trim)
            mWS = Nothing


            'I assign the data
            If (dsDataManager IsNot Nothing) Then
                'lblreason_social.Text = dsDataManager.Tables("Manager").Rows(0).Item("RAZON_SOCIAL").ToString.Trim
                'lblcontac.Text = dsDataManager.Tables("Manager").Rows(0).Item("CONTACTO").ToString.Trim
                'lbladdress.Text = dsDataManager.Tables("Manager").Rows(0).Item("DIRECCION").ToString.Trim
                'lbltelephone.Text = dsDataManager.Tables("Manager").Rows(0).Item("TELEFONO").ToString.Trim
                'lblemail.Text = dsDataManager.Tables("Manager").Rows(0).Item("EMAIL").ToString.Trim
            End If


        Catch ex As Exception

        End Try

    End Sub

 

    Private Function ValidateData(ByVal pMotive As String) As Boolean

        If (txtcuit.Text.Trim = "") Then
            Return False
        End If

        If (pMotive.ToString.Trim = "") Then
            Return False
        End If

        Return True
    End Function


    Private Sub DownUsers(ByVal pMotive As String)

        Try

            If (ValidateData(pMotive)) Then

                mWS = New ws_tish.Service1
                mXML = mWS.DownUsers(txtcuit.Text.Trim, pMotive.ToString.Trim)
                mWS = Nothing

                Session("type_messenger") = "operation"
                Response.Redirect("webfrmresultado.aspx", False)
            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btndown_user_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btndown_user.Click
        Call Me.DownUsers("baja")
    End Sub
End Class