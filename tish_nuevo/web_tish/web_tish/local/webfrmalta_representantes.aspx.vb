﻿Option Explicit On
Option Strict On

Partial Public Class webfrmalta_representantes
    Inherits System.Web.UI.Page

#Region "Variables"
    Private mWS As ws_tish.Service1
    Private mXML As String
    Private mPositionTrade As Integer
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mEvent As String
        Dim mPosition As Integer


        If Not (IsPostBack) Then

            Call Me.SloganMuniicpalidad()
            Call Me.CargarPie()
            Call Me.ValidarSession()


            'I assign the events javascript
            Me.btnagree.Attributes.Add("onclick", "javascript:return to_validate_data();")
            Me.btn_load_trade.Attributes.Add("onclick", "javascript:return to_validate_load_trade();")
            Me.btnadd_trade.Attributes.Add("onclick", "javascript:return to_validate_add_trade();")


            Me.txtcuit.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtcuit.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtreason_social.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtreason_social.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtcontact.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtcontact.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtaddress.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtaddress.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txttelephone.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txttelephone.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtemail.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtemail.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtnumber_trade.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtnumber_trade.Attributes.Add("onblur", "javascript:event_focus(this);")

      

            Me.div_container_trade.Visible = False
            Me.table_data_trade.Visible = False
            Me.div_container_trade.Visible = False


            Call Me.LoadGrilla_RecordBlank()
            Me.txtcuit.Focus()
        Else

            mEvent = Request.Params.Get("__EVENTTARGET")
            If (Request.Params.Get("__EVENTARGUMENT").ToString <> "") Then
                mPosition = CInt(Request.Params.Get("__EVENTARGUMENT").ToString)

                If (mEvent.ToString.Trim = "delete_trade") Then
                    Call Me.DeleteTrade(mPosition)
                End If
            End If

        End If
    End Sub

    Private Sub SloganMuniicpalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema TISH."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema TISH."
        End If

    End Sub

    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            Me.lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            Me.lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If


        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub


    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("user_name") IsNot Nothing) Then
                If (Session("user_name").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        If (mLogin_OK) Then                                     
            Me.lblnombre_usuario.Text = Session("user_name").ToString.Trim
        Else
            Response.Redirect("webfrmlogin_rafam.aspx", False)
        End If

    End Function




    'LoadGrilla_RecordBlank:
    'This procedure loads the grilla with a row in white
    Private Sub LoadGrilla_RecordBlank()
        Dim dsDatos As DataSet
        Dim dtDatos As DataTable
        Dim mFila As DataRow





        Try

            'I create the columns
            dtDatos = New DataTable
            mFila = dtDatos.NewRow
            dtDatos.Columns.Add("COMERCIO_NUMERO")
            dtDatos.Columns.Add("COMERCIO_RAZONSOCIAL")
            dtDatos.Columns.Add("CONTRIBUYENTE_NOMBRE")



            'I believe the first record
            mFila = dtDatos.NewRow
            mFila("COMERCIO_NUMERO") = ""
            mFila("COMERCIO_RAZONSOCIAL") = ""
            mFila("CONTRIBUYENTE_NOMBRE") = ""
            dtDatos.Rows.Add(mFila)


            'I fill the dataset
            dsDatos = New DataSet
            dsDatos.Tables.Add(dtDatos)



            Session("dsList_Trades") = dsDatos.Clone


            'I fill the grilla
            Me.gvlist_trade.DataSource = dsDatos.Tables(0)
            Me.gvlist_trade.DataBind()


            'I clear the link of the first record
            Me.gvlist_trade.Rows(0).Cells(3).Controls.Clear()


        Catch ex As Exception

        End Try
    End Sub


    Private Sub ObtainTrade()
        Dim mXML As String
        Dim dsData As DataSet
        Dim dsDataRafam As DataSet
        Dim dsData_AUX As DataSet
        Dim dtData As DataTable
        Dim mRow As DataRow
        Dim txtnumer As String

        Try
            'Si es RAFAM
            If (System.Configuration.ConfigurationManager.AppSettings("TipoConexion").ToString.Trim = "RAFAM") Then

                Me.mWS = New ws_tish.Service1
                txtnumer = txtnumber_trade.Text
                mXML = Me.mWS.ObtainDataTrade(CLng(txtnumer))
                Me.mWS = Nothing

                dsDataRafam = New DataSet
                dsDataRafam.ReadXml(New IO.StringReader(mXML.ToString.Trim))

                'I create the columns
                dtData = New DataTable
                mRow = dtData.NewRow
                dtData.Columns.Add("COMERCIO_NUMERO")
                dtData.Columns.Add("COMERCIO_RAZONSOCIAL")
                dtData.Columns.Add("CONTRIBUYENTE_NOMBRE")

                For i = 0 To dsDataRafam.Tables(1).Rows.Count - 1
                    'I believe the first record
                    mRow = dtData.NewRow
                    mRow("COMERCIO_NUMERO") = dsDataRafam.Tables(1).Rows(0).Item("COMERCIO_NUMERO").ToString.Trim
                    mRow("COMERCIO_RAZONSOCIAL") = dsDataRafam.Tables(1).Rows(0).Item("COMERCIO_RAZONSOCIAL").ToString.Trim
                    mRow("CONTRIBUYENTE_NOMBRE") = dsDataRafam.Tables(1).Rows(0).Item("CONTRIBUYENTE_NOMBRE").ToString.Trim
                    dtData.Rows.Add(mRow)

                Next


                'I fill the dataset
                dsData_AUX = New DataSet
                dsData_AUX.Tables.Add(dtData)
                Session("dsSelect_Trade") = dsData_AUX.Copy



                Me.lblreason_social.Text = dsData_AUX.Tables(0).Rows(0).Item("COMERCIO_RAZONSOCIAL").ToString.Trim
                Me.lblcontribuyente.Text = dsData_AUX.Tables(0).Rows(0).Item("CONTRIBUYENTE_NOMBRE").ToString.Trim
            End If


            'Si es FOX
            If (System.Configuration.ConfigurationManager.AppSettings("TipoConexion").ToString.Trim = "FOX") Then


                'I obtain the data of the trade
                Me.mWS = New ws_tish.Service1
                mXML = Me.mWS.ObtenerDatosComercio(Me.txtnumber_trade.Text.Trim)
                Me.mWS = Nothing
                'I create the XML on the disc to read it
                dsData = New DataSet
                dsData = clsTools.ObtainDataXML(mXML.ToString.Trim)
                'I create the columns
                dtData = New DataTable
                mRow = dtData.NewRow
                dtData.Columns.Add("COMERCIO_NUMERO")
                dtData.Columns.Add("COMERCIO_RAZONSOCIAL")
                dtData.Columns.Add("CONTRIBUYENTE_NOMBRE")



                'Si el dataset no esta vacio asigno los datos
                If (clsTools.HasData(dsData)) Then
                    If (dsData.Tables(0).Rows(0).Item("COMERCIO_ESTADO").ToString.Trim = "OK") Then


                        For i = 0 To dsData.Tables(0).Rows.Count - 1

                            'I believe the first record
                            mRow = dtData.NewRow
                            mRow("COMERCIO_NUMERO") = dsData.Tables(0).Rows(0).Item("COMERCIO_NUMERO").ToString.Trim
                            mRow("COMERCIO_RAZONSOCIAL") = dsData.Tables(0).Rows(0).Item("COMERCIO_RAZONSOCIAL").ToString.Trim
                            mRow("CONTRIBUYENTE_NOMBRE") = dsData.Tables(0).Rows(0).Item("CONTRIBUYENTE_NOMBRE").ToString.Trim
                            dtData.Rows.Add(mRow)

                        Next


                        'I fill the dataset
                        dsData_AUX = New DataSet
                        dsData_AUX.Tables.Add(dtData)
                        Session("dsSelect_Trade") = dsData_AUX.Copy



                        Me.lblreason_social.Text = dsData_AUX.Tables(0).Rows(0).Item("COMERCIO_RAZONSOCIAL").ToString.Trim
                        Me.lblcontribuyente.Text = dsData_AUX.Tables(0).Rows(0).Item("CONTRIBUYENTE_NOMBRE").ToString.Trim



                    Else
                        Me.lblreason_social.Text = "Comercio Inactivo."
                        Me.lblcontribuyente.Text = ""
                    End If




                Else
                    Me.lblreason_social.Text = "Comercio Inexistente."
                    Me.lblcontribuyente.Text = ""
                End If
            End If
            Me.table_select_trade.Visible = False
            Me.table_data_trade.Visible = True

        Catch ex As Exception
            Me.table_select_trade.Visible = True
            Me.table_data_trade.Visible = False
        End Try

    End Sub



    Protected Sub btn_load_trade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_load_trade.Click
        Call Me.ObtainTrade()
    End Sub

    Protected Sub btnadd_trade_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnadd_trade.Click
        Dim dsList_Trades As DataSet
        Dim dsSelect_Trade_AUX As DataSet
        Dim dtList_Trades_AUX As DataTable
        Dim mRow As DataRow
        Try

            If (Session("dsSelect_Trade") IsNot Nothing) Then


                dsList_Trades = New DataSet
                dsList_Trades = CType(Session("dsList_Trades"), DataSet)

                dsSelect_Trade_AUX = New DataSet
                dsSelect_Trade_AUX = CType(Session("dsSelect_Trade"), DataSet)


                'To validate data
                If Not (Me.ValidateData(dsList_Trades, dsSelect_Trade_AUX)) Then
                    Exit Sub
                End If


                dtList_Trades_AUX = New DataTable
                dtList_Trades_AUX = dsList_Trades.Tables(0).Copy


                'I create the first record
                mRow = dsList_Trades.Tables(0).NewRow()
                mRow("COMERCIO_NUMERO") = dsSelect_Trade_AUX.Tables(0).Rows(0).Item("COMERCIO_NUMERO").ToString.Trim
                mRow("COMERCIO_RAZONSOCIAL") = dsSelect_Trade_AUX.Tables(0).Rows(0).Item("COMERCIO_RAZONSOCIAL").ToString.Trim
                mRow("CONTRIBUYENTE_NOMBRE") = dsSelect_Trade_AUX.Tables(0).Rows(0).Item("CONTRIBUYENTE_NOMBRE").ToString.Trim
                dsList_Trades.Tables(0).Rows.Add(mRow)



                'dsList_Trades.Tables.Add(dtList_Trades_AUX)
                Session("dsList_Trades") = dsList_Trades.Copy


                Me.mPositionTrade = 0
                Me.gvlist_trade.DataSource = dsList_Trades.Tables(0)
                Me.gvlist_trade.DataBind()



                Me.txtnumber_trade.Text = ""
                Me.lblreason_social.Text = ""
                Me.lblcontribuyente.Text = ""
                'txtnumber_trade.Focus()

            End If

            Me.table_select_trade.Visible = True
            Me.table_data_trade.Visible = False
            Me.div_list_trades.Visible = True
        Catch ex As Exception

        End Try


    End Sub

    Private Sub gvlist_trade_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvlist_trade.RowCommand
        Dim dsList_Trades As DataSet
        Dim mSelectedRow As Integer


        'I obtain the selected row
        mSelectedRow = Convert.ToInt32(e.CommandArgument)


        dsList_Trades = New DataSet
        dsList_Trades = CType(Session("dsList_Trades"), DataSet).Copy


        dsList_Trades.Tables(0).Rows.RemoveAt(mSelectedRow)
        Session("dsList_Trades") = dsList_Trades.Copy



        If (dsList_Trades.Tables(0).Rows.Count = 0) Then
            Call Me.LoadGrilla_RecordBlank()
        Else
            Me.gvlist_trade.DataSource = dsList_Trades.Tables(0)
            Me.gvlist_trade.DataBind()
        End If

    End Sub

    Private Sub gvlist_trade_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvlist_trade.RowDataBound
        Dim dsDataTrade_AUX As DataSet

        If (e.Row.RowType = DataControlRowType.DataRow) Then

            dsDataTrade_AUX = New DataSet
            dsDataTrade_AUX = CType(Session("dsList_Trades"), DataSet).Copy

            If (clsTools.HasData(dsDataTrade_AUX)) Then

                e.Row.Cells(3).Attributes.Add("onclick", "javascript:return delete_trade(" & Me.mPositionTrade.ToString.Trim & ");")
                e.Row.Cells(3).ToolTip = "Eliminar comercio (Número " & dsDataTrade_AUX.Tables(0).Rows(Me.mPositionTrade).Item(0).ToString.Trim() & ")"
                Me.mPositionTrade = Me.mPositionTrade + 1

            End If

            dsDataTrade_AUX = Nothing
        End If
    End Sub

    Protected Sub link_manager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_manager.Click

        If (Me.link_manager.Text = "[Ver]") Then
            Me.link_manager.Text = "[Ocultar]"
            Me.div_manager.Visible = True
        Else
            Me.link_manager.Text = "[Ver]"
            Me.div_manager.Visible = False
        End If

    End Sub

    Protected Sub link_add_trade_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_add_trade.Click

        If (Me.link_add_trade.Text = "[Ver]") Then
            Me.link_add_trade.Text = "[Ocultar]"
            Me.div_container_trade.Visible = True
        Else
            Me.link_add_trade.Text = "[Ver]"
            Me.div_container_trade.Visible = False
        End If

    End Sub

    Protected Sub btnagree_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnagree.Click
        If (Me.ValidateData()) Then
            Call Me.Agree()
        End If
    End Sub



    Private Function Agree() As Boolean
        Dim dsList_Trades As DataSet
        Dim mDataManager As String
        Dim mDataTrades As String


        mDataManager = ""
        If (Me.txtcuit.Text.Trim <> "") Then
            mDataManager = Me.txtcuit.Text.Trim & "|"
        End If

        If (Me.txtreason_social.Text.Trim <> "") Then
            mDataManager = mDataManager.ToString.Trim & Me.txtreason_social.Text.Trim & "|"
        End If

        If (Me.txtcontact.Text.Trim <> "") Then
            mDataManager = mDataManager.ToString.Trim & Me.txtcontact.Text.Trim & "|"
        End If

        If (Me.txtaddress.Text.Trim <> "") Then
            mDataManager = mDataManager.ToString.Trim & Me.txtaddress.Text.Trim & "|"
        End If

        If (Me.txttelephone.Text.Trim <> "") Then
            mDataManager = mDataManager.ToString.Trim & Me.txttelephone.Text.Trim & "|"
        End If

        If (Me.txtemail.Text.Trim <> "") Then
            mDataManager = mDataManager.ToString.Trim & Me.txtemail.Text.Trim & "|"
        End If




        dsList_Trades = New DataSet
        dsList_Trades = CType(Session("dsList_Trades"), DataSet).Copy

        mDataTrades = ""
        For i = 0 To dsList_Trades.Tables(0).Rows.Count - 1
            mDataTrades = mDataTrades.ToString.Trim & dsList_Trades.Tables(0).Rows(i).Item(0).ToString.Trim & "|"
        Next




        Try

            'I obtain the data of the trade
            Me.mWS = New ws_tish.Service1
            Me.mXML = Me.mWS.CreateUserWeb(mDataManager.ToString.Trim, mDataTrades.ToString.Trim)
            Me.mWS = Nothing

        Catch ex As Exception

        End Try




        'Variables para la Impresion
        If (mXML.ToString.Trim <> "") Then
            Session("state_pdf") = False
            Session("data_manager") = mXML.ToString.Trim & "|" & mDataManager.ToString.Trim
            Response.Redirect("webfrmimprimir_clave.aspx", False)
        End If



    End Function



    Private Function ValidateData() As Boolean
        Dim mValidate As Boolean
        Dim mExistUser As Boolean
        Dim dsList_Trades As DataSet

        mValidate = False        
        Me.lblmessage_user_web.Visible = False
        Me.div_menssage_userweb.Visible = False


        Try


            mExistUser = False
            Me.mWS = New ws_tish.Service1
            mExistUser = Me.mWS.UserWebExist(Me.txtcuit.Text.Trim)
            Me.mWS = Nothing


            'Verifivo que el usuario a registrar no exista en la base de datos
            If (mExistUser) Then
                Me.lblmessage_user_web.Text = "El usuario Web ya se encuentra registrado en el sistema."
                Me.div_menssage_userweb.Visible = True
                Me.lblmessage_user_web.Visible = True
                Me.txtcuit.Focus()
                Return False
            End If




            'Verifico que se alla cargado al menos un comercio
            dsList_Trades = New DataSet
            dsList_Trades = CType(Session("dsList_Trades"), DataSet)

            If Not (clsTools.HasData(dsList_Trades)) Then
                Me.lblmensaje.Text = "Debe cargar al menos un comercio para este usuario Web."
                Me.div_mensaje.Visible = True
                Me.lblmensaje.Visible = True

                'Muestro la carga de comercios
                If (Me.link_add_trade.Text = "[Ver]") Then
                    Me.link_add_trade.Text = "[Ocultar]"
                    Me.div_container_trade.Visible = True
                End If

                Me.txtnumber_trade.Focus()
                Return False
            End If


            mValidate = True
        Catch ex As Exception
            mValidate = False
        End Try

        Return mValidate
    End Function



    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btncancel.Click
        Me.div_mensaje.Visible = False
        Me.lblmensaje.Visible = False
        Me.txtnumber_trade.Text = ""
        Me.table_select_trade.Visible = True
        Me.table_data_trade.Visible = False
        Me.txtnumber_trade.Focus()
    End Sub

    'This function delete the selected trade
    Private Sub DeleteTrade(ByVal pPosition As Integer)
        Dim mSelectedRow As Integer
        Dim dsDataTrade_AUX As DataSet


        'I obtain the selected row
        mSelectedRow = Convert.ToInt32(pPosition)

        'I Obtain the dataset
        dsDataTrade_AUX = New DataSet
        dsDataTrade_AUX = CType(Session("dsList_Trades"), DataSet).Copy


        'I delete el record
        dsDataTrade_AUX.Tables(0).Rows.RemoveAt(mSelectedRow)

        'I Update the session variable and I update the grid
        Session("dsList_Trades") = dsDataTrade_AUX



        If (dsDataTrade_AUX.Tables(0).Rows.Count = 0) Then
            Call Me.LoadGrilla_RecordBlank()
        Else
            Me.gvlist_trade.DataSource = dsDataTrade_AUX
            Me.gvlist_trade.DataBind()
        End If



    End Sub


    'Validate the trade for add
    Private Function ValidateData(ByVal pdsList_Trades As DataSet, ByVal pdsSelect_Trade_AUX As DataSet) As Boolean

        'To validate trade
        If (Me.lblreason_social.Text = "Comercio Inactivo.") Then
            Me.lblmensaje.Text = "No puede agregar un comercio inactivo."
            Me.div_mensaje.Visible = True
            Me.lblmensaje.Visible = True
            Return False
        End If

        If (Me.lblreason_social.Text = "Comercio Inexistente.") Then
            Me.lblmensaje.Text = "No puede agregar un comercio inexistente."
            Me.div_mensaje.Visible = True
            Me.lblmensaje.Visible = True
            Return False
        End If


        'to validate existing trade
        Me.lblmensaje.Visible = False
        Me.div_mensaje.Visible = False
        For i = 0 To pdsList_Trades.Tables(0).Rows.Count - 1
            If (pdsList_Trades.Tables(0).Rows(i).Item("COMERCIO_NUMERO").ToString = pdsSelect_Trade_AUX.Tables(0).Rows(0).Item("COMERCIO_NUMERO").ToString.Trim) Then
                Me.lblmensaje.Text = "El comercio seleccionado ya se encuentra relacionado a este usuario Web."
                Me.div_mensaje.Visible = True
                Me.lblmensaje.Visible = True
                Return False
            End If
        Next

        Return True
    End Function

End Class