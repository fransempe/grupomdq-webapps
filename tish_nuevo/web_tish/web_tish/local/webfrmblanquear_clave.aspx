﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmblanquear_clave.aspx.vb" Inherits="web_tish.webfrmblanquear_clave" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Sistema TISH :: Generación de nueva clave de acceso Web</title>
        <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
    	
        
        
        
            
        <!-- Seteo the resources -->
        <script src="../js/funciones.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../css/estilos_listado.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_botones.css" type="text/css" />     
        <link rel="stylesheet" href="../css/estilos_carteles.css" type="text/css" />       
        <%           
            'Seteo el css que voy a utilizar
            Dim mPath_css As String                
            mPath_css = ""
            mPath_css = System.Configuration.ConfigurationManager.AppSettings("css").ToString.Trim()
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_listado.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_genericos.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_botones.css' type='text/css' />")            
        %> 
        
        
        <!-- mascara CUIT -->
        <script type="text/javascript" src="../js/jquery.js"></script>
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" language="javascript"  src="../js/jquery.maskedinput.js"></script>
        <script type="text/javascript">
            jQuery(function($){
                $("#txtcuit").mask("99-99999999-9"); 
            });
        </script>
        
        
        
        <!-- ajax -->       
        <script type="text/javascript" language="javascript" src="../js/jquery1-2-6-min.js"></script>
        <script type="text/javascript" language="javascript">
        
            jQuery.noConflict();
            function ajax_load_manager(p_cuit){                                
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_load_manager.ashx",                                        
                    data:"action=clear&cuit=" + p_cuit, 
                    success: see_response
                });
            }
            
            
            function see_response(html){                   
                if (html != '') {
                    var _control = document.getElementById('div_data_manager');
                    _control.style.display = 'none';
                    _control.innerHTML = html;
                    show_div();
                    return false;
                }
                
            return true;    
            }          
            
            
            
	        function show_div() {	
	            var _control;

		        jQuery("#div_data_manager").slideToggle();		
		        
		         _control = document.getElementById('div_button');
		        if (!control_exists(document.getElementById('lblmessenger'))) {		            
		            _control.style.display = 'block';            
		        } else {
		            _control.style.display = 'none';            
		        }		             
	        }
	        
	        
	        
	        
	        
	        /* Carga del logo de la municipalidad */
            function ajax_load_logo(){   
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_logo.ashx",                                                            
                    success: see_response_logo
                });
            }
            
            
            function see_response_logo(html){                   
                if (html != '') {
                    var _control = document.getElementById('div_header');                   
                    _control.innerHTML = html;                   
                    return false;
                }
                
            return true;    
            }          
	        
        </script>
     
     
     
     
     
        <script type="text/javascript">
            function load_manager_key(val, ev){

                if (window.event) {
                    var key = window.event.keyCode;
                } else {
                    var key = ev.which;
                }
                
              
                if (key == 13 || key == 9){    
                   /* return load_manager(); */
                   window.document.getElementById('btnload_manager').focus();
                }
            }
        </script>
     
     
     
        
        
        <script type="text/javascript">
            var _control;
            var _codehtml;
        
        
            function load_manager() {  
            
                if (validate_data()) {
                
                    _codehtml = '<table border="0" width="100%" align="center">' +
                                    '<tr>' + 
                                        '<td align="right" style="width:35%;"><img alt="" src="../imagenes/loading.gif"/ width="24px" height="24px" /></td>' + 
                                        '<td align="left" style="width:75%;"><p>Cargando datos ...</p></td>' +                                      
                                    '</tr>' +
                                '</table>';


                
                    _control = document.getElementById('div_data_manager');                
                    _control.innerHTML = _codehtml;
                              
                    ajax_load_manager(window.document.getElementById('txtcuit').value);
                
                }
            return false;
            }
        
        
        
             function validate_data() {

                    //Cadena Vacia
                    if (control_vacio(window.document.getElementById('txtcuit'),'Número de comercio:\nDebe ingresar un número de comercio.')) {
                       return false;
                    }
                    
                    /*
                    //Cadena Valida
                    if (!validar_cadena_solo_numeros(window.document.getElementById('txtcuit'),'Número de comercio')) {          
                        return false;
                    }
                    */
                return true;
                }
                
                
                
            function clear_key() {
                var _messenger;
                
                _messenger = false;
                _messenger = confirm('Usted esta a punto de generar una nueva clave.\n ¿Desea continuar?');    
                if (_messenger) {
                
                    _codehtml = '<table border="0" width="100%" align="center">' +
                                    '<tr>' + 
                                        '<td align="right" style="width:35%;"><img alt="" src="../imagenes/loading.gif"/ width="24px" height="24px" /></td>' + 
                                        '<td align="left" style="width:75%;"><p>Generando clave ...</p></td>' +                                      
                                    '</tr>' +
                                '</table>';
                
                     
                    _control = document.getElementById('div_data_manager');                
                    _control.innerHTML = _codehtml;
               
                
                    __doPostBack("btnclear_key",'')               
                   
                    return true;
                }
                
            return false;
            }
            
                
        </script>
        
        
        
   
        
        
        
        
        <script type="text/javascript">

            function validar_datos() {

                /* -- Nro de Comercio -- */
                //Cadena Vacia
                if (control_vacio(window.document.getElementById('txtnro_comercio'),'Número de comercio:\nDebe ingresar un número de comercio.')) {
                   return false;
                }
                
                //Cadena Valida
                if (!validar_cadena_solo_numeros(window.document.getElementById('txtnro_comercio'),'Número de comercio')) {          
                    return false;
                }
                
            return true;
            }
            
    
    
            /* Valido que la clave exista y le pregunto al usuario si quiere remplazar la clave*/
            function generar_clave() {
            
                /* -- Nro de Comercio -- */
                //Cadena Vacia
                if (control_vacio(window.document.getElementById('txtnro_comercio'),'Número de comercio:\nDebe ingresar un número de comercio.')) {
                   return false;
                }
                
                //Cadena Valida
                if (!validar_cadena_solo_numeros(window.document.getElementById('txtnro_comercio'),'Número de comercio')) {          
                    return false;
                }
                
                
                /* -- Cuit del contribuyente -- */
                //Cadena Vacia
                if (control_vacio(window.document.getElementById('txtcontribuyente_cuit'),'Cuit contribuyente:\nDebe ingresar el cuit del contribuyente.')) {
                   return false;
                }
                
                //Cadena Valida
                if (!validar_cadena_solo_numeros(window.document.getElementById('txtcontribuyente_cuit'),'Cuit contribuyente')) {          
                    return false;
                }
            
            
            
                // Validar comercio cargado
                if (window.document.getElementById('lblcomercio_nombre').innerHTML === '') {          
                    alert('Generacion de clave:\nDebe seleccionar un comercio.');
                    return false;
                }
            
                
                           
                
                return imprimir_clave();
                
            
            return true;
            }
            
    
    
    /* I warn him the user who for the trade selecciondo already exists a key */    
    function existing_key() {
        if (window.document.getElementById('lblestado_clave').innerHTML == 'CLAVE EXISTENTE.') {
            return confirm ('El comercio seleccionado ya contiene un clave para el ingreso Web.\n'
                            + '¿Desea remplazarla de todas formas?')
        }
    }
    
    
    
    /* Valido que clave exista y le recomiendo que use la ultima version de Acrobat */    
    function imprimir_clave() {
        var existing_key_aux = false;
        
        
        /* I obtain the value of the function */   
        existing_key_aux = existing_key();
       
       
        if (existing_key_aux) {
                     
            if (window.document.getElementById('lblestado_clave').innerHTML != 'CLAVE INEXISTENTE.') {
                return confirm("Usted está a punto de generar una constancia de clave.\n"
                               + "Para el correcto funcionamiento se recomienda descargar la última versión de Adobe Reader.\n"
                               + "¿Desea continuar?")
            }
        }
        
    return existing_key_aux;
    }
    
       
    
    
    /* Esta Funcion ASIGNA ESTILO a un BOTON */          
    function efecto_over(boton){
        boton.className = "boton_gris"; 	        
	}
	    

    /* Esta Funcion ASIGNA ESTILO a un BOTON */          
  	function efecto_out(boton){
        boton.className = "boton_verde";    
	}



</script>
        
        
         <!-- recursos para crear la ventana modal -->
	    <link rel="stylesheet" href="../modal/Assets/LightFace.css" />	
	    <script src="../modal/mootools.js" type="text/javascript"></script>
	    <script src="../modal/mootools-more-drag.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.IFrame.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Image.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Request.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Static.js" type="text/javascript"></script>
        

        <!-- Logout -->
        <script type="text/javascript">
        	function logout() {	 	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema TISH :: Cerrar sesión', 
				    width: 250,
				    height: 50,
	  			    content: '<div align="left">Usted esta a punto de cerrar su sesión.\n¿Desea continuar?</div>',
	 			    buttons: [		
	 			        {
						    title: 'Aceptar',
						    event: function() { this.close();window.location= "../webfrmindex.aspx";}
					    },
					    {
						    title: 'Cerrar',
						    event: function() { this.close();}
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }        
        </script>
        
        
        
        
    </head>

    <body>
    
        
        <!-- wrap starts here -->	
        <div id="wrap">
                     
		    <div id="header">
		          
		        <!-- div container font -->
                <div id="container_font">			        
		        <div class="div_font" style="padding:0px 5px 0px 5px;">
                    <a href="#" title="Usar fuente menor" class="link_font" onclick="assign_font('-');">A-</a> 
                </div>
                
                <div class="div_font">
		            <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 
                </div>		            
                
		        <div class="div_font">
		            <a href="#" title="Usar fuente mayor" class="link_font" onclick="assign_font('+');">A+</a> 
		        </div>	            
            </div>
                    
	        <div id="div_header"  class="div_image_logo">                
 	            <img  class="image_logo" alt="" src="../imagenes/topiz.jpg"  />
            </div>   
		                
			<h1 id="logo-text">Tasa de Inspección por Seguridad e Higiene</h1>			
			<h2 id="slogan"><asp:Label ID="lblslogan_municipalidad" runat="server" Text=""></asp:Label></h2>	
			
		</div>
	  
            <!-- content-wrap starts here -->
            <div id="content-wrap">
    	  
    	  
    	        <!-- div container page title -->
                <div id="div_title" style="background-color:Black;" align ="left" >
                    <strong class="titulo_page">Sistema TISH :: Generación de nueva clave de acceso Web</strong>
                </div>
    	  
    	  
                <!-- div container right -->
                <div id="div_optiones" class="menu_options">
                    
                    <div id="div_comercio" style=" padding-top:20px;">
                            <div align="center">
                                <table  style="width:95%">
                                    <tr>
                                        <td  rowspan="2"><img id ="user" alt="" src="../imagenes/user.png"  style=" width:60px; height:60px; border:none"/></td>
                                        <td><p  class="titulo_usuario"align ="left" style="color:Black; height: 25px;">Bienvenido</p></td>
                                    </tr>
                                    <tr>                                
                                        <td>
                                            Usuario: <asp:Label ID="lblname_user" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        
                    <h1 align ="left" style="color:Black">Opciones</h1>                    			
                    <ul class="sidemenu">                                       	                    
                        <li><a href="webfrmopciones_rafam.aspx" >Volver a opciones</a></li>	                					
                        <li><a href="#" onclick="javascript:logout();">Cerrar sesión</a></li>	                					
                    </ul>	
                </div>
    	    
    	  
    	  
  		        <div id="main"> 
    			
                    
                    <form id="formulario" runat="server">
                        
                    
                        <div id="div_container_trade" align="center" style="padding-bottom:30px;"  runat="server">
                    
            		   
            		     
            		            <!-- message for the user -->
                                
                        
                                <!-- div add trade -->
                                <div id="add_trade" align="center" style="width:90%">
                                         
                                    <div id="div_list_trades" style="padding-top:20px; width:100%;" align="center">
                                    
                                        <table border="0" align="center" style="width:100%;">
                                            <tr>
                                                <td style= "padding-top:10px;" align="center">
                                                   <table id="table_data_manager" border="0" cellspacing="10" style="border-style: double; width: 90%; height: 220px; background-color: #FFFFFF; color: #000000;" align="center">
                            
                                                        <tr>
                                                            <td class="column_data1" align="right">CUIT:</td>
                                                            <td class="column_data2">
                                                                <asp:TextBox ID="txtcuit" runat="server" CssClass="textbox" MaxLength="11" Width="80%" ></asp:TextBox>                                        
                                                            </td>
                                                            
                                                            <td >
                                                                 <asp:ImageButton ID="btnload_manager" runat="server" ImageUrl="~/imagenes/load.gif"  CssClass="boton_load" TabIndex="1"  />                                                                                                                              
                                                            </td>
                                                        </tr> 
                                                     
                                                       <tr>
                                                            <td id ="td_data_manager" colspan="3" align="center">  
                                                                <div id="div_data_manager"  style="display:none;" align="center">
                                                                
                                                                </div>                                                     
                                                            </td>
                                                       </tr>                                     
                                                    </table>
                                                
                                                                   		                            
            		                            		                            
                                                </td>
                                            </tr>
                                        </table>                                                                    
                                        <div id="div_button" align="center" style="padding-top:15px; display:none;">
                                            <table>
                                                <tr>
                                                    <td align="center">
                                                        <asp:ImageButton ID="btnclear_key" runat="server" Height="32px" 
                                                            ImageUrl="~/imagenes/key.gif" Width="32px" />                                                                                                                    
                                                     </td>
                                                     <td>                                                      
                                                        <strong>Blanquear clave</strong>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                        
                                        
                                        
                                     
                                     
                                </div>
                              	
                              
                    
                        
	                    </div>
	                    
	                    
                       
	                </form>
    				
      								
  		        </div> 	
    
	        </div>
		
		
		    <!-- div data footer -->
		    <div id="footer">	
		       <div id="div_pie_municipalidad" 
                    style="width:70%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		            Municipalidad de
		                <strong> 
                            <asp:Label ID="lblnombre_municipalidad" runat="server" Text=""></asp:Label> 
                        </strong>                    
				    <br />
				    Teléfono: 
				        <strong> 
                            <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                        </strong> 
                     | Email: 
				        <strong> 
                            <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                        </strong> 
                    
			    </div>
    		   
		       <div id="div_pie_grupomdq"  class="footer_grupomdq">
				    Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ S.A</a></strong>  
				    <br />
				    Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				    <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
                   <asp:Label ID="lblversion" runat="server" Text=""></asp:Label>
		       </div>
		    </div>	
		    
        </div>	



    </body>
</html>
