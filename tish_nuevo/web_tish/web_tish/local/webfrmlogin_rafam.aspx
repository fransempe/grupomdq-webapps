﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmlogin_rafam.aspx.vb" Inherits="web_tish.webfrmlogin_rafam" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        
        
        <!-- Seteo the resources -->
        <script src="../js/funciones.js" type="text/javascript"></script>        
        <link rel='stylesheet' href='../css/estilos_login.css' type='text/css' />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_carteles.css" type="text/css" />
        <%           
            'Seteo el css que voy a utilizar
            Dim mPath_css As String                
            mPath_css = ""
            mPath_css = System.Configuration.ConfigurationManager.AppSettings("css").ToString.Trim()
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_login.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_genericos.css' type='text/css' />")
        %>
        
        
                
        <!-- Virtual keyboard -->       
        <script type="text/javascript" src="../keyboard/keyboard.js" charset="UTF-8"></script>
        <link rel="stylesheet" href="../keyboard/keyboard.css" type="text/css" />
        
        
        
        <!-- ajax -->       
        <script type="text/javascript" language="javascript" src="../js/jquery1-2-6-min.js"></script>       
        <script type="text/javascript" language="javascript">
        
            function login(puser, pkey){
                $.ajax({
                    type:"POST",
                    url:"../controladores/handler_login_rafam.ashx",
                    data:"user=" + puser + "&key=" + pkey,
                    success: ver_respuesta
                });
            }
            
            
            function ver_respuesta(html){            
                if (html == 'MAL') {
                    var _control = document.getElementById('mensaje');
                    _control.innerHTML = '<span id="lblmensaje" style="color: Red; font-weight: bold;">Código de Verificación Incorrecto.</span>';
                    return false;
                }
                
            return true;    
            }

        
        
        
        
        
        
        
        
        
        
            /* Carga del logo de la municipalidad */
            function ajax_load_logo(){   
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_logo.ashx",                                        
                    success: see_response_logo
                });
            }
            
            
            function see_response_logo(html){                   
                if (html != '') {
                    var _control = document.getElementById('div_header');                   
                    _control.innerHTML = html;                   
                    return false;
                }
                
            return true;    
            }          
        </script>

        
        <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
    
        
        <title>Sistema TISH :: Login</title>	
        
        
        <script type="text/javascript">

            function key_up_txt(e, p_control) {                    
                pressed_key = (document.all) ? e.keyCode : e.which;
                if (pressed_key == 13){
                    document.getElementById(p_control).focus();    
                }               
            }
        </script>
        
        
        
        <!-- reasignar_fuente: -->
        <!-- Esta funcion reasigna el tamaño de la fuente en el documento -->
        <script type="text/javascript">
        
            var tamano_fuente = 10
            
            function reasignar_fuente(tamanio_fuente) {
                
                /* Asigno la fuente al documento */
                //document.body.style.fontSize = tamanio_fuente + "px"
                
               // document.body.style.font = '70%/1.5  Verdana, 'Trebuchet MS', arial, sans-serif';
                
            }
        </script>
        
        
        
        <script type="text/javascript">

    function validar_datos() {

        
        //Cadena Vacia
        if (control_vacio(window.document.getElementById('txtusuario'),'Usuario:\nDebe ingresar un nombre de usuario')) {
           return false;
        }
        
        //Cadena Valida
        if (!validar_cadena(window.document.getElementById('txtusuario'),'Usuario')) {          
            return false;
        }
        
        
        
        
        //Cadena Vacia
        if (control_vacio(window.document.getElementById('txtclave'),'Clave:\nDebe ingresar Clave')) {
           return false;
        }
        
        //Cadena Valida
        if (!validar_cadena(window.document.getElementById('txtclave'),'Clave')) {          
            return false;
        }
        
        
    
     
     
        //Cadena Vacia
        if (control_vacio(window.document.getElementById('txtcaptcha'),'Debe Ingresar el Codigo de Confirmacion')) {          
            return false;
        }


        if (!validar_cadena(window.document.getElementById('txtcaptcha'),'Codigo de Verificación')) {          
            return false;
        }
    

        /* check the login with ajax */
        var _login_ok = false;
        _login_ok = login(window.document.getElementById('txtcuit').value,window.document.getElementById('txtclave').value);
        if (_login_ok)  {
            return false;
        }
     
    return true;
    }
    
    
    


 
    
    
    
    function consultar_deuda(val, ev){

        if (window.event) {
            var key = window.event.keyCode;
        } else {
            var key = ev.which;
        }
        
      
        if (key == 13) {    
            if (validar_datos()){
                __doPostBack("link_login",'');
            }
        }
    }



    

</script>
        
        
    </head>

    <body>
    
    
    
        <!-- wrap starts here -->	
        <div id="wrap">
         
        
         
         
		<div id="header">
		 
		   <!-- div container font -->
            <div id="container_font">			        
		        <div class="div_font" style="padding:0px 5px 0px 5px;">                    
                    <a href="#" title="Usar fuente menor" class="link_font" onclick="assign_font('-');">A-</a> 
                </div>
                
                <div class="div_font">
		            <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 		            
                </div>		            
                
		        <div  style="width:5%; float:left;">
		            <a href="#" title="Usar fuente mayor" class="link_font" onclick="assign_font('+');">A+</a> 
		        </div>	
		        
            </div>
                    
                    
            <div id="div_header"  class="div_image_logo">                
                <img  class="image_logo" alt="" src="../imagenes/topiz.jpg"  />
            </div>     
     
		 
		
			<h1 id="logo-text">Tasa de Inspección por Seguridad e Higiene</h1>			
			<h2 id="slogan"><asp:Label ID="lblslogan_municipalidad" runat="server" Text=""></asp:Label></h2>	
				
			
		</div>
		
		
	  
	  <!-- content-wrap starts here -->
	  <div id="content-wrap">
	  
	  
	        <!-- div container page title -->
            <div id="div_title" style="background-color:Black;" align ="left" >
                <strong class="titulo_page">Sistema TISH :: Login (Uso interno de la municipalidad)</strong>
            </div>
	  
	  
	  		<div id="main"> 
				
			
            <div id="div_login" align="center" style="padding-bottom:30px;">
                
                
		        <form id="formulario" runat="server">
		         		   
		   <div id="mensaje" style="padding-top:50px; padding-bottom:5px;">
		        <div id="div_mensaje" class="Globo GlbRed" style="width:45%;" runat="server" visible="false">
		            <asp:Label ID="lblmensaje" runat="server" Text="" Visible="False"></asp:Label>
		        </div>
           </div>
           
             
            <div id="div_tabla_login" align="center" style="width:100%; height:209px;">     
             
                
		        <table id="tabla_login"  class="table_login" border="1" cellspacing="10" align="center">
                        <tr>
                            <td class="table_login_column1">USUARIO:</td>
                            <td class="table_login_column2">
                                <asp:TextBox ID="txtusuario" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                            </td>
                        </tr> 
                                        
                        <tr>
                            <td class="table_login_column1">CLAVE:</td>                        
                            <td id = "td_keyboard" class="columna_dato">
                               <asp:TextBox ID="txtclave" runat="server" CssClass="keyboardInputInitiator" MaxLength="20" TextMode="Password"></asp:TextBox>
                            </td>                                        
                        </tr>
                                                                
                        <tr>
                             <td class="table_login_captcha">
                                        <cc1:captchacontrol ID="verificador_captcha" runat="server" CaptchaLength="5"
                                                 CaptchaLineNoise="Low" CaptchaMinTimeout="5"
                                                 CaptchaMaxTimeout="240" FontColor = "Black" 
                                            Font-Size="XX-Large" Height="50px" 
                                                                    Width="179px" 
                                                CaptchaChars="ACDEFGHJKLNPQRTUVXYZ2346789" BackColor="White" 
                                             />
                                        </td>
                            <td class="table_login_column2">
                                <asp:TextBox ID="txtcaptcha" runat="server" CssClass="textbox" Font-Bold="True" 
                                    MaxLength="5"></asp:TextBox>
                                 <br />
                                            <asp:LinkButton Id="link_login" runat="server"  CssClass="link">[Login]</asp:LinkButton>
                            </td>
                        </tr>
                    </table>    	
                
            </div>
                    
                    
		         </form>
		         
		 </div>
				
					
						
				
			
				
								
	  		</div> 	
			  
	  		 	
		
		<!-- content-wrap ends here -->
		</div>
		
		<div id="footer">
		
		   <div id="div_pie_municipalidad" 
                style="width:70%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		        Municipalidad de
		            <strong> 
		                <asp:Label ID="lblnombre_municipalidad" runat="server" Text="NOMBRE_MUNICIPIO"></asp:Label> 
                    </strong> 
                    
				<br />
				Teléfono: 
				    <strong> 
                        <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                    </strong> 
                 | Email: 
				    <strong> 
                        <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                    </strong> 
                
			</div>
		   
		   <div id="div_pie_grupomdq"  class="footer_grupomdq">
				Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ S.A</a></strong>  
				<br />
				Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				<a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
				<asp:Label ID="lblversion" runat="server" Text=""></asp:Label>
		   </div>
            
			
		</div>	

<!-- wrap ends here -->		
</div>	



    </body>
</html>