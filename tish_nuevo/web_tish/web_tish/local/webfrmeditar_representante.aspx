﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmeditar_representante.aspx.vb" Inherits="web_tish.webfrmeditar_representante" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
              
        
        <!-- Seteo the resources -->
        <script src="../js/funciones.js" type="text/javascript"></script>       
        <link rel="stylesheet" href="../css/estilos_listado.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_botones.css" type="text/css" />        
        <link rel="stylesheet" href="../css/estilos_carteles.css" type="text/css" />
        <%           
            'Seteo el css que voy a utilizar
            Dim mPath_css As String                
            mPath_css = ""
            mPath_css = System.Configuration.ConfigurationManager.AppSettings("css").ToString.Trim()
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_listado.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_genericos.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_botones.css' type='text/css' />")            
        %> 
        
        
        
        
        
        <!-- mascara CUIT -->
        <script type="text/javascript" src="../js/jquery.js"></script>
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" language="javascript"  src="../js/jquery.maskedinput.js"></script>
        <script type="text/javascript">
            jQuery(function($){
                $("#txtcuit").mask("99-99999999-9"); 
            });
        </script>
        
        
        
        <!-- ajax -->       
        <script type="text/javascript" language="javascript" src="../js/jquery1-2-6-min.js"></script>
        <script type="text/javascript" language="javascript">
        
            /* Carga del logo de la municipalidad */
            function ajax_load_logo(){   
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_logo.ashx",                                                            
                    success: see_response_logo
                });
            }
            
            
            function see_response_logo(html){                   
                if (html != '') {
                    var _control = document.getElementById('div_header');                   
                    _control.innerHTML = html;                   
                    return false;
                }
                
            return true;    
            }          
        </script>
        
        
        
        <!-- Esta funcion da el efecto de cargando... miestras va al server -->
        <link href="../efecto_ajax/cssUpdateProgress.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript">
	        var ModalProgress = '<%= ModalProgress.ClientID %>';         
        </script>
        
        
        <title>Sistema TISH :: Edición de usuarios Web</title>
        <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
    
        
        
         <script type="text/javascript">
         
           function validar_nro_cuit() {
              
                /* -- Nro de Comercio -- */
                //Cadena Vacia
                /*
                if (!validar_cuit(window.document.getElementById('txtcuit'))) {
                   return false;
                }
                */
                
                  loading();
                  
                 
                  
                
            return true;
            }

         
            
           function validar_nro_comercio() {
              
                /* -- Nro de Comercio -- */
                //Cadena Vacia
                if (control_vacio(window.document.getElementById('txtnumber_trade'),'Número de comercio:\nDebe ingresar un número de comercio.')) {
                   return false;
                }
                
                //Cadena Valida
                /*
                if (!validar_cadena_solo_numeros(window.document.getElementById('txtnumber_trade'),'Número de comercio')) {          
                    return false;
                }
                */
                
              
                
                
            return true;
            }
         
         </script>
         
         
         <!-- valido los dato del contribuyente -->
         <script type="text/javascript">
             function to_validate_data() {

                 /*--------------- CUIT ----------------*/
                 /* control empty */
                 if (control_vacio(window.document.getElementById('txtcuit'), 'CUIT:\nDebe ingresar un número de cuit.')) {
                     return false;
                 }


                 /*---------- RAZON SOCIAL ------------*/
                 /* control empty */
                 if (control_vacio(window.document.getElementById('txtreason_social'), 'Razon social:\nDebe ingresar la razon social.')) {
                     return false;
                 }

                 /* reason social validate */
                 if (!validar_cadena(window.document.getElementById('txtreason_social'), 'Razon social')) {
                     return false;
                 }


                 /*------------- CONTACT ---------------*/
                 /* control empty */
                 if (control_vacio(window.document.getElementById('txtcontact'), 'Contacto:\nDebe ingresar un contacto.')) {
                     return false;
                 }

                 /* contact validate */
                 if (!validar_cadena(window.document.getElementById('txtcontact'), 'Contacto')) {
                     return false;
                 }


                 /*------------- ADDRESS --------------*/
                 /* control empty */
                 if (control_vacio(window.document.getElementById('txtaddress'), 'Direccion:\nDebe ingresar un direccion.')) {
                     return false;
                 }

                 /* address validate */
                 if (!validar_cadena(window.document.getElementById('txtaddress'), 'Direccion')) {
                     return false;
                 }
                
             return true;
             }
        </script>
         
        
        
        
        <script type="text/javascript">

            function validar_datos() {

                /* -- Nro de Comercio -- */
                //Cadena Vacia
                if (control_vacio(window.document.getElementById('txtnro_comercio'),'Número de comercio:\nDebe ingresar un número de comercio.')) {
                   return false;
                }
                
                //Cadena Valida
                /*
                if (!validar_cadena_solo_numeros(window.document.getElementById('txtnro_comercio'),'Número de comercio')) {          
                    return false;
                }
                */
            return true;
            }
            
    
    
            /* Valido que la clave exista y le pregunto al usuario si quiere remplazar la clave*/
            function generar_clave() {
            
                /* -- Nro de Comercio -- */
                //Cadena Vacia
                if (control_vacio(window.document.getElementById('txtnro_comercio'),'Número de comercio:\nDebe ingresar un número de comercio.')) {
                   return false;
                }
                
                //Cadena Valida
                if (!validar_cadena_solo_numeros(window.document.getElementById('txtnro_comercio'),'Número de comercio')) {          
                    return false;
                }
                
                
                /* -- Cuit del contribuyente -- */
                //Cadena Vacia
                if (control_vacio(window.document.getElementById('txtcontribuyente_cuit'),'Cuit contribuyente:\nDebe ingresar el cuit del contribuyente.')) {
                   return false;
                }
                
                //Cadena Valida
                if (!validar_cadena_solo_numeros(window.document.getElementById('txtcontribuyente_cuit'),'Cuit contribuyente')) {          
                    return false;
                }
            
            
            
                // Validar comercio cargado
                if (window.document.getElementById('lblcomercio_nombre').innerHTML === '') {          
                    alert('Generacion de clave:\nDebe seleccionar un comercio.');
                    return false;
                }
            
                
                           
                
                return imprimir_clave();
                
            
            return true;
            }
            
    
    
    /* I warn him the user who for the trade selecciondo already exists a key */    
    function existing_key() {
        if (window.document.getElementById('lblestado_clave').innerHTML == 'CLAVE EXISTENTE.') {
            return confirm ('El comercio seleccionado ya contiene un clave para el ingreso Web.\n'
                            + '¿Desea remplazarla de todas formas?')
        }
    }
    
    
    
    /* Valido que clave exista y le recomiendo que use la ultima version de Acrobat */    
    function imprimir_clave() {
        var existing_key_aux = false;
        
        
        /* I obtain the value of the function */   
        existing_key_aux = existing_key();
       
       
        if (existing_key_aux) {
                     
            if (window.document.getElementById('lblestado_clave').innerHTML != 'CLAVE INEXISTENTE.') {
                return confirm("Usted está a punto de generar una constancia de clave.\n"
                               + "Para el correcto funcionamiento se recomienda descargar la última versión de Adobe Reader.\n"
                               + "¿Desea continuar?")
            }
        }
        
    return existing_key_aux;
    }
    
    
    
    
    
    /* Esta funcion genera el mensaje al usuario de que esta a punto de cerrar la session de usuario*/
    function cerrar_session() {
        return confirm("Usted esta a punto de cerrar su sesión\n¿Desea continuar?")              
    }
    
    
    
    /* Esta funcion elimina los comercios asociados al contribuyente */          
    function delete_trade(pos){   
        var respuesta;
        
        respuesta = confirm('Usted esta a punto de eliminar el comercio seleccionado\n¿Desea continuar?');
        if (respuesta) {
            __doPostBack("delete_trade", pos);
        }
	}
 
 
 
    function loading_key(val, ev){
        if (window.event) {
            var key = window.event.keyCode;
        } else {
            var key = ev.which;
        }
        
     
        if (key == 13 || key == 9){    
           return loading(); 
        }
    }
 
 

    function loading() {
        window.document.getElementById('img_loading').style.display = 'block';
        window.document.getElementById('btnload').style.display = 'none';
        
        __doPostBack("load_manager", '');
        return true;
    }
   
   
   
   
     

</script>
        
        
    </head>

    <body>
    
        
        <!-- wrap starts here -->	
        <div id="wrap">
                     
		    <div id="header">
		          
		        <!-- div container font -->
                <div id="container_font">			        
		        <div class="div_font" style="padding:0px 5px 0px 5px;">
                    <a href="#" title="Usar fuente menor" class="link_font" onclick="assign_font('-');">A-</a> 
                </div>
                
                <div class="div_font">
		            <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 
                </div>		            
                
		        <div class="div_font">
		            <a href="#" title="Usar fuente mayor" class="link_font" onclick="assign_font('+');">A+</a> 
		        </div>	            
            </div>
                    
		    
	        <div id="div_header"  class="div_image_logo">                
 	            <img  class="image_logo" alt="" src="../imagenes/topiz.jpg"  />
            </div>   
		                
			<h1 id="logo-text">Tasa de Inspección por Seguridad e Higiene</h1>			
			<h2 id="slogan"><asp:Label ID="lblslogan_municipalidad" runat="server" Text=""></asp:Label></h2>	
						
			
		</div>
	  
            <!-- content-wrap starts here -->
            <div id="content-wrap">
            
                <!-- div container page title -->
                <div id="div_title" style="background-color:Black;" align ="left" >
                    <strong class="titulo_page">Sistema TISH :: Edición de usuarios Web</strong>
                </div>
    	  
    	  
                <!-- div container right -->
                <div id="div_optiones" class="menu_options">
                    
                    <div id="div_comercio" style=" padding-top:20px;">
                            <div align="center">
                                <table  style="width:95%">
                                    <tr>
                                        <td  rowspan="2"><img id="user" alt="" src="../imagenes/user.png"  style=" width:60px; height:60px; border:none" onclick="return user_onclick()" /></td>
                                        <td><p  class="titulo_usuario"align ="left" style="color:Black; height: 25px;">Bienvenido</p></td>
                                    </tr>
                                    <tr>                                
                                        <td>
                                            Usuario: <asp:Label ID="lblnombre_usuario" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        
                    <h1 align ="left" style="color:Black">Opciones</h1>                    
                    <ul class="sidemenu">                                       	                    
                        <li><a href="webfrmopciones_rafam.aspx" >Volver a opciones</a></li>	                					
                        <li><a href="../webfrmindex.aspx" onclick="javascript:return cerrar_session();">Cerrar sesión</a></li>	                					
                    </ul>	
                </div>
    	    
    	  
    	  
  		        <div id="main"> 
    			
                    
                    <form id="formulario" runat="server">
                        
                        
                        <div id="div_container" align="center" style="padding-bottom:30px;"  runat="server">
                    
            		       
	                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	                        
	                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
	                            <ContentTemplate >
                    
                               
                    
                    
                    
            		            <!-- message for the user -->
                                <div id="div_messenger" class="Globo GlbRed" style="width:70%;" runat="server" visible="false">
                                    <asp:Label ID="lblmessenger" runat="server" Text="" Visible="False"></asp:Label>
                                </div>
                               
                        
                    
                    
                        <div id="div_link_manager"  runat="server" align="left"> 
                            <strong class="titulo_3">Usuario Web</strong>  - 
                            <asp:LinkButton ID="link_manager" runat="server">[Ocultar]</asp:LinkButton>
                            <hr class="linea"/>
                        </div>	
                        
                    
                        <!-- div data manager-->
                        <div id="div_manager" align="center" style="width:100%;  height:200px; padding-bottom:0px;" runat="server">
                            <table id="table_data_manager" border="0" style="width: 70%; height: 136px; background-color: #FFFFFF; color: #000000;" align="center"> 
                                <tr>
                                    <td class="columna_titulo_left">CUIT:</td>
                                    <td class="columna_dato_left" align="center">
                                        <asp:TextBox ID="txtcuit" runat="server" CssClass="textbox" MaxLength="11" Width="88%"></asp:TextBox>
                                    </td>
                                    <td id="td_load" runat="server">                                   
                                        <img id="img_loading" alt="" src="../imagenes/loading.gif"  style="display:none;"/>                                         
                                        <img id="btnload" alt="" src="../imagenes/load.gif" class="boton_load" onclick="javascript:return loading();" />                                         
                                    </td>                                                                                         
                                </tr>                                 

                                <tr>
                                    <td class="columna_titulo_left">Razón social:</td>                        
                                    <td colspan="2" class="columna_dato_left">
                                        <asp:TextBox ID="txtreason_social" runat="server" CssClass="textbox" MaxLength="100" Width="98%"></asp:TextBox>
                                    </td>                                                                                                   
                                </tr>
                                
                                <tr>
                                    <td class="columna_titulo_left">Persona de contacto:</td>                        
                                    <td  colspan="2" class="columna_dato_left">
                                        <asp:TextBox ID="txtcontac" runat="server" CssClass="textbox" MaxLength="100" Width="98%"></asp:TextBox>
                                    </td>                                                                                                   
                                </tr>
                                
                                <tr>
                                    <td class="columna_titulo_left">Dirección:</td>                        
                                    <td  colspan ="2" class="columna_dato_left">
                                        <asp:TextBox ID="txtaddress" runat="server" CssClass="textbox" MaxLength="100" Width="98%"></asp:TextBox>
                                    </td>                                                                                                   
                                </tr>
                                
                                <tr>
                                    <td class="columna_titulo_left">Teléfono:</td>                        
                                    <td  colspan="2" class="columna_dato_left">
                                        <asp:TextBox ID="txttelephone" runat="server" CssClass="textbox" MaxLength="100" Width="98%"></asp:TextBox>
                                    </td>                                                                                                   
                                </tr>
                                
                                <tr>
                                    <td class="columna_titulo_left">e-mail</td>                        
                                    <td colspan="2" class="columna_dato_left">
                                        <asp:TextBox ID="txtemail" runat="server" CssClass="textbox" MaxLength="100" Width="98%"></asp:TextBox>
                                    </td>                                                                                                   
                                </tr>                        
                                
                            
                            </table>   
                        
                        </div>  
                
                    
                    
                        <div id="div_link_add_trade"  runat="server" align="left"> 
                            <strong class="titulo_3">Comercios asociados</strong>  - 
                            <asp:LinkButton ID="link_add_trade" runat="server">[Ver]</asp:LinkButton>
                            <hr class="linea"/>
                        </div>	
                        
                        
                        
                      

                    
                       
            		     <div id="div_container_trade" align="center" style="padding-bottom:0px;"  runat="server">
            		     
            		     
            		     
                                <!-- div new user -->
                                <div id="add_user" align="center" style="width:100%">
                                
                                    <div id="div_table_trade" style="width:100%">
                                    
                                        <!-- message for the user -->
                                        <div id="div_messenger_trade" class="Globo GlbRed" style="width:90%;" runat="server" visible="false">
                                            <asp:Label ID="lblmessenger_trade" runat="server" Text="" Visible="False"></asp:Label>
                                        </div>
                                       
                                    
                                        <div id="div_table_add_trade">
                                            <asp:LinkButton ID="link_table_add_trade" runat="server">Agregar comercios</asp:LinkButton>
                                        </div>
                                        <table id="table_trade" border="0" style="width: 100%; background-color: #FFFFFF; color: #000000;" align="center">                                        
                                            <tr>   
                                                <td>            		                                                                       
                                                    <table id="table_select_trade" align="center"  border="0" style="width:100%;"  runat="server">
                                                    
                                                        <tr>
                                                            <td  align="right" style="width:25%">Nro. Comercio:</td>
                                                            
                                                            <td  class="columna_dato_left" style="width:25%">
                                                                <asp:TextBox ID="txtnumber_trade" runat="server" CssClass="textbox" MaxLength="10" Width="100%"></asp:TextBox>
                                                            </td>
                                                            
                                                           <td  class="columna_dato_left" style="width:25%">  
                                                                <asp:ImageButton ID="btn_load_trade" runat="server" 
                                                                    ImageUrl="~/imagenes/load.gif" CssClass="boton_load" Height="30px" Width="30px" 
                                                                />                                                                                                                                                                       
                                                           </td>
                                                        </tr>
                                                    </table>
                                                        
                                                        
                                                    <table id="table_data_trade" align="center"  border="0" style="width:100%;" runat="server">
                                                        <tr>
                                                            <td class="columna_dato_left" style="width:10%">Razón Social:</td>                                        
                                                            <td class="columna_dato_left" style="width:90%" align="left">                                                        
                                                                <asp:Label ID="lblreason_social" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td class="columna_dato_left" style="width:10%">Contribuyente:</td>                                        
                                                            <td class="columna_dato_left" style="width:90%" align="left">                                                        
                                                                <asp:Label ID="lblcontribuyente" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr align="right">
                                                            <td  colspan="2" align="right" style="width:100%;">
                                                                <asp:Button ID="btnadd_trade" runat="server"                                                          
                                                                    Text="Agregar" BorderStyle="None" 
                                                                    CssClass="boton" Height="30px" Width="70px"                                                                     
                                                                    ToolTip="Agregar comercio."                                                           
                                                                />                                                              
                                                            
                                                                <asp:Button ID="btncancel" runat="server"                                                          
                                                                    Text="Cancelar" BorderStyle="None" 
                                                                    CssClass="boton" Height="30px" Width="70px"              
                                                                    ToolTip="Cancelar."
                                                                />
                                                            </td>                                                            
                                                        </tr>
                                                                
                                                    </table>                                        
                                                </td>
                                            </tr>                                        
                                        </table>
                                    </div>                                                  
                                         
                                    <div id="div_list_trades" style="padding-top:0px; width:100%;" runat="server">
                                        <table id="table1" border="0" style="width: 100%; background-color: #FFFFFF; color: #000000;" align="center">                                                    		            
                                            <tr>   
                                                <td>
                		                            
                                                    <!-- Div contenedor de la tabla de movimientos -->
                                                    <div id="div_tabla_movimientos" runat="server" >                                    
                        		                         		                
                                                
                                                        <asp:GridView ID="gvlist_trade" runat="server" AutoGenerateColumns="False" 
                                                            BackColor="White" BorderColor="#999999" 
                                                            BorderStyle="Solid" BorderWidth="1px" CellPadding="1" ForeColor="Black" 
                                                            GridLines="Vertical" PageSize="5" Width="100%" AllowSorting="True" 
                                                            EnableTheming="True" >
                                        
                                                            <FooterStyle BackColor="#CCCCCC" />
                                                            <RowStyle ForeColor="Black" />
                                                        
                                                                <Columns>
                                                                    <asp:BoundField HeaderText="Número" DataField="TRADE_NUMBER" 
                                                            HeaderStyle-Width="55px">
                                                            <HeaderStyle Width="55px" HorizontalAlign="Center" />
                                                            <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </asp:BoundField>                                                        
                                                                    <asp:BoundField DataField="TRADE_NAME" HeaderText="Razon social" >                                     
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="40%" />
                                                        </asp:BoundField>                                     
                                                                    <asp:BoundField DataField="CONTRIBUYENTE_NOMBRE" HeaderText="Contribuyente" >
                                                                    <FooterStyle HorizontalAlign="Center" />
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                    <ItemStyle HorizontalAlign="Left" Width="40%" />
                                                                </asp:BoundField>
                                                                
                                                                
                                                            <asp:templatefield HeaderText="" ShowHeader="False"  ControlStyle-Font-Bold="true" ItemStyle-Width="5px" HeaderStyle-BackColor="Black" >
                                                            <HeaderTemplate>Eliminar</HeaderTemplate>
                                                            
                                                            <ItemTemplate> 
                                                                <asp:Image ID="image1"  runat="server"  ImageUrl="~/imagenes/delete.gif" Width="16px" Height="16px"/>
                                                            </ItemTemplate>
                                                                 
                                                            
                                                            <ControlStyle CssClass="columna_eliminar" />
                                                                 
                                                            
                                                            <HeaderStyle BackColor="Black" VerticalAlign="NotSet" Wrap="True" 
                                                                HorizontalAlign="Center"  Width="5%"/>
                                                            <ItemStyle Wrap="True" Width="5%" HorizontalAlign="Center"  />                                                             
                                                        </asp:templatefield>
                                                                
                                                                
                                                                </Columns>
                                                    
                                                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                            <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                                            <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                                                            <AlternatingRowStyle BackColor="#CCCCCC" />
                                                        </asp:GridView>
                                                     
                                                    
                                                    </div>   
                                                     
                                                </td>                                
                                            </tr>                                                     
                                        </table>                                                                          
                                    </div>
                                    
                                     
                                </div>
                              	
                         </div>
                         
                         <div align="right">
                            <asp:Button ID="btnedit" runat="server"                                                          
                                Text="Guardar cambios" BorderStyle="None" 
                                CssClass="boton" Height="35px" Width="120px"  
                                ToolTip="Guardar cambios."          
                                Visible = "false"
                            />                                        
                         </div>
                        
	                      </ContentTemplate >		
                           </asp:UpdatePanel>
                        
                            <!-- panel ajax-->
                            <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="../efecto_ajax/updateProgress">
                                <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server">
                                    <ProgressTemplate>
	                                    <div style="position: relative; text-align: center;">
		                                    <img src="../efecto_ajax/loading.gif" style="vertical-align: middle" alt="Processing" />
		                                    Cargando ...
	                                    </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </asp:Panel>
                            <ajaxToolkit:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress" BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
                
                        
	                    </div>
	                    
	                    
                       
	                </form>
    				
      								
  		        </div> 	
    
	        </div>
		
		
		    <!-- div data footer -->
		    
		    <div id="footer">	
		       <div id="div_pie_municipalidad" 
                    style="width:70%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		            Municipalidad de
		                <strong> 
		                    <asp:Label ID="lblnombre_municipalidad" runat="server" Text="NOMBRE_MUNICIPIO"></asp:Label> 
                        </strong> 
                        
				    <br />
				    Teléfono: 
				        <strong> 
                            <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                        </strong> 
                     | Email: 
				        <strong> 
                            <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                        </strong> 
                    
			    </div>
    		   
		       <div id="div_pie_grupomdq" class="footer_grupomdq">
				    Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ S.A</a></strong>  
				    <br />
				    Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				    <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
                   <asp:Label ID="lblversion" runat="server" Text=""></asp:Label>
		       </div>
		    </div>	
		    
        </div>	



    </body>
</html>
