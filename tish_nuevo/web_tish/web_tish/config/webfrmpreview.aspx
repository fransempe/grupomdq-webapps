﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmpreview.aspx.vb" Inherits="web_tish.webfrmpreview" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <script src="../js/funciones.js" type="text/javascript"></script>
        
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_botones.css" type="text/css" />
        
        <%
            If (1 = 1) Then
                Response.Write("<link rel='stylesheet' href='../css/estilos_listado.css' type='text/css' />")
            Else
                Response.Write("<link rel='stylesheet' href='../css/estilos_prueba.css' type='text/css' />")
            End If
        %>
        
        <script type="text/javascript" language="javascript" src="js/colorPicker.js"></script>
        <link rel="stylesheet" href="css/colorPicker.css" type="text/css"/>
      
        
        
        <title>Sistema TISH</title>	
        
        
        <script type="text/javascript" language="javascript">
        
            function change_color_preview(p_control, p_control_backgroundcolor, p_control_color) {                        
                window.document.getElementById(p_control).style.backgroundColor = window.document.getElementById(p_control_backgroundcolor).style.backgroundColor;                            
                window.document.getElementById(p_control).style.color = window.document.getElementById(p_control_color).style.backgroundColor;            
            }
        
            function change_titles() {
                alert("llego");
                window.document.getElementById('body').style.link= "#FAF0E6";
                alert("paso");
            }
        
        
            function change_font() {             
                alert (window.document.getElementById('text_font').value);            
                window.document.getElementById('footer1').style.font = window.document.getElementById('text_font').text;
            }
            
        </script>
        
        
        
        
        
    </head>

    <body id="body">
    
    
    
        <!-- wrap starts here -->	
        <div id="wrap">
         
         
         <div>
            
                <div style="background-color:black; color:White;">
                    Configuracion visual de TISH
                </div>
            
                <form id="formconfig"  method="post" action="webfrmconfig_preview.aspx">
                    <div style="background-color:Gray; color:White;">
                    <table border="1">
                    <tr>
                        <td><input type="text" id="txtbody" name="txtbody" onclick="startColorPicker(this)" onkeyup="maskedHex(this)"/></td>
                        <td>Fondo</td>
                        <td><input type="button"  value="Refrescar"  class="boton_generico_verde"  onclick="change_color_preview('body', 'txtbody');" /> </td>
                    </tr>
                    
                    
                    <tr>
                        <td><input type="text" id="txtheader" onclick="startColorPicker(this)" onkeyup="maskedHex(this)"/></td>
                        <td>Cabecera</td>
                        <td><input type="button" value="Refrescar" class="boton_generico_verde"  onclick="change_color_preview('header', 'txtheader');" /> </td>
                    </tr>
                    
                    
                    <tr>
                        <td><input type="text" id="txtcontent" onclick="startColorPicker(this)" onkeyup="maskedHex(this)"/></td>
                        <td>Contenido</td>
                        <td><input type="button" value="Refrescar" class="boton_generico_verde"  onclick="change_color_preview('content-wrap', 'txtcontent');" /> </td>
                    </tr>

                    
                    <tr><td colspan="5">Pie de la pagina</td></tr>
                    <tr>                                                
                        <td>Fondo</td>
                        <td><input type="text" id="txtbackground_footer" onclick="startColorPicker(this)" onkeyup="maskedHex(this)"/></td>
                        <td><input type="button" value="Refrescar" class="boton_generico_verde"  onclick="change_color_preview('footer', 'txtbackground_footer', 'txtword_footer');" /> </td>
                        
                        <td>Color de la letra</td>
                        <td><input type="text" id="txtword_footer" onclick="startColorPicker(this)" onkeyup="maskedHex(this)"/></td>
                        <td><input type="button" value="Refrescar" class="boton_generico_verde"  onclick="change_color_preview('footer', 'txtbackground_footer', 'txtword_footer');" /> </td>
                    </tr>
                    
                    
                    <tr><td colspan="5">Generales</td></tr>
                    <tr>                                                
                        <td>Titulos</td>
                        <td><input type="text" id="txttitles" onclick="startColorPicker(this)" onkeyup="maskedHex(this)"/></td>
                        <td><input type="button" value="Refrescar" class="boton_generico_verde"  onclick="change_titles();" /> </td>
                        
                        <td>Links</td>
                        <td><input type="text" id="txtlinks" onclick="startColorPicker(this)" onkeyup="maskedHex(this)"/></td>
                        <td><input type="button" value="Refrescar" class="boton_generico_verde"  onclick="change_color_preview('footer', 'txtbackground_footer', 'txtword_footer');" /> </td>
                    </tr>
                       

                    <tr><td colspan="5">
                        
                        <input id="Submit1" type="submit"value="submit" /></td>
                    </tr>


                </table>
                </form>            
                
                
            </div>
            
         </div>
         
		<div id="header">
		          
		    <!-- div container font -->
            <div id="div_container_font" style="width:60%; height:20%; float:left; padding-left:10px;">			        
		        <div  style="width:7%; float:left;  padding:0px 5px 0px 5px;">
                    <a href="#" title="Usar fuente menor" style="font-family: Jokerman; font-size: large; font-weight: bold;" onclick="assign_font('-');">A-</a> 
                </div>
                
                <div  style="width:7%; float:left;">
		            <a href="#" title="Usar fuente por defecto" style="font-family: Jokerman; font-size: large; font-weight: bold;" onclick="assign_font('=');">A</a> 
                </div>		            
                
		        <div  style="width:7%; float:left;">
		            <a href="#" title="Usar fuente mayor" style="font-family: Jokerman; font-size: large; font-weight: bold;" onclick="assign_font('+');">A+</a> 
		        </div>	            
            </div>
                    
		    
		          
		          
		                
			<h1 id="logo-text">Tasa de Seguridad e Higiene</h1>			
			<h2 id="slogan">Sistema TISH.</h2>	
					
						
			
		</div>
	  
	  <!-- content-wrap starts here -->
	  <div id="content-wrap">
	  
	  
	       <!-- div container right -->
                <div id="div_optiones" style="width:27%;  height:100%; float:right;">
                
                    <div id="div_comercio" style=" padding-top:20px;">
                        <div align="center">
                            <table class="modelborder"  style="width:95%">
                                <tr>
                                    <td  rowspan="2"><img alt="" src="../imagenes/user.jpg"  style=" width:60px; height:60px; border:none"/></td>
                                    <td><p  class="titulo_usuario"align ="left" style="color:Black; height: 25px;">Bienvenido</p></td>
                                </tr>
                                <tr>                                
                                    <td>
                                        Usuario: <asp:Label ID="lblnombre_usuario" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                
                                
                            </table>
                        </div>
                       
                        
                        
                        
                        
                    </div>
                    
                    
                    <h1 align ="left" style="color:Black">Opciones</h1>                    			
	                <ul class="sidemenu">                                       
	                    <li><a href="../webfrmindex.aspx" onclick="">Menu 1</a></li>	                					
                        <li><a href="../webfrmindex.aspx" onclick="">Menu 2</a></li>	                					
	                </ul>	
                </div>
	    
	  
	  
	  
	  
	  		<div id="main"> 
	
	
	
				<a name="Sistema TISH"></a>
			
				<p>
				    <strong>Sistema TISH:</strong>
				    Esta función le permita a usted realizar alguna de las siguientes opciones para el gestionamientos de los accesos web al sistema TISH.
			    </p>  

				
			
			 
				
				
			<!-- Login -->				
            <h1>TITULO</h1>
            
            <div id="div_container" align="center" style="padding-bottom:30px;">
                
                 
		        
		         
        	</div>
				
					
						
				
			
				
								
	  		</div> 	
			  
	  		 	
		
		<!-- content-wrap ends here -->
		</div>
		
		<div id="footer">
		
		   <div id="div_pie_municipalidad" 
                style="width:72%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		        Municipalidad de
		            <strong> 
		                <asp:Label ID="lblnombre_municipalidad" runat="server" Text="NOMBRE_MUNICIPIO"></asp:Label> 
                    </strong> 
                    
				<br />
				Teléfono: 
				    <strong> 
                        <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                    </strong> 
                 | Email: 
				    <strong> 
                        <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                    </strong> 
                
			</div>
		   
		   <div id="div_pie_grupomdq"  style="width:26%; height:100%;  float:right; padding-top:5px; ">
				Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ.-</a></strong>  
				<br />
				Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				<a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>
		   </div>
			
			
		</div>	

<!-- wrap ends here -->		
</div>	



    </body>
</html>
