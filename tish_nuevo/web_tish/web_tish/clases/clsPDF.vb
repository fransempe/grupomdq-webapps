﻿Option Explicit On
Option Strict On



Imports iTextSharp.text
Imports iTextSharp.text.pdf


'Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Text



Public Class ClsPDF

    Private Enum eManager
        Key = 0
        Cuit = 1
        Reason_Social = 2
        Contac = 3
        Address = 4
        Telephone = 5
        Email = 6
    End Enum


#Region "Variables"

  
    Private mRutaFisica As String


    'Variables para Generar el LOGO, SOLO PARA RAFAM
    Private mLogoOrganismo As Byte()


    'Variables de Comercio
    Private mDataManager() As String
    



    'Variable que contiene los datos que son configurables
    Private dsDatosConfigurables As DataSet


    Private dsMunicipalidadData As DataSet
    Private mMunicipalidadName As String

#End Region

#Region "Propertys"

    Public WriteOnly Property RutaFisica() As String
        Set(ByVal value As String)
            Me.mRutaFisica = value.Trim
        End Set
    End Property





    'Property Comercio
    Public WriteOnly Property DataManager() As String()
        Set(ByVal value As String())
            Me.mDataManager = value
        End Set
    End Property




    'Variables Configurables
    Public WriteOnly Property VariablesConfigurables() As DataSet
        Set(ByVal value As DataSet)
            Me.dsDatosConfigurables = value
        End Set
    End Property


    'Property MunicipalidadData
    Public WriteOnly Property MunicipalidadData() As DataSet
        Set(ByVal value As DataSet)
            Me.dsMunicipalidadData = value
        End Set
    End Property
    
      
#End Region


#Region "Constructor"

    Public Sub New()

        Me.mRutaFisica = ""


        'Variables para Generar el LOGO, SOLO PARA RAFAM
        Me.mLogoOrganismo = Nothing
        

        'Data Manager
        Erase Me.mDataManager
        


        'Variables Configurables
        Me.dsDatosConfigurables = Nothing


        'Property de Municipalidad
        Me.dsMunicipalidadData = Nothing
        Me.mMunicipalidadName = System.Configuration.ConfigurationManager.AppSettings("Nombre_Municipalidad").ToString.Trim()

    End Sub

    Protected Overrides Sub Finalize()

        Me.mRutaFisica = ""

        'Variables para Generar el LOGO, SOLO PARA RAFAM
        Me.mLogoOrganismo = Nothing


        'Data Manager
        Erase Me.mDataManager



        'Variables Configurables
        Me.dsDatosConfigurables = Nothing


        'Property de Municipalidad
        Me.dsMunicipalidadData = Nothing
        Me.mMunicipalidadName = ""


        MyBase.Finalize()
    End Sub

#End Region




    Public Function CrearPDF() As Boolean
        Dim mDocumentoPDF As Document
        Dim mWriter As PdfWriter
        Dim mOK As Boolean

        Try


            mOK = False
            mDocumentoPDF = New Document(iTextSharp.text.PageSize.A4, 15, 15, 50, 50)
            mWriter = PdfWriter.GetInstance(mDocumentoPDF, New System.IO.FileStream(mRutaFisica.ToString, System.IO.FileMode.Create))



            mWriter.ViewerPreferences = PdfWriter.PageLayoutSinglePage
            mDocumentoPDF.Open()

            Call Me.CrearEncabezadoLogo(mDocumentoPDF, mWriter)
            Call Me.CrearEncabezado(mDocumentoPDF, mWriter)
            Call Me.CrearVariablesConfigurables(mDocumentoPDF, mWriter)

            mDocumentoPDF.Close()
       




            mOK = True
        Catch ex As Exception
            'Call MyBase.GenerarLog(ex)
            mOK = False
        Finally
            mWriter = Nothing
            mDocumentoPDF = Nothing
        End Try


        Return mOK
    End Function

#Region "Crear PDF"

    Private Sub CrearEncabezadoLogo(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mLogo As Image
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte


        Dim mNombreLogo As String
        Dim mLogoHeight As Single
        Dim mLogoWidth As Single
        Dim mLogoCoordenadaX As Single
        Dim mLogoCoordenadaY As Single
        Dim mLogo_AUX As Byte()



        'Obtengo el Nombre del Logo segun la CONEXION y Seteo propiedades
        'Logo
        mLogo_AUX = clsTools.ObtenerLogo()
        mNombreLogo = "logo.jpg"
        mLogoHeight = 30
        mLogoWidth = 30
        mLogoCoordenadaX = 50
        mLogoCoordenadaY = 755






        'Agrego el Logo a PDF
        Try



            mLogo = iTextSharp.text.Image.GetInstance(mLogo_AUX)


            mLogo.SetAbsolutePosition(mLogoCoordenadaX, mLogoCoordenadaY)
            mLogo.ScaleAbsolute(mLogoWidth, mLogoHeight)
            mDocumentoPDF.Add(mLogo)
        Catch ex As Exception
            'Call GenerarLog(ex)
        End Try



        'Nombre de La Municipalidad
        cb = writer.DirectContent
        cb.BeginText()
        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 14)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Municipalidad de " & Me.mMunicipalidadName.Trim, 95, 765, 0)

        cb.SetFontAndSize(mFuente, 14)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Datos para el ingreso Web", 370, 765, 0)
        cb.EndText()


    End Sub

    Private Sub CrearEncabezado(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim mFuenteNegrita As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mValorY As Integer

        cb = writer.DirectContent



        'Rectangulo Contenedora 
        cb.Rectangle(30, 670, 540, 120)


        'Linea de Vertical
        cb.SetLineWidth(1)
        cb.MoveTo(330, 670)
        cb.LineTo(330, 790)


        'Linea de Encabezado
        cb.MoveTo(30, 750)
        cb.LineTo(570, 750)



        'Rectangulo clave 
        cb.Rectangle(345, 675, 210, 70)
        cb.Rectangle(350, 680, 200, 60)


        cb.Stroke()


        mFuente = FontFactory.GetFont(FontFactory.TIMES_ITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        mFuenteNegrita = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 10)


        cb.BeginText()




        mValorY = 735
        cb.SetFontAndSize(mFuenteNegrita, 12)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Datos del usuario Web:", 40, mValorY, 0)


        'Razon social del usuario Web
        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Razón social: ", 40, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Me.mDataManager(eManager.Reason_Social).ToString.Trim, 100, mValorY, 0)


        'Nombre de contacto del usuario Web
        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Contacto: ", 40, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Me.mDataManager(eManager.Contac).ToString.Trim, 100, mValorY, 0)


        'Cuit del usuario Web
        cb.SetFontAndSize(mFuenteNegrita, 15)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "CUIT: ", 375, (mValorY + 10), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Me.mDataManager(eManager.Cuit).ToString.Trim, 435, (mValorY + 10), 0)


        'CLAVE
        cb.SetFontAndSize(mFuenteNegrita, 15)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Clave: ", 370, (mValorY - 10), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Me.mDataManager(eManager.Key).ToString.Trim, 435, (mValorY - 10), 0)


        'Direccion del usuario Web
        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Dirección: ", 40, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Me.mDataManager(eManager.Address).ToString.Trim, 100, mValorY, 0)


        'Telefono del usuario Web
        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Teléfono: ", 40, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Me.mDataManager(eManager.Telephone).ToString.Trim, 100, mValorY, 0)


        cb.EndText()

    End Sub

    Private Sub CrearVariablesConfigurables(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim mFuenteNegrita As iTextSharp.text.pdf.BaseFont
        Dim mFuenteLineas As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mCoordenada_X As Integer
        Dim mCoordenada_Y As Integer



        'Fuentes
        mFuente = FontFactory.GetFont(FontFactory.TIMES_ITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        mFuenteNegrita = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        mFuenteLineas = FontFactory.GetFont(FontFactory.TIMES_ITALIC, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont


        cb = writer.DirectContent
        cb.BeginText()
        cb.SetFontAndSize(mFuente, 10)


        Try

            mCoordenada_X = 0
            mCoordenada_Y = 0
            If (dsDatosConfigurables IsNot Nothing) Then

                'Renglones 
                If (dsDatosConfigurables.Tables("Datos_Libres") IsNot Nothing) Then
                    If (dsDatosConfigurables.Tables("Datos_Libres").Rows(0).Item("Visible").ToString.Trim = "TRUE") Then
                        For i = 0 To dsDatosConfigurables.Tables("Dato").Rows.Count - 1
                            mCoordenada_X = CInt(dsDatosConfigurables.Tables("Dato").Rows(i).Item("Coordenada_X").ToString.Trim)
                            mCoordenada_Y = CInt(dsDatosConfigurables.Tables("Dato").Rows(i).Item("Coordenada_Y").ToString.Trim)
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosConfigurables.Tables("Dato").Rows(i).Item("Etiqueta").ToString.Trim, mCoordenada_X, mCoordenada_Y, 0)
                        Next
                    End If
                End If



                'Datos de la municipalidad
                If (dsDatosConfigurables.Tables("Datos_Municipio") IsNot Nothing) Then
                    If (dsDatosConfigurables.Tables("Datos_Municipio").Rows(0).Item("Visible").ToString.Trim = "TRUE") Then

                        'Nombre Municipio
                        If (dsDatosConfigurables.Tables("Nombre").Rows(0).Item("Visible").ToString.Trim = "TRUE") Then
                            mCoordenada_X = CInt(dsDatosConfigurables.Tables("Nombre").Rows(0).Item("Coordenada_X").ToString.Trim)
                            mCoordenada_Y = CInt(dsDatosConfigurables.Tables("Nombre").Rows(0).Item("Coordenada_Y").ToString.Trim)
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Municipalidad de " & Me.mMunicipalidadName.Trim & ".", mCoordenada_X, mCoordenada_Y, 0)
                        End If


                        'Direccion Municipio
                        If (dsDatosConfigurables.Tables("Direccion").Rows(0).Item("Visible").ToString.Trim = "TRUE") Then
                            mCoordenada_X = CInt(dsDatosConfigurables.Tables("Direccion").Rows(0).Item("Coordenada_X").ToString.Trim)
                            mCoordenada_Y = CInt(dsDatosConfigurables.Tables("Direccion").Rows(0).Item("Coordenada_Y").ToString.Trim)
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Me.dsMunicipalidadData.Tables(0).Rows(0).Item("MUNICIPALIDAD_DIRECCION").ToString.Trim, mCoordenada_X, mCoordenada_Y, 0)
                        End If


                        'Telefono Municipio
                        If (dsDatosConfigurables.Tables("Telefono").Rows(0).Item("Visible").ToString.Trim = "TRUE") Then
                            mCoordenada_X = CInt(dsDatosConfigurables.Tables("Telefono").Rows(0).Item("Coordenada_X").ToString.Trim)
                            mCoordenada_Y = CInt(dsDatosConfigurables.Tables("Telefono").Rows(0).Item("Coordenada_Y").ToString.Trim)
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Me.dsMunicipalidadData.Tables(0).Rows(0).Item("MUNICIPALIDAD_TELEFONO").ToString.Trim, mCoordenada_X, mCoordenada_Y, 0)
                        End If


                        'Email Municipio
                        If (dsDatosConfigurables.Tables("Email").Rows(0).Item("Visible").ToString.Trim = "TRUE") Then
                            mCoordenada_X = CInt(dsDatosConfigurables.Tables("Email").Rows(0).Item("Coordenada_X").ToString.Trim)
                            mCoordenada_Y = CInt(dsDatosConfigurables.Tables("Email").Rows(0).Item("Coordenada_Y").ToString.Trim)
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Me.dsMunicipalidadData.Tables(0).Rows(0).Item("MUNICIPALIDAD_MAIL").ToString.Trim, mCoordenada_X, mCoordenada_Y, 0)
                        End If


                        'Web Municipio
                        If (dsDatosConfigurables.Tables("Web").Rows(0).Item("Visible").ToString.Trim = "TRUE") Then
                            mCoordenada_X = CInt(dsDatosConfigurables.Tables("Web").Rows(0).Item("Coordenada_X").ToString.Trim)
                            mCoordenada_Y = CInt(dsDatosConfigurables.Tables("Web").Rows(0).Item("Coordenada_Y").ToString.Trim)
                            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Me.dsMunicipalidadData.Tables(0).Rows(0).Item("MUNICIPALIDAD_WEB").ToString.Trim, mCoordenada_X, mCoordenada_Y, 0)
                        End If

                    End If
                End If





                'Dibujo Lineas
                If (dsDatosConfigurables.Tables("Lineas") IsNot Nothing) Then
                    If (dsDatosConfigurables.Tables("Lineas").Rows(0).Item("Visible").ToString.Trim = "TRUE") Then

                        cb.SetFontAndSize(mFuenteLineas, 8)
                        If (dsDatosConfigurables.Tables("Linea").Rows(0).Item("Visible").ToString.Trim = "TRUE") Then

                            'Dibujo las Lineas
                            cb.MoveTo(CInt(dsDatosConfigurables.Tables("Linea").Rows(0).Item("Coordenada_X_Inicio").ToString.Trim), CInt(dsDatosConfigurables.Tables("Linea").Rows(0).Item("Coordenada_Y_Inicio").ToString.Trim))
                            cb.LineTo(CInt(dsDatosConfigurables.Tables("Linea").Rows(0).Item("Coordenada_X_Fin").ToString.Trim), CInt(dsDatosConfigurables.Tables("Linea").Rows(0).Item("Coordenada_Y_Fin").ToString.Trim))

                        End If
                    End If
                    cb.Stroke()
                End If



            End If

        Catch ex As Exception
        End Try


        cb.EndText()

    End Sub

 

#End Region

     


    


End Class
