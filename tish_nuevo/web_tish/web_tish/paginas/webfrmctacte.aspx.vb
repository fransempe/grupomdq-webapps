﻿Partial Public Class webfrmctacte
    Inherits System.Web.UI.Page

    Private mWS As ws_tish.Service1
    Private dtDataComercio_AUX As DataTable


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mXML As String

        If Not (IsPostBack) Then
            'I Update the dataset
            dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))

            Call Me.SloganMunicipalidad()
            Call Me.ValidarSession()
            Call Me.CargarPie()
            Me.lblfecha_intereses.Text = Me.ObtenerFechaIntereses()
            Session("LIST_CTACTE") = Nothing
        Else


            mXML = Request.Params.Get("__EVENTTARGET").Trim
            Call Me.Pay(mXML.Trim, CBool(Request.Params.Get("__EVENTARGUMENT").Trim = "expired"))
        End If
    End Sub



    Private Sub SloganMunicipalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema TISH."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema TISH."
        End If

    End Sub


    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("contribuyente_nombre") IsNot Nothing) Then
                If (Session("contribuyente_nombre").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        If (mLogin_OK) Then

            'I Update the dataset
            dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))
            lblcomercio_user.Text = dtDataComercio_AUX.Rows(0).Item("COMERCIO_NUMERO").ToString.Trim()
        Else
            Response.Redirect("webfrmlogin.aspx", False)
        End If

    End Function

    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If

        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub



    Private Function ObtenerFechaIntereses() As String
        Dim mFecha As String

        Try

            mFecha = ""
            mWS = New ws_tish.Service1
            mFecha = mWS.ObtenerFechaActuaWeb()
            mWS = Nothing

        Catch ex As Exception
            mFecha = ""
        End Try

        Return mFecha.Trim
    End Function



    Private Sub Pay(ByVal pXML As String, ByVal pExpiredVoucher As Boolean)

        Session("xml_vouchers_expired") = ""
        Session("xml_vouchers_no_expired") = ""

        If (pExpiredVoucher) Then
            Session("xml_vouchers_expired") = pXML.Trim
        Else
            Session("xml_vouchers_no_expired") = pXML.Trim
        End If

        Response.Redirect("comprobantes/webfrmcomprobantepago.aspx", False)
    End Sub


    'ObtainNumberRecursoTISH:
    'This function returns the number of the recurso TISH
    Private Function ObtainNumberRecursoTISH() As String
        Dim mNumberRecurso As String

        Try
            mNumberRecurso = ""
            mWS = New ws_tish.Service1
            mNumberRecurso = mWS.ObtainNumberRecursoTISH()
            mWS = Nothing

        Catch ex As Exception
            mNumberRecurso = ""
        End Try

        Return mNumberRecurso.ToString.Trim
    End Function


End Class