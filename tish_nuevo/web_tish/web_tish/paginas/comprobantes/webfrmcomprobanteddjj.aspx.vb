﻿Option Explicit On
Option Strict On


'Web Services
Imports web_tish.ws_tish.Service1

Partial Public Class webfrmcomprobanteddjj
    Inherits System.Web.UI.Page



#Region "Variables"

    Private Structure SLogo
        Dim LogoOrganismo As Byte()
        Dim RenglonOrganismo As String
        Dim RenglonNombreOrganismo As String
    End Structure

    Private mPDF As clsPDFComprobanteDDJJ
    Private mLogo As SLogo
    Private mWS As web_tish.ws_tish.Service1

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dsDatosConstancia As DataSet
        Dim dsDatosComprobante As DataSet
        Dim mRutaFisica As String
        Dim mNombreArchivoComprobante As String
        Dim mArchivo As String
        Dim mCodigoMunicipalidad As String
        Dim dtDataComercio_AUX As DataTable

        Dim rubros As New List(Of entidades.rubro)


        Try

            '  If Not (IsPostBack) Then
            If (Session("xml_constancia_add_ddjj") IsNot Nothing) Then


                'Obtengo los datos
                dsDatosConstancia = New DataSet
                dsDatosConstancia = CType(Session("xml_constancia_add_ddjj"), DataSet).Copy


                ' Obtengo todos los rubros para después buscar la alicuota que corresponda.
                'Hay que modificar esto!!!!!
                Try
                    Me.mWS = New ws_tish.Service1
                    Dim strXML As String = Me.mWS.getRubros()
                    Me.mWS = Nothing


                    rubros = entidades.serializacion.deserializarXML(Of List(Of entidades.rubro))(strXML.Trim)
                Catch ex As Exception
                    rubros = Nothing
                End Try




                dsDatosComprobante = New DataSet
                dsDatosComprobante.ReadXml(New IO.StringReader(Session("xml_voucher_add_ddjj").ToString.Trim))



                'I Update the dataset
                dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))



                'Creo Directorio y Archivo en Disco
                mRutaFisica = Request.ServerVariables("APPL_PHYSICAL_PATH").ToString & "comprobantespdf"
                If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString)) Then
                    My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString)
                End If
                mNombreArchivoComprobante = dtDataComercio_AUX.Rows(0).Item("COMERCIO_NUMERO").ToString.Trim & "ddjj" & Format(Now, "HHmmss")
                mNombreArchivoComprobante = "ddjj" & Format(Now, "HHmmss")
                mArchivo = mRutaFisica & "\" & mNombreArchivoComprobante & ".pdf"



                Session("type_messenger") = "pdf"
                Session("name_pdf") = mNombreArchivoComprobante.ToString.Trim



                'Creo y Seteo el Nombre del Logo dependiendo de la Conexion
                mCodigoMunicipalidad = ""
                'If (Application("tipo_conexion").ToString = "RAFAM") Then
                '    Call GenerarLogo()
                '   mCodigoMunicipalidad = ObtenerCodigoMunicipalidad()
                'End If





                'Genero el PDF
                mPDF = New clsPDFComprobanteDDJJ

                'mPDF.TipoConexion  = Application("tipo_conexion").ToString


                'Estos Datos son para agregar al LOGO si esta Conectado a RAFAM
                'If (Application("tipo_conexion").ToString = "RAFAM") Then
                '    mPDF.Organismo = mLogo.RenglonOrganismo.ToString
                '    mPDF.NombreOrganismo = mLogo.RenglonNombreOrganismo.ToString
                'End If


                'Este Dato es utilizado si estoy conectado a RAFAM, es usado para GENERAR el BARCODE39
                mPDF.CodigoMunicipalidad = mCodigoMunicipalidad.ToString


                mPDF.RutaFisica = mArchivo.ToString
                mPDF.DatosMunicipalidad = Me.ObtenerDatosMunicipalidad()

                If (Session("municipalidad_nombre") IsNot Nothing) Then
                    mPDF.NombreMunicipalidad = Session("municipalidad_nombre").ToString.Trim
                End If


                If (Session("period_voucher_add_ddjj") IsNot Nothing) Then
                    mPDF.Period_DDJJ = Session("period_voucher_add_ddjj").ToString.Trim
                End If


                If (Session("Rectificativa") IsNot Nothing) Then
                    mPDF.IsRectificativa = CBool(Session("Rectificativa").ToString.Trim)
                End If



                If (Session("is_new_ddjj") IsNot Nothing) Then
                    mPDF.IsNew_DDJJ = CBool(Session("is_new_ddjj").ToString.Trim)
                End If



                If (Session("add_and_voucher") IsNot Nothing) Then
                    mPDF.Add_AND_Voucher = CBool(Session("add_and_voucher").ToString.Trim)
                End If




                mPDF.DatosConstancia = dsDatosConstancia
                mPDF.Rubros = rubros
                mPDF.DatosComprobante = dsDatosComprobante
                mPDF.CrearPDF()

                Response.Redirect("../webfrmresultados_genericos.aspx", False)

            Else
                Call LimpiarSession()
            End If

        Catch ex As Exception
            Response.Redirect("../webfrmerrorweb.aspx", False)
                End Try
    End Sub


#Region "procedimientos y Funciones"



    Private Sub LimpiarSession()
        Session.RemoveAll()
        Session.Clear()
        Session.Abandon()
        Response.Redirect("../webfrmindex.aspx", False)
    End Sub

    Private Function ObtenerTipoCuenta(ByVal mTipoCuenta As String) As String
        Select Case mTipoCuenta
            Case "I" : Return "INMUEBLE"
            Case "C" : Return "COMERCIO"
            Case "E", "O" : Return "CEMENTERIO"
            Case "V" : Return "VEHICULOS"
            Case Else : Return ""
        End Select
    End Function

#End Region



    Private Function CargarDataset() As DataSet

        Dim dsdatos As DataSet
        Dim dtcomercio As DataTable
        Dim dtrubros As DataTable

        Dim mFila As DataRow



        'Create the columns
        dtcomercio = New DataTable("COMERCIO")
        mFila = dtcomercio.NewRow
        dtcomercio.Columns.Add("COMERCIO_NUMERO")
        dtcomercio.Columns.Add("COMERCIO_RAZONSOCIAL")
        dtcomercio.Columns.Add("COMERCIO_NOMBREFANTASIA")
        dtcomercio.Columns.Add("COMERCIO_NOMBRE")
        dtcomercio.Columns.Add("COMERCIO_UBICACION")
        dtcomercio.Columns.Add("COMERCIO_LOCALIDAD")
        dtcomercio.Columns.Add("CONTRIBUYENTE_CUIT")
        dtcomercio.Columns.Add("CONTRIBUYENTE_NOMBRE")

        mFila = dtcomercio.NewRow
        mFila("COMERCIO_NUMERO") = "1234"
        mFila("COMERCIO_RAZONSOCIAL") = "martin-godoy-nizoli agustin-oscar-carlos"
        mFila("COMERCIO_NOMBREFANTASIA") = "martin-godoy-nizoli agustin-oscar-carlos"
        mFila("COMERCIO_NOMBRE") = "martin-godoy-nizoli agustin-oscar-carlos"
        mFila("COMERCIO_UBICACION") = "Calle alvarado, Puerta 35, Piso 32, Depto. 3 A "
        mFila("COMERCIO_LOCALIDAD") = "Mar del plata (7600)"
        mFila("CONTRIBUYENTE_CUIT") = "20305067858"
        mFila("CONTRIBUYENTE_NOMBRE") = "martin-godoy-nizoli agustin-oscar-carlos"
        dtcomercio.Rows.Add(mFila)
            




        'Create the columns
        dtrubros = New DataTable("RUBROS")
        mFila = dtcomercio.NewRow
        dtrubros.Columns.Add("RUBRO_CODE")
        dtrubros.Columns.Add("RUBRO_DESCRIPTION")
        dtrubros.Columns.Add("RUBRO_EMPLOYEES")
        dtrubros.Columns.Add("RUBRO_AMOUNT")
        

        mFila = dtrubros.NewRow
        mFila("RUBRO_CODE") = "1234"
        mFila("RUBRO_DESCRIPTION") = "fabricacion de lapices, lapiceras, boligraods, se asdada"
        mFila("RUBRO_EMPLOYEES") = "1999"
        mFila("RUBRO_AMOUNT") = "10000.00"
        dtrubros.Rows.Add(mFila)

        mFila = dtrubros.NewRow
        mFila("RUBRO_CODE") = "4234234"
        mFila("RUBRO_DESCRIPTION") = "fabricacion de lapices, lapiceras, boligraods, se asdada asdlañlsdkaskñldmsakñd"
        mFila("RUBRO_EMPLOYEES") = "12"
        mFila("RUBRO_AMOUNT") = "11111.36"
        dtrubros.Rows.Add(mFila)




        dsdatos = New DataSet
        dsdatos.Tables.Add(dtcomercio)
        dsdatos.Tables.Add(dtrubros)


        Return dsdatos

    End Function


    Private Function ObtenerDatosMunicipalidad() As DataSet
        Dim dsDatos As DataSet
        Dim mXML As String

        Try

            'Obtengo los datos de la municipalidad
            mWS = New ws_tish.Service1
            mXML = mWS.ObtainMunicipalidadData()
            mWS = Nothing


            dsDatos = New DataSet
            dsDatos = ClsTools.ObtainDataXML(mXML.ToString.Trim)

            Return dsDatos
        Catch ex As Exception
            'mLog = New Clslog
            'mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            'mLog = Nothing
            Response.Redirect("../webfrmerrorweb.aspx", False)
            Return Nothing
        End Try

    End Function
End Class