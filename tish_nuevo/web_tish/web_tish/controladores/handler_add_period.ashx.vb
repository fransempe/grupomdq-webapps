﻿Option Explicit On
Option Strict On


Imports System.Web
Imports System.Web.Services

Public Class handler_add_period
    Implements System.Web.IHttpHandler


#Region "Variables"
    Private mWS As ws_tish.Service1
#End Region

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim mAction As String
        Dim mYear As Integer
        Dim mQuota As Integer
        Dim mQuotaExpirationDate1 As String
        Dim mQuotaExpirationDate2 As String
        Dim mPresentationExpirationDate As String
        Dim mHTML As String
        Dim mResponse As String


        Try

            context.Response.ContentType = "text/plain"


            'I obtain the variables
            mAction = context.Request.Form("action")


            mHTML = ""
            If (mAction.Trim = "save") Then
                mYear = CInt(context.Request.Form("year"))
                mQuota = CInt(context.Request.Form("quota"))
                mQuotaExpirationDate1 = context.Request.Form("date_quota1").Trim
                mQuotaExpirationDate2 = context.Request.Form("date_quota2").Trim
                mPresentationExpirationDate = context.Request.Form("date_presentation").Trim


                mHTML = Me.SavePeriod(mYear, mQuota, mQuotaExpirationDate1.Trim, mQuotaExpirationDate2.Trim, mPresentationExpirationDate.Trim)
            Else
                mHTML = Me.AddPeriod()
            End If



            mResponse = mHTML.Trim
        Catch ex As Exception
            mResponse = "<span id='lblmessenger' style='color: Red; font-weight: bold;'>" & _
                            "Error al intentar recuperar los datos." & _
                        "</span>"
        End Try

        context.Response.Write(mResponse.ToString.Trim)
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property



    Private Function AddPeriod() As String
        Dim mPath As String
        Dim mHTML As String
        Dim mDescriptionResourceTISH As String


        mHTML = ""
        mPath = System.Configuration.ConfigurationManager.AppSettings("path_modal_html").ToString.Trim & "\add_periods.html"
        If (My.Computer.FileSystem.FileExists(mPath.ToString.Trim)) Then
            mHTML = My.Computer.FileSystem.ReadAllText(mPath.Trim)
        Else
            mHTML = "No se encontro la pagina solicitada"
        End If


        'Obtengo y asigno el recurso utilizado para el TISH
        mDescriptionResourceTISH = ""
        mDescriptionResourceTISH = ObtainDescriptionResourceTISH()
        mHTML = mHTML.Replace("#VAR_RESOURCE", mDescriptionResourceTISH.Trim)


        Return mHTML.Trim
    End Function


    Private Function SavePeriod(ByVal pYear As Integer, ByVal pQuota As Integer, _
                                ByVal pQuotaExpirationDate1 As String, ByVal pQuotaExpirationDate2 As String, _
                                ByVal pPresentationExpirationDate As String) As String
        Dim mSaveOK As String


        mSaveOK = ""
        mSaveOK = Me.Save(pYear, pQuota, pQuotaExpirationDate1.Trim, pQuotaExpirationDate2.Trim, pPresentationExpirationDate.Trim)




        'If (mSaveOK.Trim = "OK") Then

        '    mHTML = ""
        '    mPath = System.Configuration.ConfigurationManager.AppSettings("path_modal_html").ToString.Trim & "\message_new_period.html"
        '    If (My.Computer.FileSystem.FileExists(mPath.ToString.Trim)) Then
        '        mHTML = My.Computer.FileSystem.ReadAllText(mPath.Trim)
        '    Else
        '        mHTML = "No se encontro la pagina solicitada"
        '    End If
        'Else

        '    mHTML = "Periodo existente"
        'End If


        Return mSaveOK.Trim
    End Function


    Private Function ObtainDescriptionResourceTISH() As String
        Dim mXMLData As String

        Try

            'I obtain the data of the trade
            mXMLData = ""
            Me.mWS = New ws_tish.Service1
            mXMLData = Me.mWS.ObtainDescriptionResourceTISH()

        Catch ex As Exception
            mXMLData = ""
        Finally
            Me.mWS = Nothing
        End Try

        Return mXMLData.Trim
    End Function





    Private Function Save(ByVal pYear As Integer, ByVal pQuota As Integer, _
                          ByVal pQuotaExpirationDate1 As String, ByVal pQuotaExpirationDate2 As String, _
                          ByVal pPresentationExpirationDate As String) As String

        Dim mXMLData As String


        Try

            'I obtain the data of the trade
            mXMLData = ""
            Me.mWS = New ws_tish.Service1
            mXMLData = Me.mWS.AddDateExpiry(pYear, pQuota, pQuotaExpirationDate1.Trim, pQuotaExpirationDate2.Trim, pPresentationExpirationDate.Trim)
            Me.mWS = Nothing


        Catch ex As Exception
            mXMLData = ""
        End Try

        Return mXMLData.Trim
    End Function


End Class