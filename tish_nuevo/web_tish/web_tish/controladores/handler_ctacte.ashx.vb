﻿Imports System.Web
Imports System.Web.Services

Public Class handler_ctacte
    Implements System.Web.IHttpHandler

#Region "Variables"
    Private mWS As ws_tish.Service1
#End Region


    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim mType As String
        Dim mComercioNumber As Long
        Dim mHTML As String
        Dim mResponse As String


        Try

            context.Response.ContentType = "text/plain"


            'I obtain the variables
            mType = context.Request.Form("type").Trim
            mComercioNumber = CLng(context.Request.Form("comercio"))


            mHTML = ""
            If (mType.Trim = "expired") Then
                mHTML = Me.GetCtaCteExpired(mComercioNumber, False)
            Else
                mHTML = Me.GetVouchers(mComercioNumber)
            End If



            mResponse = mHTML.Trim
        Catch ex As Exception
            mResponse = "<span id='lblmessenger' style='color: Red; font-weight: bold;'>" & _
                            "Error al intentar recuperar los datos." & _
                        "</span>"
        End Try

        context.Response.Write(mResponse.ToString.Trim)
    End Sub

    Private Function GetCtaCteExpired(ByVal pNumberComercio As Long, ByVal pListComplete As Boolean) As String
        Dim mXML_AUX As String
        Dim dsListCtaCte As DataSet
        Dim mHTML_AUX As String


        Try


            'I look for the information of the comercio
            mWS = New ws_tish.Service1
            mXML_AUX = mWS.ObtainCTACTE(pNumberComercio)
            mWS = Nothing


            'I read the XML
            dsListCtaCte = New DataSet
            dsListCtaCte = clsTools.ObtainDataXML(mXML_AUX.ToString.Trim)


            'If the dsListDDJJ is not empty I assign information
            If (clsTools.HasData(dsListCtaCte)) Then
                dsListCtaCte = Me.CreateDataSet(dsListCtaCte, pListComplete)
                mHTML_AUX = Me.CreateHTMLCtaCte(dsListCtaCte)
            End If



        Catch ex As Exception
            dsListCtaCte = Nothing
        Finally
            mWS = Nothing
        End Try


        Return mHTML_AUX.Trim
    End Function


    Private Function CreateDataSet(ByVal dsCtaCte As DataSet, ByVal pListComplete As Boolean) As DataSet
        Dim dsDatos As DataSet
        Dim dtDatos As DataTable
        Dim mFila As DataRow
        Dim i As Integer

        Dim dvListCTACTE As DataView
        Dim mPrimaryKey(2) As DataColumn
        Dim mPrimaryKey_Data(2) As Object
        Dim mIndexRow As Integer




        Try


            'Create the columns
            dtDatos = New DataTable("LIST_CTACTE")
            mFila = dtDatos.NewRow
            dtDatos.Columns.Add("DDV_NROMOV", System.Type.GetType("System.Int32"))
            dtDatos.Columns.Add("DDV_REC", System.Type.GetType("System.String"))
            dtDatos.Columns.Add("DDV_ANIO", System.Type.GetType("System.Int32"))
            dtDatos.Columns.Add("DDV_CUOTA", System.Type.GetType("System.Int32"))
            dtDatos.Columns.Add("DDV_CONCEPTO")
            dtDatos.Columns.Add("DDV_FECHAVENC")
            dtDatos.Columns.Add("DDV_CONDESPECIAL")
            dtDatos.Columns.Add("DDV_IMPORTEORIGEN")
            dtDatos.Columns.Add("DDV_IMPORTERECARGO")
            dtDatos.Columns.Add("DDV_IMPORTETOTAL")
            dtDatos.Columns.Add("ESTADO")



            'I fill the dataset whit the rec_cc
            If (dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA") IsNot Nothing) Then
                For i = 0 To dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows.Count - 1
                    mFila = dtDatos.NewRow
                    mFila("DDV_NROMOV") = CInt(dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_NROMOV").ToString.Trim)
                    mFila("DDV_REC") = dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_REC").ToString.Trim
                    mFila("DDV_ANIO") = CInt(dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_ANIO").ToString.Trim)
                    mFila("DDV_CUOTA") = dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_CUOTA").ToString.Trim
                    mFila("DDV_CONCEPTO") = dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_CONCEPTO").ToString.Trim
                    mFila("DDV_FECHAVENC") = dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_FECHAVENC").ToString.Trim
                    mFila("DDV_CONDESPECIAL") = dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_CONDESPECIAL").ToString.Trim
                    mFila("DDV_IMPORTEORIGEN") = dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_IMPORTEORIGEN").ToString.Trim
                    mFila("DDV_IMPORTERECARGO") = dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_IMPORTERECARGO").ToString.Trim
                    mFila("DDV_IMPORTETOTAL") = dsCtaCte.Tables("REGISTRO_DETALLEDEUDAVENCIDA").Rows(i).Item("DDV_IMPORTETOTAL").ToString.Trim
                    mFila("ESTADO") = "Adeudado"
                    dtDatos.Rows.Add(mFila)
                Next
            End If





            dsDatos = New DataSet
            dsDatos.Tables.Add(dtDatos)




            'I create the index
            mPrimaryKey(0) = dsDatos.Tables("LIST_CTACTE").Columns("DDV_ANIO")
            mPrimaryKey(1) = dsDatos.Tables("LIST_CTACTE").Columns("DDV_CUOTA")
            mPrimaryKey(2) = dsDatos.Tables("LIST_CTACTE").Columns("DDV_NROMOV")
            dsDatos.Tables("LIST_CTACTE").PrimaryKey = mPrimaryKey

            'I seteo the order
            dsDatos.Tables(0).DefaultView.Sort = "DDV_ANIO, DDV_CUOTA, DDV_NROMOV"




            'I add the mov of her rec_cc And updated the new mov
            If (dsCtaCte.Tables("RENGLONES_REC_CCC") IsNot Nothing) Then
                For i = 0 To dsCtaCte.Tables("RENGLONES_REC_CCC").Rows.Count - 1

                    'I seteo the index
                    mPrimaryKey_Data(0) = CInt(dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_ANIO").ToString.Trim)
                    mPrimaryKey_Data(1) = CInt(dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_CUOTA").ToString.Trim)
                    mPrimaryKey_Data(2) = CInt(dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_NROMOV").ToString.Trim)


                    'I search for the index
                    mIndexRow = dsDatos.Tables("LIST_CTACTE").DefaultView.Find(mPrimaryKey_Data)


                    If (mIndexRow = -1) Then

                        mFila = dtDatos.NewRow
                        mFila("DDV_NROMOV") = CInt(dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_NROMOV").ToString.Trim)
                        mFila("DDV_REC") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_REC").ToString.Trim
                        mFila("DDV_ANIO") = CInt(dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_ANIO").ToString.Trim)
                        mFila("DDV_CUOTA") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_CUOTA").ToString.Trim
                        mFila("DDV_CONCEPTO") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_CONCEPTO").ToString.Trim
                        mFila("DDV_FECHAVENC") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_FECHAVENC").ToString.Trim
                        mFila("DDV_CONDESPECIAL") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_CONDESPECIAL").ToString.Trim
                        mFila("DDV_IMPORTEORIGEN") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTEORIGEN").ToString.Trim
                        mFila("DDV_IMPORTERECARGO") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTERECARGO").ToString.Trim
                        mFila("DDV_IMPORTETOTAL") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTETOTAL").ToString.Trim
                        mFila("ESTADO") = "Pagado"

                        'I add the row
                        dsDatos.Tables("LIST_CTACTE").Rows.Add(mFila)
                    Else



                        'dsDatos.Tables("LIST_DDJJ").Rows(mIndexRow).Item("DDV_IMPORTEORIGEN") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTEORIGEN").ToString.Trim
                        'dsDatos.Tables("LIST_DDJJ").Rows(mIndexRow).Item("DDV_IMPORTERECARGO") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTERECARGO").ToString.Trim
                        'dsDatos.Tables("LIST_DDJJ").Rows(mIndexRow).Item("DDV_IMPORTETOTAL") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTETOTAL").ToString.Trim
                        'dsDatos.Tables("LIST_DDJJ").Rows(mIndexRow).Item("NRO_COMPROBANTE") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("NRO_COMPROBANTE").ToString.Trim

                        dsDatos.Tables("LIST_CTACTE").Rows(mIndexRow).Item("DDV_IMPORTEORIGEN") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTEORIGEN").ToString.Trim
                        dsDatos.Tables("LIST_CTACTE").Rows(mIndexRow).Item("DDV_IMPORTERECARGO") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTERECARGO").ToString.Trim
                        dsDatos.Tables("LIST_CTACTE").Rows(mIndexRow).Item("DDV_IMPORTETOTAL") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("DDV_IMPORTETOTAL").ToString.Trim
                        'dsDatos.Tables("LIST_CTACTE").Rows(mIndexRow).Item("NRO_COMPROBANTE") = dsCtaCte.Tables("RENGLONES_REC_CCC").Rows(i).Item("NRO_COMPROBANTE").ToString.Trim

                    End If
                Next
            End If


            'I seteo the order
            'dsDatos.Tables("LIST_CTACTE").DefaultView.Sort = "DDV_ANIO ASC, DDV_CUOTA ASC"
            dsDatos.Tables("LIST_CTACTE").DefaultView.Sort = "DDV_ANIO, DDV_CUOTA, DDV_NROMOV"




            If Not (pListComplete) Then
                dvListCTACTE = New DataView()
                dvListCTACTE.Table = dsDatos.Tables("LIST_CTACTE")


                dvListCTACTE.RowFilter = "DDV_ANIO >= " & (Now.Year - 2).ToString.Trim & " AND DDV_ANIO <= " & Now.Year
                dsDatos.Tables.Clear()
                dsDatos.Tables.Add(dvListCTACTE.ToTable)

                'I seteo the order
                dsDatos.Tables("LIST_CTACTE").DefaultView.Sort = "DDV_ANIO ASC, DDV_CUOTA ASC, DDV_NROMOV"
            End If


            Return dsDatos
        Catch ex As Exception
            Return Nothing
        End Try
    End Function


    Private Function CreateHTMLCtaCte(ByVal dsData As DataSet) As String
        Dim mHTML_Table_Header As String
        Dim mHTML_Table_Body As String
        Dim mHTML As String
        Dim mCountCtaCteSaldada As Integer
        Dim mImporte_AUX As String
        Dim mImporteTotalAdeudado As Double


        Try

            mHTML = ""


            'Cabecera de la tabla
            mHTML_Table_Header = "<thead>" & _
                                        "<tr>" & _
                                            "<th style='display:none;'><h6>NroMov</h6></th>" & _
                                            "<th><h6>Año</h6></th>" & _
                                            "<th><h6>Perío.</h6></th>" & _
                                            "<th><h6>Concepto</h6></th>" & _
                                            "<th><h6>Fecha Vto.</h6></th>" & _
                                            "<th><h6>Concep. Esp.</h6></th>" & _
                                            "<th><h6 align='right'>Origen</h6></th>" & _
                                            "<th><h6 align='right'>Recargo</h6></th>" & _
                                            "<th><h6 align='right'>Total</h6></th>" & _
                                            "<th align='center'><h6>Estado</h6></th>" & _
                                            "<th><h6>Sel.</h6></th>" & _
                                        "</tr>" & _
                                     "</thead>"




            'Creo los renglones de la tabla
            mHTML_Table_Body = ""
            mCountCtaCteSaldada = 0
            mImporte_AUX = "0"
            mImporteTotalAdeudado = 0
            For i = 0 To dsData.Tables(0).Rows.Count - 1

                mHTML_Table_Body = mHTML_Table_Body & "<tr>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='center' style='display:none;'>" & dsData.Tables(0).Rows(i).Item("DDV_NROMOV").ToString.Trim & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='center'>" & dsData.Tables(0).Rows(i).Item("DDV_ANIO").ToString.Trim & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='center'>" & dsData.Tables(0).Rows(i).Item("DDV_CUOTA").ToString.Trim & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='center'>" & dsData.Tables(0).Rows(i).Item("DDV_CONCEPTO").ToString.Trim & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='center'>" & dsData.Tables(0).Rows(i).Item("DDV_FECHAVENC").ToString.Trim & "</td>"

                If (dsData.Tables(0).Rows(i).Item("DDV_CONDESPECIAL").ToString.Trim = "Juicio") Then
                    mHTML_Table_Body = mHTML_Table_Body & "<td style='color:#FF0000;' align='center'>" & dsData.Tables(0).Rows(i).Item("DDV_CONDESPECIAL").ToString.Trim & "</td>"
                    mHTML_Table_Body = mHTML_Table_Body & "<td style='text-decoration:line-through;' align='center'>" & dsData.Tables(0).Rows(i).Item("DDV_IMPORTEORIGEN").ToString.Trim & "</td>"
                    mHTML_Table_Body = mHTML_Table_Body & "<td style='text-decoration:line-through;' align='center'>" & dsData.Tables(0).Rows(i).Item("DDV_IMPORTERECARGO").ToString.Trim & "</td>"
                    mHTML_Table_Body = mHTML_Table_Body & "<td style='text-decoration:line-through;' align='center'>" & dsData.Tables(0).Rows(i).Item("DDV_IMPORTETOTAL").ToString.Trim & "</td>"
                    mHTML_Table_Body = mHTML_Table_Body & "<td style='color:#FF0000;' align='center'>" & dsData.Tables(0).Rows(i).Item("ESTADO").ToString.Trim & "</td>"
                    mHTML_Table_Body = mHTML_Table_Body & "<td align='center'></td>"

                Else
                    mHTML_Table_Body = mHTML_Table_Body & "<td align='center'>" & dsData.Tables(0).Rows(i).Item("DDV_CONDESPECIAL").ToString.Trim & "</td>"
                    mHTML_Table_Body = mHTML_Table_Body & "<td align='right'>" & dsData.Tables(0).Rows(i).Item("DDV_IMPORTEORIGEN").ToString.Trim & "</td>"
                    mHTML_Table_Body = mHTML_Table_Body & "<td align='right'>" & dsData.Tables(0).Rows(i).Item("DDV_IMPORTERECARGO").ToString.Trim & "</td>"
                    mHTML_Table_Body = mHTML_Table_Body & "<td align='right'>" & dsData.Tables(0).Rows(i).Item("DDV_IMPORTETOTAL").ToString.Trim & "</td>"



                    If (dsData.Tables(0).Rows(i).Item("ESTADO").ToString.Trim <> "Pagado") Then


                        mHTML_Table_Body = mHTML_Table_Body & "<td style='color:#FF0000;' align='center'>" & dsData.Tables(0).Rows(i).Item("ESTADO").ToString.Trim & "</td>"


                        If (dsData.Tables(0).Rows(i).Item("ESTADO").ToString.Trim <> "No disponib.") Then
                            mHTML_Table_Body = mHTML_Table_Body & "<td align='center'> "
                            mHTML_Table_Body = mHTML_Table_Body & "<input id=check" & (i + 1).ToString & " name=check" & (i + 1).ToString & " type='checkbox' value='' style='cursor:pointer;' onclick=""javascript:calcular_seleccion('gridctacte');""/> "
                            mHTML_Table_Body = mHTML_Table_Body & "</td>"
                            mHTML_Table_Body = mHTML_Table_Body & "</tr>"


                            mImporte_AUX = dsData.Tables(0).Rows(i).Item("DDV_IMPORTETOTAL").ToString.Replace(",", Nothing)
                            mImporte_AUX = mImporte_AUX.Replace(".", ",")
                            mImporteTotalAdeudado = mImporteTotalAdeudado + CDbl(mImporte_AUX)
                        Else
                            mHTML_Table_Body = mHTML_Table_Body & "<td align='center'></td>"
                        End If
                    Else
                        mHTML_Table_Body = mHTML_Table_Body & "<td style='color:#006600;' align='center'>" & dsData.Tables(0).Rows(i).Item("ESTADO").ToString.Trim & "</td>"
                        mHTML_Table_Body = mHTML_Table_Body & "<td align='center'></td>"
                        mCountCtaCteSaldada = mCountCtaCteSaldada + 1
                    End If
                End If

                mHTML_Table_Body = mHTML_Table_Body & "</tr>"

            Next





            mHTML = "<table id='gridctacte' cellpadding='0' cellspacing='0' border='0' id='table_periods' class='table_periods' onmouseover=""javascript:efecto_grilla('gridctacte');"">" & _
                        mHTML_Table_Header.Trim & vbCr & _
                        "<tbody>" & mHTML_Table_Body.Trim & "</tbody>" & _
                    "</table>"



            mHTML = mHTML & " <div id='div_totales' class='titulo_3' style='padding-top: 10px; padding-bottom: 10px; padding-left: 30px; width: 90%;' align='center'> " & _
                            "   Detalle de totales: " & _
                            "   <div style=width: 90%;> " & _
                            " 	    <hr class='linea'> " & _
                            "   </div> " & _
                            " </div> " & _
                            " <div style='width:90%; padding-left: 30px;' align='center'> " & _
                            "   <div style='width:35%; float:left;'> " & _
                            " 	    <table border = '1' style='width:100%;' rules='rows'> " & _
                            " 		    <tr> " & _
                            " 			    <td>Cantidad de movimientos:</td> " & _
                            " 			    <td id='td_cantidad_ctacte' align='right' style='padding-right:5px;'>" & dsData.Tables(0).Rows.Count.ToString & "</td> " & _
                            " 		    </tr> " & _
                            " 		    <tr> " & _
                            " 			    <td>Registros no seleccionados:</td> " & _
                            " 			    <td id='td_registros_no_seleccionados' align='right' style='padding-right:5px;'>" & dsData.Tables(0).Rows.Count.ToString & "</td> " & _
                            " 		    </tr> " & _
                            " 		    <tr> " & _
                            " 			    <td>Registros seleccionados:</td> " & _
                            " 			    <td id='td_registros_selecionados' align='right' style='padding-right:5px;'>0</td> " & _
                            " 		    </tr> " & _
                            "   	</table> " & _
                            "   </div> " & _
                            "   <div style='width:55%; float:right;'> " & _
                            " 	    <table border = '1' style='width:100%;' rules='rows'> " & _
                            " 		    <tr> " & _
                            " 			    <td>Movimientos saldados:</td> " & _
                            " 			    <td id='td_cantidad_ctacte_saldadas' align='right' style='padding-right:5px;'>" & mCountCtaCteSaldada.ToString.Trim & "</td> " & _
                            " 		    </tr> " & _
                            " 		    <tr> " & _
                            " 			    <td>Total importe adeudado:</td> " & _
                            " 			    <td id='td_importe_total_deuda' align='right' style='color:#FF0000; font-weight:bold; padding-right:5px;'>$ " & Format(mImporteTotalAdeudado, "N") & "</td> " & _
                            " 		    </tr> " & _
                            " 		    <tr> " & _
                            " 			    <td>Total importe seleccionados:</td> " & _
                            " 			    <td id='td_importe_total_seleccionado' align='right' style='color:#009933; font-weight:bold; padding-right:5px;' >0.00</td> " & _
                            " 		    </tr> " & _
                            " 	    </table> " & _
                            "   </div> " & _
                            " </div> "







            mHTML = mHTML & " <div id='div_contenedor_botones' style='width:90%; float:left; padding-top:10px; padding-left: 30px;' align='center'> " & _
                                " <div id='div_boton_list_ctacte' style='width:49%; float:left'; align='center'> " & _
                                    " <input type='button' value='Imprimir informe de cta. cte.' class='boton' style='width:200px; height:35px; cursor:pointer' onclick=""javascript:list_cta_cte();""/> " & _
                                " </div> " & _
                                " <div id='div_container_button_pay' align='center' style='width:49%; float:right;' align='center'> " & _
                                    " <input type='button' value='Pagar registros seleccionados' class='boton' style='width:200px; height:35px; cursor:pointer' onclick=""javascript:pay_voucher_expired();""/> " & _
                                " </div> " & _
                            " </div> "


        Catch ex As Exception
            mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                        "<p id='lblmessenger' style='color: Red; font-weight: inherit;'> " & _
                            "Error al intentar obtener los datos." & _
                        "</p>" & _
                    "</div>"
        End Try


        Return mHTML.ToString.Trim
    End Function





    Private Function GetVouchers(ByVal pNumberComercio As Long) As String
        Dim mXML_AUX As String
        Dim dsListCtaCte As DataSet
        Dim mHTML_AUX As String


        Try


            'I look for the information of the comercio
            mWS = New ws_tish.Service1
            mXML_AUX = mWS.ObtainCTACTE(pNumberComercio)
            mWS = Nothing


            'I read the XML
            dsListCtaCte = New DataSet
            dsListCtaCte = clsTools.ObtainDataXML(mXML_AUX.ToString.Trim)

            'If the dsListDDJJ is not empty I assign information
            If (clsTools.HasData(dsListCtaCte)) Then
                If (dsListCtaCte.Tables("REGISTRO_DETALLEPROXCOMPAVENC") IsNot Nothing) Then                    
                    mHTML_AUX = Me.CreateHTMLVouchers(dsListCtaCte)
                Else
                    mHTML_AUX = "<div id='div_messenger' class='Globo GlbRed' style='width:100%; padding-top:30px;'>" & _
                                    "<p id='lblmessenger' style='font-weight: inherit;' align='center'>" & _
                                        "No existen comprobantes a vencer para esta cuenta." & _
                                    "</p>" & _
                                "</div>"
                End If
            End If



        Catch ex As Exception
            dsListCtaCte = Nothing
        Finally
            mWS = Nothing
        End Try


        Return mHTML_AUX.Trim
    End Function


    Private Function CreateHTMLVouchers(ByVal dsData As DataSet) As String
        Dim mHTML_Table_Header As String
        Dim mHTML_Table_Body As String
        Dim mHTML As String
        Dim mCountCtaCteSaldada As Integer
        Dim mImporte_AUX As String
        Dim mImporteTotalAdeudado As Double

        Try

            mHTML = ""

            'Cabecera de la tabla
            mHTML_Table_Header = "<thead>" & _
                                        "<tr>" & _
                                            "<th><h6>Comprobante</h6></th>" & _
                                            "<th><h6>Detalle</h6></th>" & _
                                            "<th><h6>Vto</h6></th>" & _
                                            "<th><h6>Fecha Vto.</h6></th>" & _
                                            "<th><h6 align='right'>Origen</h6></th>" & _
                                            "<th><h6 align='right'>Recargo</h6></th>" & _
                                            "<th><h6 align='right'>Total</h6></th>" & _
                                            "<th><h6>Sel.</h6></th>" & _
                                        "</tr>" & _
                                     "</thead>"



            'Creo los renglones de la tabla
            mHTML_Table_Body = ""
            mCountCtaCteSaldada = 0
            For i = 0 To dsData.Tables("REGISTRO_DETALLEPROXCOMPAVENC").Rows.Count - 1
                mHTML_Table_Body = mHTML_Table_Body & "<tr>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='center'>" & dsData.Tables("REGISTRO_DETALLEPROXCOMPAVENC").Rows(i).Item("DPC_COMPROBANTE").ToString.Trim & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='left'>" & dsData.Tables("REGISTRO_DETALLEPROXCOMPAVENC").Rows(i).Item("DPC_DETALLE").ToString.Trim & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='center'>" & dsData.Tables("REGISTRO_DETALLEPROXCOMPAVENC").Rows(i).Item("DPC_VENC").ToString.Trim & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='center'>" & dsData.Tables("REGISTRO_DETALLEPROXCOMPAVENC").Rows(i).Item("DPC_FECHAVENC").ToString.Trim & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='right'>" & dsData.Tables("REGISTRO_DETALLEPROXCOMPAVENC").Rows(i).Item("DPC_IMPORTEORIGEN").ToString.Trim & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='right'>" & dsData.Tables("REGISTRO_DETALLEPROXCOMPAVENC").Rows(i).Item("DPC_IMPORTERECARGO").ToString.Trim & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='right'>" & dsData.Tables("REGISTRO_DETALLEPROXCOMPAVENC").Rows(i).Item("DPC_IMPORTETOTAL").ToString.Trim & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='center'> "
                mHTML_Table_Body = mHTML_Table_Body & "<input id=check" & (i + 1).ToString & " name=check" & (i + 1).ToString & " type='checkbox' value='' style='cursor:pointer;' onclick=""javascript:calcular_seleccion('gridvouchers');""/> "
                mHTML_Table_Body = mHTML_Table_Body & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "</tr>"

                mImporte_AUX = dsData.Tables("REGISTRO_DETALLEPROXCOMPAVENC").Rows(i).Item("DPC_IMPORTETOTAL").ToString.Trim.Replace(",", Nothing)
                mImporte_AUX = mImporte_AUX.ToString.Trim.Replace(".", ",")
                mImporteTotalAdeudado = mImporteTotalAdeudado + CDbl(mImporte_AUX)
            Next




            ' Si el contri. cuenta con credito me muestro un cartel y lo dejo fijo en la pantalla para que lo vea siempre
            If (dsData.Tables("CREDITOCTACTE") IsNot Nothing) Then
                If (dsData.Tables("CREDITOCTACTE").Rows(0).Item("IMPORTE").ToString.Trim <> "0.00") Then
                    mHTML = "<div id='div_messenger_credito' class='Globo GlbRed' style='width:100%; padding-top:0px; padding-bottom:0px;'> " & _
                            "   <p id='lblmessenger_credito' style='font-weight: inherit;' align='center'> " & _
                            "	    <strong>Importante: </strong>Usted cuenta con un valor de <strong id= 'strong_importe'> $ " & dsData.Tables("CREDITOCTACTE").Rows(0).Item("IMPORTE").ToString.Trim.Replace("-", Nothing) & " </strong> en crédito. <a href='#' onclick=""javascript:message_credito();"">Ver más ..</a>" & _
                            "   </p> " & _
                            "</div>"
                End If
            End If




            mHTML = mHTML & "<table id='gridvouchers' cellpadding='0' cellspacing='0' border='0' id='table_periods' class='table_periods' onmouseover=""javascript:efecto_grilla('gridvouchers');"">" & _
                                mHTML_Table_Header.Trim & vbCr & _
                                "<tbody>" & mHTML_Table_Body.Trim & "</tbody>" & _
                            "</table>"



            mHTML = mHTML & " <div id='div_totales' class='titulo_3' style='padding-top: 10px; padding-bottom: 10px; padding-left: 30px; width: 90%' align='center'> " & _
                            "   Detalle de totales: " & _
                            "   <div style=width: 90%;> " & _
                            " 	    <hr class='linea'> " & _
                            "   </div> " & _
                            " </div> " & _
                            "   <div style='width:90%; padding-left: 30px;' align='center'> " & _
                            "       <div style='width:35%; float:left;'> " & _
                            " 	        <table border = '1' style='width:90%;' rules='rows'> " & _
                            " 		        <tr> " & _
                            " 			        <td>Cantidad de movimientos:</td> " & _
                            " 			        <td id='td_cantidad_ctacte' align='right' style='padding-right:5px;'>" & dsData.Tables(0).Rows.Count.ToString & "</td> " & _
                            " 		        </tr> " & _
                            " 		        <tr> " & _
                            " 			    <td>Registros no seleccionados:</td> " & _
                            " 			    <td id='td_registros_no_seleccionados' align='right' style='padding-right:5px;'>" & dsData.Tables(0).Rows.Count.ToString & "</td> " & _
                            " 		    </tr> " & _
                            " 		    <tr> " & _
                            " 			    <td>Registros seleccionados:</td> " & _
                            " 			    <td id='td_registros_selecionados' align='right' style='padding-right:5px;'>0</td> " & _
                            " 		    </tr> " & _
                            " 	    </table> " & _
                            "   </div> " & _
                            "   <div style='width:55%; float:right;'> " & _
                            " 	    <table border = '1' style='width:90%;' rules='rows'> " & _
                            " 		    <tr> " & _
                            " 			    <td>Movimientos saldados:</td> " & _
                            " 			    <td id='td_cantidad_ctacte_saldadas' align='right' style='padding-right:5px;'>" & mCountCtaCteSaldada.ToString.Trim & "</td> " & _
                            " 		    </tr> " & _
                            " 		    <tr> " & _
                            " 			    <td>Total importe adeudado:</td> " & _
                            " 			    <td id='td_importe_total_adeudado' align='right' style='color:#FF0000; font-weight:bold; padding-right:5px;'>$ " & Format(mImporteTotalAdeudado, "N") & "</td> " & _
                            " 		    </tr> " & _
                            " 		    <tr> " & _
                            " 			    <td>Total importe seleccionados:</td> " & _
                            " 			    <td id='td_importe_total_seleccionado' align='right' style='color:#009933; font-weight:bold; padding-right:5px;'>0.00</td> " & _
                            " 		    </tr> " & _
                            " 	    </table> " & _
                            "   </div> " & _
                            " </div> "







            mHTML = mHTML & " <div id='div_contenedor_botones' style='width:90%; float:left; padding-top:10px; padding-left: 30px;' align='center'> " & _
                                " <div id='div_container_button_pay' align='center' style='width:49%; align='center'> " & _
                                    " <input type='button' value='Pagar registros seleccionados' class='boton' style='width:200px; height:35px; cursor:pointer' onclick=""javascript:pay_voucher();""/> " & _
                                " </div> " & _
                            " </div> "


        Catch ex As Exception
            mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                        "<p id='lblmessenger' style='color: Red; font-weight: inherit;'> " & _
                            "Error al intentar obtener los datos." & _
                        "</p>" & _
                    "</div>"
        End Try


        Return mHTML.ToString.Trim
    End Function


    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class