﻿Imports System.Web
Imports System.Web.Services

Public Class handler_ddjj
    Implements System.Web.IHttpHandler

#Region "Variables"
    Private mWS As ws_tish.Service1
#End Region

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim strAccion As String
        Dim lngNroComercio As Long
        Dim strPeriodo As String
        Dim strHTML As String
        Dim strResponse As String


        Try
            context.Response.ContentType = "text/plain"
            strAccion = context.Request.Form("action").Trim
            lngNroComercio = CLng(context.Request.Form("comercio"))
            If (strAccion.Trim() = "exist") Then
                strPeriodo = context.Request.Form("period").Trim
            End If


            strHTML = ""
            Select Case strAccion.Trim()
                Case "get"
                    strHTML = Me.getListadoDDJJ(lngNroComercio)

                Case "exist"
                    strHTML = Me.Exist_DDJJ(lngNroComercio, strPeriodo.Trim)
            End Select



            strResponse = strHTML.Trim()
        Catch ex As Exception
            strResponse = "<span id='lblmessenger' style='color: Red; font-weight: bold;'>" & _
                                "Error al intentar recuperar los datos." & _
                          "</span>"
        End Try

        context.Response.Write(strResponse.Trim)
    End Sub


    Private Function getListadoDDJJ(ByVal p_lngNumberComercio As Long) As String
        Dim com_ddjj As List(Of entidades.com_ddjj)
        Dim strXML As String = ""
        Dim strHTML As String = ""


        Try
            ' Obtengo un listado de las declaraciones juradas de un comercio
            mWS = New ws_tish.Service1
            strXML = mWS.getListadoDDJJByComercio(p_lngNumberComercio)
            mWS = Nothing


            com_ddjj = New List(Of entidades.com_ddjj)()
            com_ddjj = entidades.serializacion.deserializarXML(Of List(Of entidades.com_ddjj))(strXML.Trim())


            If (com_ddjj.Count <> 0) Then
                strHTML = Me.crearHTML(com_ddjj)
            Else
                strHTML = Me.createHtmlSoloBoton()
            End If

        Catch ex As Exception
        Finally
            com_ddjj = Nothing
            mWS = Nothing
        End Try


        Return strHTML.Trim()
    End Function


    Private Function crearHTML(ByVal com_ddjj As List(Of entidades.com_ddjj)) As String
        Dim mHTML_Table_Header As String = ""
        Dim mHTML_Table_Body As String = ""
        Dim mHTML As String = ""

        Try

            'Cabecera de la tabla
            mHTML_Table_Header = ""
            mHTML_Table_Header = mHTML_Table_Header & "<thead>"
            mHTML_Table_Header = mHTML_Table_Header & "     <tr>"
            mHTML_Table_Header = mHTML_Table_Header & "         <th><h6 align='center'>Año</h6></th>"
            mHTML_Table_Header = mHTML_Table_Header & "         <th><h6 align='center'>Período</h6></th>"
            mHTML_Table_Header = mHTML_Table_Header & "         <th><h6 align='right'>Monto</h6></th>"
            mHTML_Table_Header = mHTML_Table_Header & "         <th><h6 align='right'>Rectif.</h6></th>"
            mHTML_Table_Header = mHTML_Table_Header & "     </tr>"
            mHTML_Table_Header = mHTML_Table_Header & "</thead>"


            'Creo los renglones de la tabla            
            For Each com_ddjj_aux As entidades.com_ddjj In com_ddjj                
                mHTML_Table_Body = mHTML_Table_Body & "<tr>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='center'>" & com_ddjj_aux.anio & "</td>"               


                mHTML_Table_Body = mHTML_Table_Body & "<td align='right'>" & "Hola" & " </td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='right'>" & Format(com_ddjj_aux.montoDeclarado, "N2") & " </td>"

                mHTML_Table_Body = mHTML_Table_Body & "<td align='center'> "
                mHTML_Table_Body = mHTML_Table_Body & "<img alt='' src='../imagenes/editar.png' style='cursor:pointer;' title='Rectificativa' onclick=""javascript:rectification('" & com_ddjj_aux.anio & " - " & com_ddjj_aux.cuota & "');""/> "
                mHTML_Table_Body = mHTML_Table_Body & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "</tr>"
            Next



            mHTML = "<table id='gridddjj' cellpadding='0' cellspacing='0' border='0' id='table_periods' class='table_periods' onmouseover=""javascript:efecto_grilla('gridddjj');"">" & _
                        mHTML_Table_Header.Trim & vbCr & _
                        "<tbody>" & mHTML_Table_Body.Trim & "</tbody>" & _
                    "</table>"




            mHTML = mHTML & " <div id='div_contenedor_botones' style='width:90%; float:left; padding-top:10px; padding-left: 30px;' align='center'> " & _
                                " <div id='div_boton_list_ddjj' style='width:49%; float:left'; align='center'> " & _
                                    " <input type='button' value='Imprimir informe de DDJJ' class='boton' style='width:200px; height:35px; cursor:pointer' onclick=""javascript:list_ddjj();""/> " & _
                                " </div> " & _
                                " <div id='div_button_declaration' align='center' style='width:49%; float:right;' align='center'> " & _
                                    " <input type='button' value='Presentar nueva DDJJ' class='boton' style='width:200px; height:35px; cursor:pointer' onclick=""javascript:declaration_new();""/> " & _
                                " </div> " & _
                            " </div> "


        Catch ex As Exception
            mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                        "<p id='lblmessenger' style='color: Red; font-weight: inherit;'> " & _
                            "Error al intentar obtener los datos." & _
                        "</p>" & _
                    "</div>"
        End Try


        Return mHTML.Trim()
    End Function



    Private Function createHtmlSoloBoton() As String
        Return " <div id='div_contenedor_botones' style='width:90%; padding-top:10px;' align='center'> " & _
                    "<p><strong class='titulo_3'>No existen declaraciones juradas</strong></p>" & _
               " </div> " & _
               " <div id='div_contenedor_botones' style='width:90%; padding-top:10px;' align='center'> " & _
                    " <div id='div_button_declaration' align='center' style='width:80%;' align='center'> " & _
                        " <input type='button' value='Presentar nueva DDJJ' class='boton' style='width:200px; height:35px; cursor:pointer' onclick=""javascript:declaration_new();""/> " & _
                    " </div> " & _
               " </div> "

    End Function





    Private Function Exist_DDJJ(ByVal pComercioNumber As Long, ByVal pPeriod As String) As String
        Dim mExist As Boolean
        Dim mExist_AUX As String
        Dim mPeriod() As String

        Try
            mExist = False
            mPeriod = pPeriod.ToString.Split(CChar("-"))
            Me.mWS = New ws_tish.Service1
            mExist = Me.mWS.Exist_DDJJ(CInt(pComercioNumber), CInt(mPeriod(0).ToString.Trim), CInt(mPeriod(1).ToString.Trim))
            Me.mWS = Nothing

        Catch ex As Exception
            mExist = False
        End Try


        mExist_AUX = "NO"
        If (mExist) Then
            mExist_AUX = "SI"
        End If

        Return mExist_AUX.Trim
    End Function


    Private Function EmployeesUsing() As Boolean
        Dim mUsing As String

        Try
            mUsing = "N"

            'Me.mWS = New ws_tish.Service1
            'mUsing = Me.mWS.Exist_DDJJ(CInt(pComercioNumber), CInt(mPeriod(0).ToString.Trim), CInt(mPeriod(1).ToString.Trim))
            'Me.mWS = Nothing

        Catch ex As Exception
            mUsing = "N"
        Finally
            Me.mWS = Nothing
        End Try

        Return CBool(mUsing.Trim = "S")
    End Function

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class