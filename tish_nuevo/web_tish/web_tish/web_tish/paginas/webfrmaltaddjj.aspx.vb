﻿Partial Public Class webfrmaltaddjj
    Inherits System.Web.UI.Page


#Region "Variables"
    Private mWS As ws_tish.Service1
    Private dtDataComercio_AUX As DataTable
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dsConstanciaData As DataSet
        Dim mPeriod As String

        If Not (IsPostBack) Then

            Call Me.SloganMunicipalidad()
            Call Me.ValidarSession()
            Call Me.CargarPie()


            'I fill combo whit the periods
            Call Me.LoadComboPeriods()


            Call Me.loadRubrosByComercio()

        Else





            If (Request.Params.Get("__EVENTTARGET").Trim <> "") And (Request.Params.Get("__EVENTARGUMENT").Trim <> "") Then

                dsConstanciaData = New DataSet
                dsConstanciaData = clsTools.ObtainDataXML(Request.Params.Get("__EVENTARGUMENT").Trim)
                If (dsConstanciaData IsNot Nothing) Then

                    mPeriod = dsConstanciaData.Tables("PERIODOS").Rows(0).Item(0).ToString.Trim
                    If (Me.AddDDJJ(Request.Params.Get("__EVENTTARGET").Trim, mPeriod.Trim)) Then
                        Call Me.GenerarDataSetPDF(dsConstanciaData)


                        Session("add_and_voucher") = dsConstanciaData.Tables("PDF_TYPE").Rows(0).Item(0).ToString.Trim
                        Response.Redirect("comprobantes/webfrmcomprobanteddjj.aspx", False)
                    End If
                End If
            End If
        End If

    End Sub




    Private Sub SloganMunicipalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema TISH."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema TISH."
        End If

    End Sub


    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("contribuyente_nombre") IsNot Nothing) Then
                If (Session("contribuyente_nombre").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        If (mLogin_OK) Then

            'I Update the dataset
            dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))
            Me.lblcomercio_user.Text = dtDataComercio_AUX.Rows(0).Item("COMERCIO_NUMERO").ToString.Trim()
        Else
            Response.Redirect("webfrmlogin.aspx", False)
        End If

    End Function

    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            Me.lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            Me.lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If


        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub


    'LoadComboPeriods
    'This function loads the combo whit the periods
    Private Sub LoadComboPeriods()
        Dim dsDataPeriods As DataSet
        Dim mXML_AUX As String



        Try

            'I obtain the periods
            Me.mWS = New ws_tish.Service1
            mXML_AUX = Me.mWS.ObtainDateOfExpiry()
            Me.mWS = Nothing



            'I read the XML
            dsDataPeriods = New DataSet
            dsDataPeriods = clsTools.ObtainDataXML(mXML_AUX.ToString.Trim)
            Session("ROWS_PERIODS") = dsDataPeriods.Tables("ROWS_PERIODS").Copy

            'If the dsListadoDDJJ is not empty I assign information
            If (clsTools.HasData(dsDataPeriods)) Then
                Me.div_period.InnerHtml = Me.CreateHTMLPeriod(dsDataPeriods.Tables("ROWS_PERIODS"))
            End If


        Catch ex As Exception
        Finally
            mWS = Nothing
        End Try

    End Sub

    Private Function CreateHTMLPeriod(ByVal dtData As DataTable) As String
        Dim mHTML As String
        Dim mHTML_ComboItems As String
        Dim mValue As String


        Try

            mHTML_ComboItems = ""
            mValue = ""
            If Not (CBool(Session("Rectificativa"))) Then
                If (dtData IsNot Nothing) Then
                    For i = 0 To dtData.Rows.Count - 1

                        mValue = dtData.Rows(i).Item("FV_CUOTA1").ToString.Trim & "#" & dtData.Rows(i).Item("FV_CUOTA2").ToString.Trim & "#" & dtData.Rows(i).Item("FV_PRESENTACION").ToString.Trim
                        If (i = 0) Then
                            mHTML_ComboItems = mHTML_ComboItems & "<option value='" & mValue.Trim & "' selected='selected'>" & dtData.Rows(i).Item("ANIO").ToString.Trim & " - " & dtData.Rows(i).Item("CUOTA").ToString.Trim & "</option>"
                        Else
                            mHTML_ComboItems = mHTML_ComboItems & "<option value='" & mValue.Trim & "'>" & dtData.Rows(i).Item("ANIO").ToString.Trim & " - " & dtData.Rows(i).Item("CUOTA").ToString.Trim & "</option>"
                        End If
                    Next
                End If

            Else
                mHTML_ComboItems = mHTML_ComboItems & "<option>" & Session("Rectificativa_Period").ToString.Trim & "</option>"
            End If



            mHTML = ""
            mHTML = mHTML & "<table id='table_period' align='center'  border='0' style='width:90%; border:1px solid #C0C0C0;'>"
            mHTML = mHTML & "   <tr>"
            If Not (CBool(Session("Rectificativa"))) Then
                mHTML = mHTML & " <td colspan='4' style='color:Black; font-weight:bold;' align='center'>Original :: Seleccione un período a declarar</td>"
            Else
                mHTML = mHTML & " <td colspan='4' style='color:Black; font-weight:bold;' align='center'>Rectificativa :: Los datos del periodo son puramente informativos.</td>"
            End If
            mHTML = mHTML & "   </tr>"
            mHTML = mHTML & "   <tr>"
            mHTML = mHTML & "       <td id='td_date_presentation' colspan='4' style='color:Black; font-weight:bold;' align='center'>Fecha presentación: " & Format(Now, "dd/MM/yyyy") & "</td>"
            mHTML = mHTML & "   </tr>"
            mHTML = mHTML & "   <tr><td colspan='4' class='linea'><hr /></td></tr>"
            mHTML = mHTML & "   <tr>"
            mHTML = mHTML & "       <td style='width:10%'>Período:</td>"
            mHTML = mHTML & "       <td style='width:60%' align='left'>"
            mHTML = mHTML & "           <select id = 'cmbperiod' name='cmbperiod' style='width:40%;' onchange=""javascript:refresh_date();"">" & mHTML_ComboItems.Trim & "</select>"
            mHTML = mHTML & "       </td>"            
            mHTML = mHTML & "       <td style='width:45%' align='right'>Fecha Vto. presentación:</td>"
            mHTML = mHTML & "       <td id='td_date_expiry_presentation' style='width:10%'>" & dtData.Rows(0).Item("FV_PRESENTACION").ToString.Trim & "</td>"
            mHTML = mHTML & "   </tr>"
            mHTML = mHTML & "   <tr><td colspan='4' class='linea'><hr /></td></tr>"
            mHTML = mHTML & "   <tr><td colspan='4' class='linea'><hr /></td></tr>"
            mHTML = mHTML & "</table>"








            'mHTML = ""
            'mHTML = mHTML & "<table id='table_period' align='center'  border='0' style='width:90%; border:1px solid #C0C0C0;'>"
            'mHTML = mHTML & "   <tr>"
            'If Not (CBool(Session("Rectificativa"))) Then
            '    mHTML = mHTML & " <td colspan='4' style='color:Black; font-weight:bold;' align='center'>Original :: Seleccione un período a declarar</td>"
            'Else
            '    mHTML = mHTML & " <td colspan='4' style='color:Black; font-weight:bold;' align='center'>Rectificativa :: Los datos del periodo son puramente informativos.</td>"
            'End If
            'mHTML = mHTML & "   </tr>"
            'mHTML = mHTML & "   <tr>"
            'mHTML = mHTML & "       <td id='td_date_presentation' colspan='4' style='color:Black; font-weight:bold;' align='center'>Fecha presentación: " & Format(Now, "dd/MM/yyyy") & "</td>"
            'mHTML = mHTML & "   </tr>"
            'mHTML = mHTML & "   <tr><td colspan='4' class='linea'><hr /></td></tr>"
            'mHTML = mHTML & "   <tr>"
            'mHTML = mHTML & "       <td style='width:20%'>Período:</td>"
            'mHTML = mHTML & "       <td style='width:20%' align='left'>"
            'mHTML = mHTML & "           <select id = 'cmbperiod' name='cmbperiod' style='width:60%;' onchange=""javascript:refresh_date();"">" & mHTML_ComboItems.Trim & "</select>"
            'mHTML = mHTML & "       </td>"
            'mHTML = mHTML & "       <td style='width:15%'>Fecha 1º Vto.:</td>"
            'mHTML = mHTML & "       <td id='td_date_expiry_quota1' style='width:15%'>" & dtData.Rows(0).Item("FV_CUOTA1").ToString.Trim & "</td>"
            'mHTML = mHTML & "   </tr>"
            'mHTML = mHTML & "   <tr>"
            'mHTML = mHTML & "       <td style='width:15%'>Fecha Vto. present.:</td>"
            'mHTML = mHTML & "       <td id='td_date_expiry_presentation' style='width:15%'>" & dtData.Rows(0).Item("FV_PRESENTACION").ToString.Trim & "</td>"
            'mHTML = mHTML & "       <td style='width:15%'>Fecha 2º Vto.:</td>"
            'mHTML = mHTML & "       <td id='td_date_expiry_quota2' style='width:15%'>" & dtData.Rows(0).Item("FV_CUOTA2").ToString.Trim & "</td>"
            'mHTML = mHTML & "   </tr>"
            'mHTML = mHTML & "   <tr><td colspan='4' class='linea'><hr /></td></tr>"
            'mHTML = mHTML & "   <tr><td colspan='4' class='linea'><hr /></td></tr>"
            'mHTML = mHTML & "</table>"



        Catch ex As Exception
            mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                        "<p id='lblmessenger' style='color: Red; font-weight: inherit;'> " & _
                            "Error al intentar obtener los datos." & _
                        "</p>" & _
                    "</div>"
        End Try

        Return mHTML.Trim
    End Function



    Private Sub loadRubrosByComercio()
        Dim rubro As New List(Of entidades.rubro)
        Dim strXML As String = ""
        Dim strRubros As String = ""

        Try
            Me.mWS = New ws_tish.Service1
            strXML = Me.mWS.getRubrosByComercio(Me.lblcomercio_user.Text.Trim())
            Me.mWS = Nothing

            rubro = entidades.serializacion.deserializarXML(Of List(Of entidades.rubro))(strXML.Trim)
            If (rubro IsNot Nothing) Then
                For i = 0 To rubro.Count - 1
                    Select Case i
                        Case 0
                            Me.td_codigo_rubro1.InnerHtml = rubro(i).codigo
                            Me.td_descripcion_rubro1.InnerHtml = rubro(i).descripcion

                        Case 1
                            Me.td_codigo_rubro2.InnerHtml = rubro(i).codigo
                            Me.td_descripcion_rubro2.InnerHtml = rubro(i).descripcion

                        Case 2
                            Me.td_codigo_rubro3.InnerHtml = rubro(i).codigo
                            Me.td_descripcion_rubro3.InnerHtml = rubro(i).descripcion

                        Case 3
                            Me.td_codigo_rubro4.InnerHtml = rubro(i).codigo
                            Me.td_descripcion_rubro4.InnerHtml = rubro(i).descripcion

                        Case 4
                            Me.td_codigo_rubro5.InnerHtml = rubro(i).codigo
                            Me.td_descripcion_rubro5.InnerHtml = rubro(i).descripcion
                    End Select
                Next
            End If

        Catch ex As Exception
            strRubros = ""
        End Try

    End Sub


    Private Function AddDDJJ(ByVal pXML As String, ByVal pPeriod As String) As Boolean
        Dim mResult_XML As String
        Dim mOK_ADD As Boolean
        Dim mNumberRecurso As String
        Dim mPeriod() As String


        Try
            mOK_ADD = False
            mResult_XML = ""


            If (pXML.ToString.Trim <> "") Then

                'I Update the dataset with the information of the comercio
                dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))


                'I obtain the number of the recurso
                mNumberRecurso = Me.ObtainNumberRecursoTISH()


                'I obtain the year and the quota
                mPeriod = pPeriod.ToString.Split(CChar("-"))




                'I add the new DDJJ
                Me.mWS = New ws_tish.Service1
                mResult_XML = Me.mWS.AddDDJJ(mNumberRecurso.ToString.Trim, dtDataComercio_AUX.Rows(0).Item("CONTRIBUYENTE_CUIT").ToString.Trim, _
                                             CInt(dtDataComercio_AUX.Rows(0).Item("COMERCIO_NUMERO").ToString.Trim), _
                                             CInt(mPeriod(0).ToString.Trim), CInt(mPeriod(1).ToString.Trim), _
                                             pXML.ToString.Trim)
                Me.mWS = Nothing
            End If


            Session("xml_voucher_add_ddjj") = mResult_XML.ToString.Trim
            Session("is_new_ddjj") = True
            mOK_ADD = True
        Catch ex As Exception
            Session("xml_voucher_add_ddjj") = Nothing
            mOK_ADD = False
            Me.mWS = Nothing
        End Try


        Return mOK_ADD
    End Function



    'ObtainNumberRecursoTISH:
    'This function returns the number of the recurso TISH
    Private Function ObtainNumberRecursoTISH() As String
        Dim mNumberRecurso As String

        Try
            mNumberRecurso = ""
            Me.mWS = New ws_tish.Service1
            mNumberRecurso = Me.mWS.ObtainNumberRecursoTISH()
            Me.mWS = Nothing

        Catch ex As Exception
            mNumberRecurso = ""
        End Try

        Return mNumberRecurso.ToString.Trim
    End Function


    Private Sub GenerarDataSetPDF(ByVal pdsConstanciaData As DataSet)
        Dim dsVoucherDDJJ As DataSet
        Dim mPeriod() As String
        Dim dtRubtos As DataTable

        Try

            'I Update the dataset with the information of the comercio
            dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))


            dtRubtos = New DataTable
            dtRubtos = pdsConstanciaData.Tables("RUBROS").Copy

            'I create the dataset with the data of the comercio and the data of the ddjj
            dsVoucherDDJJ = New DataSet
            dsVoucherDDJJ.Tables.Add(dtDataComercio_AUX)
            dsVoucherDDJJ.Tables.Add(dtRubtos)


            'I obtain the year and the quota
            mPeriod = pdsConstanciaData.Tables("PERIODOS").Rows(0).Item(0).ToString.Split(CChar("-"))


            'I assing then period
            Session("period_voucher_add_ddjj") = mPeriod(0).ToString.Trim & "/" & mPeriod(1).ToString.Trim


            'I assign the dataset a the variable of session
            Session("xml_constancia_add_ddjj") = dsVoucherDDJJ

        Catch ex As Exception
            Session("xml_constancia_add_ddjj") = Nothing
        End Try

    End Sub


    Private Function EmployeesUsing() As Boolean
        Dim mUsing As String

        Try
            mUsing = "N"

            'Me.mWS = New ws_tish.Service1
            'mUsing = Me.mWS.Exist_DDJJ(CInt(pComercioNumber), CInt(mPeriod(0).ToString.Trim), CInt(mPeriod(1).ToString.Trim))
            'Me.mWS = Nothing

        Catch ex As Exception
            mUsing = "N"
        Finally
            Me.mWS = Nothing
        End Try

        Return CBool(mUsing.Trim = "S")
    End Function

End Class