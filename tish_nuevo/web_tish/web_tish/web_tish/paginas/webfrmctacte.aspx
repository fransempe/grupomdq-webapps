﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmctacte.aspx.vb" Inherits="web_tish.webfrmctacte"  ValidateRequest="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>

    
    <!-- recursos para crear la ventana modal -->
	<link rel="stylesheet" href="../modal/Assets/LightFace.css" />	
	<script src="../modal/mootools.js" type="text/javascript"></script>
	<script src="../modal/mootools-more-drag.js" type="text/javascript"></script>
	<script src="../modal/Source/LightFace.js" type="text/javascript"></script>
	<script src="../modal/Source/LightFace.IFrame.js" type="text/javascript"></script>
	<script src="../modal/Source/LightFace.Image.js" type="text/javascript"></script>
	<script src="../modal/Source/LightFace.Request.js" type="text/javascript"></script>
	<script src="../modal/Source/LightFace.Static.js" type="text/javascript"></script>
	
	    
    
    <script type="text/javascript">    	    
      	function logout() {	 	    	 	        
 	        box = new LightFace({ 
 			    title: 'Sistema TISH :: Cerrar sesión', 
			    width: 250,
			    height: 50,
  			    content: '<div align="left">Usted esta a punto de cerrar su sesión.\n¿Desea continuar?</div>',
 			    buttons: [		
 			        {
					    title: 'Aceptar',
					    event: function() { this.close(); window.location = "webfrmlogin.aspx"; }
				    },
				    {
					    title: 'Cerrar',
					    event: function() { this.close();}
				    }
			    ]
 		    });
 		    box.open();		
 	    }        
	 	    

        function change_trade() {	 	    	 	        
            box = new LightFace({ 
                title: 'Sistema TISH :: Cambiar comercio', 
                width: 300,
                height: 50,
                content: '<div align="left">Usted esta a punto de cambiar de comercio\n¿Desea continuar?</div>',
                buttons: [		
                    {
		                title: 'Aceptar',
		                event: function() { this.close(); window.location= "webfrmseleccionar_comercio.aspx";}
	                },
	                {
		                title: 'Cerrar',
		                event: function() { this.close();}
	                }
                ]
            });
            box.open();		
        }        	 	    

	 	    
	 	function modal_window(p_title, p_width, p_height, p_html) {
	  		box = new LightFace({ 
	 			title: 'Sistema TISH :: ' + p_title, 
				width: p_width,
				height: p_height,
	  			content: p_html,
	 			buttons: [					
					{
						title: 'Cerrar',
						event: function() { this.close(); }
					}
				]
	 		});
	 		box.open();		
	 	}
    
        </script>
    
    
    
        <!-- Seteo the resources -->
        <script src="../js/funciones.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../css/estilos_listado.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_botones.css" type="text/css" />
        <%           
            'Seteo el css que voy a utilizar
            Dim mPath_css As String                
            mPath_css = ""
            mPath_css = System.Configuration.ConfigurationManager.AppSettings("css").ToString.Trim()
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_listado.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_genericos.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_botones.css' type='text/css' />")
        %>                       
               
               
        <!-- ajax -->       
        <script type="text/javascript" language="javascript" src="../js/jquery1-2-6-min.js"></script>
        <script type="text/javascript" language="javascript">
        
            /* Carga del logo de la municipalidad */
            function ajax_load_logo(){   
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_logo.ashx",                                        
                    success: see_response_logo
                });
            }
                        
            function see_response_logo(html){                   
                if (html != '') {
                    var _control = document.getElementById('div_header');                   
                    _control.innerHTML = html;                   
                    return false;
                }
                
            return true;    
            }      
            
            
            
            /* Cargar ctacte */
            function ajax_load_ctacte(p_type){  
                var _html;
                var _comercio;
                var _controles;
                var _control;
                var _message;
                

                _controles = new Array('gridctacte','gridvouchers','div_totales','div_contenedor_botones','div_messenger');                                
                for (i=0; i < _controles.length; i++){                
                    _control = window.document.getElementById(_controles[i]);
                    if (_control != null){                
                        padre = _control.parentNode;
                        padre.removeChild(_control); 
                    }
                }
                
                
                             
                if (p_type == 'expired') {
                    window.document.getElementById('lbltitulo_ctacte').innerHTML = 'Cuenta corriente :: Comprobantes vencidos';
                    window.document.getElementById('link_vouchers_expired').style.display = 'none';
                    window.document.getElementById('link_vouchers').style.display = 'inline';
                    _message = 'Cargando cuenta corriente vencida......';
                } else {
                    window.document.getElementById('lbltitulo_ctacte').innerHTML = 'Cuenta corriente :: Comprobantes a vencer';
                    window.document.getElementById('link_vouchers_expired').style.display = 'inline';
                    window.document.getElementById('link_vouchers').style.display = 'none';
                    _message = 'Cargando comprobantes a vencer......';
                }

            		          
            	_html = '<div id="loading" align="center">'+
            	            '<table border= "0">' +
	                            '<tr>' +
	                                '<td align="center" style="width:10%;"><img id="img_load" alt="" src="../imagenes/loading.gif" /></td>' +
	                                '<td align="left" style="width:90%;">' + _message +'</td>' +
	                            '</tr>' +
	                        '</table>' +
	                    '</div>';              		          
                window.document.getElementById('div_deuda').innerHTML = _html;
                
                
                _comercio = window.document.getElementById('lblcomercio_user').innerHTML;                
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_ctacte.ashx",                                                            
                    data:"type="+p_type+"&comercio=" + _comercio,
                    success: see_response_load_ctacte
                });
            }            
            
            function see_response_load_ctacte(html){                   
                var _pos;
                var _message;
                
                if (html != '') {
                    window.document.getElementById('div_deuda').innerHTML = html;    
                    message_credito();                                                                                                    
                                        
                return false;
                }                
            return true;    
            }          
            
            
            
            /* emitir comprobante */
            function ajax_voucher(p_xml, p_type){    
                
                loading('Generando comprobantes de pago');
                
                if (p_type == 'expired') {
                    jQuery.ajax({
                        type:"POST", 
                        url:"../controladores/handler_voucher.ashx",                                        
                        data:"xml=" + p_xml + "&type="+p_type,
                        success: see_response_voucher_expired
                    });            
                } else { 
                    jQuery.ajax({
                        type:"POST", 
                        url:"../controladores/handler_voucher.ashx",                                        
                        data:"xml=" + p_xml + "&type="+p_type,
                        success: see_response_voucher
                    });                            
                }
            }
            
            function see_response_voucher_expired(xml) { if (xml != '') { voucher_expired_pdf(xml); }}            
            function see_response_voucher(xml) { if (xml != '') { voucher_pdf(xml); }}      
            
        </script>     
               
             
        
        
        
                
        <!-- Estas funciones le dan efecto a la grilla cuando paso el mouse por arriba -->
        <script type="text/javascript">
        
            /* Esta Funcion Asigna Efectos a las Grillas */
            function efecto_grilla(id_grilla) {
              var grilla = window.document.getElementById(id_grilla);
              if (grilla != null) {
                for (i=0; fila=grilla.getElementsByTagName('td')[i]; i++) {
                    fila.onmouseover = function() {iluminar(this,true)}
                    fila.onmouseout = function() {iluminar(this,false)} 
                }     
              }
            }

            /* Esta Funcion Asigna el EFECTO de CAMBIAR DE COLOR */
            function iluminar(obj,valor) {
                var fila = obj.parentNode;
                for (i=0; filas = fila.getElementsByTagName('td')[i]; i++)
                    filas.style.background = (valor) ? 'gray' : '';     
            }
        </script>
        
        
        
         



        <!-- funcion doPostBack y quienes las usan -->
        <script type="text/javascript" language="javascript">
            
            function voucher_expired_pdf(p_xml){ __doPostBack(p_xml, 'expired'); }
            function voucher_pdf(p_xml){ __doPostBack(p_xml, 'voucher'); }
        
            function __doPostBack(eventTarget, eventArgument) {
                var _form;
                
                form = document.forms["formulario"];                                
                form.__EVENTTARGET.value = eventTarget.split("$").join(":");
                form.__EVENTARGUMENT.value = eventArgument;
                form.submit();
            }
        </script>
        
        
        
        
        
        <!-- funciones varias -->
        <script type="text/javascript" language="javascript">

            
            /* Esta Funcion CALCULA el IMPORTE de lo seleccionado */        
            function calcular_seleccion(p_grilla){
                var total = 0;
                var total_aux = 0;
                var celda_importe = 0;
                var cant_seleccionados = 0;
                var id_grilla = p_grilla;
                
                                
                /* celda que contiene el importe */
                if (id_grilla == 'gridctacte') {celda_importe = 8;} else {celda_importe = 6;}
                
             
                for (var i=1; i<window.document.getElementById(id_grilla).rows.length; i++) {
                               
                    var control_check = window.document.getElementById('check'+i);
                    if (control_check != null){
                        if (control_check.checked) {               
                            total_aux = window.document.getElementById(id_grilla).rows[i].cells[celda_importe].innerHTML;
                            total = parseFloat(total) + parseFloat(total_aux);
                            cant_seleccionados = cant_seleccionados + 1;
                        }                        
                    }                            
                }

                /* asigno datos */
                window.document.getElementById('td_registros_selecionados').innerHTML = cant_seleccionados;
                window.document.getElementById('td_registros_no_seleccionados').innerHTML = ((window.document.getElementById(id_grilla).rows.length-1) - cant_seleccionados);
                window.document.getElementById('td_importe_total_seleccionado').innerHTML = formato_importe(total);            
            }
            
            
            /* Esta Funcion ASIGNA FORMATO a un IMPORTE */        
            function formato_importe(importe) {
                importe = importe.toString().replace(/$|,/g,'');
                if(isNaN(importe))
                    importe = "0";
                    
                sign = (importe == (importe = Math.abs(importe)));
                importe = Math.floor(importe*100+0.50000000001);
                cents = importe%100;
                importe = Math.floor(importe/100).toString();
                
                if(cents<10)
                    cents = "0" + cents;
                    
                for (var i = 0; i < Math.floor((importe.length-(1+i))/3); i++)
                    importe = importe.substring(0,importe.length-(4*i+3))+','+
                    
                importe.substring(importe.length-(4*i+3));
                return (((sign)?'':'-') + '$ ' + importe + '.' + cents);
            }



            function pay_voucher_expired() { 
                var _xml; 
                
                _xml = get_vouchers_expired_selected();                                
                if (_xml != '') {                
                    ajax_voucher(_xml, 'expired');
                } else {                                     
                    modal_window('Comprobantes vencidos', 350, 30, 'Debe seleccionar al menos un comprobante a pagar.'); 
                }
            }
                        
            function pay_voucher() {
                var _xml;

                _xml = get_vouchers_selected();
                if (_xml != '') {                
                    ajax_voucher(_xml, 'voucher');                
                } else {                                     
                    modal_window('Comprobantes a vencer', 350, 30, 'Debe seleccionar al menos un comprobante a pagar.'); 
                }
            }                   





            /* Genero el XML con los comprobantes vencidos */
            function get_vouchers_expired_selected() {                           
                var id_grilla = 'gridctacte';
                var _xml;
                var _xml_selected;
                
                
                _xml = '<VFPDATA>\n';
                _xml = _xml + '<DETALLEPERIODOSSELECCIONADOS>\n' +
                                '<NROIMPONIBLE></NROIMPONIBLE>\n' +
                                '<TIPOIMPONIBLE></TIPOIMPONIBLE>\n' +
                                '<RECURSO>A1</RECURSO>\n' +
                                '<ANIO></ANIO>\n' +
                                '<CUOTA></CUOTA>\n' +
                                '<NROMOV></NROMOV>\n' +
                              '</DETALLEPERIODOSSELECCIONADOS>\n';
                
                
                _xml_selected = '';
                for (var i=1; i< window.document.getElementById(id_grilla).rows.length; i++) {
                      
                    var control_check = window.document.getElementById('check'+i);                                             
                    if (control_check != null){
                        if (control_check.checked) {                                                                
                          _xml_selected = _xml_selected + '<DETALLEPERIODOSSELECCIONADOS>\n' +
                                                                '<NROIMPONIBLE>' + window.document.getElementById('lblcomercio_user').innerHTML + '</NROIMPONIBLE>\n' +
                                                                '<TIPOIMPONIBLE>C</TIPOIMPONIBLE>\n' +
                                                                '<RECURSO>05</RECURSO>\n' +
                                                                '<ANIO>' + window.document.getElementById(id_grilla).rows[i].cells[1].innerHTML + '</ANIO>\n' +
                                                                '<CUOTA>' + window.document.getElementById(id_grilla).rows[i].cells[2].innerHTML + '</CUOTA>\n' +
                                                                '<NROMOV>' + window.document.getElementById(id_grilla).rows[i].cells[0].innerHTML + '</NROMOV>\n' +
                                                          '</DETALLEPERIODOSSELECCIONADOS>\n';                                                                  
                        }                        
                    }
                }          
                
                              
                if (_xml_selected == '') {
                    _xml = '';
                } else {
                    _xml = _xml + _xml_selected
                    _xml = _xml + "</VFPDATA>";
                }
                
            return _xml;
            }


            /* Genero el XML con los comprobantes a vencer */
            function get_vouchers_selected() {                           
                var id_grilla = 'gridvouchers';
                var _voucher_number_aux;
                var _voucher_number;
                var _xml;
                var _xml_selected;


                _xml = '<VFPDATA>\n';                              
                _xml = _xml + '<DETALLECOMPROBSELECCIONADOS>\n' +
                                '<TIPOIMPONIBLE></TIPOIMPONIBLE>\n' +
                                '<NROIMPONIBLE></NROIMPONIBLE>\n' +
                                '<GRUPOCOMP></GRUPOCOMP>\n' +
                                '<NROCOMP></NROCOMP>\n' +
                              '</DETALLECOMPROBSELECCIONADOS>\n';

                
                _xml_selected = '';
                for (var i=1; i< window.document.getElementById(id_grilla).rows.length; i++) {
                      
                    var control_check = window.document.getElementById('check'+i);                                             
                    if (control_check != null){
                        if (control_check.checked) {                                        
                                                                                          
                            _voucher_number_aux = window.document.getElementById(id_grilla).rows[i].cells[0].innerHTML;
                            _voucher_number = _voucher_number_aux.split('/');                                                                                                          
                                                                       
                            _xml_selected = _xml_selected + '<DETALLECOMPROBSELECCIONADOS>\n' +
                                                                '<TIPOIMPONIBLE>C</TIPOIMPONIBLE>\n' +
                                                                '<NROIMPONIBLE>' + window.document.getElementById('lblcomercio_user').innerHTML + '</NROIMPONIBLE>\n' +
                                                                '<GRUPOCOMP>' + _voucher_number[0] + '</GRUPOCOMP>\n' +
                                                                '<NROCOMP>' + _voucher_number[1] + '</NROCOMP>\n' +
                                                            '</DETALLECOMPROBSELECCIONADOS>\n';                                                                             
                        }                        
                    }
                }                 
                
                
               if (_xml_selected == '') {
                    _xml = '';
                } else {
                    _xml = _xml + _xml_selected
                    _xml = _xml + "</VFPDATA>";
                }
                                                            
            return _xml;
            }




        function message_credito() {
            var _message;
        
            if (window.document.getElementById('div_messenger_credito') != null) {                        
                _message = '<div align="left">\n' +
                                'Usted cuenta con un valor de ' + window.document.getElementById('strong_importe').innerHTML + ' en crédito.\n' +
                                'Para poder realizar el uso de este crédito deberá presentarte en el palacio municipal.\n' +
                           '</div>';
                modal_window('Comprobantes a vencer', 500, 50, _message); 
            }                                    
        }


        function list_cta_cte() {
            loading('Generando listado de cuenta corriente');
            window.location= "listados/webfrmlistadoctacte.aspx";
        }


        function loading(p_message) {
            var _html;
        
        	_html = '<div id="loading" align="center">'+
        	            '<table border= "0">' +
                            '<tr>' +
                                '<td align="center" style="width:10%;"><img id="img_load" alt="" src="../imagenes/loading.gif" /></td>' +
                                '<td align="left" style="width:90%;">' + p_message + '....' +'</td>' +
                            '</tr>' +
                        '</table>' +
	                '</div>';          
             window.document.getElementById('div_contenedor_botones').innerHTML = _html;
        }

            
        </script> 
        
        
        <title>Sistema TISH :: Cuenta Corriente</title>
        <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
    
    
    </head>


    <body onload="javascript:ajax_load_ctacte('expired')">
      
    
        <!-- div container of the page -->	
        <div id="wrap">
            
            <!-- div header -->	            
		    <div id="header">		
		    
		        <!-- div container font -->
                <div id="container_font">			        
		            <div  style="width:5%; float:left;  padding:0px 5px 0px 5px;">
                        <a href="#" title="Usar fuente menor" class="link_font" onclick="assign_font('-');">A-</a> 
                    </div>
                    
                    <div  style="width:5%; float:left;">
		                <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 
                    </div>		            
                    
		            <div  style="width:5%; float:left;">
		                <a href="#" title="Usar fuente mayor" class="link_font" onclick="assign_font('+');">A+</a> 
		            </div>	            
                </div>
		    
		        <div id="div_header"  class="div_image_logo">                
                    <img  class="image_logo" alt="" src="../imagenes/topiz.jpg"  />
                </div>    
		     	     
			    <h1 id="logo-text">Tasa de Inspección por Seguridad e Higiene</h1>			
			    <h2 id="slogan">
                    <asp:Label ID="lblslogan_municipalidad" runat="server" Text=""></asp:Label>
                </h2>				
		    </div>
	  
	  
	  
	        <!-- div container -->	
	        <div id="content-wrap">
	  
	            <!-- div container page title -->
                <div id="div_title" style="background-color:Black;" align ="left" >
                    <strong class="titulo_page">Sistema TISH :: Cuenta corriente</strong>
                </div>
	        
	  
	  	          <!-- div container right -->
                <div id="div_optiones" class="menu_options">
                
                    <div id="div_comercio" style=" padding-top:20px;">
                        <div align="center">
                            <table style="width:95%">
                                <tr>
                                    <td  rowspan="2"><img id="user" alt="" src="../imagenes/user.png"  style=" width:50px; height:60px; border:none"/></td>
                                    <td><h1 align ="left" style="color:Black">Comercio</h1></td>
                                </tr>
                                <tr>                                
                                    <td>
                                        NÚMERO: <asp:Label ID="lblcomercio_user" runat="server" Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>                                          
                    </div>
                    
                    
                    <h1 align ="left" style="color:Black">Accesos</h1>                    			
	                <ul class="sidemenu">               
                        <li><a href="webfrmdeclaracionesjuradas.aspx">Ver listado de DDJJ</a></li>
                        <li><a href="webfrmctacte.aspx">Ver cuenta corriente</a></li>
                        <li><a href="webfrmopciones.aspx">Datos del comercio</a></li>				                
                        <li><a href="#" onclick="javascript:change_trade();">Cambiar comercio</a></li>			                        
                    </ul>	
	                
	                <h1 align ="left" style="color:Black">Usuario Web</h1>				
                    <ul class="sidemenu">
	                    <li><a href="webfrmdatos_representante.aspx">Mis datos</a></li>				                
		                <li><a href="webfrmcambiar_clave.aspx">Cambiar clave</a></li>	
		                <li><a href="#" onclick="javascript:logout();">Cerrar sesión</a></li>
	                </ul>	
                </div>
	  
	  
                <!-- div container left -->		  
	  		    <div id="main"> 

                <!-- form -->	                
                <form id="formulario" runat="server">
                      
                    <!-- Controles para utilizar la funcion doPostBack sin controles ASP -->
                    <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
                    <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />

                

                            <!-- div container links -->	
                            <div  id="div_link_listado_ddjj">                                 
                                <strong id="lbltitulo_ctacte" class="titulo_3">Cuenta Corriente</strong>
                                <a id="link_vouchers_expired" class="link" href="#" onclick="javascript:ajax_load_ctacte('expired')">[ver comprobantes vencidos]</a>
                                <a id="link_vouchers" class="link" href="#" onclick="javascript:ajax_load_ctacte('vouchers')">[ver comprobantes a vencer]</a>
                            </div>			
                                          
                                          
                            <!-- message the user -->	                                                     
	                        <p id="p_attention" runat="server">
                                <strong>ATENCIÓN:</strong> 
                                <asp:Label ID="lblattention" runat="server" Text="Usted se encuentra viendo la cuenta corriente correspondiente a los últimos dos años."></asp:Label>
                                <br />
                                Los importes por recargos de los períodos adeudados que aquí se detallan se encuentran calculados a la fecha <asp:Label ID="lblfecha_intereses" runat="server" Text=""></asp:Label>.
                            </p>
                            
                    
		                            
                            <!-- Div contenedor de la tabla de movimientos -->
                            <div id="div_container_deuda">                                    
		                        
                                <div id="div_deuda">
            		            
	                            </div>
                            </div>   
                    
           
		        </form>		         
		    </div>
											
            </div> 	
			  
			  
			  
            <!-- div container footer -->				  
	  		<div id="footer">
		
		        <div id="div_pie_municipalidad" style="width:70%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		            Municipalidad de 		            
		            <strong> 
		                <asp:Label ID="lblnombre_municipalidad" runat="server" Text="NOMBRE_MUNICIPIO"></asp:Label> 
                    </strong> 
                    
				    <br />
				    Teléfono: 
				    <strong> 
                        <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                    </strong> 
                    | Email: 
				    <strong> 
                        <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                    </strong>                 
			    </div>
		   		   
		        <div id="div_pie_grupomdq"  class="footer_grupomdq" >
				    Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ S.A</a></strong>  
				    <br />
				    Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				    <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
                    <asp:Label ID="lblversion" runat="server" Text=""></asp:Label>
		        </div>
			
		    </div>	
		
		
		<!-- content-wrap ends here -->
		</div>	
		
    </body>
</html>
