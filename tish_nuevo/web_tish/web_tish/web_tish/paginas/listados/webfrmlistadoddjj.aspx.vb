﻿Option Explicit On
Option Strict On



'Web Services
Imports web_tish.ws_tish.Service1
Partial Public Class webfrmlistadoddjj
    Inherits System.Web.UI.Page

#Region "Variables"

    Private Structure SLogo
        Dim LogoOrganismo As Byte()
        Dim RenglonOrganismo As String
        Dim RenglonNombreOrganismo As String
    End Structure

    Private mWS As web_tish.ws_tish.Service1
    Private mPDF As clsPDFListadoDDJJ
    Private mLogo As SLogo

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dsDataComercio As DataSet
        Dim mRutaFisica As String
        Dim mNombreArchivoComprobante As String
        Dim mArchivo As String
        Dim mCodigoMunicipalidad As String
        Dim dtDataComercio_AUX As DataTable
        Dim com_ddjj As List(Of entidades.com_ddjj)


        Try

            com_ddjj = Me.getListadoDDJJ()
            If (com_ddjj IsNot Nothing) Then

                'I Update the dataset
                dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))


                'I obtain the data of the comercio
                dsDataComercio = New DataSet
                dsDataComercio.Tables.Add(dtDataComercio_AUX)



                'Creo Directorio y Archivo en Disco
                mRutaFisica = Request.ServerVariables("APPL_PHYSICAL_PATH").ToString & "comprobantespdf"
                If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString)) Then
                    My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString)
                End If
                mNombreArchivoComprobante = dtDataComercio_AUX.Rows(0).Item("COMERCIO_NUMERO").ToString.Trim & "listadoddjj" & Format(Now, "HHmmss")
                mNombreArchivoComprobante = "ddjj" & Format(Now, "HHmmss")
                mArchivo = mRutaFisica & "\" & mNombreArchivoComprobante & ".pdf"


                'Creo y Seteo el Nombre del Logo dependiendo de la Conexion
                mCodigoMunicipalidad = ""



                'Genero el PDF
                mPDF = New clsPDFListadoDDJJ
                mPDF.RutaFisica = mArchivo.ToString


                mPDF.DatosMunicipalidad = Me.ObtenerDatosMunicipalidad()
                mPDF.DataComercio = dsDataComercio
                mPDF.listado_com_ddjj = com_ddjj
                mPDF.crearPDF()


                'Muestro el Comprobante por pantalla
                Response.Expires = 0
                Response.Buffer = True
                Response.Clear()
                Response.ContentType = "application/pdf"
                Response.TransmitFile(mArchivo.ToString)
                Response.Flush()



            Else
                Call LimpiarSession()
            End If

        Catch ex As Exception
            Response.Redirect("../webfrmerror.aspx", False)
        End Try
    End Sub


#Region "procedimientos y Funciones"

    Private Sub LimpiarSession()
        Session.RemoveAll()
        Session.Clear()
        Session.Abandon()
        Response.Redirect("../webfrmindex.aspx", False)
    End Sub

    Private Function ObtenerTipoCuenta(ByVal mTipoCuenta As String) As String
        Select Case mTipoCuenta
            Case "I" : Return "INMUEBLE"
            Case "C" : Return "COMERCIO"
            Case "E", "O" : Return "CEMENTERIO"
            Case "V" : Return "VEHICULOS"
            Case Else : Return ""
        End Select
    End Function


    Private Function ObtenerDatosMunicipalidad() As DataSet
        Dim dsDatos As DataSet
        Dim mXML As String

        Try

            'Obtengo los datos de la municipalidad
            mWS = New web_tish.ws_tish.Service1
            mXML = mWS.ObtainMunicipalidadData()
            mWS = Nothing


            dsDatos = New DataSet
            dsDatos = ClsTools.ObtainDataXML(mXML.ToString.Trim)

            Return dsDatos
        Catch ex As Exception
            'mLog = New Clslog
            'mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            'mLog = Nothing
            Response.Redirect("../webfrmerror.aspx", False)
            Return Nothing
        End Try

    End Function


    Private Function getListadoDDJJ() As List(Of entidades.com_ddjj)
        Dim dtDataComercio_AUX As DataTable
        Dim com_ddjj_aux As List(Of entidades.com_ddjj)
        Dim strXML As String

        Try

            'I Update the dataset
            dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))

            mWS = New ws_tish.Service1
            strXML = mWS.getListadoDDJJByComercio(CLng(dtDataComercio_AUX.Rows(0).Item("COMERCIO_NUMERO").ToString.Trim))
            mWS = Nothing

            com_ddjj_aux = New List(Of entidades.com_ddjj)()
            com_ddjj_aux = entidades.serializacion.deserializarXML(Of List(Of entidades.com_ddjj))(strXML.Trim())

        Catch ex As Exception
            com_ddjj_aux = Nothing
        Finally
            mWS = Nothing
        End Try


        Return com_ddjj_aux
    End Function


    Private Function EmployeesUsing() As Boolean
        Dim mUsing As String

        Try
            mUsing = "N"

            'Me.mWS = New ws_tish.Service1
            'mUsing = Me.mWS.Exist_DDJJ(CInt(pComercioNumber), CInt(mPeriod(0).ToString.Trim), CInt(mPeriod(1).ToString.Trim))
            'Me.mWS = Nothing

        Catch ex As Exception
            mUsing = "N"
        Finally
            Me.mWS = Nothing
        End Try

        Return CBool(mUsing.Trim = "S")
    End Function


#End Region

End Class