﻿Option Explicit On
Option Strict On

Partial Public Class webfrmresultados_genericos
    Inherits System.Web.UI.Page

    Private dtDataComercio_AUX As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not (IsPostBack) Then

            'I Update the dataset
            dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))


            Call Me.SloganMuniicpalidad()
            Call Me.ValidarSession()
            Call Me.CargarPie()

            Me.table_messenger_pdf.Visible = False
            Me.table_messenger.Visible = False


            If (Session("type_messenger") IsNot Nothing) Then

                If (Session("type_messenger").ToString.Trim = "pdf") Then

                    Me.table_messenger_pdf.Visible = True
                    If (Session("name_pdf") IsNot Nothing) Then

                        Me.lblmessenger_pdf.Text = "La operación se ha realizado con éxito"


                        Me.lblmessage_mascomprobantes.Text = "Usted adeuda aun más comprobantes, si desea acceder a ellos ingrese al siguiente link:" & _
                                                             "<a href='#'>Ver cuenta corriente</a>"


                        Me.link_pdf.HRef = "../comprobantespdf/" & Session("name_pdf").ToString.Trim & ".pdf"
                    Else
                        Me.lblmessenger_pdf.Text = "No se ha podido generar el comprobante"
                    End If


                Else


                    ' Si existe la variable relogin, y si tiene un valor true, significa que viene
                    ' desde el primer imgreso del usuario y que acaba de modificar su clave.
                    ' Por este motivo, el usuario debera volver a loguearse con su nueva clave antes de poder
                    ' acceder a algún menú del sitio Web. 
                    If (Session("relogin") IsNot Nothing) Then
                        If CBool((Session("relogin"))) Then
                            Me.h1_accesos.Visible = False
                            Me.ul_accesos.Visible = False
                            Me.h1_usuario_web.Visible = False
                            Me.ul_usuario_web.Visible = False

                            Me.btnback_messenger.Text = "Finalizar"
                        End If
                    End If



                    'Me.lbltitle.Text = "Información no encontrada."
                    Me.lbltitle.Visible = False
                    Me.table_messenger.Visible = True

                    If (Session("messenger") IsNot Nothing) Then
                        If (Session("messenger").ToString.Trim <> "") Then
                            Me.lblmessenger.Text = Session("messenger").ToString.Trim
                        End If
                    End If
                    End If
            End If


        End If
    End Sub


    Private Sub SloganMuniicpalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema TISH."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema TISH."
        End If

    End Sub


    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            Me.lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            Me.lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If


        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub


    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("contribuyente_nombre") IsNot Nothing) Then
                If (Session("contribuyente_nombre").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        If (mLogin_OK) Then


            ' Si la variable de sesión se encuentra null significa que el usuario realizo el primer ingreso y viene de la
            ' página webfrmcambiar_clave.
            dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))
            If (dtDataComercio_AUX Is Nothing) Then
                td_numero.InnerHtml = "CUIT: " & Session("cuit_manager").ToString.Trim
            Else
                dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))
                Me.lblcomercio_user.Text = dtDataComercio_AUX.Rows(0).Item("COMERCIO_NUMERO").ToString.Trim()
            End If
        Else
            Response.Redirect("webfrmlogin.aspx", False)
        End If


    End Function

    Protected Sub btnback_messenger_pdf_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnback_messenger_pdf.Click
        Call Go_Options()
    End Sub

    Protected Sub btnback_messenger_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnback_messenger.Click
        Call Go_Options()
    End Sub

    Private Sub Go_Options()
        Dim mSession As String

        mSession = "webfrmopciones.aspx"
        If (Session("url") IsNot Nothing) Then
            If (Session("url").ToString.Trim <> "") Then
                mSession = Session("url").ToString.Trim
            End If
        End If


        Response.Redirect(mSession.ToString.Trim, False)
    End Sub

End Class