﻿Public Partial Class webfrmexpiration_periods_list
    Inherits System.Web.UI.Page


#Region "Variables"
    Private mWS As ws_tish.Service1    
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not (IsPostBack) Then

            Call Me.SloganMunicipalidad()
            Call Me.ValidarSession()
            Call Me.CargarPie()
            Call Me.LoadPeriods()
            Call Me.LoadDescriptionResourceTISH()

            Me.cbperiodos.Focus()
        Else
        End If

    End Sub


    Private Sub SloganMunicipalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema TISH."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema TISH."
        End If

    End Sub

    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If


        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub


    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("nombre_usuario") IsNot Nothing) Then
                If (Session("nombre_usuario").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        If (mLogin_OK) Then
            lblnombre_usuario.Text = Session("user_name").ToString.Trim
        Else
            Response.Redirect("webfrmlogin_rafam.aspx", False)
        End If

    End Function


    Private Sub LoadPeriods()

        Me.cbperiodos.Items.Add((Now.Year - 1).ToString)
        Me.cbperiodos.Items.Add(Now.Year.ToString)
        Me.cbperiodos.Items.Add((Now.Year + 1).ToString)

        Me.cbperiodos.SelectedIndex = 1
    End Sub


    Private Sub LoadDescriptionResourceTISH()
        Dim mDescriptionResourceTISH As String

        Try
            mDescriptionResourceTISH = ""
            Me.mWS = New ws_tish.Service1
            mDescriptionResourceTISH = Me.mWS.ObtainDescriptionResourceTISH()


        Catch ex As Exception
            mDescriptionResourceTISH = "Recurso TISH"
        Finally
            Me.mWS = Nothing
        End Try

        Me.lbldescription_resource_tish_add.Text = mDescriptionResourceTISH.Trim
        Me.lbldescription_resource_tish_edit.Text = mDescriptionResourceTISH.Trim
    End Sub

End Class