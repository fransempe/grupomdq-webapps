﻿Option Explicit On
Option Strict On

Partial Public Class webfrmeditar_representante
    Inherits System.Web.UI.Page

#Region "Variables"
    Private mWS As ws_tish.Service1
    Private mXML As String
    Private mPositionTrade As Integer

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mEvent As String
        Dim mPosition As Integer

        If Not (IsPostBack) Then

            Call Me.SloganMunicipalidad()
            Call Me.ValidarSession()
            Call Me.CargarPie()


            'I assign the events javascript
            Me.btnedit.Attributes.Add("onclick", "javascript:return to_validate_data();")
            Me.txtcuit.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtcuit.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtcuit.Attributes.Add("onkeydown", "javascript:return loading_key(this, event);")
            Me.txtreason_social.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtreason_social.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtcontac.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtcontac.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtaddress.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtaddress.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txttelephone.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txttelephone.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtemail.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtemail.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtnumber_trade.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtnumber_trade.Attributes.Add("onblur", "javascript:event_focus(this);")



            Me.btn_load_trade.Attributes.Add("onclick", "javascript:return validar_nro_comercio();")



            Me.div_link_add_trade.visible = False
            Me.div_container_trade.Visible = False
            Me.div_list_trades.Visible = False
            Me.table_data_trade.Visible = False
            Me.table_select_trade.Visible = False
            Me.div_container_trade.Visible = False

            Call Me.LoadGrilla_RecordBlank()
            Me.txtcuit.Focus()


        Else

            mEvent = Request.Params.Get("__EVENTTARGET")


            Select Case mEvent.Trim
                Case "delete_trade"
                    If (Request.Params.Get("__EVENTARGUMENT").ToString <> "") Then
                        mPosition = CInt(Request.Params.Get("__EVENTARGUMENT").ToString)

                        If (mEvent.ToString.Trim = "delete_trade") Then
                            Call Me.DeleteTrade(mPosition)
                        End If
                    End If

                Case "load_manager"
                    Call Me.LoadDataManager(txtcuit.Text.Trim)
            End Select

 
        End If
    End Sub

    Protected Sub btnload_manager_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnload_manager.Click
        Call Me.LoadDataManager(txtcuit.Text.Trim)
    End Sub


    Private Sub LoadDataManager(ByVal pCuit As String)
        Dim dsDataManager_AUX As DataSet
        Dim mListTrades As String
        Dim mListTrades_XML As String
        Dim dsDataTrades As DataSet


        Try


            'I obtain the data of the trade
            mWS = New ws_tish.Service1
            dsDataManager_AUX = mWS.ObtainDataManager(pCuit.ToString.Trim)
            mWS = Nothing

            Me.lblmessenger.Visible = False
            Me.div_messenger.Visible = False
            If (dsDataManager_AUX IsNot Nothing) Then
                If ((dsDataManager_AUX.Tables("Manager").Rows.Count - 1) <> -1) Then
                    If (dsDataManager_AUX.Tables("Manager").Rows(0).Item("FECHA_BAJA").ToString.Trim <> "") Then

                        Me.lblmessenger.Text = "El usuario Web seleccionado se encuentra en estado de baja." & vbCr & _
                                            "Para realizar esta operación usted deberá primero dirigirse a la opción: " & vbCr & _
                                            "<A href='webfrmactivar_representante.aspx' class='link'>[Activar usuario Web] </A>"
                        Me.lblmessenger.Visible = True
                        Me.div_messenger.Visible = True
                        Call Me.CleanTXT()
                        Me.txtcuit.Focus()
                        Exit Sub
                    Else

                        txtcuit.Text = dsDataManager_AUX.Tables("Manager").Rows(0).Item("CUIT").ToString.Trim
                        txtreason_social.Text = dsDataManager_AUX.Tables("Manager").Rows(0).Item("RAZON_SOCIAL").ToString.Trim
                        txtcontac.Text = dsDataManager_AUX.Tables("Manager").Rows(0).Item("CONTACTO").ToString.Trim
                        txtaddress.Text = dsDataManager_AUX.Tables("Manager").Rows(0).Item("DIRECCION").ToString.Trim
                        txttelephone.Text = dsDataManager_AUX.Tables("Manager").Rows(0).Item("TELEFONO").ToString.Trim
                        txtemail.Text = dsDataManager_AUX.Tables("Manager").Rows(0).Item("EMAIL").ToString.Trim
                        Me.btnedit.Visible = True
                    End If

                Else
                    Me.lblmessenger.Text = "El cuit ingresado no corresponde a ningún usuario Web." & vbCr & _
                                        "Si usted quiere dar de alta este representante utilice la siguiente opción: " & vbCr & _
                                        "<A href='webfrmalta_representantes.aspx' class='link'>[Alta de usuario Web] </A>"
                    Me.lblmessenger.Visible = True
                    Me.div_messenger.Visible = True
                    Call Me.CleanTXT()
                    Me.txtcuit.Focus()
                    Exit Sub
                End If
            End If




            'I believe the string with the numbers of trades
            mListTrades = ""
            For i = 0 To dsDataManager_AUX.Tables("Trades").Rows.Count - 1
                mListTrades = mListTrades.ToString.Trim & dsDataManager_AUX.Tables("Trades").Rows(i).Item("NRO_COMERCIO").ToString & "|"
            Next


            'I obtain the data of the trades in FOX
            mListTrades_XML = ""
            dsDataTrades = New DataSet
            If (mListTrades.ToString.Trim <> "") Then
                mWS = New ws_tish.Service1
                mListTrades_XML = mWS.obtainListTrades(mListTrades.ToString.Trim)
                mWS = Nothing


                'I fill the dataset with the information of the trades
                dsDataTrades = clsTools.ObtainDataXML(mListTrades_XML.ToString.Trim)
            End If



            If (clsTools.HasData(dsDataTrades)) Then
                div_list_trades.Visible = True
            End If



            Me.mPositionTrade = 0
            If (clsTools.HasData(dsDataTrades)) Then
                Session("dsList_Trades") = dsDataTrades.Copy
                Me.gvlist_trade.DataSource = dsDataTrades
                Me.gvlist_trade.DataBind()
            End If



            Me.div_link_add_trade.Visible = True
        Catch ex As Exception

        Finally
            Me.td_load.InnerHtml = "<img id='img_loading' alt='' src='../imagenes/loading.gif'  style='display:none;'/>" & vbCr & _
                                   "<img id='btnload' alt='' src='../imagenes/load.gif' class='boton_load' onclick='javascript:return loading();' />"
        End Try
    End Sub

    Private Sub SloganMunicipalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema TISH."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema TISH."
        End If

    End Sub

    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If


        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub


    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("nombre_usuario") IsNot Nothing) Then
                If (Session("nombre_usuario").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        If (mLogin_OK) Then
            lblnombre_usuario.Text = Session("user_name").ToString.Trim            
        Else
            Response.Redirect("webfrmlogin_rafam.aspx", False)
        End If

    End Function

    Protected Sub link_manager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_manager.Click
        If (link_manager.Text = "[Ver]") Then
            link_manager.Text = "[Ocultar]"
            div_manager.Visible = True
        Else
            link_manager.Text = "[Ver]"
            div_manager.Visible = False
        End If
    End Sub

    Protected Sub link_add_trade_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_add_trade.Click
        If (link_add_trade.Text = "[Ver]") Then
            link_add_trade.Text = "[Ocultar]"
            div_container_trade.Visible = True
        Else
            link_add_trade.Text = "[Ver]"
            div_container_trade.Visible = False
        End If

    End Sub



    Private Function Edit() As Boolean
        Dim dsList_Trades As DataSet
        Dim mDataManager As String
        Dim mDataTrades As String
        Dim mOK As Boolean


        mDataManager = ""        
        mDataManager = txtcuit.Text.Trim & "|"        
        mDataManager = mDataManager.ToString.Trim & txtreason_social.Text.Trim & "|"
        mDataManager = mDataManager.ToString.Trim & txtcontac.Text.Trim & "|"
        mDataManager = mDataManager.ToString.Trim & txtaddress.Text.Trim & "|"
        mDataManager = mDataManager.ToString.Trim & txttelephone.Text.Trim & "|"
        mDataManager = mDataManager.ToString.Trim & txtemail.Text.Trim & "|"


        dsList_Trades = New DataSet
        dsList_Trades = CType(Session("dsList_Trades"), DataSet).Copy


        mDataTrades = ""
        If ((dsList_Trades.Tables(0).Rows.Count - 1) <> -1) Then
            For i = 0 To dsList_Trades.Tables(0).Rows.Count - 1
                mDataTrades = mDataTrades.ToString.Trim & dsList_Trades.Tables(0).Rows(i).Item(0).ToString.Trim & "|"
            Next
        Else
            mDataTrades = "|"
        End If



        Try

            'I obtain the data of the trade
            mOK = False
            mWS = New ws_tish.Service1
            mOK = mWS.EditUserWeb(mDataManager.ToString.Trim, mDataTrades.ToString.Trim)
            mWS = Nothing

        Catch ex As Exception
            mOK = False
        End Try




        Session("type_messenger") = "operation"
        Response.Redirect("webfrmresultado.aspx", False)




    End Function

    Protected Sub btnedit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnedit.Click
        Call Me.Edit()
    End Sub

    Protected Sub btnadd_trade_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnadd_trade.Click
        Dim dsList_Trades As DataSet
        Dim dsSelect_Trade_AUX As DataSet
        Dim dtList_Trades_AUX As DataTable
        Dim mRow As DataRow


        Try

            If (Session("dsSelect_Trade") IsNot Nothing) Then

                dsList_Trades = New DataSet
                dsList_Trades = CType(Session("dsList_Trades"), DataSet)

                dsSelect_Trade_AUX = New DataSet
                dsSelect_Trade_AUX = CType(Session("dsSelect_Trade"), DataSet)

                'To validate data
                If Not (Me.ValidateData(dsList_Trades, dsSelect_Trade_AUX)) Then
                    Exit Sub
                End If



                dtList_Trades_AUX = New DataTable
                dtList_Trades_AUX = dsList_Trades.Tables(0).Copy


                'I believe the first record
                mRow = dsList_Trades.Tables(0).NewRow()            
                mRow("TRADE_NUMBER") = dsSelect_Trade_AUX.Tables(0).Rows(0).Item("COMERCIO_NUMERO").ToString.Trim
                mRow("TRADE_NAME") = dsSelect_Trade_AUX.Tables(0).Rows(0).Item("COMERCIO_RAZONSOCIAL").ToString.Trim
                mRow("CONTRIBUYENTE_NOMBRE") = dsSelect_Trade_AUX.Tables(0).Rows(0).Item("CONTRIBUYENTE_NOMBRE").ToString.Trim
                dsList_Trades.Tables(0).Rows.Add(mRow)



                'dsList_Trades.Tables.Add(dtList_Trades_AUX)
                Session("dsList_Trades") = dsList_Trades.Copy



                gvlist_trade.DataSource = dsList_Trades.Tables(0)
                gvlist_trade.DataBind()



                txtnumber_trade.Text = ""
                lblreason_social.Text = ""
                lblcontribuyente.Text = ""
                'txtnumber_trade.Focus()

            End If



            Me.table_select_trade.Visible = True
            Me.table_data_trade.Visible = False
            Me.div_list_trades.Visible = True
        Catch ex As Exception

        End Try



    End Sub

    Protected Sub btn_load_trade_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btn_load_trade.Click
        Call Me.ObtainTrade()
    End Sub


    Private Sub ObtainTrade()
        Dim mXML As String
        Dim dsData As DataSet
        Dim dsData_AUX As DataSet
        Dim dtData As DataTable
        Dim mRow As DataRow



        Try


            'I obtain the data of the trade
            mWS = New ws_tish.Service1
            mXML = mWS.ObtenerDatosComercio(txtnumber_trade.Text.Trim)
            mWS = Nothing


            'I believe the XML on the disc to read it
            dsData = New DataSet
            dsData = clsTools.ObtainDataXML(mXML.ToString.Trim)



            'I create the columns
            dtData = New DataTable
            mRow = dtData.NewRow
            dtData.Columns.Add("COMERCIO_NUMERO")
            dtData.Columns.Add("COMERCIO_RAZONSOCIAL")
            dtData.Columns.Add("CONTRIBUYENTE_NOMBRE")




            'Si el dataset no esta vacio asigno los datos
            If (clsTools.HasData(dsData)) Then
                If (dsData.Tables(0).Rows(0).Item("COMERCIO_ESTADO").ToString.Trim = "OK") Then


                    For i = 0 To dsData.Tables(0).Rows.Count - 1

                        'I believe the first record
                        mRow = dtData.NewRow
                        mRow("COMERCIO_NUMERO") = dsData.Tables(0).Rows(0).Item("COMERCIO_NUMERO").ToString.Trim
                        mRow("COMERCIO_RAZONSOCIAL") = dsData.Tables(0).Rows(0).Item("COMERCIO_RAZONSOCIAL").ToString.Trim
                        mRow("CONTRIBUYENTE_NOMBRE") = dsData.Tables(0).Rows(0).Item("CONTRIBUYENTE_NOMBRE").ToString.Trim
                        dtData.Rows.Add(mRow)

                    Next


                    'I fill the dataset
                    dsData_AUX = New DataSet
                    dsData_AUX.Tables.Add(dtData)
                    Session("dsSelect_Trade") = dsData_AUX.Copy



                    lblreason_social.Text = dsData_AUX.Tables(0).Rows(0).Item("COMERCIO_RAZONSOCIAL").ToString.Trim
                    lblcontribuyente.Text = dsData_AUX.Tables(0).Rows(0).Item("CONTRIBUYENTE_NOMBRE").ToString.Trim


                    div_list_trades.Visible = True
                Else
                    lblreason_social.Text = "Comercio Inactivo."
                    lblcontribuyente.Text = ""
                End If




            Else
                lblreason_social.Text = "Comercio Inexistente."
                lblcontribuyente.Text = ""
            End If




            Me.table_select_trade.Visible = False
            Me.table_data_trade.Visible = True
        Catch ex As Exception
            Me.table_select_trade.Visible = True
            Me.table_data_trade.Visible = False
        End Try

    End Sub


    'LoadGrilla_RecordBlank:
    'This procedure loads the grilla with a row in white
    Private Sub LoadGrilla_RecordBlank()
        Dim dsDatos As DataSet
        Dim dtDatos As DataTable
        Dim mFila As DataRow





        Try

            'I create the columns
            dtDatos = New DataTable
            mFila = dtDatos.NewRow
            dtDatos.Columns.Add("TRADE_NUMBER")
            dtDatos.Columns.Add("TRADE_NAME")
            dtDatos.Columns.Add("CONTRIBUYENTE_NOMBRE")



            'I believe the first record
            mFila = dtDatos.NewRow
            mFila("TRADE_NUMBER") = ""
            mFila("TRADE_NAME") = ""
            mFila("CONTRIBUYENTE_NOMBRE") = ""
            dtDatos.Rows.Add(mFila)


            'I fill the dataset
            dsDatos = New DataSet
            dsDatos.Tables.Add(dtDatos)



            Session("dsList_Trades") = dsDatos.Clone


            'I fill the grilla
            gvlist_trade.DataSource = dsDatos.Tables(0)
            gvlist_trade.DataBind()


            'I clear the link of the first record
            gvlist_trade.Rows(0).Cells(3).Controls.Clear()


        Catch ex As Exception

        End Try
    End Sub


    Private Sub gvlist_trade_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvlist_trade.RowDataBound
        Dim dsDataTrade_AUX As DataSet

        If (e.Row.RowType = DataControlRowType.DataRow) Then

            dsDataTrade_AUX = New DataSet
            dsDataTrade_AUX = CType(Session("dsList_Trades"), DataSet).Copy

            e.Row.Cells(3).Attributes.Add("onclick", "javascript:return delete_trade(" & Me.mPositionTrade.ToString.Trim & ");")
            e.Row.Cells(3).ToolTip = "Eliminar comercio (Número " & dsDataTrade_AUX.Tables(0).Rows(Me.mPositionTrade).Item(0).ToString.Trim() & ")"

            ' Si el comercio no tiene un contribuyente asociado lo resalto en rojo
            If (dsDataTrade_AUX.Tables(0).Rows(Me.mPositionTrade).Item("CONTRIBUYENTE_NOMBRE").ToString.Trim() = "Contribuyente sin asignar") Then
                e.Row.Attributes.CssStyle.Add(HtmlTextWriterStyle.Color, "red")
                e.Row.Attributes.Add("title", "Importante: Este comercio no tiene un contribuyente asociado")
            End If
            Me.mPositionTrade = Me.mPositionTrade + 1


            dsDataTrade_AUX = Nothing
        End If
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btncancel.Click
        Me.lblmessenger_trade.Visible = False
        Me.div_messenger_trade.Visible = False
        Me.txtnumber_trade.Text = ""
        Me.table_select_trade.Visible = True
        Me.table_data_trade.Visible = False
        Me.txtnumber_trade.Focus()
    End Sub

    Protected Sub link_table_add_trade_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_table_add_trade.Click
        Me.table_select_trade.Visible = True
        Me.link_table_add_trade.Visible = False
        Me.txtnumber_trade.Focus()
    End Sub

    'This function delete the selected trade
    Private Sub DeleteTrade(ByVal pPosition As Integer)
        Dim mSelectedRow As Integer
        Dim dsDataTrade_AUX As DataSet


        'I obtain the selected row
        mSelectedRow = Convert.ToInt32(pPosition)

        'I Obtain the dataset
        dsDataTrade_AUX = New DataSet
        dsDataTrade_AUX = CType(Session("dsList_Trades"), DataSet).Copy


        'I delete el record
        dsDataTrade_AUX.Tables(0).Rows.RemoveAt(mSelectedRow)

        'I Update the session variable and I update the grid
        Session("dsList_Trades") = dsDataTrade_AUX
        gvlist_trade.DataSource = dsDataTrade_AUX
        gvlist_trade.DataBind()
    End Sub



    'Validate the trade for add
    Private Function ValidateData(ByVal pdsList_Trades As DataSet, ByVal pdsSelect_Trade_AUX As DataSet) As Boolean

        'To validate trade
        If (Me.lblreason_social.Text = "Comercio Inactivo.") Then
            Me.lblmessenger_trade.Text = "No puede agregar un comercio inactivo."
            Me.lblmessenger_trade.Visible = True
            Me.div_messenger_trade.Visible = True
            Return False
        End If

        If (Me.lblreason_social.Text = "Comercio Inexistente.") Then
            Me.lblmessenger_trade.Text = "No puede agregar un comercio inexistente."
            Me.lblmessenger_trade.Visible = True
            Me.div_messenger_trade.Visible = True
            Return False
        End If


        'to validate existing trade
        Me.lblmessenger_trade.Visible = False
        Me.div_messenger_trade.Visible = False
        For i = 0 To pdsList_Trades.Tables(0).Rows.Count - 1
            If (pdsList_Trades.Tables(0).Rows(i).Item("TRADE_NUMBER").ToString = pdsSelect_Trade_AUX.Tables(0).Rows(0).Item("COMERCIO_NUMERO").ToString.Trim) Then
                Me.lblmessenger_trade.Text = "El comercio seleccionado ya se encuentra relacionado a este usuario Web."
                Me.lblmessenger_trade.Visible = True
                Me.div_messenger_trade.Visible = True
                Return False
            End If
        Next

        Return True
    End Function

    Private Sub CleanTXT()
        Me.txtcuit.Text = ""
        Me.txtreason_social.Text = ""
        Me.txtcontac.Text = ""
        Me.txtaddress.Text = ""
        Me.txttelephone.Text = ""
        Me.txtemail.Text = ""

        Me.div_link_add_trade.Visible = False
        Me.div_list_trades.Visible = False
        Me.btnedit.Visible = False
    End Sub
End Class