﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace entidades
{
    [XmlRootAttribute("rubro", Namespace = "", IsNullable = false)]
    public class rubro {
        private string strCodigo;
        private string strDescripcion;
        private string strAlicuotaActual;
        private string strAlicuotaAnterior;


        public string codigo {
            get { return this.strCodigo.Trim(); }
            set { this.strCodigo = value.Trim(); }
        }

        public string descripcion {
            get { return this.strDescripcion.Trim(); }
            set { this.strDescripcion = value.Trim(); }
        }

        public string alicuotaActual {
            get { return this.strAlicuotaActual.Trim(); }
            set { this.strAlicuotaActual = value.Trim(); }
        }

        public string alicuotaAnterior {
            get { return this.strAlicuotaAnterior.Trim(); }
            set { this.strAlicuotaAnterior = value.Trim(); }
        }



        public rubro() {
            this.strCodigo = "";
            this.strDescripcion = "";
            this.alicuotaActual = "";
            this.alicuotaAnterior = "";
        }

    }
}
