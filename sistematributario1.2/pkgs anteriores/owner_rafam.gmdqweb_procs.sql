DROP PACKAGE OWNER_RAFAM.GMDQWEB_PROCS;

CREATE OR REPLACE PACKAGE OWNER_RAFAM.GMDQWEB_PROCS IS
  AuxCantCuotasSel      NUMBER;
  AuxCantRengCompWeb    NUMBER;
  AuxCantCuotasComp     NUMBER;
  AuxFechaVtoComp       DATE;
  AuxContenidoParam     CONFIG.contenido%TYPE;
  AuxMontoTotalAcum     NUMBER;
  AuxMontoOrigAcum      NUMBER;
  AuxMontoRecAcum       NUMBER;
  AuxMontoIVAAcum       NUMBER;
  --AuxArchComp           VARCHAR2(4000);
  AuxArchComp           CLOB;
  mFECVTOAUX DATE;

  regING_COMPR_CAB_CC   ING_COMPR_CAB_CC%ROWTYPE;
  regING_COMPR_DET_CC   ING_COMPR_DET_CC%ROWTYPE;
  regING_CONTRIBUYENTES ING_CONTRIBUYENTES%ROWTYPE;
  regING_TMP_CC         ING_TMP_CC%ROWTYPE;

  PROCEDURE WEB_EmitirCompPagoDeudaVenc (P_AuxArchComp OUT CLOB,
                                         P_Usuario NUMBER, P_Recurso VARCHAR2);
  PROCEDURE GenComprobNormal   (P_Usuario NUMBER, P_Recurso VARCHAR2);
  PROCEDURE GenComprobPlan     (P_Usuario NUMBER, P_Recurso VARCHAR2);
  PROCEDURE GrabarCabeceraComp (P_TipoComprob NUMBER, P_NroComprob NUMBER, P_Recurso VARCHAR2);
  PROCEDURE GrabarRengComp     (P_TipoComprob NUMBER, P_NroComprob NUMBER, P_OrdenReng NUMBER);
  PROCEDURE ActualCabeceraComp (P_TipoComprob NUMBER, P_NroComprob NUMBER, P_CantReng NUMBER);
END;
/



DROP PACKAGE BODY OWNER_RAFAM.GMDQWEB_PROCS;

CREATE OR REPLACE PACKAGE BODY OWNER_RAFAM.GMDQWEB_PROCS IS

/* Procedimiento que genera los Comprobantes de Pago WEB (23/12/2009) */
PROCEDURE WEB_EmitirCompPagoDeudaVenc (P_AuxArchComp OUT CLOB, P_Usuario NUMBER, P_Recurso VARCHAR2)
IS 
BEGIN

  --Inicializar P_AuxArchComp, variable que contendr� el XML de salida con la info de los comprobantes.
  --Adem�s del par�metro usamos una variable auxiliar global, que al final del procedimiento, si toda
  --la generaci�n del/de los comprob. fue correcta, la asignamos al par�metro.
  SELECT NULL INTO P_AuxArchComp FROM DUAL;
  SELECT NULL INTO AuxArchComp   FROM DUAL;
  

 --P_AuxArchComp := 'PEPE';
 --RETURN;

  --Obtener la cantidad de d�as que deben sumarse a la fecha actual para obtener la fecha de
  --vencimiento del comprobante. Si el par�metro contiene el string "ULTIMO DIA" no se suman los
  --d�as sino que se toma directamente el �ltimo d�a del mes.
  BEGIN
    SELECT TRIM(NVL(Contenido,'0')) INTO AuxContenidoParam FROM CONFIG
     WHERE SISTEMA='Ingresos P�blicos' AND NOMBRE_PARAMETRO ='WEB_CANTDIAS_FECHA_VTO';
        
    IF TRIM(AuxContenidoParam) = 'ULTIMO DIA' THEN
      AuxFechaVtoComp := TRUNC(LAST_DAY(SYSDATE));
    ELSE 
      AuxFechaVtoComp := TRUNC(SYSDATE + TO_NUMBER(AuxContenidoParam));
    END IF;
          
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
      AuxFechaVtoComp := TRUNC(SYSDATE);
  END;

  --Si el usuario seleccion� movimientos NORMALES (o sea, que no son cuotas de un plan de pagos ni
  --cuotas en plan de pago) vencidos de cuenta corriente, generar comprobante/s y agregarlo al XML.
  SELECT COUNT(*) INTO AuxCantCuotasSel FROM ING_TMP_CC WHERE Usuario = P_Usuario AND Marcado = 'S' AND 
                                 Fecha_Vto < TRUNC(SYSDATE) AND Estado = 'S' AND Situacion = 'V' AND
                                 Recurso = TRIM(P_Recurso);
  IF AuxCantCuotasSel > 0 THEN
    GenComprobNormal(P_Usuario, P_Recurso);
  END IF;

  --Si el usuario seleccion� movimientos DE PLAN vencidos de cuenta corriente, generar comprobante/s y
  --agregarlo al XML.
  SELECT COUNT(*) INTO AuxCantCuotasSel FROM ING_TMP_CC WHERE Usuario = P_Usuario AND Marcado = 'S' AND 
       Fecha_Vto < TRUNC(SYSDATE) AND Estado = 'D' AND Situacion IN('V','C','I','G','N') AND Recurso = TRIM(P_Recurso);
  IF AuxCantCuotasSel > 0 THEN
    GenComprobPlan(P_Usuario, P_Recurso);
  END IF;

  COMMIT;

  --Si us�bamos tipo de dato varchar2 para el par�metro P_AuxArchComp muchas veces se generaban
  --errores porque la m�xima cant. de caracteres de un VARCHAR2 es 4000 y los XML generados 
  --normalmente superan ese cantidad. Por eso empezamos a usar tipo de dato CLOB (Character Large
  --object).
  --SELECT LENGTH(TRIM(AuxArchComp)) INTO AuxCantCuotasComp FROM DUAL;

  --Como todo fue OK asignamos el XML al par�metro de salida.
  P_AuxArchComp := TRIM(AuxArchComp);

EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
    SELECT NULL INTO P_AuxArchComp FROM DUAL;
    raise_application_error(-20140,'ERROR: Procedimiento WEB_EMITIRCOMPPAGODEUDAVENC' || SQLCODE || SQLERRM);
END;


--Procedimiento que genera comprobante/s con los movimientos NORMALES vencidos seleccionados.
PROCEDURE GenComprobNormal (P_Usuario NUMBER, P_Recurso VARCHAR2) IS
AuxNroComprob       ING_COMPR_CAB.nro_comprob%TYPE;
AuxTipoCompAct      ING_COMPR_CAB.tipo_comprob%TYPE;
AuxNroCompAct       ING_COMPR_CAB.nro_comprob%TYPE;
AuxCantMovMismoComp NUMBER;

--En el siguiente cursor es important�simo el ordenamiento por tipo y nro. de comprobante original
--porque luego hay que incluir 
CURSOR CursorMovim IS SELECT * FROM ING_TMP_CC WHERE Usuario = P_Usuario AND Marcado = 'S' AND 
     Fecha_Vto < TRUNC(SYSDATE) AND Estado = 'S' AND Situacion IN('V','C','I','G','N') AND Recurso = TRIM(P_Recurso)
     ORDER BY tipo_comprob, nro_comprob, recurso, anio, cuota, orden;

BEGIN
  --Levantar en un cursor todos los movimientos normales seleccionados y recorrerlos uno por uno.
  AuxTipoCompAct    := 0;
  AuxNroCompAct     := 0;
  AuxCantCuotasComp := 0;
  OPEN CursorMovim;
  LOOP
    FETCH CursorMovim INTO regING_TMP_CC;
    EXIT WHEN CursorMovim%NOTFOUND;

    --En RAFAM si dos movimientos de cuenta corriente corresponden a distintos conceptos de un mismo
    --comprobante original, es necesario que ambos movimientos deban incluirse en un mismo comprobante
    --web. Pero cada comprobante web tiene una limitaci�n en cuanto a cantidad de renglones que puede
    --incluir. Debido a esto, al leer un movimiento, necesitamos contar la cantidad de renglones 
    --seleccionados que corresponden al mismo comprobante original y verificar que tengamos suficiente
    --lugar en el nuevo comprobante web que estamos generando para incluir todos estos renglones.
    --Si no hubiera tal espacio tenemos que "cerrar" el comprobante actual y crear uno nuevo.
    --OJO! Este chequeo hay que hacerlo s�lo para el primer rengl�n de cada comprobante.
    IF regING_TMP_CC.tipo_comprob <> AuxTipoCompAct OR regING_TMP_CC.nro_comprob <> AuxNroCompAct THEN
      SELECT COUNT(*) INTO AuxCantMovMismoComp FROM ING_TMP_CC
                     WHERE Usuario = P_Usuario AND Marcado = 'S'   AND Fecha_Vto < TRUNC(SYSDATE) AND
                           Estado = 'S'        AND Situacion IN('V','C','I','G','N') AND 
                           Tipo_Comprob = regING_TMP_CC.tipo_comprob AND
                           Nro_Comprob  = regING_TMP_CC.nro_comprob;
      IF AuxCantMovMismoComp > 1 AND AuxCantCuotasComp > 0 AND
                          AuxCantRengCompWeb - AuxCantCuotasComp < AuxCantMovMismoComp THEN
         ActualCabeceraComp(7, AuxNroComprob, AuxCantCuotasComp);
         AuxCantCuotasComp := 0;
      END IF;

      AuxTipoCompAct := regING_TMP_CC.tipo_comprob;
      AuxNroCompAct  := regING_TMP_CC.nro_comprob;
    END IF;   

    --Incrementar contador de renglones del comprobante.
    AuxCantCuotasComp := AuxCantCuotasComp + 1;

    --Si es el primer rengl�n, grabar la cabecera del comprobante. Los importes totales se
    --inicializan en cero.
    IF AuxCantCuotasComp = 1 THEN
      --Obtener pr�ximo nro. de comprobante
      --Se hace desde ac� y no desde adentro del procedimiento GrabarCabeceraCompNormal porque
      --se necesita saber el nro. de comprobante para luego invocar al procedimiento que graba
      --los renglones del comprobante.
      SELECT numerador + 1 INTO AuxNroComprob FROM OWNER_RAFAM.ING_TIPO_DE_COMPROBANTE WHERE Codigo = 7;
      UPDATE OWNER_RAFAM.ING_TIPO_DE_COMPROBANTE SET Numerador = Numerador + 1 WHERE Codigo = 7;

      GrabarCabeceraComp(7, AuxNroComprob, P_Recurso);
    END IF;

    --Grabar rengl�n del comprobante e ir acumulando los importes del rengl�n en variables
    --auxiliares.
    GrabarRengComp(7, AuxNroComprob, AuxCantCuotasComp);

    --Si se lleg� a la cantidad m�xima de renglones por comprobante, actualizar los importes
    --totales de la cabecera.
    IF AuxCantCuotasComp >= AuxCantRengCompWeb THEN
      ActualCabeceraComp(7, AuxNroComprob, AuxCantCuotasComp);
      AuxCantCuotasComp := 0;
    END IF;
  END LOOP;
  CLOSE CursorMovim;

  --Si qued� un comprobante incompleto, actualizar la cabecera.
  IF AuxCantCuotasComp > 0 THEN
    ActualCabeceraComp(7, AuxNroComprob, AuxCantCuotasComp);
    AuxCantCuotasComp := 0;
  END IF;
  
  AuxArchComp := AuxArchComp || '</COMPROBANTES>' || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '</VFPDATA>'      || CHR(13) || CHR(10);
END;


--Procedimiento que graba UN comprobante con movimientos NORMALES vencidos seleccionados.
PROCEDURE GrabarCabeceraComp (P_TipoComprob NUMBER, P_NroComprob NUMBER, P_Recurso VARCHAR2) IS
AuxNroComprobChar     VARCHAR2(13);
AuxNombLoc            localidades.descripcion%TYPE;
BEGIN
  AuxMontoTotalAcum := 0;
  AuxMontoOrigAcum  := 0;
  AuxMontoRecAcum   := 0;
  AuxMontoIVAAcum   := 0;

  regING_COMPR_CAB_CC.Tipo_Comprob := P_TipoComprob;
  regING_COMPR_CAB_CC.Nro_Comprob  := P_NroComprob;
  AuxNroComprobChar                := TRIM(TO_CHAR(P_NroComprob,'000000000000'));
  regING_COMPR_CAB_CC.Digito_Ver   := CALC_DIG_VERIF_COMPROB(TRIM(AuxNroComprobChar));
  regING_COMPR_CAB_CC.Tipo_Imp     := regING_TMP_CC.Tipo_Imp;
  regING_COMPR_CAB_CC.Nro_Contrib  := regING_TMP_CC.Nro_Contrib;

  IF TRIM(regING_COMPR_CAB_CC.Tipo_Imp) = 'I' THEN
    regING_COMPR_CAB_CC.Nro_Inmueble := regING_TMP_CC.Nro_Imp;
    regING_COMPR_CAB_CC.Nro_Comercio := NULL;
    regING_COMPR_CAB_CC.Nro_Rodado   := NULL;
    regING_COMPR_CAB_CC.Nro_Cuenta   := NULL;
  ELSIF regING_COMPR_CAB_CC.Tipo_Imp = 'C' THEN
    regING_COMPR_CAB_CC.Nro_Inmueble := NULL;
    regING_COMPR_CAB_CC.Nro_Comercio := regING_TMP_CC.Nro_Imp;
    regING_COMPR_CAB_CC.Nro_Rodado   := NULL;
    regING_COMPR_CAB_CC.Nro_Cuenta   := NULL;
  ELSIF regING_COMPR_CAB_CC.Tipo_Imp = 'R' THEN
    regING_COMPR_CAB_CC.Nro_Inmueble := NULL;
    regING_COMPR_CAB_CC.Nro_Comercio := NULL;
    regING_COMPR_CAB_CC.Nro_Rodado   := regING_TMP_CC.Nro_Imp;
    regING_COMPR_CAB_CC.Nro_Cuenta   := NULL;
  ELSIF regING_COMPR_CAB_CC.Tipo_Imp = 'E' THEN
    regING_COMPR_CAB_CC.Nro_Inmueble := NULL;
    regING_COMPR_CAB_CC.Nro_Comercio := NULL;
    regING_COMPR_CAB_CC.Nro_Rodado   := NULL;
    regING_COMPR_CAB_CC.Nro_Cuenta   := regING_TMP_CC.Nro_Imp;
  ELSE 
    regING_COMPR_CAB_CC.Nro_Inmueble := NULL;
    regING_COMPR_CAB_CC.Nro_Comercio := NULL;
    regING_COMPR_CAB_CC.Nro_Rodado   := NULL;
    regING_COMPR_CAB_CC.Nro_Cuenta   := NULL;
  END IF;

  regING_COMPR_CAB_CC.Estado           := 'E';
  regING_COMPR_CAB_CC.Monto_Total      := 0;  --Despu�s de recorrer y grabar todos los renglones se actualizan los importes.
  regING_COMPR_CAB_CC.Fecha_Vto        := AuxFechaVtoComp;
  regING_COMPR_CAB_CC.Monto_IVA        := 0;
  regING_COMPR_CAB_CC.Fecha_Emision    := TRUNC(SYSDATE);
  regING_COMPR_CAB_CC.Unif_Recurso     := TRIM(P_Recurso);
  regING_COMPR_CAB_CC.Unif_Anio        := NULL;
  regING_COMPR_CAB_CC.Unif_Cuota       := NULL;
  regING_COMPR_CAB_CC.Oficio_Nro       := NULL;
  regING_COMPR_CAB_CC.Oficio_Ejercicio := NULL;
  regING_COMPR_CAB_CC.Oficio_Vto       := NULL;

  INSERT INTO ING_COMPR_CAB_CC VALUES regING_COMPR_CAB_CC;

  -- Si todav�a est� vac�o el XML, agregar informaci�n del Imponible y del Contribuyente.
  IF TRIM(AuxArchComp) IS NULL THEN
    BEGIN
      SELECT * INTO regING_CONTRIBUYENTES FROM ING_CONTRIBUYENTES
                   WHERE nro_contrib = regING_TMP_CC.nro_contrib;
      SELECT descripcion INTO AuxNombLoc FROM Localidades
                   WHERE Codigo = regING_CONTRIBUYENTES.dp_cod_loc;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        regING_CONTRIBUYENTES := NULL;
        AuxNombLoc            := NULL;
    END;

	AuxArchComp := AuxArchComp || '<VFPDATA>'        || CHR(13) || CHR(10);
	AuxArchComp := AuxArchComp || '<IMPONIBLE>'      || CHR(13) || CHR(10);
	AuxArchComp := AuxArchComp || '<TIPOIMPONIBLE>'  || TRIM(regING_COMPR_CAB_CC.Tipo_Imp) ||
                                  '</TIPOIMPONIBLE>' || CHR(13) || CHR(10);
	AuxArchComp := AuxArchComp || '<NROIMPONIBLE>'   || TO_CHAR(regING_TMP_CC.Nro_Imp) ||
				                  '</NROIMPONIBLE>'  || CHR(13) || CHR(10);
	AuxArchComp := AuxArchComp || '<TITULAR>'        || TRIM(regING_CONTRIBUYENTES.apynom) ||
                                  '</TITULAR>'       || CHR(13) || CHR(10);
	AuxArchComp := AuxArchComp || '<DOMICILIO>'      || TRIM(regING_CONTRIBUYENTES.dp_calle)  || ' ' ||
                                                        TO_CHAR(regING_CONTRIBUYENTES.dp_nro) || ' ' ||
                                                        TRIM(regING_CONTRIBUYENTES.dp_piso)   || ' ' ||
                                                        TRIM(regING_CONTRIBUYENTES.dp_dept)   ||
                                  '</DOMICILIO>'     || CHR(13) || CHR(10);
	AuxArchComp := AuxArchComp || '<LOCALIDAD> ('    || TO_CHAR(regING_CONTRIBUYENTES.dp_cod_postal) ||
                                                        ') ' || TRIM(AuxNombLoc) ||
                                  '</LOCALIDAD>'     || CHR(13) || CHR(10);
	AuxArchComp := AuxArchComp || '</IMPONIBLE>';
	AuxArchComp := AuxArchComp || '<COMPROBANTES>'   || CHR(13) || CHR(10);
  END IF;

  --Agregar al XML el encabezado del comprobante.
  AuxArchComp   := AuxArchComp || '<COMPROBANTE>'    || CHR(13) || CHR(10);
  AuxArchComp   := AuxArchComp || '<NROCOMP>'        ||
                              LPAD(TRIM(TO_CHAR(regING_COMPR_CAB_CC.tipo_comprob)), 3, '0') || 
                       '/' || LPAD(TRIM(TO_CHAR(regING_COMPR_CAB_CC.nro_comprob)), 12, '0') ||
                       '/' || TO_CHAR(regING_COMPR_CAB_CC.digito_ver) ||
                                   '</NROCOMP>'      || CHR(13) || CHR(10);
  AuxArchComp    := AuxArchComp || '<FECHA_EMISION>' ||
                              TO_CHAR(regING_COMPR_CAB_CC.fecha_emision, 'dd/mm/yyyy') ||
                                   '</FECHA_EMISION>' || CHR(13) || CHR(10);
  AuxArchComp    := AuxArchComp || '<RENGLONES>'      || CHR(13) || CHR(10);
END;


--Procedimiento que graba UN rengl�n de un comprobante con movimientos NORMALES vencidos seleccionados.
PROCEDURE GrabarRengComp (P_TipoComprob NUMBER, P_NroComprob NUMBER, P_OrdenReng NUMBER) IS
BEGIN
  regING_COMPR_DET_CC.Tipo_Comprob     := P_TipoComprob;
  regING_COMPR_DET_CC.Nro_Comprob      := P_NroComprob;
  regING_COMPR_DET_CC.Orden            := P_OrdenReng;
  regING_COMPR_DET_CC.Recurso          := regING_TMP_CC.recurso;
  regING_COMPR_DET_CC.Anio             := regING_TMP_CC.anio;
  regING_COMPR_DET_CC.Cuota            := regING_TMP_CC.cuota;
  regING_COMPR_DET_CC.Monto_Total      := NVL(regING_TMP_CC.monto_total, 0);
  regING_COMPR_DET_CC.Monto_Origen     := NVL(regING_TMP_CC.monto_origen, 0);
  regING_COMPR_DET_CC.Monto_Act        := NVL(regING_TMP_CC.monto_act, 0);
  regING_COMPR_DET_CC.Monto_Interes    := NVL(regING_TMP_CC.monto_int, 0);
  regING_COMPR_DET_CC.Monto_IVA        := NVL(regING_TMP_CC.monto_iva, 0);
  regING_COMPR_DET_CC.Monto_Multa      := NVL(regING_TMP_CC.monto_multa, 0);
  regING_COMPR_DET_CC.Tipo_Comprob_Ori := regING_TMP_CC.tipo_comprob;
  regING_COMPR_DET_CC.Nro_Comprob_Ori  := regING_TMP_CC.nro_comprob;
  regING_COMPR_DET_CC.Orden_CC         := regING_TMP_CC.orden;

  INSERT INTO ING_COMPR_DET_CC VALUES regING_COMPR_DET_CC;

  --Acumular montos
  AuxMontoOrigAcum  := AuxMontoOrigAcum  + regING_TMP_CC.monto_origen;
  AuxMontoRecAcum   := AuxMontoRecAcum   + regING_TMP_CC.monto_act + regING_TMP_CC.monto_int +
                                           regING_TMP_CC.monto_multa;
  AuxMontoIVAAcum   := AuxMontoIVAAcum   + regING_TMP_CC.monto_iva;
  AuxMontoTotalAcum := AuxMontoTotalAcum + regING_TMP_CC.monto_total;

  --Agregar rengl�n a archivo XML.
  AuxArchComp := AuxArchComp || '<RENGLON>'       || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '<RECURSO>'       || TRIM(regING_COMPR_DET_CC.recurso) ||
                               '</RECURSO>'       || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '<ANIO>'          || TRIM(regING_COMPR_DET_CC.anio) ||
                               '</ANIO>'          || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '<CUOTA>'         || TRIM(regING_COMPR_DET_CC.cuota) ||
                               '</CUOTA>'         || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '<CONCEPTO>'      || TRIM(regING_TMP_CC.concepto_cc) ||
                               '</CONCEPTO>'      || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '<CODPLAN>'       || '</CODPLAN>' || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '<IMPORIGENRENG>' ||
                        TRIM(TO_CHAR(regING_COMPR_DET_CC.Monto_Origen, '999999990.99')) ||
                                '</IMPORIGENRENG>' || CHR(13) || CHR(10);

  AuxArchComp := AuxArchComp || '<IMPRECARGOSRENG>'  ||
          TRIM(TO_CHAR(regING_COMPR_DET_CC.monto_act + regING_COMPR_DET_CC.monto_interes +
                  regING_COMPR_DET_CC.monto_iva + regING_COMPR_DET_CC.monto_multa, '999999990.99')) ||
                                '</IMPRECARGOSRENG>' || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '<IMPTOTALRENG>' || 
          TRIM(TO_CHAR(regING_COMPR_DET_CC.monto_total, '999999990.99')) ||
                                '</IMPTOTALRENG>' || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '</RENGLON>' || CHR(13) || CHR(10);
END;


--Procedimiento que graba UN comprobante con movimientos NORMALES vencidos seleccionados.
PROCEDURE ActualCabeceraComp (P_TipoComprob NUMBER, P_NroComprob NUMBER, P_CantReng NUMBER) IS
AuxCodBarra50Nro    VARCHAR2(50);
AuxCodBarra50       VARCHAR2(75);
BEGIN
  IF P_CantReng = 1 THEN
    UPDATE ING_COMPR_CAB_CC SET monto_total = AuxMontoTotalAcum,  monto_iva  = AuxMontoIVAAcum,
                                unif_anio   = regING_TMP_CC.anio, unif_cuota = regING_TMP_CC.cuota
     WHERE tipo_comprob = P_TipoComprob AND nro_comprob = P_NroComprob;
  ELSE
    UPDATE ING_COMPR_CAB_CC SET monto_total = AuxMontoTotalAcum, monto_iva = AuxMontoIVAAcum
     WHERE tipo_comprob = P_TipoComprob AND nro_comprob = P_NroComprob;
  END IF;

  --Agregar al XML otros datos generales del comprobante.
  AuxArchComp := AuxArchComp || '</RENGLONES>' || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '<FECHA_VENCIMIENTO_1>' ||
     TO_CHAR(regING_COMPR_CAB_CC.fecha_vto, 'dd/mm/yyyy') || '</FECHA_VENCIMIENTO_1>' || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '<IMPORTE_TOTAL_ORIGEN1>' ||
     TRIM(TO_CHAR(AuxMontoOrigAcum, '999999990.99')) || '</IMPORTE_TOTAL_ORIGEN1>' || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '<IMPORTE_RECARGO_1>' ||
     TRIM(TO_CHAR(AuxMontoRecAcum, '999999990.99'))  || '</IMPORTE_RECARGO_1>' || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '<IMPORTE_TOTAL_1>' ||
     TRIM(TO_CHAR(AuxMontoTotalAcum, '999999990.99'))  || '</IMPORTE_TOTAL_1>' || CHR(13) || CHR(10);

  AuxArchComp := AuxArchComp || '<FECHA_VENCIMIENTO_2>'     ||
                                '</FECHA_VENCIMIENTO_2>'    || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '<IMPORTE_TOTAL_ORIGEN2>'   ||
           TRIM(TO_CHAR(0, '999999990.99')) || '</IMPORTE_TOTAL_ORIGEN2>'  || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '<IMPORTE_RECARGO_2>'       ||
           TRIM(TO_CHAR(0, '999999990.99')) || '</IMPORTE_RECARGO_2>' || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '<IMPORTE_TOTAL_2>'         ||
           TRIM(TO_CHAR(0, '999999990.99')) || '</IMPORTE_TOTAL_2>'        || CHR(13) || CHR(10);

  AuxArchComp := AuxArchComp || '<FECHA_VENCIMIENTO_3>'     ||
                                '</FECHA_VENCIMIENTO_3>'    || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '<IMPORTE_TOTAL_ORIGEN3>'   ||
           TRIM(TO_CHAR(0, '999999990.99')) || '</IMPORTE_TOTAL_ORIGEN3>'  || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '<IMPORTE_RECARGO_3>'       ||
           TRIM(TO_CHAR(0, '999999990.99')) || '</IMPORTE_RECARGO_3>' || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '<IMPORTE_TOTAL_3>'         ||
           TRIM(TO_CHAR(0, '999999990.99')) || '</IMPORTE_TOTAL_3>'        || CHR(13) || CHR(10);

  AuxCodBarra50Nro := SUBSTR(Ing_Calcula_Cod50(regING_COMPR_CAB_CC.fecha_vto,
                       AuxMontoTotalAcum, NULL, NULL, regING_COMPR_CAB_CC.tipo_comprob,
                       regING_COMPR_CAB_CC.nro_comprob, 3), 1, 50);

  /*
  AuxCodBarra50   := SUBSTR(Ing_Calcula_Cod50(regING_COMPR_CAB_CC.fecha_vto,
                       regING_COMPR_CAB_CC.monto_total, NULL, NULL, regING_COMPR_CAB_CC.tipo_comprob,
                       regING_COMPR_CAB_CC.nro_comprob, 3, 'S'), 51, 75);
  */

  AuxArchComp := AuxArchComp || '<STRINGCODBARRA>' || TRIM(AuxCodBarra50Nro) ||
                                '</STRINGCODBARRA>' || CHR(13) || CHR(10);

  AuxArchComp := AuxArchComp || '</COMPROBANTE>' || CHR(13) || CHR(10);
END;

--Procedimiento que genera comprobante/s con los movimientos de cuotas vencidas de PLANES seleccionados.
PROCEDURE GenComprobPlan (P_Usuario NUMBER, P_Recurso VARCHAR2) IS
AuxNroComprob       ING_COMPR_CAB.nro_comprob%TYPE;
AuxTipoCompAct      ING_COMPR_CAB.tipo_comprob%TYPE;
AuxNroCompAct       ING_COMPR_CAB.nro_comprob%TYPE;
AuxCantMovMismoComp NUMBER;

CURSOR CursorMovim IS SELECT * FROM ING_TMP_CC WHERE Usuario = P_Usuario AND Marcado = 'S' AND 
     Fecha_Vto < TRUNC(SYSDATE) AND Estado = 'D' AND Situacion IN('V','C','I','G','N') AND Recurso = TRIM(P_Recurso);

BEGIN
  --Levantar en un cursor todos los movimientos de plan seleccionados y recorrerlos uno por uno.
  --Cada cuota de plan se imprimir� en un comprobante distinto.
  AuxTipoCompAct    := 0;
  AuxNroCompAct     := 0;
  AuxCantCuotasComp := 0;
  OPEN CursorMovim;
  LOOP
    FETCH CursorMovim INTO regING_TMP_CC;
    EXIT WHEN CursorMovim%NOTFOUND;

    --En RAFAM si dos movimientos de cuenta corriente corresponden a distintos conceptos de un mismo
    --comprobante original, es necesario que ambos movimientos deban incluirse en un mismo comprobante
    --web. Pero cada comprobante web tiene una limitaci�n en cuanto a cantidad de renglones que puede
    --incluir. Debido a esto, al leer un movimiento, necesitamos contar la cantidad de renglones 
    --seleccionados que corresponden al mismo comprobante original y verificar que tengamos suficiente
    --lugar en el nuevo comprobante web que estamos generando para incluir todos estos renglones.
    --Si no hubiera tal espacio tenemos que "cerrar" el comprobante actual y crear uno nuevo.
    IF regING_TMP_CC.tipo_comprob <> AuxTipoCompAct OR regING_TMP_CC.nro_comprob <> AuxNroCompAct THEN
      SELECT COUNT(*) INTO AuxCantMovMismoComp FROM ING_TMP_CC
                     WHERE Usuario = P_Usuario AND Marcado = 'S'   AND Fecha_Vto < TRUNC(SYSDATE) AND
                           Estado = 'D'        AND Situacion IN('V','C','I','G','N') AND 
                           Tipo_Comprob = regING_TMP_CC.tipo_comprob AND
                           Nro_Comprob  = regING_TMP_CC.nro_comprob;
      IF AuxCantMovMismoComp > 1 AND AuxCantCuotasComp > 0 AND
                          AuxCantRengCompWeb - AuxCantCuotasComp < AuxCantMovMismoComp THEN
         ActualCabeceraComp(8, AuxNroComprob, AuxCantCuotasComp);
         AuxCantCuotasComp := 0;
      END IF;

      AuxTipoCompAct := regING_TMP_CC.tipo_comprob;
      AuxNroCompAct  := regING_TMP_CC.nro_comprob;
    END IF;

    --Incrementar contador de renglones del comprobante.
    AuxCantCuotasComp := AuxCantCuotasComp + 1;

    --Si es el primer rengl�n, grabar la cabecera del comprobante. Los importes totales se
    --inicializan en cero.
    IF AuxCantCuotasComp = 1 THEN
      --Obtener pr�ximo nro. de comprobante
      --Se hace desde ac� y no desde adentro del procedimiento GrabarCabeceraCompNormal porque
      --se necesita saber el nro. de comprobante para luego invocar al procedimiento que graba
      --los renglones del comprobante.
      SELECT numerador + 1 INTO AuxNroComprob FROM OWNER_RAFAM.ING_TIPO_DE_COMPROBANTE WHERE Codigo = 8;
      UPDATE OWNER_RAFAM.ING_TIPO_DE_COMPROBANTE SET Numerador = Numerador + 1 WHERE Codigo = 8;

      GrabarCabeceraComp(8, AuxNroComprob, P_Recurso);
    END IF;

    --Grabar rengl�n del comprobante e ir acumulando los importes del rengl�n en variables
    --auxiliares.
    GrabarRengComp(8, AuxNroComprob, AuxCantCuotasComp);

    --Si se lleg� a la cantidad m�xima de renglones por comprobante, actualizar los importes
    --totales de la cabecera.
    --Por ahora las cuotas de planes se imprimen una en cada comprobante.
    IF AuxCantCuotasComp >= AuxCantRengCompWeb OR AuxCantCuotasComp = 1 THEN
      ActualCabeceraComp(8, AuxNroComprob, AuxCantCuotasComp);
      AuxCantCuotasComp := 0;
    END IF;
  END LOOP;
  CLOSE CursorMovim;

  --Si qued� un comprobante incompleto, actualizar la cabecera.
  IF AuxCantCuotasComp > 0 THEN
    ActualCabeceraComp(8, AuxNroComprob, AuxCantCuotasComp);
  END IF;
  
  AuxArchComp := AuxArchComp || '</COMPROBANTES>' || CHR(13) || CHR(10);
  AuxArchComp := AuxArchComp || '</VFPDATA>'      || CHR(13) || CHR(10);
END;

END;
/


