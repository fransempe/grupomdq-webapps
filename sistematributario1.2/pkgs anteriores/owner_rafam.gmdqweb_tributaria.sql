DROP PACKAGE OWNER_RAFAM.GMDQWEB_TRIBUTARIO_PKG;

CREATE OR REPLACE PACKAGE OWNER_RAFAM.GMDQWEB_TRIBUTARIO_PKG IS

    -- Variables Publicas
    TYPE mCursorGenerico IS REF CURSOR;

  
  
    /* Este procedimiento llena la tabla de USUARIOS WEB para usar desde la WEB */
    PROCEDURE CREAR_TABLA_USUARIOS_WEB;


    -- Genero la Deuda del Contribuyente
    PROCEDURE GENERAR_DEUDA (mNroCuenta IN INTEGER, mTipoCuenta IN CHAR,
                             mNroUsuario IN OUT NUMBER, mCantidadRegistros IN OUT NUMBER);


    -- Obtengo Datos de la Municipalidad
    FUNCTION OBTENER_DATOS_MUNICIPALIDAD RETURN mCursorGenerico;


    -- Obtengo la Deuda del contribuyente
    -- mTipoDeuda V = Vencida; P = Proxima 60 dias; O = Otros
    PROCEDURE OBTENER_DEUDA (mNroUsuario IN NUMBER, mCursorRecursos IN OUT mCursorGenerico,
                             mCursorDeudaVencida IN OUT mCursorGenerico,
                             mCursorTotalDeudaVencida IN OUT mCursorGenerico,
                             mCursorDeudaProxima IN OUT mCursorGenerico,
                             mCursorDeudaOtra IN OUT mCursorGenerico);


    /* Este Procedimiento Limpia la Deuda de la TABLA TEMPORAL ING_TMP_CC y Libera al USUARIO */
    PROCEDURE LIMPIAR_DATOS_SESSION (mNroUsuario IN NUMBER);


    /* Este Procedimiento marca en la TABLA temporal ING_TMP_CC los registros seleccionados */
    FUNCTION MARCAR_REGISTROS_SELECCIONADOS (mNroUsuario IN NUMBER, mRecurso IN VARCHAR2, mTipo_Y_NroComprobante IN VARCHAR2) RETURN VARCHAR2;


    /* Este Procedimiento marca en la TABLA temporal ING_TMP_CC los registros seleccionados */
    FUNCTION EMITIR_COMPROB_NO_VENCIDO (mNroUsuario IN NUMBER) RETURN CLOB;


    /* Obtengo el Nombre de Contribuyente */
    FUNCTION OBTENER_NOMBRE_CONTRIBUYENTE (mNroCuenta IN NUMBER, mTipoCuenta IN VARCHAR) RETURN VARCHAR;

    /*Esta Funcion Devuelve los datos del CONTRIBUYENTE para mostrar en el PDF de INFORME DE DEUDA */
    FUNCTION OBTENER_DATOS_CONTRIBUYENTE (mNroUsuario IN NUMBER) RETURN VARCHAR2;

    /* Esta Funcion Devuelve el Logo y Datos para la Impresion de los PDF */
    FUNCTION OBTENER_LOGO RETURN mCursorGenerico;
    
    /* Esta Funcion Devuelve el CODIGO de la MUNICIPALIDAD para generar el BARCODE39 */
    FUNCTION OBTENER_CODIGO_MUNICIPALIDAD RETURN VARCHAR2;
    
    /* Esta Funcion Devuelve el NUMERO de CUENTA en base al DOMINIO ingresado */
    FUNCTION OBTENER_RODADO_POR_DOMINIO(mDominio IN VARCHAR2) RETURN NUMBER;
    
    /*Esta funcion devuelve el la fecha a la que se calcularan los interes de la cta. cte. a mostrar */
    FUNCTION OBTENER_FECHA_ACTUA_WEB RETURN VARCHAR2;

    FUNCTION ES_PLAN_DE_PAGO (pComprobanteTipo IN NUMBER, pComprobNro IN NUMBER) RETURN VARCHAR;
    
    /* Esta funcion agrega una visita realizada realizada a traves de la Web */
    FUNCTION AGREGAR_VISITA (p_varFecha IN VARCHAR2, p_varHora IN VARCHAR2, p_chrTipoImponible IN CHAR, p_numNroImponible IN NUMBER) RETURN VARCHAR2;
        
    /* Esta funcion actualiza una visita cuando se imprime un comprobante */
    FUNCTION ACTUALIZAR_VISITA (p_varFecha IN VARCHAR2, p_varHora IN VARCHAR2, p_chrTipoImponible IN CHAR, p_numNroImponible IN NUMBER) RETURN VARCHAR2;
    
    /* Esta funcion devuelve el numero total de visitas a la Web */
    FUNCTION GET_VISITAS_CANTIDAD RETURN VARCHAR2;

    /* Esta funcion devuelve la cantidad de visitas a la Web agrupada por tipo de imponible y si imprimio o no */
    FUNCTION GET_VISITAS_CANTIDAD_DETALLE (p_varFechaDesde IN VARCHAR2, p_varFechaHasta IN VARCHAR2) RETURN VARCHAR2;
    
    /*Esta funcion devuelve la cadena de caracteres de los tipos de imponible a mostrar*/
    FUNCTION OBTENER_TIPOS_IMPONIBLE RETURN VARCHAR2;
    
        /* Esta Funcion Devuelve los datos de un rodado para mostrar por la WEB */
    FUNCTION OBTENER_DATOS_RODADO (p_numNroVehiculo IN NUMBER) RETURN mCursorGenerico;
    
    
END;
/



DROP PACKAGE BODY OWNER_RAFAM.GMDQWEB_TRIBUTARIO_PKG;

CREATE OR REPLACE PACKAGE BODY OWNER_RAFAM.GMDQWEB_TRIBUTARIO_PKG IS



/* ###################################################################################################################### */
/* ######################################### INICIO FUNCIONES PRIVADAS ################################################## */

------------------------------------------ INICIO FUNCION ---------------------------------------
/* Esta Funcion genera un caracter entre comillas simples para poder concatenar una consulta */
FUNCTION AGREGAR_COMILLAS(mCadena VARCHAR2) RETURN VARCHAR2 IS
    BEGIN
        RETURN CHR(39) || mCadena || CHR(39);
END;
------------------------------------------ FIN FUNCION ------------------------------------------




------------------------------------------ INICIO FUNCION ---------------------------------------
/* Esta Funcion devuelve los nombre de TABLAS o los CAMPOS segun los parametros usados para armar una Consulta */
FUNCTION OBTENER_DATOS_SQL(mTipoCuenta IN CHAR, mTipoDato IN CHAR, mNroDato INTEGER) RETURN VARCHAR2 IS
    mCadenaDato VARCHAR2(100);


    BEGIN

        IF (mTipoDato = 'T') THEN
            CASE mTipoCuenta
                 WHEN 'I' THEN
                    mCadenaDato := 'ING_INMUEBLES?ING_CON_INMUEBLES�ING_CC_INM';
                 WHEN 'C' THEN
                    mCadenaDato :='ING_COMERCIOS?ING_CON_COMERCIOS�ING_CC_COM';
                 WHEN 'R' THEN
                    mCadenaDato :='ING_RODADOS?ING_CON_RODADOS�ING_CC_ROD';
                 WHEN 'E' THEN
                    mCadenaDato :='ING_CEMENTERIO?ING_CON_CEMENTERIO�ING_CC_CEM';
            END CASE;

        ELSE
            CASE mTipoCuenta
                 WHEN 'I' THEN
                    RETURN 'nro_inmueble';
                 WHEN 'C' THEN
                    RETURN 'nro_comercio';
                 WHEN 'R' THEN
                    RETURN 'nro_rodado';
                 WHEN 'E' THEN
                    RETURN 'nro_cuenta';
            END CASE;
        END IF;



        CASE mNroDato
            WHEN 1 THEN
                DBMS_OUTPUT.PUT_LINE('Dato1: ' || TRIM(SUBSTR(mCadenaDato, 0, (INSTR(mCadenaDato, '?') - 1))));
                RETURN TRIM(SUBSTR(mCadenaDato, 0, (INSTR(mCadenaDato, '?') - 1)));
            WHEN 2 THEN
                DBMS_OUTPUT.PUT_LINE('Dato2: ' || SUBSTR(mCadenaDato,(INSTR(mCadenaDato, '?') + 1), ((INSTR(mCadenaDato, '�')) - (INSTR(mCadenaDato, '?') + 1))));
                RETURN SUBSTR(mCadenaDato,(INSTR(mCadenaDato, '?') + 1), ((INSTR(mCadenaDato, '�')) - (INSTR(mCadenaDato, '?') + 1)));
            WHEN 3 THEN
                DBMS_OUTPUT.PUT_LINE('Dato3: ' || TRIM(SUBSTR(mCadenaDato, (INSTR(mCadenaDato, '�') + 1))));
                RETURN TRIM(SUBSTR(mCadenaDato, (INSTR(mCadenaDato, '�') + 1)));
            ELSE
                DBMS_OUTPUT.PUT_LINE('Dato vacio: ' || TRIM(SUBSTR(mCadenaDato, (INSTR(mCadenaDato, '�') + 1))));
                RETURN '';
        END CASE;

END;
------------------------------------------ FIN FUNCION ------------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
/* Esta funcion OBTIENE el NOMBRE Y APELLIDO del Contribuyente en base a el NRO. de IMPONIBLE */
FUNCTION OBTENER_NRO_CONTRIBUYENTE(mNroCuenta IN NUMBER, mTipoCuenta IN VARCHAR2) RETURN NUMBER IS

    mNroContribuyente INTEGER;
    mDetalleError VARCHAR2(100);
    mTabla_ING VARCHAR2(20);
    mTabla_ING_CON VARCHAR2(20);
    mCampo VARCHAR2(20);
    mSQL VARCHAR2(1000);


    BEGIN

        -- Obtengo el Nombre de las Tablas
        -- Parametros -> 1: Tipo de Cuenta; 2:Tabla|Campo; 3:Posicion del dato
        mTabla_ING := OBTENER_DATOS_SQL(mTipoCuenta, 'T', 1);
        mTabla_ING_CON := OBTENER_DATOS_SQL(mTipoCuenta, 'T', 2);
        mCampo := OBTENER_DATOS_SQL(mTipoCuenta, 'C', 0);

        -- Genero la SENTENCIA SQL
        mSQL := 'SELECT ING_CONTRIBUYENTES.nro_contrib
                FROM ' || mTabla_ING || ', ' || mTabla_ING_CON || ', ING_CONTRIBUYENTES, ING_MOT_BAJ_IMPONIBLES
                WHERE ' || mTabla_ING_CON  || '.' || mCampo || ' = ' || mTabla_ING || '.'|| mCampo ||'
                AND ' || mTabla_ING_CON || '.titular = ' || AGREGAR_COMILLAS('S')||  '
                AND ING_CONTRIBUYENTES.nro_contrib = ' || mTabla_ING_CON || '.nro_contrib
                AND ' || mTabla_ING || '.mot_baja = ING_MOT_BAJ_IMPONIBLES.codigo(+)
                AND ' || mTabla_ING || '.' || mCampo || ' = ' || mNroCuenta;


        -- EJECUTO el SQL
        BEGIN
            mNroContribuyente := -1;
            EXECUTE IMMEDIATE mSQL INTO mNroContribuyente;
        EXCEPTION WHEN NO_DATA_FOUND THEN
            mNroContribuyente := 0;
        END;

        RETURN mNroContribuyente;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
            RETURN -1;
    END;
------------------------------------------ FIN FUNCION ---------------------------------------




------------------------------------------ INICIO FUNCION ---------------------------------------
/* Esta Funcion Agrega los registros del Contribuyente en la TABLA TEMPORAL ING_TMP_CC */
FUNCTION INSERT_ING_TEMP_CC(mNroCuenta IN INTEGER, mTipoCuenta IN VARCHAR2,
                            mNroContribuyente IN INTEGER, mNroUsuario IN INTEGER) RETURN INTEGER IS

    mDetalleError VARCHAR2(100);
    mTabla_ING VARCHAR2(20);
    mTabla_ING_CON VARCHAR2(20);
    mTabla_ING_CC VARCHAR2(20);
    mCampo_ING_CC VARCHAR(20);
    mSQL VARCHAR2(10000);
    mOK INTEGER;


    BEGIN

        mOK := -1;
        mTabla_ING := OBTENER_DATOS_SQL(mTipoCuenta, 'T', 1);
        mTabla_ING_CON := OBTENER_DATOS_SQL(mTipoCuenta, 'T', 2);
        mTabla_ING_CC := OBTENER_DATOS_SQL(mTipoCuenta, 'T', 3);

        mCampo_ING_CC := OBTENER_DATOS_SQL(mTipoCuenta, 'C', 1);



        -- Genero la SENTENCIA SQL
        mSQL := 'INSERT INTO ING_TMP_CC
                SELECT ' || mNroUsuario || ' USUARIO,
                CC.recurso, CC.anio, CC.Cuota, '|| AGREGAR_COMILLAS(mTipoCuenta) || ' Imponible, CC.' || mCampo_ING_CC || ' Nro_impo, CC.orden, (CC.anio*1000 + CC.cuota) Periodo, CC.concepto_cc, CCTE.descripcion DesacCtaCte,
                CC.monto_original,0,0,0, CC.fecha_vto, CC.tipo_comprob, CC.nro_comprob,
                ' || mNroContribuyente || ' Nro_Contrib,
                CCTE.descr_abrev DescCtaCteAbrev, R.descripcion, CC.SITUACION, CC.estado, 0, CC.nro_plan, CC.descr_abrev, '|| AGREGAR_COMILLAS('N') || ', 0, CO.observaciones
                FROM ' || mTabla_ING_CC || ' CC, ING_RECURSOS_CTOS_CC CCTE, ' || mTabla_ING_CON || ' C, ING_RECURSOS R, ING_COMPR_OBS CO
                WHERE C.nro_contrib = ' || mNroContribuyente || '
                AND CC.'|| mCampo_ING_CC || ' = ' || mNroCuenta || '
                AND C.titular = '|| AGREGAR_COMILLAS('S') || '
                AND CC.' || mCampo_ING_CC || '= C.' || mCampo_ING_CC || '
                AND CC.concepto_cc =CCTE.concepto_cc
                AND CC.recurso = CCTE.recurso
                AND CC.recurso = R.recurso
                AND CC.estado <> '|| AGREGAR_COMILLAS('C') || '
                AND CC.tipo_comprob = CO.tipo_comprob(+)
                AND CC.nro_comprob = CO.tipo_comprob(+)';


                -- EJECUTO la SENTENCIA
                BEGIN
                    EXECUTE IMMEDIATE mSQL;
--                EXCEPTION WHEN NO_DATA_FOUND THEN
                EXCEPTION WHEN OTHERS THEN
                    mOK := -1;
                END;

        mOK := 1;
        RETURN mOK;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
            RETURN -1;
    END;
------------------------------------------ FIN FUNCION ---------------------------------------



------------------------------------------ INICIO STORE ---------------------------------------
/* Este Procedimiento GENERA las ACTUALIZACIONES en la TABLA TEMPORAL ING_TMP_CC */
PROCEDURE CALCULAR_ACTUALIZACIONES(mNroUsuario NUMBER, mFechaPago DATE, mTabla VARCHAR2) IS
    mDetalleError VARCHAR2(100);
    mError VARCHAR2(4000);

    BEGIN

        ING_CALC_ACTUALIZACION(mNroUsuario, mFechaPago, mTabla, mError);
        IF (mError = '*') THEN
            DBMS_OUTPUT.PUT_LINE('Calcular actualizacion OK');
        ELSE
            DBMS_OUTPUT.PUT_LINE('Error calcular actualizacion: ' || mError );
        END IF;

    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
    END;
------------------------------------------ FIN STORE ---------------------------------------



------------------------------------------ INICIO STORE ---------------------------------------
/* Este Procedimiento GENERA los INTERESES en la TABLA TEMPORAL ING_TMP_CC */
PROCEDURE CALCULAR_INTERESES(mNroUsuario NUMBER, mFechaPago DATE, mTabla VARCHAR2) IS
    mDetalleError VARCHAR2(100);
    mError VARCHAR2(10);

    BEGIN

        ING_CALC_INTERESES(mNroUsuario, mFechaPago, mTabla, mError);
        IF (mError = '*') THEN
            DBMS_OUTPUT.PUT_LINE('Calcular interes OK');
        ELSE
            DBMS_OUTPUT.PUT_LINE('Error calcular interes: ' || mError );
        END IF;

    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
    END;
------------------------------------------ FIN STORE ---------------------------------------



------------------------------------------ INICIO STORE ---------------------------------------
/* Este Procedimiento GENERA el IVA Y El TOTAL en la TABLA TEMPORAL ING_TMP_CC */
PROCEDURE CALCULAR_IVA(mNroUsuario NUMBER, mTabla VARCHAR2) IS
    mDetalleError VARCHAR2(100);


    BEGIN

        ING_CALC_IVA(mNroUsuario, mTabla);
        DBMS_OUTPUT.PUT_LINE('Calcular iva OK');

    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
    END;
------------------------------------------ FIN STORE ---------------------------------------



------------------------------------------ INICIO STORE ---------------------------------------
/* Este Procedimiento GENERA las MULTAS en la TABLA TEMPORAL ING_TMP_CC */
PROCEDURE CALCULAR_MULTA(mNroUsuario NUMBER, mTabla VARCHAR2) IS
    mDetalleError VARCHAR2(100);
    mError VARCHAR2(4000);

    BEGIN

        -- ING_CALCULO_MULTA(mNroUsuario, mTabla, mError);
        IF (mError = '*') THEN
            DBMS_OUTPUT.PUT_LINE('Calcular multa OK');
        ELSE
            DBMS_OUTPUT.PUT_LINE('Error calcular multa: ' || mError );
        END IF;


    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
    END;
------------------------------------------ FIN STORE ---------------------------------------



------------------------------------------ INICIO STORE ---------------------------------------
/* Este Procedimiento GENERA las ACTUALIZACIONES de los PLANES DE PAGO en la TABLA TEMPORAL ING_TMP_CC */
PROCEDURE CALCULAR_ACTUALIZ_PLAN_PAGO(mNroUsuario NUMBER, mTabla VARCHAR2) IS
    mDetalleError VARCHAR2(100);

    BEGIN
        ING_CTA_CTE_ACTUAL_PPAGO(mNroUsuario, mTabla);
        DBMS_OUTPUT.PUT_LINE('Calcular actualizacion de plan de pago OK');

    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
    END;
------------------------------------------ FIN STORE ---------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
/* Este Funcion S o N si el comprobante es un plan de pago */
FUNCTION ES_PLAN_DE_PAGO (pComprobanteTipo IN NUMBER, pComprobNro IN NUMBER) RETURN VARCHAR IS
    mDetalleError VARCHAR2(100);
    mES_PLAN VARCHAR2(1);
    mSQL VARCHAR(200);


    BEGIN

        --IF (pComprobanteTipo <> 16) THEN
        --    mES_PLAN := 'N';
        --ELSE
            -- Genero la SENTENCIA SQL
         --   mSQL := 'SELECT CASE  WHEN (ANIO = 0) THEN '|| AGREGAR_COMILLAS('S') || ' ELSE '|| AGREGAR_COMILLAS('N') || ' END AS esPLAN
         --           FROM ING_COMPR_CAB
         --           WHERE TIPO_COMPROB = 16
         --           AND NRO_COMPROB = ' || TO_CHAR(pComprobNro);

         --   DBMS_OUTPUT.PUT_LINE(mSQL);

            -- EJECUTO el SQL
         --  BEGIN
         --       mES_PLAN := '';
         --       EXECUTE IMMEDIATE mSQL INTO mES_PLAN;
         --   EXCEPTION WHEN NO_DATA_FOUND THEN
         --       mES_PLAN := 'N';
         --   END;
        --END IF;

        IF pComprobanteTipo = 16 THEN
            mES_PLAN := 'S';
        ELSE
            mES_PLAN := 'N';
        END IF;

        RETURN mES_PLAN;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
           RETURN '';
    END;
------------------------------------------ FIN FUNCION ---------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
/* Este Funcion Devuelve el Nombre del Contribuyente */
FUNCTION OBTENER_NOMBRE_CONTRIBUYENTE (mNroCuenta IN NUMBER, mTipoCuenta IN VARCHAR) RETURN VARCHAR IS
    mNombreContribuyente VARCHAR(100);
    mDetalleError VARCHAR2(100);
    mTabla_ING VARCHAR2(20);
    mTabla_ING_CON VARCHAR2(20);
    mCampo VARCHAR2(20);
    mSQL VARCHAR2(1000);


    BEGIN

        -- Obtengo el Nombre de las Tablas
        -- Parametros -> 1: Tipo de Cuenta; 2:Tabla|Campo; 3:Posicion del dato
        mTabla_ING := OBTENER_DATOS_SQL(mTipoCuenta, 'T', 1);
        mTabla_ING_CON := OBTENER_DATOS_SQL(mTipoCuenta, 'T', 2);
        mCampo := OBTENER_DATOS_SQL(mTipoCuenta, 'C', 0);


        -- Genero la SENTENCIA SQL
        mSQL := 'SELECT ING_CONTRIBUYENTES.apynom NOMBRE_APELLIDO_CONTRIBUYENTE
                FROM ' || mTabla_ING || ', ' || mTabla_ING_CON || ', ING_CONTRIBUYENTES, ING_MOT_BAJ_IMPONIBLES
                WHERE ' || mTabla_ING_CON  || '.' || mCampo || ' = ' || mTabla_ING || '.'|| mCampo ||'
                AND ' || mTabla_ING_CON || '.titular = ' || AGREGAR_COMILLAS('S')||  '
                AND ING_CONTRIBUYENTES.nro_contrib = ' || mTabla_ING_CON || '.nro_contrib
                AND ' || mTabla_ING || '.mot_baja = ING_MOT_BAJ_IMPONIBLES.codigo(+)
                AND ' || mTabla_ING || '.' || mCampo || ' = ' || mNroCuenta;


        -- EJECUTO el SQL
        BEGIN
            mNombreContribuyente := '';
            EXECUTE IMMEDIATE mSQL INTO mNombreContribuyente;
        EXCEPTION WHEN NO_DATA_FOUND THEN
            mNombreContribuyente := 'Cuenta Inexistente';
        END;


        RETURN mNombreContribuyente;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
           RETURN '';
    END;
------------------------------------------ FIN FUNCION ---------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
/* Esta Funcion Devuelve el COONTENIDO segun el PARAMETRO UTILIZADO */
FUNCTION OBTENER_PARAMETRO_CONFIG (mNombreParametro IN VARCHAR2) RETURN VARCHAR2 IS
    mDetalleError VARCHAR2(100);
    mValorParametro VARCHAR2(100);

    CURSOR mCursorDatoParametro IS SELECT TRIM(CONTENIDO) AS VALOR
                                   FROM CONFIG
                                   WHERE SISTEMA = 'Ingresos P�blicos'
                                   AND NOMBRE_PARAMETRO IN (mNombreParametro);


    BEGIN

        -- Obtengo Datos del PARAMETRO
        OPEN mCursorDatoParametro;
        FETCH mCursorDatoParametro INTO mValorParametro;
        CLOSE mCursorDatoParametro;


    RETURN mValorParametro;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
            RETURN 'ERROR';
    END;
------------------------------------------ FIN FUNCION ---------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
/* Esta Funcion Devuelve la etiqueta IMPONIBLE con los datos cargados */
FUNCTION OBTENER_ETIQUETA_IMPONIBLE (mNroUsuario IN NUMBER) RETURN VARCHAR2 IS
    mDetalleError VARCHAR2(100);
    mXML_AUX VARCHAR2(4999);
    mNroContribuyente NUMBER;
    mTipoImponible VARCHAR2(5);
    mNroImponible VARCHAR(20);
    mTitular VARCHAR2(100);
    mDomicilio VARCHAR2(100);
    mLocalidad VARCHAR2(100);




    CURSOR mCursorImponible IS SELECT NRO_CONTRIB, TRIM(TIPO_IMP) AS TIPO_IMPONIBLE, NRO_IMP AS NRO_IMPONIBLE
                               FROM ING_TMP_CC
                               WHERE USUARIO = mNroUsuario
                               AND MARCADO = 'S'
                               AND ROWNUM = 1;


    CURSOR mCursorContribuyente (mNroContribuyente_AUX IN NUMBER) IS
                                 SELECT ING_CONTRIBUYENTES.APYNOM AS TITULAR,
                                 (TRIM(ING_CONTRIBUYENTES.DP_CALLE) || ' ' || TO_CHAR(ING_CONTRIBUYENTES.DP_NRO) || ' ' ||
                                 TRIM(ING_CONTRIBUYENTES.DP_PISO) || ' ' || TRIM(ING_CONTRIBUYENTES.DP_DEPT)) AS DOMICILIO,
                                 ('(' || LOCALIDADES.CODIGO || ') ' ||  TRIM(LOCALIDADES.DESCRIPCION)) AS LOCALIDAD
                                 FROM ING_CONTRIBUYENTES, LOCALIDADES
                                 WHERE ING_CONTRIBUYENTES.NRO_CONTRIB = mNroContribuyente_AUX
                                 AND ING_CONTRIBUYENTES.COD_LOC = LOCALIDADES.CODIGO;





    BEGIN

        -- Obtengo Datos del IMPONIBLE
        OPEN mCursorImponible;
        FETCH mCursorImponible INTO mNroContribuyente, mTipoImponible, mNroImponible;
        CLOSE mCursorImponible;


        -- Obtengo Datos del CONTRIBUYENTE
        OPEN mCursorContribuyente(mNroContribuyente);
        FETCH mCursorContribuyente INTO mTitular, mDomicilio, mLocalidad;
        CLOSE mCursorContribuyente;



        -- Genero la Etiqueta
        mXML_AUX := '<IMPONIBLE>' || CHR(13) || CHR(10) ||
		                  '<TIPOIMPONIBLE>' || mTipoImponible || '</TIPOIMPONIBLE>' || CHR(13) || CHR(10) ||
    		              '<NROIMPONIBLE>' || mNroImponible || '</NROIMPONIBLE>' || CHR(13) || CHR(10) ||
	      	              '<TITULAR>' || mTitular || '</TITULAR>' || CHR(13) || CHR(10) ||
	       	              '<DOMICILIO>' || mDomicilio || '</DOMICILIO>' || CHR(13) || CHR(10) ||
    	       	          '<LOCALIDAD>' || mLocalidad || '</LOCALIDAD>' || CHR(13) || CHR(10) ||
                	'</IMPONIBLE>' || CHR(13) || CHR(10);



    RETURN mXML_AUX;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
            RETURN 'ERROR';
    END;
------------------------------------------ FIN FUNCION ---------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
/* Esta Funcion Devuelve la etiqueta COMPROBANTES con los datos cargados */
FUNCTION OBTENER_ETIQUETA_COMPROBANTES (mNroUsuario IN NUMBER) RETURN CLOB IS
    mEsPlan VARCHAR2(1);

    -- ESTRUCTURA de los COMPROBANTES SELECCIONADOS
    TYPE rComprobantes IS RECORD
        (
            TIPO_COMPROBANTE VARCHAR2(100),
            NRO_COMPROBANTE VARCHAR2(100),
            DIGITO_VERIF VARCHAR2(100),
            FECHA_EMISION DATE,
            FECHA_VENCIMIENTO_1 DATE,
            IMPORTE_TOTAL_ORIGEN1 NUMBER,
            IMPORTE_RECARGO_1 NUMBER,
            IMPORTE_TOTAL_1 NUMBER,
            FECHA_VENCIMIENTO_2 date,
            IMPORTE_TOTAL_ORIGEN2 NUMBER,
            IMPORTE_RECARGO_2 NUMBER,
            IMPORTE_TOTAL_2 NUMBER,
            FECHA_VENCIMIENTO_3 date,
            IMPORTE_TOTAL_ORIGEN3 NUMBER,
            IMPORTE_RECARGO_3 NUMBER,
            IMPORTE_TOTAL_3 NUMBER
         );


    -- ESTRUCTURA de los RENGLONES de los COMPROBANTES SELECCIONADOS
    TYPE rRenglones IS RECORD
        (
            RECURSO VARCHAR2(100),
            ANIO VARCHAR2(100),
            CUOTA VARCHAR2(100),
            CONCEPTO VARCHAR2(100),
            COD_PLAN VARCHAR2(100),
            IMPORIGENRENG NUMBER,
            IMPRECARGOSRENG NUMBER,
            IMPTOTALRENG NUMBER
         );


    mDetalleError VARCHAR2(100);
    mXML CLOB;
    regComprobantes  rComprobantes;
    regRenglones  rRenglones;
    mNroComp VARCHAR(100);
    mCodigoBarra VARCHAR(100);
    
    AuxCodRecurso VARCHAR2(3);
    AuxCuota NUMBER;

    CURSOR mCursorComprobantes IS SELECT ING_COMPR_CAB.tipo_comprob AS TIPO_COMPROBANTE,
                                  LPAD(TRIM(TO_CHAR(ING_COMPR_CAB.nro_comprob)), 12, 0) AS NRO_COMPROBANTE,
                                  ING_COMPR_CAB.digito_ver AS DIGITO_VERIF,
                                  ING_COMPR_CAB.fecha_emision AS FECHA_EMISION,
                                  ING_COMPR_CAB.fecha_vto AS FECHA_VENCIMIENTO_1,
                                  ING_COMPR_CAB.monto_total AS IMPORTE_TOTAL_ORIGEN1,
                                  0 AS IMPORTE_RECARGO_1,
                                  ING_COMPR_CAB.monto_total AS IMPORTE_TOTAL_1,
                                  ING_COMPR_CAB.fecha_vto2 AS FECHA_VENCIMIENTO_2,
                                  ING_COMPR_CAB.monto_total AS IMPORTE_TOTAL_ORIGEN2,
                                  0 AS IMPORTE_RECARGO_2,
                                  ING_COMPR_CAB.monto_total AS IMPORTE_TOTAL_2,
                                  ING_COMPR_CAB.fecha_vto3 AS FECHA_VENCIMIENTO_3,
                                  ING_COMPR_CAB.monto_total AS IMPORTE_TOTAL_ORIGEN3,
                                  0 AS IMPORTE_RECARGO_3,
                                  ING_COMPR_CAB.monto_total AS IMPORTE_TOTAL_3
                                  FROM ing_tmp_cc, ING_COMPR_CAB
                                  WHERE ing_tmp_cc.usuario = mNroUsuario
                                  AND ing_tmp_cc.marcado  = 'S'
                                  AND ING_TMP_CC.nro_comprob = ING_COMPR_CAB.nro_comprob
                                  AND ING_TMP_CC.tipo_comprob = ING_COMPR_CAB.tipo_comprob
                                  GROUP by ING_COMPR_CAB.tipo_comprob, ING_COMPR_CAB.nro_comprob,
                                  ING_COMPR_CAB.digito_ver, ING_COMPR_CAB.fecha_emision,
                                  ING_COMPR_CAB.fecha_vto, ING_COMPR_CAB.monto_total,
                                  0, ING_COMPR_CAB.monto_total,
                                  ING_COMPR_CAB.fecha_vto2, ING_COMPR_CAB.monto_total,
                                  0, ING_COMPR_CAB.monto_total,
                                  ING_COMPR_CAB.fecha_vto3, ING_COMPR_CAB.monto_total,
                                  0, ING_COMPR_CAB.monto_total;
                                  
                                  

    CURSOR mCursorRenglones (mNroComprobante_AUX IN VARCHAR2, mTipoComprobante_AUX IN VARCHAR2)
                             IS SELECT ING_COMPR_CAB.recurso AS RECURSO,
                             ING_COMPR_CAB.ANIO AS ANIO,
                             ING_COMPR_CAB.CUOTA AS CUOTA,
                             ING_COMPR_DET.concepto AS CONCEPTO,
                             ING_COMPR_CAB.nro_plan AS COD_PLAN,
                             ING_COMPR_DET.monto AS IMPORIGENRENG,
                             0 AS IMPRECARGOSRENG,
                             ING_COMPR_DET.monto AS IMPTOTALRENG
                             FROM ING_COMPR_CAB, ING_COMPR_DET
                             WHERE ING_COMPR_CAB.nro_comprob = mNroComprobante_AUX
                             AND ING_COMPR_CAB.tipo_comprob = mTipoComprobante_AUX
                             AND ING_COMPR_DET.nro_comprob = ING_COMPR_CAB.nro_comprob
                             AND ING_COMPR_DET.tipo_comprob = ING_COMPR_CAB.tipo_comprob;

    BEGIN

        mXML :=	'<COMPROBANTES>' || CHR(13) || CHR(10);
        OPEN mCursorComprobantes;
            LOOP
                FETCH mCursorComprobantes INTO regComprobantes;
                EXIT WHEN mCursorComprobantes%NOTFOUND;

                -- Genero el Nro. de Comprobante
                mNroComp := TO_CHAR(regComprobantes.TIPO_COMPROBANTE, '000') || '/' ||
                            regComprobantes.NRO_COMPROBANTE || '/' ||
                            regComprobantes.DIGITO_VERIF;


                -- Obtengo el codigo de Barras
                mCodigoBarra := SUBSTR(Ing_Calcula_Cod50(regComprobantes.FECHA_VENCIMIENTO_1,
                                regComprobantes.IMPORTE_TOTAL_1, NULL, NULL, regComprobantes.TIPO_COMPROBANTE,
                                regComprobantes.NRO_COMPROBANTE, 3), 1, 50);




                mXML :=	mXML || '<COMPROBANTE>' || CHR(13) || CHR(10);
                mXML := mXML || '<NROCOMP>' || TRIM(mNroComp) || '</NROCOMP>' || CHR(13) || CHR(10) ||
			                    '<FECHA_EMISION>' || TRIM(TO_CHAR(regComprobantes.FECHA_EMISION, 'dd/mm/yyyy')) ||'</FECHA_EMISION>' || CHR(13) || CHR(10);



                mXML := mXML || '<RENGLONES>' || CHR(13) || CHR(10);


                mEsPlan := ES_PLAN_DE_PAGO(regComprobantes.TIPO_COMPROBANTE, regComprobantes.NRO_COMPROBANTE);
                IF (mEsPlan = 'S') THEN
                    BEGIN
                      SELECT RECURSO, CUOTA INTO AuxCodRecurso, AuxCuota FROM ING_COMPR_CAB 
                       WHERE TIPO_COMPROB = regComprobantes.TIPO_COMPROBANTE AND
                              NRO_COMPROB = regComprobantes.NRO_COMPROBANTE;
                    EXCEPTION
                    WHEN OTHERS THEN
                      AuxCodRecurso := NULL;
                      AuxCuota := NULL;
                    END;

                    mXML := mXML || '<RENGLON>' || CHR(13) || CHR(10) ||
                                        '<RECURSO>' || TRIM(AuxCodRecurso) || '</RECURSO>' || CHR(13) || CHR(10) ||
                                        '<ANIO> </ANIO>' || CHR(13) || CHR(10) ||
                                        '<CUOTA>' || TRIM(TO_CHAR(AuxCuota)) || '</CUOTA>' || CHR(13) || CHR(10) ||
                                        '<CONCEPTO>Plan de pago</CONCEPTO>' || CHR(13) || CHR(10) ||
                                        '<CODPLAN></CODPLAN>' || CHR(13) || CHR(10) ||
                                        '<IMPORIGENRENG>' || TRIM(TO_CHAR(regComprobantes.IMPORTE_TOTAL_ORIGEN1, '999999990.99')) || '</IMPORIGENRENG>' || CHR(13) || CHR(10) ||
                                        '<IMPRECARGOSRENG>' || TRIM(TO_CHAR(0, '999999990.99')) || '</IMPRECARGOSRENG>' || CHR(13) || CHR(10) ||
                                        '<IMPTOTALRENG>' || TRIM(TO_CHAR(regComprobantes.IMPORTE_TOTAL_ORIGEN1, '999999990.99')) || '</IMPTOTALRENG>' || CHR(13) || CHR(10) ||
                                    '</RENGLON>' || CHR(13) || CHR(10);

                  ELSE
                    OPEN mCursorRenglones(regComprobantes.NRO_COMPROBANTE, regComprobantes.TIPO_COMPROBANTE);
                        LOOP
                            FETCH mCursorRenglones INTO regRenglones;
                               EXIT WHEN mCursorRenglones%NOTFOUND;

                                mXML := mXML || '<RENGLON>' || CHR(13) || CHR(10) ||
                                					'<RECURSO>' || TRIM(regRenglones.RECURSO) || '</RECURSO>' || CHR(13) || CHR(10) ||
                                					'<ANIO>' || TRIM(regRenglones.ANIO) || '</ANIO>' || CHR(13) || CHR(10) ||
                                					'<CUOTA>' || TRIM(regRenglones.CUOTA) || '</CUOTA>' || CHR(13) || CHR(10) ||
                                					'<CONCEPTO>' || TRIM(regRenglones.CONCEPTO) || '</CONCEPTO>' || CHR(13) || CHR(10) ||
                                					'<CODPLAN></CODPLAN>' || CHR(13) || CHR(10) ||
                                					'<IMPORIGENRENG>' || TRIM(TO_CHAR(regRenglones.IMPORIGENRENG, '999999990.99')) || '</IMPORIGENRENG>' || CHR(13) || CHR(10) ||
                                					'<IMPRECARGOSRENG>' || TRIM(TO_CHAR(regRenglones.IMPRECARGOSRENG, '999999990.99')) || '</IMPRECARGOSRENG>' || CHR(13) || CHR(10) ||
                            	   				    '<IMPTOTALRENG>' || TRIM(TO_CHAR(regRenglones.IMPTOTALRENG, '999999990.99')) || '</IMPTOTALRENG>' || CHR(13) || CHR(10) ||
                                	   			'</RENGLON>' || CHR(13) || CHR(10);

                        END LOOP;
                    CLOSE mCursorRenglones;
                END IF;
                mXML := mXML || '</RENGLONES>' || CHR(13) || CHR(10);


                mXML := mXML ||	'<FECHA_VENCIMIENTO_1>' || TRIM(TO_CHAR(regComprobantes.FECHA_VENCIMIENTO_1, 'dd/mm/yyyy')) || '</FECHA_VENCIMIENTO_1>' || CHR(13) || CHR(10) ||
                    			'<IMPORTE_TOTAL_ORIGEN1>' || TRIM(TO_CHAR(regComprobantes.IMPORTE_TOTAL_ORIGEN1, '999999990.99')) || '</IMPORTE_TOTAL_ORIGEN1>' || CHR(13) || CHR(10) ||
                    			'<IMPORTE_RECARGO_1>' || TRIM(TO_CHAR(regComprobantes.IMPORTE_RECARGO_1, '999999990.99')) || '</IMPORTE_RECARGO_1>' || CHR(13) || CHR(10) ||
                    			'<IMPORTE_TOTAL_1>' || TRIM(TO_CHAR(regComprobantes.IMPORTE_TOTAL_1, '999999990.99')) || '</IMPORTE_TOTAL_1>' || CHR(13) || CHR(10) ||
                    			'<FECHA_VENCIMIENTO_2>' || TRIM(TO_CHAR(regComprobantes.FECHA_VENCIMIENTO_2, 'dd/mm/yyyy')) || '</FECHA_VENCIMIENTO_2>' || CHR(13) || CHR(10) ||
                    			'<IMPORTE_TOTAL_ORIGEN2>' || TRIM(TO_CHAR(regComprobantes.IMPORTE_TOTAL_ORIGEN2, '999999990.99')) || '</IMPORTE_TOTAL_ORIGEN2>' || CHR(13) || CHR(10) ||
                    			'<IMPORTE_RECARGO_2>' || TRIM(TO_CHAR(regComprobantes.IMPORTE_RECARGO_2, '999999990.99')) || '</IMPORTE_RECARGO_2>' || CHR(13) || CHR(10) ||
                    			'<IMPORTE_TOTAL_2>' || TRIM(TO_CHAR(regComprobantes.IMPORTE_TOTAL_2, '999999990.99')) || '</IMPORTE_TOTAL_2>' || CHR(13) || CHR(10) ||
                    			'<FECHA_VENCIMIENTO_3>' || TRIM(TO_CHAR(regComprobantes.FECHA_VENCIMIENTO_3, 'dd/mm/yyyy')) || '</FECHA_VENCIMIENTO_3>' || CHR(13) || CHR(10) ||
                    			'<IMPORTE_TOTAL_ORIGEN3>' || TRIM(TO_CHAR(regComprobantes.IMPORTE_TOTAL_ORIGEN3, '999999990.99')) || '</IMPORTE_TOTAL_ORIGEN3>' || CHR(13) || CHR(10) ||
                    			'<IMPORTE_RECARGO_3>' || TRIM(TO_CHAR(regComprobantes.IMPORTE_RECARGO_3, '999999990.99')) || '</IMPORTE_RECARGO_3>' || CHR(13) || CHR(10) ||
                    			'<IMPORTE_TOTAL_3>' || TRIM(TO_CHAR(regComprobantes.IMPORTE_TOTAL_3, '999999990.99')) || '</IMPORTE_TOTAL_3>' || CHR(13) || CHR(10) ||
                    			'<STRINGCODBARRA>' || TRIM(mCodigoBarra) || '</STRINGCODBARRA>' || CHR(13) || CHR(10);



                mXML :=	mXML || '</COMPROBANTE>' || CHR(13) || CHR(10);
            END LOOP;
        CLOSE mCursorComprobantes;
        mXML :=	mXML || '</COMPROBANTES>' || CHR(13) || CHR(10);


    RETURN (mXML);
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
            RETURN 'ERROR';
    END;
------------------------------------------ FIN FUNCION ---------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
/* Esta Funcion Devuelve la etiqueta MUNICIPALIDAD con los datos cargados */
FUNCTION OBTENER_ETIQUETA_MUNICIPALIDAD RETURN VARCHAR2 IS
    mDetalleError VARCHAR2(100);
    mXML VARCHAR2(4999);
    mNroTelefono VARCHAR2(50);
    mMail VARCHAR2(100);

    BEGIN

        -- Obtengo los Valores de los PARAMETROS
        mNroTelefono := OBTENER_PARAMETRO_CONFIG('WEB_NROTELRECWEB');
        mMail := OBTENER_PARAMETRO_CONFIG('WEB_DIRMAILRECWEB');


        -- Genero la Etiqueta
        mXML := '<DATOSCONTACTOCONMUNICIPALIDAD>' || CHR(13) || CHR(10) ||
                	 '<NROTELRECLAMOSWEB>' || mNroTelefono || '</NROTELRECLAMOSWEB>' || CHR(13) || CHR(10) ||
            		 '<DIRMAILRECLAMOSWEB>' || mMail || '</DIRMAILRECLAMOSWEB>' || CHR(13) || CHR(10) ||
            	'</DATOSCONTACTOCONMUNICIPALIDAD>' || CHR(13) || CHR(10);


    RETURN mXML;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
            RETURN 'ERROR';
    END;
------------------------------------------ FIN FUNCION ---------------------------------------

/* ######################################### FIN FUNCIONES PRIVADAS ##################################################### */
/* ###################################################################################################################### */









/* ###################################################################################################################### */
/* ######################################### INICIO FUNCIONES PUBLICAS ################################################## */

------------------------------------------ INICIO STORE ---------------------------------------
/* Este procedimiento llena la tabla de USUARIOS WEB para usar desde la WEB */
PROCEDURE CREAR_TABLA_USUARIOS_WEB IS
    mDetalleError VARCHAR2(100);


    BEGIN
    	FOR mNroUsuario IN  500..999 LOOP
            INSERT INTO GMDQWEB_TRIBUTARIA_USUARIOS(NRO_USUARIO, ESTADO) VALUES (mNroUsuario, 'D');
    	END LOOP;



    COMMIT;
    DBMS_OUTPUT.PUT_LINE('OK');
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
    END;
------------------------------------------ FIN STORE ---------------------------------------




------------------------------------------ INICIO FUNCION ---------------------------------------
/* Este procedimiento OBTIENE el USUARIO y GENERA la DEUDA en la TABLA ING_TMP_CC */
PROCEDURE GENERAR_DEUDA (mNroCuenta IN INTEGER, mTipoCuenta IN CHAR,
                         mNroUsuario IN OUT NUMBER, mCantidadRegistros IN OUT NUMBER)  IS
                         
    mDetalleError VARCHAR2(100);
    mCadenaTablas VARCHAR2(100);
    mNroContribuyente INTEGER;
    mNroUsuarioAUX INTEGER;
    mDato integer;
    mSQl VARCHAR2(100);
    mValor VARCHAR2(10);
    mCantidadRegistrosAUX NUMBER;
    mCursorBloqueo mCursorGenerico;
    mCantidadDiasFechaVencimiento INTEGER;
    mFECVTO VARCHAR2(50);
    mFECVTOAUX DATE;
    BEGIN

        -- Obtengo el Nro. de Contribuyente
        mNroContribuyente := OBTENER_NRO_CONTRIBUYENTE(mNroCuenta,  mTipoCuenta);
        DBMS_OUTPUT.PUT_LINE('Numero de Contribuyente: ' || mNroContribuyente);


        -- Realizo el Bloqueo de tabla
        OPEN mCursorBloqueo FOR
        SELECT *
        FROM GMDQWEB_TRIBUTARIA_USUARIOS
        WHERE ESTADO = 'D'
        FOR UPDATE WAIT 15;



        -- Obtengo el Nro de Usuario mas Chico DISPONIBLE
        SELECT MIN(Nro_Usuario) AS NUMERO_USUARIO INTO mNroUsuarioAUX
        FROM GMDQWEB_TRIBUTARIA_USUARIOS
        WHERE ESTADO = 'D';


        -- Actualizo el Estado del Usuario y Libero la Tabla
        UPDATE GMDQWEB_TRIBUTARIA_USUARIOS SET ESTADO = 'O' WHERE NRO_USUARIO = mNroUsuarioAUX;
        COMMIT;


        -- Limpio los registros para el Usuario Obtenido
        DELETE FROM ING_TMP_CC WHERE USUARIO = mNroUsuarioAUX;
        COMMIT;


        -- Genero los Registros en la Tabla Temporal
        mdato := INSERT_ING_TEMP_CC(mNroCuenta,  mTipoCuenta, mNroContribuyente,  mNroUsuarioAUX);

        -- Obtengo la cantidad de dias que tengo que sumarle a hoy para saber la fecha de vencimiento
        -- del comprobante para generar los intereses y las actualizaciones

        BEGIN
          SELECT TRIM(NVL(Contenido,'0')) INTO mFECVTO
          FROM CONFIG
          WHERE SISTEMA='Ingresos P�blicos'
          and NOMBRE_PARAMETRO ='WEB_CANTDIAS_FECHA_VTO';
        
          IF TRIM(mFECVTO) = 'ULTIMO DIA' THEN
               mFECVTOAUX := TRUNC(LAST_DAY(SYSDATE));
          ELSE 
               mFECVTOAUX := TRUNC(SYSDATE + TO_NUMBER(mFECVTO));
          END IF;
            
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
             mFECVTOAUX := TRUNC(SYSDATE);
        END;


        -- Calculo el Actualizacion en la Tabla Temporal
        CALCULAR_ACTUALIZACIONES(mNroUsuarioAUX, mFECVTOAUX, 'C');

        -- Calculo el Interes en la Tabla Temporal
        CALCULAR_INTERESES(mNroUsuarioAUX, mFECVTOAUX, 'C');

        -- Calculo la Multa en la Tabla Temporal
        --CALCULAR_MULTA(mNroUsuarioAUX, 'C');

        -- Calculo el IVA en la Tabla Temporal
        CALCULAR_IVA(mNroUsuarioAUX, 'C');

        -- Calculo la Actualizacion del plan de pagos en la Tabla Temporal
        CALCULAR_ACTUALIZ_PLAN_PAGO(mNroUsuarioAUX, 'C');

        COMMIT;
    
    
        mCantidadRegistrosAUX := -1;
        SELECT COUNT(USUARIO) INTO mCantidadRegistrosAUX
        FROM ING_TMP_CC
        WHERE USUARIO = mNroUsuarioAUX;


        -- Asigno Variables
        mCantidadRegistros := mCantidadRegistrosAUX;
        mNroUsuario := mNroUsuarioAUX ;
        DBMS_OUTPUT.PUT_LINE('OK');
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
            mCantidadRegistros := -1;
            mNroUsuario := -1;
    END;
------------------------------------------ FIN FUNCION ---------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
/* Esta Funcion Devuelve los datos de la MUNICIPALIDAD para mostrar por la WEB */
FUNCTION OBTENER_DATOS_MUNICIPALIDAD RETURN mCursorGenerico IS
    mDetalleError VARCHAR2(100);
    mCursorSalida mCursorGenerico;

    BEGIN
        OPEN mCursorSalida FOR
        SELECT NOMBRE_PARAMETRO, CONTENIDO AS DATO
        FROM CONFIG
        WHERE SISTEMA = 'Ingresos P�blicos'
        AND NOMBRE_PARAMETRO IN ('WEB_NROTELRECWEB', 'WEB_DIRMAILRECWEB', 'WEB_DIRECCION_MUNI', 'WEB_PAGINA')
        ORDER BY NOMBRE_PARAMETRO DESC;

    RETURN mCursorSalida;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
    END;
------------------------------------------ FIN FUNCION ---------------------------------------




------------------------------------------ INICIO STORE ---------------------------------------
/* Este Procedimiento devuelve la DEUDA del contribuyente y los RECURSOS Involucrados */
 PROCEDURE OBTENER_DEUDA (mNroUsuario IN NUMBER, mCursorRecursos IN OUT mCursorGenerico,
                          mCursorDeudaVencida IN OUT mCursorGenerico,
                          mCursorTotalDeudaVencida IN OUT mCursorGenerico,
                          mCursorDeudaProxima IN OUT mCursorGenerico,
                          mCursorDeudaOtra IN OUT mCursorGenerico) IS

    mDetalleError VARCHAR2(100);
    mCursorRecursosAUX mCursorGenerico;
    mCursorDeudaVencidaAUX mCursorGenerico;
    mCursorTotalDeudaVencidaAUX mCursorGenerico;
    mCursorDeudaProximaAUX mCursorGenerico;
    mCursorDeudaOtraAUX mCursorGenerico;
    
    
    BEGIN

        OPEN mCursorRecursosAUX FOR
        SELECT RECURSO AS CODIGO_RECURSO, DESCRIPCION
        FROM ING_RECURSOS
        WHERE RECURSO IN
        (
            SELECT RECURSO FROM ING_TMP_CC WHERE USUARIO = mNroUsuario
            GROUP BY RECURSO
        );

        -- Deuda Vencida
        OPEN mCursorDeudaVencidaAUX FOR
        SELECT
            TRIM(TRIM(TIPO_COMPROB) || '/' || LPAD(TRIM(TO_CHAR(NRO_COMPROB)), 12, 0) || '/' ||
            TRIM((
                    SELECT DIGITO_VER
                    FROM ING_COMPR_CAB
                    WHERE ING_COMPR_CAB.TIPO_COMPROB = ING_TMP_CC.TIPO_COMPROB
                    AND ING_COMPR_CAB.NRO_COMPROB = ING_TMP_CC.NRO_COMPROB
                ))) AS DDV_NROMOV,
            
            TRIM(RECURSO) AS DDV_REC,
            TRIM(ANIO) AS DDV_ANIO,
            TRIM(CUOTA) AS DDV_CUOTA,
            TRIM(CONCEPTO_CC) AS DDV_CONCEPTO,
            CASE WHEN (ESTADO = 'D') THEN 'SI' ELSE 'NO' END AS DDV_DEPLAN,
            TRIM(TO_CHAR(FECHA_VTO, 'dd/MM/yyyy')) AS DDV_FECHAVENC,
            CASE WHEN (SITUACION = 'J') THEN 'En juicio' ELSE '' END AS DDV_CONDESPECIAL,
            TRIM(TO_CHAR(MONTO_ORIGEN, '999999990.99')) AS DDV_IMPORTEORIGEN,
            TRIM(TO_CHAR(MONTO_INT, '999999990.99')) AS DDV_IMPORTERECARGO,
            TRIM(TO_CHAR(MONTO_TOTAL, '999999990.99')) AS DDV_IMPORTETOTAL
        FROM
            ING_TMP_CC
        WHERE
            USUARIO = mNroUsuario
            AND SITUACION <> 'P'
            AND FECHA_VTO < TRUNC(SYSDATE)
        ORDER BY FECHA_VTO, TIPO_COMPROB, NRO_COMPROB;





        -- Total Deuda Vencida
        OPEN mCursorTotalDeudaVencidaAUX FOR
        SELECT
            TRIM(TO_CHAR(SUM(MONTO_ORIGEN), '999999990.99')) AS DDV_TOTAL_IMPORTEORIGEN,
            TRIM(TO_CHAR(SUM(MONTO_INT), '999999990.99')) AS DDV_TOTAL_IMPORTERECARGO,
            TRIM(TO_CHAR(SUM(MONTO_TOTAL), '999999990.99')) AS DDV_TOTAL_IMPORTETOTAL
        FROM ING_TMP_CC
        WHERE USUARIO = mNroUsuario
        AND SITUACION <> 'P'
        AND FECHA_VTO < TRUNC(SYSDATE);




        -- Deuda Proxima a Vencer dentro de los 60 dias
        OPEN mCursorDeudaProximaAUX FOR
        SELECT
            TRIM(TIPO_COMPROB) AS TIPO_COMPROB,
            LPAD(TRIM(TO_CHAR(NRO_COMPROB)), 12, 0) AS NRO_COMPROB,
            TRIM(RECURSO) AS RECURSO,
            TRIM(ANIO) AS ANIO,
            TRIM(CUOTA) AS CUOTA,
            TRIM(CONCEPTO_CC) AS CONCEPTO_CC,
            TRIM(DESCRIPCION) AS DESCRIPCION,
            TRIM(TO_CHAR(FECHA_VTO, 'dd/MM/yyyy')) AS FECHA_VTO,
            TRIM(TO_CHAR(MONTO_ORIGEN, '999999990.99')) AS MONTO_ORIGEN,
            TRIM(TO_CHAR(MONTO_INT, '999999990.99')) AS MONTO_INT,
            TRIM(TO_CHAR(MONTO_TOTAL, '999999990.99')) AS MONTO_TOTAL,
                (
                    SELECT DIGITO_VER
                    FROM ING_COMPR_CAB
                    WHERE ING_COMPR_CAB.TIPO_COMPROB = ING_TMP_CC.TIPO_COMPROB
                    AND ING_COMPR_CAB.NRO_COMPROB = ING_TMP_CC.NRO_COMPROB
                ) AS DIGITO_VER
        FROM ING_TMP_CC
        WHERE USUARIO = mNroUsuario
        AND SITUACION <> 'P'
        AND FECHA_VTO BETWEEN TRUNC(SYSDATE) AND (TRUNC(SYSDATE) + 60)
        ORDER BY FECHA_VTO, TIPO_COMPROB, NRO_COMPROB;





        -- Deuda Pasado los 60 dias
        OPEN mCursorDeudaOtraAUX FOR
        SELECT
            TRIM(TIPO_COMPROB) AS TIPO_COMPROB,
            LPAD(TRIM(TO_CHAR(NRO_COMPROB)), 12, 0) AS NRO_COMPROB,
            TRIM(RECURSO) AS RECURSO,
            TRIM(ANIO) AS ANIO,
            TRIM(CUOTA) AS CUOTA,
            TRIM(CONCEPTO_CC) AS CONCEPTO_CC,
            TRIM(DESCRIPCION) AS DESCRIPCION,
            TRIM(TO_CHAR(FECHA_VTO, 'dd/MM/yyyy')) AS FECHA_VTO,
            TRIM(TO_CHAR(MONTO_ORIGEN, '999999990.99')) AS MONTO_ORIGEN,
            TRIM(TO_CHAR(MONTO_INT, '999999990.99')) AS MONTO_INT,
            TRIM(TO_CHAR(MONTO_TOTAL, '999999990.99')) AS MONTO_TOTAL,
                (
                    SELECT DIGITO_VER
                    FROM ING_COMPR_CAB
                    WHERE ING_COMPR_CAB.TIPO_COMPROB = ING_TMP_CC.TIPO_COMPROB
                    AND ING_COMPR_CAB.NRO_COMPROB = ING_TMP_CC.NRO_COMPROB
                ) AS DIGITO_VER
        FROM ING_TMP_CC
        WHERE USUARIO = mNroUsuario
        AND SITUACION <> 'P'
        AND FECHA_VTO > (TRUNC(SYSDATE) + 60)
        ORDER BY FECHA_VTO, TIPO_COMPROB, NRO_COMPROB;



        -- Asigno Cursores
        mCursorRecursos := mCursorRecursosAUX;
        mCursorDeudaVencida := mCursorDeudaVencidaAUX;
        mCursorTotalDeudaVencida := mCursorTotalDeudaVencidaAUX;
        mCursorDeudaProxima := mCursorDeudaProximaAUX;
        mCursorDeudaOtra := mCursorDeudaOtraAUX;
        DBMS_OUTPUT.PUT_LINE('OK');
 
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
    END;
------------------------------------------ FIN STORE ---------------------------------------




------------------------------------------ INICIO STORE ---------------------------------------
/* Este Procedimiento Limpia la Deuda de la TABLA TEMPORAL ING_TMP_CC y Libera al USUARIO */
 PROCEDURE LIMPIAR_DATOS_SESSION (mNroUsuario IN NUMBER) IS

    mDetalleError VARCHAR2(100);

    BEGIN

        -- Limpio los registros para el Nuevo Usuario
        DELETE FROM ING_TMP_CC WHERE USUARIO = mNroUsuario;

        -- Actualizo el Estado del Usuario
        UPDATE GMDQWEB_TRIBUTARIA_USUARIOS SET ESTADO = 'D' WHERE NRO_USUARIO = mNroUsuario;


        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
    END;
------------------------------------------ FIN STORE ---------------------------------------




------------------------------------------ INICIO STORE ---------------------------------------
/* Este Procedimiento marca en la TABLA temporal ING_TMP_CC los registros seleccionados */
FUNCTION MARCAR_REGISTROS_SELECCIONADOS (mNroUsuario IN NUMBER, mRecurso IN VARCHAR2,
                                         mTipo_Y_NroComprobante IN VARCHAR2) RETURN VARCHAR2 IS
    mDetalleError VARCHAR2(100);
    mSQL VARCHAR2(4999);
    mOK VARCHAR(5);

    BEGIN

        -- Borro la Seleccion Anterior
        UPDATE ING_TMP_CC SET MARCADO = 'N'
        WHERE USUARIO = mNroUsuario;


        -- Actualizo los Registros Seleccionados
        mSQL := 'UPDATE ING_TMP_CC SET MARCADO = ' || AGREGAR_COMILLAS('S') || '
        WHERE USUARIO = ' || mNroUsuario || '
        AND (TIPO_COMPROB, NRO_COMPROB)
        IN (' || mTipo_Y_NroComprobante || ')';
        

        -- EJECUTO el SQL
        BEGIN
            mOK := 'ERROR';
            EXECUTE IMMEDIATE mSQL;
            mOK := 'OK';
        EXCEPTION WHEN OTHERS THEN
            mOK := 'ERROR';
        END;


        COMMIT;
        RETURN mOK;
    EXCEPTION
        WHEN OTHERS THEN
            ROLLBACK;
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
            RETURN 'ERROR';
    END;
------------------------------------------ FIN STORE ---------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
/* Esta Funcion Devuelve el XML para la GENERACION de los COMPROBANTES NO VENCIDOS PDFs */
FUNCTION EMITIR_COMPROB_NO_VENCIDO (mNroUsuario IN NUMBER) RETURN CLOB IS
    mDetalleError VARCHAR2(100);
    mXML_IMPONIBLE VARCHAR2(4999);
    mXML_COMPROBANTES CLOB;
    mXML_MUNICIPALIDAD VARCHAR2(4999);
    mXML CLOB;
 
 
 
    BEGIN
    
    -- Obtengo la Etiqueta IMPONIBLE
    mXML_IMPONIBLE := OBTENER_ETIQUETA_IMPONIBLE(mNroUsuario);
    DBMS_OUTPUT.PUT_LINE('Obtengo la Etiqueta IMPONIBLE');

    -- Obtengo la Etiqueta COMPROBANTES
    mXML_COMPROBANTES := OBTENER_ETIQUETA_COMPROBANTES(mNroUsuario);
    DBMS_OUTPUT.PUT_LINE('Obtengo la Etiqueta COMPROBANTES');
    
    -- Obtengo la Etiqueta DATOSCONTACTOCONMUNICIPALIDAD
    mXML_MUNICIPALIDAD := OBTENER_ETIQUETA_MUNICIPALIDAD();
    DBMS_OUTPUT.PUT_LINE('Obtengo la Etiqueta MUNICIPALIDAD');
    


    mXML := '<VFPDATA>' || CHR(13) || CHR(10) ||
                mXML_IMPONIBLE || mXML_COMPROBANTES || mXML_MUNICIPALIDAD ||
            '</VFPDATA>';


    RETURN mXML;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
    END;
------------------------------------------ FIN FUNCION ---------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
/*Esta Funcion Devuelve los datos del CONTRIBUYENTE para mostrar en el PDF de INFORME DE DEUDA */
FUNCTION OBTENER_DATOS_CONTRIBUYENTE(mNroUsuario IN NUMBER) RETURN VARCHAR2 IS
    mDetalleError VARCHAR2(100);
    mCadenaDato VARCHAR2(500);
    mTitular VARCHAR2(100);
    mDomicilio VARCHAR2(100);
    mLocalidad VARCHAR2(100);


    BEGIN
        -- Instancio las Variables
        mTitular := '';
        mDomicilio := '';
        mLocalidad := '';


        -- Obtengo los Datos
        BEGIN
            SELECT
                ING_CONTRIBUYENTES.APYNOM AS TITULAR,
                (TRIM(ING_CONTRIBUYENTES.DP_CALLE) || ' ' || TO_CHAR(ING_CONTRIBUYENTES.DP_NRO) || ' ' ||
                TRIM(ING_CONTRIBUYENTES.DP_PISO) || ' ' || TRIM(ING_CONTRIBUYENTES.DP_DEPT)) AS DOMICILIO,
                ('(' || LOCALIDADES.CODIGO || ') ' ||  TRIM(LOCALIDADES.DESCRIPCION)) AS LOCALIDAD
            INTO mTitular, mDomicilio, mLocalidad
            FROM
                ING_CONTRIBUYENTES, LOCALIDADES
            WHERE
                ING_CONTRIBUYENTES.NRO_CONTRIB IN
                    (
                    SELECT NRO_CONTRIB
                    FROM ing_tmp_cc
                    WHERE USUARIO = mNroUsuario
                    AND ROWNUM = 1
                    )
            AND ING_CONTRIBUYENTES.COD_LOC = LOCALIDADES.CODIGO;

        EXCEPTION WHEN NO_DATA_FOUND THEN
            RETURN '';
        END;

        -- Genero la Cadena
        mCadenaDato := TRIM(mTitular) || '|' || TRIM(mDomicilio) || '|' || TRIM(mLocalidad);

    RETURN mCadenaDato;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
    END;
------------------------------------------ FIN FUNCION ---------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
/*Esta Funcion Devuelve los datos del CONTRIBUYENTE para mostrar en el PDF de INFORME DE DEUDA */
FUNCTION OBTENER_LOGO RETURN mCursorGenerico IS
    mDetalleError VARCHAR2(100);
    mCursorSalida mCursorGenerico;

    BEGIN
    
        -- Obtengo los Datos
        BEGIN
            OPEN mCursorSalida FOR
            SELECT * FROM LOGOMUNI WHERE ID = 'LOGO';
        EXCEPTION WHEN NO_DATA_FOUND THEN
            RETURN NULL;
        END;


    RETURN mCursorSalida;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
    END;
------------------------------------------ FIN FUNCION ---------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
/*Esta Funcion Devuelve el CODIGO de la MUNICIPALIDAD para generar el BARCODE39 */
FUNCTION OBTENER_CODIGO_MUNICIPALIDAD RETURN VARCHAR2 IS
    mDetalleError VARCHAR2(100);
    mCodigoMunicipalidad VARCHAR(5);


    BEGIN
        -- Obtengo los Datos
        BEGIN
            SELECT TRIM(CONTENIDO)
            INTO mCodigoMunicipalidad
            FROM CONFIG
            WHERE NOMBRE_PARAMETRO = 'COD_MUNICIPALIDAD';
        EXCEPTION WHEN NO_DATA_FOUND THEN
            RETURN '-1';
        END;


    RETURN mCodigoMunicipalidad;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
            RETURN '-1';
    END;
------------------------------------------ FIN FUNCION ---------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
    /* Esta Funcion Devuelve el NUMERO de CUENTA en base al DOMINIO ingresado */
    FUNCTION OBTENER_RODADO_POR_DOMINIO(mDominio IN VARCHAR2) RETURN NUMBER IS
        mDetalleError VARCHAR2(100);
        mNroCuenta NUMBER;


        BEGIN
    
            -- Busco por el Dominio ORIGINAL
            mNroCuenta := 0;
            BEGIN
                SELECT TRIM(NRO_RODADO)
                INTO mNroCuenta
                FROM ING_RODADOS
                WHERE DOMINIO = TRIM(mDominio);
                
                DBMS_OUTPUT.PUT_LINE('DOMINIO: ' || mNroCuenta);
            EXCEPTION WHEN NO_DATA_FOUND THEN
                mNroCuenta := 0;
            END;
        
        
        
            -- Busco por el Dominio ACTUAL
            IF (mNroCuenta = 0) THEN
                BEGIN
                    SELECT TRIM(NRO_RODADO)
                    INTO mNroCuenta
                    FROM ING_RODADOS
                    WHERE DOMINIO_ACTUAL = TRIM(mDominio);
                    
                    DBMS_OUTPUT.PUT_LINE('DOMINIO ACTUAL: ' || mNroCuenta);
                EXCEPTION WHEN NO_DATA_FOUND THEN
                    mNroCuenta := 0;
                END;
            END IF;
        

        RETURN mNroCuenta;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
            RETURN 0;
    END;
------------------------------------------ FIN FUNCION ---------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
/*Esta funcion devuelve el la fecha a la que se calcularan los interes de la cta. cte. a mostrar */
FUNCTION OBTENER_FECHA_ACTUA_WEB RETURN VARCHAR2 IS
    mDetalleError VARCHAR2(100);
    mDias VARCHAR2(2);
    mFecha VARCHAR2(12);


    BEGIN
        -- Obtengo los Datos
        BEGIN
        
            mDias := OBTENER_PARAMETRO_CONFIG('WEB_CANTDIAS_FECHA_ACTUA');             
            mFecha := (SYSDATE + TO_NUMBER(mDias));                   
        
        EXCEPTION WHEN NO_DATA_FOUND THEN
            RETURN '-1';
        END;


    RETURN mFecha;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
            RETURN '-1';
    END;
------------------------------------------ FIN FUNCION ---------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
/* Esta funcion agrega una visita realizada realizada a traves de la Web */
FUNCTION AGREGAR_VISITA (p_varFecha IN VARCHAR2, p_varHora IN VARCHAR2, p_chrTipoImponible IN CHAR, p_numNroImponible IN NUMBER) RETURN VARCHAR2 IS
   mDetalleError VARCHAR2(100);


    BEGIN
    
        INSERT INTO GMDQWEB_TRIBUTARIA_VISITAS(FECHA, HORA, TIPO_IMPONIBLE, NRO_IMPONIBLE, IMPRIMIO) VALUES
                                              (TO_DATE(p_varFecha, 'dd/mm/yyyy'), TRIM(p_varHora), TRIM(p_chrTipoImponible), p_numNroImponible, 'N');
        COMMIT;
        RETURN 'OK';
         
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
            RETURN 'ERROR';
    END;
------------------------------------------ FIN FUNCION ---------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
/* Esta funcion actualiza una visita cuando se imprime un comprobante */
FUNCTION ACTUALIZAR_VISITA (p_varFecha IN VARCHAR2, p_varHora IN VARCHAR2, p_chrTipoImponible IN CHAR, p_numNroImponible IN NUMBER) RETURN VARCHAR2 IS
   mDetalleError VARCHAR2(100);


    BEGIN
    
        UPDATE GMDQWEB_TRIBUTARIA_VISITAS SET IMPRIMIO = 'S'
        WHERE FECHA = TO_DATE(TRIM(p_varFecha), 'dd/mm/yyyy')
        AND HORA = TRIM(p_varHora)
        AND TIPO_IMPONIBLE = TRIM(p_chrTipoImponible)
        AND NRO_IMPONIBLE = p_numNroImponible;
        COMMIT;
        RETURN 'OK';
         
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
            RETURN 'visita inexistente';
    END;
------------------------------------------ FIN FUNCION ---------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
/* Esta funcion devuelve el numero total de visitas a la Web */
FUNCTION GET_VISITAS_CANTIDAD RETURN VARCHAR2 IS
    mDetalleError VARCHAR2(100);
    varCantidad VARCHAR(5);


    BEGIN
        BEGIN
            
            SELECT TO_CHAR(COUNT(FECHA)) 
            INTO varCantidad
            FROM GMDQWEB_TRIBUTARIA_VISITAS;            
        EXCEPTION WHEN NO_DATA_FOUND THEN
            RETURN '0';
        END;


    RETURN varCantidad;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
            RETURN '0';
    END;
------------------------------------------ FIN FUNCION ---------------------------------------





------------------------------------------ INICIO FUNCION ---------------------------------------
/* Esta funcion devuelve la cantidad de visitas a la Web agrupada por tipo de imponible y si imprimio o no */
FUNCTION GET_VISITAS_CANTIDAD_DETALLE (p_varFechaDesde IN VARCHAR2, p_varFechaHasta IN VARCHAR2) RETURN VARCHAR2 IS
    mDetalleError VARCHAR2(100); 
    numInmuebleConsulta NUMBER;
    numInmuebleConsultaImpresion NUMBER;
    numComercioConsulta NUMBER;
    numComercioConsultaImpresion NUMBER;  
    numRodadoConsulta NUMBER;
    numRodadoConsultaImpresion NUMBER;
    numCementerioConsulta NUMBER;
    numCementerioConsultaImpresion NUMBER;    
    numContribConsulta NUMBER;
    numContribConsultaImpresion NUMBER;
    varDetalle VARCHAR2(4000);
    
     
    TYPE strucTotales IS RECORD (
        tipoImponible CHAR(1),
        imprimio CHAR(1),
        total VARCHAR2(5)
    );
    strTotales strucTotales;
        

    CURSOR cursorDetalle IS SELECT TIPO_IMPONIBLE, IMPRIMIO, TO_CHAR(COUNT(FECHA)) AS TOTAL 
                            FROM GMDQWEB_TRIBUTARIA_VISITAS 
                            WHERE FECHA BETWEEN TO_DATE(TRIM(p_varFechaDesde), 'dd/mm/yyyy') AND TO_DATE(TRIM(p_varFechaHasta), 'dd/mm/yyyy')
                            GROUP BY TIPO_IMPONIBLE, IMPRIMIO
                            ORDER BY TIPO_IMPONIBLE;

                      


    BEGIN
       
        numInmuebleConsulta := 0;
        numInmuebleConsultaImpresion := 0;
        numComercioConsulta := 0;
        numComercioConsultaImpresion := 0;
        numRodadoConsulta := 0;
        numRodadoConsultaImpresion := 0;
        numCementerioConsulta := 0;
        numCementerioConsultaImpresion := 0;
        numContribConsulta := 0;
        numContribConsultaImpresion := 0;           

        OPEN cursorDetalle;
        LOOP
            FETCH cursorDetalle INTO strTotales;
            EXIT WHEN cursorDetalle%NOTFOUND;
 

            /* Inmuebles */
            IF (strTotales.tipoImponible = 'I') THEN
                IF (strTotales.imprimio = 'N') THEN
                    numInmuebleConsulta := strTotales.total;
                ELSE
                    numInmuebleConsultaImpresion := strTotales.total;
                END IF;
            END IF;

            /* Comercios */
            IF (strTotales.tipoImponible = 'C') THEN
                IF (strTotales.imprimio = 'N') THEN
                    numComercioConsulta := strTotales.total;
                ELSE
                    numComercioConsultaImpresion := strTotales.total;
                END IF;
            END IF;

            /* Rodados */
            IF (strTotales.tipoImponible = 'R') THEN
                IF (strTotales.imprimio = 'N') THEN
                    numRodadoConsulta := strTotales.total;
                ELSE
                    numRodadoConsultaImpresion := strTotales.total ;
                END IF;
            END IF;

            /* Cementerio */
            IF (strTotales.tipoImponible = 'E') THEN
                IF (strTotales.imprimio = 'N') THEN
                    numCementerioConsulta := strTotales.total;
                ELSE
                    numCementerioConsultaImpresion := strTotales.total;
                END IF;
            END IF;

            /* Contribuyentes */
            IF (strTotales.tipoImponible = 'N') THEN
                IF (strTotales.imprimio = 'N') THEN
                    numContribConsulta := strTotales.total;
                ELSE
                    numContribConsultaImpresion := strTotales.total;
                END IF;
            END IF;

        END LOOP;
        CLOSE cursorDetalle;

       
       

        /* Genero la cadena de respuesta */
        varDetalle := 'I' || '|' || TRIM(TO_CHAR(numInmuebleConsulta)) || '|' || TRIM(TO_CHAR(numInmuebleConsultaImpresion)) || '|' || TRIM(TO_CHAR(numInmuebleConsulta + numInmuebleConsultaImpresion)) || ';' ||
		      		  'C' || '|' || TRIM(TO_CHAR(numComercioConsulta)) || '|' || TRIM(TO_CHAR(numComercioConsultaImpresion)) || '|' || TRIM(TO_CHAR(numComercioConsulta + numComercioConsultaImpresion)) || ';' || 
			     	  'V' || '|' || TRIM(TO_CHAR(numRodadoConsulta)) || '|' || TRIM(TO_CHAR(numRodadoConsultaImpresion)) || '|' || TRIM(TO_CHAR(numRodadoConsulta + numRodadoConsultaImpresion)) || ';' || 
                      'E' || '|' || TRIM(TO_CHAR(numCementerioConsulta)) || '|' || TRIM(TO_CHAR(numCementerioConsultaImpresion)) || '|' || TRIM(TO_CHAR(numCementerioConsulta + numCementerioConsultaImpresion)) || ';' || 
    				  'N' || '|' || TRIM(TO_CHAR(numContribConsulta)) || '|' || TRIM(TO_CHAR(numContribConsultaImpresion)) || '|' || TRIM(TO_CHAR(numContribConsulta + numContribConsultaImpresion)) || ';'; 

       
       
         


    RETURN varDetalle;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
            RETURN '';
    END;
------------------------------------------ FIN FUNCION ---------------------------------------

------------------------------------------ INICIO FUNCION ---------------------------------------
/* Esta Funcion Devuelve el COONTENIDO segun el PARAMETRO UTILIZADO */
FUNCTION OBTENER_TIPOS_IMPONIBLE RETURN VARCHAR2 IS
    mDetalleError VARCHAR2(100);
    mTipos VARCHAR2(100);


    BEGIN            
            -- Obtengo los Datos
        BEGIN
            SELECT TRIM(CONTENIDO)
            INTO mTipos
            FROM CONFIG
            WHERE NOMBRE_PARAMETRO='WEB_TIPOSIMP';
            
        EXCEPTION WHEN NO_DATA_FOUND THEN
            RETURN '0';
        END;


    RETURN mTipos;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
            RETURN '0';
    END;
------------------------------------------ FIN FUNCION ---------------------------------------
------------------------------------------ INICIO FUNCION ---------------------------------------
/* Esta Funcion Devuelve los datos de un rodado para mostrar por la WEB */
FUNCTION OBTENER_DATOS_RODADO (p_numNroVehiculo IN NUMBER) RETURN mCursorGenerico IS
    mDetalleError VARCHAR2(100);
    mCursorSalida mCursorGenerico;

    BEGIN
        OPEN mCursorSalida FOR
        SELECT 
            ING_RODADOS.DOMINIO, 
            ING_ROD_MARCAS.DESCRIPCION AS MARCA,
            ING_ROD_MODELOS.DESCRIPCION AS MODELO
        FROM ING_RODADOS INNER JOIN ING_ROD_MARCAS 
             ON ING_RODADOS.MARCA = ING_ROD_MARCAS.CODIGO
        INNER JOIN ING_ROD_MODELOS 
             ON ING_RODADOS.MARCA = ING_ROD_MODELOS.MARCA 
             AND ING_RODADOS.MODELO = ING_ROD_MODELOS.MODELO
        WHERE 
            ING_RODADOS.NRO_RODADO = p_numNroVehiculo; 
    
    RETURN mCursorSalida;
    EXCEPTION
        WHEN OTHERS THEN
            mDetalleError := 'Error Numero: ' || SQLCODE || CHR(13) || 'Descripcion: ' || SQLERRM;
            DBMS_OUTPUT.PUT_LINE(mDetalleError);
    END;
------------------------------------------ FIN FUNCION ---------------------------------------


/* ######################################### FIN FUNCIONES PUBLICAS ##################################################### */
/* ###################################################################################################################### */


END;
/


