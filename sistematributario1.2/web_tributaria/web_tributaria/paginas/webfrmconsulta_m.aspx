<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmconsulta_m.aspx.vb" Inherits="web_tributaria.webfrmconsulta_m" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


<script src="../js/funciones.js" type="text/javascript"></script>

<script type="text/javascript">

    function validar_datos() {

        
        //Cadena Vacia
        if (control_vacio(window.document.getElementById('txtnro_cuenta'), 'N�mero de Cuenta:\nDebe Ingresar un N�mero de Cuenta')) {
            return false;
        }
        
       //Validar Cadena Ingresada Nro. de Cuenta
        if (!(validar_cadena_solo_numeros(window.document.getElementById('txtnro_cuenta'),'N�mero de Cuenta'))) {
            return false;
        }       




        //Cadena Vacia
        if (control_vacio(window.document.getElementById('txtgrupo_comprobante1'),'Comprobante 1:\nDebe Ingresar el N�mero de Comprobante pagado.\n(Grupo / N�mero / D�gito Verificador)')) {
           return false;
        }
        
       //Validar Cadena Ingresada Grupo Comprobante1
        if (!(validar_cadena_solo_numeros(window.document.getElementById('txtgrupo_comprobante1'),'Comprobante 1'))) {
            return false;
        }       




        //Cadena Vacia
        if (control_vacio(window.document.getElementById('txtnro_comprobante1'),'Comprobante 1:\nDebe Ingresar el N�mero de Comprobante pagado.\n(Grupo / N�mero / D�gito Verificador)')) {
            return false;
        }
        
       //Validar Cadena Ingresada Nro. de Comprobante
        if (!(validar_cadena_solo_numeros(window.document.getElementById('txtnro_comprobante1'),'Comprobante 1'))) {
            return false;
        }       




        //Cadena Vacia
        if (control_vacio(window.document.getElementById('txtnro_verificador1'),'Comprobante 1:\nDebe Ingresar el N�mero de Comprobante pagado.\n(Grupo / N�mero / D�gito Verificador)')) {
           return false;
        }
        
       //Validar Cadena Ingresada Nro. Verificador
        if (!(validar_cadena_solo_numeros(window.document.getElementById('txtnro_verificador1'),'Comprobante 1'))) {
            return false;
        }       





        //Cadena Vacia
        if (control_vacio(window.document.getElementById('txtgrupo_comprobante2'),'Comprobante 2:\nDebe Ingresar el N�mero de Comprobante pagado.\n(Grupo / N�mero / D�gito Verificador)')) {
           return false;
        }
        
       //Validar Cadena Ingresada Grupo Comprobante2
        if (!(validar_cadena_solo_numeros(window.document.getElementById('txtgrupo_comprobante2'),'Comprobante 2'))) {
            return false;
        }       




        //Cadena Vacia
        if (control_vacio(window.document.getElementById('txtnro_comprobante2'),'Comprobante 2:\nDebe Ingresar el N�mero de Comprobante pagado.\n(Grupo / N�mero / D�gito Verificador)')) {           
           return false;
        }
        
       //Validar Cadena Ingresada Nro. de Cuenta
        if (!(validar_cadena_solo_numeros(window.document.getElementById('txtnro_comprobante2'),'Comprobante 2'))) {
            return false;
        }       




        //Cadena Vacia
        if (control_vacio(window.document.getElementById('txtnro_verificador2'),'Comprobante 2:\nDebe Ingresar el N�mero de Comprobante pagado.\n(Grupo / N�mero / D�gito Verificador)')) {
           return false;
        }
        
       //Validar Cadena Ingresada Nro. de Cuenta
        if (!(validar_cadena_solo_numeros(window.document.getElementById('txtnro_verificador2'),  'Comprobante 2'))) {
            return false;
        }       




        //Numeros de Comprobantes
        if (document.formulario.txtgrupo_comprobante1.value == document.formulario.txtgrupo_comprobante2.value &&
            document.formulario.txtnro_comprobante1.value == document.formulario.txtnro_comprobante2.value &&
            document.formulario.txtnro_verificador1.value == document.formulario.txtnro_verificador2.value) {
            alert('Los N�meros de Comprobantes no pueden ser iguales.');
            return false;
        }
        
              




        //Cadena Vacia
        if (control_vacio(window.document.getElementById('txtcaptcha'),'Debe Ingresar el C�digo de Confirmaci�n')) {          
            return false;
        }
        
        
        if (!validar_cadena(window.document.getElementById('txtcaptcha'),'Codigo de Verificaci�n')) {          
            return false;
        }
    
    return true; 
    }
    
    












    function completar_ceros_derecha(control, cantidad_ceros, caracter) {
        var dato = control.value.toString();
        var id_control = control.id;
        
        
        while(dato.length < cantidad_ceros) {
            dato = caracter + dato;
        }    
        window.document.getElementById(id_control).value = dato;
    }



    function consultar_deuda(val, ev){

        if (window.event) {
            var key = window.event.keyCode;
        } else {
            var key = ev.which;
        }

        
      
        if (key == 13) {    
            if (validar_datos()){
                __doPostBack("link_consultar_deuda",'');
            }
        }                 
    }
    
    
    
    function carga_inicial(){
        var control_label = document.getElementById('lblmensaje');
        var control_div
        
        if (control_label.innerHTML == 'OK') {
            document.getElementById('div_mensaje').style.display = 'none';
        } else {
            document.getElementById('div_mensaje').style.display = 'block';
        }
    }
    

</script>



<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sistema TRIBUTARIO.-</title>



<link href="../css/webfrmconsulta_m.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            #footer
            {
                height: 0px;
            }
                        
            .columna_titulo{
            	width: 40%;
            	text-align:center;
            }
            
            .columna_dato{
            	width: 60%;
            	text-align:center;                    
            }
            
            .textbox {
                 text-align:center;
            }
            
            .textbox_captcha {
                 text-align:center;
                  margin-bottom:2px;
            }
            
                        
            .titulo_tabla{
	            background-image: url('../imagenes/barra_cabecera.png');
	            color:#FFFFFF;
	            font-size:12px;
	            text-align:center;
	            border-left:1px solid #FFFFFF;
	            height:40px;
            }
                             
            
            .style7
            {
                width: 329px;
                text-align: center;
            }
                             
            
        </style>
</head>

<body onload="carga_inicial()">

    <form id="formulario" runat="server">
        <div class="main">
        
            <div id="header">   
                <div class="top">
                    <div class="title"><span class="degrade"></span>Consulta Tributaria</div>
                </div>                               
            </div>
        
         
        
        
        <div class="content">
		    <div class="barra">
                <strong>&nbsp;CONSULTA DE DEUDA Y EMISION DE COMPROBANTES</strong>
                </div>
                <div class="recomendaciones">
                     &nbsp;* Ingrese el tipo y n�mero de cuenta (sin guiones, barras ni d�gito 
                     verificador).<br />
                     &nbsp;* Seleccione tipo de cuenta <strong><em>INMUEBLE</em></strong> para deudas 
                     relacionadas con tasas del tipo Alumbrado, Barrido y Limipieza, Servicios 
                     Urbanos y Servicios Sanitarios. <br />
                            &nbsp;* Seleccione tipo de cuenta <strong><em>COMERCIO</em></strong> para 
                     deudas relacionadas con Inspecci�n de Seguridad e Higiene.<br />
                            &nbsp;* Seleccione tipo de cuenta <strong><em>CEMENTERIO</em></strong> para 
                     deudas relacionadas con Cementerios.<br />
                            &nbsp;* Seleccione tipo de cuenta <em><strong>VEHICULO</strong></em> para 
                     deudas relacionadas con Impuesto Automotor o Patentes de Rodados Menores.</div>
                            
                            <div class="barra"></div>
                            <div id ="tabla" style="width: 857px;  padding:10px 0px 10px 0px;" align="center"; >
                            
                            <div id="div_mensaje" 
                                style="color: #FF0000; font-size: small; font-weight: bold;">
                                <asp:Label ID="lblmensaje" runat="server" Text=""></asp:Label>
                            </div>
                           
                            
                                <table border="1" cellspacing="10"                                      
                                    
                                    style="border-style: double; width: 45%; height: 187px; background-color: #FFFFFF; color: #000000;" 
                                    align="center">
                                    <tr>
                                        <td class="style7">Tipo de Cuenta</td>
                                        <td class="columna_dato">
                                            <asp:DropDownList ID="cmbtipo_cuenta" runat="server" Height="22px" 
                                                Width="100px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="style7">N�mero de Cuenta</td>
                                            
                                        <td class="columna_dato">
                                        <asp:TextBox ID="txtnro_cuenta" runat="server" Width="70px" CssClass="textbox" 
                                                MaxLength="10"></asp:TextBox>
                                            &nbsp;</td>
                                    </tr>
                                                            
                                                            
                                    <tr>
                                        <td class="titulo_tabla" colspan="2" 
                                            style="font-size: small; color: #FFFFFF; font-weight: bold">
                                            
                                            &nbsp;Ingrese los n�meros de dos comprobantes pagados
                                            &nbsp;para verificar su n�mero de cuenta.
                                            </td>
                                    </tr>
                                                            
                                                            
                                    <tr>
                                        <td class="style7">Comprobante 1</td>  
                                        <td class ="columna_dato">
                                            <asp:TextBox ID="txtgrupo_comprobante1" runat="server" Width="24px"  CssClass="textbox" MaxLength="2"></asp:TextBox>
                                            /
                                            <asp:TextBox ID="txtnro_comprobante1" runat="server" MaxLength="9" Width="77px" CssClass="textbox"></asp:TextBox>
                                            /
                                            <asp:TextBox ID="txtnro_verificador1" runat="server" Width="24px" MaxLength="2" CssClass="textbox"></asp:TextBox>
                                        </td>
                                    </tr>
                                                            
                                                            
                                    <tr>
                                        <td class="style7">Comprobante 2</td>
                                        <td class="columna_dato">
                                            <asp:TextBox ID="txtgrupo_comprobante2" runat="server" Width="24px" MaxLength="2" CssClass="textbox"></asp:TextBox>
                                            /
                                            <asp:TextBox ID="txtnro_comprobante2" runat="server" MaxLength="9" Width="77px" CssClass="textbox"></asp:TextBox>
                                            /
                                            <asp:TextBox ID="txtnro_verificador2" runat="server" Width="24px" MaxLength="2" CssClass="textbox"></asp:TextBox>
                                        </td>
                                    </tr>
                                    
                                    
                                    <tr>
                                        <td class="style7">
                                        <cc1:captchacontrol ID="verificador_captcha" runat="server" CaptchaLength="5"
                                                 CaptchaLineNoise="Low" CaptchaMinTimeout="5"  
                                                 CaptchaMaxTimeout="240" FontColor = "#529E00" Font-Size="XX-Large" Height="50px" 
                                                                    Width="179px" 
                                                CaptchaChars="ACDEFGHJKLNPQRTUVXYZ2346789" />
                                        </td>
                                        <td class="columna_dato">
                                            <asp:TextBox Id="txtcaptcha" runat="server" Width="90%" MaxLength="5" CssClass="textbox_captcha" Font-Bold="True" Font-Size="Small" ></asp:TextBox>                                            
                                            <br />
                                            <asp:LinkButton Id="link_consultar_deuda" runat="server"  >[Consultar Deuda]</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                                        
                                        <div>
                                        </div>
                                        
                                        
                                                        
                              </div>    
                                
                                
                                
                                <div style="height: 15px; font-weight: bold; font-size: small; text-align: center; font-family: 'Courier New', Courier, 'espacio sencillo';" 
                    class="barra">NOTA: </div>
                                <div align="center">&nbsp; - Los per&iacute;odos de deuda aqu&iacute; listados pueden no reflejar los pagos realizados recientemente. Ante cualquier duda relacionada con su estado de deuda&nbsp; p�ngase en contacto con la Municipalidad a trav&eacute;s del tel&eacute;fono 
                                    <asp:Label ID="lbltelefono" runat="server" Text="TELEFONO"></asp:Label>
                                     &nbsp;o la direcci&oacute;n de correo electr&oacute;nico <asp:Label ID="lblmail" runat="server" Text="MAIL"></asp:Label>
                                    .</div>
	
	                                <div align="center">
&nbsp; - Para un correcto funcionamiento en la Emisi�n de Comprobantes se le aconseja tener actualizado el Acrobat 
                                        Reader. Para actualizar a la �ltima versi�n
                                        
                                        <a href="http://www.adobe.com/es/products/acrobat/readstep2.html" 
                                            target="_blank">[clic aqu�]</a>.
                                    </div>
                                    
	                        </div>
	                        <div style="padding: 0px 5px 0px 0px; text-align:right;"><a href="http://www.grupomdq.com" target="_blank">[Desarrollado por Grupo MDQ]</a></div>
                    
            </div>
            
        </form>
    </body>
</html>

