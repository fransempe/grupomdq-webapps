﻿Imports ws_tributario_rafam


'Seguridad de Caducidad
Imports gmdq


Public Class ClsConexionRAFAM

    Private mDLL As ws_tributario_rafam.ClsRafam
    Private mXML As String
    Private ObjLog As Clslog
    Private mRutaFisica As String


    Public Sub New(ByVal mRuta As String)
        mRutaFisica = mRuta.ToString
    End Sub


    Private Sub Limpiar()
        mDLL = Nothing
        System.GC.Collect()
        System.GC.WaitForPendingFinalizers()
    End Sub


    Public Function VerRuta_CadenaConexion() As String
        Try

            Call Limpiar()
            mDLL = New ws_tributario_rafam.ClsRafam(mRutaFisica.ToString)
            mXML = mDLL.VerRuta_CadenaConexion()
            mDLL = Nothing
            Return mXML.ToString


        Catch ex As Exception
            ObjLog = New Clslog
            ObjLog.GenerarLog(ex, mRutaFisica.ToString)
            ObjLog = Nothing
            Return "Error"
        End Try
    End Function

    Public Function LimpiarDatosSession(ByVal mNroUsuario As Integer) As Boolean
        Try

            Call Limpiar()
            mDLL = New ws_tributario_rafam.ClsRafam(mRutaFisica.ToString)
            mXML = mDLL.LimpiarDatosSession(mNroUsuario)
            mDLL = Nothing
            Return mXML.ToString


        Catch ex As Exception
            ObjLog = New Clslog
            ObjLog.GenerarLog(ex, mRutaFisica.ToString)
            ObjLog = Nothing
            Return "Error"
        End Try
    End Function



    Public Function ObtenerDatosMunicipalidad() As String

        Try

            Call Limpiar()
            mDLL = New ws_tributario_rafam.ClsRafam(mRutaFisica.ToString)
            mXML = mDLL.ObtenerTelefonoYMail()
            mDLL = Nothing
            Return mXML.ToString

        Catch ex As Exception
            ObjLog = New Clslog
            ObjLog.GenerarLog(ex, mRutaFisica.ToString)
            ObjLog = Nothing
            Return "Error"
        End Try


    End Function

    Public Function ObtenerDatosContribuyente(ByVal mNroUsuario As Integer) As String

        Try
            Call Limpiar()
            mDLL = New ws_tributario_rafam.ClsRafam(mRutaFisica.ToString)
            mXML = mDLL.ObtenerDatosContribuyente(mNroUsuario)
            mDLL = Nothing
            Return mXML.ToString

        Catch ex As Exception
            ObjLog = New Clslog
            ObjLog.GenerarLog(ex, mRutaFisica.ToString)
            ObjLog = Nothing
            Return "Error"
        End Try


    End Function

    Public Function ConsultarDeuda(ByVal mTipoCuenta As String, ByVal mNroCuenta As Long, ByVal mNroUsuario As Integer) As String

        Try
            Call Limpiar()
            mDLL = New ws_tributario_rafam.ClsRafam(mRutaFisica.ToString)
            mXML = mDLL.ConsultarDeuda(mTipoCuenta, mNroCuenta, mNroUsuario)
            'mXML = mDLL.ConsultarDeuda("I", 10000503, 526)
            mDLL = Nothing



        Catch ex As Exception
            ObjLog = New Clslog
            ObjLog.GenerarLog(ex, mRutaFisica.ToString)
            ObjLog = Nothing
            Return "Error"
        End Try

        Return mXML.ToString
    End Function

    Public Function EmitirComprobantes(ByVal p_strComprobantesVencidos As String, ByVal mComprobantesVencidos As Boolean) As String

        Try

            Call Limpiar()
            mDLL = New ws_tributario_rafam.ClsRafam(mRutaFisica.ToString)
            mXML = mDLL.EmitirComprobantes(p_strComprobantesVencidos.ToString, mComprobantesVencidos)
            mDLL = Nothing
            Return mXML.ToString


        Catch ex As Exception
            ObjLog = New Clslog
            ObjLog.GenerarLog(ex, mRutaFisica.ToString)
            ObjLog = Nothing
            Return "Error"
        End Try


    End Function


    Public Function ObtenerLogo() As DataSet
        Dim dsDatosLogo As DataSet

        Try
            Call Limpiar()
            mDLL = New ws_tributario_rafam.ClsRafam(mRutaFisica.ToString)
            dsDatosLogo = mDLL.ObtenerLogo
            mDLL = Nothing
            Return dsDatosLogo

        Catch ex As Exception
            ObjLog = New Clslog
            ObjLog.GenerarLog(ex, mRutaFisica.ToString)
            ObjLog = Nothing
            Return Nothing
        End Try


    End Function

    Public Function ObtenerCodigoMunicipalidad() As String

        Try
            Call Limpiar()
            mDLL = New ws_tributario_rafam.ClsRafam(mRutaFisica.ToString)
            mXML = mDLL.ObtenerCodigoMunicipalidad
            mDLL = Nothing
            Return mXML.ToString

        Catch ex As Exception
            ObjLog = New Clslog
            ObjLog.GenerarLog(ex, mRutaFisica.ToString)
            ObjLog = Nothing
            Return "Error"
        End Try
    End Function



    'Esta Funcion me Devuelve el Número de Cuenta en Base a un Dominio
    Public Function ObtenerNroCuentaPorPatente(ByVal mDominio As String) As String

        Try
            mXML = ""
            Call Limpiar()
            mDLL = New ws_tributario_rafam.ClsRafam(mRutaFisica.ToString)
            mXML = mDLL.ObtenerRodadoPorDominio(mDominio.ToString.Trim)
            mDLL = Nothing



        Catch ex As Exception
            ObjLog = New Clslog
            ObjLog.GenerarLog(ex, mRutaFisica.ToString)
            ObjLog = Nothing
            Return "Error"
        End Try

        Return mXML.ToString.Trim
    End Function


    'Esta Funcion Devuelve la fecha a la cual se calcularan los intereses
    Public Function ObtenerFechaActuaWeb() As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            mDLL = New ws_tributario_rafam.ClsRafam(mRutaFisica.ToString)
            mXML = mDLL.ObtenerFechaActuaWeb

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
            Me.mXML = "Error"
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function


    Public Function agregarVisita(ByVal p_sFecha As String, ByVal p_sHora As String, ByVal p_sTipoImponible As String, ByVal p_lNroImponible As Long) As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New ws_tributario_rafam.ClsRafam(mRutaFisica.ToString)
            Me.mXML = Me.mDLL.agregarVisita(p_sFecha.Trim, p_sHora.Trim, p_sTipoImponible.Trim, p_lNroImponible)

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function


    Public Function actualizarVisita(ByVal p_sFecha As String, ByVal p_sHora As String, ByVal p_sTipoImponible As String, ByVal p_lNroImponible As Long) As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New ws_tributario_rafam.ClsRafam(mRutaFisica.ToString)
            Me.mXML = Me.mDLL.actualizarVisita(p_sFecha.Trim, p_sHora.Trim, p_sTipoImponible.Trim, p_lNroImponible)

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function


    Public Function getVisitasCantidad() As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New ws_tributario_rafam.ClsRafam(mRutaFisica.ToString)
            Me.mXML = Me.mDLL.getVisitasCantidad()

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function


    Public Function getVisitasCantidadDetalle(ByVal p_sFechaDesde As String, ByVal p_sFechaHasta As String) As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New ws_tributario_rafam.ClsRafam(mRutaFisica.ToString)
            Me.mXML = Me.mDLL.getVisitasCantidadDetalle(p_sFechaDesde.Trim, p_sFechaHasta.Trim)

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function

    'Esta funcion no hace nada porque es solo usada si estas conectado a fox
    Public Function getParametroExcluirRecurso() As String
        Return ""
    End Function

    '19/09/2014 Francisco
    'RAFAM: Esta funcion conecta al metodo ObtenerTiposImponible.
    Public Function ObtenerTiposImponible() As String
        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New ws_tributario_rafam.ClsRafam(mRutaFisica.ToString)
            Me.mXML = Me.mDLL.ObtenerTiposImponible

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function


    'Esta funcion devuelve FALSE porque es solo usada si estas conectado a fox
    Public Function comprobanteContieneRecurso(ByVal p_lngGrupoComprobante As Long, ByVal p_lngNumeroComprobante As Long, ByVal p_strRecurso As String) As String
        Return "FALSE"
    End Function

    '11-03-2019 Francisco
    'RAFAM: Esta funcion devuelve la nomenclatura catastral segun el numero de inmueble.
    Public Function ObtenerNomenclaturaCatastral(ByVal p_nroInmueble As Long) As String
        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New ws_tributario_rafam.ClsRafam(mRutaFisica.ToString)
            Me.mXML = Me.mDLL.ObtenerNomenclaturaCatastral(p_nroInmueble)

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function


End Class
