﻿Option Explicit On
Option Strict On

Public Class clsConexionFOX
    Inherits clsConfiguracion


#Region "Variables"

    Private mDLL As tish.tishweb
    Private mXML As String


    Private mNroComercio As String
    Private mTipoBusqueda As String
    Private mListDDJJComplete As Boolean

    Private mYear As Integer
    Private mQuota As Integer


    Private mXMLVouchers As String
    Private mExpiredVouchers As Boolean


    Private mRecurso As String
    Private mCuit As String
    Private mXML_AddDDJJ As String

    Private mListTrades As String
    Private mDebug As String

#End Region

#Region "Contructores"

    Public Sub New()

        Me.mXML = ""
        Me.mNroComercio = ""
        Me.TipoBusqueda = ""

        Me.mListDDJJComplete = False
        Me.mYear = 0
        Me.mQuota = 0


        Me.mXMLVouchers = ""
        Me.mExpiredVouchers = False


        Me.mRecurso = ""
        Me.mCuit = ""
        Me.mXML_AddDDJJ = ""

        Me.mListTrades = ""


        Me.mDebug = System.Configuration.ConfigurationManager.AppSettings("Debug")

        Call Me.Limpiar()
    End Sub

    Protected Overrides Sub Finalize()

        Me.mXML = ""
        Me.mNroComercio = ""
        Me.TipoBusqueda = ""
        Me.mListDDJJComplete = False

        Me.mYear = 0
        Me.mQuota = 0

        Me.mXMLVouchers = ""
        Me.mExpiredVouchers = False


        Me.mRecurso = ""
        Me.mCuit = ""
        Me.mXML_AddDDJJ = ""

        Me.mListTrades = ""

        Me.mDebug = ""

        Call Me.Limpiar()

        MyBase.Finalize()
    End Sub
#End Region

#Region "Propertys"

    Public WriteOnly Property NroComercio() As String
        Set(ByVal value As String)
            Me.mNroComercio = value.ToString.Trim
        End Set
    End Property

    Public WriteOnly Property TipoBusqueda() As String
        Set(ByVal value As String)
            Me.mTipoBusqueda = value.ToString.Trim
        End Set
    End Property


    Public WriteOnly Property ListDDJJComplete() As Boolean
        Set(ByVal value As Boolean)
            Me.mListDDJJComplete = value
        End Set
    End Property

    Public WriteOnly Property Year() As Integer
        Set(ByVal value As Integer)
            Me.mYear = value
        End Set
    End Property

    Public WriteOnly Property Quota() As Integer
        Set(ByVal value As Integer)
            Me.mQuota = value
        End Set
    End Property


    Public WriteOnly Property XMLVouchers() As String
        Set(ByVal value As String)
            Me.mXMLVouchers = value.ToString.Trim
        End Set
    End Property

    Public WriteOnly Property ExpiredVouchers() As Boolean
        Set(ByVal value As Boolean)
            Me.mExpiredVouchers = value
        End Set
    End Property


    Public WriteOnly Property XML_AddDDJJ() As String
        Set(ByVal value As String)
            Me.mXML_AddDDJJ = value.ToString.Trim
        End Set
    End Property


    Public WriteOnly Property Recurso() As String
        Set(ByVal value As String)
            Me.mRecurso = value.ToString.Trim
        End Set
    End Property



    Public WriteOnly Property Cuit() As String
        Set(ByVal value As String)
            Me.mCuit = value.ToString.Trim
        End Set
    End Property


    Public WriteOnly Property ListTrades() As String
        Set(ByVal value As String)
            Me.mListTrades = value.ToString.Trim
        End Set
    End Property




#End Region

#Region "Funciones Privadas"

    'Esta funcion limpia y elimina todo lo que este en memoria
    Private Sub Limpiar()
        Me.mDLL = Nothing
        System.GC.Collect()
        System.GC.WaitForPendingFinalizers()
    End Sub

#End Region

#Region "Funciones Publicas"

    'ObtenerDatosMunicipalidad:
    'Esta funcion devuelve los datos de la municipalidad que van al pie de la web
    Public Function ObtenerDatosMunicipalidad() As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.ObtenerDatosMunicipalidad()

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function


    'ObtenerDatosContribuyente:
    'Esta funcion me devuelve los datos del comercio en base a un Nro. de Comercio
    Public Function ObtenerDatosComercio() As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.ObtenerDatosComercio(CDbl(mNroComercio))

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function


    'BuscarComercio:
    'Esta funcion me devuelve los datos del comercio dependiendo del tipo de busqueda
    Public Function BuscarComercio() As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.BuscarComercio(Me.mNroComercio.ToString, Me.mTipoBusqueda.ToString)

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function



    'ObtainCTACTE:
    'This function returns a list of the presented DDJJ
    Public Function ObtainCTACTE() As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.ConsultarDeuda("C", CDbl(Me.mNroComercio))

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function



    'getListadoDDJJByComercio:
    Public Function getListadoDDJJByComercio(ByVal p_lngNroComercio As Long) As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.getListadoDDJJByComercio(CDbl(p_lngNroComercio))


        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function



    'ObtainDetailDDJJ:
    'This function returns the detail of only a DDJJ
    Public Function ObtainDetailDDJJ() As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.ObtainDetailDDJJ(CDbl(Me.mNroComercio.ToString.Trim), Me.mYear, Me.mQuota)

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function



    'ObtainRubros:
    'This function returns the rubros of the comercio
    Public Function ObtainRubros() As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.ObtainRubros(CDbl(Me.mNroComercio.ToString.Trim))

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function



    'ObtainRubros:
    'This function returns the rubros of the comercio
    Public Function ToEmitVouchers() As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb


            If (Me.mExpiredVouchers) Then
                Me.mXML = Me.mDLL.EmitircompPagoDeudaVenc(Me.mXMLVouchers.ToString)
            Else
                Me.mXML = Me.mDLL.EmitirCompPagoNoVenc(Me.mXMLVouchers.ToString)
            End If

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function



    'ObtainRubros:
    'This function returns the number of the recurso TISH
    Public Function ObtainNumberRecursoTISH() As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.ObtainNumberRecursoTISH()

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function


    'ObtainPeriodsTISH: 
    'This function returns the periods that the municipalidad was using to receive the tish
    Public Function ObtainPeriodsTISH() As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.ObtainPeriodsTISH()

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function


    'Exist_DDJJ: 
    'This function says to us if the ddjj exists
    Public Function Exist_DDJJ() As Boolean
        Dim mExist As Boolean

        Try
            mExist = False
            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            mExist = Me.mDLL.Exist_DDJJ(CInt(Me.mNroComercio), Me.mYear, Me.mQuota)


        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return mExist
    End Function



    'AddDDJJ: 
    'This function a new DDJJ adds
    Public Function AddDDJJ() As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.AddDDJJ(Me.mRecurso.ToString.Trim, Me.mCuit.ToString.Trim, CInt(Me.mNroComercio.ToString.Trim), Me.mYear, Me.mQuota, Me.mXML_AddDDJJ.ToString.Trim)


        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function



    'ObtainNumberVoucher:
    'This function returns the group of the voucher and the number of the voucher 
    Public Function ObtainNumberVoucher() As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.ObtainNumberVoucher(Me.mRecurso.ToString.Trim, CInt(Me.mNroComercio.ToString.Trim), Me.mYear, Me.mQuota)

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function



    'ObtainListTrades:
    'This function returns the list of trades
    Public Function getListadoDeComercios(ByVal p_strComercios As String) As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.getListadoDeComercios(p_strComercios.Trim)


        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = "Valor: " & Me.mListTrades.ToString.Trim & vbCr & ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function



    'ObtainListTrades:
    'This function returns the list of trades
    Public Function obtainListTrades(ByVal p_strComercios As String) As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.obtainListTrades(p_strComercios.Trim)


        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = "Valor: " & Me.mListTrades.ToString.Trim & vbCr & ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function


    'ObtainDateOfExpiry:
    'This function returns periods with date of expiry bigger than today
    Public Function ObtainDateOfExpiry() As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.ObtainDateOfExpiry()

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function


    Public Function ObtainMunicipalidadData() As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.ObtainMunicipalidadData()

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function


    'AddDateExpiry:
    'This function returns periods with date of expiry bigger than today
    Public Function AddDateExpiry(ByVal pYear As Integer, ByVal pQuota As Integer, _
                                  ByVal pDateQuota1 As String, ByVal pDateQuota2 As String, ByVal pDate_p_DDJJ As String) As String


        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.Add_Date_Expiry(pYear, pQuota, pDateQuota1.Trim, pDateQuota2.Trim, pDate_p_DDJJ.Trim)

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function



    'ObtainList_DateExpiry:
    'This function returns 
    Public Function ObtainList_DateExpiry(ByVal pYear As Integer) As String


        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.ObtainList_DateExpiry(pYear)

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function




    'EditDateExpiry:
    'This function returns 
    Public Function EditDateExpiry(ByVal pYear As Integer, ByVal pQuota As Integer, _
                                   ByVal pDateQuota1 As String, ByVal pDateQuota2 As String, ByVal pDate_p_DDJJ As String) As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.Edit_Fec_Vtos(pYear, pQuota, pDateQuota1, pDateQuota2, pDate_p_DDJJ)

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function


    'Fec_Vto_CTACTE_Exist:
    'This function returns 
    Public Function Fec_Vto_CTACTE_Exist(ByVal pYear As Integer, ByVal pQuota As Integer) As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.Fec_Vto_CTACTE_Exist(pYear, pQuota)

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function




    'DeleteDateExpiry:
    'This function returns 
    Public Function DeleteDateExpiry(ByVal pYear As Integer, ByVal pQuota As Integer) As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.Delete_Fec_Vto(pYear, pQuota)

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function




    'ObtainDescriptionResourceTISH:
    'This function returns 
    Public Function ObtainDescriptionResourceTISH() As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.ObtainDescriptionResourceTISH()

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function


    'VerificarParametros:
    Public Function VerificarParametros(ByVal pVerParametros As Boolean) As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.VerificarParametros(pVerParametros)

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function


    'ObtenerFechaActuaWeb:
    Public Function ObtenerFechaActuaWeb() As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.ObtenerFechaActuaWeb()

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function

    'Devuelve el articulo que se va a mostrar en la carga de una nueva DDJJ:
    Public Function getArticle() As String

        Try
            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.getArticle()

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function


    'Checkeo si usan el valor cantidad empleados para la DDJJ:
    Public Function getEmployeesUsing() As String

        Try
            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.getEmployeesUsing()

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function



    'Obtengo los vencimientos de un período particular:
    Public Function getFechasVtoPeriodo() As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.getFechasVtoPeriodo()

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.ToString.Trim
    End Function



    'Obtengo un listado de los rubros existentes
    Public Function getRubros() As String

        Try
            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.getRubros()

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function


    'Obtengo los rubros asociados a un comercio
    Public Function getRubrosByComercio(ByVal p_strNroComercio As String) As String

        Try
            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.getRubrosByComercio(p_strNroComercio.Trim())

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function

    'Obtengo los datos de un contribuyente a partir de un Cuit
    Public Function getDatosContribuyenteByCuit(ByVal pNroCuit As String) As String

        Try
            Call Me.Limpiar()
            Me.mDLL = New tish.tishweb
            Me.mXML = Me.mDLL.getDatosContribuyentebyCuit(pNroCuit.Trim())

        Catch ex As Exception
            If (Me.mDebug = "N") Then
                Call MyBase.GenerarLog(ex)
                Me.mXML = "Error"
            Else
                Me.mXML = ex.Message.Trim
            End If
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function


#End Region
End Class

