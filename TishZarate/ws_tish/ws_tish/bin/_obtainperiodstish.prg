
* _ObtainPeriodsTISH:
* This function returns the periods that the municipalidad was using to receive the tish
PROCEDURE _ObtainPeriodsTISH
PRIVATE mResultPeriods as String
PRIVATE mCurrentPeriod as String
PRIVATE mResult_XML as String
PRIVATE i as Integer


	* Seteo the variables
	mResultPeriods = ''
	mResult_XML = ''
	
	
	* I create the periods
	mResultPeriods = '<PERIODS>'
	FOR i = 1 TO 12
		mResultPeriods = mResultPeriods + '<PERIOD>' + _S_ + STR(i) + _S_ +'</PERIOD>' + _S_
	ENDFOR
	mResultPeriods = mResultPeriods + '</PERIODS>'


	* I obtain the current period
	* mCurrentPeriod = '<CURRENT_PERIOD>' + _S_ + '<PERIOD>' + STR(MONTH(DATE() -1)) + '</PERIOD>' + _S_ + '</CURRENT_PERIOD>'

	mCurrentPeriod = '<CURRENT_PERIOD>' + _S_ + '<PERIOD>' + STR(99) + '</PERIOD>' + _S_ + '</CURRENT_PERIOD>'


	* I create the container XML
	mResult_XML = '<PERIODS_XML>' + _S_ + ALLTRIM(mResultPeriods) + _S_ + ALLTRIM(mCurrentPeriod) + _S_ + '</PERIODS_XML>'


	RETURN ALLTRIM(mResult_XML)
ENDPROC