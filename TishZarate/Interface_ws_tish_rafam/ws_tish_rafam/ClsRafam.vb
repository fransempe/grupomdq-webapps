﻿Option Explicit On
Option Strict On


'DB ORACLE
Imports System.Data.OleDb
Imports System.Data.OracleClient


Public Class ClsRafam

#Region "Variables"

    Private mConexion As OracleConnection
    Private mAdaptador As New OracleDataAdapter
    Private mComando As New OracleCommand()
    Private mCadenaConexion As String



    Private mHost As String
    Private mPuerto As String
    Private mServidor As String
    Private mServicio As String


    Private mUsuarioRAFAM As String
    Private mClaveRAFAM As String

    Private mNroComercio As Long
    Private mContribuyenteCuit As String



    Private mKeyManager As String
    Private mNew_KeyManager As String



    Private mDataManager As String
    Private mDataTrades As String


    Private mCuit As String

    Private mMotive_Down As String
    
#End Region



#Region "Propertys"

    Public WriteOnly Property Host() As String
        Set(ByVal value As String)
            Me.mHost = value.ToString.Trim
        End Set
    End Property

    Public WriteOnly Property Puerto() As String
        Set(ByVal value As String)
            Me.mPuerto = value.ToString.Trim
        End Set
    End Property

    Public WriteOnly Property Servidor() As String
        Set(ByVal value As String)
            Me.mServidor = value.ToString.Trim
        End Set
    End Property

    Public WriteOnly Property Servicio() As String
        Set(ByVal value As String)
            Me.mServicio = value.ToString.Trim
        End Set
    End Property

    Public WriteOnly Property UsuarioRAFAM() As String
        Set(ByVal value As String)
            Me.mUsuarioRAFAM = value.ToString.Trim
        End Set
    End Property

    Public WriteOnly Property ClaveRAFAM() As String
        Set(ByVal value As String)
            Me.mClaveRAFAM = value.ToString.Trim
        End Set
    End Property


    Public WriteOnly Property NroComercio() As Long
        Set(ByVal value As Long)
            Me.mNroComercio = value
        End Set
    End Property


    Public WriteOnly Property ContribuyenteCuit() As String
        Set(ByVal value As String)
            Me.mContribuyenteCuit = value.ToString.Trim
        End Set
    End Property


    Public WriteOnly Property KeyManager() As String
        Set(ByVal value As String)
            Me.mKeyManager = value.ToString.Trim
        End Set
    End Property


    Public WriteOnly Property New_KeyManager() As String
        Set(ByVal value As String)
            Me.mNew_KeyManager = value.ToString.Trim
        End Set
    End Property


    Public WriteOnly Property DataManager() As String
        Set(ByVal value As String)
            Me.mDataManager = value.ToString.Trim
        End Set
    End Property


    Public WriteOnly Property DataTrades() As String
        Set(ByVal value As String)
            Me.mDataTrades = value.ToString.Trim
        End Set
    End Property



    Public WriteOnly Property Cuit() As String
        Set(ByVal value As String)
            Me.mCuit = value.Trim
        End Set
    End Property


    Public WriteOnly Property Motive_Down() As String
        Set(ByVal value As String)
            Me.mMotive_Down = value.ToString.Trim
        End Set
    End Property

#End Region


#Region "Contructores"

    Public Sub New()

        Me.mConexion = Nothing
        Me.mAdaptador = Nothing
        Me.mComando  = NOTHING
        Me.mCadenaConexion = ""


        Me.mHost = ""
        Me.mPuerto = ""
        Me.mServidor = ""
        Me.mServicio = ""


        Me.mUsuarioRAFAM = ""
        Me.mClaveRAFAM = ""

        Me.mNroComercio = 0
        Me.ContribuyenteCuit = ""


        Me.mKeyManager = ""
        Me.mNew_KeyManager = ""

        Me.DataManager = ""
        Me.DataTrades = ""

        Me.mCuit = ""
        Me.mMotive_Down = ""

    End Sub

    Protected Overrides Sub Finalize()

        Me.mConexion = Nothing
        Me.mAdaptador = Nothing
        Me.mComando = Nothing
        Me.mCadenaConexion = ""


        Me.mHost = ""
        Me.mPuerto = ""
        Me.mServidor = ""
        Me.mServicio = ""


        Me.mUsuarioRAFAM = ""
        Me.mClaveRAFAM = ""

        Me.mNroComercio = 0
        Me.ContribuyenteCuit = ""


        Me.mKeyManager = ""
        Me.mNew_KeyManager = ""


        Me.DataManager = ""
        Me.DataTrades = ""
        Me.mCuit = ""

        Me.mMotive_Down = ""


        MyBase.Finalize()
    End Sub

    
#End Region


#Region "Funciones Privadas"

    'ObtenerCadenaConexion:
    'Esta funcion genera y devuelve la cadena de conexion a la base de datos
    Private Function ObtenerCadenaConexion() As String
        Dim mCadenaConexion As String

        mCadenaConexion = " Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)" & _
                          " (HOST=" & Me.mHost.ToString & ")" & _
                          " (PORT=" & Me.mPuerto.ToString & "))) " & _
                          " (CONNECT_DATA=(SERVER=" & Me.mServidor.ToString & ")" & _
                          " (SERVICE_NAME=" & Me.mServicio.ToString & "))); " & _
                          " User Id = OWNER_RAFAM;" & _
                          " Password = OWNERDBA;"



       



        Return mCadenaConexion.ToString.Trim
    End Function

    'InvertirCadena:
    'Esta funcion devuelve una cadena invertida
    Private Function InvertirCadena(ByVal mCadena As String) As String
        Dim mCadena_AUX As String
        Dim i As Integer


        mCadena_AUX = ""
        For i = mCadena.ToString.Trim.Length - 1 To 0 Step -1
            mCadena_AUX = mCadena_AUX & mCadena.ToString.Trim.Substring(i, 1)
        Next


        Return mCadena_AUX.ToString.Trim
    End Function

#End Region

#Region "Funciones WS"

    Public Function VerRuta_CadenaConexion() As String
        Dim mCadenaConexion As String
        mCadenaConexion = ""
        Return mCadenaConexion
    End Function



#Region "Funciones Publicas"


    Public Function Conectar() As Boolean
        Dim mConexion_OK As Boolean

        Try
            mConexion_OK = False
            Me.mConexion = New OracleConnection
            Me.mCadenaConexion = Me.ObtenerCadenaConexion()
            Me.mConexion.ConnectionString = Me.mCadenaConexion.ToString.Trim
            Me.mConexion.Open()
            Me.mConexion.Close()

            mConexion_OK = True
        Catch ex As Exception


            Call Me.GenerarLog(ex)

            mConexion_OK = False
        Finally
            Me.mConexion = Nothing
        End Try


        Return mConexion_OK
    End Function


    Public Function Login(ByVal pCuit As String, ByVal pClave As String) As String
        Dim mNroComercio_AUX As String

        Try

            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TISH_PKG.LOGIN"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("pCUIT", OracleType.VarChar)).Value = pCuit.ToString.Trim
            mComando.Parameters.Add(New OracleParameter("pCLAVE", OracleType.VarChar)).Value = pClave.ToString.Trim
            mComando.Parameters.Add(New OracleParameter("mNroComercio", OracleType.VarChar, 30)).Direction = ParameterDirection.ReturnValue



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Analizo el Resultado
            mNroComercio_AUX = ""
            If Not (IsDBNull(mComando.Parameters.Item("mNroComercio").Value)) Then
                mNroComercio_AUX = mComando.Parameters.Item("mNroComercio").Value.ToString()
            End If


            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            Call GenerarLog(ex)
            mNroComercio_AUX = "Error"
        Finally
            mComando = Nothing
            mConexion = Nothing
        End Try


        Return mNroComercio_AUX.ToString.Trim
    End Function



    'Esta funcion valida el usuario de RAFAM
    'Esta validacion se realiza intentando conectar con el usuario ingresado y 
    'la siguiente estructura armada con su contraseña:
    'Donde "R" es un valor fijo y lo demas es la clave concatenada con la clave invertida
    'CLAVE: hola -> Rholaaloh
    Public Function LoginUsuariosRAFAM() As Boolean
        Dim mCadenaConexion_AUX As String
        Dim mClave_AUX As String
        Dim mClave_Invertida As String
        Dim mOK_LOGIN_RAFAM As Boolean


        Try
            mOK_LOGIN_RAFAM = False
            Me.mConexion = New OracleConnection


            'Creo la Clave
            mClave_Invertida = InvertirCadena(Me.mClaveRAFAM.ToString.Trim)
            mClave_AUX = "R" & Me.mClaveRAFAM.ToString.Trim & mClave_Invertida.ToString




            'Genero la Cadena de Conexion
            mCadenaConexion_AUX = " Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)" & _
                                  " (HOST=" & Me.mHost.ToString.Trim & ")" & _
                                  " (PORT=" & Me.mPuerto.ToString.Trim & "))) " & _
                                  " (CONNECT_DATA=(SERVER=" & Me.mServidor.ToString.Trim & ")" & _
                                  " (SERVICE_NAME=" & Me.mServicio.ToString.Trim & "))); " & _
                                  " User Id=" & Me.mUsuarioRAFAM.ToString.ToUpper & ";" & _
                                  " Password=" & mClave_AUX.ToString.Trim & ";"





            Me.mConexion.ConnectionString = mCadenaConexion_AUX.ToString
            Me.mConexion.Open()
            Me.mConexion.Close()
            mOK_LOGIN_RAFAM = True
        Catch ex As Exception
            Call GenerarLog(ex)
            mOK_LOGIN_RAFAM = False
        Finally
            Me.mConexion = Nothing
        End Try

        Return mOK_LOGIN_RAFAM
    End Function



    Public Function CrearComercio() As String
        Dim mNroComercio_AUX As String

        Try

            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TISH_PKG.CREAR_COMERCIO_WEB"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("pNroComercio", OracleType.Number)).Value = Me.mNroComercio
            mComando.Parameters.Add(New OracleParameter("pContribuyenteCuit", OracleType.VarChar)).Value = Me.mContribuyenteCuit.ToString
            mComando.Parameters.Add(New OracleParameter("mClave", OracleType.VarChar, 32)).Direction = ParameterDirection.ReturnValue



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Analizo el Resultado
            mNroComercio_AUX = ""
            If Not (IsDBNull(mComando.Parameters.Item("mClave").Value)) Then
                mNroComercio_AUX = mComando.Parameters.Item("mClave").Value.ToString()
            End If


            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            Call GenerarLog(ex)
            mNroComercio_AUX = "Error"
        Finally
            mComando = Nothing
            mConexion = Nothing
        End Try


        Return mNroComercio_AUX.ToString.Trim
    End Function


    Public Function CreateUserWeb() As String
        Dim mNroComercio_AUX As String

        Try

            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TISH_PKG.CREATE_USER_WEB"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("pDataManager", OracleType.VarChar)).Value = Me.mDataManager.ToString.Trim
            mComando.Parameters.Add(New OracleParameter("pDataTrades", OracleType.VarChar)).Value = Me.mDataTrades.ToString.Trim
            mComando.Parameters.Add(New OracleParameter("mKey", OracleType.VarChar, 100)).Direction = ParameterDirection.ReturnValue



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Analizo el Resultado
            mNroComercio_AUX = ""
            If Not (IsDBNull(mComando.Parameters.Item("mKey").Value)) Then
                mNroComercio_AUX = mComando.Parameters.Item("mKey").Value.ToString()
            End If


            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            Call GenerarLog(ex)
            mNroComercio_AUX = "Error"
        Finally
            mComando = Nothing
            mConexion = Nothing
        End Try


        Return mNroComercio_AUX.ToString.Trim
    End Function


    Public Function EditUserWeb() As Boolean
        Dim mOK As Boolean

        Try

            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TISH_PKG.EDIT_USER_WEB"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("pDataManager", OracleType.VarChar)).Value = Me.mDataManager.ToString.Trim
            mComando.Parameters.Add(New OracleParameter("pDataTrades", OracleType.VarChar)).Value = Me.mDataTrades.ToString.Trim
            mComando.Parameters.Add(New OracleParameter("mOK", OracleType.VarChar, 5)).Direction = ParameterDirection.ReturnValue



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Analizo el Resultado
            mOK = False
            If Not (IsDBNull(mComando.Parameters.Item("mOK").Value)) Then
                mOK = CBool(mComando.Parameters.Item("mOK").Value.ToString())
            End If


            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            Call GenerarLog(ex)
            mOK = False
        Finally
            mComando = Nothing
            mConexion = Nothing
        End Try


        Return mOK
    End Function


    Public Function ClearKey() As String
        Dim mNroComercio_AUX As String

        Try

            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TISH_PKG.CLEAR_KEY"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("pCUIT", OracleType.VarChar)).Value = Me.mCuit.ToString.Trim
            mComando.Parameters.Add(New OracleParameter("mKey", OracleType.VarChar, 32)).Direction = ParameterDirection.ReturnValue



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Analizo el Resultado
            mNroComercio_AUX = ""
            If Not (IsDBNull(mComando.Parameters.Item("mKey").Value)) Then
                mNroComercio_AUX = mComando.Parameters.Item("mKey").Value.ToString()
            End If


            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            Call GenerarLog(ex)
            mNroComercio_AUX = "Error"
        Finally
            mComando = Nothing
            mConexion = Nothing
        End Try


        Return mNroComercio_AUX.ToString.Trim
    End Function

    'DownUsers
    'This function edit the key of the Manager
    Public Function DownUsers() As String
        Dim mDown_OK As String

        Try

            'Instancio the connection
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio the command
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TISH_PKG.DOWN_USERS"
            mComando.CommandType = CommandType.StoredProcedure



            'I assign parameters of entry and exit
            mComando.Parameters.Add(New OracleParameter("pCUIT", OracleType.VarChar)).Value = Me.mCuit.Trim
            mComando.Parameters.Add(New OracleParameter("pMotive_Down", OracleType.VarChar)).Value = Me.mMotive_Down.ToString.Trim
            mComando.Parameters.Add(New OracleParameter("mDown_OK", OracleType.VarChar, 32)).Direction = ParameterDirection.ReturnValue



            'I execute the function
            mComando.ExecuteNonQuery()


            'I analyze the result
            mDown_OK = ""
            If Not (IsDBNull(mComando.Parameters.Item("mDown_OK").Value)) Then
                mDown_OK = mComando.Parameters.Item("mDown_OK").Value.ToString()
            End If


            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            Call GenerarLog(ex)
            mDown_OK = "Error"
        Finally
            mComando = Nothing
            mConexion = Nothing
        End Try


        Return mDown_OK.ToString.Trim
    End Function


    'UpUsers
    'This function edit the key of the Manager
    Public Function UpUsers() As String
        Dim mUp_OK As String

        Try

            'Instancio the connection
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio the command
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TISH_PKG.UP_USERS"
            mComando.CommandType = CommandType.StoredProcedure



            'I assign parameters of entry and exit
            mComando.Parameters.Add(New OracleParameter("pCUIT", OracleType.VarChar)).Value = Me.mCuit.Trim
            mComando.Parameters.Add(New OracleParameter("mUp_OK", OracleType.VarChar, 32)).Direction = ParameterDirection.ReturnValue



            'I execute the function
            mComando.ExecuteNonQuery()


            'I analyze the result
            mUp_OK = ""
            If Not (IsDBNull(mComando.Parameters.Item("mUp_OK").Value)) Then
                mUp_OK = mComando.Parameters.Item("mUp_OK").Value.ToString()
            End If


            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            Call GenerarLog(ex)
            mUp_OK = "Error"
        Finally
            mComando = Nothing
            mConexion = Nothing
        End Try


        Return mUp_OK.ToString.Trim
    End Function




    'EditKey
    'This function edit the key of the Manager
    Public Function EditKey() As String
        Dim mEdit_OK As String

        Try

            'Instancio the connection
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio the command
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TISH_PKG.EDIT_KEY"
            mComando.CommandType = CommandType.StoredProcedure



            'I assign parameters of entry and exit
            mComando.Parameters.Add(New OracleParameter("pCUIT", OracleType.VarChar)).Value = Me.mCuit.ToString.Trim
            mComando.Parameters.Add(New OracleParameter("pKeyManager", OracleType.VarChar)).Value = Me.mKeyManager.ToString
            mComando.Parameters.Add(New OracleParameter("pNew_KeyManager", OracleType.VarChar)).Value = Me.mNew_KeyManager.ToString
            mComando.Parameters.Add(New OracleParameter("mEdit_OK", OracleType.VarChar, 32)).Direction = ParameterDirection.ReturnValue



            'I execute the function
            mComando.ExecuteNonQuery()


            'I analyze the result
            mEdit_OK = ""
            If Not (IsDBNull(mComando.Parameters.Item("mEdit_OK").Value)) Then
                mEdit_OK = mComando.Parameters.Item("mEdit_OK").Value.ToString()
            End If


            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            Call GenerarLog(ex)
            mEdit_OK = "Error"
        Finally
            mComando = Nothing
            mConexion = Nothing
        End Try


        Return mEdit_OK.ToString.Trim
    End Function


    'ExisteComercio:
    'Esta funcion nos devuelve un boolean confirmando si existe un comercio en la tabla de comercios web 
    Public Function ExisteComercio() As String
        Dim mCUIT As String

        Try

            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TISH_PKG.EXISTE_COMERCIO"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("pNroComercio", OracleType.Number)).Value = Me.mNroComercio
            mComando.Parameters.Add(New OracleParameter("mCUIT", OracleType.VarChar, 11)).Direction = ParameterDirection.ReturnValue



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Analizo el Resultado
            mCUIT = ""
            If Not (IsDBNull(mComando.Parameters.Item("mCUIT").Value)) Then
                mCUIT = mComando.Parameters.Item("mCUIT").Value.ToString()
            End If


            mConexion.Close()
        Catch ex As Exception


            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            Call GenerarLog(ex)
            mCUIT = ""
        Finally
            mComando = Nothing
            mConexion = Nothing
        End Try


        Return mCUIT.ToString.Trim
    End Function




    Public Function ObtainDataManager() As DataSet
        Dim dsDataManager As DataSet


        Try

            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TISH_PKG.OBTAIN_DATA_MANAGER"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("pCUIT", OracleType.VarChar)).Value = Me.mCuit.trim
            mComando.Parameters.Add(New OracleParameter("pCursorDataManager", OracleType.Cursor)).Direction = ParameterDirection.Output
            mComando.Parameters.Add(New OracleParameter("pCursorDataTrades", OracleType.Cursor)).Direction = ParameterDirection.Output




            'Ejecuto el Comando y Obtengo los datos en Dataset
            dsDataManager = New DataSet
            mAdaptador = New OracleDataAdapter(mComando)
            mAdaptador.Fill(dsDataManager, "DATOS")
            mConexion.Close()


            'Asigno Nombre a las etiquetas
            dsDataManager.Tables(0).TableName = "Manager"
            dsDataManager.Tables(1).TableName = "Trades"
            dsDataManager.DataSetName = "DataManager"


            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            Call GenerarLog(ex)
            dsDataManager = Nothing
        Finally
            mComando = Nothing
            mConexion = Nothing
        End Try


        Return dsDataManager
    End Function




#End Region



#Region "Obtener LOGO"

    ' Esta Funcion Devuelve un array de byte con LOGO
    Public Function ObtenerLogo() As DataSet
        Dim dsdatosLogo As DataSet


        Try
            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TRIBUTARIO_PKG.OBTENER_LOGO"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("mCadenaDatosContribuyente", OracleType.Cursor)).Direction = ParameterDirection.ReturnValue


            'Obtener Logo
            dsdatosLogo = New DataSet
            mAdaptador = New OracleDataAdapter(mComando)
            mAdaptador.Fill(dsdatosLogo, "DATOS")
            mConexion.Close()




            mComando = Nothing
            mConexion.Close()

            Return dsdatosLogo
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            mConexion = Nothing
            Call GenerarLog(ex)
            Return Nothing
        End Try
    End Function

#End Region

#End Region

#Region "Genero LOG"
    Private Sub GenerarLog(ByVal mError As Exception)

        'Genero un txt con el error 
        Call LogTXT(mError)

    End Sub

    Private Sub LogTXT(ByVal mError As Exception)
        Dim mRutaFisicaDirectorio As String


        mRutaFisicaDirectorio = My.Application.Info.DirectoryPath.ToString
        If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisicaDirectorio.ToString & "log")) Then
            My.Computer.FileSystem.CreateDirectory(mRutaFisicaDirectorio.ToString & "log")
        End If



        Dim mLog As New IO.StreamWriter(mRutaFisicaDirectorio.ToString & "log\log" & Format(Now, "ddMMyyyyHHmmss") & ".txt")
        Dim i As Integer
        Dim mCadenaError(0 To 4) As String



        'Armo el Log
        mCadenaError(0) = "FECHA: " & Format(Now, "dd/MM/yy hh:mm:ss")
        mCadenaError(1) = "MENSAJE: " & mError.Message.ToString
        mCadenaError(2) = "SOURCE: " & mError.Source.ToString
        mCadenaError(3) = "TARGETSITE: " & mError.TargetSite.ToString
        mCadenaError(4) = "STACKTRACE (Ruta): " & mError.StackTrace.ToString


        'Escribo el Log
        mLog.WriteLine("Descripcion de ERROR:")
        mLog.WriteLine("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
        For i = 0 To 4
            mLog.WriteLine(mCadenaError(i).ToString)
        Next
        mLog.WriteLine("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")


        mLog.Close()
    End Sub

#End Region







    'Esta funcion valida el usuario de RAFAM
    'Esta validacion se realiza intentando conectar con el usuario ingresado y 
    'la siguiente estructura armada con su contraseña:
    'Donde "R" es un valor fijo y lo demas es la clave concatenada con la clave invertida
    'CLAVE: hola -> Rholaaloh
    Public Function LoginUsuariosRAFAM2() As String
        Dim mCadenaConexion_AUX As String
        Dim mClave_AUX As String
        Dim mClave_Invertida As String
        Dim mOK_LOGIN_RAFAM As String

        mCadenaConexion_AUX = ""
        Try
            mOK_LOGIN_RAFAM = ""
            Me.mConexion = New OracleConnection


            'Creo la Clave
            mClave_Invertida = InvertirCadena(Me.mClaveRAFAM.ToString.Trim)
            mClave_AUX = "R" & Me.mClaveRAFAM.ToString.Trim & mClave_Invertida.ToString




            'Genero la Cadena de Conexion
            mCadenaConexion_AUX = " Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)" & _
                                  " (HOST=" & Me.mHost.ToString.Trim & ")" & _
                                  " (PORT=" & Me.mPuerto.ToString.Trim & "))) " & _
                                  " (CONNECT_DATA=(SERVER=" & Me.mServidor.ToString.Trim & ")" & _
                                  " (SERVICE_NAME=" & Me.mServicio.ToString.Trim & "))); " & _
                                  " User Id=" & Me.mUsuarioRAFAM.ToString.ToUpper & ";" & _
                                  " Password=" & mClave_AUX.ToString.Trim & ";"





            Me.mConexion.ConnectionString = mCadenaConexion_AUX.ToString
            Me.mConexion.Open()
            Me.mConexion.Close()
            mOK_LOGIN_RAFAM = "OK"
        Catch ex As Exception
            Call GenerarLog(ex)
            mOK_LOGIN_RAFAM = ex.ToString & vbCr & "Connectionstring: " & mCadenaConexion_AUX.Trim
        Finally
            Me.mConexion = Nothing
        End Try

        Return mOK_LOGIN_RAFAM
    End Function



    Public Function UserWebExist() As Boolean
        Dim mExist As Boolean

        Try

            'Instancio la Conexion
            mConexion = New OracleConnection
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TISH_PKG.USER_WEB_EXIST"
            mComando.CommandType = CommandType.StoredProcedure



            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("pCUIT", OracleType.VarChar)).Value = Me.mCuit.ToString.Trim
            mComando.Parameters.Add(New OracleParameter("mExist", OracleType.Number)).Direction = ParameterDirection.ReturnValue



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


            'Analizo el Resultado
            mExist = False
            If Not (IsDBNull(mComando.Parameters.Item("mExist").Value)) Then
                mExist = CBool((CDbl(mComando.Parameters.Item("mExist").Value) = 1))
            End If


            mConexion.Close()
        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            Call GenerarLog(ex)
            mExist = False
        Finally
            mComando = Nothing
            mConexion = Nothing
        End Try


        Return mExist
    End Function



    Public Function ObtenerTipoUsuario() As String


        Try

            'Instancio la Conexion
            mConexion = New OracleConnection
            mCadenaConexion = ObtenerCadenaConexion()
            mConexion.ConnectionString = mCadenaConexion.ToString
            mConexion.Open()



            'Instancio el Comando
            mComando = Nothing
            mComando = New OracleCommand()
            mComando.Connection = mConexion
            mComando.CommandText = "GMDQWEB_TISH_PKG.OBTENER_TIPO_USUARIO"
            mComando.CommandType = CommandType.StoredProcedure


            'Asigno Parametros de Entrada o Salida
            mComando.Parameters.Add(New OracleParameter("mTipoUsuario", OracleType.VarChar, 10)).Direction = ParameterDirection.ReturnValue
            mComando.Parameters.Add(New OracleParameter("pUsuario", OracleType.VarChar)).Value = Me.mUsuarioRAFAM.ToString.Trim



            'Ejecuto la Funcion
            mComando.ExecuteNonQuery()


 
            Return mComando.Parameters.Item("mTipoUsuario").Value.ToString


            mComando = Nothing
            mConexion.Close()

        Catch ex As Exception
            If (mConexion.State = ConnectionState.Open) Then
                mConexion.Close()
            End If

            Call GenerarLog(ex)
        Finally
            mComando = Nothing
            mConexion = Nothing
        End Try

    End Function
 
End Class


