﻿Option Explicit On
Option Strict On

Partial Public Class webfrmcambiar_clave
    Inherits System.Web.UI.Page


#Region "Variables"
    Private mWS As ws_tish.Service1
    Private mXML As String

    Private dtDataComercio_AUX As DataTable
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mEvent As String

        If Not (IsPostBack) Then


            Me.p_mensaje.Visible = False
            If (CBool(Session("own_key"))) Then
                Me.p_mensaje.Visible = True


                ' Si es el primer ingreso el usuario no puede ir a otro parte del sistema hasta que personalice
                ' su clave
                Me.h1_accesos.Visible = False
                Me.MenuNormal.Visible = False
                Me.MenuZarate.Visible = False
                Me.li_misdatos.Visible = False
                Me.li_cambiarclave.Visible = False
            Else

                lblname_user.Text = Session("NombreUsuario-Cuit").ToString.Trim

                'Si es Zarate que oculte/muestre el menu.
                If System.Configuration.ConfigurationManager.AppSettings("zarate").ToString.Trim() = "True" Then
                    MenuZarate.Visible = True
                    MenuNormal.Visible = False
                Else
                    MenuZarate.Visible = False
                    MenuNormal.Visible = True
                End If
            End If


            Call Me.SloganMuniicpalidad()
            Call Me.ValidarSession()
            Call Me.CargarPie()


            'I assign events javasript
            Me.btncambiar_clave.Attributes.Add("onclick", "javascript:return validate_data();")
            Me.txtclave_anterior.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtclave_anterior.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtclave_anterior.Attributes.Add("onkeyup", "javascript:quantity_characters('txtclave_anterior', 'lblquantity_characters_key_old');")

            Me.txtclave_nueva.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtclave_nueva.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtclave_nueva.Attributes.Add("onkeyup", "javascript:quantity_characters('txtclave_nueva', 'lblquantity_characters_key_new');")

            Me.txtconfirmar_clave.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtconfirmar_clave.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtconfirmar_clave.Attributes.Add("onkeyup", "javascript:quantity_characters('txtconfirmar_clave', 'lblquantity_characters_key_confirm');")



            'I assign the focus
            Me.txtclave_anterior.Focus()

        Else

            mEvent = Request.Params.Get("__EVENTTARGET")
            If (mEvent.ToString.Trim = "change_key") Then
                Call Me.ChangeKey()
            End If

        End If
    End Sub

    Private Sub SloganMuniicpalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema TISH."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema TISH."
        End If

    End Sub


    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("contribuyente_nombre") IsNot Nothing) Then
                If (Session("contribuyente_nombre").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        If (mLogin_OK) Then

            'I Update the dataset
            'dtDataComercio_AUX = UpdatedsComercio(CType(Session("data_trade"), DataTable))
            'lblcomercio_user.Text = dtDataComercio_AUX.Rows(0).Item("COMERCIO_NUMERO").ToString.Trim()
            Me.lblcuit_manager.Text = Session("cuit_manager").ToString.Trim
        Else
            Response.Redirect("webfrmlogin.aspx", False)
        End If

    End Function

    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            Me.lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            Me.lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If


        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub


    Protected Sub btncambiar_clave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btncambiar_clave.Click
        Call Me.ChangeKey()
    End Sub

    Private Sub ChangeKey()
        Dim mEdicion_OK As String
        Try

            If (Me.ValidarDatos()) Then


                If (Session("cuit_manager").ToString.Trim <> "") Then


                    'Obtengo los datos
                    Me.mWS = New ws_tish.Service1
                    mEdicion_OK = Me.mWS.EditKey(Session("cuit_manager").ToString.Trim, txtclave_anterior.Text.Trim, txtclave_nueva.Text.Trim)
                    Me.mWS = Nothing




                    Me.lblmensaje.Visible = False
                    Me.div_mensaje.Visible = False
                    If (mEdicion_OK <> "OK") Then
                        Me.lblmensaje.Visible = True
                        Me.div_mensaje.Visible = True
                        Me.lblmensaje.Text = mEdicion_OK.ToString.Trim

                        Me.txtclave_anterior.Text = ""
                        Me.txtclave_nueva.Text = ""
                        Me.txtconfirmar_clave.Text = ""

                    Else
                        Me.tabla_clave.Visible = False
                        Me.lblmensaje.Visible = True
                        Me.div_mensaje.Visible = True
                        Me.lblmensaje.Text = "CLAVE MODIFICADA."


                        Session("type_messenger") = "operation"
                        Session("messenger") = "El cambio de clave se ha realizado con éxito."
                        'Session("url") = "webfrmseleccionar_comercio.aspx"
                        Session("url") = "webfrmlogin.aspx"
                        Session("own_key") = True
                        Session("relogin") = True
                        Response.Redirect("webfrmresultados_genericos.aspx", False)
                    End If
                End If
            End If

        Catch ex As Exception
            Me.mWS = Nothing
        End Try
    End Sub


    'ValidarDatos:
    'This function valid the entered information and it return a boolean
    Private Function ValidarDatos() As Boolean

        'Clave anterior
        If (Me.txtclave_anterior.Text.Trim = "") Then
            Return False
        End If


        'Nueva clave
        If (Me.txtclave_nueva.Text.Trim = "") Then
            Return False
        End If


        'Nueva clave
        If (Me.txtconfirmar_clave.Text.Trim = "") Then
            Return False
        End If


        'Confirmacion
        If (Me.txtclave_nueva.Text.Trim <> Me.txtconfirmar_clave.Text.Trim) Then
            Return False
        End If


        Return True
    End Function


    Private Function CambiarClave() As Boolean
        Dim mEdicion_OK As String

        Try

            If (ValidarDatos()) Then

                If (Session("nro_comercio") IsNot Nothing) Then


                    'Obtengo los datos
                    Me.mWS = New ws_tish.Service1
                    mEdicion_OK = Me.mWS.EditKey(CLng(Session("cuit_manager").ToString.Trim).ToString.Trim, txtclave_anterior.Text.Trim, txtclave_nueva.Text.Trim)
                    Me.mWS = Nothing




                    Me.lblmensaje.Visible = False
                    Me.div_mensaje.Visible = False
                    If (mEdicion_OK <> "OK") Then
                        Me.lblmensaje.Visible = True
                        Me.div_mensaje.Visible = True
                        Me.lblmensaje.Text = mEdicion_OK.ToString.Trim

                        Me.txtclave_anterior.Text = ""
                        Me.txtclave_nueva.Text = ""
                        Me.txtconfirmar_clave.Text = ""
                        Me.txtclave_anterior.Focus()

                    Else
                        Me.tabla_clave.Visible = False
                        Me.lblmensaje.Visible = True
                        Me.div_mensaje.Visible = True
                        Me.lblmensaje.Text = "CLAVE MODIFICADA.-"

                    End If
                End If
            End If

        Catch ex As Exception
            Me.mWS = Nothing
        End Try
    End Function


End Class