﻿Partial Public Class webfrmdeclaracionesjuradas
    Inherits System.Web.UI.Page



#Region "Variables"
    Private mWS As ws_tish.Service1
    Private dtDataComercio_AUX As DataTable

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not (IsPostBack) Then

            Call Me.SloganMuniicpalidad()
            Call Me.ValidateSession()
            Call Me.LoadFooter()
            lblname_user.Text = Session("NombreUsuario-Cuit").ToString.Trim
        Else

            If (Request.Params.Get("__EVENTTARGET").Trim = "new") Then
                Session("Rectificativa") = False
                Response.Redirect("webfrmaltaddjj.aspx", False)
            Else

                Session("Rectificativa") = True
                Session("Rectificativa_Period") = Request.Params.Get("__EVENTARGUMENT").Trim
                Response.Redirect("webfrmaltaddjj.aspx", False)
            End If
        End If
    End Sub



    Private Sub SloganMuniicpalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema TISH."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema TISH."
        End If

    End Sub


    'ValidateSession:
    'This function validate the data of session
    Private Function ValidateSession() As Boolean
        Dim mLogin_OK As Boolean


        'flag Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'name user
        If (mLogin_OK) Then
            If (Session("contribuyente_nombre") IsNot Nothing) Then
                If (Session("contribuyente_nombre").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If




        If (mLogin_OK) Then

            'I Update the dataset
            dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))
            lblcomercio_user.Text = dtDataComercio_AUX.Rows(0).Item("COMERCIO_NUMERO").ToString.Trim()

        Else
            Response.Redirect("webfrmlogin.aspx", False)
        End If

    End Function


    'LoadFooter:
    'This function loads the information of the municipality in the foot of the page
    Private Sub LoadFooter()

        'name Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telephone Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            Me.lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            Me.lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If

        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub


End Class