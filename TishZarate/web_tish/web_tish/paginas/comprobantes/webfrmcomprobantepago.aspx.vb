﻿Option Explicit On
Option Strict On


Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Text


'Web Services
Imports web_tish.ws_tish.Service1


Partial Public Class webfrmcomprobantepago
    Inherits System.Web.UI.Page

#Region "Variables"

    Private Structure SLogo
        Dim LogoOrganismo As Byte()
        Dim RenglonOrganismo As String
        Dim RenglonNombreOrganismo As String
    End Structure

    Private mPDF As ClsPDFComprobantePago
    'Private mLog As Clslog
    Private mWS As web_tish.ws_tish.Service1
    Private mLogo As SLogo

#End Region

#Region "Eventos Formulario"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dsDatosVouchers_Expired_AUX As DataSet
        Dim dsDatosVouchers_No_Expired_AUX As DataSet
        Dim mRutaFisica As String
        Dim mNombreArchivoComprobante As String
        Dim mArchivo As String
        Dim mCodigoMunicipalidad As String
        Dim dtDataComercio_AUX As DataTable



        Try

            '  If Not (IsPostBack) Then
            If (Session("xml_vouchers_expired") IsNot Nothing) Or (Session("xml_vouchers_no_expired") IsNot Nothing) Then


                'Obtengo los datos
                dsDatosVouchers_Expired_AUX = Nothing
                If (Session("xml_vouchers_expired").ToString.Trim <> "") Then
                    dsDatosVouchers_Expired_AUX = New DataSet
                    dsDatosVouchers_Expired_AUX.ReadXml(New IO.StringReader(Session("xml_vouchers_expired").ToString.Trim))
                End If


                'Obtengo los datos
                dsDatosVouchers_No_Expired_AUX = Nothing
                If (Session("xml_vouchers_no_expired").ToString.Trim <> "") Then
                    dsDatosVouchers_No_Expired_AUX = New DataSet
                    dsDatosVouchers_No_Expired_AUX.ReadXml(New IO.StringReader(Session("xml_vouchers_no_expired").ToString.Trim))
                End If



                'I Update the dataset
                dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))








                'Creo Directorio y Archivo en Disco
                mRutaFisica = Request.ServerVariables("APPL_PHYSICAL_PATH").ToString & "comprobantespdf"
                If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString)) Then
                    My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString)
                End If
                mNombreArchivoComprobante = dtDataComercio_AUX.Rows(0).Item("COMERCIO_NUMERO").ToString.Trim & Format(Now, "HHmmss")
                mArchivo = mRutaFisica & "\" & mNombreArchivoComprobante & ".pdf"



                Session("type_messenger") = "pdf"
                Session("name_pdf") = mNombreArchivoComprobante.ToString.Trim


                'Creo y Seteo el Nombre del Logo dependiendo de la Conexion
                mCodigoMunicipalidad = ""
                'If (Application("tipo_conexion").ToString = "RAFAM") Then
                '    Call GenerarLogo()
                '    mCodigoMunicipalidad = ObtenerCodigoMunicipalidad()
                'End If





                'Genero el PDF
                mPDF = New ClsPDFComprobantePago

                'mPDF.TipoConexion  = Application("tipo_conexion").ToString


                'Estos Datos son para agregar al LOGO si esta Conectado a RAFAM
                'If (Application("tipo_conexion").ToString = "RAFAM") Then
                '    mPDF.Organismo = mLogo.RenglonOrganismo.ToString
                '    mPDF.NombreOrganismo = mLogo.RenglonNombreOrganismo.ToString
                'End If


                'Este Dato es utilizado si estoy conectado a RAFAM, es usado para GENERAR el BARCODE39
                mPDF.CodigoMunicipalidad = mCodigoMunicipalidad.ToString


                mPDF.RutaFisica = mArchivo.ToString
                mPDF.DatosMunicipalidad = Me.ObtenerDatosMunicipalidad()

                If (Session("municipalidad_nombre") IsNot Nothing) Then
                    mPDF.NombreMunicipalidad = Session("municipalidad_nombre").ToString.Trim
                End If



                mPDF.dataVouchers_Expired = dsDatosVouchers_Expired_AUX
                mPDF.dataVouchers_NO_Expired = dsDatosVouchers_No_Expired_AUX
                mPDF.CrearPDF()

                Response.Redirect("../webfrmresultados_genericos.aspx", False)


            Else
                Call LimpiarSession()
            End If

        Catch ex As Exception
            clsTools.log(ex)
            Response.Redirect("../webfrmerrorweb.aspx", False)
        End Try

    End Sub

#End Region

#Region "procedimientos y Funciones"



    Private Sub LimpiarSession()
        Session.RemoveAll()
        Session.Clear()
        Session.Abandon()
        Response.Redirect("../webfrmindex.aspx", False)
    End Sub

    Private Function ObtenerTipoCuenta(ByVal mTipoCuenta As String) As String
        Select Case mTipoCuenta
            Case "I" : Return "INMUEBLE"
            Case "C" : Return "COMERCIO"
            Case "E", "O" : Return "CEMENTERIO"
            Case "V" : Return "VEHICULOS"
            Case Else : Return ""
        End Select
    End Function

#End Region

#Region "LOG"

    Private Function ObtenerDetalleWEB() As String
        Dim mInfoLog As String = ""
        Try

            For Each mNombreVariable In Request.ServerVariables
                mInfoLog = mInfoLog & mNombreVariable.ToString & ": " & Request.ServerVariables(mNombreVariable.ToString)
                mInfoLog = mInfoLog & vbCrLf
            Next

            Return mInfoLog.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function
#End Region

    Private Function ObtenerDatoConfig(ByVal mNombreNodo As String, ByVal mNombreDato As String) As String
        Dim mRutaFisica As String
        Dim dsConfiguracion As DataSet
        '  Dim ObjSeguridad As ClsSeguridad
        Dim mNombreMunicipio As String


        Try

            'Obtengo la Ruta del archivo de Configuracion
            mRutaFisica = Server.MapPath("Configuracion.gmdq").ToString.Replace("Configuracion.gmdq", Nothing)
            mRutaFisica = mRutaFisica.ToString.Replace("\comprobantes", Nothing)
            dsConfiguracion = New DataSet
            dsConfiguracion.ReadXml(mRutaFisica.ToString & "Configuracion.gmdq")


            'Obtengo el Dato del archivo configuracion
            mNombreMunicipio = ""
            'ObjSeguridad = New ClsSeguridad
            'mNombreMunicipio = ObjSeguridad.DesEncriptar(dsConfiguracion.Tables(mNombreNodo.ToString).Rows(0).Item(mNombreDato.ToString).ToString)



            'Libero Memoria
            'ObjSeguridad = Nothing
            dsConfiguracion = Nothing


            Return mNombreMunicipio.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function


    Private Sub GenerarLogo()
        Dim dsdatosLogo As DataSet
        Dim mCadenaBytes As IO.MemoryStream
        Dim mLogoImagen As Image

        Try

            'Obtengo el LOGO
            'mWS = New ws_consulta_tributaria.Service1
            'dsdatosLogo = mWS.ObtenerLogo
            'mWS = Nothing


            'Obtengo los Datos para generar el Logo
            mLogo.RenglonOrganismo = ""
            mLogo.RenglonNombreOrganismo = ""
            mLogo.LogoOrganismo = CType(dsdatosLogo.Tables(0).Rows(0).Item("BMP"), Byte())
            mLogo.RenglonOrganismo = dsdatosLogo.Tables(0).Rows(0).Item("RPTORGANISMO").ToString
            mLogo.RenglonNombreOrganismo = dsdatosLogo.Tables(0).Rows(0).Item("RPTNOMBREORG").ToString


            'Genero un Imagen a partir del Array de Byte y lo guardo en disco
            mCadenaBytes = New IO.MemoryStream(mLogo.LogoOrganismo)
            mLogoImagen = Image.FromStream(mCadenaBytes)
            mLogoImagen.Save("C:\logo_rafam.jpg")


        Catch ex As Exception
            'mLogo.RenglonOrganismo = "Nombre No Obtenido"
            'mLogo.RenglonNombreOrganismo = "Nombre No Obtenido"

            'mLog = New Clslog
            'mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            'mLog = Nothing
        End Try

    End Sub

    Private Function ObtenerCodigoMunicipalidad() As String
        'Dim mCodigoMunicipalidad As String

        'Try

        '    'Obtengo Nombre Municpalidad
        '    mCodigoMunicipalidad = ""
        '    mWS = New ws_consulta_tributaria.Service1
        '    mCodigoMunicipalidad = mWS.ObtenerCodigoMunicipalidad
        '    mWS = Nothing

        '    Return mCodigoMunicipalidad.ToString.Trim
        'Catch ex As Exception
        '    mLog = New Clslog
        '    mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
        '    mLog = Nothing
        '    Response.Redirect("../webfrmerror.aspx", False)
        'Return ""
        'End Try

    End Function



    Private Function ObtenerDatosMunicipalidad() As DataSet
        Dim dsDatos As DataSet
        Dim mXML As String

        Try

            'Obtengo los datos de la municipalidad
            mWS = New ws_tish.Service1
            mXML = mWS.ObtainMunicipalidadData()
            mWS = Nothing


            dsDatos = New DataSet
            dsDatos = ClsTools.ObtainDataXML(mXML.ToString.Trim)

            Return dsDatos
        Catch ex As Exception
            'mLog = New Clslog
            'mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            'mLog = Nothing
            Response.Redirect("../webfrmerrorweb.aspx", False)
            Return Nothing
        End Try

    End Function

End Class