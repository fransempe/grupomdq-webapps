﻿Option Explicit On
Option Strict On


Partial Public Class webfrmlogin_rafam
    Inherits System.Web.UI.Page


#Region "Variables"
    Private mWS As ws_tish.Service1
    Private mXML As String
    Private ObjEncriptar As clsEncriptar
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not (IsPostBack) Then



            'Saber de donde viene
            Dim ip As String
            ip = Request.UserHostAddress


            'Limpio la Session por las dudas que el usuario este logueado y quiera volver a loguearse
            Call Me.LimpiarSession()

            'Obtengo los datos de la municipalidad para mostrarlos en el pie de la pagina y para asignar las variables de session
            Call Me.CargarDatosMunicipalidad()
            Call Me.SloganMunicipalidad()




            'I asssign the events javascript
            Me.link_login.Attributes.Add("onclick", "javascript:return validar_datos();")
            Me.txtcaptcha.Attributes.Add("onkeydown", "javascript:consultar_deuda(this, event);")

            Me.txtusuario.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtusuario.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtclave.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtclave.Attributes.Add("onblur", "javascript:event_focus(this);")
            Me.txtcaptcha.Attributes.Add("onfocus", "javascript:event_focus(this);")
            Me.txtcaptcha.Attributes.Add("onblur", "javascript:event_focus(this);")



            Me.txtusuario.Attributes.Add("onkeyup", "javascript:key_up_txt(event, 'txtclave');")
            Me.txtclave.Attributes.Add("onkeyup", "javascript:key_up_txt(event, 'txtcaptcha');")
            Me.txtcaptcha.Attributes.Add("onkeyup", "javascript:key_up_txt(event, 'link_login');")


            'I assign the focus
            Me.txtusuario.Focus()
        Else
            'Call LimpiarControles()
        End If

        Me.txtclave.CssClass = "keyboardInputInitiator"
    End Sub


    Private Sub SloganMunicipalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema TISH."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema TISH."
        End If

    End Sub


    Private Sub CargarDatosMunicipalidad()
        Dim dsDataMunicipalidad As DataSet
        Dim mMunicipalidadName As String


        Try

            'Obtengo los datos         
            Me.mWS = New ws_tish.Service1
            Me.mXML = Me.mWS.ObtainMunicipalidadData()
            Me.mWS = Nothing



            dsDataMunicipalidad = New DataSet
            dsDataMunicipalidad = clsTools.ObtainDataXML(Me.mXML.Trim)


            mMunicipalidadName = ""
            mMunicipalidadName = System.Configuration.ConfigurationManager.AppSettings("Nombre_Municipalidad").ToString.Trim()
            Me.lblnombre_municipalidad.Text = mMunicipalidadName.Trim
            Me.lbltelefono_municipalidad.Text = dsDataMunicipalidad.Tables(0).Rows(0).Item("MUNICIPALIDAD_TELEFONO").ToString.Trim
            Me.lblemail_municipalidad.Text = dsDataMunicipalidad.Tables(0).Rows(0).Item("MUNICIPALIDAD_MAIL").ToString.Trim
            Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim


            'Asigno las variables de session con los datos del municipio                    
            Session("municipalidad_nombre") = Me.lblnombre_municipalidad.Text
            Session("municipalidad_telefono") = Me.lbltelefono_municipalidad.Text
            Session("municipalidad_mail") = Me.lblemail_municipalidad.Text
            
        Catch ex As Exception
            Me.mWS = Nothing
        End Try
    End Sub

    Protected Sub link_login_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_login.Click
        Dim mLogin_OK As Boolean

        Try
            If (Validar()) Then


                'Limpio la Session por las dudas que el usuario este logueado y quiera volver a loguearse
                ' Call LimpiarSession()


                'Realizo el Logueo
                mLogin_OK = False
                Me.mWS = New ws_tish.Service1
                mLogin_OK = Me.mWS.LoginRAFAM(Me.txtusuario.Text.Trim, Me.txtclave.Text.Trim)
                Me.mWS = Nothing


                
                'Si el Logueo no es valido le doy un aviso al usuario
                If Not (mLogin_OK) Then
                    Call Me.GenerarMensaje("El usuario ingresado y/o la contraseña no son válidos", False)
                    Me.txtusuario.Text = ""
                    Me.txtclave.Text = ""
                    Me.txtcaptcha.Text = ""
                    Me.txtusuario.Focus()
                    Exit Sub
                End If



                'Asigno Variables de Session
                Session("login") = True



                'Obtengo los datos de la municipalidad para mostrarlos en el pie de la pagina y para asignar las variables de session
                Call Me.CargarDatosMunicipalidad()

                'Obtengo el tipo de usuario A / O (admin / operador) según el usuario ingresado.
                Call Me.CargarTipoUsuario()

                Session("user_name") = Me.txtusuario.Text.Trim.ToUpper
                Response.Redirect("webfrmopciones_rafam.aspx", False)
            End If


        Catch ex As Exception
            'mLog = New Clslog
            'mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            'mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)
        End Try

    End Sub

    Private Sub CargarTipoUsuario()
        Dim mTipoUsuario As String
        mTipoUsuario = ""
        Me.mWS = New ws_tish.Service1
        'Buscamos el tipo del usuario (A="Admin" / O="Operador") en base al Usuario ingresado
        mTipoUsuario = Me.mWS.ObtenerTipoUsuario(Me.txtusuario.Text.Trim)
        Session("Tipo_Usuario") = mTipoUsuario.Trim
        Me.mWS = Nothing
    End Sub


#Region "Procedimientos y Funciones"

    Private Function Validar() As Boolean

        'Usuario
        If (Me.txtusuario.Text.Trim = "") Then
            Return False
        End If


        'Clave
        If (Me.txtclave.Text.Trim = "") Then
            Return False
        End If


        'Validacion CAPTCHA
        Me.verificador_captcha.ValidateCaptcha(txtcaptcha.Text.Trim())
        If Not (Me.verificador_captcha.UserValidated) Then

            Call Me.GenerarMensaje("Código de Verificación Incorrecto.", True)
            Me.txtcaptcha.Text = ""
            Me.txtcaptcha.Focus()

            Return False
        End If




        Return True
    End Function

    Private Sub GenerarMensaje(ByVal mMensaje As String, ByVal EsCaptcha As Boolean)
        Me.lblmensaje.Visible = True
        Me.div_mensaje.Visible = True
        Me.lblmensaje.Text = mMensaje.ToString.Trim


        'Seteo colores al control txtcaptcha
        If (EsCaptcha) Then
            Me.txtcaptcha.BackColor = Drawing.Color.Red
            Me.txtcaptcha.ForeColor = Drawing.Color.White
        Else
            Me.txtcaptcha.BackColor = Drawing.Color.White
            Me.txtcaptcha.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff009900")
        End If
    End Sub


    'Si los datos son Invalidos borro cualquier Session
    Private Sub LimpiarSession()
        Session.RemoveAll()
        Session.Clear()
        Session.Abandon()
    End Sub

#End Region

End Class