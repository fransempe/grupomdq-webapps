﻿    //------------- FUNCIONES TRIM --------------------
    function ltrim(s) {
       return s.replace(/^\s+/, "");
    }

    function rtrim(s) {
       return s.replace(/\s+$/, "");
    }

    function trim(s) {
       return rtrim(ltrim(s));
    }
    //------------- FUNCIONES TRIM --------------------

  


    //------------- VALIDAT CUIT --------------------
    function validar_cuit(mcontrol_cuit) {       
        
        
            //Campo Vacio
            if (mcontrol_cuit.value == "") {
                alert('CUIT:\nDebe ingresar un CUIT');
                mcontrol_cuit.focus;
                mcontrol_cuit.select();
                return false;
            }


            //Solo Numeros
            if (isNaN(mcontrol_cuit.value)) {
                alert('CUIT:\nDebe ingresar solo números');
                mcontrol_cuit.focus;
                mcontrol_cuit.select();
                return false;
            }        
            
        
            //Cantidad de Numeros
            if (mcontrol_cuit.value.length != 11) {
                alert('CUIT:\nLa cantidad de números ingresados no son validos');
                mcontrol_cuit.focus;
                mcontrol_cuit.select();
                return false;
            }
            
        return true;          
    } 
    //-------------------------------------------------------------    








    //-------------------- Validar que la cadena solo Contenga Numero y Letras --------------------
    function validar_cadena(mcontrol, titulo) {
        var mcadena_valida = "0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz "
        var mok = "ok";
        var mtemp;
        
            for (var i=0; i< mcontrol.value.length; i++) {
                mtemp = "" + mcontrol.value.substring(i, i+1);
                    if (mcadena_valida.indexOf(mtemp) == "-1") mok = "no";
            }
              
            if (mok == "no") {
                alert(titulo + ':\n'+ 'Ingrese solo caracteres que sean letras o números.');
                mcontrol.focus();
                mcontrol.select();                       
                return false;
            }   
                  
        return true;
    }
    //------------------------------------------------------------------------------------------------ 
    
    
    
    //-------------------- Validar que la Cadena Solo Contenga Numeros --------------------
    function validar_cadena_solo_numeros(mcontrol, titulo_mensaje) {       
        var patron =/\d/; 
        var ok = true;
        
        
          try {
            // Valido el caracater espacio
            if (patron.test(mcontrol.value) == false) {
                ok = false;
            }
            
            //Valido que sea solo numeros
            if (isNaN(mcontrol.value)) {
                ok = false;
            }
            
            
            if (ok == false) {
                alert(titulo_mensaje + ':\n'+  'Solo debe Ingresar Números');
                mcontrol.focus;
                mcontrol.select();
                return false;
            }
            
        return true;
        
         }
        catch(err) {
            return true;
        }
            
    } 
    //---------------------------------------------------------------------------------------
    
    
    

    
    
    
    //-------------------- Validar que la caja de TEXTO VACIA -----------------------
    function control_vacio(mcontrol, mensaje) {
 
        try {
            if (mcontrol.value == "") {
                alert(mensaje);
                mcontrol.focus;
                mcontrol.select();
                return true;
            }
            return false;
            
        }
        catch(err) {
            return false;
        }
            
    }
    //-------------------------------------------------------------------------------------------
    
    
    
     

    //-------------------- Cerrar Session de Usuarios -----------------------
    function cerrar_sesion_usuario() {        
        return confirm("Usted esta a punto de cerrar su sesión\n¿Desea continuar?")              
    }
    //-----------------------------------------------------------------------
 
 
 
     //------------- VALIDAT DOMINIO --------------------
    function validar_cadena_dominio(mcontrol) {       
        
        
            //Campo Vacio
            if (mcontrol.value == "") {
                alert('Debe Ingresar un DOMINIO');
                mcontrol.focus;
                mcontrol.select();
                return false;
            }


            //Solo Numeros y Letras
            if (!validar_cadena(mcontrol, 'Dominio No Valido')) {
                mcontrol.focus;
                mcontrol.select();
                return false;
            }        
            
        
            //Cantidad de Digitos
            if (mcontrol.value.length < 6) {
                alert('La cantidad de dígitos no puede ser menor a cinco (5).');
                mcontrol.focus;
                mcontrol.select();
                return false;
            }
            
        return true;          
    } 
    //-------------------------------------------------------------    




    //------------- event_focus --------------------
    // this function assign the style the control   
    function event_focus(control_txt) {              
                  	    
        /* I assign the style */
        if (control_txt.className=='textbox_onfocus') {
            control_txt.className='textbox_onblur';
        } else {
            control_txt.className='textbox_onfocus';
        }    
    }
    //-------------------------------------------------------------    
    
    
    
    //------------- event_focus --------------------
    // this function assign the style the control   
    function assign_font(size_font) {
        switch (size_font)
          {
            case '-': {document.body.className = 'body_menor'; break}
            case '+': {document.body.className = 'body_mayor'; break}                   
            default: {document.body.className = 'body_igual'; break}                   
          }
        
    }
    //-------------------------------------------------------------    
    
    
    
    /* ----------------------------------------------------------------------------- 
    trade_change: 
    This function shows a cartel of notice to the user
    */    
    function trade_change() {        
        return confirm("Usted esta a punto de cambiar de comercio\n¿Desea continuar?")              
    }
    //-----------------------------------------------------------------------------






    //-------------------- Validar que la cadena solo Contenga Numero y Letras --------------------
    function validar_cadena_importe(mcontrol, titulo) {
        var mcadena_valida = "0123456789."
        var mok = "ok";
        var mtemp;
        
            for (var i=0; i< mcontrol.value.length; i++) {
                mtemp = "" + mcontrol.value.substring(i, i+1);
                    if (mcadena_valida.indexOf(mtemp) == "-1") mok = "no";
            }
              
            if (mok == "no") {
                alert(titulo + ':\n'+ 'Ingrese solo números.');
                mcontrol.focus();
                mcontrol.select();                       
                return false;
            }   
                  
        return true;
    }
    //------------------------------------------------------------------------------------------------ 





    //-------------------- Validar que la cadena solo Contenga Numero y Letras --------------------
    function validar_cadena_telefono(mcontrol, titulo) {
        var mcadena_valida = "0123456789-()"
        var mok = "ok";
        var mtemp;
        
            for (var i=0; i< mcontrol.value.length; i++) {
                mtemp = "" + mcontrol.value.substring(i, i+1);
                    if (mcadena_valida.indexOf(mtemp) == "-1") mok = "no";
            }
              
            if (mok == "no") {
                alert(titulo + ':\n'+ 'Ingrese número de teléfono válido.');
                mcontrol.focus();
                mcontrol.select();                       
                return false;
            }   
                  
        return true;
    }
    //------------------------------------------------------------------------------------------------ 
    
    
    
    
    
    //---------------------------------- to validate e-mail ------------------------------------------
    function validate_email(pcontrol_email) {    
        _Boss = /^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/
        
        
        if (!_Boss.exec(pcontrol_email.value)) {
            alert('E-mail:\n'+ 'La cuenta de correo electronico no tiene un formato valido.');
            pcontrol_email.focus();
            pcontrol_email.select();                       
            return false;        
        }
        
    return true;
    }
    //------------------------------------------------------------------------------------------------ 
    
    
    
    
    
    //------------------------------------ control exists --------------------------------------------      
         function control_exists(p_control) {
                return (p_control != undefined);
            }
    //------------------------------------------------------------------------------------------------ 