﻿Imports iTextSharp.text
Imports iTextSharp.text.pdf


Public Class ClsPDFComprobantePago

#Region "Variables"

    Private Structure SColumnas
        Dim Recurso As Integer
        Dim Anio As Integer
        Dim Cuota As Integer
        Dim Concepto As Integer
        Dim ImporteOrigen As Integer
        Dim ImporteRecargo As Integer
        Dim ImporteTotal As Integer
    End Structure

    Private dsDatos As DataSet
    Private dsVouchers_Expired As DataSet
    Private dsVouchers_NO_Expired As DataSet

    Private mNombreMunicipalidad As String
    Private mRutaFisica As String
    Private mVistaDatos As DataView


    'Variables para Generar el LOGO, SOLO PARA RAFAM
    Private mTipoConexion As String
    Private mOrganismo As String
    Private mNombreOrganismo As String


    Private mCodigoMunicipalidad As String
    Private dsDatosMunicipalidad As DataSet

#End Region

#Region "Propertys"

    Public Property dataVouchers_Expired() As DataSet
        Get
            Return Me.dsVouchers_Expired
        End Get
        Set(ByVal value As DataSet)
            Me.dsVouchers_Expired = value
        End Set
    End Property

    Public Property dataVouchers_NO_Expired() As DataSet
        Get
            Return Me.dsVouchers_NO_Expired
        End Get
        Set(ByVal value As DataSet)
            Me.dsVouchers_NO_Expired = value
        End Set
    End Property


    Public WriteOnly Property NombreMunicipalidad() As String
        Set(ByVal value As String)
            mNombreMunicipalidad = value.Trim
        End Set
    End Property

    Public WriteOnly Property RutaFisica() As String
        Set(ByVal value As String)
            mRutaFisica = value.Trim
        End Set
    End Property

    Public WriteOnly Property TipoConexion() As String
        Set(ByVal value As String)
            mTipoConexion = value
        End Set
    End Property

    Public WriteOnly Property Organismo() As String
        Set(ByVal value As String)
            mOrganismo = value
        End Set
    End Property


    Public WriteOnly Property NombreOrganismo() As String
        Set(ByVal value As String)
            mNombreOrganismo = value
        End Set
    End Property


    Public WriteOnly Property CodigoMunicipalidad() As String
        Set(ByVal value As String)
            mCodigoMunicipalidad = value
        End Set
    End Property


    Public WriteOnly Property DatosMunicipalidad() As DataSet
        Set(ByVal value As DataSet)
            Me.dsDatosMunicipalidad = value
        End Set
    End Property

#End Region

#Region "Constructor"

    Public Sub New()
        Me.dsDatos = Nothing
        Me.dsVouchers_Expired = Nothing
        Me.dsVouchers_NO_Expired = Nothing
        Me.mNombreMunicipalidad = ""
        Me.mRutaFisica = ""
        Me.mVistaDatos = Nothing


        'Variables para Generar el LOGO, SOLO PARA RAFAM
        Me.mTipoConexion = ""
        Me.mOrganismo = ""
        Me.mNombreOrganismo = ""

    End Sub


    Protected Overrides Sub Finalize()

        Me.dsDatos = Nothing
        Me.dsVouchers_Expired = Nothing
        Me.dsVouchers_NO_Expired = Nothing
        Me.mNombreMunicipalidad = ""
        Me.mRutaFisica = ""
        Me.mVistaDatos = Nothing


        'Variables para Generar el LOGO, SOLO PARA RAFAM
        Me.mTipoConexion = ""
        Me.mOrganismo = ""
        Me.mNombreOrganismo = ""


        MyBase.Finalize()
    End Sub

#End Region




    Public Sub CrearPDF()
        Dim mDocumentoPDF As Document = New Document(iTextSharp.text.PageSize.A4, 15, 15, 50, 50)
        Dim writer As PdfWriter = PdfWriter.GetInstance(mDocumentoPDF, New System.IO.FileStream(mRutaFisica.ToString, System.IO.FileMode.Create))


        writer.ViewerPreferences = PdfWriter.PageLayoutSinglePage
        mDocumentoPDF.Open()

        If (Me.dsVouchers_Expired IsNot Nothing) Then
            Me.dsDatos = Nothing
            Me.dsDatos = New DataSet
            Me.dsDatos = Me.dsVouchers_Expired
            For i = 0 To dsDatos.Tables("renglones").Rows.Count - 1
                Call Me.CrearEncabezadoLogo(mDocumentoPDF, writer)
                Call Me.CrearEncabezado(mDocumentoPDF, writer)
                Call Me.TablaMovimientos(mDocumentoPDF, writer, i)
                Call Me.Talon(mDocumentoPDF, writer, i)
                Call Me.CrearBarCode(mDocumentoPDF, writer, i)

                mDocumentoPDF.NewPage()
            Next
        End If



        If (Me.dsVouchers_NO_Expired IsNot Nothing) Then
            Me.dsDatos = Nothing
            Me.dsDatos = New DataSet
            Me.dsDatos = Me.dsVouchers_NO_Expired
            For i = 0 To dsDatos.Tables("renglones").Rows.Count - 1
                Call Me.CrearEncabezadoLogo(mDocumentoPDF, writer)
                Call Me.CrearEncabezado(mDocumentoPDF, writer)
                Call Me.TablaMovimientos(mDocumentoPDF, writer, i)
                Call Me.Talon(mDocumentoPDF, writer, i)
                Call Me.CrearBarCode(mDocumentoPDF, writer, i)

                mDocumentoPDF.NewPage()
            Next
        End If




        mDocumentoPDF.Close()
    End Sub

#Region "Crear PDF"

    Private Sub CrearEncabezadoLogo(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mLogo As Image
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte

        Dim mNombreLogo As String
        Dim mLogoHeight As Single
        Dim mLogoWidth As Single
        Dim mLogoCoordenadaX As Single
        Dim mLogoCoordenadaY As Single
        Dim mLogo_AUX As Byte()



        'Obtengo el Nombre del Logo segun la CONEXION y Seteo propiedades
        If (mTipoConexion.ToString = "RAFAM") Then
            mNombreLogo = "logo_rafam.jpg"
            mLogoHeight = 60
            mLogoWidth = 60
            mLogoCoordenadaX = 50
            mLogoCoordenadaY = 745
        Else

            'Logo
            mLogo_AUX = clsTools.ObtenerLogo()
            mNombreLogo = "logo.jpg"
            mLogoHeight = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoHeight").ToString.Trim)
            mLogoWidth = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoWidth").ToString.Trim)
            mLogoCoordenadaX = CSng(System.Configuration.ConfigurationManager.AppSettings("Comprobante_LogoCoordenadaX").ToString.Trim)
            mLogoCoordenadaY = CSng(System.Configuration.ConfigurationManager.AppSettings("Comprobante_LogoCoordenadaY").ToString.Trim)
        End If



        'Agrego el Logo a PDF
        Try
            If (mTipoConexion.ToString = "RAFAM") Then
                mLogo = iTextSharp.text.Image.GetInstance(mNombreLogo.ToString)
            Else
                mLogo = iTextSharp.text.Image.GetInstance(mLogo_AUX)
            End If
            mLogo.SetAbsolutePosition(mLogoCoordenadaX, mLogoCoordenadaY)
            mLogo.ScaleAbsolute(mLogoWidth, mLogoHeight)
            mDocumentoPDF.Add(mLogo)
        Catch ex As Exception
        End Try



        'Nombre de La Municipalidad
        If (mTipoConexion.ToString = "RAFAM") Then
            cb = writer.DirectContent
            cb.BeginText()
            mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
            cb.SetFontAndSize(mFuente, 10)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mOrganismo.ToString, 105, 775, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mNombreOrganismo.ToString, 105, 765, 0)
            cb.EndText()
        End If







        'TITULO de la IMPRESION
        cb = writer.DirectContent
        cb.BeginText()


        'Datos de la municipalidad
        If (dsDatosMunicipalidad IsNot Nothing) Then


            'Pie del logo
            mFuente = FontFactory.GetFont(FontFactory.TIMES, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
            cb.SetFontAndSize(mFuente, 8)


            Me.mNombreMunicipalidad = ""
            Me.mNombreMunicipalidad = clsTools.ObtenerNombreMunicipalidad()
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Municipalidad de " & Me.mNombreMunicipalidad.ToString.Trim, 75, 755, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Pcia. de Buenos Aires", 75, 745, 0)


            mFuente = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
            cb.SetFontAndSize(mFuente, 12)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Municipalidad de " & Me.mNombreMunicipalidad.ToString.Trim, 580, 790, 0)


            mFuente = FontFactory.GetFont(FontFactory.TIMES, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
            cb.SetFontAndSize(mFuente, 8)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_DIRECCION").ToString.Trim, 580, 775, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_TELEFONO").ToString.Trim, 580, 765, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_MAIL").ToString.Trim, 580, 755, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_WEB").ToString.Trim, 580, 745, 0)
        End If





        'mFuente = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        'cb.SetFontAndSize(mFuente, 10)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Comprobante de Pago", 480, 780, 0)

        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 17)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Tasa por inspección de Seg. e Higiene", 150, 760, 0)
        cb.EndText()
    End Sub

    Private Sub CrearEncabezado(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim mFuenteNegrita As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mValorY As Integer

        cb = writer.DirectContent

        'Rectangulo Contenedora 
        cb.Rectangle(15, 690, 565, 50)




        mFuente = FontFactory.GetFont(FontFactory.TIMES_ITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        mFuenteNegrita = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 10)


        cb.BeginText()




        mValorY = 725
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Titular: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatos.Tables("imponible").Rows(0).Item("TITULAR").ToString, 70, mValorY, 0)
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Tipo de Cuenta: ", 430, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, ObtenerTipoCuenta(dsDatos.Tables("imponible").Rows(0).Item("TIPOIMPONIBLE").ToString.Trim), 500, mValorY, 0)


        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Domicilio: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatos.Tables("imponible").Rows(0).Item("DOMICILIO").ToString, 70, mValorY, 0)
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Nro. de Legajo: ", 430, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatos.Tables("imponible").Rows(0).Item("NROIMPONIBLE").ToString, 500, mValorY, 0)


        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Localidad: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatos.Tables("imponible").Rows(0).Item("LOCALIDAD").ToString, 70, mValorY, 0)

        cb.EndText()

    End Sub

    Private Sub TablaMovimientos(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter, ByVal mIdComprobante As Integer)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mColumnas As SColumnas
        Dim mRenglon As Integer


        cb = writer.DirectContent


        'Rectangulo Contenedora 
        cb.Rectangle(15, 300, 565, 380)


        'Linea de Encabezado
        cb.SetLineWidth(1)
        cb.MoveTo(15, 660)
        cb.LineTo(580, 660)


        'Linea de Encabezado 2
        cb.MoveTo(15, 645)
        cb.LineTo(580, 645)



        'Line de Vertical
        cb.SetLineWidth(1)
        cb.MoveTo(300, 300)
        cb.LineTo(300, 340)



        'Linea superior
        cb.MoveTo(300, 340)
        cb.LineTo(580, 340)


        'Linea Media
        cb.MoveTo(300, 320)
        cb.LineTo(580, 320)
        cb.Stroke()




        mColumnas.Recurso = 35
        mColumnas.Anio = 70
        mColumnas.Cuota = 105
        mColumnas.Concepto = 140
        mColumnas.ImporteOrigen = 400
        mColumnas.ImporteRecargo = 490
        mColumnas.ImporteTotal = 570


        cb.BeginText()
        Dim fuente As iTextSharp.text.pdf.BaseFont


        mRenglon = 667


        'Filtro el DataSet
        mVistaDatos = New DataView(dsDatos.Tables("comprobante"))
        mVistaDatos.RowFilter = "Comprobante_Id = " & mIdComprobante.ToString


        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Nº Comprobante:", 80, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("NROCOMP").ToString, 150, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Fecha de Emisión:", 440, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, dsDatos.Tables("comprobante").Rows(0).Item("FECHA_EMISION").ToString, 500, mRenglon, 0)


        mRenglon = mRenglon - 17
        fuente = FontFactory.GetFont(FontFactory.TIMES_BOLD, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Recurso", mColumnas.Recurso, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Año", mColumnas.Anio, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Cuota", mColumnas.Cuota, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Concepto", mColumnas.Concepto, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Importe Origen", mColumnas.ImporteOrigen, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Importe Recargo", mColumnas.ImporteRecargo, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Importe Total", mColumnas.ImporteTotal, mRenglon, 0)




        Dim i As Integer
        mRenglon = mRenglon - 20
        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(fuente, 8)





        'Filtro el DataSet
        mVistaDatos = New DataView(dsDatos.Tables("renglon"))
        mVistaDatos.RowFilter = "Renglones_Id = " & mIdComprobante.ToString



        If (mVistaDatos.Count > 0) Then
            For i = 0 To mVistaDatos.Count - 1
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(i).Row("RECURSO").ToString, mColumnas.Recurso, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(i).Row("ANIO").ToString, mColumnas.Anio, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(i).Row("CUOTA").ToString, mColumnas.Cuota, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mVistaDatos.Item(i).Row("CONCEPTO").ToString, mColumnas.Concepto, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, mVistaDatos.Item(i).Row("IMPORIGENRENG").ToString, mColumnas.ImporteOrigen, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, mVistaDatos.Item(i).Row("IMPRECARGOSRENG").ToString, mColumnas.ImporteRecargo, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, mVistaDatos.Item(i).Row("IMPTOTALRENG").ToString, mColumnas.ImporteTotal, mRenglon, 0)

                mRenglon = mRenglon - 15
            Next
        End If




        'Filtro el DataSet
        mVistaDatos = New DataView(dsDatos.Tables("comprobante"))
        mVistaDatos.RowFilter = "Comprobante_Id = " & mIdComprobante.ToString



        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "VENCIMIENTO", 400, 325, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_1").ToString, 400, 303, 0)


        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "TOTAL", 560, 325, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_1").ToString, 560, 303, 0)



        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(mFuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Contribuyente", 555, 290, 0)
        cb.EndText()


    End Sub

    Private Sub Talon(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter, ByVal mIdComprobante As Integer)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim mFuenteNegrita As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte

        Dim mRows As Integer = 25


        cb = writer.DirectContent



        'Rectangulo Contenedora 
        cb.Rectangle(15, 25, 300, 145)

        'Rectangulo Fecha Vencimiento
        cb.Rectangle(20, 130, 290, 20)


        'Rectangulos Importes
        cb.Rectangle(20, 70, 290, 53)


        'Primer linea de Cuadro de Importes
        cb.SetLineWidth(1)
        cb.MoveTo(20, 108)
        cb.LineTo(310, 108)


        'Segundo linea de Cuadro de Importes
        cb.SetLineWidth(1)
        cb.MoveTo(20, 89)
        cb.LineTo(310, 89)
        cb.Stroke()








        cb.BeginText()

        'Nombre de la Municipalidad 
        mFuente = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont

        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Municipalidad de " & mNombreMunicipalidad.ToString, 15, (160 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Comprobante de Pago", 15, (148 + mRows), 0)



        '------ Datos Rectangulo Talon --------
        'Asigno fuente
        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        mFuenteNegrita = FontFactory.GetFont(FontFactory.TIMES_BOLD, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(mFuente, 8)



        'Filtro el DataSet
        mVistaDatos = New DataView(dsDatos.Tables("comprobante"))
        mVistaDatos.RowFilter = "Comprobante_Id = " & mIdComprobante.ToString



        'Imprimo Datos
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Nº Comprobante:", 120, (132 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("NROCOMP").ToString, 185, (132 + mRows), 0)


        cb.SetFontAndSize(mFuenteNegrita, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "1º Vencimiento:", 55, (113 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_1").ToString, 108, (113 + mRows), 0)

        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "2º Vencimiento:", 235, (113 + mRows), 0)
        If (mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString.Trim = "") Then
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "--------", 283, (113 + mRows), 0)
        Else
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString, 283, (113 + mRows), 0)
        End If



        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Vencimiento", 50, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Origen", 120, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Recargos", 210, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Total", 290, (88 + mRows), 0)

        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "1º", 50, (73 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_ORIGEN1").ToString, 120, (73 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_RECARGO_1").ToString, 210, (73 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_1").ToString, 290, (73 + mRows), 0)


        ' Segundo vencimiento
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "2º", 50, (53 + mRows), 0)
        If (mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString.Trim = "") Then
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "------", 120, (53 + mRows), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "------", 210, (53 + mRows), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "------", 290, (53 + mRows), 0)
        Else
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_ORIGEN1").ToString, 120, (53 + mRows), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_RECARGO_1").ToString, 210, (53 + mRows), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_1").ToString, 290, (53 + mRows), 0)
        End If



        cb.SetFontAndSize(mFuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Banco", 305, 15, 0)
        cb.EndText()








        '----------------- CUADRO MUNICIPALIDAD -----------
        'Rectangulo Contenedora 
        cb.Rectangle(320, 25, 260, 145)



        'Rectangulo Fecha Vencimiento
        cb.Rectangle(325, 130, 250, 20)



        'Rectangulos Importes
        cb.Rectangle(325, 70, 250, 53)



        'Primera Line de Cuadro de Importes
        cb.SetLineWidth(1)
        cb.MoveTo(325, 108)
        cb.LineTo(575, 108)


        'Segundo linea de Cuadro de Importes
        cb.SetLineWidth(1)
        cb.MoveTo(325, 89)
        cb.LineTo(575, 89)
        cb.Stroke()






        cb.BeginText()

        'Nombre de la Municipalidad 
        mFuente = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Municipalidad de " & mNombreMunicipalidad.ToString, 320, (160 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Comprobante de Pago", 320, (148 + mRows), 0)



        '------ Datos Rectangulo Talon --------
        'Asigno fuente
        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(mFuente, 8)


        'Imprimo Datos
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Nº Comprobante:", 410, (132 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("NROCOMP").ToString, 475, (132 + mRows), 0)
        cb.SetFontAndSize(mFuenteNegrita, 8)

        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "1º Vencimiento:", 360, (113 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_1").ToString, 410, (113 + mRows), 0)


        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "2º Vencimiento:", 500, (113 + mRows), 0)
        If (mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString.Trim = "") Then
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "--------", 550, (113 + mRows), 0)
        Else
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString, 550, (113 + mRows), 0)
        End If




        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Vencimiento", 355, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Origen", 420, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Recargos", 480, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Total", 550, (88 + mRows), 0)


        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "1º", 355, (73 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_ORIGEN1").ToString, 420, (73 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_RECARGO_1").ToString, 480, (73 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_1").ToString, 550, (73 + mRows), 0)


        ' Segundo Vencimiento
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "2º", 355, (53 + mRows), 0)
        If (mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString.Trim = "") Then
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "------", 420, (53 + mRows), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "------", 480, (53 + mRows), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "------", 550, (53 + mRows), 0)
        Else
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_ORIGEN1").ToString, 420, (53 + mRows), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_RECARGO_1").ToString, 480, (53 + mRows), 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_1").ToString, 550, (53 + mRows), 0)
        End If



        cb.SetFontAndSize(mFuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Municipalidad", 555, 15, 0)
        cb.EndText()

    End Sub

    Private Sub CrearBarCode(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter, ByVal mIdComprobante As Integer)
        Dim mCelda As New iTextSharp.text.Cell
        Dim mTabla As New Table(2)
        Dim mCode39 As String
        Dim mCode39CoordenadaX As Single


        Dim cb As PdfContentByte
        cb = writer.DirectContent


        'Filtro el DataSet
        mVistaDatos = New DataView(dsDatos.Tables("comprobante"))
        mVistaDatos.RowFilter = "Comprobante_Id = " & mIdComprobante.ToString


        'Interlib 2of5
        Dim code25 As BarcodeInter25 = New BarcodeInter25
        code25.StartStopText = False
        code25.GenerateChecksum = False
        code25.Code = mVistaDatos.Item(0).Row("stringcodbarra").ToString
        Dim image25 As Image
        image25 = code25.CreateImageWithBarcode(cb, Nothing, Nothing)
        image25.SetAbsolutePosition(22, 30)
        mDocumentoPDF.Add(image25)


        'Code 39
        Dim code39 As New Barcode39
        Dim image39 As Image


        'Genero el Codigo de Barras para la Municipalidad segun la CONEXION
        If (mTipoConexion.ToString = "RAFAM") Then
            mCode39 = mCodigoMunicipalidad.ToString.Trim & _
                      mVistaDatos.Item(0).Row("NROCOMP").ToString.Substring(0, 3) & _
                      mVistaDatos.Item(0).Row("NROCOMP").ToString.Substring(4, 12) & _
                      mVistaDatos.Item(0).Row("NROCOMP").ToString.Substring(17, 1)

            'Codigo Verificador del BARCODE39
            mCode39 = mCode39 & "3"
            mCode39CoordenadaX = 335
        Else
            mCode39 = mVistaDatos.Item(0).Row("stringcodbarra").ToString.Substring((mVistaDatos.Item(0).Row("stringcodbarra").ToString.Length - 14))
            mCode39CoordenadaX = 370
        End If




        code39.Code = mCode39.ToString
        code39.StartStopText = False
        cb = writer.DirectContent
        image39 = code39.CreateImageWithBarcode(cb, Nothing, Nothing)
        image39.SetAbsolutePosition(mCode39CoordenadaX, 30)
        mDocumentoPDF.Add(image39)
    End Sub

#End Region

    Private Function ObtenerTipoCuenta(ByVal mTipoCuenta As String) As String
        Select Case mTipoCuenta
            Case "I" : Return "INMUEBLE"
            Case "C" : Return "COMERCIO"
            Case "E", "O" : Return "CEMENTERIO"
            Case "V", "R" : Return "VEHICULOS"
            Case Else : Return ""
        End Select
    End Function

End Class
