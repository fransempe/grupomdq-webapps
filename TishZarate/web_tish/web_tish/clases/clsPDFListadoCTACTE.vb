﻿Option Explicit On
Option Strict On


Imports iTextSharp.text
Imports iTextSharp.text.pdf



Public Class clsPDFListadoCTACTE

#Region "Variables y Estructuras"

    Private Structure SColumns
        Dim Column_Year As Integer
        Dim Column_Period As Integer
        Dim Column_Concept As Integer
        Dim Column_Date_Expired As Integer
        Dim Column_Special_Conditions As Integer
        Dim Column_Origin_Amount As Integer
        Dim Column_Surcharge_Amount As Integer
        Dim Column_Total_Amount As Integer
        Dim Column_State As Integer
    End Structure

    Private dsDataComercio As DataSet
    Private dsDataRows As DataSet

    Private mNumeroRegistro As Integer
    Private mRutaFisica As String
    Private mPagina As Integer
    Private mTotalPagina As Double
    Private mCantidadRegistros As Integer
    Private mTipoConexion As String
    Private mOrganismo As String
    Private mNombreOrganismo As String

    Private dsDatosMunicipalidad As DataSet

#End Region

#Region "Propertys"

    Public WriteOnly Property DataComercio() As DataSet
        Set(ByVal value As DataSet)
            Me.dsDataComercio = value
        End Set
    End Property

    Public WriteOnly Property DataRows() As DataSet
        Set(ByVal value As DataSet)
            Me.dsDataRows = value
        End Set
    End Property

    Public WriteOnly Property RutaFisica() As String
        Set(ByVal value As String)
            mRutaFisica = value.Trim
        End Set
    End Property

    Public WriteOnly Property TipoConexion() As String
        Set(ByVal value As String)
            mTipoConexion = value
        End Set
    End Property

    Public WriteOnly Property Organismo() As String
        Set(ByVal value As String)
            mOrganismo = value
        End Set
    End Property


    Public WriteOnly Property NombreOrganismo() As String
        Set(ByVal value As String)
            mNombreOrganismo = value
        End Set
    End Property


    Public WriteOnly Property DatosMunicipalidad() As DataSet
        Set(ByVal value As DataSet)
            Me.dsDatosMunicipalidad = value
        End Set
    End Property

#End Region

#Region "Contructor"

    ' Contructor de la CLASE
    Public Sub New()
        Me.dsDataComercio = Nothing
        Me.dsDataRows = Nothing
        Me.mRutaFisica = ""

        Me.mNumeroRegistro = -1
        Me.mPagina = -1
        Me.mTotalPagina = -1
        Me.mCantidadRegistros = -1
        Me.mTipoConexion = ""
        Me.mOrganismo = ""
        Me.mNombreOrganismo = ""
    End Sub

#End Region

#Region "Procedimientos"

    'Procedimiento donde se CREA el PDF y se SETEA las propiedades del DOCUMENTO y CONTROLA cuando se CREA una NUEVA PAGINA
    Public Sub CrearPDF()
        Dim mDocumentoPDF As Document = New Document(iTextSharp.text.PageSize.A4, 15, 15, 50, 50)
        Dim writer As PdfWriter = PdfWriter.GetInstance(mDocumentoPDF, New System.IO.FileStream(mRutaFisica.ToString, System.IO.FileMode.Create))

        mDocumentoPDF.SetPageSize(PageSize.A4)
        writer.ViewerPreferences = PdfWriter.PageLayoutOneColumn
        mDocumentoPDF.Open()


        'Seteo Variables
        mNumeroRegistro = 0
        mPagina = 0
        mTotalPagina = 0
        mCantidadRegistros = 39



        'Obtengo la Cantidad de Hojas que voy a generar
        mTotalPagina = ((dsDataRows.Tables(0).Rows.Count - 1) / mCantidadRegistros)

        If (CInt(mTotalPagina) = 0) Then
            mTotalPagina = 1
        End If

        If CInt(mTotalPagina) <> mTotalPagina Then
            If (mTotalPagina) > CInt(mTotalPagina) Then
                mTotalPagina = CInt(mTotalPagina) + 1
            Else
                mTotalPagina = CInt(mTotalPagina)
            End If
        End If


        'Genero Hojas
        Do While (mNumeroRegistro <= dsDataRows.Tables(0).Rows.Count - 1)
            mPagina = mPagina + 1
            Call CrearPaginas(mDocumentoPDF, writer)
        Loop


        mDocumentoPDF.Close()
    End Sub

    'Este Procedimiento LLAMA a las procedimientos que CREAN las distintas partes del DOCUMENTO
    Private Sub CrearPaginas(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Call CrearEncabezadoLogo(mDocumentoPDF, writer)
        Call CrearEncabezado(mDocumentoPDF, writer)
        Call TablaMovimientos(mDocumentoPDF, writer)
    End Sub

    'Este Procedimiento DIBUJA el ENCABEZADO del DOCUMENTO INCLUYENDO el LOGO
    Private Sub CrearEncabezadoLogo(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mLogo As Image
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mNombreLogo As String
        Dim mLogoHeight As Single
        Dim mLogoWidth As Single
        Dim mLogoCoordenadaX As Single
        Dim mLogoCoordenadaY As Single
        Dim mLogo_AUX As Byte()
        Dim mNombreMunicipalidad_AUX As String
        Dim mValor_Y As Single




        'Obtengo el Nombre del Logo segun la CONEXION y propiedades para setearlo
        If (mTipoConexion.ToString = "RAFAM") Then
            mNombreLogo = "logo_rafam.jpg"
            mLogoHeight = 60
            mLogoWidth = 60
            mLogoCoordenadaX = 50
            mLogoCoordenadaY = 525
        Else

            'LOGO
            mLogo_AUX = clsTools.ObtenerLogo()
            mLogoHeight = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoHeight").ToString.Trim)
            mLogoWidth = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoWidth").ToString.Trim)
            mLogoCoordenadaX = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_LogoCoordenadaX").ToString.Trim)
            mLogoCoordenadaY = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_LogoCoordenadaY").ToString.Trim)
        End If


        'Agrego el Logo a PDF
        Try

            If (mTipoConexion.ToString = "RAFAM") Then
                mLogo = iTextSharp.text.Image.GetInstance(mNombreLogo.ToString)
            Else
                mLogo = iTextSharp.text.Image.GetInstance(mLogo_AUX)
            End If

            mLogo.SetAbsolutePosition(mLogoCoordenadaX, mLogoCoordenadaY)
            mLogo.ScaleAbsolute(mLogoWidth, mLogoHeight)
            mDocumentoPDF.Add(mLogo)
        Catch ex As Exception
        End Try


        'Nombre de La Municipalidad
        cb = writer.DirectContent
        cb.BeginText()
        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mOrganismo.ToString, 105, 555, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mNombreOrganismo.ToString, 105, 545, 0)
        cb.EndText()








        'TITULO y HORA de la IMPRESION
        cb = writer.DirectContent
        cb.BeginText()

        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 20)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Cuenta Corriente", 205, 750, 0)

        mFuente = FontFactory.GetFont(FontFactory.HELVETICA, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(mFuente, 8)
        


        'Datos de la municipalidad
        If (dsDatosMunicipalidad IsNot Nothing) Then


            'Pie del logo
            mFuente = FontFactory.GetFont(FontFactory.TIMES, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
            cb.SetFontAndSize(mFuente, 8)


            mNombreMunicipalidad_AUX = ""
            mNombreMunicipalidad_AUX = clsTools.ObtenerNombreMunicipalidad()



            mValor_Y = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_Cabecera_Pie_Logo_Municipalidad_de_Y").ToString.Trim)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Municipalidad de " & mNombreMunicipalidad_AUX.ToString.Trim, 75, mValor_Y, 0)


            mValor_Y = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_Cabecera_Pie_Logo_Provincia_de_Buenos_Aires_Y").ToString.Trim)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Pcia. de Buenos Aires", 75, mValor_Y, 0)



            mFuente = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
            cb.SetFontAndSize(mFuente, 12)
            mValor_Y = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_Cabecera_Nombre_Municipalidad_Negrita_Y").ToString.Trim)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Municipalidad de " & mNombreMunicipalidad_AUX.ToString.Trim, 580, mValor_Y, 0)


            mFuente = FontFactory.GetFont(FontFactory.TIMES, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
            cb.SetFontAndSize(mFuente, 8)

            mValor_Y = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_Cabecera_Municipalidad_Direccion_Y").ToString.Trim)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_DIRECCION").ToString.Trim, 580, mValor_Y, 0)

            mValor_Y = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_Cabecera_Municipalidad_Telefono_Y").ToString.Trim)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_TELEFONO").ToString.Trim, 580, mValor_Y, 0)

            mValor_Y = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_Cabecera_Municipalidad_Mail_Y").ToString.Trim)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_MAIL").ToString.Trim, 580, mValor_Y, 0)


            mValor_Y = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_Cabecera_Municipalidad_Web_Y").ToString.Trim)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_WEB").ToString.Trim, 580, mValor_Y, 0)

        End If



        cb.EndText()
    End Sub

    'Este Porcedimiento DIBUJA los DATOS del ENCABEZADO del DOCUMENTO
    Private Sub CrearEncabezado(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim mFuenteNegrita As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mValorY As Integer

        cb = writer.DirectContent

        'Rectangulo Contenedora 
        cb.SetLineWidth(1)
        cb.Rectangle(15, 690, 565, 50)

        cb.Stroke()


        mFuente = FontFactory.GetFont(FontFactory.TIMES_ITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        mFuenteNegrita = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 10)




        cb.BeginText()
        mValorY = 725
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Titular: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDataComercio.Tables("COMERCIO").Rows(0).Item("CONTRIBUYENTE_NOMBRE").ToString.Trim, 70, mValorY, 0)
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Tipo de Cuenta: ", 430, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "COMERCIO", 500, mValorY, 0)


        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Domicilio: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDataComercio.Tables("COMERCIO").Rows(0).Item("COMERCIO_UBICACION").ToString.Trim, 70, mValorY, 0)


        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Nro. de Legajo: ", 430, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDataComercio.Tables("COMERCIO").Rows(0).Item("COMERCIO_NUMERO").ToString.Trim, 500, mValorY, 0)


        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Localidad: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDataComercio.Tables("COMERCIO").Rows(0).Item("COMERCIO_LOCALIDAD").ToString.Trim, 70, mValorY, 0)


        cb.EndText()

    End Sub

    'Este Procedimiento DIBUJA los REGISTROS SELECCIONADOS, como tambien DIBUJA los TOTALES del PIE
    Private Sub TablaMovimientos(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim cb As PdfContentByte
        Dim mColumns As SColumns
        Dim mRenglon As Integer
        

        cb = writer.DirectContent


        'Rectangulo Contenedora 
        'cb.Rectangle(20, 50, 400, 400)
        cb.Rectangle(15, 50, 565, 630)



        'Linea superior
        'cb.MoveTo(20, 430)
        'cb.LineTo(820, 430)

        cb.SetLineWidth(1)
        cb.MoveTo(15, 660)
        cb.LineTo(580, 660)

        cb.Stroke()




        mColumns.Column_Year = 30
        mColumns.Column_Period = 70
        mColumns.Column_Concept = 140
        mColumns.Column_Date_Expired = 220
        mColumns.Column_Special_Conditions = 310
        mColumns.Column_Origin_Amount = 400
        mColumns.Column_Surcharge_Amount = 450
        mColumns.Column_Total_Amount = 500
        mColumns.Column_State = 550


        cb.BeginText()
        Dim fuente As iTextSharp.text.pdf.BaseFont


        mRenglon = 667
        fuente = FontFactory.GetFont(FontFactory.TIMES_BOLD, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Año", mColumns.Column_Year, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Período", mColumns.Column_Period, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Concepto", mColumns.Column_Concept, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Fecha Vto.", mColumns.Column_Date_Expired, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Con. Especiales", mColumns.Column_Special_Conditions, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Origen", mColumns.Column_Origin_Amount, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Recargo", mColumns.Column_Surcharge_Amount, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Total", mColumns.Column_Total_Amount, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Estado", mColumns.Column_State, mRenglon, 0)





        Dim i As Integer
        Dim mBandera As Integer
        mRenglon = mRenglon - 20
        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(fuente, 8)
        mBandera = -1


        'I seteo the order
        dsDataRows.Tables("LIST_CTACTE").DefaultView.Sort = "DDV_ANIO ASC, DDV_CUOTA ASC"
 

        For i = mNumeroRegistro To dsDataRows.Tables(0).Rows.Count - 1
            mBandera = mBandera + 1
            If (mBandera <= mCantidadRegistros) Then

                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, Me.dsDataRows.Tables("LIST_CTACTE").DefaultView(i).Item("DDV_ANIO").ToString.Trim, mColumns.Column_Year, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, Me.dsDataRows.Tables("LIST_CTACTE").DefaultView(i).Item("DDV_CUOTA").ToString.Trim, mColumns.Column_Period, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Me.dsDataRows.Tables("LIST_CTACTE").DefaultView(i).Item("DDV_CONCEPTO").ToString.Trim, (mColumns.Column_Concept - 15), mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, Me.dsDataRows.Tables("LIST_CTACTE").DefaultView(i).Item("DDV_FECHAVENC").ToString.Trim, mColumns.Column_Date_Expired, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, Me.dsDataRows.Tables("LIST_CTACTE").DefaultView(i).Item("DDV_CONDESPECIAL").ToString.Trim, mColumns.Column_Special_Conditions, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, Me.dsDataRows.Tables("LIST_CTACTE").DefaultView(i).Item("DDV_IMPORTEORIGEN").ToString.Trim, (mColumns.Column_Origin_Amount + 13), mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, Me.dsDataRows.Tables("LIST_CTACTE").DefaultView(i).Item("DDV_IMPORTERECARGO").ToString.Trim, (mColumns.Column_Surcharge_Amount + 13), mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, Me.dsDataRows.Tables("LIST_CTACTE").DefaultView(i).Item("DDV_IMPORTETOTAL").ToString.Trim, (mColumns.Column_Total_Amount + 12), mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, Me.dsDataRows.Tables("LIST_CTACTE").DefaultView(i).Item("ESTADO").ToString.Trim, mColumns.Column_State, mRenglon, 0)

                mRenglon = mRenglon - 15
            Else

                mNumeroRegistro = i
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Página " & mPagina.ToString & " de " & mTotalPagina.ToString, 550, 25, 0)
                cb.EndText()
                mDocumentoPDF.NewPage()
                Exit Sub
            End If
        Next
        mNumeroRegistro = i





        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Cuenta corriente actualizada al: " & Format(Now, "dd/MM/yyy").ToString, 15, 30, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Fecha / Hora de Impresión: " & Format(Now, "dd/MM/yyy").ToString & "  -  " & Format(Now, "HH:mm:ss").ToString, 15, 20, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Página " & mPagina.ToString & " de " & mTotalPagina.ToString, 550, 25, 0)


        mRenglon = mRenglon - 15
        fuente = FontFactory.GetFont(FontFactory.TIMES_BOLD, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 9)
        

        cb.EndText()

    End Sub

#End Region

End Class

