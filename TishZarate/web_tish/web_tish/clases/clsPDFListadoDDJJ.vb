﻿Imports iTextSharp.text
Imports iTextSharp.text.pdf



Public Class clsPDFListadoDDJJ

#Region "Variables y Estructuras"

    Private Structure strucColumnas
        Dim anio As Integer
        Dim cuota As Integer
        Dim rubro As Integer
        Dim empleados As Integer
        Dim importeDeclarado As Integer
    End Structure

    Private dsDataComercio As DataSet
    Private com_ddjj As List(Of entidades.com_ddjj)

    Private mNumeroRegistro As Integer
    Private mRutaFisica As String
    Private mPagina As Integer
    Private mTotalPagina As Double
    Private mCantidadRegistros As Integer
    Private mTipoConexion As String
    Private mOrganismo As String
    Private mNombreOrganismo As String

    Private dsDatosMunicipalidad As DataSet

    Private mEmployeesUsing As Boolean

#End Region

#Region "Propertys"

    Public WriteOnly Property DataComercio() As DataSet
        Set(ByVal value As DataSet)
            Me.dsDataComercio = value
        End Set
    End Property

    Public WriteOnly Property listado_com_ddjj() As List(Of entidades.com_ddjj)
        Set(ByVal value As List(Of entidades.com_ddjj))
            Me.com_ddjj = value
        End Set
    End Property

    Public WriteOnly Property RutaFisica() As String
        Set(ByVal value As String)
            mRutaFisica = value.Trim
        End Set
    End Property

    Public WriteOnly Property TipoConexion() As String
        Set(ByVal value As String)
            mTipoConexion = value
        End Set
    End Property

    Public WriteOnly Property Organismo() As String
        Set(ByVal value As String)
            mOrganismo = value
        End Set
    End Property


    Public WriteOnly Property NombreOrganismo() As String
        Set(ByVal value As String)
            mNombreOrganismo = value
        End Set
    End Property

    Public WriteOnly Property DatosMunicipalidad() As DataSet
        Set(ByVal value As DataSet)
            Me.dsDatosMunicipalidad = value
        End Set
    End Property

    Public WriteOnly Property EmployeesUsing() As Boolean
        Set(ByVal value As Boolean)
            Me.mEmployeesUsing = value
        End Set
    End Property


#End Region

#Region "Contructor"

    ' Contructor de la CLASE
    Public Sub New()
        Me.dsDataComercio = Nothing     
        Me.mRutaFisica = ""

        Me.mNumeroRegistro = -1
        Me.mPagina = -1
        Me.mTotalPagina = -1
        Me.mCantidadRegistros = -1
        Me.mTipoConexion = ""
        Me.mOrganismo = ""
        Me.mNombreOrganismo = ""
    End Sub

#End Region

#Region "Procedimientos"

    'Procedimiento donde se CREA el PDF y se SETEA las propiedades del DOCUMENTO y CONTROLA cuando se CREA una NUEVA PAGINA
    Public Sub crearPDF()
        Dim mDocumentoPDF As Document = New Document(iTextSharp.text.PageSize.A4, 15, 15, 50, 50)
        Dim writer As PdfWriter = PdfWriter.GetInstance(mDocumentoPDF, New System.IO.FileStream(mRutaFisica.ToString, System.IO.FileMode.Create))

        mDocumentoPDF.SetPageSize(PageSize.A4)
        writer.ViewerPreferences = PdfWriter.PageLayoutOneColumn
        mDocumentoPDF.Open()


        'Seteo Variables
        mNumeroRegistro = 0
        mPagina = 0
        mTotalPagina = 0
        mCantidadRegistros = 39



        'Obtengo la Cantidad de Hojas que voy a generar
        mTotalPagina = ((com_ddjj.Count - 1) / mCantidadRegistros)

        If (CInt(mTotalPagina) = 0) Then
            mTotalPagina = 1
        End If

        If CInt(mTotalPagina) <> mTotalPagina Then
            If (mTotalPagina) > CInt(mTotalPagina) Then
                mTotalPagina = CInt(mTotalPagina) + 1
            Else
                mTotalPagina = CInt(mTotalPagina)
            End If
        End If


        'Genero Hojas
        Do While (mNumeroRegistro <= Me.com_ddjj.Count - 1)
            mPagina = mPagina + 1
            Call CrearPaginas(mDocumentoPDF, writer)
        Loop


        mDocumentoPDF.Close()
    End Sub

    'Este Procedimiento LLAMA a las procedimientos que CREAN las distintas partes del DOCUMENTO
    Private Sub CrearPaginas(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Call CrearEncabezadoLogo(mDocumentoPDF, writer)
        Call CrearEncabezado(mDocumentoPDF, writer)
        Call TablaMovimientos(mDocumentoPDF, writer)
    End Sub

    'Este Procedimiento DIBUJA el ENCABEZADO del DOCUMENTO INCLUYENDO el LOGO
    Private Sub CrearEncabezadoLogo(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mLogo As Image
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mNombreLogo As String
        Dim mLogoHeight As Single
        Dim mLogoWidth As Single
        Dim mLogoCoordenadaX As Single
        Dim mLogoCoordenadaY As Single
        Dim mLogo_AUX As Byte()
        Dim mNombreMunicipalidad_AUX As String
        Dim mValor_Y As Single


        'Obtengo el Nombre del Logo segun la CONEXION y propiedades para setearlo
        If (mTipoConexion.ToString = "RAFAM") Then
            mNombreLogo = "logo_rafam.jpg"
            mLogoHeight = 60
            mLogoWidth = 40
            mLogoCoordenadaX = 50
            mLogoCoordenadaY = 525
        Else

            'LOGO
            mLogo_AUX = clsTools.ObtenerLogo()
            mLogoHeight = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoHeight").ToString.Trim)
            mLogoWidth = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoWidth").ToString.Trim)
            mLogoCoordenadaX = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_LogoCoordenadaX").ToString.Trim)
            mLogoCoordenadaY = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_LogoCoordenadaY").ToString.Trim)
        End If


        'Agrego el Logo a PDF
        Try

            If (mTipoConexion.ToString = "RAFAM") Then
                mLogo = iTextSharp.text.Image.GetInstance(mNombreLogo.ToString)
            Else
                mLogo = iTextSharp.text.Image.GetInstance(mLogo_AUX)
            End If


            mLogo.SetAbsolutePosition(mLogoCoordenadaX, mLogoCoordenadaY)
            mLogo.ScaleAbsolute(mLogoWidth, mLogoHeight)
            mDocumentoPDF.Add(mLogo)
        Catch ex As Exception
        End Try


        'Nombre de La Municipalidad
        cb = writer.DirectContent
        cb.BeginText()
        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mOrganismo.ToString, 105, 555, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mNombreOrganismo.ToString, 105, 545, 0)
        cb.EndText()




        'TITULO y HORA de la IMPRESION
        cb = writer.DirectContent
        cb.BeginText()

        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 18)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Listado de declaraciones juradas", 150, 750, 0)

        mFuente = FontFactory.GetFont(FontFactory.HELVETICA, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(mFuente, 8)



        'Datos de la municipalidad
        If (dsDatosMunicipalidad IsNot Nothing) Then


            'Pie del logo
            mFuente = FontFactory.GetFont(FontFactory.TIMES, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
            cb.SetFontAndSize(mFuente, 8)


            mNombreMunicipalidad_AUX = ""
            mNombreMunicipalidad_AUX = clsTools.ObtenerNombreMunicipalidad()



            mValor_Y = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_Cabecera_Pie_Logo_Municipalidad_de_Y").ToString.Trim)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Municipalidad de " & mNombreMunicipalidad_AUX.ToString.Trim, 75, mValor_Y, 0)


            mValor_Y = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_Cabecera_Pie_Logo_Provincia_de_Buenos_Aires_Y").ToString.Trim)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Pcia. de Buenos Aires", 75, mValor_Y, 0)



            mFuente = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
            cb.SetFontAndSize(mFuente, 12)
            mValor_Y = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_Cabecera_Nombre_Municipalidad_Negrita_Y").ToString.Trim)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Municipalidad de " & mNombreMunicipalidad_AUX.ToString.Trim, 580, mValor_Y, 0)


            mFuente = FontFactory.GetFont(FontFactory.TIMES, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
            cb.SetFontAndSize(mFuente, 8)

            mValor_Y = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_Cabecera_Municipalidad_Direccion_Y").ToString.Trim)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_DIRECCION").ToString.Trim, 580, mValor_Y, 0)

            mValor_Y = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_Cabecera_Municipalidad_Telefono_Y").ToString.Trim)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_TELEFONO").ToString.Trim, 580, mValor_Y, 0)

            mValor_Y = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_Cabecera_Municipalidad_Mail_Y").ToString.Trim)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_MAIL").ToString.Trim, 580, mValor_Y, 0)


            mValor_Y = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_Cabecera_Municipalidad_Web_Y").ToString.Trim)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_WEB").ToString.Trim, 580, mValor_Y, 0)

        End If
        cb.EndText()
    End Sub

    'Este Porcedimiento DIBUJA los DATOS del ENCABEZADO del DOCUMENTO
    Private Sub CrearEncabezado(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim mFuenteNegrita As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mValorY As Integer

        cb = writer.DirectContent

        'Rectangulo Contenedora 
        cb.SetLineWidth(1)
        'cb.Rectangle(20, 460,  800, 60)
        cb.Rectangle(15, 690, 565, 50)

        cb.Stroke()


        mFuente = FontFactory.GetFont(FontFactory.TIMES_ITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        mFuenteNegrita = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 10)




        cb.BeginText()
        mValorY = 725
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Titular: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDataComercio.Tables("COMERCIO").Rows(0).Item("CONTRIBUYENTE_NOMBRE").ToString.Trim, 70, mValorY, 0)
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Tipo de Cuenta: ", 430, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "COMERCIO", 500, mValorY, 0)


        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Domicilio: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDataComercio.Tables("COMERCIO").Rows(0).Item("COMERCIO_UBICACION").ToString.Trim, 70, mValorY, 0)
        

        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Nro. de Legajo: ", 430, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDataComercio.Tables("COMERCIO").Rows(0).Item("COMERCIO_NUMERO").ToString.Trim, 500, mValorY, 0)


        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Localidad: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDataComercio.Tables("COMERCIO").Rows(0).Item("COMERCIO_LOCALIDAD").ToString.Trim, 70, mValorY, 0)
        

        cb.EndText()

    End Sub

    'Este Procedimiento DIBUJA los REGISTROS SELECCIONADOS, como tambien DIBUJA los TOTALES del PIE
    Private Sub TablaMovimientos(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim cb As PdfContentByte
        Dim columnas As strucColumnas
        Dim mRenglon As Integer


        cb = writer.DirectContent

        'Rectangulo Contenedora 
        cb.Rectangle(15, 50, 565, 630)


        cb.SetLineWidth(1)
        cb.MoveTo(15, 660)
        cb.LineTo(580, 660)

        cb.Stroke()




        columnas.anio = 30
        columnas.cuota = 60
        columnas.rubro = 80
        columnas.empleados = 480
        columnas.importeDeclarado = 570


        cb.BeginText()
        Dim fuente As iTextSharp.text.pdf.BaseFont


        mRenglon = 667
        fuente = FontFactory.GetFont(FontFactory.TIMES_BOLD, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Año", columnas.anio, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Período", columnas.cuota, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Rubro", columnas.rubro, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Empleados", columnas.empleados, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Monto Declarado", columnas.importeDeclarado, mRenglon, 0)



        Dim i As Integer
        Dim mBandera As Integer
        mRenglon = mRenglon - 20
        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(fuente, 8)
        mBandera = -1




        For i = mNumeroRegistro To Me.com_ddjj.Count - 1
            mBandera = mBandera + 1
            If (mBandera <= mCantidadRegistros) Then

                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, Me.com_ddjj(i).anio, columnas.anio, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, Me.com_ddjj(i).cuota, columnas.cuota, mRenglon, 0)
                If (Me.com_ddjj(i).rubro IsNot Nothing) Then
                    If (Me.com_ddjj(i).rubro.descripcion.Length > 70) Then                        
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "(" & Me.com_ddjj(i).rubro.codigo & ") " & Me.com_ddjj(i).rubro.descripcion.Substring(1, 70) & " ..", columnas.rubro, mRenglon, 0)
                    Else
                        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "(" & Me.com_ddjj(i).rubro.codigo & ") " & Me.com_ddjj(i).rubro.descripcion, columnas.rubro, mRenglon, 0)
                    End If
                Else
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "(0)", columnas.rubro, mRenglon, 0)
                End If
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, Me.com_ddjj(i).empleados, columnas.empleados, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, Format(Me.com_ddjj(i).montoDeclarado, "N2"), columnas.importeDeclarado, mRenglon, 0)


                mRenglon = mRenglon - 15
            Else

                mNumeroRegistro = i
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Página " & mPagina.ToString & " de " & mTotalPagina.ToString, 550, 25, 0)
                cb.EndText()
                mDocumentoPDF.NewPage()
                Exit Sub
            End If
        Next
        mNumeroRegistro = i
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Página " & mPagina.ToString & " de " & mTotalPagina.ToString, 550, 25, 0)



        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Listado de declaraciones juradas actualizado al: " & Format(Now, "dd/MM/yyy").ToString, 15, 30, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Fecha / Hora de Impresión: " & Format(Now, "dd/MM/yyy").ToString & "  -  " & Format(Now, "HH:mm:ss").ToString, 15, 20, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Página " & mPagina.ToString & " de " & mTotalPagina.ToString, 550, 25, 0)



        mRenglon = mRenglon - 15
        fuente = FontFactory.GetFont(FontFactory.TIMES_BOLD, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 9)

        cb.EndText()

    End Sub

#End Region

End Class
