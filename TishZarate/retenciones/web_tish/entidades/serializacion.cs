﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;


namespace entidades
{
    public class serializacion
    {
        public static void serializeXML<T>(T obj, string p_stArchivo)
        {

            //----------------------------------------------------------------------
            // Primero creamos el serializador que va a generar el XML a partir de
            // la clase que le hemos pasado. Después creamos el stream de salida
            // de nuestro programa, que escribirá el fichero.
            //----------------------------------------------------------------------
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            XmlTextWriter stream = new XmlTextWriter(p_stArchivo.Trim(), Encoding.UTF8);

            //----------------------------------------------------------------------
            // A fin de permitir una mejor claridad si lo abrimos desde el bloc de
            // notas, le indicaremos al stream, que emplee un estilo de formato
            // identado con tabulaciones.
            //----------------------------------------------------------------------
            stream.Formatting = Formatting.Indented;
            stream.Indentation = 3;
            stream.IndentChar = ' ';


            //----------------------------------------------------------------------
            // Finalmente mandamos que cree el XML y se cierre el stream.
            //----------------------------------------------------------------------
            serializer.Serialize(stream, obj);
            stream.Close();




        }



        public static T deserializarXML<T>(string p_strXML)
        {
            T objeto;

            var serializer = new XmlSerializer(typeof(T));
            var stream = new StringReader(p_strXML.Trim());

            objeto = (T)serializer.Deserialize(stream);

            return objeto;
        }
    }
}
