﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace entidades  {
    [XmlRootAttribute("comercio", Namespace = "", IsNullable = false)]
    public class comercio {
        private string strNumero;
        private string strRazonSocial;
        private string strContribuyente;


        public string numero {
            get { return this.strNumero.Trim(); }
            set { this.strNumero = value.Trim(); }
        }
        
        public string razonSocial {
            get { return this.strRazonSocial.Trim(); }
            set { this.strRazonSocial = value.Trim(); }
        }
        
        public string contribuyente {
            get { return this.strContribuyente.Trim(); }
            set { this.strContribuyente = value.Trim(); }
        }
    }
}
