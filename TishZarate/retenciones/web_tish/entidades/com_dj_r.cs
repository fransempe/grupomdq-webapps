﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace entidades {
    [XmlRootAttribute("com_dj_r", Namespace = "", IsNullable = false)]
    public class com_dj_r {           
        private string strNroComercio;
        private string strAnio;
        private string strCuota;
        private string strCuit;
        private string strDescripcion;
        private string strFecha;
        private double dblimporteRetenido;
        private double dblimporteFacturado;
        private string strnroComprob;
        
        public string nroComercio {
            get { return this.strNroComercio.Trim(); }
            set { this.strNroComercio = value.Trim(); }
        }
        
        public string anio {
            get { return this.strAnio.Trim(); }
            set { this.strAnio = value.Trim(); }
        }
        
        public string cuota {
            get { return this.strCuota.Trim(); }
            set { this.strCuota = value.Trim(); }
        }

        public string cuit {
            get { return this.strCuit.Trim(); }
            set { this.strCuit = value.Trim(); }
        }

        public string descripcion
        {
            get { return this.strDescripcion.Trim(); }
            set { this.strDescripcion = value.Trim(); }
        }
        public string fecha_ret
        {
            get { return this.strFecha.Trim(); }
            set { this.strFecha = value.Trim(); }
        }

        public double importe_retenido {
            get { return this.dblimporteRetenido; }
            set { this.dblimporteRetenido = value; }
        }
        public double importe_facturado
        {
            get { return this.dblimporteFacturado; }
            set { this.dblimporteFacturado = value; }
        }
        public string nroComprob
        {
            get { return this.strnroComprob.Trim(); }
            set { this.strnroComprob = value.Trim(); }
        }
    }
}
