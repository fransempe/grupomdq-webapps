﻿Imports iTextSharp.text
Imports iTextSharp.text.pdf


Public Class clsPDFComprobanteRetencion

#Region "Variables"

    Private mNombreMunicipalidad As String
    Private mRutaFisica As String


    'Variables para Generar el LOGO, SOLO PARA RAFAM
    Private mTipoConexion As String
    Private mOrganismo As String
    Private mNombreOrganismo As String


    Private mCodigoMunicipalidad As String


    Public mNombre_Archivo As String = ""


    Private dsDatosMunicipalidad As DataSet
    Private dsDatosConstancia As DataSet
    Private dsPeriodo_DDJJ As String
#End Region

#Region "Propertys"


    Public Property DatosConstancia() As DataSet
        Get
            Return Me.dsDatosConstancia
        End Get
        Set(ByVal value As DataSet)
            Me.dsDatosConstancia = value
        End Set
    End Property

    Public WriteOnly Property NombreMunicipalidad() As String
        Set(ByVal value As String)
            mNombreMunicipalidad = value.Trim
        End Set
    End Property

    Public WriteOnly Property RutaFisica() As String
        Set(ByVal value As String)
            mRutaFisica = value.Trim
        End Set
    End Property

    Public WriteOnly Property TipoConexion() As String
        Set(ByVal value As String)
            mTipoConexion = value
        End Set
    End Property

    Public WriteOnly Property Organismo() As String
        Set(ByVal value As String)
            mOrganismo = value
        End Set
    End Property


    Public WriteOnly Property NombreOrganismo() As String
        Set(ByVal value As String)
            mNombreOrganismo = value
        End Set
    End Property


    Public WriteOnly Property CodigoMunicipalidad() As String
        Set(ByVal value As String)
            mCodigoMunicipalidad = value
        End Set
    End Property


    Public WriteOnly Property DatosMunicipalidad() As DataSet
        Set(ByVal value As DataSet)
            Me.dsDatosMunicipalidad = value
        End Set
    End Property

    Public Property Periodo_DDJJ() As String
        Get
            Return Me.dsPeriodo_DDJJ
        End Get
        Set(ByVal value As String)
            dsPeriodo_DDJJ = value.Trim
        End Set
    End Property


#End Region


#Region "Constructor"

    Public Sub New()

        Me.dsDatosConstancia = Nothing
        Me.mNombreMunicipalidad = ""
        Me.mRutaFisica = ""


        'Variables para Generar el LOGO, SOLO PARA RAFAM
        Me.mTipoConexion = ""
        Me.mOrganismo = ""
        Me.mNombreOrganismo = ""


        Me.mCodigoMunicipalidad = ""
        Me.dsPeriodo_DDJJ = ""
    End Sub



    Protected Overrides Sub Finalize()

        Me.dsDatosConstancia = Nothing
        Me.mNombreMunicipalidad = ""
        Me.mRutaFisica = ""


        'Variables para Generar el LOGO, SOLO PARA RAFAM
        Me.mTipoConexion = ""
        Me.mOrganismo = ""
        Me.mNombreOrganismo = ""


        Me.mCodigoMunicipalidad = ""

        Me.dsPeriodo_DDJJ = ""

        MyBase.Finalize()
    End Sub

#End Region




    Public Sub CrearPDF()
        Dim mDocumentoPDF As Document = New Document(iTextSharp.text.PageSize.A4, 15, 15, 50, 50)
        Dim writer As PdfWriter = PdfWriter.GetInstance(mDocumentoPDF, New System.IO.FileStream(mRutaFisica.ToString, System.IO.FileMode.Create))




        writer.ViewerPreferences = PdfWriter.PageLayoutSinglePage
        mDocumentoPDF.Open()



        Call Me.Constancia_CrearEncabezadoLogo(mDocumentoPDF, writer)
        Call Me.Constancia_CrearEncabezado(mDocumentoPDF, writer)
        Call Me.Constancia_TablaMovimientos(mDocumentoPDF, writer, 0)


        mDocumentoPDF.Close()
    End Sub

#Region "Crear PDF"


#Region "Constancia de Presentacion de DDJJ"

    Private Sub Constancia_CrearEncabezadoLogo(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mLogo As Image
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte

        Dim mNombreLogo As String
        Dim mLogoHeight As Single
        Dim mLogoWidth As Single
        Dim mLogoCoordenadaX As Single
        Dim mLogoCoordenadaY As Single
        Dim mLogo_AUX As Byte()



        'Obtengo el Nombre del Logo segun la CONEXION y Seteo propiedades
        If (mTipoConexion.ToString = "RAFAM") Then
            mNombreLogo = "logo_rafam.jpg"
            mLogoHeight = 60
            mLogoWidth = 60
            mLogoCoordenadaX = 50
            mLogoCoordenadaY = 745
        Else

            'Logo
            mLogo_AUX = clsTools.ObtenerLogo()
            mNombreLogo = "logo.jpg"
            'ZARATE
            mLogoHeight = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoHeight").ToString.Trim)
            mLogoWidth = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoWidth").ToString.Trim)
            mLogoCoordenadaX = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_LogoCoordenadaX").ToString.Trim)
            mLogoCoordenadaY = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_LogoCoordenadaY").ToString.Trim)

        End If



        'Agrego el Logo a PDF
        Try

            If (mTipoConexion.ToString = "RAFAM") Then
                mLogo = iTextSharp.text.Image.GetInstance(mNombreLogo.ToString)
            Else
                mLogo = iTextSharp.text.Image.GetInstance(mLogo_AUX)
            End If

            mLogo.SetAbsolutePosition(mLogoCoordenadaX, mLogoCoordenadaY)
            mLogo.ScaleAbsolute(mLogoWidth, mLogoHeight)
            mDocumentoPDF.Add(mLogo)
        Catch ex As Exception
        End Try



        'Nombre de La Municipalidad
        If (mTipoConexion.ToString = "RAFAM") Then
            cb = writer.DirectContent
            cb.BeginText()
            mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
            cb.SetFontAndSize(mFuente, 10)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mOrganismo.ToString, 105, 775, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mNombreOrganismo.ToString, 105, 765, 0)
            cb.EndText()
        End If



        'TITULO de la IMPRESION
        cb = writer.DirectContent
        cb.BeginText()
        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont


        cb.SetFontAndSize(mFuente, 12)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Original", 530, 715, 0)

        cb.SetFontAndSize(mFuente, 15)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "CONSTANCIA DE PRESENTACIÓN DE DDJJ DE AGENTES DE RETENCIÓN ", 40, (775 - 30), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "DE LA TASA POR INSPECCIÓN DE SEG. E HIGIENE", 150, (760 - 30), 0)
        cb.EndText()
    End Sub

    Private Sub Constancia_CrearEncabezado(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim mFuenteNegrita As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mValorY As Integer

        cb = writer.DirectContent

        'Rectangulo Contenedora 
        'cb.Rectangle( 15, 690, 565, 50)
        cb.Rectangle(15, 645, 565, 65)




        mFuente = FontFactory.GetFont(FontFactory.TIMES_ITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        mFuenteNegrita = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 10)


        cb.BeginText()




        'mValorY = 725
        mValorY = 725 - 30
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Cuit: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosConstancia.Tables("MANAGER").Rows(0).Item("CUIT").ToString.Trim, 60, mValorY, 0)

        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Nro. de Legajo: ", 300, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosConstancia.Tables("TRADES").Rows(0).Item("NRO_COMERCIO").ToString.Trim, 370, mValorY, 0)







        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Razón Social: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosConstancia.Tables("MANAGER").Rows(0).Item("RAZON_SOCIAL").ToString.Trim, 80, mValorY, 0)
        cb.SetFontAndSize(mFuente, 10)



        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Contacto: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosConstancia.Tables("MANAGER").Rows(0).Item("CONTACTO").ToString.Trim, 70, mValorY, 0)
        cb.SetFontAndSize(mFuente, 10)


        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Ubicación: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosConstancia.Tables("MANAGER").Rows(0).Item("DIRECCION").ToString.Trim, 70, mValorY, 0)

        'cb.SetFontAndSize(mFuente, 10)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Localidad: ", 300, mValorY, 0)
        'cb.SetFontAndSize(mFuenteNegrita, 10)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosConstancia.Tables("COMERCIO").Rows(0).Item("COMERCIO_LOCALIDAD").ToString.Trim, 350, mValorY, 0)


        cb.EndText()

    End Sub

    Private Sub Constancia_TablaMovimientos(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter, ByVal mIdComprobante As Integer)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mRenglon As Integer


        cb = writer.DirectContent


        'Rectangulo Contenedora 
        cb.Rectangle(15, (400 - 45), 565, 280)


        'Linea de Encabezado
        cb.SetLineWidth(1)

        cb.MoveTo(15, (660 - 45))
        cb.LineTo(580, (660 - 45))

        cb.Stroke()


        cb.BeginText()
        Dim fuente As iTextSharp.text.pdf.BaseFont


        'mRenglon = (667 - 30)
        mRenglon = (667 - 45)




        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 12)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Período:", 80, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Me.Periodo_DDJJ.ToString, 110, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Fecha de Presentación:", 405, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, Format(Now, "dd/MM/yyyy").ToString.Trim, 500, mRenglon, 0)


        'Actividades
        mRenglon = mRenglon - 18
        fuente = FontFactory.GetFont(FontFactory.TIMES_BOLD, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 15)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "DDJJ DE AGENTES DE RETENCIÓN PRESENTADO.", 210, mRenglon - 10, 0)


        cb.EndText()

    End Sub

#End Region





    Private Sub CrearEncabezado(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim mFuenteNegrita As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mValorY As Integer

        cb = writer.DirectContent

        'Rectangulo Contenedora 
        cb.Rectangle(15, 690, 565, 50)




        mFuente = FontFactory.GetFont(FontFactory.TIMES_ITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        mFuenteNegrita = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 10)


        cb.BeginText()




        mValorY = 725
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Titular: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mCodigoMunicipalidad.ToString, 70, mValorY, 0)
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Tipo de Cuenta: ", 430, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, ObtenerTipoCuenta(dsDatosComprobante.Tables("imponible").Rows(0).Item("TIPOIMPONIBLE").ToString.Trim), 500, mValorY, 0)


        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Domicilio: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosComprobante.Tables("imponible").Rows(0).Item("DOMICILIO").ToString, 70, mValorY, 0)
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Nro. de Cuenta: ", 430, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosComprobante.Tables("imponible").Rows(0).Item("NROIMPONIBLE").ToString, 500, mValorY, 0)


        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Localidad: ", 20, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatosComprobante.Tables("imponible").Rows(0).Item("LOCALIDAD").ToString, 70, mValorY, 0)


        cb.EndText()

    End Sub

    Private Sub TablaMovimientos(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter, ByVal mIdComprobante As Integer)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mRenglon As Integer


        cb = writer.DirectContent


        'Rectangulo Contenedora 
        cb.Rectangle(15, 200, 565, 480)


        'Linea de Encabezado
        cb.SetLineWidth(1)
        cb.MoveTo(15, 660)
        cb.LineTo(580, 660)


        'Linea de Encabezado 2
        cb.MoveTo(15, 645)
        cb.LineTo(580, 645)



        'Line de Vertical
        cb.SetLineWidth(1)
        cb.MoveTo(300, 200)
        cb.LineTo(300, 240)



        'Linea superior
        cb.MoveTo(300, 240)
        cb.LineTo(580, 240)


        'Linea Media
        cb.MoveTo(300, 220)
        cb.LineTo(580, 220)
        cb.Stroke()


        cb.BeginText()
        Dim fuente As iTextSharp.text.pdf.BaseFont


        mRenglon = 667



        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Nº Comprobante:", 80, mRenglon, 0)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("NROCOMP").ToString, 150, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Fecha de Emisión:", 440, mRenglon, 0)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, dsDatosComprobante.Tables("comprobante").Rows(0).Item("FECHA_EMISION").ToString, 500, mRenglon, 0)





        Dim i As Integer
        mRenglon = mRenglon - 20
        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(fuente, 8)




        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "VENCIMIENTO", 400, 225, 0)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_1").ToString, 400, 203, 0)


        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "TOTAL", 560, 225, 0)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_1").ToString, 560, 203, 0)



        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(mFuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Contribuyente", 555, 190, 0)
        cb.EndText()


    End Sub

    Private Sub Talon(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter, ByVal mIdComprobante As Integer)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim mFuenteNegrita As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte

        Dim mRows As Integer = 25


        cb = writer.DirectContent



        'Rectangulo Contenedora 
        cb.Rectangle(15, 25, 300, 145)

        'Rectangulo Fecha Vencimiento
        cb.Rectangle(20, 130, 290, 20)


        'Rectangulos Importes
        cb.Rectangle(20, 70, 290, 53)


        'Primer linea de Cuadro de Importes
        cb.SetLineWidth(1)
        cb.MoveTo(20, 108)
        cb.LineTo(310, 108)


        'Segundo linea de Cuadro de Importes
        cb.SetLineWidth(1)
        cb.MoveTo(20, 89)
        cb.LineTo(310, 89)
        cb.Stroke()


        cb.BeginText()

        'Nombre de la Municipalidad 
        mFuente = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont

        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Municipalidad de " & mNombreMunicipalidad.ToString, 15, (160 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Comprobante de Pago", 15, (148 + mRows), 0)



        '------ Datos Rectangulo Talon --------
        'Asigno fuente
        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        mFuenteNegrita = FontFactory.GetFont(FontFactory.TIMES_BOLD, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(mFuente, 8)



        'Imprimo Datos
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Nº Comprobante:", 120, (132 + mRows), 0)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("NROCOMP").ToString, 185, (132 + mRows), 0)


        cb.SetFontAndSize(mFuenteNegrita, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "1º Vencimiento:", 55, (113 + mRows), 0)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_1").ToString, 108, (112 + mRows), 0)

        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "2º Vencimiento:", 235, (113 + mRows), 0)
        'If (mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString.Trim = "") Then
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "--------", 283, (113 + mRows), 0)
        ' Else
        'cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString, 283, (113 + mRows), 0)
        'End If



        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Vencimiento", 50, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Origen", 120, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Recargos", 210, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Total", 290, (88 + mRows), 0)




        cb.SetFontAndSize(mFuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Banco", 305, 15, 0)
        cb.EndText()








        '----------------- CUADRO MUNICIPALIDAD -----------
        'Rectangulo Contenedora 
        cb.Rectangle(320, 25, 260, 145)



        'Rectangulo Fecha Vencimiento
        cb.Rectangle(325, 130, 250, 20)



        'Rectangulos Importes
        cb.Rectangle(325, 70, 250, 53)



        'Primera Line de Cuadro de Importes
        cb.SetLineWidth(1)
        cb.MoveTo(325, 108)
        cb.LineTo(575, 108)


        'Segundo linea de Cuadro de Importes
        cb.SetLineWidth(1)
        cb.MoveTo(325, 89)
        cb.LineTo(575, 89)
        cb.Stroke()






        cb.BeginText()

        'Nombre de la Municipalidad 
        mFuente = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Municipalidad de " & mNombreMunicipalidad.ToString, 320, (160 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Comprobante de Pago", 320, (148 + mRows), 0)



        '------ Datos Rectangulo Talon --------
        'Asigno fuente
        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(mFuente, 8)


        'Imprimo Datos
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Nº Comprobante:", 410, (132 + mRows), 0)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("NROCOMP").ToString, 475, (132 + mRows), 0)
        cb.SetFontAndSize(mFuenteNegrita, 8)

        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "1º Vencimiento:", 360, (113 + mRows), 0)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_1").ToString, 410, (113 + mRows), 0)

        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "2º Vencimiento:", 500, (113 + mRows), 0)
        'If (mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString.Trim = "") Then
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "--------", 550, (113 + mRows), 0)
        ' Else
        '    cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString, 550, (113 + mRows), 0)
        'End If



        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Vencimiento", 355, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Origen", 420, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Recargos", 480, (88 + mRows), 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Total", 550, (88 + mRows), 0)


        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "1º", 355, (73 + mRows), 0)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_ORIGEN1").ToString, 420, (73 + mRows), 0)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_RECARGO_1").ToString, 480, (73 + mRows), 0)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_1").ToString, 550, (73 + mRows), 0)







        cb.SetFontAndSize(mFuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Municipalidad", 555, 15, 0)
        cb.EndText()



    End Sub

#End Region


    Private Function ObtenerTipoCuenta(ByVal mTipoCuenta As String) As String
        Select Case mTipoCuenta
            Case "I" : Return "INMUEBLE"
            Case "C" : Return "COMERCIO"
            Case "E", "O" : Return "CEMENTERIO"
            Case "V", "R" : Return "VEHICULOS"
            Case Else : Return ""
        End Select
    End Function


End Class
