﻿Option Explicit On
Option Strict On

Imports System.Xml

Imports System.Drawing.Imaging
Imports System.IO

Public Class clsTools

    Public Shared mVersion As String = "4.8.0"


    'ObtainDataXML:
    'This function create a file in disc for fiil the dataset
    Public Shared Function ObtainDataXML(ByVal mXML_AUX As String) As DataSet
        Dim mPath As String
        Dim mFile As String
        Dim dsData As DataSet


        Try

            mPath = System.Configuration.ConfigurationManager.AppSettings("path_datos_temporales").ToString.Trim
            If Not (My.Computer.FileSystem.DirectoryExists(mPath.ToString & "datos_temporales")) Then
                My.Computer.FileSystem.CreateDirectory(mPath.ToString & "datos_temporales")
            End If

            mFile = mPath.ToString & "datos_temporales\" & Format(Now, "ddMMyyyyHHmmss") & ".xml"


            Dim mCreaTeXML As New IO.StreamWriter(mFile.ToString)
            mCreaTeXML.WriteLine(mXML_AUX.ToString)
            mCreaTeXML.Close()

            dsData = New DataSet

            dsData.ReadXml(mFile.ToString)
            My.Computer.FileSystem.DeleteFile(mFile.ToString)


            Return dsData

        Catch ex As Exception
            'mLog = New Clslog
            'mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            'mLog = Nothing
            'Response.Redirect("webfrmerror.aspx", False)
            Return Nothing
        End Try
    End Function


    'HasData
    'Esta funcion valida que un dataset contenga datos
    Public Shared Function HasData(ByVal dsData_AUX As DataSet) As Boolean

        'This one created
        If (dsData_AUX Is Nothing) Then
            Return False
        End If

        'It has at least a table
        If (dsData_AUX.Tables.Count = 0) Then
            Return False
        End If


        'has rows
        If ((dsData_AUX.Tables(0).Rows.Count - 1) = -1) Then
            Return False
        End If


        Return True
    End Function


    'This function returns a cuit with format
    Public Shared Function FormatoCUIT(ByVal pCuit As String) As String
        Dim mCUIT_AUX As String


        Try

            mCUIT_AUX = pCuit.ToString.Trim
            If (pCuit.ToString.Length = 11) Then
                mCUIT_AUX = pCuit.ToString.Substring(1, 2)
                mCUIT_AUX = mCUIT_AUX & "-" & pCuit.ToString.Substring(3, 9)
                mCUIT_AUX = mCUIT_AUX & "-" & pCuit.ToString.Substring(10, 1)
            End If

        Catch ex As Exception
            mCUIT_AUX = pCuit.ToString.Trim
        End Try

        Return mCUIT_AUX.ToString.Trim
    End Function


    'UpdatedsTrade:
    'This function returns a dataset with the data of the Trade
    Public Shared Function UpdatedsTrade(ByVal pDataTrade As DataTable) As DataTable
        Dim dtAUX As DataTable

        Try
            dtAUX = New DataTable
            dtAUX = pDataTrade.Copy

        Catch ex As Exception
            dtAUX = Nothing
        End Try

        Return dtAUX
    End Function


    'AddRowsEmpty:
    'This function returns a dataset with new rows empty
    Public Shared Function AddRowsEmpty(ByVal pdsData As DataSet, ByVal pRowsEmpty As Integer) As DataSet
        Dim mExisting_Rows As Integer
        Dim dsData_AUX As DataSet
        Dim dtData As DataTable
        Dim mRow As DataRow
        Dim i As Integer
        Dim mCountCells As Integer


        Try


            'Obtain the structure and the data of the dataset
            dtData = New DataTable
            dtData = pdsData.Tables(0).Copy


            'Fiil the dataset with the quantity of empty rows spent by parameter
            mExisting_Rows = pdsData.Tables(0).Rows.Count
            For i = mExisting_Rows To pRowsEmpty

                'Believe the row empty and the add a the table
                mRow = dtData.NewRow
                For mCountCells = 0 To dtData.Columns.Count - 1
                    mRow(dtData.Columns(mCountCells).ColumnName.ToString.Trim()) = ""
                Next
                dtData.Rows.Add(mRow)
            Next


            'I generate the dataset to coming back
            dsData_AUX = New DataSet
            dsData_AUX.Tables.Add(dtData)


        Catch ex As Exception
            dsData_AUX = Nothing
        End Try


        Return dsData_AUX
    End Function



    Public Shared Function ObtenerLogo() As Byte()
        Dim mSecuencia As MemoryStream
        Dim mBuffer As Byte()
        Dim mImagen As System.Drawing.Image
        Dim mPath As String


        mSecuencia = Nothing
        mBuffer = Nothing
        mPath = ""

        Try

            mPath = System.Configuration.ConfigurationManager.AppSettings("path_logo").ToString.Trim & "\logo.jpg"
            If (My.Computer.FileSystem.FileExists(mPath.ToString.Trim)) Then

                mImagen = Drawing.Image.FromFile(mPath.ToString.Trim)
                mSecuencia = New MemoryStream()
                mImagen.Save(mSecuencia, ImageFormat.Png)
                mBuffer = mSecuencia.ToArray()
                mSecuencia.Close()
            End If

        Catch ex As Exception
            mBuffer = Nothing
        End Try


        Return mBuffer
    End Function


    Public Shared Function ObtenerNombreMunicipalidad() As String
        Dim mNombreMunicipio As String
        mNombreMunicipio = ""
        Try

            mNombreMunicipio = System.Configuration.ConfigurationManager.AppSettings("Nombre_Municipalidad").ToString.Trim
        Catch ex As Exception
            mNombreMunicipio = ""
        End Try

        Return mNombreMunicipio.ToString.Trim
    End Function



    Public Shared Sub log(ByVal pError As Exception)
        Dim mCadena As String = ""
        Dim mRuta As String


        Try

            'Ruta donde se van a guardar los Log
            mRuta = System.Configuration.ConfigurationManager.AppSettings("path_logo") & "\log"
            If Not (My.Computer.FileSystem.DirectoryExists(mRuta.ToString)) Then
                My.Computer.FileSystem.CreateDirectory(mRuta.ToString)
            End If



            'Armo el Log
            mCadena = "EQUIPO: " & My.Computer.Name.ToString.ToUpper & vbCrLf & _
                      "FECHA: " & Format(Now, "dd/MM/yy hh:mm:ss") & vbCrLf & _
                      "MENSAJE: " & pError.Message.ToString & vbCrLf & _
                      "SOURCE: " & pError.Source.ToString & vbCrLf & _
                      "TARGETSITE: " & pError.TargetSite.ToString & vbCrLf & _
                      "STACKTRACE (Ruta): " & pError.StackTrace.ToString


            Dim mLog As New IO.StreamWriter(mRuta.ToString & "\log" & Format(Now, "ddMMyyyyHHmmss") & ".txt")
            mLog.Write(mCadena.ToString)
            mLog.Close()

        Catch ex As Exception
        End Try


    End Sub


    
End Class
