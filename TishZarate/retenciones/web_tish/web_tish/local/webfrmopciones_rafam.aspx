﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmopciones_rafam.aspx.vb" Inherits="web_tish.webfrmopciones_rafam" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
    
                      
        <!-- Seteo the resources -->
        <script src="../js/funciones.js" type="text/javascript"></script>        
        <link rel='stylesheet' href='../css/estilos_login.css' type='text/css' />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        <%           
            'Seteo el css que voy a utilizar
            Dim mPath_css As String                
            mPath_css = ""
            mPath_css = System.Configuration.ConfigurationManager.AppSettings("css").ToString.Trim()
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_login.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_genericos.css' type='text/css' />")
        %>
                
                
        <!-- ajax -->       
        <script type="text/javascript" language="javascript" src="../js/jquery1-2-6-min.js"></script>
        <script type="text/javascript" language="javascript">
        
            /* Carga del logo de la municipalidad */
            function ajax_load_logo(){   
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_logo.ashx",                                        
                    success: see_response_logo
                });
            }
            
            
            function see_response_logo(html){                   
                if (html != '') {
                    var _control = document.getElementById('div_header');                   
                    _control.innerHTML = html;                   
                    return false;
                }
                
            return true;    
            }          
        </script>
                
                
                
                
                
        
        <!-- reasignar_fuente: -->
        <!-- Esta funcion reasigna el tamaño de la fuente en el documento -->
        <script type="text/javascript">
        
            var tamano_fuente = 10
            
            function reasignar_fuente(valor) {
                if (valor == '1') {
                    tamano_fuente++;
                } else {
                    tamano_fuente--;
                }
                
                
                /* limito los extremos */
                if (tamano_fuente < 10) a = 10
                if (tamano_fuente > 16) a = 16
                
                /* Asigno la fuente al documento */
                document.body.style.fontSize = tamano_fuente + "px"
            }
        </script>
        
        
        
        <style type="text/css">
      
            .style1
            {
                width: 168px;
                text-align: left;
            }
    </style>
        
        
        <!-- recursos para crear la ventana modal -->
	    <link rel="stylesheet" href="../modal/Assets/LightFace.css" />	
	    <script src="../modal/mootools.js" type="text/javascript"></script>
	    <script src="../modal/mootools-more-drag.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.IFrame.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Image.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Request.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Static.js" type="text/javascript"></script>
        

        <!-- Logout -->
        <script type="text/javascript">
        	function logout() {	 	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema TISH :: Cerrar sesión', 
				    width: 250,
				    height: 50,
	  			    content: 'Usted esta a punto de cerrar su sesión.\n¿Desea continuar?',
	 			    buttons: [		
	 			        {
						    title: 'Aceptar',
						    event: function() { this.close();window.location= "../webfrmindex.aspx";}
					    },
					    {
						    title: 'Cerrar',
						    event: function() { this.close();}
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }        
        </script>

        
       
       
        <title>Sistema TISH :: Opciones de acceso</title>	
        <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
    
    </head>


    <body>
    
        
        <!-- div container of the page -->	
        <div id="wrap">
                  
            <!-- div header -->
		    <div id="header">
			
			   <!-- div container font -->
                <div id="container_font">			        
		            <div class="div_font" style="padding:0px 5px 0px 5px;">
                        <a href="#" title="Usar fuente menor" class="link_font" onclick="assign_font('-');">A-</a> 
                    </div>
                    
                    <div class="div_font">
		                <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 
                    </div>		            
                    
		            <div class="div_font">
		                <a href="#" title="Usar fuente mayor" class="link_font" onclick="assign_font('+');">A+</a> 
		            </div>	            
                </div>
                 
                
                 <div id="div_header"  class="div_image_logo">                
 	                <img  class="image_logo" alt="" src="../imagenes/topiz.jpg"  />
                </div>   
   
                    
			    <h1 id="logo-text">Tasa por Inspección de Seguridad e Higiene</h1>			
			    <h2 id="slogan">
                    <asp:Label ID="lblslogan_municipalidad" runat="server" Text=""></asp:Label></h2>	
		    </div>
	  
	  
	  		
	  
	        <!-- div container -->		
	        <div id="content-wrap">
    	  
                 <!-- div sub container -->		
	  		    <div id="main">     	   
				    <a name="Sistema TISH"></a>
    				
                        <!-- div container page title -->
                        <div id="div_title" style="background-color:Black;" align ="left" >
                            <strong class="titulo_page">Sistema TISH :: Opciones de acceso</strong>
                        </div>
                    
                    <!-- -->
                    <div id="div_container" style="width:100%; height:350px;">
                    
                       
                      
                    
                        <!-- div container left -->
                        <div id="div_container_left" style="width:100%; float:left; height:300px;">
                    
                            <!-- greet user -->				
                            <div style="padding-left:20px;">
                                <table>
                                    <tr>
                                        <td>
                                            <img alt="" src="../imagenes/user.png"  style=" width:60px; height:60px; border:none"/>
                                        </td>
                                        <td>
                                            <h1>
                                                
                                                <asp:Label ID="lblname_user" runat="server" ForeColor="Black"></asp:Label>                                    
                                            </h1>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                    
                            <form id="form_option"  runat="server">
                                <div id="div_table_comercio" align="center" style="padding:0px 0px 0px 0px;" >
                                    <table border="5" align="center" style="width:50%;">
                                    <tr>
                                        <td align="center"><strong class="titulo">Opciones</strong></td>
                                    </tr>
                                        <tr>
                                            <td align="center">
                                                <table  align ="center" style="width:auto;  padding: 5px 5px 0px 5px;">
                                                                                                                 
                                                      
                                                    <tr>                                                    
                                                        <td align="left">
                                                            <asp:LinkButton ID="link_new_manager" runat="server">Alta de usuarios Web</asp:LinkButton>
                                                        </td>                                                                                                   
                                                    </tr>
                                                    
                                                    
                                                    <tr>                                                    
                                                        <td align="left">
                                                            <asp:LinkButton ID="link_edit_manager" runat="server">Edición de usuarios Web</asp:LinkButton>
                                                        </td>                                                                                                   
                                                    </tr>

                                                    
                                                    <tr>
                                                        <td align="left">
                                                            <asp:LinkButton ID="link_clear_key" runat="server">Generación de nueva clave de acceso Web</asp:LinkButton>
                                                        </td>                                                                                                   
                                                    </tr>
                                                    
                                                    
                                                    <tr>
                                                        <td align="left">
                                                            <asp:LinkButton ID="link_down_user" runat="server">Baja de usuarios Web</asp:LinkButton>
                                                        </td>                                                                                                   
                                                    </tr>
                                                    
                                                     <tr>
                                                        <td align="left">
                                                            <asp:LinkButton ID="link_up_user" runat="server">Reactivación de usuarios Web</asp:LinkButton>
                                                        </td>                                                                                                   
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td align="left">
                                                            <asp:LinkButton ID="link_abm_date" runat="server">Abm de fechas de vencimiento</asp:LinkButton>
                                                        </td>                                                                                                   
                                                    </tr>
                                                    
                                                    
                                                    <tr>
                                                        <td align="left" style="display:none;">
                                                            <a href="webfrmuserweb_new.aspx">prueba add user web</a>
                                                        </td>                                                                                                   
                                                    </tr>
                                                    
                                                      


                                                    
                                                    <tr>
                                                        <td align="left">
                                                            <a href="#" onclick="javascript:logout();">Cerrar sesión</a>
                                                        </td>                                                
                                                    </tr>
                                                    
                                                  
                                                   
                                                                
                                                </table>
                                            </td>   
                                        </tr>                                
                                    </table>
                                </div>
                            </form>
                                
                                                        
                    
                        </div>
                                         
                    </div>
                    
                    
    			
    					
    						
    						
    				
    			
    				
    								
	  		    </div> 	
    			  
    	  		 	
    		
		    <!-- content-wrap ends here -->
		    </div>
	
	
	        <!-- div footer -->	
		    <div id="footer">
		
		   <div id="div_pie_municipalidad" 
                style="width:70%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		        Municipalidad de
		            <strong> 
		                <asp:Label ID="lblnombre_municipalidad" runat="server" Text="NOMBRE_MUNICIPIO"></asp:Label> 
                    </strong> 
                    
				<br />
				Teléfono: 
				    <strong> 
                        <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                    </strong> 
                 | Email: 
				    <strong> 
                        <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                    </strong> 
                
			</div>
		   
		   <div id="div_pie_grupomdq"  class="footer_grupomdq">
				Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ S.A</a></strong>  
				<br />
				Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				<a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
               <asp:Label ID="lblversion" runat="server" Text=""></asp:Label>
		   </div>
			
			
		</div>	

        
        </div>	
        
    </body>
</html>

