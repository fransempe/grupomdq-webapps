﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmresultado.aspx.vb" Inherits="web_tish.webfrmresultado" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
                
        <!-- Seteo the resources -->
        <script src="../js/funciones.js" type="text/javascript"></script>       
        <link rel="stylesheet" href="../css/estilos_listado.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_botones.css" type="text/css" />        
        <%           
            'Seteo el css que voy a utilizar
            Dim mPath_css As String                
            mPath_css = ""
            mPath_css = System.Configuration.ConfigurationManager.AppSettings("css").ToString.Trim()
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_listado.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_genericos.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_botones.css' type='text/css' />")            
        %> 
        
        
        
        <!-- ajax -->       
        <script type="text/javascript" language="javascript" src="../js/jquery1-2-6-min.js"></script>
        <script type="text/javascript" language="javascript">
        
            /* Carga del logo de la municipalidad */
            function ajax_load_logo(){   
                jQuery.ajax({
                    type:"POST", 
                    url:"../controladores/handler_logo.ashx",                                        
                    success: see_response_logo
                });
            }
            
            
            function see_response_logo(html){                   
                if (html != '') {
                    var _control = document.getElementById('div_header');                   
                    _control.innerHTML = html;                   
                    return false;
                }
                
            return true;    
            }          
        </script>
                
        
        
        <!-- Esta funcion da el efecto de cargando... miestras va al server -->
        <link href="../efecto_ajax/cssUpdateProgress.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" language="javascript">
	        var ModalProgress = '<%= ModalProgress.ClientID %>';         
        </script>
        
        
        <title>Sistema TISH :: Resultado de su operación</title>	
        <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
    
        
        
        <script type="text/javascript">

            function validar_datos() {

                /* -- Nro de Comercio -- */
                //Cadena Vacia
                if (control_vacio(window.document.getElementById('txtnro_comercio'),'Número de legajo:\nDebe ingresar un número de legajo.')) {
                   return false;
                }
                
                //Cadena Valida
                if (!validar_cadena_solo_numeros(window.document.getElementById('txtnro_comercio'),'Número de comercio')) {          
                    return false;
                }
                
            return true;
            }
            
    
    
            /* Valido que la clave exista y le pregunto al usuario si quiere remplazar la clave*/
            function generar_clave() {
            
                /* -- Nro de Comercio -- */
                //Cadena Vacia
                if (control_vacio(window.document.getElementById('txtnro_comercio'),'Número de legajo:\nDebe ingresar un número de legajo.')) {
                   return false;
                }
                
                //Cadena Valida
                if (!validar_cadena_solo_numeros(window.document.getElementById('txtnro_comercio'),'Número de comercio')) {          
                    return false;
                }
                
                
                /* -- Cuit del contribuyente -- */
                //Cadena Vacia
                if (control_vacio(window.document.getElementById('txtcontribuyente_cuit'),'Cuit contribuyente:\nDebe ingresar el cuit del contribuyente.')) {
                   return false;
                }
                
                //Cadena Valida
                if (!validar_cadena_solo_numeros(window.document.getElementById('txtcontribuyente_cuit'),'Cuit contribuyente')) {          
                    return false;
                }
            
            
            
                // Validar comercio cargado
                if (window.document.getElementById('lblcomercio_nombre').innerHTML === '') {          
                    alert('Generacion de clave:\nDebe seleccionar un comercio.');
                    return false;
                }
            
                
                           
                
                return imprimir_clave();
                
            
            return true;
            }
            
    
    
    /* I warn him the user who for the trade selecciondo already exists a key */    
    function existing_key() {
        if (window.document.getElementById('lblestado_clave').innerHTML == 'CLAVE EXISTENTE.') {
            return confirm ('El comercio seleccionado ya contiene un clave para el ingreso Web.\n'
                            + '¿Desea remplazarla de todas formas?')
        }
    }
    
    
    
    /* Valido que clave exista y le recomiendo que use la ultima version de Acrobat */    
    function imprimir_clave() {
        var existing_key_aux = false;
        
        
        /* I obtain the value of the function */   
        existing_key_aux = existing_key();
       
       
        if (existing_key_aux) {
                     
            if (window.document.getElementById('lblestado_clave').innerHTML != 'CLAVE INEXISTENTE.') {
                return confirm("Usted está a punto de generar una constancia de clave.\n"
                               + "Para el correcto funcionamiento se recomienda descargar la última versión de Adobe Reader.\n"
                               + "¿Desea continuar?")
            }
        }
        
    return existing_key_aux;
    }
    
    
    
    
    
    /* Esta funcion genera el mensaje al usuario de que esta a punto de cerrar la session de usuario*/
    function cerrar_session() {
        return confirm("Usted esta a punto de cerrar su sesión\n¿Desea continuar?")              
    }
    
    
    
    /* Esta Funcion ASIGNA ESTILO a un BOTON */          
    function efecto_over(boton){
        boton.className = "boton_gris"; 	        
	}
	    

    /* Esta Funcion ASIGNA ESTILO a un BOTON */          
  	function efecto_out(boton){
        boton.className = "boton_verde";    
	}


 
    
    
    
    


    

</script>
        
        
    </head>

    <body>
    
        
        <!-- wrap starts here -->	
        <div id="wrap">
                     
		    <div id="header">
		          
		        <!-- div container font -->
                <div id="container_font">			        
		        <div class="div_font" style="padding:0px 5px 0px 5px;">
                    <a href="#" title="Usar fuente menor" style="font-family: Jokerman; font-size: large; font-weight: bold;" onclick="assign_font('-');">A-</a> 
                </div>
                
                <div class="div_font">
		            <a href="#" title="Usar fuente por defecto" style="font-family: Jokerman; font-size: large; font-weight: bold;" onclick="assign_font('=');">A</a> 
                </div>		            
                
		        <div class="div_font">
		            <a href="#" title="Usar fuente mayor" style="font-family: Jokerman; font-size: large; font-weight: bold;" onclick="assign_font('+');">A+</a> 
		        </div>	            
            </div>
                    
		     
		    <div id="div_header"  class="div_image_logo">                
 	            <img  class="image_logo" alt="" src="../imagenes/topiz.jpg"  />
            </div>  
		                
			<h1 id="logo-text">Tasa por Inspección de Seguridad e Higiene</h1>			
			<h2 id="slogan"><asp:Label ID="lblslogan_municipalidad" runat="server" Text=""></asp:Label></h2>	
					
						
			
		</div>
	  
            <!-- content-wrap starts here -->
            <div id="content-wrap">
    	  
    	  
    	        <!-- div container page title -->
                <div id="div_title" style="background-color:Black;" align ="left" >
                    <strong class="titulo_page">Sistema TISH :: Resultado de su operación</strong>
                </div>
    	  
    	  
                <!-- div container right -->
                <div id="div_optiones"  class="menu_options">
                    
                    <div id="div_comercio" style=" padding-top:20px;">
                            <div align="center">
                                <table style="width:95%">
                                    <tr>
                                        <td  rowspan="2"><img id="user" alt="" src="../imagenes/user.png"  style=" width:60px; height:60px; border:none"/></td>
                                        <td><p  class="titulo_usuario"align ="left" style="color:Black; height: 25px;">Bienvenido</p></td>
                                    </tr>
                                    <tr>                                
                                        <td>
                                            Usuario: <asp:Label ID="lblnombre_usuario" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        
                    <h1 align ="left" style="color:Black">Opciones</h1>                    			
                    <ul class="sidemenu">                                       	                    
                        <li><a href="webfrmopciones_rafam.aspx" >Volver a opciones</a></li>	                					
                        <li><a href="../webfrmindex.aspx" onclick="javascript:return cerrar_sesion_usuario();">Cerrar sesión</a></li>	                					
                    </ul>	
                </div>
    	    
    	  
    	  
  		        <div id="main"> 
                    
                    <form id="formulario" runat="server">
                        
                        
                       
                    
                    
                   
                    
                    
                 
                        
                        
                      

                    
                        <div id="div_container_trade" align="center" style="padding-bottom:30px;"  runat="server">
                    
            		        <script type="text/javascript" src="../efecto_ajax/jsUpdateProgress.js"></script>		
	                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	                        
	                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
	                            <ContentTemplate >
            		     
            		     
            		            <!-- message for the user -->
                                
                        
                                <!-- div add trade -->
                                <div id="add_trade" align="center" style="width:90%">
                                
                                         
                                         
                                         
                                         
                                         
                                    <div id="div_list_trades" style="padding-top:20px; width:100%;">
                                        <table id="table_messenger_pdf" border="5" align="center" style="width:100%; height:236px;" runat="server">
                                            <tr>
                                                <td style= "padding-top:10px;">
                                                    <table id="table_period" align="center" style="width:100%;">
                                                        <tr align="center" style="font-size:medium">
                                                            <td colspan="2">
                                                                <div id="mensaje">
                                                                    <asp:Label ID="lblmessenger_pdf" runat="server" Text=""  CssClass="titulo_2"></asp:Label>
                                                                </div>
                                                            </td>
                                                            
                                                        <tr> 
                                                            <td colspan="2">
                                                                <p>Desde acá usted podrá descargar su constancia y nueva clave</p>
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td align="right" style="width:55%;">
                                                                <p><strong>DESCARGAR CONSTANCIA:</strong></p>
                                                            </td>
                                                            <td align="left" style="width:45%;">
                                                                
                                                                <a id="link_pdf" href="#" runat="server"><img alt="Constancia" src="../imagenes/pdf.jpg" width="42"  height="65"></a>
                                                                
                                                            </td>
                                                        </tr>
                                                            
                                                        </tr>
                                                    </table>
                                                
                                                                   		                            
            		                            		                            
		                                            <div  align="center" style="padding-top:15px;"> 
		                                                <table>
		                                                    <tr>
		                                                        
		                                                        <td>
                                                                    <asp:LinkButton ID="link_volver" runat="server" CssClass="link">Volver a opciones</asp:LinkButton>
                                                                </td>
		                                                    </tr>		                                        
		                                                </table>	
                                                    </div>	
                                                </td>
                                            </tr>
                                        </table>                                                                    
                                        
                                        
                                        <table id="table_messenger" border="5" align="center" style="width:100%; height:236px;"   runat="server">
                                            <tr>
                                                <td style= "padding-top:10px;">
                                                    <table id="table1" align="center" style="width:100%;">
                                                        <tr align="center" style="font-size:medium">
                                                            <td colspan="2">
                                                                <div id="Div1">
                                                                    <asp:Label ID="lblmessenger" runat="server" Text=""  CssClass="titulo_2"></asp:Label>
                                                                </div>
                                                            </td>
                                                  
                                                        </tr>
                                                  
                                                        
                                                      
                                                            
                                                         
                                                    </table>
                                                
                                                                   		                            
            		                            		                            
		                                            <div  align="center" style="padding-top:15px;"> 
		                                                <table>
		                                                    <tr>
		                                                        <td>
		                                                           <asp:LinkButton ID="link_volver_mensaje" runat="server">Volver a opciones</asp:LinkButton>	                                                            
		                                                        </td>
		                                                    </tr>		                                        
		                                                </table>	
                                                    </div>	
                                                </td>
                                            </tr>
                                        </table>                                                                    
                                    </div>
                                        
                                        
                                        
                                     
                                     
                                </div>
                              	
                              
                        
	                      </ContentTemplate >		
                            </asp:UpdatePanel>
                        
                            <!-- panel ajax-->
                            <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
                                <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server">
                                    <ProgressTemplate>
	                                    <div style="position: relative; text-align: center;">
		                                    <img src="../efecto_ajax/loading.gif" style="vertical-align: middle" alt="Processing" />
		                                    Cargando ...
	                                    </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </asp:Panel>
                            <ajaxToolkit:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress" BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
                
                        
	                    </div>
	                    
	                    
                       
	                </form>
    				
      								
  		        </div> 	
    
	        </div>
		
		
		    <!-- div data footer -->
		    <div id="footer">	
		       <div id="div_pie_municipalidad" 
                    style="width:70%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		            Municipalidad de
		                <strong> 
		                    <asp:Label ID="lblnombre_municipalidad" runat="server" Text="NOMBRE_MUNICIPIO"></asp:Label> 
                        </strong> 
                        
				    <br />
				    Teléfono: 
				        <strong> 
                            <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                        </strong> 
                     | Email: 
				        <strong> 
                            <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                        </strong> 
                    
			    </div>
    		   
		       <div id="div_pie_grupomdq"  style="width:28%; height:100%;  float:right; padding-top:5px; ">
				    Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ S.A</a></strong>  
				    <br />
				    Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				    <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
                   <asp:Label ID="lblversion" runat="server" Text=""></asp:Label>
		       </div>
		    </div>	
		    
        </div>	



    </body>
</html>
