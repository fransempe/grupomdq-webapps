﻿Option Explicit On
Option Strict On

Partial Public Class webfrmabm_fechas_vto
    Inherits System.Web.UI.Page


#Region "Variables"

    Private mWS As ws_tish.Service1
    Private mXML As String
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not (IsPostBack) Then
            Call Me.ValidarSession()
            Call Me.CargarPie()
        End If

    End Sub

    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("user_name") IsNot Nothing) Then
                If (Session("user_name").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        If (mLogin_OK) Then
            lblname_user.Text = "Bienvenido usuario " & Session("user_name").ToString.Trim
        Else
            Response.Redirect("webfrmlogin_rafam.aspx", False)
        End If


    End Function

    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If

    End Sub



    Protected Sub btnadd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnadd.Click

    End Sub


    Private Function ValidateData() As Boolean

        Call AddDayExpery()

    End Function

    Private Sub AddDayExpery()


        Try

            'Me.mWS = New ws_tish.Service1
            'Me.mXML = Me.mWS.AddDayExpiry("05", 2010, 10, Now, Now)
            'Me.mWS = Nothing



        Catch ex As Exception
        Finally
        End Try


    End Sub
End Class