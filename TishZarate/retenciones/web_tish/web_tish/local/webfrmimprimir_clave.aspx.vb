﻿Partial Public Class webfrmimprimir_clave
    Inherits System.Web.UI.Page

    Private ObjImpresionPDF As ClsPDF
    Private mDataManager() As String
    Private mWS As ws_tish.Service1

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mRutaFisica As String
        Dim mNombreArchivoComprobante As String
        Dim mArchivo As String
        Dim dsVariablesConfigurables As DataSet




        Try

            Session("state_pdf") = False
            Session("type_messenger") = "pdf"

            'Creo Directorio y Archivo en Disco
            mRutaFisica = Request.ServerVariables("APPL_PHYSICAL_PATH").ToString & "comprobantespdf"
            If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString)) Then
                My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString)
            End If
            mNombreArchivoComprobante = "clave" & Format(Now, "ddMMyyyyHHmmss")
            Session("name_pdf") = mNombreArchivoComprobante.ToString.Trim
            mArchivo = mRutaFisica & "\" & mNombreArchivoComprobante & ".pdf"




            'Instancio el Objeto
            ObjImpresionPDF = New ClsPDF


            'Asigno la ruta del PDF
            ObjImpresionPDF.RutaFisica = mArchivo.ToString




            ' ----------- Datos municipalidad -----------
            ObjImpresionPDF.MunicipalidadData = Me.ObtainMunicipalidadData()


   

            'Data Manager 
            If (Session("data_manager") IsNot Nothing) Then
                mDataManager = Session("data_manager").ToString.Split("|")
                ObjImpresionPDF.DataManager = mDataManager
            End If




            'Obtengo las Variables Configurables
            dsVariablesConfigurables = New DataSet
            dsVariablesConfigurables.ReadXml(Request.ServerVariables("APPL_PHYSICAL_PATH").ToString.ToString.Trim & "\local\variables_configurables.xml")
            ObjImpresionPDF.VariablesConfigurables = dsVariablesConfigurables



            'Genero el PDF
            ObjImpresionPDF.CrearPDF()



            Session("state_pdf") = True
            Response.Redirect("webfrmresultado.aspx", False)

        Catch ex As Exception
            'mLog = New Clslog
            'mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            'mLog = Nothing
            Session("state_pdf") = False
            Response.Redirect("webfrmerror.aspx", False)
        End Try
    End Sub



    Private Function ObtainMunicipalidadData() As DataSet
        Dim dsMunicipalidadData As DataSet
        Dim dsMunicipalidadData_XML As String


        Try

            'Obtengo los datos del WS
            Me.mWS = New ws_tish.Service1
            dsMunicipalidadData_XML = Me.mWS.ObtainMunicipalidadData()


            'Genero el dataset
            dsMunicipalidadData = New DataSet
            dsMunicipalidadData = clsTools.ObtainDataXML(dsMunicipalidadData_XML.Trim)


        Catch ex As Exception
            dsMunicipalidadData = Nothing
        Finally
            Me.mWS = Nothing
        End Try

        Return dsMunicipalidadData
    End Function

End Class