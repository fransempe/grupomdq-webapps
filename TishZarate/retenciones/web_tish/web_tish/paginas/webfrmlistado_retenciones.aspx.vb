﻿Public Partial Class webfrmlistado_retenciones
    Inherits System.Web.UI.Page

#Region "Variables"
    Private mWS As ws_tish.Service1
    Private dtDataComercio_AUX As DataTable
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim mPeriod As String


        If Not (IsPostBack) Then

            Call Me.SloganMunicipalidad()
            Call Me.ValidarSession()
            Call Me.CargarPie()

            lblname_user.text = Session("NombreUsuario-Cuit").ToString.Trim

            'If System.Configuration.ConfigurationManager.AppSettings("upload_archivo") = True Then
            '    div_contenedor_archivo.Visible = True
            'Else
            '    div_contenedor_archivo.Visible = False
            'End If
            'Si es Zarate que oculte/muestre el nombre de fantasía, el contribuyente y el menu.
            If System.Configuration.ConfigurationManager.AppSettings("zarate").ToString.Trim() = "True" Then
                MenuZarate.Visible = True
                MenuNormal.Visible = False
                'Carga o no si es Agente de retencion, la opcion para cargar el archivo.
                If Session("Agente_retencion").ToString.Trim = "S" Then
                    OpcionCargaArchivo.Visible = True
                End If
            Else
                MenuZarate.Visible = False
                MenuNormal.Visible = True
                OpcionCargaArchivo.Visible = False
            End If
            'Si el subidor de archivos esta activado lo muestra
            'If System.Configuration.ConfigurationManager.AppSettings("upload_archivo").ToString.Trim() = "True" Then
            '    Me.div_contenedor_archivo.Visible = True
            'Else
            '    Me.div_contenedor_archivo.Visible = False
            'End If
            'I fill combo whit the periods
            Call Me.LoadComboPeriods()
        End If


    End Sub
    Private Sub SloganMunicipalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema TISH."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema TISH."
        End If

    End Sub

    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("contribuyente_nombre") IsNot Nothing) Then
                If (Session("contribuyente_nombre").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If



        If (mLogin_OK) Then

            'I Update the dataset
            dtDataComercio_AUX = clsTools.UpdatedsTrade(CType(Session("data_trade"), DataTable))
            Session("NROCOMERCIO") = dtDataComercio_AUX.Rows(0).Item("COMERCIO_NUMERO").ToString.Trim()
            Me.lblcomercio_user.Text = Session("NROCOMERCIO")
        Else
            Response.Redirect("webfrmlogin.aspx", False)
        End If

    End Function

    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            Me.lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            Me.lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If


        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub


    'LoadComboPeriods
    'This function loads the combo whit the periods
    Private Sub LoadComboPeriods()
        Dim dsDataPeriods As DataSet
        Dim mXML_AUX As String
        Dim mHTML As String


        Try

            'I obtain the periods
            Me.mWS = New ws_tish.Service1
            mXML_AUX = Me.mWS.ObtainAllDatePeriods()
            Me.mWS = Nothing


            'I read the XML
            dsDataPeriods = New DataSet
            dsDataPeriods = clsTools.ObtainDataXML(mXML_AUX.ToString.Trim)
            Session("ROWS_PERIODS") = dsDataPeriods.Tables("ROWS_PERIODS").Copy

            'If the dsListadoDDJJ is not empty I assign information
            If (clsTools.HasData(dsDataPeriods)) Then
                Me.div_period.InnerHtml = Me.CreateHTMLPeriod(dsDataPeriods.Tables("ROWS_PERIODS"))
            End If


        Catch ex As Exception

        Finally
            mWS = Nothing
        End Try

    End Sub

    Private Function CreateHTMLPeriod(ByVal dtData As DataTable) As String
        Dim mHTML As String
        Dim mHTML_ComboItems As String
        Dim mValue As String


        Try

            mHTML_ComboItems = ""
            mValue = ""
            If Not (CBool(Session("Rectificativa"))) Then
                If (dtData IsNot Nothing) Then
                    For i = 0 To dtData.Rows.Count - 1

                        mValue = dtData.Rows(i).Item("FV_CUOTA1").ToString.Trim & "#" & dtData.Rows(i).Item("FV_CUOTA2").ToString.Trim & "#" & dtData.Rows(i).Item("FV_PRESENTACION").ToString.Trim
                        If (i = 0) Then
                            mHTML_ComboItems = mHTML_ComboItems & "<option value='" & mValue.Trim & "' selected='selected'>" & dtData.Rows(i).Item("ANIO").ToString.Trim & " - " & dtData.Rows(i).Item("CUOTA").ToString.Trim & "</option>"
                            'Guardo en variable de session el año y cuota para el archivo upload
                            Session("ANIO") = dtData.Rows(i).Item("ANIO").ToString.Trim
                            Session("CUOTA") = dtData.Rows(i).Item("CUOTA").ToString.Trim
                        Else
                            mHTML_ComboItems = mHTML_ComboItems & "<option value='" & mValue.Trim & "'>" & dtData.Rows(i).Item("ANIO").ToString.Trim & " - " & dtData.Rows(i).Item("CUOTA").ToString.Trim & "</option>"
                            Session("ANIO") = dtData.Rows(i).Item("ANIO").ToString.Trim
                            Session("CUOTA") = dtData.Rows(i).Item("CUOTA").ToString.Trim
                        End If
                    Next
                End If

            Else
                mHTML_ComboItems = mHTML_ComboItems & "<option>" & Session("Rectificativa_Period").ToString.Trim & "</option>"
            End If



            mHTML = ""
            mHTML = mHTML & "<table id='table_period' align='center'  border='0' style='width:90%; border:1px solid #C0C0C0;'>"
            mHTML = mHTML & "   <tr>"
            mHTML = mHTML & "   </tr>"
            mHTML = mHTML & "   <tr><td colspan='4' class='linea'><hr /></td></tr>"
            mHTML = mHTML & "   <tr>"
            mHTML = mHTML & "       <td style='width:10%'>Período:</td>"
            mHTML = mHTML & "       <td style='width:60%' align='left'>"
            mHTML = mHTML & "           <select id = 'cmbperiod' name='cmbperiod' style='width:40%;' onchange=""javascript:refresh_date();"">" & mHTML_ComboItems.Trim & "</select>"
            mHTML = mHTML & "       </td>"
            mHTML = mHTML & "   </tr>"
            mHTML = mHTML & "   <tr><td colspan='4' class='linea'><hr /></td></tr>"
            mHTML = mHTML & "</table>"



        Catch ex As Exception
            mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                        "<p id='lblmessenger' style='color: Red; font-weight: inherit;'> " & _
                            "Error al intentar obtener los datos." & _
                        "</p>" & _
                    "</div>"
        End Try

        Return mHTML.Trim
    End Function
End Class