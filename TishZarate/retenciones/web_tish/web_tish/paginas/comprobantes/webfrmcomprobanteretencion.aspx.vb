﻿Option Explicit On
Option Strict On


'Web Services
Imports web_tish.ws_tish.Service1

Partial Public Class webfrmcomprobanteretencion
    Inherits System.Web.UI.Page


#Region "Variables"

    Private Structure SLogo
        Dim LogoOrganismo As Byte()
        Dim RenglonOrganismo As String
        Dim RenglonNombreOrganismo As String
    End Structure

    Private mPDF As clsPDFComprobanteRetencion
    Private mLogo As SLogo
    Private mWS As web_tish.ws_tish.Service1

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mRutaFisica As String
        Dim mNombreArchivoComprobante As String
        Dim mArchivo As String
        Dim mCodigoMunicipalidad As String


        Try

            'Creo Directorio y Archivo en Disco
            mRutaFisica = Request.ServerVariables("APPL_PHYSICAL_PATH").ToString & "comprobantespdf"
            If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString)) Then
                My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString)
            End If
            mNombreArchivoComprobante = "PDFConstanciaRetencion" & Format(Now, "HHmmss")
            mArchivo = mRutaFisica & "\" & mNombreArchivoComprobante & ".pdf"



            Session("type_messenger") = "pdf"
            Session("name_pdf") = mNombreArchivoComprobante.ToString.Trim



            'Creo y Seteo el Nombre del Logo dependiendo de la Conexion
            mCodigoMunicipalidad = ""

            'Genero el PDF
            mPDF = New clsPDFComprobanteRetencion


            mPDF.RutaFisica = mArchivo.ToString
            mPDF.DatosMunicipalidad = Me.ObtenerDatosMunicipalidad()

            If (Session("municipalidad_nombre") IsNot Nothing) Then
                mPDF.NombreMunicipalidad = Session("municipalidad_nombre").ToString.Trim
            End If

            If (Session("Nombre_Archivo") IsNot Nothing) Then
                mPDF.mNombre_Archivo = Session("Nombre_Archivo").ToString.Trim
            End If

            mWS = New ws_tish.Service1
            mPDF.DatosConstancia = Me.mWS.ObtainDataManager(Session("CUIT").ToString)
            mPDF.Periodo_DDJJ = Session("ANIO").ToString.Trim + " - " + Session("CUOTA").ToString.Trim

            mPDF.CrearPDF()

            Response.Redirect("../webfrmresultados_genericos.aspx", False)

            'Else
            'Call LimpiarSession()
            'End If

        Catch ex As Exception
            Response.Redirect("../webfrmerrorweb.aspx", False)
        End Try
    End Sub


#Region "procedimientos y Funciones"



    Private Sub LimpiarSession()
        Session.RemoveAll()
        Session.Clear()
        Session.Abandon()
        Response.Redirect("../webfrmindex.aspx", False)
    End Sub

    Private Function ObtenerTipoCuenta(ByVal mTipoCuenta As String) As String
        Select Case mTipoCuenta
            Case "I" : Return "INMUEBLE"
            Case "C" : Return "COMERCIO"
            Case "E", "O" : Return "CEMENTERIO"
            Case "V" : Return "VEHICULOS"
            Case Else : Return ""
        End Select
    End Function

#End Region


    Private Function CargarDataset() As DataSet

        Dim dsdatos As DataSet
        Dim dtcomercio As DataTable
        Dim dtrubros As DataTable

        Dim mFila As DataRow



        'Create the columns
        dtcomercio = New DataTable("COMERCIO")
        mFila = dtcomercio.NewRow
        dtcomercio.Columns.Add("COMERCIO_NUMERO")
        dtcomercio.Columns.Add("COMERCIO_RAZONSOCIAL")
        dtcomercio.Columns.Add("COMERCIO_NOMBREFANTASIA")
        dtcomercio.Columns.Add("COMERCIO_NOMBRE")
        dtcomercio.Columns.Add("COMERCIO_UBICACION")
        dtcomercio.Columns.Add("COMERCIO_LOCALIDAD")
        dtcomercio.Columns.Add("CONTRIBUYENTE_CUIT")
        dtcomercio.Columns.Add("CONTRIBUYENTE_NOMBRE")

        mFila = dtcomercio.NewRow
        mFila("COMERCIO_NUMERO") = "1234"
        mFila("COMERCIO_RAZONSOCIAL") = "martin-godoy-nizoli agustin-oscar-carlos"
        mFila("COMERCIO_NOMBREFANTASIA") = "martin-godoy-nizoli agustin-oscar-carlos"
        mFila("COMERCIO_NOMBRE") = "martin-godoy-nizoli agustin-oscar-carlos"
        mFila("COMERCIO_UBICACION") = "Calle alvarado, Puerta 35, Piso 32, Depto. 3 A "
        mFila("COMERCIO_LOCALIDAD") = "Mar del plata (7600)"
        mFila("CONTRIBUYENTE_CUIT") = "20305067858"
        mFila("CONTRIBUYENTE_NOMBRE") = "martin-godoy-nizoli agustin-oscar-carlos"
        dtcomercio.Rows.Add(mFila)





        'Create the columns
        dtrubros = New DataTable("RUBROS")
        mFila = dtcomercio.NewRow
        dtrubros.Columns.Add("RUBRO_CODE")
        dtrubros.Columns.Add("RUBRO_DESCRIPTION")
        dtrubros.Columns.Add("RUBRO_EMPLOYEES")
        dtrubros.Columns.Add("RUBRO_AMOUNT")


        mFila = dtrubros.NewRow
        mFila("RUBRO_CODE") = "1234"
        mFila("RUBRO_DESCRIPTION") = "fabricacion de lapices, lapiceras, boligraods, se asdada"
        mFila("RUBRO_EMPLOYEES") = "1999"
        mFila("RUBRO_AMOUNT") = "10000.00"
        dtrubros.Rows.Add(mFila)

        mFila = dtrubros.NewRow
        mFila("RUBRO_CODE") = "4234234"
        mFila("RUBRO_DESCRIPTION") = "fabricacion de lapices, lapiceras, boligraods, se asdada asdlañlsdkaskñldmsakñd"
        mFila("RUBRO_EMPLOYEES") = "12"
        mFila("RUBRO_AMOUNT") = "11111.36"
        dtrubros.Rows.Add(mFila)




        dsdatos = New DataSet
        dsdatos.Tables.Add(dtcomercio)
        dsdatos.Tables.Add(dtrubros)


        Return dsdatos

    End Function


    Private Function ObtenerDatosMunicipalidad() As DataSet
        Dim dsDatos As DataSet
        Dim mXML As String

        Try

            'Obtengo los datos de la municipalidad
            mWS = New ws_tish.Service1
            mXML = mWS.ObtainMunicipalidadData()
            mWS = Nothing


            dsDatos = New DataSet
            dsDatos = ClsTools.ObtainDataXML(mXML.ToString.Trim)

            Return dsDatos
        Catch ex As Exception
            'mLog = New Clslog
            'mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            'mLog = Nothing
            Response.Redirect("../webfrmerrorweb.aspx", False)
            Return Nothing
        End Try

    End Function
End Class
