﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmpresentacion_retencion.aspx.vb" Inherits="web_tish.webfrmpresentacion_retencion" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
    
    
     <style type="text/css">
    
    /* base semi-transparente */
    .overlay{
        display: none;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 720px;
        background: #000;
        z-index:1001;
		/*opacity=.75;*/
        -moz-opacity: 0.75;
        /*filter: alpha(opacity=75);*/
    }
	
    /* estilo para lo q este dentro de la ventana modal */
    .modal {
        display: none;
        position: absolute;
        /*
        top: 25%;
        left: 25%;
        width: 50%;
        height: 50%;
        */
        
        left: 50%;
        top: 50%;
        width: 700px;
        height: 350px;
        margin-top: -120px;
        margin-left: -250px;
        
        padding: 0px;
        background: #fff;
		color: #333;
        z-index:1002;
        overflow: auto;        
    }
    
    
    .modal_column_title{ width:50%; text-align:right;}
	.modal_column_data{ width:50%; text-align:left;}
	
    
    .modal_boton {
	    height: 30px;
	    width:90px;
	    background-color:#5b6951;
	    line-height: 35px;
    	
	    margin: 2px 5px 2px 5px;
	    color: #fff;
	    font-size: 1em;
	    font-weight:bold;
	    text-decoration: none;
	    cursor:pointer;
	
    }

    .modal_boton:hover  {
	    color:white;
	    background-color:Gray;	
    }
    
    
    .modal_div_header {
    	background-color:#5b6951;
    	color:White;
    	padding-top:5px;
    	padding-bottom:5px;
    }
    
    
    .modal_div_header_h2 {
    	font-family: 'Trebuchet MS', Arial, sans-serif;
	    font-weight: bold;
	    font-size: 1.3em;
	    text-transform: uppercase;
    }
    
    
    .modal_div_resource {    	
    	padding-top:20px;
    	width:80%;
    }
    
       
         </style>
        
        <!-- Variable pública -->
        <script type="text/javascript">
            var intIndiceRubro = 0;            
        </script>
               
               
        <!-- recursos para crear la ventana modal -->
        
	    <link rel="stylesheet" href="../modal/Assets/LightFace.css" />	
	    <script src="../modal/mootools.js" type="text/javascript"></script>
	    <script src="../modal/mootools-more-drag.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.IFrame.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Image.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Request.js" type="text/javascript"></script>
	    <script src="../modal/Source/LightFace.Static.js" type="text/javascript"></script>
        
        <script type="text/javascript">    	    
      	    function logout() {	 	    	 	        
 	            box = new LightFace({ 
 			        title: 'Sistema TISH :: Cerrar sesión', 
			        width: 250,
			        height: 50,
  			        content: '<div align="left">Usted esta a punto de cerrar su sesión.\n¿Desea continuar?</div>',
 			        buttons: [		
 			            {
					        title: 'Aceptar',
					        event: function() { this.close();window.location= "webfrmlogin.aspx";}
				        },
				        {
					        title: 'Cerrar',
					        event: function() { this.close();}
				        }
			        ]
 		        });
 		        box.open();		
 	        }     
 	        
 	        
 	        function change_trade() {	 	    	 	        
	 	        box = new LightFace({ 
	 			    title: 'Sistema TISH :: Cambiar comercio', 
				    width: 300,
				    height: 50,
	  			    content: '<div align="left">Usted esta a punto de cambiar de comercio\n¿Desea continuar?</div>',
	 			    buttons: [		
	 			        {
						    title: 'Aceptar',
						    event: function() { this.close();window.location= "webfrmseleccionar_comercio.aspx";}
					    },
					    {
						    title: 'Cerrar',
						    event: function() { this.close();}
					    }
				    ]
	 		    });
	 		    box.open();		
	 	    }




	 	    function mensaje_limpiarRubro(p_title, p_width, p_height, p_html, p_indice) {
	 	        box = new LightFace({
	 	            title: 'Sistema TISH :: ' + p_title,
	 	            width: p_width,
	 	            height: p_height,
	 	            content: p_html,
	 	            buttons: [
	 	                 {
	 	                     title: 'Aceptar',
	 	                     event: function() { this.close(); borrarRubro(p_indice); }
	 	                 },
				        {
				            title: 'Cerrar',
				            event: function() { this.close(); }
				        }
			        ]
	 	        });
	 	        box.open();
	 	    }


	 	    function verRubros(p_title, p_width, p_height, p_html) {	 	        
	 	        box = new LightFace({
	 	            title: 'Sistema TISH :: ' + p_title,
	 	            width: p_width,
	 	            height: p_height,
	 	            content: p_html,
	 	            buttons: [
	 	                 {
	 	                     title: 'Aceptar',
	 	                     event: function() {	 	                     
	 	                     this.close();
	 	                         	 	                         
	 	                     cargarRubro(); }
	 	                 },
				        {
				            title: 'Cerrar',
				            event: function() { this.close(); }
				        }
			        ]
	 	        });
	 	        box.open();
	 	    }        


	 	    function modal_window(p_title, p_width, p_height, p_html) {	 	        
 	            box = new LightFace({
 	                title: 'Sistema TISH :: ' + p_title,
 	                width: p_width,
 	                height: p_height,
 	                content: p_html,
 	                buttons: [
				        {
				            title: 'Cerrar',
				            event: function() { this.close(); }
				        }
			        ]
 	            });
	 	        box.open();	 	        
	 	    }        
            </script>
               
               
               
               
        <!-- Seteo the resources -->
        <script src="../js/funciones.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../css/estilos_listado.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_botones.css" type="text/css" />        
        <%           
            'Seteo el css que voy a utilizar
            Dim mPath_css As String                
            mPath_css = ""
            mPath_css = System.Configuration.ConfigurationManager.AppSettings("css").ToString.Trim()
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_listado.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_genericos.css' type='text/css' />")
            Response.Write("<link rel='stylesheet' href='../css" & mPath_css.ToString.Trim & "/estilos_botones.css' type='text/css' />")
        %>                   
               
               
         
         
        <!-- ajax -->       
        <script type="text/javascript" language="javascript" src="../js/jquery1-2-6-min.js"></script>
        <script type="text/javascript" language="javascript">
        
 
          
            
            
        </script>          
               
               

        <script type="text/javascript" language="javascript">

            /* Asigno el código del rubro en base a la descripción seleecionada en el combo */
            function asignarCodigoRubro(p_index) {
                control_combo = window.document.getElementById('cbRubros' + p_index);
                index_aux = control_combo.selectedIndex;
                valor_aux = control_combo.options[index_aux].value;

                if (valor_aux == 0) {
                    window.document.getElementById('td_codigo' + p_index).innerHTML = '[Código]';
                } else {
                    window.document.getElementById('td_codigo' + p_index).innerHTML = valor_aux;
                }
            }
        
        
        
            function refresh_date() {
                var _dates;
                var _date;

                //window.document.getElementById('td_date_expiry_quota1').innerHTML = 'Actualizado fecha ...';
                //window.document.getElementById('td_date_expiry_quota2').innerHTML = 'Actualizado fecha ...';
                window.document.getElementById('td_date_expiry_presentation').innerHTML = 'Actualizado fecha ...';

                control_combo = window.document.getElementById('cmbperiod');
                index_aux = control_combo.selectedIndex;
                _dates = control_combo.options[index_aux].value;                           
                _date = _dates.split('#');

                //window.document.getElementById('td_date_expiry_quota1').innerHTML = _date[0];
                //window.document.getElementById('td_date_expiry_quota2').innerHTML = _date[1];
                window.document.getElementById('td_date_expiry_presentation').innerHTML = _date[2];
            }


            function loading(p_message) {
                var _html;
            
        	    _html = '<div id="loading" align="center">'+
        	                '<table border= "0">' +
                                '<tr>' +
                                    '<td align="center" style="width:20%;"><img id="img_load" alt="" src="../imagenes/loading.gif" /></td>' +
                                    '<td align="left" style="width:80%;">' + p_message + ' ....' +'</td>' +
                                '</tr>' +
                            '</table>' +
	                    '</div>';          
                 window.document.getElementById('div_contenedor_botones').innerHTML = _html;
            }

            
        </script>                
                
 
        
        <!-- funciones varias -->
        <script type="text/javascript">

            /* Esta Funcion ASIGNA ESTILO a un BOTON */          
            function efecto_over(boton) { boton.className = "boton_generico_gris"; }
    	    
            /* Esta Funcion ASIGNA ESTILO a un BOTON */          
  	        function efecto_out(boton){ boton.className = "boton_generico_verde"; }
        

            /* Genero el XML  */
            function get_constanciadata(ptype_pdf) {
                var id_grilla = 'gridrubros';               
                var _xml;
                var _xml_rubros;
                var _txtcontrol_employees;
                var _txtcontrol_amount;


                control_combo = window.document.getElementById('cmbperiod');
                index_aux = control_combo.selectedIndex;
                valor_aux = control_combo.options[index_aux].text;                               
                                
                        
                _xml = '<PERIODOS>\n' + 
                            '<PERIODO>\n' +
                                valor_aux + '\n' +
                            '</PERIODO>\n' +
                       '</PERIODOS>\n' +
                       '<PDF_TYPE>\n' + 
                            '<ADD_AND_VOUCHER>\n' +
                                ptype_pdf + '\n' +
                            '</ADD_AND_VOUCHER>\n' +
                       '</PDF_TYPE>\n';
                
                
                _xml_rubros = '';
                for (var i = 1; i < (window.document.getElementById(id_grilla).rows.length - 2); i++) {


                    /* verifico que el combo contenga un rubro seleccionado, de no ser así, lo salteo */
                    codigo_rubro = window.document.getElementById('td_codigo_rubro' + i).innerHTML;
                    descripcion_rubro = window.document.getElementById('td_descripcion_rubro' + i).innerHTML;

                    if (codigo_rubro != '[Código]') {

                        /* obtengo los valores ingresados*/
                        _txtcontrol_employees = window.document.getElementById('txtemployees' + i).value;
                        _txtcontrol_amount = window.document.getElementById('txtamount' + i).value;

                        /* I guard the data of the rubro */
                        _xml_rubros = _xml_rubros + '<RUBROS>\n' +
                                                        '<RUBRO_CODE>' + codigo_rubro + '</RUBRO_CODE>\n' +
                                                        '<RUBRO_DESCRIPTION>' + descripcion_rubro + '</RUBRO_DESCRIPTION>\n' +
                                                        '<RUBRO_EMPLOYEES>' + _txtcontrol_employees + '</RUBRO_EMPLOYEES>\n' +
                                                        '<RUBRO_AMOUNT>' + _txtcontrol_amount + '</RUBRO_AMOUNT>\n' +
                                                    '</RUBROS>\n';
                    }
                }                                                        
                
                _xml_rubros = '<CONTENEDOR_RUBROS>\n' + _xml_rubros + '</CONTENEDOR_RUBROS>\n';
                _xml = _xml + _xml_rubros;
                _xml = '<CONSTANCIA>\n' + _xml + '</CONSTANCIA>';
                
            return _xml;
            }

        </script>
        
        
        
        <!-- funcion doPostBack y quienes las usan -->
        <script type="text/javascript" language="javascript">
            
            function declaration_pdf(p_xml_rubros, p_xml_constanciadata) { __doPostBack(p_xml_rubros, p_xml_constanciadata); }
        
            function __doPostBack(eventTarget, eventArgument) {
                var form;                
                                
                form = document.forms["formulario"];                                
                form.__EVENTTARGET.value = eventTarget.split("$").join(":");
                form.__EVENTARGUMENT.value = eventArgument;
                form.submit();
            }


        </script>
        
        <script type="text/javascript">
        
                    /**************************************************************
                    Validar solo numeros y punto.
                    ****************************************************************/
                    function solonumeros(e){
                    key=e.keyCode || e.which;
                    teclado=String.fromCharCode(key);
                    numeros="0123456789."; 
                    especiales="8-37-38-46";
                    teclado_especial=false;
                    
                        for(var i in especiales){
                            if (key==especiales[i]){
                                teclado_especial=true;
                            }
                        }
                        if (numeros.indexOf(teclado)==-1 && !teclado_especial){
                            return false;
                        }
                    }
        
                    /**************************************************************
                    Máscara de entrada para validar la fecha.
                    ****************************************************************/
                    var patron = new Array(2,2,4)
                    var patron2 = new Array(1,3,3,3,3)
                    function mascara(d,sep,pat,nums){
                    if(d.valant != d.value){
	                    val = d.value
	                    largo = val.length
	                    val = val.split(sep)
	                    val2 = ''
	                    for(r=0;r<val.length;r++){
		                    val2 += val[r]	
	                    }
	                    if(nums){
		                    for(z=0;z<val2.length;z++){
			                    if(isNaN(val2.charAt(z))){
				                    letra = new RegExp(val2.charAt(z),"g")
				                    val2 = val2.replace(letra,"")
			                    }
		                    }
	                    }
	                    val = ''
	                    val3 = new Array()
	                    for(s=0; s<pat.length; s++){
		                    val3[s] = val2.substring(0,pat[s])
		                    val2 = val2.substr(pat[s])
	                    }
	                    for(q=0;q<val3.length; q++){
		                    if(q ==0){
			                    val = val3[q]
		                    }
		                    else{
			                    if(val3[q] != ""){
				                    val += sep + val3[q]
				                    }
		                    }
	                    }
	                    d.value = val
	                    d.valant = val
	                    }
                    }
                    </script>
                    
                    <!-- mascara CUIT -->
                    <script type="text/javascript" src="../js/jquery.js"></script>
                    <script type="text/javascript" src="../js/jquery.min.js"></script>
                    <script type="text/javascript" language="javascript"  src="../js/jquery.maskedinput.js"></script>
                    <script type="text/javascript">

                        jQuery(function($){
                            $("#txtCuit").mask("99-99999999-9"); 
                        });
            
                    </script>
                    
                   <script type="text/javascript">
                    /* formato_importe: */
                        /* Esta Funcion ASIGNA FORMATO a un IMPORTE */        
                        function formato_importe(importe) {
                            importe = importe.toString().replace(/$|,/g,'');
                            if(isNaN(importe))
                                importe = "0";
                                
                            sign = (importe == (importe = Math.abs(importe)));
                            importe = Math.floor(importe*100+0.50000000001);
                            cents = importe%100;
                            importe = Math.floor(importe/100).toString();
                            
                            if(cents<10)
                                cents = "0" + cents;
                                
                            for (var i = 0; i < Math.floor((importe.length-(1+i))/3); i++)
                                importe = importe.substring(0,importe.length-(4*i+3))+','+
                                
                            importe.substring(importe.length-(4*i+3));
                            return (((sign)?'':'-') + '$ ' + importe + '.' + cents);
                        }
                    </script>


        <title>Sistema TISH :: Presentación de declaración jurada</title>
        <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
    	        
    </head>


    <body>
    
        <!-- div container of the page -->	
           
         <form id="formulario" runat="server">

        <div id="wrap" runat="server">
            
            <!-- div header -->	            
           
		        <div id="header">			   
                    <div id="container_font">
		                <div  style="width:5%; float:left;  padding:0px 5px 0px 5px;">
                            <a href="#" title="Usar fuente menor"  class="link_font" onclick="assign_font('-');">A-</a> 
                        </div>
                        
                        <div  style="width:5%; float:left;">
		                    <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 
                        </div>		            
                        
		                <div  style="width:5%; float:left;">
		                    <a href="#" title="Usar fuente mayor" class="link_font" onclick="assign_font('+');">A+</a> 
		                </div>	            
                    </div>

                    <div id="div_header"  class="div_image_logo">                
                    <img  class="image_logo" alt="" src="../imagenes/topiz.jpg"  /><div id="div_menssage" align="center" runat="server" style="width:100%;">                               
                        <asp:Label ID="lblmessage" runat="server" Text="mensaje" Font-Bold="True" ForeColor="Red" Visible="False"></asp:Label>                               
                    </div>                                          		                            
                </div>       		    
		    	 	     
		    	 	     
			    <h1 id="logo-text">Tasa por Inspección de Seguridad e Higiene</h1>			
			    <h2 id="slogan"><asp:Label ID="lblslogan_municipalidad" runat="server" Text=""></asp:Label></h2>				
		    </div>

	  
	            <!-- div container -->	
	            <div id="content-wrap">
	  
	     	        <!-- div container page title -->
                    <div id="div_title" runat="server" style="background-color:Black;" align ="left" >
                        <strong class="titulo_page">Sistema TISH :: Presentación de Retenciones - </strong>
                        <br />
                        <asp:Label ID="lblname_user" runat="server" ForeColor="white" Font-Bold="True"></asp:Label>
                    </div>
  					  
	  
	  	            <!-- div container right -->
                    <div id="div_optiones" runat="server" class="menu_options">                
                        <div id="div_comercio" style=" padding-top:20px;">
                            <div align="center">
                                <table style="width:95%">
                                    <tr>
                                        <td  rowspan="2"><img id="user" alt="" src="../imagenes/user.png"  style=" width:50px; height:60px; border:none"/></td>
                                        <td><h1 align ="left" style="color:Black">Comercio</h1></td>
                                    </tr>
                                    <tr>                                
                                        <td>
                                            LEGAJO: <asp:Label ID="lblcomercio_user" runat="server" Font-Bold="True"></asp:Label>
                                        </td>
                                    </tr>                                                               
                                </table>
                            </div>
                        </div>
                    
                    
                        <h1 align ="left" style="color:Black">Comercio</h1>                    			
	                    <ul id="MenuNormal" runat="server"  class="sidemenu">               
                            <li><a href="webfrmdeclaracionesjuradas.aspx">Ver listado de DDJJ</a></li>
                            <li><a href="webfrmctacte.aspx">Ver cuenta corriente</a></li>				                
                            <li><a href="webfrmopciones.aspx">Datos del comercio</a></li>				                
                            <li><a href="#" onclick="javascript:change_trade();">Cambiar comercio</a></li>			                        
                        </ul>	
                        <ul id="MenuZarate" runat="server" class="sidemenu">               
                            <li><a href="webfrmaltaddjj.aspx">Confección de DDJJ</a></li>
                            <li><a href="webfrmdeclaracionesjuradas.aspx">Ver listado de DDJJ</a></li>
                            <li id="OpcionCargaArchivo" runat="server" visible="false"><a href="webfrmpresentacion_retencion.aspx">Carga de archivo de Agentes de Retención</a></li>			                
                            <li><a href="webfrmlistado_retenciones.aspx">Listado de Retenciones</a></li>						                
                            <li><a href="webfrmopciones.aspx">Datos del comercio</a></li>				                
                            <li><a href="#" onclick="javascript:change_trade();">Cambiar comercio</a></li>			                        
                        </ul>	
    	                
	                    <h1 align ="left" style="color:Black">Usuario Web</h1>				
                        <ul class="sidemenu">
	                        <li><a href="webfrmdatos_representante.aspx">Mis datos</a></li>				                
		                    <li><a href="webfrmcambiar_clave.aspx">Cambiar clave</a></li>	
		                    <li><a href="#" onclick="javascript:return logout();">Cerrar sesión</a></li>
	                    </ul>	
                    </div>
	  
	  
	  
                    <!-- div container left -->		  
	  		        <div id="main" runat="server"> 

                <!-- form -->	                 

                    <!-- Controles para utilizar la funcion doPostBack sin controles ASP -->
                    <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
                    <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
                  

                   <div id= "div_period"  runat="server">
                   
                   </div>
                   
                   
                    
            
                    <!-- Div contenedor de la tabla de movimientos -->

                    <div id="div_carga_retenciones" runat="server" visible="True" align="left">
                    <div id='div_contenedor_archivo' runat="server" 
                                    style='width:90%; float:left; padding-top:10px; padding-left: 30px;' 
                                    align='left' visible="True" >
                            <asp:Label ID="lblTextoRetenciones" runat="server" Font-Bold="True" Font-Size="Medium" 
                                Text="Carga de archivo Agentes de Retención. (.TXT)"></asp:Label>
                            <br />
                            <asp:Label ID="msg_error_upload_archivo" runat="server" Text=""></asp:Label>
                            <br />
                            <br />
                            <asp:FileUpload ID="upload_archivo" runat="server" Width="511px" 
                                Height="25px" />
                        </div>
                    </div>    
                        
                        <div id='div_contenedor_botones' 
                            style='width:90%; float:left; padding-top:30px; padding-left: 100px; height: 81px;' 
                            align='center'>
                            
                      
                            <div id='div_boton_declaration' style='width:98%; float:left; height: 40px;'; 
                                align='center'>
                                <asp:Button ID="btnCargar" runat="server" Text="Cargar Archivo" 
                                    style='Width:150px; height:40px;' CssClass="boton"/>
                            </div>
                            
                        
                    

                        </div> 

                </div>    
			
  		</div> 	
                        
            <!-- div container footer -->				  
	  		<div id="footer">
		
		        <div id="div_pie_municipalidad" style="width:70%; height:83%;  float:left;   padding-top:5px; padding-left: 5px; ">
		            Municipalidad de
		                <strong> 
		                <asp:Label ID="lblnombre_municipalidad" runat="server" Text="NOMBRE_MUNICIPIO"></asp:Label> 
                    </strong> 
                    
				    <br />
				    Teléfono: 
				    <strong> 
                        <asp:Label ID="lbltelefono_municipalidad" runat="server" Text="(000) 000000 "></asp:Label> 
                    </strong> 
                    | Email: 
				    <strong> 
                        <asp:Label ID="lblemail_municipalidad" runat="server" Text="info@hola.com.ar"></asp:Label> 
                    </strong>                 
			    </div>
		   		   
		        <div id="div_pie_grupomdq"  class="footer_grupomdq">
				    Desarrollado por: <strong><a href="http://www.grupomdq.com">&copy;Grupo MDQ S.A</a></strong>  
				    <br />
				    Válido: <a href="http://validator.w3.org/check?uri=referer">XHTML</a> | 
				    <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a> |
                    <asp:Label ID="lblversion" runat="server" Text=""></asp:Label>
		        </div>
			
		    </div>	
		
		
            
		         
		
		
		
            
		         
		
		
                 </form>		       
		
		
            
		         
		
		

	
	
	
         		<!-- content-wrap ends here -->
		</div>	
	
	
    </body>

</html>