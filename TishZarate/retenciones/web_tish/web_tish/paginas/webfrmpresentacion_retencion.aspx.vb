﻿Public Partial Class webfrmpresentacion_retencion
    Inherits System.Web.UI.Page


#Region "Variables"
    Private mWS As ws_tish.Service1
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim mPeriod As String

        If Not (IsPostBack) Then

            Call Me.SloganMunicipalidad()
            Call Me.ValidarSession()
            Call Me.CargarPie()
            lblname_user.Text = Session("NombreUsuario-Cuit").ToString.Trim
            lblcomercio_user.Text = Session("NROCOMERCIO")
            'Si es Zarate que oculte/muestre el nombre de fantasía, el contribuyente y el menu.

            If System.Configuration.ConfigurationManager.AppSettings("zarate").ToString.Trim() = "True" Then
                MenuZarate.Visible = True
                MenuNormal.Visible = False
                'Carga o no si es Agente de retencion, la opcion para cargar el archivo.
                If Session("Agente_retencion").ToString.Trim = "S" Then
                    OpcionCargaArchivo.Visible = True
                Else
                    Response.Redirect("webfrmlogin.aspx", False)
                End If
            Else
                MenuZarate.Visible = False
                MenuNormal.Visible = True
                OpcionCargaArchivo.Visible = False

            End If

            Call Me.LoadComboPeriods()


        End If
    End Sub

    Private Sub SloganMunicipalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema TISH."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema TISH."
        End If

    End Sub

    Private Function ValidarSession() As Boolean
        Dim mLogin_OK As Boolean


        'Bandera Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'Nombre Usuario
        If (mLogin_OK) Then
            If (Session("contribuyente_nombre") IsNot Nothing) Then
                If (Session("contribuyente_nombre").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If

        Me.lblcomercio_user.Text = Session("NROCOMERCIO")

        If (mLogin_OK) Then

        Else
            Response.Redirect("webfrmlogin.aspx", False)
        End If

    End Function

    Private Sub CargarPie()

        'Nombre Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telefono Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            Me.lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            Me.lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If


        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub

    'LoadComboPeriods
    'This function loads the combo whit the periods
    Private Sub LoadComboPeriods()
        Dim dsDataPeriods As DataSet
        Dim mXML_AUX As String
        Dim mHTML As String


        Try

            'I obtain the periods
            Me.mWS = New ws_tish.Service1
            mXML_AUX = Me.mWS.ObtainDateOfExpiry()
            Me.mWS = Nothing



            'I read the XML
            dsDataPeriods = New DataSet
            dsDataPeriods = clsTools.ObtainDataXML(mXML_AUX.ToString.Trim)
            Session("ROWS_PERIODS") = dsDataPeriods.Tables("ROWS_PERIODS").Copy

            'If the dsListadoDDJJ is not empty I assign information
            If (clsTools.HasData(dsDataPeriods)) Then
                Me.div_period.InnerHtml = Me.CreateHTMLPeriod(dsDataPeriods.Tables("ROWS_PERIODS"))
            End If


        Catch ex As Exception
            'Si no esta en fecha de presentación muestra cartel y oculta archivo de presentación.
            Me.lblmessage.Visible = True
            Me.lblmessage.Text = "ATENCIÓN: A la fecha de hoy no es posible presentar la DD.JJ. Debe presentarla en la fecha correspondiente al período de presentación."
            Me.btnCargar.Visible = False
            'div_contenedor_archivo.Visible = False
        Finally
            mWS = Nothing
        End Try

    End Sub

    Private Function CreateHTMLPeriod(ByVal dtData As DataTable) As String
        Dim mHTML As String
        Dim mHTML_ComboItems As String
        Dim mValue As String


        Try

            mHTML_ComboItems = ""
            mValue = ""
            If Not (CBool(Session("Rectificativa"))) Then
                If (dtData IsNot Nothing) Then
                    For i = 0 To dtData.Rows.Count - 1

                        mValue = dtData.Rows(i).Item("FV_PRESENTACION_AGRET").ToString.Trim
                        If (i = 0) Then
                            mHTML_ComboItems = mHTML_ComboItems & "<option value='" & mValue.Trim & "' selected='selected'>" & dtData.Rows(i).Item("FV_PRESENTACION_AGRET").ToString.Trim & "</option>"
                        End If

                        mValue = dtData.Rows(i).Item("FV_CUOTA1").ToString.Trim & "#" & dtData.Rows(i).Item("FV_CUOTA2").ToString.Trim & "#" & dtData.Rows(i).Item("FV_PRESENTACION").ToString.Trim
                        If (i = 0) Then
                            mHTML_ComboItems = mHTML_ComboItems & "<option value='" & mValue.Trim & "' selected='selected'>" & dtData.Rows(i).Item("ANIO").ToString.Trim & " - " & dtData.Rows(i).Item("CUOTA").ToString.Trim & "</option>"
                            'Guardo en variable de session el año y cuota para el archivo upload
                            Session("ANIO") = dtData.Rows(i).Item("ANIO").ToString.Trim
                            Session("CUOTA") = dtData.Rows(i).Item("CUOTA").ToString.Trim
                        Else
                            mHTML_ComboItems = mHTML_ComboItems & "<option value='" & mValue.Trim & "'>" & dtData.Rows(i).Item("ANIO").ToString.Trim & " - " & dtData.Rows(i).Item("CUOTA").ToString.Trim & "</option>"
                            Session("ANIO") = dtData.Rows(i).Item("ANIO").ToString.Trim
                            Session("CUOTA") = dtData.Rows(i).Item("CUOTA").ToString.Trim
                        End If
                    Next
                End If


            End If



            mHTML = ""
            mHTML = mHTML & "<table id='table_period' align='center'  border='0' style='width:90%; border:1px solid #C0C0C0;'>"
            mHTML = mHTML & "   <tr>"
            If Not (CBool(Session("Rectificativa"))) Then
                mHTML = mHTML & " <td colspan='4' style='color:Black; font-weight:bold;' align='center'>Seleccione el período</td>"
            Else
                mHTML = mHTML & " <td colspan='4' style='color:Black; font-weight:bold;' align='center'>Rectificativa :: Los datos del periodo son puramente informativos.</td>"
            End If
            mHTML = mHTML & "   </tr>"
            'mHTML = mHTML & "   <tr>"
            'mHTML = mHTML & "       <td id='td_date_presentation' colspan='4' style='color:Black; font-weight:bold;' align='center'>Fecha presentación: " & Format(Now, "dd/MM/yyyy") & "</td>"
            'mHTML = mHTML & "   </tr>"
            mHTML = mHTML & "   <tr><td colspan='4' class='linea'><hr /></td></tr>"
            mHTML = mHTML & "   <tr>"
            'mHTML = mHTML & "       <td style='width:60%'>Fec. Vencimiento presentación de Archivo de Retenciones:</td>"
            'mHTML = mHTML & "       <td style='width:60%' align='left'>"
            'mHTML = mHTML & "           <select id = 'cmbperiod' name='cmbperiod' style='width:40%;' onchange=""javascript:refresh_date();"">" & mHTML_ComboItems.Trim & "</select>"
            'mHTML = mHTML & "       </td>"
            mHTML = mHTML & "       <td style='width:45%' align='right'>Fecha de Vencimiento de presentación de Archivo de Retenciones (AGRET):</td>"
            mHTML = mHTML & "       <td id='td_date_expiry_presentation' style='width:10%'>" & dtData.Rows(0).Item("FV_PRESENTACION_AGRET").ToString.Trim & "</td>"
            mHTML = mHTML & "   </tr>"
            mHTML = mHTML & "   <tr><td colspan='4' class='linea'><hr /></td></tr>"
            mHTML = mHTML & "   <tr><td colspan='4' class='linea'><hr /></td></tr>"
            mHTML = mHTML & "</table>"

            If dtData.Rows(0).Item("FV_PRESENTACION_AGRET").ToString.Trim = "Vencida" Then
                Me.upload_archivo.Enabled = False
                Me.btnCargar.Enabled = False
            End If

        Catch ex As Exception
            mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                        "<p id='lblmessenger' style='color: Red; font-weight: inherit;'> " & _
                            "Error al intentar obtener los datos." & _
                        "</p>" & _
                    "</div>"
        End Try

        Return mHTML.Trim
    End Function

    Protected Sub btnCargar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCargar.Click


        Session("Nombre_Archivo") = "NO"
        If upload_archivo.HasFile Then
            Dim fileExt As String
            fileExt = System.IO.Path.GetExtension(upload_archivo.FileName)
            If (fileExt = ".txt") Or (fileExt = ".TXT") Then

                If archivo() = True Then
                    msg_error_upload_archivo.Text = "El archivo se cargó correctamente."
                    Response.Redirect("comprobantes/webfrmcomprobanteretencion.aspx", False)
                ElseIf archivo() = False Then
                    msg_error_upload_archivo.Text = "ERROR al procesar el archivo de texto."
                End If

            Else
                msg_error_upload_archivo.Text = "Formato no válido, sólo archivos de texto. (.TXT)"
            End If
        Else
            msg_error_upload_archivo.Text = "Archivo no especificado."
        End If
    End Sub


    Private Function archivo()

        'Renombrado de archivo
        Dim anio, cuota, nombre, comercio As String


        If Session("CUOTA").ToString.Length = 1 Then
            cuota = "0" + Session("CUOTA")
        Else
            cuota = Session("CUOTA")
        End If

        comercio = Me.lblcomercio_user.Text
        For i = comercio.Length To 9
            comercio = "0" + comercio
        Next

        anio = Session("ANIO")
        nombre = "RET" + anio.ToString.Trim + cuota.ToString.Trim + comercio.ToString.Trim

        'Guarda archivo en Path y con nombre establecido.
        Try
            upload_archivo.SaveAs(System.Configuration.ConfigurationManager.AppSettings("Path_upload_archivo") + nombre.ToString.Trim + ".txt")
            Session("Nombre_Archivo") = nombre + ".txt"
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
End Class