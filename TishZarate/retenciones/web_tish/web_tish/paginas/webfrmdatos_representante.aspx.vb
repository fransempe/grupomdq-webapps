﻿Option Explicit On
Option Strict On


Partial Public Class webfrmdatos_representante
    Inherits System.Web.UI.Page

#Region "Variables"
    Private mWS As ws_tish.Service1
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not (IsPostBack) Then

            Call Me.SloganMuniicpalidad()
            Call Me.ValidateSession()
            Call Me.LoadFooter()
            lblname_user.Text = Session("NombreUsuario-Cuit").ToString.Trim
            'Si es Zarate que oculte/muestre el nombre de fantasía, el contribuyente y el menu.
            If System.Configuration.ConfigurationManager.AppSettings("zarate").ToString.Trim() = "True" Then
                MenuZarate.Visible = True
                MenuNormal.Visible = False
                'Carga o no si es Agente de retencion, la opcion para cargar el archivo.
                If Session("Agente_retencion").ToString.Trim = "S" Then
                    OpcionCargaArchivo.Visible = True
                End If
            Else
                MenuZarate.Visible = False
                MenuNormal.Visible = True
                OpcionCargaArchivo.Visible = False
            End If

            'Cargo la información del usuario Web
            Call Me.LoadDataManager()

        End If


    End Sub


#Region "functions"


    Private Sub SloganMuniicpalidad()

        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblslogan_municipalidad.Text = "Municipalidad de " & Session("municipalidad_nombre").ToString.Trim & " - Sistema TISH."
        Else
            Me.lblslogan_municipalidad.Text = "Sistema TISH."
        End If

    End Sub


    'ValidateSession:
    'This function validate the data of session
    Private Function ValidateSession() As Boolean
        Dim mLogin_OK As Boolean


        'flag Login OK
        mLogin_OK = False
        If (Session("login") IsNot Nothing) Then
            If (CBool(Session("login"))) Then
                mLogin_OK = True
            End If
        End If



        'name user
        If (mLogin_OK) Then
            If (Session("contribuyente_nombre") IsNot Nothing) Then
                If (Session("contribuyente_nombre").ToString.Trim <> "") Then
                    mLogin_OK = True
                End If
            End If
        End If




        If (mLogin_OK) Then
            'lblcontribuyente_nombre.Text = Session("data_comercio").rows(0).item("CONTRIBUYENTE_CUIT").ToString.Trim()
        Else
            Response.Redirect("webfrmlogin.aspx", False)
        End If

    End Function


    'LoadFooter:
    'This function loads the information of the municipality in the foot of the page
    Private Sub LoadFooter()

        'name Municipio
        If (Session("municipalidad_nombre") IsNot Nothing) Then
            Me.lblnombre_municipalidad.Text = Session("municipalidad_nombre").ToString.Trim
        End If


        'Telephone Municipio
        If (Session("municipalidad_telefono") IsNot Nothing) Then
            Me.lbltelefono_municipalidad.Text = Session("municipalidad_telefono").ToString.Trim
        End If


        'Email Municipio
        If (Session("municipalidad_mail") IsNot Nothing) Then
            Me.lblemail_municipalidad.Text = Session("municipalidad_mail").ToString.Trim
        End If


        Me.lblversion.Text = "Versión " & clsTools.mVersion.ToString.Trim
    End Sub


    'LoadDataManager
    'This function loads the data of the manager
    Private Sub LoadDataManager()
        Dim dsDataManager As DataSet


        Try
            dsDataManager = New DataSet
            mWS = New ws_tish.Service1
            dsDataManager = mWS.ObtainDataManager(Session("cuit_manager").ToString.Trim)
            mWS = Nothing



            
            Session("data_manager") = dsDataManager.Tables(0)


            'I validate that the this loaded datatable and I assign the data
            If (dsDataManager IsNot Nothing) Then
                lblCUIT.Text = dsDataManager.Tables("Manager").Rows(0).Item("CUIT").ToString.Trim
                lblreason_social.Text = dsDataManager.Tables("Manager").Rows(0).Item("RAZON_SOCIAL").ToString.Trim
                lblcontact.Text = dsDataManager.Tables("Manager").Rows(0).Item("DIRECCION").ToString.Trim
                lbladdress.Text = dsDataManager.Tables("Manager").Rows(0).Item("CONTACTO").ToString.Trim
                lbltelephono.Text = dsDataManager.Tables("Manager").Rows(0).Item("TELEFONO").ToString.Trim
                lblemail.Text = dsDataManager.Tables("Manager").Rows(0).Item("EMAIL").ToString.Trim

                'Type Key
                If (dsDataManager.Tables("Manager").Rows(0).Item("TIPO_CLAVE").ToString.Trim = "P") Then
                    lbltype_key.Text = "PROPIA"
                Else
                    lbltype_key.Text = "AUTOMATICA"
                End If



                'Selected manager
                lblname_manager.Text = "Usuario Web: " & dsDataManager.Tables(0).Rows(0).Item("RAZON_SOCIAL").ToString.Trim.ToUpper
            End If


        Catch ex As Exception
        Finally

        End Try

    End Sub
#End Region

End Class