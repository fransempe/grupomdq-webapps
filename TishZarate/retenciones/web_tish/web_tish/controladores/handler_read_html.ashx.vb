﻿Imports System.Web
Imports System.Web.Services
Imports System.IO.File

Public Class handler_read_html
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Dim mHTML As String        
        Dim mResponse_HTML As String
        Dim mPath As String


        Try

            context.Response.ContentType = "text/plain"

            'I obtain the variables
            mHTML = context.Request.Form("html")


            mResponse_HTML = ""
            mPath = System.Configuration.ConfigurationManager.AppSettings("path_modal_html").ToString.Trim & "\" & mHTML.Trim & ".html"
            If (My.Computer.FileSystem.FileExists(mPath.ToString.Trim)) Then
                mResponse_HTML = My.Computer.FileSystem.ReadAllText(mPath.Trim)
            End If


        Catch ex As Exception
            mResponse_HTML = ""
        End Try

        context.Response.Write(mResponse_HTML.ToString.Trim)

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class