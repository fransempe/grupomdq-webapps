﻿Option Explicit On
Option Strict On

Imports System.Web
Imports System.Web.Services

Public Class handler_login
    Implements System.Web.IHttpHandler

#Region "Variables"

    Private mWS As ws_tish.Service1

#End Region


    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim mDataUser As String
        Dim mDataKey As String
        Dim mLogin_OK As Boolean
        Dim mResponse As String


        Try

            context.Response.ContentType = "text/plain"

            'I obtain the variables
            mDataUser = context.Request.Form("user")
            mDataKey = context.Request.Form("key")

            'check the login
            mLogin_OK = False
            mLogin_OK = Login(mDataUser.ToString.Trim, mDataKey.ToString.Trim)


            'believe the response
            mResponse = ""
            If (mLogin_OK) Then
                mResponse = "OK"
            Else
                mResponse = "MAL"
            End If


        Catch ex As Exception
            mResponse = "MAL"
        End Try

        context.Response.Write(mResponse.ToString.Trim)
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property


    Private Function Login(ByVal pUser As String, ByVal pKey As String) As Boolean
        Dim mDataLogin As String
        Dim mDataLogin_AUX() As String
        Dim mLogin_OK As Boolean


        Try

            'realize the login
            mLogin_OK = False
            mDataLogin = ""
            mWS = New ws_tish.Service1
            mDataLogin = mWS.Login(pUser.ToString.Trim, pKey.ToString.Trim)
            mWS = Nothing


            'obtain the data of the login
            mDataLogin_AUX = mDataLogin.ToString.Trim.Split(CChar("|"))


            'believe the response
            mLogin_OK = CBool((mDataLogin_AUX(0).ToString.Trim <> ""))


        Catch ex As Exception
            mLogin_OK = False
            mWS = Nothing
        End Try


        Return mLogin_OK
    End Function

End Class