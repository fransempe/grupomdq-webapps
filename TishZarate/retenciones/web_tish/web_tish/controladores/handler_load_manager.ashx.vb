﻿Imports System.Web
Imports System.Web.Services

Public Class handler_load_manager
    Implements System.Web.IHttpHandler

#Region "Variables"
    Private mWS As ws_tish.Service1
#End Region

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim mAction As String
        Dim mCuit As String
        Dim mHTML As String
        Dim mResponse As String


        Try

            context.Response.ContentType = "text/plain"

            'I obtain the variables
            mAction = context.Request.Form("action")
            mCuit = context.Request.Form("cuit")


            'check the login
            mHTML = ""
            Select Case mAction.ToString.Trim
                Case "clear"
                    mHTML = Me.CreateHTMLManager(mCuit.ToString.Trim)
                Case "down"
                    mHTML = Me.CreateHTMLManagerDown(mCuit.ToString.Trim)
                Case "activate"
                    mHTML = Me.CreateHTMLManagerActivate(mCuit.ToString.Trim)
            End Select


            'create the response
            mResponse = ""
            mResponse = mHTML.ToString.Trim


        Catch ex As Exception
            mResponse = "<span id='lblmessenger' style='color: Red; font-weight: bold;'>" & _
                            "Error al intentar recuperar los datos." & _
                        "</span>"
        End Try

        context.Response.Write(mResponse.ToString.Trim)
    End Sub

    Private Function CreateHTMLManager(ByVal pCuit As String) As String
        Dim dsDataManager As DataSet
        Dim mReason_Social As String
        Dim mContac As String
        Dim mAddress As String
        Dim mTelephone As String
        Dim mEmail As String
        Dim mHTML As String


        Try

            mHTML = ""


            'I obtain the data of the trade
            dsDataManager = Me.ObtainDataManager(pCuit)


            'I assign the data
            'lblmessenger.Visible = False
            If (clsTools.HasData(dsDataManager)) Then

                If (dsDataManager.Tables("Manager").Rows(0).Item("FECHA_BAJA").ToString.Trim <> "") Then

                    mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:90%;'>" & _
                                "<p id='lblmessenger'>" & _
                                    "El usuario Web seleccionado se encuentra en estado de baja. </br>" & _
                                "</p>" & _
                                "<p id='lblmessenger1'>" & _
                                    "Para realizar esta operación usted deberá primero dirigirse a la opción  <a href='webfrmactivar_representante.aspx'>[Activar usuario Web]</a>" & _
                                "</p>" & _
                            "</div>"
                Else
                    mReason_Social = dsDataManager.Tables("Manager").Rows(0).Item("RAZON_SOCIAL").ToString.Trim
                    mContac = dsDataManager.Tables("Manager").Rows(0).Item("CONTACTO").ToString.Trim
                    mAddress = dsDataManager.Tables("Manager").Rows(0).Item("DIRECCION").ToString.Trim
                    mTelephone = dsDataManager.Tables("Manager").Rows(0).Item("TELEFONO").ToString.Trim
                    mEmail = dsDataManager.Tables("Manager").Rows(0).Item("EMAIL").ToString.Trim

                    mHTML = "<strong>Datos del usuario Web:</strong> " & _
                            "<hr class='linea'/> " & _
                            "<table border='0' style='border-bottom-color:Green; width:100%;' visible='false'; > " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>Razón social:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mReason_Social.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>Responsable:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mContac.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>Dirección:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mAddress.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>Teléfono:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mTelephone.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>E-mail:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mEmail.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                            "</table>"

                End If
            Else
                mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                            "<p id='lblmessenger'> " & _
                                "Usuario Web inexistente." & _
                            "</p>" & _
                        "</div>"

            End If


        Catch ex As Exception
            mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                        "<p id='lblmessenger'> " & _
                            "Error al intentar obtener los datos." & _
                        "</p>" & _
                    "</div>"
        End Try


        Return mHTML.ToString.Trim
    End Function


    Private Function CreateHTMLManagerDown(ByVal pCuit As String) As String
        Dim dsDataManager As DataSet
        Dim mReason_Social As String
        Dim mContac As String
        Dim mAddress As String
        Dim mTelephone As String
        Dim mEmail As String
        Dim mHTML As String


        Try

            mHTML = ""


            'I obtain the data of the trade
            dsDataManager = Me.ObtainDataManager(pCuit)


            'I assign the data
            'lblmessenger.Visible = False
            If (clsTools.HasData(dsDataManager)) Then

                If (dsDataManager.Tables("Manager").Rows(0).Item("FECHA_BAJA").ToString.Trim <> "") Then


                    mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                                "<p id='lblmessenger'> " & _
                                    "El usuario Web ingresado ya se encuentra dado de baja en el sistema." & _
                                "</p>" & _
                            "</div>"
                Else
                    mReason_Social = dsDataManager.Tables("Manager").Rows(0).Item("RAZON_SOCIAL").ToString.Trim
                    mContac = dsDataManager.Tables("Manager").Rows(0).Item("CONTACTO").ToString.Trim
                    mAddress = dsDataManager.Tables("Manager").Rows(0).Item("DIRECCION").ToString.Trim
                    mTelephone = dsDataManager.Tables("Manager").Rows(0).Item("TELEFONO").ToString.Trim
                    mEmail = dsDataManager.Tables("Manager").Rows(0).Item("EMAIL").ToString.Trim

                    mHTML = "<strong>Datos del usuario Web:</strong> " & _
                            "<hr class='linea'/> " & _
                            "<table border='0' style='border-bottom-color:Green; width:100%;' visible='false'; > " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>Razón social:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mReason_Social.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>Responsable:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mContac.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>Dirección:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mAddress.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>Teléfono:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mTelephone.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>E-mail:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mEmail.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                    " <td colspan='3' align='center'><strong>Motivo de baja:</strong></td> " & _
                                " </tr> " & _
                                " <tr> " & _
                                    " <td colspan='3' align='center'> " & _
                                         " <textarea name='txtmotive' id='txtmotive' class='textbox' onfocus='javascript:event_focus(this);' onblur='javascript:event_focus(this);' style='height: 50px; width: 200px;'></textarea> " & _
                                    " </td> " & _
                                " </tr>  " & _
                            "</table>"

                End If
            Else
                mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                            "<p id='lblmessenger'> " & _
                                "Usuario Web inexistente." & _
                            "</p>" & _
                        "</div>"
            End If


        Catch ex As Exception
            mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                        "<p id='lblmessenger'> " & _
                            "Error al intentar obtener los datos." & _
                        "</p>" & _
                    "</div>"
        End Try


        Return mHTML.ToString.Trim
    End Function


    Private Function CreateHTMLManagerActivate(ByVal pCuit As String) As String
        Dim dsDataManager As DataSet
        Dim mReason_Social As String
        Dim mContac As String
        Dim mAddress As String
        Dim mTelephone As String
        Dim mEmail As String
        Dim mHTML As String


        Try

            mHTML = ""


            'I obtain the data of the trade
            dsDataManager = Me.ObtainDataManager(pCuit)


            'I assign the data
            If (clsTools.HasData(dsDataManager)) Then

                If (dsDataManager.Tables("Manager").Rows(0).Item("FECHA_BAJA").ToString.Trim = "") Then

                    mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                                "<p id='lblmessenger'> " & _
                                    "El usuario Web ingresado ya se encuentra activo en el sistema." & _
                                "</p>" & _
                            "</div>"
                Else
                    mReason_Social = dsDataManager.Tables("Manager").Rows(0).Item("RAZON_SOCIAL").ToString.Trim
                    mContac = dsDataManager.Tables("Manager").Rows(0).Item("CONTACTO").ToString.Trim
                    mAddress = dsDataManager.Tables("Manager").Rows(0).Item("DIRECCION").ToString.Trim
                    mTelephone = dsDataManager.Tables("Manager").Rows(0).Item("TELEFONO").ToString.Trim
                    mEmail = dsDataManager.Tables("Manager").Rows(0).Item("EMAIL").ToString.Trim


                    mHTML = "<strong>Datos del usuario Web:</strong> " & _
                            "<hr class='linea'/> " & _
                            "<table border='0' style='border-bottom-color:Green; width:100%;' visible='false'; > " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>Razón social:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mReason_Social.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>Responsable:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mContac.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>Dirección:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mAddress.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>Teléfono:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mTelephone.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>E-mail:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mEmail.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                            "</table>"

                End If
            Else
                mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                           "<p id='lblmessenger'> " & _
                               "Usuario Web inexistente." & _
                           "</p>" & _
                       "</div>"
            End If


        Catch ex As Exception
            mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                        "<p id='lblmessenger'> " & _
                            "Error al intentar obtener los datos." & _
                        "</p>" & _
                    "</div>"
        End Try


        Return mHTML.ToString.Trim
    End Function


    Private Function ObtainDataManager(ByVal pCuit As String) As DataSet
        Dim dsDataManager_AUX As DataSet

        Try

            'I obtain the data of the trade
            dsDataManager_AUX = New DataSet
            Me.mWS = New ws_tish.Service1
            dsDataManager_AUX = Me.mWS.ObtainDataManager(pCuit.ToString.Trim)
            Me.mWS = Nothing


        Catch ex As Exception
            dsDataManager_AUX = Nothing
        End Try

        Return dsDataManager_AUX
    End Function


    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class