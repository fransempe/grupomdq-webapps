﻿Imports System.Web
Imports System.Web.Services

Public Class handler_period_edit
    Implements System.Web.IHttpHandler

#Region "Variables"
    Private mWS As ws_tish.Service1
#End Region

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim mYear As Integer
        Dim mQuota As Integer
        Dim mQuotaExpirationDate1 As String
        Dim mQuotaExpirationDate2 As String
        Dim mPresentationExpirationDate As String




        Dim mHTML As String
        Dim mResponse As String


        Try

            context.Response.ContentType = "text/plain"

            'I obtain the variables
            mYear = CInt(context.Request.Form("year"))
            mQuota = CInt(context.Request.Form("quota"))
            mQuotaExpirationDate1 = context.Request.Form("date_quota1").Trim
            mQuotaExpirationDate2 = context.Request.Form("date_quota2").Trim
            mPresentationExpirationDate = context.Request.Form("date_presentation").Trim



            mHTML = ""
            mHTML = Me.Edit(mYear, mQuota, mQuotaExpirationDate1.Trim, mQuotaExpirationDate2.Trim, mPresentationExpirationDate.Trim)


            mResponse = mHTML.Trim
        Catch ex As Exception
            mResponse = "<span id='lblmessenger' style='color: Red; font-weight: bold;'>" & _
                            "Error al intentar recuperar los datos." & _
                        "</span>"
        End Try

        context.Response.Write(mResponse.ToString.Trim)

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property


    Private Function Edit(ByVal pYear As Integer, ByVal pQuota As Integer, _
                          ByVal pQuotaExpirationDate1 As String, ByVal pQuotaExpirationDate2 As String, _
                          ByVal pPresentationExpirationDate As String) As String
        Dim mEdit_OK As String

        Try

            'edit the date expiry
            mEdit_OK = ""
            Me.mWS = New ws_tish.Service1
            mEdit_OK = Me.mWS.EditDateExpiry(pYear, pQuota, pQuotaExpirationDate1.Trim, pQuotaExpirationDate2.Trim, pPresentationExpirationDate.Trim)


        Catch ex As Exception
            mEdit_OK = ""
        Finally
            mWS = Nothing
        End Try


        Return mEdit_OK.Trim
    End Function

End Class