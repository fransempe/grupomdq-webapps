﻿Imports System.Web
Imports System.Web.Services

Public Class handler_getRetenciones_list
    Implements System.Web.IHttpHandler

#Region "Variables"
    Private mWS As ws_tish.Service1
#End Region

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim strAccion As String
        Dim lngNroComercio As Long
        Dim strPeriodo As String
        Dim strAño As String
        Dim strCuota As String
        Dim strHTML As String
        Dim strResponse As String


        Try
            context.Response.ContentType = "text/plain"
            strAccion = context.Request.Form("action").Trim
            lngNroComercio = CLng(context.Request.Form("comercio"))
            strCuota = context.Request.Form("cuota")
            strAño = context.Request.Form("anio")

            If (strAccion.Trim() = "exist") Then
                strPeriodo = context.Request.Form("period").Trim
            End If


            strHTML = ""
            Select Case strAccion.Trim()
                Case "get"
                    strHTML = Me.getListadoRetenciones(lngNroComercio, strAño.Trim, strCuota.Trim)
            End Select



            strResponse = strHTML.Trim()
        Catch ex As Exception
            strResponse = "<span id='lblmessenger' style='color: Red; font-weight: bold;'>" & _
                                "Error al intentar recuperar los datos." & _
                          "</span>"
        End Try

        context.Response.Write(strResponse.Trim)
    End Sub


    Private Function getListadoRetenciones(ByVal p_lngNumberComercio As Long, ByVal pYear As String, ByVal pQuota As String) As String
        Dim com_dj_r As List(Of entidades.com_dj_r)
        Dim strXML As String = ""
        Dim strHTML As String = ""


        Try
            ' Obtengo un listado de las declaraciones juradas de un comercio
            mWS = New ws_tish.Service1
            strXML = mWS.obtainlist_Retenciones(p_lngNumberComercio, pYear, pQuota)
            mWS = Nothing


            com_dj_r = New List(Of entidades.com_dj_r)()
            com_dj_r = entidades.serializacion.deserializarXML(Of List(Of entidades.com_dj_r))(strXML.Trim())


            If (com_dj_r.Count <> 0) Then
                strHTML = Me.crearHTML(com_dj_r)
            Else
                strHTML = Me.createHtmlSoloBoton()
            End If

        Catch ex As Exception
        Finally
            com_dj_r = Nothing
            mWS = Nothing
        End Try


        Return strHTML.Trim()
    End Function


    Private Function crearHTML(ByVal com_dj_r As List(Of entidades.com_dj_r)) As String
        Dim mHTML_Table_Header As String = ""
        Dim mHTML_Table_Body As String = ""
        Dim mHTML As String = ""

        Try

            'Cabecera de la tabla
            mHTML_Table_Header = ""
            mHTML_Table_Header = mHTML_Table_Header & "<thead>"
            mHTML_Table_Header = mHTML_Table_Header & "     <tr>"
            mHTML_Table_Header = mHTML_Table_Header & "         <th><h6 align='center'>Año</h6></th>"
            mHTML_Table_Header = mHTML_Table_Header & "         <th><h6 align='center'>Período</h6></th>"
            mHTML_Table_Header = mHTML_Table_Header & "         <th><h6 align='center'>Descripción</h6></th>"
            mHTML_Table_Header = mHTML_Table_Header & "         <th><h6 align='center'>Cuit</h6></th>"
            mHTML_Table_Header = mHTML_Table_Header & "         <th><h6 align='right'>Fecha</h6></th>"
            mHTML_Table_Header = mHTML_Table_Header & "         <th><h6 align='right'>Monto Retenido</h6></th>"
            mHTML_Table_Header = mHTML_Table_Header & "         <th><h6 align='right'>Monto Facturado</h6></th>"
            mHTML_Table_Header = mHTML_Table_Header & "         <th><h6 align='right'>NºComprob.</h6></th>"
            mHTML_Table_Header = mHTML_Table_Header & "     </tr>"
            mHTML_Table_Header = mHTML_Table_Header & "</thead>"


            'Creo los renglones de la tabla            
            For Each com_dj_r_aux As entidades.com_dj_r In com_dj_r
                mHTML_Table_Body = mHTML_Table_Body & "<tr>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='center'>" & com_dj_r_aux.anio & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='center'>" & com_dj_r_aux.cuota & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='center'>" & com_dj_r_aux.descripcion & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='center'>" & com_dj_r_aux.cuit & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='center'>" & com_dj_r_aux.fecha_ret & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='right'>" & Format(com_dj_r_aux.importe_retenido, "N2") & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='right'>" & Format(com_dj_r_aux.importe_facturado, "N2") & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "<td align='right'>" & com_dj_r_aux.nroComprob & "</td>"
                mHTML_Table_Body = mHTML_Table_Body & "</tr>"
            Next



            mHTML = "<table id='gridretenciones' cellpadding='0' cellspacing='0' border='0' id='table_retenciones' class='table_retenciones' onmouseover=""javascript:efecto_grilla('gridretenciones');"">" & _
                        mHTML_Table_Header.Trim & vbCr & _
                        "<tbody>" & mHTML_Table_Body.Trim & "</tbody>" & _
                    "</table>"



        Catch ex As Exception
            mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                        "<p id='lblmessenger' style='color: Red; font-weight: inherit;'> " & _
                            "Error al intentar obtener los datos." & _
                        "</p>" & _
                    "</div>"
        End Try

        Return mHTML.Trim()
    End Function



    Private Function createHtmlSoloBoton() As String
        Return " <div id='div_contenedor_botones' style='width:90%; padding-top:10px;' align='center'> " & _
                    "<p><strong class='titulo_3'>No existen Retenciones presentadas en este período.</strong></p>" & _
               " </div> "
    End Function

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property


End Class