﻿Option Explicit On
Option Strict On

Imports System.Web
Imports System.Web.Services


Public Class handler_detail_ddjj
    Implements System.Web.IHttpHandler

#Region "Variables"
    Private mWS As ws_tish.Service1
#End Region

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim mYear As String
        Dim mQuota As String
        Dim mType As String
        Dim mTradeNumber As Integer

        Dim mHTML As String
        Dim mResponse As String


        Try

            context.Response.ContentType = "text/plain"

            'I obtain the variables
            mYear = context.Request.Form("year")
            mQuota = context.Request.Form("quota")
            mType = context.Request.Form("type")
            mTradeNumber = CInt(context.Request.Form("trade_number"))


            'check the login
            mHTML = ""
            mHTML = Me.CreateHTML(CInt(mYear), CInt(mQuota), mType.Trim, mTradeNumber)
            

            'create the response
            mResponse = ""
            mResponse = mHTML.ToString.Trim


        Catch ex As Exception
            mResponse = "<span id='lblmessenger' style='color: Red; font-weight: bold;'>" & _
                            "Error al intentar recuperar los datos." & _
                        "</span>"
        End Try

        context.Response.Write(mResponse.ToString.Trim)
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property


    Private Function CreateHTMLManager(ByVal pCuit As String) As String
        Dim dsDataManager As DataSet
        Dim mReason_Social As String
        Dim mContac As String
        Dim mAddress As String
        Dim mTelephone As String
        Dim mEmail As String
        Dim mHTML As String


        Try

            mHTML = ""


            'I obtain the data of the trade
            dsDataManager = Me.ObtainDataManager(pCuit)


            'I assign the data
            'lblmessenger.Visible = False
            If (clsTools.HasData(dsDataManager)) Then

                If (dsDataManager.Tables("Manager").Rows(0).Item("FECHA_BAJA").ToString.Trim <> "") Then

                    mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:90%;'>" & _
                                "<p id='lblmessenger' style='color: Red; font-weight: inherit;'>" & _
                                    "El usuario Web seleccionado se encuentra en estado de baja. </br>" & _
                                "</p>" & _
                                "<p id='lblmessenger1' style='color: black; font-weight: inherit;'>" & _
                                    "Para realizar esta operación usted deberá primero dirigirse a la opción  <a href='webfrmactivar_representante.aspx'>[Activar usuario Web]</a>" & _
                                "</p>" & _
                            "</div>"
                Else
                    mReason_Social = dsDataManager.Tables("Manager").Rows(0).Item("RAZON_SOCIAL").ToString.Trim
                    mContac = dsDataManager.Tables("Manager").Rows(0).Item("CONTACTO").ToString.Trim
                    mAddress = dsDataManager.Tables("Manager").Rows(0).Item("DIRECCION").ToString.Trim
                    mTelephone = dsDataManager.Tables("Manager").Rows(0).Item("TELEFONO").ToString.Trim
                    mEmail = dsDataManager.Tables("Manager").Rows(0).Item("EMAIL").ToString.Trim

                    mHTML = "<strong>Datos del usuario Web:</strong> " & _
                            "<hr class='linea'/> " & _
                            "<table border='0' style='border-bottom-color:Green; width:100%;' visible='false'; > " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>Razón social:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mReason_Social.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>Responsable:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mContac.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>Dirección:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mAddress.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>Teléfono:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mTelephone.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                               " <tr> " & _
                                   " <td class='columna_titulo_right'><strong>E-mail:</strong></td> " & _
                                   " <td class='columna_dato_left' colspan='2'> " & _
                                       mEmail.ToString.Trim & _
                                   " </td> " & _
                               " </tr> " & _
                            "</table>"

                End If
            Else
                mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                            "<p id='lblmessenger' style='color: Red; font-weight: inherit;'> " & _
                                "Usuario Web inexistente." & _
                            "</p>" & _
                        "</div>"

            End If


        Catch ex As Exception
            mHTML = "<div id='div_messenger' class='Globo GlbRed' style='width:70%;'>" & _
                        "<p id='lblmessenger' style='color: Red; font-weight: inherit;'> " & _
                            "Error al intentar obtener los datos." & _
                        "</p>" & _
                    "</div>"
        End Try


        Return mHTML.ToString.Trim
    End Function


    Private Function ObtainDataManager(ByVal pCuit As String) As DataSet
        Dim dsDataManager_AUX As DataSet

        Try

            'I obtain the data of the trade
            dsDataManager_AUX = New DataSet
            Me.mWS = New ws_tish.Service1
            dsDataManager_AUX = Me.mWS.ObtainDataManager(pCuit.ToString.Trim)
            Me.mWS = Nothing


        Catch ex As Exception
            dsDataManager_AUX = Nothing
        End Try

        Return dsDataManager_AUX
    End Function


    Private Function CreateHTML(ByVal pYear As Integer, ByVal pQuota As Integer, ByVal pType As String, ByVal pTradeNumber As Integer) As String
        Dim dsDetail_DDJJ As DataSet
        Dim mXML_AUX As String
        Dim i As Integer



        Dim mHTML_Header_AUX As String
        Dim mHTML_Header_Rows_AUX As String
        Dim mHTML_Rows_AUX As String
        Dim mHTML_Table As String
        Dim mHTML_Return As String        
        Dim mBgColor_TD As String

        Try



            'I look for the information of the comercio
            mWS = New ws_tish.Service1
            mXML_AUX = mWS.ObtainDetailDDJJ(pTradeNumber, pYear, pQuota)
            mWS = Nothing


            'I read the XML
            dsDetail_DDJJ = New DataSet
            dsDetail_DDJJ = clsTools.ObtainDataXML(mXML_AUX.ToString.Trim)



            mHTML_Header_AUX = "<div id='cabecera' align='center' style='padding: 10px 0px 20px 0px; width:95%;'> " & _
                                    "<table align='center' id='table_period' style='width:70%;' border='0' > " & _
                                        "<tr align='center' style='font-size:medium'> " & _
                                             "<td style='width:5%;' align='center'><strong>A&ntilde;o</strong></td> " & _
                                             "<td style='width:10%;' align='center'><strong>Per&iacute;odo</strong></td> " & _
                                             "<td style='width:10%;' align='center'><strong>Tipo de DDJJ</strong></td> " & _
                                        "<tr> " & _
                                        "<tr> " & _
                                            "<td colspan='3'><hr  style='background-color:black;'/></td> " & _
                                        "</tr> " & _
                                        "<tr> " & _
                                             "<td  align='center'>" & pYear.ToString.Trim & "</td> " & _
                                             "<td style='width:10%;' align='center'>" & pQuota.ToString.Trim & "</td> " & _
                                             "<td style='width:50%;' align='center'>" & pType.Trim & "</td> " & _
                                        "</tr> " & _
                                    "</table> " & _
                                "</div>"






            mHTML_Header_Rows_AUX = "<tr align='center' style='font-size:medium; color:#FFFFFF;' bgcolor='#000000'>" & _
                                        "<td align='left' style='width:60%;'>" & _
                                            "<strong>Rubro</strong>" & _
                                        "</td> " & _
                                        " <td align='center' style='width:10%;'>" & _
                                            "<strong>Empleados</strong>" & _
                                        "</td> " & _
                                        " <td align='center' style='width:20%;'>" & _
                                            "<strong>Monto</strong>" & _
                                        "</td> " & _
                                        " <td align='center' style='width:10%;'>" & _
                                            "<strong>Fecha DDJJ</strong>" & _
                                        "</td>" & _
                                    "</tr>"



            'If the dsDetail_DDJJ is not empty I assign information
            If (clsTools.HasData(dsDetail_DDJJ)) Then

                'I fill the dataset
                mBgColor_TD = ""
                For i = 0 To dsDetail_DDJJ.Tables("RENGLONES_DDJJ").Rows.Count - 1

                    If (mBgColor_TD.Trim <> "white") Then
                        mBgColor_TD = "white"
                    Else
                        mBgColor_TD = "gray"
                    End If

                    mHTML_Rows_AUX = "<tr align='center' style='font-size:medium' bgcolor='" & mBgColor_TD.Trim & "'>" & _
                                        "<td align='left' style='width:60%;'>" & _
                                            dsDetail_DDJJ.Tables("RENGLONES_DDJJ").Rows(i).Item("RUBRO").ToString.Trim & _
                                        "</td> " & _
                                        " <td align='center' style='width:10%;'>" & _
                                            dsDetail_DDJJ.Tables("RENGLONES_DDJJ").Rows(i).Item("EMPLEADOS").ToString.Trim & _
                                        "</td> " & _
                                        " <td align='center' style='width:20%;'>" & _
                                            dsDetail_DDJJ.Tables("RENGLONES_DDJJ").Rows(i).Item("MONTO").ToString.Trim & _
                                        "</td> " & _
                                        " <td align='center' style='width:10%;'>" & _
                                            dsDetail_DDJJ.Tables("RENGLONES_DDJJ").Rows(i).Item("FECHA_PRESENTACION_DDJJ").ToString.Trim & _
                                        "</td>" & _
                                     "</tr>"

                Next
            End If



            mHTML_Table = "<div id='div_table_detail' align='center' style='padding: 10px 5px 0px 5px;'> " & _
                                "<table id='table_detail' align='center' style='width:95%;'>" & _
                                    mHTML_Header_Rows_AUX & _
                                    mHTML_Rows_AUX & _
                                "</table" & _
                          "</div"








            mHTML_Return = "<div id='container'> " & _
                                mHTML_Header_AUX.Trim & vbCr & _
                                mHTML_Table & vbCr & _
                           "</div>"

        Catch ex As Exception
            Return Nothing
        End Try


        Return mHTML_Return.Trim
    End Function


End Class