﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="WebUserControl_Menu.ascx.vb" Inherits="web_tish.WebUserControl_Menu" %>



        <!-- Seteo the resources -->
        <script src="../js/funciones.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../css/estilos_listado.css" type="text/css" />
        <link rel="stylesheet" href="../css/estilos_genericos.css" type="text/css" />
        
                


        <div id="div_options" style="width:27%;  background-color:White;">
            <h1 align="left" style="color:Black">Comercio</h1>
            <ul class="sidemenu">
                <li><a href="../paginas/webfrmdeclaracionesjuradas.aspx">Presentar DDJJ</a></li>
                <li><a href="../paginas/webfrmctacte.aspx">Ver cuenta corriente</a></li>
                <li><a href="../paginas/webfrmopciones.aspx">Datos del comercio</a></li>
                <li><a href="../paginas/webfrmseleccionar_comercio.aspx" onclick="javascript: return trade_change();">Cambiar comercio</a></li>
            </ul>
            
            
            <h1 align="left" style="color:Black">Responsable</h1>
                <ul class="sidemenu">
                    <li><a href="../paginas/webfrmdatos_representante.aspx">Mis datos</a></li>
                    <li><a href="../paginas/webfrmcambiar_clave.aspx">Cambiar clave</a></li>
                    <li><a href="../webfrmindex.aspx" onclick="javascript:return cerrar_sesion_usuario();">Cerrar sesion</a></li>
                </ul>
            </div>
