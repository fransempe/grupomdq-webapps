*************************************************
* FUNCTION FPRXNUMW
*************************************************
* Funci�n id�ntica a FPRXNUM pero que usa la tabla NUMERADW en vez de NUMERAD.
* Se usa para la generaci�n de comprobantes web cuando no se trabaja con la base de datos
* principal en l�nea sino con una r�plica de la misma.
* Como estas bases se sobreescriben peri�dicamente, los nuevos comprobantes no se
* generan en COMPROB, COMP_REN, NUMERAD sino en COMPROBW, COMP_RENW, NUMERADW.
*
******************************************************************
* Autor : RODRIGO
* Dise�o: PACHU
* 
* Fecha :	22/02/95
* 
* Funcionamiento: Devuelve el proximo n�mero de un numerador determinado.
* 
* Par�metros:	NUMERADOR	(C)	:	Numerador
*				GRUPO_COMP	(N)	:	Grupo de comprobantes
*				ACT_TAB		(L)	:	Actualiza tabla ?
*				CHEQ_TAB	(L)	:	Chequea tabla ?
*				DESBLOQ		(L)	:	Desbloquea ?
*				MSG			(L)	:	Emite mensajes de error
*				@PROX_NRO	(N)	:	Pr�ximo n�mero
*				@TIPO_INC	(C)	:	Tipo de inconsistencia
* 
* Modificaciones:
* Gabriel 16/01/96. Agregue hacer varios intentos en el bloqueo.
* Diego 20/02/2007. Agregue la variable msg a la condici�n de muestra del mensaje de espera,
*					para el caso en que no se pueda bloquear NUMERADW. Esto es para permitir
*					la funcionalidad web que no soporta ning�n tipo de mensajes FOX.
*

PARAMETERS m.numerador, m.grupo_comp, act_tab, cheq_tab, desbloq, msg, prox_nro, tipo_inc

IF SET ('DEBUG') = 'ON'
	=VerNuPar ('FPRXNUM', PARAMETERS()	, 8 )
	=VerTiPar ('FPRXNUM', 'M.NUMERADOR'	,'C')
	=VerTiPar ('FPRXNUM', 'M.GRUPO_COMP','N')
	=VerTiPar ('FPRXNUM', 'ACT_TAB'			,'L')
	=VerTiPar ('FPRXNUM', 'CHEQ_TAB'		,'L')
	=VerTiPar ('FPRXNUM', 'DESBLOQ'			,'L')
	=VerTiPar ('FPRXNUM', 'MSG'					,'L')
	=VerTiPar ('FPRXNUM', 'PROX_NRO'		,'N')
	=VerTiPar ('FPRXNUM', 'TIPO_INC'		,'C')
ENDIF

PRIVATE retorno, ex_tabla, ex_recno
PRIVATE salir, mensaje, segundos, bloqueo
retorno 		= .T.
ex_tabla		=	SELE()
ex_recno		=	RECNO()
m.numerador	=	UPPER(PADR(m.numerador,6))
SELE NUMERADW
IF SEEK(m.numerador + STR(m.grupo_comp,2,0) , 'NUMERADW')

	IF act_tab
	
		m.salir = .F.
		DO WHILE !m.salir
		
			m.mensaje  = .F.
			m.segundos = SECONDS ()	
			m.bloqueo  = RLOCK ('numeradw')
			DO WHILE !m.bloqueo AND (SECONDS () - m.segundos <= 10)
				*IF SECONDS () - m.segundos >= 5 AND !m.mensaje
				IF SECONDS () - m.segundos >= 5 AND !m.mensaje AND msg
					WAIT WIND "Intentando Bloquear Numerador de Comprobantes. Aguarde..." NOWAIT
					m.mensaje = .T.
				ENDIF
				m.bloqueo = RLOCK ('numeradw')
			ENDDO
			WAIT CLEAR
				
			IF m.bloqueo
				retorno = .T.
				m.salir = .T.
			ELSE
				retorno  = .F.
				tipo_inc = 'B'
				IF msg
					m.salir = !sino ('El numerador ' + UPPER(ALLT(m.numerador)) + ' ' +;
													 UPPER(ALLT(STR(m.grupo_comp,2,0))) + ' est� bloqueado.' +;
													 CHR (13) + CHR (13) +;
													 '� Desea Reintentar el Bloqueo ?')
					* =MENS_AVI ('RECUR','NUMERADOR BLOQUEADO')
					* Este mensaje esta incorporado dentro de la pregunta anterior.
				ELSE
					m.salir = .T.					
				ENDIF
			ENDIF
		ENDDO
		
	ENDIF
	IF retorno
		prox_nro = numeradw.ult_num + 1
		IF cheq_tab
			retorno = CHEQNUM()
		ENDIF
	ENDIF
	IF retorno AND act_tab
		REPLACE numeradw.ult_num WITH prox_nro
*		IF desbloq
*			UNLOCK IN NUMERADW
*		ENDIF
		UNLOCK IN NUMERADW
		IF !desbloq
			= RLock ('numeradw')
		ENDIF
	ENDIF
ELSE
	retorno 	= .F.
	tipo_inc	= 'N'
	IF msg
		=MENS_AVI('RECUR','NUMERADOR INEXISTENTE')
	ENDIF
ENDIF

IF retorno
	tipo_inc	=	'O'
ELSE
	prox_nro	=	0
ENDIF
	
SELE (ex_tabla)
=IR_REG (ex_recno)

RETURN retorno

*************************************************
FUNCTION CHEQNUM
*************************************************
PRIVATE retorno, ord1, ord2, rec1, rec2
retorno 	= .T.
tipo_inc	=	'O'

DO CASE

CASE ALLTRIM(numeradw.numerador) = 'COMPR'

	ord1	=	ORDER('COMPROBW')
	rec1	=	RECNO('COMPROBW')
	IF SEEK(STR(m.grupo_comp,2,0) + STR(m.prox_nro,9,0), 'COMPROBW')
		tipo_inc	=	'D'
		IF msg
			=MENS_AVI('RECUR','DIFERENCIAS DE NUMERADOR')
		ENDIF
		IF SEEK (STR(m.grupo_comp,2,0)+'999999999', 'COMPROBW')
			=MSG('RECUR', 'NUMERADOR COLMADO')
			prox_nro	=	999999999
		ELSE
			SKIP -1 IN COMPROBW
			prox_nro	=	comprobw.nro_comp + 1
		ENDIF
	ENDIF
	SET ORDER TO (ord1) IN COMPROBW
	=IR_REG(rec1,'COMPROBW')

CASE ALLTRIM(numeradw.numerador) = 'DEVEN'

	ord2	=	ORDER('DEV_TANC')
	rec2	=	RECNO('DEV_TANC')
	GO TOP IN DEV_TANC
	IF !EOF('DEV_TANC')
		SKIP IN DEV_TANC
	ENDIF
	DO WHILE !EOF('DEV_TANC') AND retorno
		IF dev_tanc.nro_dev	=	prox_nro
			retorno 	= .F.
			tipo_inc	=	'D'
			IF msg
				=MENS_AVI('RECUR','DIFERENCIAS DE NUMERADOR')
			ENDIF
		ELSE
			SKIP IN DEV_TANC
		ENDIF
	ENDDO
	SET ORDER TO (ord2) IN DEV_TANC
	=IR_REG(rec2,'DEV_TANC')

ENDCASE

RETURN retorno
