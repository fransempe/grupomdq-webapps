PROCEDURE AgregarCuotasVenc(AuxArchDeud as String)
	IF RECCOUNT('CuotasVenc') # 0
		AuxArchDeud = AuxArchDeud + Indent(1) + '<DETALLEDEUDAVENCIDA>' + _S_
		GO TOP IN CuotasVenc
		DO WHILE NOT EOF('CuotasVenc')
			SELECT CuotasVenc
			AuxArchDeud = AuxArchDeud + Indent(2) + '<REGISTRO_DETALLEDEUDAVENCIDA>' + _S_
			
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DDV_NROMOV>' + ALLTRIM(STR(nro_mov,9,0)) + '</DDV_NROMOV>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DDV_REC>' + ALLTRIM(recurso) + '</DDV_REC>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DDV_ANIO>' + ALLTRIM(STR(anio,4,0)) + '</DDV_ANIO>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DDV_CUOTA>' + ALLTRIM(STR(cuota,3,0)) + '</DDV_CUOTA>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DDV_CONCEPTO>' + ALLTRIM(dsc_ccc) + '</DDV_CONCEPTO>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DDV_DEPLAN>' + ALLTRIM(de_plan) + '</DDV_DEPLAN>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DDV_FECHAVENC>' + ALLTRIM(DTOC(fecven_mov)) + '</DDV_FECHAVENC>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DDV_CONDESPECIAL>' + ALLTRIM(cond_esp) + '</DDV_CONDESPECIAL>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DDV_IMPORTEORIGEN>' + ALLTRIM(STR(imp_ori,15,2)) + '</DDV_IMPORTEORIGEN>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DDV_IMPORTERECARGO>' + ALLTRIM(STR(imp_actint,15,2)) + '</DDV_IMPORTERECARGO>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<DDV_IMPORTETOTAL>' + ALLTRIM(STR(imp_tot,15,2)) + '</DDV_IMPORTETOTAL>' + _S_
			
			AuxArchDeud = AuxArchDeud + Indent(2) + '</REGISTRO_DETALLEDEUDAVENCIDA>' + _S_					
			SKIP IN CuotasVenc
		ENDDO
		AuxArchDeud = AuxArchDeud + Indent(1) + '</DETALLEDEUDAVENCIDA>' + _S_				
		
		AuxArchDeud = AuxArchDeud + Indent(1) + '<TOTALESDEUDAVENCIDA>' + _S_
		
		AuxArchDeud = AuxArchDeud + Indent(2) + '<REGISTRO_TOTALESDEUDAVENCIDA>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_TEXTO>Deuda por per�odos normales sin condiciones especiales:</TDV_TEXTO>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_IMPORTEORIGEN>' + ALLT(STR(AuxOrigNorSCE,15,2)) + '</TDV_IMPORTEORIGEN>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_IMPORTERECARGO>' + ALLT(STR(AuxRecNorSCE,15,2)) + '</TDV_IMPORTERECARGO>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_IMPORTETOTAL>' + ALLT(STR(AuxTotNorSCE,15,2)) + '</TDV_IMPORTETOTAL>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(2) + '</REGISTRO_TOTALESDEUDAVENCIDA>' + _S_
		
		AuxArchDeud = AuxArchDeud + Indent(2) + '<REGISTRO_TOTALESDEUDAVENCIDA>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_TEXTO>Deuda por per�odos normales con condiciones especiales:</TDV_TEXTO>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_IMPORTEORIGEN>' + ALLT(STR(AuxOrigNorCCE,15,2)) + '</TDV_IMPORTEORIGEN>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_IMPORTERECARGO>' + ALLT(STR(AuxRecNorCCE,15,2)) + '</TDV_IMPORTERECARGO>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_IMPORTETOTAL>' + ALLT(STR(AuxTotNorCCE,15,2)) + '</TDV_IMPORTETOTAL>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(2) + '</REGISTRO_TOTALESDEUDAVENCIDA>' + _S_
		
		AuxArchDeud = AuxArchDeud + Indent(2) + '<REGISTRO_TOTALESDEUDAVENCIDA>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_TEXTO>Deuda por cuotas de planes sin condiciones especiales:</TDV_TEXTO>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_IMPORTEORIGEN>' + ALLT(STR(AuxOrigPlanSCE,15,2)) + '</TDV_IMPORTEORIGEN>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_IMPORTERECARGO>' + ALLT(STR(AuxRecPlanSCE,15,2)) + '</TDV_IMPORTERECARGO>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_IMPORTETOTAL>' + ALLT(STR(AuxTotPlanSCE,15,2)) + '</TDV_IMPORTETOTAL>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(2) + '</REGISTRO_TOTALESDEUDAVENCIDA>' + _S_
		
		AuxArchDeud = AuxArchDeud + Indent(2) + '<REGISTRO_TOTALESDEUDAVENCIDA>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_TEXTO>Deuda por cuotas de planes con condiciones especiales:</TDV_TEXTO>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_IMPORTEORIGEN>' + ALLT(STR(AuxOrigPlanCCE,15,2)) + '</TDV_IMPORTEORIGEN>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_IMPORTERECARGO>' + ALLT(STR(AuxRecPlanCCE,15,2)) + '</TDV_IMPORTERECARGO>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_IMPORTETOTAL>' + ALLT(STR(AuxTotPlanCCE,15,2)) + '</TDV_IMPORTETOTAL>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(2) + '</REGISTRO_TOTALESDEUDAVENCIDA>' + _S_
		
		AuxArchDeud = AuxArchDeud + Indent(2) + '<REGISTRO_TOTALESDEUDAVENCIDA>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_TEXTO>Importe total adeudado:</TDV_TEXTO>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_IMPORTEORIGEN>' + ALLT(STR(AuxOrigNorSCE+AuxOrigNorCCE+AuxOrigPlanSCE+AuxOrigPlanCCE,15,2)) + '</TDV_IMPORTEORIGEN>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_IMPORTERECARGO>' + ALLT(STR(AuxRecNorSCE+AuxRecNorCCE+AuxRecPlanSCE+AuxRecPlanCCE,15,2)) + '</TDV_IMPORTERECARGO>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(3) + '<TDV_IMPORTETOTAL>' + ALLT(STR(AuxOrigNorSCE+AuxOrigNorCCE+AuxOrigPlanSCE+AuxOrigPlanCCE+AuxRecNorSCE+AuxRecNorCCE+AuxRecPlanSCE+AuxRecPlanCCE,15,2)) + '</TDV_IMPORTETOTAL>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(2) + '</REGISTRO_TOTALESDEUDAVENCIDA>' + _S_
		
		AuxArchDeud = AuxArchDeud + Indent(1) + '</TOTALESDEUDAVENCIDA>' + _S_
	ELSE
		AuxArchDeud = AuxArchDeud + Indent(1) + '<DETALLEDEUDAVENCIDA>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(1) + '</DETALLEDEUDAVENCIDA>' + _S_
		
		AuxArchDeud = AuxArchDeud + Indent(1) + '<TOTALESDEUDAVENCIDA>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(1) + '</TOTALESDEUDAVENCIDA>' + _S_
	ENDIF
ENDPROC