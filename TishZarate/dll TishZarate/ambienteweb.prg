
PROCEDURE AmbienteWeb() as Boolean

	PUBLIC _STRCONV_, _S_, _CR_, _LF_, _SERVER, _DBASE, sistema, modulo, programa, M_Mensaje, _SISTEMA, _DATOSREPLICADOS, AuxGCompWeb 
	PRIVATE retorno
	retorno = .T.
	
	PUBLIC _CreditoCtaCte as Number
	
	M_Mensaje = ''
	_STRCONV_ = 9	&& C�digo de conversi�n de codificaci�n para pasar a STRCONV()
	_CR_ = CHR(13)
	_LF_ = CHR(10)
	_S_ = _CR_ + _LF_
	
	***************************************************************
	*
	* Seteos de FoxPro
	*
	***************************************************************
	SET DEBUG OFF
	SET BLOCKSIZE TO 33
	SET COLLATE TO 'SPANISH'
	SET CONFIRM ON
	SET CENTURY ON
	SET DATE TO DMY
	SET DELETED ON
	SET EXACT OFF
	SET EXCLUSIVE OFF
	SET MARK TO '/'
	SET MESSAGE TO 24 CENTER
	SET NEAR ON
	SET POINT TO '.'
	SET PRINT OFF
	SET REPROCESS TO 1
	SET SAFETY OFF
	SET SEPARATOR TO ','
	SET CONSOLE OFF
	SET DECIMALS TO 2
	SET ESCAPE OFF
	SET MULTILOCKS ON
	SET NOTIFY ON
	SET REFRESH TO 1,1
	SET SPACE ON
	SET UDFPARMS TO VALUE
	SET TALK OFF
	SET STRICTDATE TO 0
	SET PROCEDURE TO MINIEFIPROC

	***************************************************************
	*
	* Lectura de archivo .INI
	*
	***************************************************************
	_PATH = ""
	_SERVER = ""
	_DBASE = ""
	_SISTEMA = ""
	_DATOSREPLICADOS = ""
	AuxGCompWeb = 0



	* Archivo que contiene los path de los recursos tanto del sistema como de base de datos
	ArchIni = 'c:\windows\ws_tish.ini'
	*ArchIni = 'ws_tish.ini'
	
	
			
	oldpath = SET("Path")
	_SYSPATH_ = GETENV("Path")
	SET PATH TO &_SYSPATH_
	
	_FILE = FOPEN(ArchIni)
	SET PATH TO &oldpath
	IF _FILE > 0

		_SISTEMA = Formar(LeerValorIni('SISTEMA'))
		_PATH	= Formar(LeerValorIni('PATH'))
		SET PATH TO 
		SET PATH TO &_PATH
		_SERVER	= LeerValorIni('SERVER')
		_DBASE	= Formar(LeerValorIni('DBASE'))
		
		_DATOSREPLICADOS = LeerValorIni('DATOSREPLICADOS')
        IF EMPTY(ALLTRIM(_DATOSREPLICADOS)) OR ALLTRIM(_DATOSREPLICADOS) <> 'S'
        	_DATOSREPLICADOS = 'N'
        ENDIF 
		
		FCLOSE(_FILE)
	ELSE
		retorno = .F.
	ENDIF

	m.sistema  = 'RECUR   '
	m.modulo   = 'RECURWEB'
	m.programa = 'RECURWEB'
	
	
	
	* Obtengo la Grupo para los comprobantes WEB
	AuxGCompWeb = VAL(Get_Para('RECUR','GRUPO_COMP_WEB'))
	IF EMPTY(AuxGCompWeb)
		AuxHuboError = .T.
		M_Mensaje = 'El par�metro de configuraci�n GRUPO_COMP_WEB no est� definido o est� mal definido. No se pueden generar comprobantes de pago para cancelaci�n de deuda.'
	ENDIF
	
	

	***************************************************************
	*
	* Control de activaci�n del software
	*
	***************************************************************
	**IF retorno AND NOT Reg2FX(m.programa)
	**	M_Mensaje = 'Servicio no disponible.'
	**	retorno = .F.
	**ENDIF

	RETURN retorno
	
ENDPROC


***********************************************************************
PROCEDURE LeerValorIni(VarALeer as String) as String
***********************************************************************
	PRIVATE VarADevolver
	VarADevolver = ''
	
	FSEEK(_FILE,0,0)
	DO WHILE NOT FEOF(_FILE)
		_LIN_ = FGETS(_FILE)
		_SEPARADOR_ = RAT('=',_LIN_,1)
		IF ALLTRIM(SUBSTR(_LIN_,1,_SEPARADOR_-1)) == ALLTRIM(VarALeer)
			VarADevolver = ALLTRIM(SUBSTR(_LIN_,_SEPARADOR_+1))
			EXIT
		ENDIF
	ENDDO
	
	RETURN VarADevolver
ENDPROC


***********************************************************************
PROCEDURE Formar(ruta as String) as String
***********************************************************************
	PRIVATE VarADevolver
	VarADevolver = ALLTRIM(ruta)
	
	IF LEN(VarADevolver) # RAT('\',VarADevolver)
		VarADevolver = VarADevolver + '\'
	ENDIF
	
	RETURN VarADevolver
ENDPROC