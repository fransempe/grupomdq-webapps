
PROCEDURE _getRubros() as String
	PRIVATE strXML as String

	IF (!USED('rubs_com')) THEN
		=UseT('recur\rubs_com')
	ENDIF
	SELECT rubs_com
	SET ORDER TO Primario IN rubs_com

	strXML = ''
	strXML = strXML + '<?xml version="1.0" encoding="utf-8"?>' + _S_
	strXML = strXML + '<ArrayOfRubro xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">' + _S_

	GO TOP IN rubs_com
	DO WHILE !EOF('rubs_com')
		strXML = strXML +  '<rubro>'				+ _S_
		strXML = strXML +  '<codigo>' 				+ ALLTRIM(STR(rubs_com.rubro_c))	+ '</codigo>' + _S_			
		strXML = strXML +  '<descripcion>' 			+ ALLTRIM(rubs_com.dsc_rubc)		+ '</descripcion>' + _S_			
		strXML = strXML +  '<alicuotaActual>' 		+ ALLTRIM(STR(rubs_com.porc_iddjj))		+ '</alicuotaActual>' + _S_								
		strXML = strXML +  '<alicuotaAnterior>' 	+ ALLTRIM(STR(0))		+ '</alicuotaAnterior>' + _S_								
		strXML = strXML +  '</rubro>' + _S_		
		
		SKIP IN rubs_com
	ENDDO
			 	
	strXML = strXML +  '</ArrayOfRubro >' + _S_			
	RETURN ALLTRIM(strXML) 
ENDPROC