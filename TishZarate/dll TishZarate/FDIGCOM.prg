*************************************************
* FUNCTION FDIGCOM
*************************************************
* Autor :	Rodrigo
* Dise�o:	Pachu
* 
* Fecha :	21/03/95
* 
* Funcionamiento: Calcula el nro que permite la correcci�n del nro de 
* 				un comprobante
* 
* 
* 
* Par�metros:	GRUPO_COMP		(N)	: Grupo de comprobantes
* 				NRO_COMP		(N)	: N�mero de comprobante
* 				TIPO_IMP		(C)	: Tipo de imponible
*				NRO_IMP			(N)	: Numero de imponible
*
* Modificaciones:
* 
PARAMETERS m.grupo_comp , m.nro_comp , m.tipo_imp , m.nro_imp

IF SET ('DEBUG') = 'ON'
	=VerNuPar ('FDIGCOM', PARAMETERS()	, 4 )
	=VerTiPar ('FDIGCOM', 'M.GRUPO_COMP','N')
	=VerTiPar ('FDIGCOM', 'M.NRO_COMP'	,'N')
	=VerTiPar ('FDIGCOM', 'M.TIPO_IMP'	,'C')
	=VerTiPar ('FDIGCOM', 'M.NRO_IMP'	,'N')
ENDIF

PRIVATE m.dig1, m.dig2
m.nro_format	= STR(m.grupo_comp,2,0) + STR(m.nro_comp,9,0) + STR((ASC(m.tipo_imp) - 64),2,0) + STR(m.nro_imp,10,0)
m.dig1			= FDV7532(m.nro_format)
m.nro_format	= m.nro_format + STR(m.dig1,1,0)
m.dig2			= FDV2121(m.nro_format)

RETURN m.dig1 * 10 + m.dig2
