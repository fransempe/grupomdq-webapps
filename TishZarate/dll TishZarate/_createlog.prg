PROCEDURE _CreateLog(p_sMensaje as String) as String
	PRIVATE sFileName as String

	LOCAL exErr AS Exception 
	PRIVATE sResultado as String


	TRY
		sResultado = ''
		
*!*			* creo el nombre del archivo junto con el path de donde lo voy a crear
*!*			sFileName = 'C:\prueba' + DTOC(DATE(),1) + STRTRAN(TIME(),':','') + '.txt'


*!*			IF FILE(ALLTRIM(sFileName)) THEN && Does file exist? 
*!*			  	mFile = FOPEN(ALLTRIM(sFileName),12)     && If so, open read/write
*!*			ELSE
*!*	   			mFile = FCREATE(ALLTRIM(sFileName))  && If not create it
*!*			ENDIF
*!*		
*!*			IF (mFile < 0) THEN     && Check for error opening file
*!*		   		sResultado = 'NO SE PUEDE ABRIR EL ARCHIVO'
*!*			ELSE  && If no error, write to file
*!*				=FWRITE(mFile, ALLTRIM(p_sMensaje))
*!*	  			*=FPUTS(mFile, ALLTRIM(p_sMensaje))
*!*	  			sResultado = 'OK'
*!*			ENDIF
*!*				
*!*			=FCLOSE(mFile) 
	
	

	CATCH TO exErr 
		sResultado = 'ERROR: ' + ALLTRIM(exErr.Message)
	FINALLY
	  	CLEAR EVENTS
	ENDTRY

	RETURN sResultado
ENDPROC