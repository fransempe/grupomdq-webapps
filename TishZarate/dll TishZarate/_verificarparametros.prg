
PROCEDURE _VerificarParametros(pVerParametros as Boolean) as string

	* Variables ASA
	PRIVATE mNombre as String
	PRIVATE mDireccion as String
	PRIVATE mTelefono as String
	PRIVATE mMail as String
	PRIVATE mWeb as String


	* Variables RECUR
	PRIVATE mCantRengCompWeb as Number
	PRIVATE mGrupoCompWebTISH as Number
	PRIVATE mFechaActuaWeb as String
	PRIVATE mFechaVtoWeb as String
	PRIVATE mRecursoTISH as String

	
	PRIVATE mParametro as String
	PRIVATE mXML as String



	IF (!USED ('config')) THEN
		=USET ('asa\config')
	ENDIF
	SELECT config
	SET ORDER TO 1
	

	************************************* Parametros ASA *************************************
	* Parametro Partido
	**********************	
	mParametro = ''	
	mIndex = PADR('ASA', 8, ' ') + 'PARTIDO'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)		
		IF (EMPTY(mParametro)) THEN
			RETURN 'El parámetro PARTIDO no se encuentra definido'				
		ELSE
			mNombre = mParametro 
		ENDIF
	ELSE
		RETURN 'El parámetro PARTIDO no se encuentra en la tabla CONFIG'
	ENDIF
	
	
	
	* Parametro Direccion
	************************
	mParametro = ''	
	mIndex = PADR('ASA', 8, ' ') + 'DOMICILIO'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		IF (EMPTY(mParametro)) THEN		
			RETURN 'El parámetro DOMICILIO no se encuentra definido'						
		ELSE
			mDireccion = mParametro 
		ENDIF
	ELSE
		RETURN 'El parámetro DOMICILIO no se encuentra en la tabla CONFIG'
	ENDIF


	* Parametro Telefono
	***********************	
	mParametro = ''	
	mIndex = PADR('ASA', 8, ' ') + 'WSH_TELEFONO'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		IF (EMPTY(mParametro)) THEN
			RETURN 'El parámetro WSH_TELEFONO no se encuentra definido'				
		ELSE
			mTelefono = mParametro 
		ENDIF
	ELSE
		RETURN 'El parámetro WSH_TELEFONO no se encuentra en la tabla CONFIG'
	ENDIF



	* Parametro Mail
	******************	
	mParametro = ''	
	mIndex = PADR('ASA', 8, ' ') + 'WSH_MAIL'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		IF (EMPTY(mParametro)) THEN
			RETURN 'El parámetro WSH_MAIL no se encuentra definido'				
		ELSE
			mMail = mParametro 
		ENDIF
	ELSE
		RETURN 'El parámetro WSH_MAIL no se encuentra en la tabla CONFIG'
	ENDIF



	* Parametro WEB
	*****************
	mParametro = ''	
	mIndex = PADR('ASA', 8, ' ') + 'WEB'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		IF (EMPTY(mParametro)) THEN
			RETURN 'El parámetro WEB no se encuentra definido'				
		ELSE
			mWeb = mParametro 
		ENDIF
	ELSE
		RETURN 'El parámetro WEB no se encuentra en la tabla CONFIG'
	ENDIF
	********************************************************************************************
	********************************************************************************************






	************************************* Parametros RECUR *************************************
	* Parametro CANTRENGCOMPWEB
	****************************
	mParametro = ''
	mIndex = PADR('RECUR', 8, ' ') + 'CANTRENGCOMPWEB'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)	
			
		IF (EMPTY(mParametro)) OR (TYPE('VAL(mParametro)') # 'N') OR (VAL(mParametro) < 1) THEN		
			RETURN 'El parámetro CANTRENGCOMPWEB no se encuentra definido'						
		ELSE
			mCantRengCompWeb = VAL(mParametro)
		ENDIF
	ELSE
		RETURN 'El parámetro CANTRENGCOMPWEB no se encuentra en la tabla CONFIG'
	ENDIF
	
	
	
	
	* Parametro GRUPO_COMP_TISH
	****************************
	mParametro = ''
	mIndex = PADR('RECUR', 8, ' ') + 'GRUPO_COMP_TISH'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)

		IF (EMPTY(mParametro)) OR (TYPE('VAL(mParametro)') # 'N') OR (VAL(mParametro) < 1) THEN		
			RETURN 'El parámetro GRUPO_COMP_TISH no se encuentra definido'						
		ELSE
			mGrupoCompWebTISH = VAL(mParametro)
		ENDIF
	ELSE	
		RETURN 'El parámetro GRUPO_COMP_TISH no se encuentra en la tabla CONFIG'	
	ENDIF
	
	


	* Parametro FECHA_ACTUA_WEB
	****************************	
	mParametro = ''
	mIndex = PADR('RECUR', 8, ' ') + 'FECHA_ACTUA_WEB'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		
		IF (EMPTY(mParametro)) OR (TYPE('EVALUATE(mParametro)') # 'D') THEN		 
			RETURN 'El parámetro FECHA_ACTUA_WEB no se encuentra definido'						 
		ELSE
			mFechaActuaWeb = mParametro
		ENDIF
	ELSE
		RETURN 'El parámetro FECHA_ACTUA_WEB no se encuentra en la tabla CONFIG'
	ENDIF



	* Parametro FECHA_VTO_WEB
	**************************
	mParametro = ''
	mIndex = PADR('RECUR', 8, ' ') + 'FECHA_VTO_WEB'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		
		IF (EMPTY(mParametro)) OR (TYPE('EVALUATE(mParametro)') # 'D') THEN		
			RETURN 'El parámetro FECHA_VTO_WEB no se encuentra definido'						
		ELSE
			mFechaVtoWeb = mParametro
		ENDIF
	ELSE
		RETURN 'El parámetro FECHA_VTO_WEB no se encuentra en la tabla CONFIG'
	ENDIF
	 
	
	
	
	* Parametro RECURSO_TISH
	**************************
	mParametro = ''
	mIndex = PADR('RECUR', 8, ' ') + 'RECURSO_TISH'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		
		IF (EMPTY(mParametro)) THEN
			RETURN 'El parámetro RECURSO_TISH no se encuentra definido'						
		ELSE
			mFechaVtoWeb = mParametro
		ENDIF
	ELSE
		RETURN 'El parámetro RECURSO_TISH no se encuentra en la tabla CONFIG'
	ENDIF
		 
	
	
	
	* Parametro FECH_VTO1_TISHW
	**************************
	mParametro = ''
	mIndex = PADR('RECUR', 8, ' ') + 'FECH_VTO1_TISHW'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		
		IF (EMPTY(mParametro)) OR (TYPE('EVALUATE(mParametro)') # 'D') THEN		
			RETURN 'El parámetro FECH_VTO1_TISHW no se encuentra definido'						
		ELSE
			mFechaVtoWeb = mParametro
		ENDIF
	ELSE
		RETURN 'El parámetro FECH_VTO1_TISHW no se encuentra en la tabla CONFIG'
	ENDIF

		 
	
	
	
	* Parametro FECH_VTO2_TISHW
	**************************
	mParametro = ''
	mIndex = PADR('RECUR', 8, ' ') + 'FECH_VTO2_TISHW'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		
		IF (EMPTY(mParametro)) OR (TYPE('EVALUATE(mParametro)') # 'D') THEN		
			RETURN 'El parámetro FECH_VTO2_TISHW no se encuentra definido'						
		ELSE
			mFechaVtoWeb = mParametro
		ENDIF
	ELSE
		RETURN 'El parámetro FECH_VTO2_TISHW no se encuentra en la tabla CONFIG'
	ENDIF


		 
	
	
	
	* Parametro CONCEPTO_TISHW
	**************************
	mParametro = ''
	mIndex = PADR('RECUR', 8, ' ') + 'CONCEPTO_TISHW'
	IF (SEEK(mIndex)) THEN
		mParametro = ALLTRIM(config.cont_par)
		
		IF (EMPTY(mParametro)) THEN
			RETURN 'El parámetro CONCEPTO_TISHW no se encuentra definido'						
		ELSE
			mFechaVtoWeb = mParametro
		ENDIF
	ELSE
		RETURN 'El parámetro CONCEPTO_TISHW no se encuentra en la tabla CONFIG'
	ENDIF
	********************************************************************************************
	********************************************************************************************







	************************************* Numerad *************************************
	* Ya sea que se trabaje con base de datos en línea o replicada siempre chequeamos que el
	* numerador exista en numerad. No chequeamos que exista en numeradw porque si la base
	* está replicada sólo basta con chequear en los datos del sistema en línea.

	IF (!USED ('numerad')) THEN
		=USET ('recur\numerad')
	ENDIF
	SELECT numerad
	SET ORDER TO Primario IN numerad

 
	IF NOT (SEEK(PADR('COMPR', 6, ' ') + STR(mGrupoCompWebTISH, 2, 0))) THEN
		RETURN 'El valor ' + ALLTRIM(STR(mGrupoCompWebTISH, 2, 0)) + ' no corresponde a un numerador válido de comprobantes. No se pueden generar comprobantes de pago para cancelación de deuda.'
	ENDIF
	********************************************************************************************
	********************************************************************************************








	************************************* RESPUESTA *************************************
	* Genero el XML para que salga por pantalla o devuelvo un OK
	IF (pVerParametros) THEN
		mXML = ''
		mXML = mXML + Indent(1) + '<PARAMETROS>' + _S_
		
			mXML = mXML + Indent(1) + '<PARAMETROS_ASA>' + _S_
				mXML = mXML + Indent(2) + '<PARTIDO>' + ALLTRIM(mNombre) + '</PARTIDO>' + _S_
				mXML = mXML + Indent(2) + '<DIRECCION>' + ALLTRIM(mDireccion) + '</DIRECCION>' + _S_
				mXML = mXML + Indent(2) + '<NROTELRECWEB>' + ALLTRIM(mTelefono) + '</NROTELRECWEB>' + _S_
				mXML = mXML + Indent(2) + '<DIRMAILRECWEB>' + ALLTRIM(mMail) + '</DIRMAILRECWEB>' + _S_	
				mXML = mXML + Indent(2) + '<WEB>' + ALLTRIM(mWeb) + '</WEB>' + _S_	
			mXML = mXML + Indent(1) + '</PARAMETROS_ASA>' + _S_
		
		
		 
			mXML = mXML + Indent(1) + '<PARAMETROS_RECUR>' + _S_
				mXML = mXML + Indent(2) + '<CANTRENGCOMPWEB>' + ALLTRIM(STR(mCantRengCompWeb)) + '</CANTRENGCOMPWEB>' + _S_
				mXML = mXML + Indent(2) + '<GRUPO_COMP_WEB_TISH>' + ALLTRIM(STR(mGrupoCompWebTISH)) + '</GRUPO_COMP_WEB_TISH>' + _S_
				mXML = mXML + Indent(2) + '<FECHA_ACTUA_WEB>' + ALLTRIM(mFechaActuaWeb) + '</FECHA_ACTUA_WEB>' + _S_
				mXML = mXML + Indent(2) + '<FECHA_VTO_WEB>' + ALLTRIM(mFechaVtoWeb) + '</FECHA_VTO_WEB>' + _S_
			mXML = mXML + Indent(1) + '</PARAMETROS_RECUR>' + _S_

	 	
		mXML = mXML + Indent(1) + '</PARAMETROS>' + _S_
	ELSE
		mXML = 'OK'
	ENDIF

	RETURN ALLTRIM(mXML)
ENDPROC