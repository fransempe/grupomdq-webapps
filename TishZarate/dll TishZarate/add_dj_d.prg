PROCEDURE Add_dj_d(pDatos_XML as String, pCountDDJJ as Number) as String
	LOCAL oErr AS Exception 
	PRIVATE mResult as String
	PRIVATE mERROR as String


	
	
	* I read the xml and believe the cursor	
	mERROR = 'OK'
	XMLTOCURSOR(pDatos_XML, 'pCursor_DJ_D')
	SELECT pCursor_DJ_D		   
	IF UPPER(ALIAS()) # UPPER('pCursor_DJ_D')
		mERROR = 'Imposible generar cursor de datos.'
	ENDIF


	GO TOP IN pCursor_DJ_D
	* Debido a problemas de conversi�n con la funci�n XMLTOCURSOR el primer registro
	*	se utiliza como m�scara de campos y, por la tanto, se omite.
	SKIP IN pCursor_DJ_D


	
	* I open table com_dj_d
	IF (!USED('com_dj_d')) THEN
 		=UseT('recur\com_dj_d')
 	ENDIF
	SELECT com_dj_d
	SCATTER MEMVAR BLANK




	* I add in com_dj_d
	APPEND BLANK						
	REPLACE NRO_COM 	WITH pCursor_DJ_D.NRO_COM
	REPLACE ANIO 		WITH pCursor_DJ_D.ANIO
	REPLACE CUOTA 		WITH pCursor_DJ_D.CUOTA
	REPLACE ORIG_RECT	WITH pCountDDJJ 
	REPLACE NRO_EMPLP 	WITH pCursor_DJ_D.NRO_EMPLP
	REPLACE MONT_FACTP 	WITH pCursor_DJ_D.MONT_FACTP
	REPLACE MONT_GANP 	WITH pCursor_DJ_D.MONT_GANP
	REPLACE MONT_GASTP 	WITH pCursor_DJ_D.MONT_GASTP
	REPLACE MONT_SUELP 	WITH pCursor_DJ_D.MONT_SUELP
	REPLACE PRES_DDJJP 	WITH pCursor_DJ_D.PRES_DDJJP
	REPLACE FECH_DDJJP 	WITH CTOD (pCursor_DJ_D.FECH_DDJJP)
	REPLACE MONT_DDJJP	WITH pCursor_DJ_D.MONT_DDJJP
	REPLACE RUBRO_C 	WITH pCursor_DJ_D.RUBRO_C



	mResult = 'OK'
	
	
	RETURN ALLTRIM(mResult)
ENDPROC