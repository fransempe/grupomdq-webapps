PROCEDURE _GetDatosContribuyenteByCuit(pNroCuit as String) as String
	PRIVATE mDatos as String
	PRIVATE mUbicacion as String
	PRIVATE auxNroCuit as String 
	PRIVATE auxDigVerif as String 
	
	mDatos = ""
	
	auxNroCuit=pNroCuit
	
	auxNroCuit = STRTRAN(auxNroCuit, '-', '') 
	auxNroCuit = SUBSTR(auxNroCuit,1,10)
	auxDigVerif = SUBSTR(pNroCuit,13,1)
	
	* Open a table "contrib"
		IF (!USED('contrib')) THEN
			=UseT('recur\contrib')
		ENDIF
	
		SELECT contrib 
		SET ORDER TO 1
	
			IF SEEK(STR(VAL(auxNroCuit),10),'contrib') AND contrib.dig_veri = VAL(auxDigVerif)
							
			*Obtain a data of contrib 
				mDatos = ''
						mDatos = '<CONTRIBUYENTE_CUIT>' + ALLTRIM(STR(contrib.Cuit)) + '</CONTRIBUYENTE_CUIT>' + _S_
						mDatos = mDatos + '<CONTRIBUYENTE_NOMBRE>' + ALLTRIM(contrib.Nomb_cont) + '</CONTRIBUYENTE_NOMBRE>' + _S_					
						mDatos = mDatos + '<CONTRIBUYENTE_TELEFONO>' + ALLTRIM(contrib.Tel1_con) + '</CONTRIBUYENTE_TELEFONO>' + _S_				
						mDatos = mDatos + '<CONTRIBUYENTE_EMAIL>' + ALLTRIM(contrib.Email1) + '</CONTRIBUYENTE_EMAIL>' + _S_							
							
						mUbicacion = ''											
							IF (!EMPTY(contrib.Nomb_calle)) THEN
								mUbicacion = mUbicacion +' '+ ALLTRIM(contrib.Nomb_calle)
							ENDIF
								
							IF (!EMPTY(contrib.Puerta)) THEN
								mUbicacion = mUbicacion +' '+ ALLTRIM(contrib.Puerta)
							ENDIF
								
							IF (!EMPTY(contrib.Piso)) THEN
								mUbicacion = mUbicacion + ' Piso ' + ALLTRIM(contrib.Piso)
							ENDIF
												
							IF (!EMPTY(contrib.Depto)) THEN
								mUbicacion = mUbicacion + ' Depto. ' + ALLTRIM(contrib.Depto)
							ENDIF
							
							IF (!EMPTY(contrib.Nomb_loc)) THEN
								mUbicacion = mUbicacion + ' - ' + ALLTRIM(contrib.Nomb_loc)
							ENDIF
							
							IF (!EMPTY(contrib.Cod_post)) THEN
								mUbicacion = mUbicacion + ' (' + ALLTRIM(contrib.Cod_post) + ')'
							ENDIF
								
							mDatos = mDatos + '<CONTRIBUYENTE_UBICACION>' + ALLTRIM(mUbicacion) + '</CONTRIBUYENTE_UBICACION>' + _S_

 			ELSE	
				mDatos = mDatos + '<CONTRIBUYENTE_ESTADO>INEXISTENTE</CONTRIBUYENTE_ESTADO>'
			ENDIF
	
	
	mResultado_XML = '<?xml version="1.0" encoding="UTF-8"?>' + _S_
	mResultado_XML = '<CONTRIBUYENTE>' + _S_ + mDatos + '</CONTRIBUYENTE>'
	
	
	RETURN ALLTRIM(mResultado_XML)
	 
ENDPROC