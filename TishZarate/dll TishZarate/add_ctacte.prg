PROCEDURE Add_CTACTE (pRecurso as String, pNumberComercio as Integer, ;
					  pYear as Integer, pQuota as Integer, ;
					  p_datFechaVTO1 as Date, pDesglosaDescuento as Boolean, ;
					  pConceptoBruto as String, pConceptoDescuento as String, ;
					  pImporteBruto as Double, pImporteDescuento as Double, ;
					  pGroupVoucherTISH as Integer, pNextNumberVoucher as Integer, ;
					  numNroMovBruto as Number, numNroMovDescuento as Number) as String
					   

	PRIVATE mResult as String


	IF (!USED('rec_cc')) THEN
		=UseT('recur\rec_cc')
	ENDIF
	

	numNroMovBruto 		= 0
	numNroMovDescuento 	= 0 
	
	
	* Obtengo el n�mero de movimiento
	numNroMovBruto = _getMaximoNroMov (ALLTRIM(pRecurso), pNumberComercio, pYear, pQuota)
	
		
	SELECT rec_cc
	APPEND BLANK	
	REPLACE rec_cc.RECURSO 		WITH ALLTRIM(pRecurso), ;
			rec_cc.TIPO_IMP 	WITH 'C', ;
			rec_cc.NRO_IMP 		WITH pNumberComercio, ;
			rec_cc.ANIO 		WITH pYear, ;
			rec_cc.CUOTA 		WITH pQuota, ;			
			rec_cc.NRO_MOV 		WITH numNroMovBruto, ;
			rec_cc.FECEMI_MOV 	WITH DATE(), ;
			rec_cc.FECVEN_MOV 	WITH p_datFechaVTO1, ;
			rec_cc.IMP_MOV 		WITH STR(pImporteBruto, 12, 2), ;
			rec_cc.CONC_CC 		WITH ALLTRIM(pConceptoBruto), ;
			rec_cc.DESC_MOV 	WITH '', ;
			rec_cc.EST_MOV 		WITH 'NO', ;
			rec_cc.ORIG_MOV 	WITH 'DE', ;
			rec_cc.ID_ORIG 		WITH '', ;
			rec_cc.JUICIO	 	WITH '', ;
			rec_cc.GEST_LEG  	WITH '', ; 			
			rec_cc.INTIMADA  	WITH '', ; 			
			rec_cc.RECONOCIDA 	WITH '', ; 			
			rec_cc.GRUPO_COMP	WITH pGroupVoucherTISH, ;		
			rec_cc.NRO_COMP		WITH pNextNumberVoucher, ;		
			rec_cc.NRO_PLAN		WITH 0 				
			


	* Si el par�metro "pDesglosaDescuento" esta en TRUE, significa que tengo que agregar un nuevo rengl�n con el importe del descuento
	IF (pDesglosaDescuento) THEN

		* Para evitar buscar el n�mero de movimiento directamente le sumo uno al anterior
		numNroMovDescuento = numNroMovBruto + 1
			
		SELECT rec_cc
		APPEND BLANK	
		REPLACE rec_cc.RECURSO 		WITH ALLTRIM(pRecurso), ;
				rec_cc.TIPO_IMP 	WITH 'C', ;
				rec_cc.NRO_IMP 		WITH pNumberComercio, ;
				rec_cc.ANIO 		WITH pYear, ;
				rec_cc.CUOTA 		WITH pQuota, ;			
				rec_cc.NRO_MOV 		WITH numNroMovDescuento, ;
				rec_cc.FECEMI_MOV 	WITH DATE(), ;
				rec_cc.FECVEN_MOV 	WITH p_datFechaVTO1, ;
				rec_cc.IMP_MOV 		WITH STR((pImporteDescuento * -1), 12, 2), ;
				rec_cc.CONC_CC 		WITH ALLTRIM(pConceptoDescuento), ;
				rec_cc.DESC_MOV 	WITH '', ;
				rec_cc.EST_MOV 		WITH 'NO', ;
				rec_cc.ORIG_MOV 	WITH 'DE', ;
				rec_cc.ID_ORIG 		WITH '', ;
				rec_cc.JUICIO	 	WITH '', ;
				rec_cc.GEST_LEG  	WITH '', ; 			
				rec_cc.INTIMADA  	WITH '', ; 			
				rec_cc.RECONOCIDA 	WITH '', ; 			
				rec_cc.GRUPO_COMP	WITH pGroupVoucherTISH, ;		
				rec_cc.NRO_COMP		WITH pNextNumberVoucher, ;		
				rec_cc.NRO_PLAN		WITH 0 										
	ENDIF			


	mResult = 'OK'			
	RETURN ALLTRIM(mResult)
ENDPROC