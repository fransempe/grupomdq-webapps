PROCEDURE _ObtainMunicipalidadData() as String
	PRIVATE mDatos as String
	PRIVATE mNombre as String
	PRIVATE mDireccion as String
	PRIVATE mTelefono as String
	PRIVATE mMail as String
	PRIVATE mWeb as String


	mNombre = ALLTRIM(Get_Para('ASA','PARTIDO'))
	mDireccion = ALLTRIM(Get_Para('ASA','DOMICILIO'))
	mTelefono = ALLTRIM(Get_Para('ASA','WSH_TELEFONO'))
	mMail = ALLTRIM(Get_Para('ASA','WSH_MAIL'))
	mWeb = ALLTRIM(Get_Para('ASA','WEB'))


	mDatos = ''
	mDatos = mDatos + Indent(1) + '<MUNICIPALIDAD>' + _S_
	mDatos = mDatos + Indent(2) + '<MUNICIPALIDAD_NOMBRE>' + TRIM(mNombre) + '</MUNICIPALIDAD_NOMBRE>' + _S_
	mDatos = mDatos + Indent(2) + '<MUNICIPALIDAD_DIRECCION>' + TRIM(mDireccion) + '</MUNICIPALIDAD_DIRECCION>' + _S_		
	mDatos = mDatos + Indent(2) + '<MUNICIPALIDAD_TELEFONO>' + TRIM(mTelefono) + '</MUNICIPALIDAD_TELEFONO>' + _S_
	mDatos = mDatos + Indent(2) + '<MUNICIPALIDAD_MAIL>' + TRIM(mMail) + '</MUNICIPALIDAD_MAIL>' + _S_		
	mDatos = mDatos + Indent(2) + '<MUNICIPALIDAD_WEB>' + TRIM(mWeb) + '</MUNICIPALIDAD_WEB>' + _S_		
	mDatos = mDatos + Indent(1) + '</MUNICIPALIDAD>' + _S_
	
	RETURN TRIM(mDatos)
ENDPROC