
* _getListadoDDJJByComercio:
* Obtengo el listado de las declaraciones juradas para un comercio
PROCEDURE _getListadoDDJJByComercio(p_numNroComercio as Number) as String
	PRIVATE numAnioDesde Number
	PRIVATE numAnioHasta as Number
	PRIVATE strXML as String
	 
	 
	IF (!USED('com_ddjj')) THEN
		=UseT('recur\com_ddjj')
	ENDIF

	IF (!USED('rubs_com')) THEN
		=UseT('recur\rubs_com')
	ENDIF



	* Seteo los per�odos 
	numAnioDesde = (YEAR(DATE()) -2)
	numAnioHasta = YEAR(DATE())
	 

	strXML = ''
	strXML = strXML + '<?xml version="1.0" encoding="utf-8"?>' + _S_
	strXML = strXML + '<ArrayOfCom_ddjj xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">' + _S_

	 
	SELECT com_ddjj
	SET ORDER TO Primario IN com_ddjj
	SET KEY TO RANGE STR(p_numNroComercio, 10, 0) + STR(numAnioDesde, 4, 0), STR(p_numNroComercio, 10, 0) + STR(numAnioHasta, 4, 0) IN com_ddjj
	GO TOP IN com_ddjj	
	DO WHILE NOT EOF('com_ddjj')		
 
		strXML = strXML +  '<com_ddjj>' 			+ _S_
      	strXML = strXML +  '<nroComercio>' 			+ ALLTRIM(STR(p_numNroComercio)) 			+ '</nroComercio>' + _S_
      	strXML = strXML +  '<anio>' 				+ ALLTRIM(STR(com_ddjj.anio)) 				+ '</anio>' + _S_
		strXML = strXML +  '<cuota>'				+ ALLTRIM(STR(com_ddjj.cuota)) 				+ '</cuota>' + _S_
		strXML = strXML +  '<empleados>' 			+ ALLTRIM(STR(com_ddjj.nro_emplp)) 			+ '</empleados>' + _S_
		strXML = strXML +  '<montoDeclarado>' 		+ ALLTRIM(STR(com_ddjj.mont_ddjjp, 13, 2)) 	+ '</montoDeclarado>' + _S_
		
		* Obtengo la descripci�n del rubro
		SELECT rubs_com
		SET ORDER TO Primario IN rubs_com
		IF (SEEK(STR(com_ddjj.rubro_c, 7, 0), 'rubs_com')) THEN				
			strXML = strXML +  '<rubro>'			+ _S_
			strXML = strXML +  '<codigo>' 			+ ALLTRIM(STR(rubs_com.rubro_c))			+ '</codigo>' + _S_			
			strXML = strXML +  '<descripcion>' 		+ ALLTRIM(rubs_com.dsc_rubc)				+ '</descripcion>' + _S_			
			strXML = strXML +  '</rubro>' + _S_						
		ENDIF		
		strXML = strXML +  '</com_ddjj>' + _S_


		SELECT com_ddjj
		SKIP IN com_ddjj
	ENDDO
	SET KEY TO
	 
	
	strXML = strXML +  '</ArrayOfCom_ddjj >'
	RETURN ALLTRIM(strXML) 		 
ENDPROC