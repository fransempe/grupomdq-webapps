*_getEmployeesUsing:
PROCEDURE _getEmployeesUsing() as String
	PRIVATE mResult as String
	PRIVATE mIndex as String
	
	
	IF (!USED('config')) THEN
		=UseT('asa\config')
	ENDIF
	SELECT config
	SCATTER MEMVAR BLANK
	 	
	
	mResult = ''	
	mIndex = PADR('RECUR',8,' ') + 'WEB_USAEMP_TISH'
	IF (SEEK(mIndex)) THEN
		mResult = ALLTRIM(config.cont_par)
	ENDIF
									
	RETURN ALLTRIM(mResult)
ENDPROC