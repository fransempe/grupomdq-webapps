
* _Exist_DDJJ:
* This function says to us if the ddjj exists
PROCEDURE _Exist_DDJJ(pNumberComercio as Integer, pYear as Integer, pQuota as Integer) as Boolean
PRIVATE mIndex as String
PRIVATE mResult as Boolean


	* I open the table com_ddjj
	IF (!USED('com_ddjj')) THEN
		=UseT('recur\com_ddjj')
	ENDIF
	SELECT com_ddjj
	SCATTER MEMVAR BLANK


	* I create the index
	mIndex = STR(pNumberComercio, 10, 0) + STR(pYear, 4, 0) + STR(pQuota, 3, 0)

			
	* I seek the DDJJ
	mResult = .F.
	IF SEEK(mIndex, 'com_ddjj') AND ALLTRIM(com_ddjj.pres_ddjjp) <> 'N' THEN	
		mResult = .T.
	ENDIF
	
	 
	 
	RETURN mResult
ENDPROC