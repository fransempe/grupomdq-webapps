*************************************************
FUNCTION Fx_Actua
*************************************************
* Autor : C&J
* Dise�o: C&J
* 
* Fecha : 30/05/96
* 
* Funcionamiento: calcula la actualizaci�n externamente a los
* programas de SIFIM.
* Esta funci�n, debe ubicarse en el directorio SIFIMPRO\BIN, a
* los efectos de que los distintos programas la encuentren.
*
* Par�metros: m.importe
* 
PARAMETERS m.importe
PRIVATE m.coefic, m.fech_orig, m.anio_bak, m.mes_bak, m.dia_bak, m.porc_admin, m.imp_actual


EXTERNAL ARRAY VEC_ACT


*********************************
*********************************
**                             **
**  PORCENTAJE ADMINISTRATIVO  **
**                             **
*********************************
*********************************
m.porc_admin = 10


m.anio_bak = m.anio_o
m.mes_bak  = m.mes_o
m.dia_bak  = m.dia_o

* Se asigna la variable m.fech_orig la fecha de vencimiento
* original.
m.fech_orig = CTOD (ALLT (STR (m.dia_o)) + '/' + ;
					ALLT (STR (m.mes_o)) + '/' + ;
					ALLT (STR (m.anio_o)))
* Si se trata del �ltimo d�a del mes, se tomar� el mes
* siguiente.
IF MONTH (m.fech_orig) != MONTH (m.fech_orig + 1)
	m.mes_o = mes_o + 1
	IF m.mes_o > 12
		m.anio_o = m.anio_o + 1
		m.mes_o  = 1
		m.dia_o  = 1
	ENDIF
ENDIF

m.coefic = vec_act [m.anio + m.mes - 1, 1] / vec_act [m.anio_o + m.mes_o, 1]

IF vec_act [m.anio_o + m.mes_o, 2] <> 0
	m.coefic = m.coefic * (1 + vec_act [m.anio_o + m.mes_o, 2] *;
					 (m.dias_m - m.dia_o))
ENDIF

IF vec_act [m.anio + m.mes, 2] <> 0
	m.coefic = m.coefic * (1 + vec_act [m.anio + m.mes, 2] * m.dia)
ENDIF

IF m.coefic < 1
	m.coefic = 0
ELSE
	m.coefic = m.coefic - 1
ENDIF

m.anio_o = m.anio_bak
m.mes_o  = m.mes_bak
m.dia_o  = m.dia_bak

* Determino el importe origen Actualizado.
m.imp_actual = ROUND (m.importe * m.coefic, 2)

* Le sumo al importe origen actualizado, el N% de el importe de
* actualizaci�n, que surge del importe origen actualizado, menos
* el importe origen. N es el porcentaje administrativo.
m.imp_actual = m.imp_actual + ((m.imp_actual - m.importe) * m.porc_admin / 100)

RETURN ROUND (m.imp_actual, 2)
