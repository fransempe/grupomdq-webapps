PROCEDURE GrabarComprobPago
	PRIVATE cuit1, cuit2

	m.cuit1 = 0
	m.cuit2 = 0
	=FCONTVIN(_TIPOIMP_, _NROIMP_, .F., .F., @m.cuit1, @m.cuit2)
	
	IF m.cuit1 > 0
		m.sit_iva = contrib.sit_iva
	ELSE
		m.sit_iva = ''
	ENDIF
	
	SELECT comprob
	APPEND BLANK
	REPLACE comprob.grupo_comp			WITH AuxGrupoCompWeb,					;
			comprob.nro_comp			WITH AuxNroComp,						;
			comprob.tipo_imp			WITH _TIPOIMP_,							;
			comprob.nro_imp				WITH _NROIMP_,							;
			comprob.dv_comp				WITH AuxDVComp,							;
			comprob.tipo_comp			WITH 'CP',								;
			comprob.fecemicomp			WITH DATE(),							;
			comprob.fec1_comp			WITH AuxFechaVto1,						;
			comprob.fec2_comp			WITH AuxFechaVto2,						;
			comprob.fec3_comp			WITH {},								;
			comprob.tot_ori				WITH m.tot_ori,							;
			comprob.tot_act				WITH m.tot_act,							;
			comprob.tot_int				WITH m.tot_int,							;
			comprob.tot_iva1			WITH 0,									;
			comprob.tot_iva2			WITH 0,									;
			comprob.tot_comp			WITH m.tot_ori + m.tot_act + m.tot_int,	;
			comprob.tot2_act			WITH 0,									;
			comprob.tot2_int			WITH 0,									;
			comprob.tot2_iva1			WITH 0,									;
			comprob.tot2_iva2			WITH 0,									;
			comprob.tot3_act			WITH 0,									;
			comprob.tot3_int			WITH 0,									;
			comprob.tot3_iva1			WITH 0,									;
			comprob.tot3_iva2			WITH 0,									;
			comprob.sit_iva				WITH m.sit_iva,							;
			comprob.nro_venc			WITH 0,									;
			comprob.est_comp			WITH 'NO',								;
			comprob.exp_comp			WITH '',								;
			comprob.horemicomp			WITH INT(SToT(SECONDS())),				;
			comprob.cuit				WITH m.cuit1,							;
			comprob.nro_aud				WITH 0
	
ENDPROC