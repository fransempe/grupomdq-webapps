PROCEDURE _getFechasVencimiento(p_strRecurso as String, p_numAnio as Number, p_numCuota as Number, datFechaVTO1 as Date, datFechaVTO2 as Date, datFechaVTOP as Date) 
	PRIVATE strIndex as String
 		

	IF (!USED('fec_vtos')) THEN
		=UseT('recur\fec_vtos')
	ENDIF
	SELECT fec_vtos
	SET ORDER TO Primario IN fec_vtos
	

	strIndex = ALLTRIM(p_strRecurso) + STR(p_numAnio,4,0) + STR(p_numCuota,3,0)
	IF (SEEK(strIndex, 'fec_vtos')) THEN	
		datFechaVTO1 = fec_vtos.fv1_cuota			
		datFechaVTO2 = fec_vtos.fv2_cuota
		datFechaVTOP = fec_vtos.fv_p_ddjj
	ENDIF
					
ENDPROC