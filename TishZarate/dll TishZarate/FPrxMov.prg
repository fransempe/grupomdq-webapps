*************************************************
*FUNCTION FPrxMov
*************************************************
PARAMETERS m.recurso, m.tipo_imp, m.nro_imp, m.anio, m.cuota, m.msg, m.batch
PRIVATE m.pto_ant, m.num_mov1, m.num_mov2, m.num_mov
PRIVATE m.pto_ant2

IF SET('DEBUG') = 'ON' AND !m.batch
	IF TYPE('exp_reccc') # 'C'
		=MSG('Debe setear la expresion del indice de REC_CC en el setup.')
		RETURN -1
	ENDIF
ENDIF

IF !m.batch
	m.pto_ant 	= RECNO('rec_cc')
	m.pto_ant2 	= RECNO('rec_ccc')
	SET KEY TO EVAL( exp_reccc ) IN REC_CC
ELSE
	SET KEY TO m.recurso+m.tipo_imp+STR(m.nro_imp,10,0)+STR(m.anio,4,0)+STR(m.cuota,3,0) IN REC_CC
ENDIF

GO BOTTOM IN REC_CC
IF !EOF('REC_CC')
	m.num_mov1 = rec_cc.nro_mov + 1
ELSE
	m.num_mov1 = 1
ENDIF
SET KEY TO '' IN REC_CC

IF m.num_mov1 < 1
	IF m.msg
		=MENS_AVI('RECUR','NROS. MOVIM. NEGATIVOS')
	ENDIF
	m.num_mov1 = 1
ENDIF

IF m.num_mov1 > 999
	IF m.msg
		=MENS_AVI('RECUR','MAS MOVIM. QUE LOS PERMIT.')
	ENDIF
	m.num_mov1 = -1
ENDIF

IF m.num_mov1 > 0
	
	IF !m.batch
		SET KEY TO EVAL( exp_reccc ) IN REC_CCC
	ELSE
		SET KEY TO m.recurso+m.tipo_imp+STR(m.nro_imp,10,0)+STR(m.anio,4,0)+STR(m.cuota,3,0) IN REC_CCC
	ENDIF

	GO BOTTOM IN REC_CCC
	IF !EOF('REC_CCC')
		m.num_mov2 = rec_ccc.nro_mov + 1
	ELSE
		m.num_mov2 = 1
	ENDIF
	SET KEY TO '' IN REC_CCC

	IF m.num_mov2 < 1
		IF m.msg
			=MENS_AVI('RECUR','NROS. MOVIM. NEGATIVOS')
		ENDIF
		m.num_mov2 = 1
	ENDIF

	IF m.num_mov2 > 999
		IF m.msg
			=MENS_AVI('RECUR','MAS MOVIM. QUE LOS PERMIT.')
		ENDIF
		m.num_mov2 = -1
	ENDIF

ENDIF

IF m.num_mov1 > 0 AND m.num_mov2 > 0
	IF m.num_mov1 > m.num_mov2
		m.num_mov = m.num_mov1
	ELSE
		m.num_mov = m.num_mov2
	ENDIF
ELSE
	m.num_mov	=	-1	
ENDIF

IF !m.batch
	=ir_reg( m.pto_ant  , 'rec_cc'  )
	=ir_reg( m.pto_ant2 , 'rec_ccc' )
ENDIF

RETURN m.num_mov

