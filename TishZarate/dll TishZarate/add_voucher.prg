PROCEDURE Add_Voucher (pGroupVoucher as Integer, pNumberVoucher as Integer, ;
					   pDigitVerification as Integer, pNumberComercio as Integer, ;
					   p_datFechaVTO1 as Date, p_datFechaVTO2 as Date, ;					   
					   pDesglosaDescuento as Boolean, ;
					   pConceptoBruto as String, pConceptoDescuento as String, ;
					   p_dblImporteBruto as Double, p_dblImporteDescuento as Double, ;
					   p_numNroMovBruto as Number, p_numNroMovDescuento, ; 					   
					   pCuit as String, pRecurso as String, ;
					   pYear as Integer, pQuota as Integer) as String
					   
					   
	PRIVATE dblImporteBrutoActualizacion as Double
	PRIVATE dblImporteBrutoInteres as Double						   
	PRIVATE dblImporteDescuentoActualizacion as Double
	PRIVATE dblImporteDescuentoInteres as Double						   
	PRIVATE dblImporte_aux as Double
	PRIVATE mResult as String
 

	IF (!USED ('comprob')) THEN
		=UseT ('recur\comprob')
	ENDIF
	
	IF (!USED ('comp_ren')) THEN
		=UseT ('recur\comp_ren')
	ENDIF


		
	
	* Si se desglosa el descuento en dos movimientos resto al bruto el descuento, si no, directamente le asigno el valor del bruto
	IF (pDesglosaDescuento) THEN
		dblImporte_aux = (p_dblImporteBruto - p_dblImporteDescuento)
	ELSE
		dblImporte_aux = p_dblImporteBruto
	ENDIF
		
		
	* Inicializo los valores de actualización y de interes en cero
	dblImporteBrutoActualizacion = 0
	dblImporteBrutoInteres = 0		
	dblImporteDescuentoActualizacion = 0
	dblImporteDescuentoInteres = 0					



	* Si la fecha al segundo vencimiento no es nula, calculo la actualización y el interes
	IF (!EMPTY(p_datFechaVTO2)) THEN
	
		* Calculo la actualización y el interes para el importe bruto
		m.fecha_vto = p_datFechaVTO1
		m.fecha_final = p_datFechaVTO2		
		m.importe = p_dblImporteBruto
		m.actualiza = 0
		m.interes = 0	
		dblImporteBrutoActualizacion = 0
		dblImporteBrutoInteres = 0					
		FACTINT(p_datFechaVTO1, p_datFechaVTO2, STR(p_dblImporteBruto, 15, 6), @dblImporteBrutoActualizacion, @dblImporteBrutoInteres)
				
		
		* Calculo la actualización y el interes para el importe descuento
		m.fecha_vto = p_datFechaVTO1
		m.fecha_final = p_datFechaVTO2		
		m.importe = p_dblImporteDescuento
		m.actualiza = 0
		m.interes = 0	
		dblImporteDescuentoActualizacion = 0
		dblImporteDescuentoInteres = 0					
		FACTINT(p_datFechaVTO1, p_datFechaVTO2, STR(p_dblImporteDescuento, 15, 6), @dblImporteDescuentoActualizacion, @dblImporteDescuentoInteres)
	ENDIF
			



	* Agrego la cabecera del comprobante
	SELECT comprob
	APPEND BLANK	
	REPLACE comprob.grupo_comp			WITH pGroupVoucher, ;					
			comprob.nro_comp			WITH pNumberVoucher, ; 						
			comprob.dv_comp				WITH pDigitVerification, ;							
			comprob.tipo_comp			WITH 'DE', ;								
			comprob.fecemicomp			WITH DATE(), ;																			
			comprob.fec1_comp			WITH p_datFechaVTO1, ; 						
			comprob.fec2_comp			WITH p_datFechaVTO2, ; 								
			comprob.fec3_comp			WITH {}, ;								
			comprob.tipo_imp			WITH 'C', ;
			comprob.nro_imp				WITH pNumberComercio, ;			
			comprob.tot_ori				WITH dblImporte_aux, ;							
			comprob.tot_act				WITH 0, ;							
			comprob.tot_int				WITH 0, ;
			comprob.tot_iva1			WITH 0, ;									
			comprob.tot_iva2			WITH 0, ;									
			comprob.tot_comp			WITH dblImporte_aux, ;
			comprob.tot2_act			WITH (dblImporteBrutoActualizacion + dblImporteDescuentoActualizacion) ;									
			comprob.tot2_int			WITH (dblImporteBrutoInteres + dblImporteDescuentoInteres) ;									
			comprob.tot2_iva1			WITH 0, ;									
			comprob.tot2_iva2			WITH 0, ;									
			comprob.tot3_act			WITH 0, ;									
			comprob.tot3_int			WITH 0, ;									
			comprob.tot3_iva1			WITH 0, ;									
			comprob.tot3_iva2			WITH 0, ;									
			comprob.sit_iva				WITH 'CF', ;							
			comprob.nro_venc			WITH 0, ;									
			comprob.est_comp			WITH 'NO', ;								
			comprob.exp_comp			WITH '', ;								
			comprob.horemicomp			WITH INT(SToT(SECONDS())), ;				
			comprob.cuit				WITH VAL(pCuit), ;
			comprob.nro_aud				WITH 0
		

	* Agrego los renglones del comprobante		
	SELECT comp_ren
	APPEND BLANK	
	REPLACE comp_ren.grupo_comp			WITH pGroupVoucher, ;
			comp_ren.nro_comp   		WITH pNumberVoucher, ;
			comp_ren.reng_comp  		WITH 1, ;
			comp_ren.tipo_imp   		WITH 'C', ;
			comp_ren.nro_imp    		WITH pNumberComercio, ;
			comp_ren.recurso    		WITH ALLTRIM(pRecurso), ;
			comp_ren.anio       		WITH pYear, ;
			comp_ren.cuota      		WITH pQuota, ;
			comp_ren.nro_mov    		WITH p_numNroMovBruto, ;
			comp_ren.conc_cc    		WITH ALLTRIM(pConceptoBruto), ;
			comp_ren.nro_plan   		WITH 0, ;
			comp_ren.imp_reng   		WITH p_dblImporteBruto, ;
			comp_ren.actualiza1 		WITH 0, ;
			comp_ren.intereses1 		WITH 0, ;
			comp_ren.imp1_iva1  		WITH 0, ;
			comp_ren.imp1_iva2  		WITH 0, ;
			comp_ren.actualiza2 		WITH dblImporteBrutoActualizacion, ;
			comp_ren.intereses2 		WITH dblImporteBrutoInteres, ;
			comp_ren.imp2_iva1  		WITH 0, ;
			comp_ren.imp2_iva2  		WITH 0, ;
			comp_ren.actualiza3 		WITH 0, ;
			comp_ren.intereses3 		WITH 0, ;
			comp_ren.imp3_iva1  		WITH 0, ;
			comp_ren.imp3_iva2  		WITH 0, ;
			comp_ren.debita_cc  		WITH 'S', ;
			comp_ren.venc_conc  		WITH 0



	* Si se desglosa el descuento un movimiento agrego un renglón con el descuento
	IF (pDesglosaDescuento) THEN
		SELECT comp_ren
		APPEND BLANK	
		REPLACE comp_ren.grupo_comp			WITH pGroupVoucher, ;
				comp_ren.nro_comp   		WITH pNumberVoucher, ;
				comp_ren.reng_comp  		WITH 2, ;
				comp_ren.tipo_imp   		WITH 'C', ;
				comp_ren.nro_imp    		WITH pNumberComercio, ;
				comp_ren.recurso    		WITH ALLTRIM(pRecurso), ;
				comp_ren.anio       		WITH pYear, ;
				comp_ren.cuota      		WITH pQuota, ;
				comp_ren.nro_mov    		WITH p_numNroMovDescuento, ;
				comp_ren.conc_cc    		WITH ALLTRIM(pConceptoDescuento), ;
				comp_ren.nro_plan   		WITH 0, ;
				comp_ren.imp_reng   		WITH (p_dblImporteDescuento * -1), ;
				comp_ren.actualiza1 		WITH 0, ;
				comp_ren.intereses1 		WITH 0, ;
				comp_ren.imp1_iva1  		WITH 0, ;
				comp_ren.imp1_iva2  		WITH 0, ;
				comp_ren.actualiza2 		WITH (dblImporteDescuentoActualizacion * -1), ;
				comp_ren.intereses2 		WITH (dblImporteDescuentoInteres * -1), ;
				comp_ren.imp2_iva1  		WITH 0, ;
				comp_ren.imp2_iva2  		WITH 0, ;
				comp_ren.actualiza3 		WITH 0, ;
				comp_ren.intereses3 		WITH 0, ;
				comp_ren.imp3_iva1  		WITH 0, ;
				comp_ren.imp3_iva2  		WITH 0, ;
				comp_ren.debita_cc  		WITH 'S', ;
				comp_ren.venc_conc  		WITH 0
	ENDIF



	mResult = 'OK'	
	RETURN ALLTRIM(mResult)
ENDPROC