PROCEDURE Add_dj_r(pDatos_XML as String) as String
	LOCAL oErr AS Exception 
	PRIVATE mResult as String
	PRIVATE mERROR as String

	
	* I read the xml and believe the cursor	
	mERROR = 'OK'
	XMLTOCURSOR(pDatos_XML, 'pCursor_DJ_R')
	SELECT pCursor_DJ_R		   
	IF UPPER(ALIAS()) # UPPER('pCursor_DJ_R')
		mERROR = 'Imposible generar cursor de datos.'
	ENDIF


	GO TOP IN pCursor_DJ_R
	* Debido a problemas de conversi�n con la funci�n XMLTOCURSOR el primer registro
	*	se utiliza como m�scara de campos y, por la tanto, se omite.
	SKIP IN pCursor_DJ_R

	
	* I open table com_dj_r
	IF (!USED('com_dj_r')) THEN
 		=UseT('recur\com_dj_r')
 	ENDIF
	SELECT com_dj_r
	SCATTER MEMVAR BLANK



	* I add in com_dj_r
	APPEND BLANK						
	REPLACE NRO_COM 	WITH pCursor_DJ_R.NRO_COM
	REPLACE ANIO 		WITH pCursor_DJ_R.ANIO
	REPLACE CUOTA 		WITH pCursor_DJ_R.CUOTA
	REPLACE CUIT_AGRET	WITH pCursor_DJ_R.CUIT_AGRET
	REPLACE ANIO_CERT 	WITH pCursor_DJ_R.ANIO_CERT
	REPLACE NRO_CERT 	WITH pCursor_DJ_R.NRO_CERT
	REPLACE DESC_AGRET 	WITH pCursor_DJ_R.DESC_AGRET
	REPLACE FECHA_RET	WITH CTOD (pCursor_DJ_R.FECHA_RET)
	REPLACE IMP_RET		WITH pCursor_DJ_R.IMP_RET


	mResult = 'OK'
	
	
	RETURN ALLTRIM(mResult)
ENDPROC