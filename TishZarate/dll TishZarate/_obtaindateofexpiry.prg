* _ObtainDateOfExpiry:
* This function returns periods with date of expiry bigger than today
PROCEDURE _ObtainDateOfExpiry() as String
	PRIVATE mXML as String
	PRIVATE mXML_AUX as String
	PRIVATE strRecursoSH as String
	PRIVATE AuxFV_P_DDJJ as Date
   PRIVATE AuxFecDesde as Date
		
	* Obtengo el recurso de Seguridad e higiene
	strRecursoSH = _ObtenerRecursoTISH()
	
	IF (!USED('fec_vtos')) THEN
		=UseT('recur\fec_vtos')
	ENDIF			


        *********************************************************
        *Fernando
        *S�lo hay que mostrar el pr�ximo per�odo a vencer pero reci�n despu�s del d�a 1 del mes.
        *Supongamos que la presentaci�n de la DDJJ para el per�odo 9/2015 vence el d�a 20/10/2015 y
        *que para el per�odo 10/2015 la presentaci�n vence el d�a 20/11/2015. Entonces, por ejemplo,
        *el d�a 17/10/2015 hay que mostrar el per�odo 9/2015, el d�a 21/10/2015 nada (porque el per�odo
        *9/2015 ya venci� y el 10/2015 a�n no venci� pero hay que mostrarlo reci�n a partir del d�a 1
        *del mes de vencimiento).

	SELECT fec_vtos
	*SET ORDER TO Primario IN fec_vtos	
	*SET KEY TO RANGE ALLTRIM(strRecursoSH) + STR((YEAR(DATE()) -1), 4, 0), ALLTRIM(strRecursoSH) + STR(YEAR(DATE()), 4, 0) + STR(MONTH(DATE()), 3, 0) IN fec_vtos
	SET ORDER TO vto_ddjj IN fec_vtos	
	SET FILTER TO fv_p_ddjj >= DATE()
	GO TOP IN fec_vtos
	IF !EOF('fec_vtos')


          *Con lo siguiente obtenemos la fecha correspondiente al d�a 1 del mes de la fecha de vencimiento.
          m.AuxFecDesde = fec_vtos.fv_p_ddjj - DAY(fec_vtos.fv_p_ddjj) + 1

          IF DATE() >= m.AuxFecDesde
  	    m.AuxFV_P_DDJJ = fec_vtos.fv_p_ddjj
          ELSE
  	    m.AuxFV_P_DDJJ = {}
          ENDIF


  	ELSE
  	  m.AuxFV_P_DDJJ = {}
  	ENDIF

	mXML_AUX = '<PERIODS>' + _S_
	GO TOP IN fec_vtos
	DO WHILE !EOF('fec_vtos')		
	  IF fec_vtos.fv_p_ddjj = m.AuxFV_P_DDJJ
	    mXML_AUX = mXML_AUX + '<ROWS_PERIODS>'
		
		mXML_AUX = mXML_AUX + '<ANIO>' + ALLTRIM(STR(fec_vtos.anio)) + '</ANIO>'  
		mXML_AUX = mXML_AUX + '<CUOTA>' + ALLTRIM(STR(fec_vtos.cuota)) + '</CUOTA>'  
		mXML_AUX = mXML_AUX + '<FV_CUOTA1>' + ALLTRIM(DTOC(fec_vtos.fv1_cuota)) + '</FV_CUOTA1>'  
		mXML_AUX = mXML_AUX + '<FV_CUOTA2>' + ALLTRIM(DTOC(fec_vtos.fv2_cuota)) + '</FV_CUOTA2>'  
		mXML_AUX = mXML_AUX + '<FV_PRESENTACION>' + ALLTRIM(DTOC(fec_vtos.fv_p_ddjj)) + '</FV_PRESENTACION>'
		
		IF (fec_vtos.fv_p_agret >= DATE()) THEN  
			mXML_AUX = mXML_AUX + '<FV_PRESENTACION_AGRET>' + ALLTRIM(DTOC(fec_vtos.fv_p_agret)) + '</FV_PRESENTACION_AGRET>'
		ELSE
		  	mXML_AUX = mXML_AUX + '<FV_PRESENTACION_AGRET>' + 'Vencida' + '</FV_PRESENTACION_AGRET>'
		ENDIF
		mXML_AUX = mXML_AUX + '</ROWS_PERIODS>'
	  ENDIF
	  SKIP IN fec_vtos
	ENDDO
    SET FILTER TO 
    SET ORDER TO PRIMARIO IN FEC_VTOS
	SET KEY TO ''
	mXML_AUX = mXML_AUX + '</PERIODS>' + _S_ 
 		

 	* I add the head-board of the XML and the label contenedora
	mXML = '<?xml version="1.0" encoding="UTF-8"?>' + _S_ 
	mXML = mXML + '<DATO>' + _S_ + mXML_AUX + _S_ + '</DATO>'
 
	RETURN ALLTRIM(mXML)
ENDPROC