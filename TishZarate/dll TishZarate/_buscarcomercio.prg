PROCEDURE _BuscarComercio(pDato as String, pTipoBusqueda as String) as String
	PRIVATE mResultado_XML as String
	PRIVATE mResultado_XML_AUX as String	
	PRIVATE mIndice as String
	PRIVATE mIndice_AUX as Integer


	* Abro las tablas que voy a utilizar
	* TABLA: Comercio
	=UseT('recur\comercio')
	SELECT comercio
	SCATTER MEMVAR BLANK
	
	* TABLA: Contrib (Contribuyentes)
	=UseT('recur\contrib')
	SELECT comercio
	SCATTER MEMVAR BLANK
	

	
	* Genero el Inidice por el cual voy a realizar la busqueda
	DO CASE
         CASE pTipoBusqueda= 'CODIGO'
         	mIndice_AUX = VAL(pDato)
         	mIndice = STR(mIndice_AUX, 10, 0)
         	
         CASE pTipoBusqueda= 'CUIT'
         	mIndice = ''
         OTHERWISE
         	mIndice = ''
	ENDCASE
	
	
	
	* Busco el comercio
	mResultado_XML = ''
	IF (SEEK(mIndice))
 
 		
 
	 	IF (EMPTY(comercio.fec_baja))
			mResultado_XML = '<COMERCIO_NUMERO>' + ALLTRIM(STR(comercio.Nro_com)) + '</COMERCIO_NUMERO>' + _S_
			mResultado_XML = mResultado_XML + '<COMERCIO_NOMBRE>' + ALLTRIM(comercio.Nomb_com) + '</COMERCIO_NOMBRE>' + _S_
			mResultado_XML = mResultado_XML + '<COMERCIO_DOMICILIO>' + ALLTRIM(comercio.Nomb_calle) + '</COMERCIO_DOMICILIO>' + _S_
			
			
			* Busco los datos del contribuyente	
			m.cuit1 = 0
			m.cuit2 = 0
			=FContVin('C', comercio.Nro_com,.F.,.F.,@m.cuit1,@m.cuit2)
			
			* Si el imponible es un contribuyente y no tiene ninguna entrada en VINC_IMP ==> ese mismo
			*	contribuyente se toma c�mo titular titular.					
			IF m.cuit1 <= 0 AND pTipoImponible = 'N'
				m.cuit1 = pNroImponible 
			ENDIF
			
			IF m.cuit1>0
				IF SEEK(STR(m.cuit1,10,0),'contrib')									
					mResultado_XML = mResultado_XML + '<CONTRIBUYENTE_CUIT>' + ALLTRIM(STR(contrib.cuit)) + '</CONTRIBUYENTE_CUIT>' + _S_
					mResultado_XML = mResultado_XML + '<CONTRIBUYENTE_NOMBRE>' + ALLTRIM(contrib.Nomb_cont) + '</CONTRIBUYENTE_NOMBRE>' + _S_							
				ENDIF
			ENDIF
			
			mResultado_XML = mResultado_XML + '<COMERCIO_ESTADO>' + 'OK' + '</COMERCIO_ESTADO>' + _S_													
		ELSE
			mResultado_XML = '<COMERCIO_ESTADO>' + 'BAJA' + '</COMERCIO_ESTADO>'
		ENDIF
	ELSE
		mResultado_XML = '<COMERCIO_ESTADO>' + 'INEXISTENTE' + '</COMERCIO_ESTADO>'
	ENDIF
	
	
	
	* Agrego las etiquetas contenedoras del XML
	mResultado_XML_AUX = '<?xml version="1.0" encoding="UTF-8"?>' + _S_
	mResultado_XML = mResultado_XML_AUX  + _S_ + '<DATOS>' + _S_ + ALLTRIM(mResultado_XML) + '</DATOS>'
		
	
	RETURN ALLTRIM(mResultado_XML)
ENDPROC