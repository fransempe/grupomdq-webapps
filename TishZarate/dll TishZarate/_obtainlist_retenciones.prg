
* 27/03/2017
* _obtainlist_Retenciones:
* Obtengo el listado de las retenciones de un comercio segun periodo y a�o.
PROCEDURE _obtainlist_Retenciones(p_numNroComercio as Number, pAnio as Number, pCuota as Number ) as String

	PRIVATE strXML as String
	PRIVATE mIndex as String 
	 
	IF (!USED('com_dj_r')) THEN
		=UseT('recur\com_dj_r')
	ENDIF

	 
	strXML = ''
	strXML = strXML + '<?xml version="1.0" encoding="utf-8"?>' + _S_
	strXML = strXML + '<ArrayOfCom_dj_r xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">' + _S_

	
	SELECT com_dj_r
	*SET ORDER TO Primario IN com_dj_r
	
	mIndex = STR(p_numNroComercio, 10, 0) + STR(pAnio, 4, 0) + STR(pCuota, 3, 0)
	
	SET KEY TO mIndex IN com_dj_r
	GO TOP IN com_dj_r	
	DO WHILE NOT EOF('com_dj_r')		
 
		strXML = strXML +  '<com_dj_r>' 			+ _S_
      	strXML = strXML +  '<nroComercio>' 			+ ALLTRIM(STR(p_numNroComercio)) 			+ '</nroComercio>' + _S_
      	strXML = strXML +  '<anio>' 				+ ALLTRIM(STR(com_dj_r.anio)) 				+ '</anio>' + _S_
		strXML = strXML +  '<cuota>'				+ ALLTRIM(STR(com_dj_r.cuota)) 				+ '</cuota>' + _S_
		strXML = strXML +  '<cuit>' 				+ ALLTRIM(STR(com_dj_r.cuit_agret,11))			+ '</cuit>' + _S_
		strXML = strXML +  '<fecha_ret>' 			+ ALLTRIM(DTOC(com_dj_r.fecha_ret))	    	+ '</fecha_ret>' + _S_
		strXML = strXML +  '<descripcion>' 			+ ALLTRIM(com_dj_r.desc_agret)		    	+ '</descripcion>' + _S_
		strXML = strXML +  '<importe_retenido>' 	+ ALLTRIM(STR(com_dj_r.imp_ret, 13, 2)) 	+ '</importe_retenido>' + _S_
		strXML = strXML +  '<importe_facturado>' 	+ ALLTRIM(STR(com_dj_r.imp_fact, 13, 2)) 	+ '</importe_facturado>' + _S_
		strXML = strXML +  '<nroComprob>' 	+ ALLTRIM(STR(com_dj_r.Anio_cert)) + '/' + ALLTRIM(STR(com_dj_r.Nro_cert)) 	+ '</nroComprob>' + _S_
		strXML = strXML +  '</com_dj_r>' + _S_


		SELECT com_dj_r
		SKIP IN com_dj_r
	ENDDO
	SET KEY TO
	 
	
	strXML = strXML +  '</ArrayOfCom_dj_r >'
	RETURN ALLTRIM(strXML) 		 
ENDPROC