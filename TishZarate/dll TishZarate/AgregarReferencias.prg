PROCEDURE AgregarReferencias(AuxArchDeud as String)
	IF RECCOUNT('refrecursos') # 0
		GO TOP IN refrecursos
		SELECT refrecursos
		DO WHILE NOT EOF('refrecursos')
			IF SEEK('RECURS'+refrecursos.recurso,'codifics')
				*REPLACE refrecursos.desc_recurso WITH codifics.desc_cod
				m.desc_recurso = codifics.desc_cod
				GATHER MEMVAR FIELDS desc_recurso
			ENDIF
			SKIP IN refrecursos
		ENDDO
		
		AuxArchDeud = AuxArchDeud + Indent(1) + '<REFRECURSOS>' + _S_
		
		GO TOP IN RefRecursos
		SELECT RefRecursos
		DO WHILE NOT EOF('RefRecursos')
			AuxArchDeud = AuxArchDeud + Indent(2) + '<REGISTRO_REFRECURSOS>' + _S_
			
			AuxArchDeud = AuxArchDeud + Indent(3) + '<REC_CODIGO>' + ALLTRIM(recurso) + '</REC_CODIGO>' + _S_
			AuxArchDeud = AuxArchDeud + Indent(3) + '<REC_DESCRIPCION>' + ALLTRIM(desc_recurso) + '</REC_DESCRIPCION>' + _S_
			
			AuxArchDeud = AuxArchDeud + Indent(2) + '</REGISTRO_REFRECURSOS>' + _S_
			SKIP IN RefRecursos
		ENDDO
		AuxArchDeud = AuxArchDeud + Indent(1) + '</REFRECURSOS>' + _S_
	ELSE
		AuxArchDeud = AuxArchDeud + Indent(1) + '<REFRECURSOS>' + _S_
		AuxArchDeud = AuxArchDeud + Indent(1) + '</REFRECURSOS>' + _S_
	ENDIF
ENDPROC