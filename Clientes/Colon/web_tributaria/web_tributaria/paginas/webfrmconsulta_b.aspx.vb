﻿Option Explicit On

Imports System.Drawing

Partial Public Class webfrmconsulta_b
    Inherits System.Web.UI.Page


#Region "Variables"

    Private mWS As web_tributaria.ws_consulta_tributaria.Service1
    Private mLog As Clslog
    Private mXml As String

#End Region

#Region "Eventos Formulario"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Call Me.ConfigurarCaptcha()

        If Not (IsPostBack) Then

            txtdominio.Visible = False
            'id_td_titulo_dominio.Visible = False
            id_tr_dominio.Visible = False


            Call Me.CargarDatosMunicipalidad()

            'Obtener Nro de Cuenta por Dominio
            Me.txtdominio.Attributes.Add("Onchange", "javascript:solo_mayusculas(this);")
            Me.btn_obtener_cuenta.Attributes.Add("onclick", "javascript:return validar_dominio();")
            Me.btn_obtener_cuenta.Attributes.Add("onmouseover", "javascript:boton_dominio_onmouseover(this);")
            Me.btn_obtener_cuenta.Attributes.Add("onmouseout", "javascript:boton_dominio_onmouseout(this);")
            Me.cmbtipo_cuentas.Attributes.Add("onchange", "javascript:redimensionar_div();")


            Me.link_consultar_deuda.Attributes.Add("onclick", "javascript:return validar_datos();")
            Me.txtcaptcha.Attributes.Add("onkeydown", "javascript:consultar_deuda(this, event);")



            Call Me.ConfigurarCaptcha()

            Me.lblcontador.Text = "Cantidad de visitas: " & Me.getVisitasCantidad()
            Me.lblversion.Text = "Versión " & ClsTools.mVersion.ToString.Trim


            Call Me.usaLoginAdmin()


            Me.txtnro_cuenta.Focus()
        Else
            Call LimpiarControles()
        End If
        Call Me.CargarListbox()

    End Sub

    Protected Sub link_consultar_deuda_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_consultar_deuda.Click
        Dim dsDatos As DataSet
        Dim NroImponible As Long
        Dim tipo_cuenta As String
        Try

            'Aquí esta el control de cuentas bloqueadas de SIFIM para Brandsen. 
            If (System.Configuration.ConfigurationManager.AppSettings("CuentaBloqueada") = True) Then
                Me.mWS = New ws_consulta_tributaria.Service1
                NroImponible = txtnro_cuenta.Text

                Select Case cmbtipo_cuentas.Text
                    Case "Inmueble"
                        tipo_cuenta = "I"
                    Case "Comercio"
                        tipo_cuenta = "C"
                    Case "Vehículo"
                        tipo_cuenta = "V"
                    Case "Cementerio"
                        tipo_cuenta = "E"
                End Select

                Me.mXml = Me.mWS.ObtenerImponibleBloqueado(tipo_cuenta, NroImponible)
                Me.mWS = Nothing
                If Me.mXml = "BLOQ" Then
                    lblmensaje.Visible = True
                    div_mensaje.Visible = True
                    lblmensaje.Text = "Este número de cuenta esta bloqueado."
                    Exit Sub
                End If
            End If


            Me.div_mensaje.Visible = False
            Me.lblmensaje.Visible = False
            If (Me.Validar()) Then


                'Limpio la Session por las dudas que el usuario este logueado y quiera volver a loguearse
                Call Me.LimpiarSession(False)


                'Consulto si el Contribuyente tiene DEUDA
                Me.mWS = New ws_consulta_tributaria.Service1
                Me.mXml = mWS.ConsultarDeuda(Me.cmbtipo_cuentas.Text, CLng(Me.txtnro_cuenta.Text.Trim), -1)
                Me.mWS = Nothing



                'Obtengo los Datos
                dsDatos = New DataSet
                dsDatos = Me.ObtenerDatosXML()



                'Si la cuenta No Existe no tiene Deuda lo muestro el mensaje
                If (dsDatos.Tables("REGISTRO_NOTAS") IsNot Nothing) Then
                    Call Me.GenerarMensajeCuenta(dsDatos)
                    Exit Sub
                End If


                'Asigno Variables de Session
                Session("tipo_cuenta") = Me.cmbtipo_cuentas.Text
                Session("nro_cuenta") = Me.txtnro_cuenta.Text.Trim
                Session("nro_usuario") = dsDatos.Tables("DATOSCUENTA").Rows(0).Item("CLAVE").ToString


                Session("dominio") = ""
                If (Session("tipo_cuenta").ToString.Trim = "Vehículo") Then
                    Session("dominio") = Me.txtdominio.Text.Trim
                End If


                'Guardo los datos del Rodado
                If (dsDatos.Tables("DATOSRODADO") IsNot Nothing) Then
                    Session("datosRodado") = dsDatos.Tables("DATOSRODADO")
                Else
                    Session("datosRodado") = Nothing
                End If

                'Cargo los Datos de la Municipalidad 
                Call Me.CargarDatosMunicipalidad()


                Call Me.agregarVisita()
                Response.Redirect("webfrmlistado.aspx", False)
            Else

                Call Me.CrearAuditoria()
                Call Me.LimpiarSession(True)
            End If


        Catch ex As Exception
            Me.mLog = New Clslog
            Me.mLog.GenerarLog(ex, Me.ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            Me.mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)
        End Try
    End Sub

#End Region

#Region "Procedimientos y Funciones"

    Private Function Validar() As Boolean

        'Nro. Cuenta
        If (txtnro_cuenta.Text <> "") Then
            If Not (IsNumeric(txtnro_cuenta.Text)) Then
                Return False
            End If
        Else
            Return False
        End If



        verificador_captcha.ValidateCaptcha(txtcaptcha.Text.Trim())
        If Not (verificador_captcha.UserValidated) Then
            Me.lblmensaje.Text = "Código de verificación incorrecto."
            Me.txtcaptcha.BackColor = Drawing.Color.Red
            Me.txtcaptcha.ForeColor = Drawing.Color.White
            Me.txtcaptcha.Text = ""
            Me.txtcaptcha.Focus()

            Me.div_mensaje.Visible = True
            Me.lblmensaje.Visible = True

            Return False
        End If


        Return True
    End Function


    Private Sub GenerarMensajeCuenta(ByVal dsdatos As DataSet)

        Select Case dsdatos.Tables("REGISTRO_NOTAS").Rows(0).Item("MENSAJE").ToString
            Case "Cuenta Inexistente"
                Me.lblmensaje.Text = "Cuenta inexistente."
                Me.txtnro_cuenta.BackColor = Drawing.Color.Red
                Me.txtnro_cuenta.ForeColor = Drawing.Color.White
                Me.div_mensaje.Visible = True
                Me.lblmensaje.Visible = True

            Case "La cuenta no registra deuda al " & Format(Now, "dd/MM/yyyy") & "."
                If (CBool(System.Configuration.ConfigurationManager.AppSettings("ImprimirListadoDeudaSinDeuda").ToString.Trim)) Then
                    Session("tipo_cuenta") = Me.cmbtipo_cuentas.Text
                    Session("nro_cuenta") = Me.txtnro_cuenta.Text.Trim
                    Response.Redirect("webfrmlistadosindeuda.aspx", False)
                Else
                    If (Me.cmbtipo_cuentas.Text = "Comercio") Then
                        Me.lblmensaje.Text = "No existen periodos adeudados que puedan visualizarse desde esta aplicación."
                    Else
                        Me.lblmensaje.Text = "La cuenta no registra deuda."
                    End If
                    Me.div_mensaje.Visible = True
                    Me.lblmensaje.Visible = True
                End If
        End Select


        Me.txtcaptcha.Text = ""
        Me.txtnro_cuenta.Focus()
    End Sub

#End Region

#Region "LOG"

    Private Function ObtenerDetalleWEB() As String
        Dim mInfoLog As String = ""
        Try

            For Each mNombreVariable In Request.ServerVariables
                mInfoLog = mInfoLog & mNombreVariable.ToString & ": " & Request.ServerVariables(mNombreVariable.ToString)
                mInfoLog = mInfoLog & vbCrLf
            Next

            Return mInfoLog.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function
#End Region

#Region "Auditoria"

    Private Sub CrearAuditoria()
        Dim mRutaFisica As String
        mRutaFisica = Request.ServerVariables("APPL_PHYSICAL_PATH").ToString & "auditoria"

        'Controlo la Cantidad de Archivos
        Call CantidadLog(1000, mRutaFisica.ToString)

        'Genero el TXT
        Call GenerarLogAuditoria(mRutaFisica.ToString)
    End Sub

    Private Sub CantidadLog(ByVal mCantidadLog As Integer, ByVal mRutaFisica As String)

        If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString)) Then
            My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString)
        End If


        Dim mArchivos As System.Collections.ObjectModel.ReadOnlyCollection(Of String)
        Dim mNombreCarpeta As String = ""
        mArchivos = My.Computer.FileSystem.GetFiles(mRutaFisica.ToString)



        Dim c As Integer
        Dim arreglo(mArchivos.Count() - 1) As String
        For c = 0 To UBound(arreglo, 1)
            arreglo(c) = mArchivos(c)
        Next


        If mCantidadLog = 0 Then
            Dim k As Integer
            For k = 0 To (UBound(arreglo, 1)) - 1
                mNombreCarpeta = My.Computer.FileSystem.GetName(Trim(arreglo(k).ToString))
                My.Computer.FileSystem.DeleteFile(Trim(arreglo(k).ToString))
            Next


        Else
            If mArchivos.Count > mCantidadLog Then
                Dim i As Integer
                For i = UBound(arreglo, 1) - (mCantidadLog) To 0 Step -1
                    mNombreCarpeta = My.Computer.FileSystem.GetName(arreglo(i))
                    My.Computer.FileSystem.DeleteFile(Trim(arreglo(i).ToString))
                Next
            End If
        End If


    End Sub

    Private Sub GenerarLogAuditoria(ByVal mRutaFisica As String)
        Dim mCadena As String


        If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString)) Then
            My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString)
        End If


        'Armo el Log
        Dim mLog As New IO.StreamWriter(mRutaFisica.ToString & "\auditorialog" & Format(Now, "ddMMyyyyHHmmss") & ".txt")
        mCadena = "FECHA: " & Format(Now, "dd/MM/yy hh:mm:ss") & "|"
        mCadena = mCadena & "IP: " & Request.ServerVariables("REMOTE_ADDR").ToString() & "|"
        mCadena = mCadena & "HOST: " & Request.ServerVariables("REMOTE_HOST").ToString() & "|"
        mCadena = mCadena & "PUERTO: " & Request.ServerVariables("REMOTE_PORT").ToString()

        mLog.WriteLine(mCadena.ToString)


        mLog.WriteLine("")
        mLog.WriteLine("-- DATOS INGRESADOS --")


        mLog.WriteLine("NRO. CUENTA:        " & txtnro_cuenta.Text)
        'mLog.WriteLine("TIPO CUENTA:        " & cmbtipo_cuenta.Text)

        mLog.Close()

    End Sub

#End Region


    Private Sub CargarDatosMunicipalidad()
        Dim dsdatos As DataSet

        Try

            Me.lblmunicipalidad_name.Text = System.Configuration.ConfigurationManager.AppSettings("Nombre_Municipalidad").ToString.Trim

            Me.mWS = New ws_consulta_tributaria.Service1
            Me.mXml = Me.mWS.ObtenerDatosMunicipalidad()
            Me.mWS = Nothing

            dsdatos = New DataSet
            dsdatos = ObtenerDatosXML()
            If (dsdatos.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_TELEFONO").ToString = "") Then
                Me.lbltelefono.Text = "00-000000"
            Else
                Me.lbltelefono.Text = dsdatos.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_TELEFONO").ToString
            End If

            If (dsdatos.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_MAIL").ToString = "") Then
                Me.lblmail.Text = "mail@mail.com.ar"
            Else
                Me.lblmail.Text = dsdatos.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_MAIL").ToString
            End If
            dsdatos = Nothing

            Session("municipalidad_telefono") = Me.lbltelefono.Text
            Session("municipalidad_mail") = Me.lblmail.Text

        Catch ex As Exception
            Me.mLog = New Clslog
            Me.mLog.GenerarLog(ex, Me.ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            Me.mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)
        End Try
    End Sub


    Private Function ObtenerDatosXML() As DataSet
        Dim mRutaFisica As String
        Dim mArchivo As String
        Dim dsDatos As DataSet

        Try

            mRutaFisica = Request.ServerVariables("APPL_PHYSICAL_PATH").ToString

            If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString & "datos_temporales")) Then
                My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString & "datos_temporales")
            End If

            mArchivo = mRutaFisica.ToString & "datos_temporales\" & Format(Now, "ddMMyyyyHHmmss") & ".xml"


            Dim mCrearXML As New IO.StreamWriter(mArchivo.ToString)
            mCrearXML.WriteLine(mXml.ToString)
            mCrearXML.Close()

            dsDatos = New DataSet
            dsDatos.ReadXml(mArchivo.ToString)

            'My.Computer.FileSystem.DeleteFile(mArchivo.ToString)


            Return dsDatos
        Catch ex As Exception
            Me.mLog = New Clslog
            Me.mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            Me.mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)
            Return Nothing
        End Try
    End Function


    Private Sub LimpiarControles()
        Me.txtnro_cuenta.BackColor = Drawing.Color.White
        Me.txtnro_cuenta.ForeColor = Drawing.Color.Black
        Me.txtcaptcha.BackColor = Drawing.Color.White
        Me.txtcaptcha.ForeColor = Drawing.Color.Black
    End Sub

#Region "Limpiar Session"

    Private Sub LimpiarSession(ByVal mDatosInvalidos As Boolean)
        Dim mNroUsuario As Integer
        Dim sesionLoginAdmin As String


        Try

            'Limpio en la Base de Datos
            If (Session("tipo_conexion") IsNot Nothing) Then
                If (Application("tipo_conexion").ToString = "RAFAM") Then
                    If (Session("nro_usuario") IsNot Nothing) Then
                        If (Session("nro_usuario").ToString <> "") Then

                            mNroUsuario = CInt(Session("nro_usuario"))

                            mWS = New ws_consulta_tributaria.Service1
                            mWS.LimpiarDatosSession(mNroUsuario)
                            mWS = Nothing

                            Application("cant_usuarios") = CInt(Application("cant_usuarios")) - 1
                        End If
                    End If
                End If
            End If



            'Si los datos son Invalidos borro cualquier Session
            If (mDatosInvalidos) Then

                sesionLoginAdmin = ""
                If (Session("loginAdmin") IsNot Nothing) Then
                    sesionLoginAdmin = Session("loginAdmin")
                End If

                Session.RemoveAll()
                Session.Clear()
                Session.Abandon()

                If (sesionLoginAdmin.Trim() <> "") Then
                    Session("loginAdmin") = sesionLoginAdmin
                End If
            End If


        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
        End Try

    End Sub

#End Region

    Protected Sub btn_obtener_cuenta_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_obtener_cuenta.Click
        Dim mNroCuenta_AUX As String
        Dim mDominio_AUX As String

        Try

            'Obtengo el Nro de Cuenta en base a un Tipo de Cuenta y un dominio
            mNroCuenta_AUX = ""
            mDominio_AUX = Me.txtdominio.Text.Trim
            Me.txtnro_cuenta.Text = "Cargando ..."
            If (mDominio_AUX.ToString.Trim <> "") Then
                Me.mWS = New ws_consulta_tributaria.Service1


                'Busco la patente con todos caracteres juntos
                mNroCuenta_AUX = Me.mWS.ObtenerNroCuentaPorPatente(mDominio_AUX.ToString.Trim)


                'Si el dominio ingresado es de los nuevos busco con la cadena segmentada
                If (mNroCuenta_AUX.ToString.Trim = "0") And (mDominio_AUX.ToString.Trim.Length = 6) Then

                    'Busco la patente con un espacio
                    mDominio_AUX = txtdominio.Text.ToString.Trim.Substring(0, 3) & " " & txtdominio.Text.ToString.Trim.Substring(3, 3)
                    mNroCuenta_AUX = mWS.ObtenerNroCuentaPorPatente(mDominio_AUX.ToString.Trim)
                End If



                If (mNroCuenta_AUX.ToString = "0") Then
                    Me.lblmensaje.Text = "Dominio inexistente."
                    Me.txtnro_cuenta.Text = ""
                    Me.div_mensaje.Visible = True
                    Me.lblmensaje.Visible = True
                Else
                    Me.lblmensaje.Text = ""
                    Me.txtnro_cuenta.Text = mNroCuenta_AUX.ToString.Trim
                    Me.div_mensaje.Visible = False
                    Me.lblmensaje.Visible = False
                End If
                Me.txtcaptcha.Focus()
            Else
                Me.txtnro_cuenta.Text = ""
                Me.txtdominio.Focus()
            End If

        Catch ex As Exception
            Me.txtnro_cuenta.Text = ""
        Finally
            Me.mWS = Nothing
        End Try

    End Sub

    Protected Sub cmbtipo_cuentas1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmbtipo_cuentas.SelectedIndexChanged

        Me.txtnro_cuenta.Text = ""
        If (Me.cmbtipo_cuentas.Text = "Vehículo") Or (Me.cmbtipo_cuentas.Text = "Rodado") Then

            Me.txtdominio.Text = ""
            Me.txtdominio.Visible = True
            Me.id_tr_dominio.Visible = True
            Me.div_contenedor.Style.Add("height", "410")
            Me.txtdominio.Focus()
        Else
            Me.txtdominio.Visible = False
            Me.id_tr_dominio.Visible = False
            Me.div_contenedor.Style.Add("height", "365")
        End If
    End Sub



    Private Sub ConfigurarCaptcha()
        Dim mColor As Color
        Dim mConvertidor As New ColorConverter


        mColor = mConvertidor.ConvertFromString(System.Configuration.ConfigurationManager.AppSettings("FontColor").ToString.Trim)
        Me.verificador_captcha.FontColor = mColor

        mColor = mConvertidor.ConvertFromString(System.Configuration.ConfigurationManager.AppSettings("LineColor").ToString.Trim)
        Me.verificador_captcha.LineColor = mColor

        mColor = mConvertidor.ConvertFromString(System.Configuration.ConfigurationManager.AppSettings("NoiseColor").ToString.Trim)
        Me.verificador_captcha.NoiseColor = mColor


        Me.verificador_captcha.CaptchaLength = CInt(System.Configuration.ConfigurationManager.AppSettings("CaptchaLength").ToString.Trim)
        Me.verificador_captcha.CaptchaBackgroundNoise = CLng(System.Configuration.ConfigurationManager.AppSettings("CaptchaBackgroundNoise").ToString.Trim)
        Me.verificador_captcha.CaptchaLineNoise = CLng(System.Configuration.ConfigurationManager.AppSettings("CaptchaLineNoise").ToString.Trim)

    End Sub

    Private Function getVisitasCantidad() As String
        Dim sCantidad As String

        Try            
            Me.mWS = New ws_consulta_tributaria.Service1
            sCantidad = Me.mWS.getVisitasCantidad()
        Catch ex As Exception
            sCantidad = "0"
        Finally
            Me.mWS = Nothing
        End Try

        Return sCantidad.PadLeft(5, "0")
    End Function


    Private Sub agregarVisita()
        Dim arrVisita As String()

        Try
            Session("visita") = Format(Now, "dd/MM/yyyy") & "|" & Format(Now, "HH:mm:ss") & "|" & _
                                Me.cmbtipo_cuentas.Text.Trim & "|" & Me.txtnro_cuenta.Text.Trim
            Session("visita_editada") = False

            arrVisita = Session("visita").ToString.Trim.Split("|")

            Me.mWS = New ws_consulta_tributaria.Service1
            Me.mWS.agregarVisita(arrVisita(0).Trim, arrVisita(1).Trim, arrVisita(2).Trim, CLng(arrVisita(3)))
        Catch ex As Exception
        Finally
            Me.mWS = Nothing
        End Try

    End Sub


    Private Sub usaLoginAdmin()
        Dim boolUsaLoginAdmin As Boolean = False

        Try
            Me.mWS = New ws_consulta_tributaria.Service1
            'boolUsaLoginAdmin = CBool(Me.mWS.usaLoginAdmin())
        Catch ex As Exception
        Finally
            Me.mWS = Nothing
        End Try


        Me.label_saludo_admin.Visible = False
        Me.loginAdmin.Visible = False
        If (boolUsaLoginAdmin) Then
            If (Session("loginAdmin") IsNot Nothing) Then
                If (CBool(Session("loginAdmin"))) Then
                    Me.label_saludo_admin.Visible = True
                    Me.loginAdmin.Visible = False
                Else
                    Me.label_saludo_admin.Visible = False
                    Me.loginAdmin.Visible = True
                End If
            Else
                Me.label_saludo_admin.Visible = False
                Me.loginAdmin.Visible = True
            End If
        End If

    End Sub

    Private Sub CargarListbox()
        'Dim items As String
        'Try
        '    items = ""
        '    'Este fragmento de codigo pregunta que conexion tiene y en base a eso guarda el metodo corresp.
        '    If (Application("tipo_conexion").ToString = "FOX") Then
        '        Me.mWS = New ws_consulta_tributaria.Service1
        '        items = Me.mWS.getObtenerTiposImponible()
        '    ElseIf (Application("tipo_conexion").ToString = "RAFAM") Then
        '        Me.mWS = New ws_consulta_tributaria.Service1
        '        items = Me.mWS.ObtenerTiposImponible()
        '    End If

        '    If cmbtipo_cuentas.Items.Count = 0 Then 'Lee si el list esta vacio.
        '        If items.ToCharArray = "FALSE" Or items = "" Or items = "0" Then
        '            cmbtipo_cuentas.Items.Add("Inmueble")
        '            cmbtipo_cuentas.Items.Add("Comercio")
        '            'cmbtipo_cuentas.Items.Add("Contribuyente")
        '            cmbtipo_cuentas.Items.Add("Cementerio")
        '            If (Application("tipo_conexion").ToString = "FOX") Then
        '                cmbtipo_cuentas.Items.Add("Vehículo")
        '            ElseIf (Application("tipo_conexion").ToString = "RAFAM") Then
        '                cmbtipo_cuentas.Items.Add("Rodado")
        '            End If
        '        Else
        '            For n = 0 To Len(items) 'Carga listbox.
        '                Select Case items.Chars(n)
        '                    Case "I"
        '                        cmbtipo_cuentas.Items.Add("Inmueble")
        '                    Case "C"
        '                        cmbtipo_cuentas.Items.Add("Comercio")
        '                        'Case "N"
        '                        'cmbtipo_cuentas.Items.Add("Contribuyente")
        '                    Case "E"
        '                        cmbtipo_cuentas.Items.Add("Cementerio")
        '                    Case "V"
        '                        If (Application("tipo_conexion").ToString = "FOX") Then
        '                            cmbtipo_cuentas.Items.Add("Vehículo")
        '                        End If
        '                    Case "R"
        '                        If (Application("tipo_conexion").ToString = "RAFAM") Then
        '                            cmbtipo_cuentas.Items.Add("Rodado")
        '                        End If
        '                End Select
        '            Next n
        '        End If
        '    End If

        'Catch ex As Exception
        '    items = "0"
        'Finally
        '    Me.mWS = Nothing
        'End Try

    End Sub

End Class