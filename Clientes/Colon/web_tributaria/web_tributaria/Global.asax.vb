﻿Imports System.Web.SessionState
Imports web_tributaria.ws_consulta_tributaria.Service1

Public Class Global_asax
    Inherits System.Web.HttpApplication

    Private mWS As web_tributaria.ws_consulta_tributaria.Service1


    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Se desencadena al iniciar la aplicación

        Application("cant_usuarios") = 0

    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Se desencadena al iniciar la sesión
        Application("cant_usuarios") = Application("cant_usuarios") + 1
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Se desencadena al comienzo de cada solicitud
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Se desencadena al intentar autenticar el uso
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Se desencadena cuando se produce un error
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Se desencadena cuando finaliza la sesión
        Try

            If (Application("tipo_conexion").ToString = "RAFAM") Then
                Call LimpiarDatosSession()
            End If


        Catch ex As Exception
        Finally
            Application("cant_usuarios") = Application("cant_usuarios") - 1
        End Try

        'Response.Redirect("paginas/webfrmindex.aspx")
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Se desencadena cuando finaliza la aplicación
    End Sub

    Private Sub LimpiarDatosSession()
        Dim mNroUsuario As Integer

        If (Session("nro_usuario").ToString <> "") Then

            mNroUsuario = Session("nro_usuario")

            mWS = New ws_consulta_tributaria.Service1
            mWS.LimpiarDatosSession(mNroUsuario)
            mWS = Nothing
        End If
    End Sub

End Class