﻿Option Explicit On

Imports System.Drawing

Partial Public Class webfrmlogin_admin
    Inherits System.Web.UI.Page

#Region "Variables"

    Private mWS As web_tributaria.ws_consulta_tributaria.Service1
    Private mLog As Clslog
    Private mXml As String

#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call Me.ConfigurarCaptcha()

        If Not (IsPostBack) Then

            Me.lblmunicipalidad_name.Text = System.Configuration.ConfigurationManager.AppSettings("Nombre_Municipalidad").ToString.Trim
            Me.btnIngresar.Attributes.Add("onclick", "javascript:return validarDatos();")
            Me.btnIngresar.Style.Add("cursor", "pointer")

            Call Me.ConfigurarCaptcha()
            Me.lblversion.Text = "Versión " & ClsTools.mVersion.ToString.Trim
            Me.txtUsuario.Focus()
        End If
    End Sub


    Private Sub ConfigurarCaptcha()
        Dim mColor As Color
        Dim mConvertidor As New ColorConverter


        mColor = mConvertidor.ConvertFromString(System.Configuration.ConfigurationManager.AppSettings("FontColor").ToString.Trim)
        Me.verificador_captcha.FontColor = mColor

        mColor = mConvertidor.ConvertFromString(System.Configuration.ConfigurationManager.AppSettings("LineColor").ToString.Trim)
        Me.verificador_captcha.LineColor = mColor

        mColor = mConvertidor.ConvertFromString(System.Configuration.ConfigurationManager.AppSettings("NoiseColor").ToString.Trim)
        Me.verificador_captcha.NoiseColor = mColor


        Me.verificador_captcha.CaptchaLength = CInt(System.Configuration.ConfigurationManager.AppSettings("CaptchaLength").ToString.Trim)
        Me.verificador_captcha.CaptchaBackgroundNoise = CLng(System.Configuration.ConfigurationManager.AppSettings("CaptchaBackgroundNoise").ToString.Trim)
        Me.verificador_captcha.CaptchaLineNoise = CLng(System.Configuration.ConfigurationManager.AppSettings("CaptchaLineNoise").ToString.Trim)

    End Sub


    Private Function ObtenerDatosXML() As DataSet
        Dim mRutaFisica As String
        Dim mArchivo As String
        Dim dsDatos As DataSet

        Try

            mRutaFisica = Request.ServerVariables("APPL_PHYSICAL_PATH").ToString

            If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString & "datos_temporales")) Then
                My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString & "datos_temporales")
            End If

            mArchivo = mRutaFisica.ToString & "datos_temporales\" & Format(Now, "ddMMyyyyHHmmss") & ".xml"


            Dim mCrearXML As New IO.StreamWriter(mArchivo.ToString)
            mCrearXML.WriteLine(mXml.ToString)
            mCrearXML.Close()

            dsDatos = New DataSet
            dsDatos.ReadXml(mArchivo.ToString)

            My.Computer.FileSystem.DeleteFile(mArchivo.ToString)


            Return dsDatos
        Catch ex As Exception
            Me.mLog = New Clslog
            Me.mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            Me.mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)
            Return Nothing
        End Try
    End Function


#Region "LOG"

    Private Function ObtenerDetalleWEB() As String
        Dim mInfoLog As String = ""
        Try

            For Each mNombreVariable In Request.ServerVariables
                mInfoLog = mInfoLog & mNombreVariable.ToString & ": " & Request.ServerVariables(mNombreVariable.ToString)
                mInfoLog = mInfoLog & vbCrLf
            Next

            Return mInfoLog.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function
#End Region


    Private Function validarDatos()

        If (Me.txtusuario.Value.Trim() = "") Then
            Me.txtusuario.Value = ""
            Me.txtpassword.Value = ""
            Me.txtcaptcha.Value = ""
            Me.txtusuario.Focus()
            Return False
        End If

        If (Me.txtpassword.Value.Trim() = "") Then
            Me.txtusuario.Value = ""
            Me.txtpassword.Value = ""
            Me.txtcaptcha.Value = ""
            Me.txtpassword.Focus()
            Return False
        End If



        Me.lblmensaje.Text = ""
        Me.div_mensaje.Visible = False
        Me.lblmensaje.Visible = False
        verificador_captcha.ValidateCaptcha(Me.txtCaptcha.Value.Trim())
        If Not (verificador_captcha.UserValidated) Then
            Session("loginAdmin") = Nothing
            Me.lblmensaje.Text = "Código de verificación incorrecto."
            Me.txtcaptcha.Value = ""
            Me.txtcaptcha.Focus()

            Me.div_mensaje.Visible = True
            Me.lblmensaje.Visible = True
            Return False
        End If

        Return True
    End Function


 
    Protected Sub btnIngresar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnIngresar.Click
        Dim strUsuario As String = ""
        Dim strAcceso As String = ""
        Dim strUsuarioInput As String = ""
        Dim strAccesoInput As String = ""


        If (Me.validarDatos()) Then

            If (System.Configuration.ConfigurationManager.AppSettings("usaLogin") IsNot Nothing And System.Configuration.ConfigurationManager.AppSettings("admin") IsNot Nothing) Then
                strUsuario = System.Configuration.ConfigurationManager.AppSettings("usaLogin").ToString.Trim
                strAcceso = System.Configuration.ConfigurationManager.AppSettings("admin").ToString.Trim

                strUsuarioInput = ClsTools.GenerateHash(Me.txtusuario.Value.Trim())
                strAccesoInput = ClsTools.GenerateHash(Me.txtpassword.Value.Trim())


                If (strUsuario.Trim() = strUsuarioInput.Trim() And strAcceso.Trim() = strAccesoInput.Trim()) Then
                    Session("loginAdmin") = "true"
                    Response.Redirect("webfrmconsulta_b.aspx", False)
                Else
                    Session("loginAdmin") = Nothing
                    Me.txtusuario.Value = ""
                    Me.txtpassword.Value = ""
                    Me.txtcaptcha.Value = ""
                    Me.txtusuario.Focus()
                End If

            Else
                Session("loginAdmin") = Nothing
                Me.txtusuario.Value = ""
                Me.txtpassword.Value = ""
                Me.txtcaptcha.Value = ""
                Me.txtusuario.Focus()
            End If
        End If

    End Sub
End Class