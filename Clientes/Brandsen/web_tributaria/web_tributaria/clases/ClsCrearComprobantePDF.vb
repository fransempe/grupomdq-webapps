﻿Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO


'Web Services
Imports web_tributaria.ws_consulta_tributaria.Service1

Public Class ClsCrearComprobantePDF


#Region "Variables"

    Private Structure SColumnas
        Dim Recurso As Integer
        Dim Anio As Integer
        Dim Cuota As Integer
        Dim Concepto As Integer
        Dim ImporteOrigen As Integer
        Dim ImporteRecargo As Integer
        Dim ImporteMulta As Integer
        Dim ImporteTotal As Integer
    End Structure

    Private dsDatos As DataSet
    Private dtDatosRodado As DataTable
    Private mNombreMunicipalidad As String
    Private mRutaFisica As String
    Private mVistaDatos As DataView


    'Variables para Generar el LOGO, SOLO PARA RAFAM
    Private mTipoConexion As String
    Private mOrganismo As String
    Private mNombreOrganismo As String


    Private mCodigoMunicipalidad As String
    Private dsDatosMunicipalidad As DataSet


    'Esta variable solo la uso si el imponibles es Vehiculo
    Private mDominio As String

    Private mImprimeCodBar As String

    Private mWS As web_tributaria.ws_consulta_tributaria.Service1
#End Region

#Region "Propertys"

    Public Property Datos() As DataSet
        Get
            Return Me.dsDatos
        End Get
        Set(ByVal value As DataSet)
            Me.dsDatos = value
        End Set
    End Property

    Public WriteOnly Property RutaFisica() As String
        Set(ByVal value As String)
            Me.mRutaFisica = value.Trim
        End Set
    End Property

    Public WriteOnly Property TipoConexion() As String
        Set(ByVal value As String)
            Me.mTipoConexion = value
        End Set
    End Property

    Public WriteOnly Property Organismo() As String
        Set(ByVal value As String)
            Me.mOrganismo = value
        End Set
    End Property


    Public WriteOnly Property NombreOrganismo() As String
        Set(ByVal value As String)
            Me.mNombreOrganismo = value
        End Set
    End Property


    Public WriteOnly Property CodigoMunicipalidad() As String
        Set(ByVal value As String)
            Me.mCodigoMunicipalidad = value
        End Set
    End Property



    Public WriteOnly Property DatosMunicipalidad() As DataSet
        Set(ByVal value As DataSet)
            Me.dsDatosMunicipalidad = value
        End Set
    End Property

    Public WriteOnly Property DatosRodado() As DataTable
        Set(ByVal value As DataTable)
            dtDatosRodado = value
        End Set
    End Property


    Public WriteOnly Property Dominio() As String
        Set(ByVal value As String)
            Me.mDominio = value.Trim
        End Set
    End Property


#End Region


#Region "Constructor"

    Public Sub New()
        Me.dsDatos = Nothing
        Me.mNombreMunicipalidad = ""
        Me.mRutaFisica = ""
        Me.mVistaDatos = Nothing
    End Sub

    Dim strImprimeCodBar As String

#End Region




    Public Sub CrearPDF()
        Dim mDocumentoPDF As Document = New Document(iTextSharp.text.PageSize.A4, 15, 15, 50, 50)
        Dim writer As PdfWriter = PdfWriter.GetInstance(mDocumentoPDF, New System.IO.FileStream(mRutaFisica.ToString, System.IO.FileMode.Create))
        Dim Tipo_Imponible As String = dsDatos.Tables("imponible").Rows(0).Item("TIPOIMPONIBLE").ToString.Trim
        Dim mImprime As String = "S"

        'Pregunta si es Zárate, y guarda el parametro de imprimir o no el codigo de barra.
        If (System.Configuration.ConfigurationManager.AppSettings("Nombre_Municipalidad").ToString.Trim) = "Zárate" Then
            mWS = New ws_consulta_tributaria.Service1
            strImprimeCodBar = mWS.ObtenerParametroImprimeCodBar
        Else
            strImprimeCodBar = "S"
        End If

        writer.ViewerPreferences = PdfWriter.PageLayoutSinglePage
        mDocumentoPDF.Open()

        For i = 0 To dsDatos.Tables("renglones").Rows.Count - 1
            Call Me.CrearEncabezadoLogo(mDocumentoPDF, writer)
            Call Me.CrearEncabezado(mDocumentoPDF, writer)
            Call Me.TablaMovimientos(mDocumentoPDF, writer, i)
            Call Me.Talon(mDocumentoPDF, writer, i)

            'ESTE CONTROL ES PARA ZÁRATE, PIDIERON QUE PARA LOS COMERCIOS, SE PERMITA NO IMPRIMIR EL CODBAR
            'A LOS COMERCIOS MEDIANOS(M) Y GRANDES(G).
            If Tipo_Imponible = "C" Then
                If strImprimeCodBar = "N" Then
                    If dsDatos.Tables("imponible").Rows(0).Item("DATOVARIABLE3").ToString = "P" Then
                        mImprime = "S"
                    Else
                        mImprime = "N"
                    End If
                Else
                    mImprime = "S"
                End If
            Else
                mImprime = "S"
            End If

            If mImprime = "S" Then
                Call Me.CrearBarCode(mDocumentoPDF, writer, i)
            End If

            mDocumentoPDF.NewPage()
        Next


        If (CBool(System.Configuration.ConfigurationManager.AppSettings("CuadroDialogoImpresion").ToString.Trim)) Then

            ' Mostrar cuadro de díalogo de impresión después de algunos segundos.
            Dim strJavaScripts As String = "var res = app.setTimeOut('var pp = this.getPrintParams();pp.interactive = pp.constants.interactionLevel.full;this.print(pp);', 500);"

            ' Mostrar cuadro de díalogo de impresión después de algunos segundos.
            ' Dim jsTextNoWait As String = "var pp = this.getPrintParams();pp.interactive = pp.constants.interactionLevel.full;this.print(pp);"
            Dim javaScripts As PdfAction = PdfAction.JavaScript(strJavaScripts.Trim(), writer)
            writer.AddJavaScript(javaScripts)
        End If


        mDocumentoPDF.Close()
    End Sub

#Region "Crear PDF"

    Private Sub CrearEncabezadoLogo(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mLogo As Image
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte

        Dim mNombreLogo As String
        Dim mLogoHeight As Single
        Dim mLogoWidth As Single
        Dim mLogoCoordenadaX As Single
        Dim mLogoCoordenadaY As Single



        Dim mLogo_AUX As Byte()


        'Obtengo el Nombre del Logo segun la CONEXION y Seteo propiedades
        If (mTipoConexion.ToString = "RAFAM") Then
            mLogo_AUX = ClsTools.ObtenerLogo()
            mNombreLogo = "logo_rafam.jpg"
            mLogoHeight = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoHeight").ToString.Trim)
            mLogoWidth = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoWidth").ToString.Trim)
            mLogoCoordenadaX = CSng(System.Configuration.ConfigurationManager.AppSettings("Comprobante_LogoCoordenadaX").ToString.Trim)
            mLogoCoordenadaY = CSng(System.Configuration.ConfigurationManager.AppSettings("Comprobante_LogoCoordenadaY").ToString.Trim)
        Else


            'LOGO
            mLogo_AUX = ClsTools.ObtenerLogo()
            mNombreLogo = "logo.jpg"
            mLogoHeight = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoHeight").ToString.Trim)
            mLogoWidth = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoWidth").ToString.Trim)
            mLogoCoordenadaX = CSng(System.Configuration.ConfigurationManager.AppSettings("Comprobante_LogoCoordenadaX").ToString.Trim)
            mLogoCoordenadaY = CSng(System.Configuration.ConfigurationManager.AppSettings("Comprobante_LogoCoordenadaY").ToString.Trim)
        End If



        'Agrego el Logo a PDF
        Try

            mLogo = iTextSharp.text.Image.GetInstance(mLogo_AUX)

            mLogo.SetAbsolutePosition(mLogoCoordenadaX, mLogoCoordenadaY)
            mLogo.ScaleAbsolute(mLogoWidth, mLogoHeight)
            mDocumentoPDF.Add(mLogo)

        Catch ex As Exception
        End Try



        ''Nombre de La Municipalidad
        'If (mTipoConexion.ToString = "RAFAM") Then
        '    cb = writer.DirectContent
        '    cb.BeginText()
        '    mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        '    cb.SetFontAndSize(mFuente, 10)
        '    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mOrganismo.ToString, 105, 775, 0)
        '    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mNombreOrganismo.ToString, 105, 765, 0)
        '    cb.EndText()
        'End If




        'TITULO de la IMPRESION
        cb = writer.DirectContent
        cb.BeginText()


        'Datos de la municipalidad
        If (dsDatosMunicipalidad IsNot Nothing) Then


            'Pie del logo
            mFuente = FontFactory.GetFont(FontFactory.TIMES, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
            cb.SetFontAndSize(mFuente, 8)


            Me.mNombreMunicipalidad = ""
            Me.mNombreMunicipalidad = ClsTools.ObtenerNombreMunicipalidad()

            If (CBool(System.Configuration.ConfigurationManager.AppSettings("NombreMunicipalidad").ToString.Trim)) Then
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Municipalidad de " & Me.mNombreMunicipalidad.ToString.Trim, 75, 755, 0)
            End If

            If (CBool(System.Configuration.ConfigurationManager.AppSettings("ProvinciaBuenosAires").ToString.Trim)) Then
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Pcia. de Buenos Aires", 75, 745, 0)
            End If

            mFuente = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
            cb.SetFontAndSize(mFuente, 12)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Municipalidad de " & Me.mNombreMunicipalidad.ToString.Trim, 580, 781, 0)


            mFuente = FontFactory.GetFont(FontFactory.TIMES, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
            cb.SetFontAndSize(mFuente, 8)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_DIRECCION").ToString.Trim, 580, 769, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_TELEFONO").ToString.Trim, 580, 757, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_MAIL").ToString.Trim, 580, 745, 0)
        End If

        cb.EndText()
    End Sub

    Private Sub CrearEncabezado(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim mFuenteNegrita As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mValorY As Integer
        Dim etiqueta() As String

        cb = writer.DirectContent

        'Rectangulo Contenedora 
        cb.Rectangle(15, 675, 565, 62)




        mFuente = FontFactory.GetFont(FontFactory.TIMES_ITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        mFuenteNegrita = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 10)


        cb.BeginText()


        mValorY = 725
        cb.SetFontAndSize(mFuente, 10)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        If (CBool(System.Configuration.ConfigurationManager.AppSettings("CabeceraDatosCompletosContribuyente").ToString.Trim)) Then
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Titular: ", 20, mValorY, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatos.Tables("imponible").Rows(0).Item("TITULAR").ToString, 70, mValorY, 0)
        End If

        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Tipo de Cuenta: ", 330, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, ObtenerTipoCuenta(dsDatos.Tables("imponible").Rows(0).Item("TIPOIMPONIBLE").ToString.Trim), 405, mValorY, 0)


        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        If (CBool(System.Configuration.ConfigurationManager.AppSettings("CabeceraDatosCompletosContribuyente").ToString.Trim)) Then
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Domicilio: ", 20, mValorY, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatos.Tables("imponible").Rows(0).Item("DOMICILIO").ToString, 70, mValorY, 0)
        End If
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Nro. de Cuenta: ", 330, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatos.Tables("imponible").Rows(0).Item("NROIMPONIBLE").ToString, 405, mValorY, 0)

        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)

        If (System.Configuration.ConfigurationManager.AppSettings("InformacionImponible").ToString.Trim = True) Then
            Select Case ObtenerTipoCuenta(dsDatos.Tables("imponible").Rows(0).Item("TIPOIMPONIBLE").ToString.Trim)
                Case "INMUEBLE" : cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Inf. Catastral: ", 330, mValorY, 0)
                    cb.SetFontAndSize(mFuenteNegrita, 10)
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatos.Tables("imponible").Rows(0).Item("DATOVARIABLE").ToString, 405, mValorY, 0)
                Case "COMERCIO" : cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Comercio: ", 330, mValorY, 0)
                    cb.SetFontAndSize(mFuenteNegrita, 10)
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatos.Tables("imponible").Rows(0).Item("DATOVARIABLE").ToString, 405, mValorY, 0)
                Case "CEMENTERIO" : cb.SetFontAndSize(mFuenteNegrita, 10)
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatos.Tables("imponible").Rows(0).Item("DATOVARIABLE").ToString, 330, mValorY, 0)

            End Select
        End If

        'mValorY = mValorY - 15
        'cb.SetFontAndSize(mFuente, 10)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Localidad: ", 20, mValorY, 0)
        'cb.SetFontAndSize(mFuenteNegrita, 10)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatos.Tables("imponible").Rows(0).Item("LOCALIDAD").ToString, 70, mValorY, 0)


        'Si es un VEHÍCULO imprimo el dominio, marca y modelo FOX
        If (mTipoConexion.ToString = "FOX") And ObtenerTipoCuenta(dsDatos.Tables("imponible").Rows(0).Item("TIPOIMPONIBLE").ToString.Trim) = "VEHÍCULO" Then
            cb.SetFontAndSize(mFuente, 10)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Dominio: ", 330, mValorY, 0)
            cb.SetFontAndSize(mFuenteNegrita, 10)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatos.Tables("imponible").Rows(0).Item("DATOVARIABLE2").ToString.Trim, 405, mValorY, 0)
            mValorY = mValorY - 15
            cb.SetFontAndSize(mFuente, 10)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Marca y Modelo: ", 330, mValorY, 0)
            cb.SetFontAndSize(mFuenteNegrita, 10)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatos.Tables("imponible").Rows(0).Item("DATOVARIABLE").ToString.Trim, 405, mValorY, 0)
        End If


        'Si es un rodado imprimo el dominio, marca y modelo RAFAM
        If (dtDatosRodado IsNot Nothing) Then
            cb.SetFontAndSize(mFuente, 10)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Dominio: ", 330, mValorY, 0)
            cb.SetFontAndSize(mFuenteNegrita, 10)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dtDatosRodado.Rows(0).Item("DOMINIO").ToString.Trim, 405, mValorY, 0)

            mValorY = mValorY - 15
            cb.SetFontAndSize(mFuente, 10)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Marca y Modelo: ", 330, mValorY, 0)
            cb.SetFontAndSize(mFuenteNegrita, 10)

            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, _
                            dtDatosRodado.Rows(0).Item("MARCA").ToString.Trim & " " & _
                            dtDatosRodado.Rows(0).Item("MODELO").ToString.Trim, _
                            405, mValorY, 0)



        End If

        cb.EndText()

    End Sub

    Private Sub TablaMovimientos(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter, ByVal mIdComprobante As Integer)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mColumnas As SColumnas
        Dim mRenglon As Integer
        Dim NroVencimiento As String


        cb = writer.DirectContent


        'Rectangulo Contenedora 
        'cb.Rectangle(15, 200, 565, 470)
        cb.Rectangle(15, 250, 565, 420)


        'Linea de Encabezado
        cb.SetLineWidth(1)
        cb.MoveTo(15, 651)
        cb.LineTo(580, 651)


        'Linea de Encabezado 2
        cb.MoveTo(15, 633)
        cb.LineTo(580, 633)



        'Line de Vertical
        cb.SetLineWidth(1)
        'cb.MoveTo(300, 200)
        'cb.LineTo(300, 240)
        cb.MoveTo(300, 250)
        cb.LineTo(300, 290)


        'Linea superior
        'cb.MoveTo(300, 240)
        'cb.LineTo(580, 240)
        cb.MoveTo(300, 290)
        cb.LineTo(580, 290)



        'Linea Media
        'cb.MoveTo(300, 220)
        'cb.LineTo(580, 220)
        cb.MoveTo(300, 270)
        cb.LineTo(580, 270)
        cb.Stroke()




        mColumnas.Recurso = 25
        mColumnas.Anio = 240
        mColumnas.Cuota = 270
        mColumnas.Concepto = 300
        mColumnas.ImporteOrigen = 450
        mColumnas.ImporteRecargo = 520
        mColumnas.ImporteMulta = 550
        mColumnas.ImporteTotal = 570


        cb.BeginText()
        Dim fuente As iTextSharp.text.pdf.BaseFont


        mRenglon = 657


        'Filtro el DataSet
        mVistaDatos = New DataView(dsDatos.Tables("comprobante"))
        mVistaDatos.RowFilter = "Comprobante_Id = " & mIdComprobante.ToString


        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Nº Comprobante:", 80, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("NROCOMP").ToString, 150, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Fecha de Emisión:", 440, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, dsDatos.Tables("comprobante").Rows(0).Item("FECHA_EMISION").ToString, 500, mRenglon, 0)


        mRenglon = mRenglon - 17
        fuente = FontFactory.GetFont(FontFactory.TIMES_BOLD, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Recurso", mColumnas.Recurso, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Año", mColumnas.Anio, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Cuota", mColumnas.Cuota, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Concepto", mColumnas.Concepto, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Imp. Origen", mColumnas.ImporteOrigen, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Imp. Recargo", mColumnas.ImporteRecargo, mRenglon, 0)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Imp. Multa", mColumnas.ImporteMulta, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Imp. Total", mColumnas.ImporteTotal, mRenglon, 0)




        Dim i As Integer
        mRenglon = mRenglon - 20
        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(fuente, 8)





        'Filtro el DataSet
        mVistaDatos = New DataView(dsDatos.Tables("renglon"))
        mVistaDatos.RowFilter = "Renglones_Id = " & mIdComprobante.ToString



        If (mVistaDatos.Count > 0) Then
            For i = 0 To mVistaDatos.Count - 1
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mVistaDatos.Item(i).Row("RECURSO").ToString, mColumnas.Recurso, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(i).Row("ANIO").ToString, mColumnas.Anio, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(i).Row("CUOTA").ToString, mColumnas.Cuota, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mVistaDatos.Item(i).Row("CONCEPTO").ToString, mColumnas.Concepto, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, mVistaDatos.Item(i).Row("IMPORIGENRENG").ToString, mColumnas.ImporteOrigen, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, mVistaDatos.Item(i).Row("IMPRECARGOSRENG").ToString, mColumnas.ImporteRecargo, mRenglon, 0)
                cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, mVistaDatos.Item(i).Row("IMPTOTALRENG").ToString, mColumnas.ImporteTotal, mRenglon, 0)

                mRenglon = mRenglon - 15
            Next
        End If




        'Filtro el DataSet
        mVistaDatos = New DataView(dsDatos.Tables("comprobante"))
        mVistaDatos.RowFilter = "Comprobante_Id = " & mIdComprobante.ToString


        'Obtengo el valor a concatenar a las variables
        NroVencimiento = "_1"
        NroVencimiento = Me.ObtenerValorFechaVencimiento()


        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 10)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "VENCIMIENTO", 400, 225, 0)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO" & NroVencimiento.Trim).ToString, 400, 203, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "VENCIMIENTO", 400, 275, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO" & NroVencimiento.Trim).ToString, 400, 253, 0)

        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "TOTAL", 560, 275, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL" & NroVencimiento.Trim).ToString, 560, 253, 0)



        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(mFuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Contribuyente", 555, 240, 0)


        Dim fuenteNormal As iTextSharp.text.pdf.BaseFont = FontFactory.GetFont(FontFactory.TIMES_ITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        Dim fuenteNegrita As iTextSharp.text.pdf.BaseFont = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont

        'Lugares de Pago 
        If CBool((System.Configuration.ConfigurationManager.AppSettings("ImprimirLugaresDePago").ToString.Trim)) Then
            

            cb.SetFontAndSize(fuenteNegrita, 10)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Lugares de Pago:", 15, 220, 0)



            'Lugares de pago
            cb.SetFontAndSize(fuenteNormal, 10)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, System.Configuration.ConfigurationManager.AppSettings("LugaresDePago").ToString.Trim, 15, 210, 0)
        End If

        'Código Red Link
        If CBool((System.Configuration.ConfigurationManager.AppSettings("ImprimirCodigoRedLink").ToString.Trim)) Then

            cb.SetFontAndSize(fuenteNegrita, 10)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Código de Pago Electrónico:", 15, 195, 0)


            'Creo el código de pago de link
            Dim pagoLink As String = ""
            cb.SetFontAndSize(fuenteNormal, 10)
            pagoLink = getPagoLink(dsDatos.Tables("imponible").Rows(0).Item("TIPOIMPONIBLE").ToString.Trim, dsDatos.Tables("imponible").Rows(0).Item("NROIMPONIBLE").ToString)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, pagoLink, 15, 185, 0)
        End If

        cb.EndText()

    End Sub

    Private Sub Talon(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter, ByVal mIdComprobante As Integer)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim mFuenteNegrita As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte

        Dim NroVencimiento As String

        cb = writer.DirectContent



        'Rectangulo Contenedora 
        cb.Rectangle(15, 25, 300, 120)

        'Rectangulo Fecha Vencimiento
        cb.Rectangle(20, 105, 290, 20)


        'Rectangulos Importes
        cb.Rectangle(20, 70, 290, 28)


        'Line de Cuadro de Importes
        cb.SetLineWidth(1)
        cb.MoveTo(20, 83)
        cb.LineTo(310, 83)
        cb.Stroke()






        cb.BeginText()

        'Nombre de la Municipalidad 
        mFuente = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont

        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Municipalidad de " & Me.mNombreMunicipalidad.ToString.Trim, 15, 160, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Comprobante de Pago", 15, 148, 0)



        '------ Datos Rectangulo Talon --------
        'Asigno fuente
        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        mFuenteNegrita = FontFactory.GetFont(FontFactory.TIMES_BOLD, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(mFuente, 8)



        'Filtro el DataSet
        mVistaDatos = New DataView(dsDatos.Tables("comprobante"))
        mVistaDatos.RowFilter = "Comprobante_Id = " & mIdComprobante.ToString


        'Obtengo el valor a concatenar a las variables 
        NroVencimiento = "_1"
        NroVencimiento = Me.ObtenerValorFechaVencimiento()



        'Imprimo Datos
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Cuenta:", 20, 132, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatos.Tables("imponible").Rows(0).Item("TIPOIMPONIBLE").ToString.Trim & " - " & dsDatos.Tables("imponible").Rows(0).Item("NROIMPONIBLE").ToString, 50, 132, 0)

        If (mTipoConexion.ToString = "RAFAM") Then
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Nº Comprobante:", 180, 132, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("NROCOMP").ToString, 250, 132, 0)
        Else
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Nº Comprobante:", 220, 132, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("NROCOMP").ToString, 280, 132, 0)
        End If
        
        cb.SetFontAndSize(mFuenteNegrita, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Vencimiento:", 130, 113, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO" & NroVencimiento.Trim).ToString, 178, 113, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Origen", 50, 88, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Recargos", 150, 88, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Total", 290, 88, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_ORIGEN1").ToString, 50, 73, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_RECARGO" & NroVencimiento.Trim).ToString, 150, 73, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL" & NroVencimiento.Trim).ToString, 290, 73, 0)


        cb.SetFontAndSize(mFuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Banco", 305, 15, 0)
        cb.EndText()








        '----------------- CUADRO MUNICIPALIDAD -----------
        'Rectangulo Contenedora 
        cb.Rectangle(320, 25, 260, 120)

        'Rectangulo Fecha Vencimiento
        cb.Rectangle(325, 105, 250, 20)


        'Rectangulos Importes
        cb.Rectangle(325, 70, 250, 28)


        'Line de Cuadro de Importes
        cb.SetLineWidth(1)
        cb.MoveTo(325, 83)
        cb.LineTo(575, 83)
        cb.Stroke()






        cb.BeginText()

        'Nombre de la Municipalidad 
        mFuente = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Municipalidad de " & Me.mNombreMunicipalidad.ToString.Trim, 320, 160, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Comprobante de Pago", 320, 148, 0)



        '------ Datos Rectangulo Talon --------
        'Asigno fuente
        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(mFuente, 8)


        'Imprimo Datos
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Cuenta:", 325, 132, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, dsDatos.Tables("imponible").Rows(0).Item("TIPOIMPONIBLE").ToString.Trim & " - " & dsDatos.Tables("imponible").Rows(0).Item("NROIMPONIBLE").ToString, 355, 132, 0)

        If (mTipoConexion.ToString = "RAFAM") Then
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Nº Comprobante:", 445, 132, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("NROCOMP").ToString, 515, 132, 0)
        Else
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Nº Comprobante:", 485, 132, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("NROCOMP").ToString, 545, 132, 0)
        End If


        cb.SetFontAndSize(mFuenteNegrita, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Vencimiento:", 420, 113, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO" & NroVencimiento.Trim).ToString, 470, 113, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Origen", 350, 88, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Recargos", 450, 88, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Total", 550, 88, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL_ORIGEN1").ToString, 350, 73, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_RECARGO" & NroVencimiento.Trim).ToString, 450, 73, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, mVistaDatos.Item(0).Row("IMPORTE_TOTAL" & NroVencimiento.Trim).ToString, 550, 73, 0)



        cb.SetFontAndSize(mFuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Municipalidad", 555, 15, 0)
        cb.EndText()




    End Sub

    Private Sub CrearBarCode(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter, ByVal mIdComprobante As Integer)
        Dim mCelda As New iTextSharp.text.Cell
        Dim mTabla As New Table(2)
        Dim mCode39 As String
        Dim mCode39CoordenadaX As Single

        Dim cb As PdfContentByte
        cb = writer.DirectContent


        'Filtro el DataSet
        mVistaDatos = New DataView(dsDatos.Tables("comprobante"))
        mVistaDatos.RowFilter = "Comprobante_Id = " & mIdComprobante.ToString


            'Interlib 2of5
            Dim code25 As BarcodeInter25 = New BarcodeInter25
            code25.StartStopText = False
            code25.GenerateChecksum = False
            code25.Code = mVistaDatos.Item(0).Row("stringcodbarra").ToString
            Dim image25 As Image
            image25 = code25.CreateImageWithBarcode(cb, Nothing, Nothing)
            image25.SetAbsolutePosition(22, 30)
            mDocumentoPDF.Add(image25)


            'Code 39
            Dim code39 As New Barcode39
            Dim image39 As Image


            'Genero el Codigo de Barras para la Municipalidad segun la CONEXION
            If (mTipoConexion.ToString = "RAFAM") Then
                'Imprime codigo de barras corto con el codigo de municipalidad.
                'mCode39 = mCodigoMunicipalidad.ToString.Trim & _
                '          mVistaDatos.Item(0).Row("NROCOMP").ToString.Substring(0, 3) & _
                '          mVistaDatos.Item(0).Row("NROCOMP").ToString.Substring(4, 12) & _
                ' mVistaDatos.Item(0).Row("NROCOMP").ToString.Substring(17, 1)
                'Imprime codigo de barras corto sin el codigo de municipalidad.
                mCode39 = mVistaDatos.Item(0).Row("NROCOMP").ToString.Substring(0, 3) & _
                          mVistaDatos.Item(0).Row("NROCOMP").ToString.Substring(4, 12) & _
                 mVistaDatos.Item(0).Row("NROCOMP").ToString.Substring(17, 1)


                'Codigo Verificador del BARCODE39
                mCode39 = mCode39 & "3"
                mCode39CoordenadaX = 335
            Else
                mCode39 = mVistaDatos.Item(0).Row("stringcodbarra").ToString.Substring((mVistaDatos.Item(0).Row("stringcodbarra").ToString.Length - 14))
                mCode39CoordenadaX = 370
            End If




            code39.Code = mCode39.ToString
            code39.StartStopText = False
            cb = writer.DirectContent
            image39 = code39.CreateImageWithBarcode(cb, Nothing, Nothing)
            image39.SetAbsolutePosition(mCode39CoordenadaX, 30)
            mDocumentoPDF.Add(image39)

    End Sub

#End Region

    Private Function ObtenerTipoCuenta(ByVal mTipoCuenta As String) As String
        Select Case mTipoCuenta
            Case "I" : Return "INMUEBLE"
            Case "C" : Return "COMERCIO"
            Case "E", "O" : Return "CEMENTERIO"
            Case "V", "R" : Return "VEHÍCULO"
            Case Else : Return ""
        End Select
    End Function

    Private Function getEtiquetaByImponible(ByVal TipoCuenta As String) As String()
        Dim datos(1) As String

        Select Case TipoCuenta
            Case "I" : datos(0) = "Nomenclatura:" : datos(1) = 83
            Case "C" : datos(0) = "Nombre del comercio:" : datos(1) = 112
            Case "V", "R" : datos(0) = "Marca-Modelo-Año:" : datos(1) = 106
        End Select

        Return datos
    End Function





    'Obtengo el valor a concatenar en las variables que dependen de la fecha de vencimiento
    'Esto es justificado por la estrutura del xml que nos devuelve la dll de fox
    Private Function ObtenerValorFechaVencimiento() As String
        Dim NroVencimiento_Aux As String


        NroVencimiento_Aux = "_1"

        'Le asigno los valores al primer vencimiento
        If (mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_1").ToString <> "") Then
            If (CDate(mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_1").ToString) >= System.DateTime.Today) Then
                NroVencimiento_Aux = "_1"
            Else

                ' Le asigno los valores al segundo vencimiento
                If (mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString <> "") Then
                    If (CDate(mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_2").ToString) >= System.DateTime.Today()) Then
                        NroVencimiento_Aux = "_2"
                    Else

                        ' Le asigno los valores al tercer vencimiento
                        If (mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_3").ToString <> "") Then
                            If (CDate(mVistaDatos.Item(0).Row("FECHA_VENCIMIENTO_3").ToString) >= System.DateTime.Today()) Then
                                NroVencimiento_Aux = "_3"
                            End If
                        End If
                    End If
                End If
            End If
        End If

        Return NroVencimiento_Aux.Trim
    End Function



    'Método para generar el código de pago de link según el tipo y número de imponible 
    Private Function getPagoLink(ByVal tipoImp As String, ByVal nroImp As String) As String
        Dim codigo As String = ""
        If (System.Configuration.ConfigurationManager.AppSettings("Nombre_Municipalidad").ToString.Trim) = "Monte" Then
            Select Case tipoImp.ToUpper.Trim
                Case "I"
                    codigo = "1" & nroImp.PadLeft(6, "0")
                Case Else
                    codigo = "0000000"
            End Select
        Else
            Select Case tipoImp.ToUpper.Trim
                Case "I"
                    codigo = "1" & nroImp.PadLeft(10, "0")
                Case "C"
                    codigo = "2" & nroImp.PadLeft(10, "0")

                    'Rodados en RAFAM
                Case "R"
                    codigo = "3" & nroImp.PadLeft(10, "0")

                    'Vehículos en SIFIM
                Case "V"
                    codigo = "3" & nroImp.PadLeft(10, "0")
                Case Else
                    codigo = "00000000000"
            End Select
        End If

        Return codigo
    End Function

End Class
