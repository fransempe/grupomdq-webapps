﻿Option Explicit On
'Option Strict On

Imports web_tish.ws_tish.Service1

Partial Public Class webfrmaltaddjj
    Inherits System.Web.UI.Page

    Private mWS As web_tish.ws_tish.Service1
    Private ExData As New DataTable
    Private mLog As Clslog

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If (Session("musuario") Is Nothing) Or (Session("mnro_comercio") Is Nothing) Then
            Response.Redirect("webfrmlogin.aspx", False)
        Else


            'Si no es la primera vez que cargo la Pagina, no asigno el año
            If Not (IsPostBack) Then
                If (txtanio.Text <> Format(Now, "yyyy")) Then
                    txtanio.Text = Format(Now, "yyyy")
                    txtanio.Focus()
                End If

                cmb_periodo.SelectedIndex = 0
                lblfecha_presentacion.Text = Format(Now, "dd/MM/yyyy")

                Call CargarPeriodos()
            End If



            'Asigno el Evento LostFocus para la caja de texto
            If Not (Page.IsPostBack) Then
                'txtanio.Attributes.Add("onblur", "setTimeout('__doPostBack(\'txtanio\',\'\')', 0);")
                txtanio.Attributes.Add("onblur", "javascript:lost_focus();")
            Else





                If (DirectCast(DirectCast(sender, webfrmaltaddjj).ScriptManager1, System.Web.UI.ScriptManager).AsyncPostBackSourceElementID.ToString = "txtanio") Then
                    If Not (String.IsNullOrEmpty(txtanio.Text)) Then
                        Call CargarPeriodos()
                    End If
                End If
            End If



            'Asigno el Evento JavaScripts al boton para cargar las Actividades
            Link_cerrar_session.Attributes.Add("onclick", "javascript:return cerrar();")
            btnVerActividades.Attributes.Add("onclick", "javascript:Popup('webfrmcargaractividades.aspx',400,400);")
            Link_presentar_ddjj.Attributes.Add("onclick", "javascript:return presentar_ddjj();")
        End If
    End Sub

#Region "BOTONES"

    Protected Sub btnSeleccionartodos_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSeleccionartodos.Click
        Call SeleccionarTodos()
    End Sub

    Protected Sub btn_eliminar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_eliminar.Click
        Call EliminarSeleccionados()
    End Sub

    Protected Sub btncargar_categoria_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btncargar_categoria.Click
        If (ValidarCargaActividades()) Then
            Call CargarCategoria()
        End If
    End Sub

#End Region

#Region "SUB Y FUNCIONES"

    Private Function Valida() As Boolean

        'Año
        If Not (IsNumeric(txtanio.Text)) Then
            txtanio.Focus()
            Return False
        End If


        'Validar el Año Ingresado
        If (txtanio.Text > Format(Now, "yyyy")) Then
            txtanio.Focus()
            Return False
        End If



        'Periodo o CUOTA
        If Not (IsNumeric(cmb_periodo.SelectedValue = "Seleccionar Periodo")) Then
            cmb_periodo.Focus()
            Return False
        End If



        'Cant. empleados
        If Not (IsNumeric(txtcantempleados.Text)) Then
            txtcantempleados.Focus()
            Return False
        End If



        Return True
    End Function

    Private Sub CargarCategoria()
        Dim mDataTable As New DataTable
        Dim mFila As DataRow
        Dim i As Integer

        Try


            'Asigno las Columnas 
            mFila = mDataTable.NewRow
            mDataTable.Columns.Add("CODIGO")
            mDataTable.Columns.Add("DESCRIPCION")
            mDataTable.Columns.Add("IMPORTE")
            mDataTable.Columns.Add("ALICUOTA")



            'Guardo los datos que ya estan cargados en la grilla
            For i = 0 To gvDDJJ.Rows.Count - 1
                mFila = mDataTable.NewRow
                mFila("CODIGO") = gvDDJJ.Rows(i).Cells(0).Text
                mFila("DESCRIPCION") = gvDDJJ.Rows(i).Cells(1).Text
                mFila("IMPORTE") = Format(CDbl(gvDDJJ.Rows(i).Cells(2).Text), "N2")
                mFila("ALICUOTA") = gvDDJJ.Rows(i).Cells(3).Text
                mDataTable.Rows.Add(mFila)
            Next



            'Genero el nuevo registro
            mFila = mDataTable.NewRow
            mFila("CODIGO") = txtcodigo.Text
            mFila("DESCRIPCION") = txtdescripcion.Text
            mFila("IMPORTE") = Format(CDbl(txtimporte.Text), "N2")
            mFila("ALICUOTA") = txtalicuota.Text
            mDataTable.Rows.Add(mFila)




            'Actualizo la Grilla
            gvDDJJ.DataSource = mDataTable
            gvDDJJ.DataBind()




            If (gvDDJJ.Rows.Count <> 0) Then
                btnSeleccionartodos.Enabled = True
            Else
                btnSeleccionartodos.Enabled = False
            End If



            'Limpio los Campos
            txtcodigo.Text = ""
            txtdescripcion.Text = ""
            txtimporte.Text = ""
            txtalicuota.Text = ""


            'Calculo el Total Declarado
            Call CalcularImporteDeclarado()

        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, "CargarCategoria", "a")
            mLog = Nothing
        End Try
    End Sub

    Private Sub EliminarSeleccionados()
        Dim mDataTable As New DataTable
        Dim mFila As DataRow
        Dim i As Integer


        Try

            'Asigno las Columnas
            mFila = mDataTable.NewRow
            mDataTable.Columns.Add("CODIGO")
            mDataTable.Columns.Add("DESCRIPCION")
            mDataTable.Columns.Add("IMPORTE")
            mDataTable.Columns.Add("ALICUOTA")



            'Obtengo solo las filas que no estan seleccionadas
            For i = 0 To gvDDJJ.Rows.Count - 1
                If CType(gvDDJJ.Rows(i).Cells(4).Controls(1), CheckBox).Checked = False Then
                    mFila = mDataTable.NewRow
                    mFila("CODIGO") = gvDDJJ.Rows(i).Cells(0).Text
                    mFila("DESCRIPCION") = gvDDJJ.Rows(i).Cells(1).Text
                    mFila("IMPORTE") = Format(CDbl(gvDDJJ.Rows(i).Cells(2).Text), "N2")
                    mFila("ALICUOTA") = gvDDJJ.Rows(i).Cells(3).Text
                    mDataTable.Rows.Add(mFila)
                End If
            Next



            'Realizo el bindeo sin las filas seleccionadas
            gvDDJJ.DataSource = mDataTable
            gvDDJJ.DataBind()





            If (gvDDJJ.Rows.Count <> 0) Then
                btnSeleccionartodos.Enabled = True
            Else
                btnSeleccionartodos.Enabled = False
            End If


            'Calculo el Total Declarado
            Call CalcularImporteDeclarado()


        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, "CargarCategoria", "a")
            mLog = Nothing
        End Try

    End Sub

    Private Sub SeleccionarTodos()
        Dim i As Integer

        'Selecciono todas las Filas
        For i = 0 To gvDDJJ.Rows.Count - 1
            CType(Me.gvDDJJ.Rows(i).Cells(4).Controls(1), CheckBox).Checked = True
        Next
    End Sub



    Private Function CargarGrillaActividades() As DataSet
        Dim mDSActividades As DataSet
        Dim mDataTable As New DataTable
        Dim mFila As DataRow
        Dim i As Integer

        Try


            'Asigno las Columnas 
            mFila = mDataTable.NewRow
            mDataTable.Columns.Add("CODIGO")
            mDataTable.Columns.Add("IMPORTE")
            mDataTable.Columns.Add("ALICUOTA")



            'Guardo los datos que ya estan cargados en la grilla
            For i = 0 To gvDDJJ.Rows.Count - 1
                mFila = mDataTable.NewRow
                mFila("CODIGO") = gvDDJJ.Rows(i).Cells(0).Text
                mFila("IMPORTE") = Format(CDbl(gvDDJJ.Rows(i).Cells(2).Text), "N2")
                mFila("ALICUOTA") = gvDDJJ.Rows(i).Cells(3).Text
                mDataTable.Rows.Add(mFila)
            Next


            mDSActividades = New DataSet
            mDSActividades.Tables.Add(mDataTable)
            Return mDSActividades
        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, "CargarGrillaActividades", "a")
            mLog = Nothing

            Return Nothing
        End Try

    End Function


    Private Function ValidarCargaActividades() As Boolean
        Dim mPosicionActividad As Integer

        'Actividad Cargada
        If (txtcodigo.Text = "") Then
            btnVerActividades.Focus()
            Return False
        End If


        'Importe
        If Not (IsNumeric(txtimporte.Text)) Then
            txtimporte.Focus()
            Return False
        End If


        'Si la Actividad esta repetida, la remplazo o no la cargo
        mPosicionActividad = ActividadCargada(txtcodigo.Text, txtdescripcion.Text)
        If (mPosicionActividad <> -1) Then

            'Limpio los Campos
            txtcodigo.Text = ""
            txtdescripcion.Text = ""
            txtimporte.Text = ""
            txtalicuota.Text = ""
            lblMensajeItemsRepetido.Visible = True
            'RemplazarActividad(mPosicionActividad)
            Return False
        End If


        lblMensajeItemsRepetido.Visible = False
        Return True
    End Function

    Private Sub fBuscar(ByVal Texto As String)
        'Dim X As Integer
        'Dim Y As Integer
        'Dim MyData As New DataTable

        'MyData = gvActividades.DataSource.copy
        'MyData.Rows.Clear()


        'For X = 0 To ExData.Rows.Count - 1
        '    For Y = 0 To ExData.Columns.Count - 1
        '        If InStr(ExData.Rows(X).Item(Y).ToString.ToUpper, Texto.ToUpper, CompareMethod.Text) > 0 Then
        '            MyData.Rows.Add(ExData.Rows(X).ItemArray)
        '            Exit For
        '        End If
        '    Next Y
        'Next X


        'gvActividades.DataSource = MyData
        'gvActividades.DataBind()
    End Sub

#End Region

    Protected Sub Link_cerrar_session_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Link_cerrar_session.Click
        Session.RemoveAll()
        Session.Clear()
        Session.Abandon()
        Response.Redirect("webfrmlogin.aspx", False)
    End Sub

    Protected Sub cmb_periodo_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles cmb_periodo.SelectedIndexChanged
        Dim mFechaVencimiento As String
        Dim mPeriodo As String



        If (cmb_periodo.SelectedValue.ToString <> "Seleccionar Periodo") Then


            mPeriodo = cmb_periodo.SelectedValue.ToString.Replace("Periodo ", Nothing)


            mWS = New web_tish.ws_tish.Service1
            mFechaVencimiento = mWS.ObtenerFechaVencimiento(CInt(txtanio.Text), CInt(mPeriodo))
            mWS = Nothing


            If (mFechaVencimiento.ToString = "NULL") Then
                lblfecha_vencimiento.Text = "##/##/####"
                cmb_periodo.Focus()
            Else
                lblfecha_vencimiento.Text = mFechaVencimiento.ToString
                txtcantempleados.Focus()
            End If
        End If

    End Sub

    Private Function ValidarCargaPeriodos() As Boolean

        If Not (IsNumeric(txtanio.Text)) Then
            txtanio.Focus()
            Return False
        End If

        If (txtanio.Text.ToString.Length < 4) Then
            txtanio.Focus()
            Return False
        End If

        Return True
    End Function


    Private Function ActividadCargada(ByVal mCodigoActividad As String, ByVal mNombreActividad As String) As Integer
        Dim i As Integer

        Try

            'Busco si existe la Actividad por Cargar
            For i = 0 To gvDDJJ.Rows.Count - 1
                If (gvDDJJ.Rows(i).Cells(0).Text = mCodigoActividad.ToString) And (gvDDJJ.Rows(i).Cells(1).Text = mNombreActividad.ToString) Then
                    Return i
                End If
            Next


            Return -1
        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)


            Return -1
        End Try
    End Function


    Private Sub RemplazarActividad(ByVal mPosicion As Integer)
        'If (MsgBox("La Actividad seleccionada ya ha sido cargada con un Importe de $ " & Format(CDbl(gvDDJJ.Rows(mPosicion).Cells(2).Text), "N2") & vbCr & "¿Desea Reamplazarla?", MsgBoxStyle.YesNo, "Actividades") = MsgBoxResult.Yes) Then
        '    gvDDJJ.Rows(mPosicion).Cells(2).Text = Format(CDbl(txtimporte.Text), "N")
        'End If




        'Limpio los Campos
        txtcodigo.Text = ""
        txtdescripcion.Text = ""
        txtimporte.Text = ""
        txtalicuota.Text = ""



        'Calculo el Total Declarado
        Call CalcularImporteDeclarado()
    End Sub

    Private Sub CalcularImporteDeclarado()
        Dim i As Integer
        Dim mImporteDeclarado As Double

        mImporteDeclarado = 0
        For i = 0 To gvDDJJ.Rows.Count - 1
            mImporteDeclarado = mImporteDeclarado + CDbl(gvDDJJ.Rows(i).Cells(2).Text)
        Next


        'Mustro el Total a Declarar
        lblimporte_declarar.Text = Format(mImporteDeclarado, "N2")
    End Sub


    Private Sub CargarPeriodos()
        Dim mXML As String
        Dim dsPeriodos As DataSet

        If (ValidarCargaPeriodos()) Then
            mWS = New web_tish.ws_tish.Service1
            mXML = mWS.ObtenerPeriodosADeclarar(CInt(txtanio.Text))
            mWS = Nothing



            dsPeriodos = New DataSet
            dsPeriodos.ReadXml(New IO.StringReader(mXML))
            cmb_periodo.Items.Clear()


            cmb_periodo.Items.Add("Seleccionar Periodo")
            If (dsPeriodos.Tables.Count = 1) Then
                For i = 0 To dsPeriodos.Tables(0).Rows.Count - 1
                    cmb_periodo.Items.Add("Periodo " & dsPeriodos.Tables(0).Rows(i).Item("CUOTA").ToString)
                Next
            End If

            cmb_periodo.SelectedIndex = 0
            cmb_periodo.Focus()

        End If
    End Sub


#Region "LOG"

    Private Function ObtenerDetalleWEB() As String
        Dim mInfoLog As String = ""
        Try

            For Each mNombreVariable In Request.ServerVariables
                mInfoLog = mInfoLog & mNombreVariable.ToString & ": " & Request.ServerVariables(mNombreVariable.ToString)
                mInfoLog = mInfoLog & vbCrLf
            Next

            Return mInfoLog.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function
#End Region

    Protected Sub Link_presentar_ddjj_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Link_presentar_ddjj.Click
        Dim mOK As Boolean
        Dim mPeriodo As String
        

        mOK = False
        If (Valida()) Then

            mPeriodo = cmb_periodo.SelectedValue.ToString.Replace("Periodo ", Nothing)

            mWS = New web_tish.ws_tish.Service1
            mOK = mWS.AltaDDJJ(CInt(Session("mnro_comercio")), CInt(txtanio.Text), CInt(mPeriodo), Session("cuit").ToString, CDate(lblfecha_presentacion.Text), CInt(txtcantempleados.Text), CDbl(lblimporte_declarar.Text))
            mWS = Nothing
        Else
            Exit Sub
        End If



        'Aviso el Resultado de la Operacion
        If (mOK) Then
            Cache("grilla_actividades") = CargarGrillaActividades()
            Session("manio") = CInt(txtanio.Text).ToString
            Session("mperiodo") = mPeriodo.ToString

            Response.Redirect("comprobantes/webfrmcomprobante_ddjj.aspx", False)
        End If


    End Sub

End Class

