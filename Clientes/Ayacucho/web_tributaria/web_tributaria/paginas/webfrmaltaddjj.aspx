<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmaltaddjj.aspx.vb" Inherits="web_tish.webfrmaltaddjj" Explicit="true" Strict="true" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



<script src="../js/funciones.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
// <!CDATA[

    //Muestro las Actividades a Cargar    
    function Popup(mURL, mAncho, mAlto) {
        var mPosicion_x;
        var mPosicion_y;
        mPosicion_x=(screen.width/2)-(mAncho/2);
        mPosicion_y=(screen.height/2)-(mAlto/2);
        
               
        ventana_actividades = open(mURL,  "Actividades", "width="+mAncho+",height="+mAlto+",menubar=0,toolbar=0,directories=0,scrollbars=0,resizable=0,left="+mPosicion_x+",top="+mPosicion_y+"");
        window.document.formulario.txtimporte.focus();
        
    } 




    function validar_datos() {
        
    
        //Campo Vacio A�o
        if (control_vacio(window.document.getElementById('txtanio'))) {
            return false;   
        }
    
        //Solo Numeros A�o
        if (!(validar_cadena_solo_numeros(window.document.getElementById('txtanio')))) {
            return false;
        }
        
        // A�o de Cuatro Digitos
        if (window.document.formulario.txtanio.value.length != 4){
            alert("Debe Ingresar un A�o de Cuatro (4) digitos");
            window.document.formulario.txtanio.focus;
            window.document.formulario.txtanio.select();
            return false;
        }    
        
        
        
        //Campo Fecha Vencimiento
        if (window.document.getElementById('lblfecha_vencimiento').innerHTML = "##/##/####") {
            alert("Debe Seleccionar un Periodo con Fecha de Vencimiento");
            window.document.formulario.cmb_periodo.focus;
            return false;   
        }
    


        //Campo Vacio Empleados
        if (control_vacio(window.document.getElementById('txtcantempleados'))) {
            return false;   
        }
    
        //Solo Numeros Empleados
        if (!(validar_cadena_solo_numeros(window.document.getElementById('txtcantempleados')))) {
            return false;
        }
        
        
     
        
    return true;
    }


     function ServerSideAdd_CallBack(response){
		if (response.error != null){
			alert("Los valores introducidos no son v�lidos.");
			return;
		}
		target="contenido";
		document.getElementById(target).innerHTML = response.value;
	}




    //Pregunto al Usuario si confirma el cierre de session   
    function cerrar() {
        return cerrar_sesion_usuario();
    }


    function presentar_ddjj() {
        if (validar_datos()) {
            return confirm("Usted est� por presentar la Declaracion Jurada correspondiente al Per�odo " + window.document.formulario.cmb_periodo.value + " del A�o " + window.document.formulario.txtanio.value + " por un Monto de $ " + window.document.getElementById('lblimporte_declarar').innerHTML);
        } else {
            return false;
        }
    }


    function lost_focus(){

         if (window.document.formulario.txtanio.value.length != 4){
            alert("Debe Ingresar un A�o de Cuatro (4) digitos");             
            window.document.formulario.txtanio.focus();
            window.document.formulario.txtanio.select();
         } else {
            __doPostBack("txtanio",'');
        }    
        
        

    }




// ]]>
</script>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sistema TISH.-</title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />

<link href="../css/botonimagen.css" rel="stylesheet" type="text/css" />



        <style type="text/css">

            .style3
            {
                width: 234px;
            }
            .style4
            {
                width: 81px;
            }
            .style5
            {
                width: 75px;
            }
            .style6
            {
                width: 54px;
            }
            .columna
            {
                width: 100px;
                height: 19px;
               
            }
            .style35
            {
                width: 113px;
                height: 16px;
            }
            .style38
            {
                width: 122px;
            }
            .style39
            {
                width: 161px;
                height: 16px;
            }
            .style42
            {
                width: 105px;
            }
            .style43
            {
                width: 113px;
                height: 2px;
            }
            .style44
            {
                width: 161px;
                height: 2px;
            }
            </style>
        </head>

<body  onkeypress = "tecla(event)">
    <form id="formulario" runat="server">
<div class="main"><div id="header"><div class="top"><div class="title"><h1>Sistema 
    de Liquidacion de Seguridad e Higiene.</h1>
																					</div></div>
																					<div class="menu"><ul class="navbar">
		 <li><a href="#" class="nav1">ALTA DE DECLARACION JURADA</a></li>
</ul></div>
            <br />
            <div align="right">
                <asp:LinkButton ID="Link_presentar_ddjj" runat="server">[Presentar Declaracion Jurada]</asp:LinkButton>
            &nbsp;
                <asp:LinkButton ID="Link_cerrar_session" runat="server">[Cerrar Sesion]</asp:LinkButton>
            </div>
            <br />
</div>
<div class="content_altaddjj">
											<div align="center">
                                                <br />
                                                <br />
											<div style="float:left; width:740px; margin:10px 0; padding:0 10px; text-align:justify; height: 123px;" 
                                                align="center">
                                                <div align="center">
                                                    DATOS DEL COMERCIO<br />
                                                    <br />
                                                    <div align="center" style="height: 132px; width: 645px;">
                                                                    <div style="height: 97px; width: 633px;">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                
                                <table align="center" border="1" style="width: 100%; height: 97px;" 
                                                        frame="border">
                                                        <tr>
                                                            <td class="style43">
                                                                A�o:</td>
                                                            <td class="style43" align="center">
                                                                <asp:TextBox ID="txtanio" runat="server" Width="76px" MaxLength="4" 
                                                                    ToolTip="Ingrese un el A�o" Height="21px"></asp:TextBox>
                                                               
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                                                            <td class="style43" align="center">
                                                                Periodos:</td>
                                                            <td class="style44" align="center">
                                                                <asp:DropDownList ID="cmb_periodo" runat="server" AutoPostBack="True">
                                                                    <asp:ListItem Value="-1">Seleccionar Periodo</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="style38">
                                                                Fecha Presentacion:</td>
                                                            <td class="style38" align="center">
                                                                
                                                                <asp:Label ID="lblfecha_presentacion" runat="server" Font-Bold="True" 
                                                                    Text="01/01/2009"></asp:Label>
                                                                
                                                                </td>
                                                            <td class="style35" align="center">
                                                                Fecha Vencimiento:</td>
                                                            <td class="style39" align="center">
                                                                <asp:Label ID="lblfecha_vencimiento" runat="server" Font-Bold="True" 
                                                                    Text="##/##/####"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="style38">
                                                                Cant. Empleados  
                                                                <td class="style35" align="center">
                                                                <asp:TextBox ID="txtcantempleados" runat="server" Width="116px" Height="21px" 
                                                                        MaxLength="3" ToolTip="Ingrese la Cantidad de Empleados"></asp:TextBox>
                                                                </td>
                                                            </td>
                                                                <td class="style42" align="left">
                                                                    &nbsp;</td>
                                                                    
                                                                    
                                                                <td class="style35" align="left">
                                                                    &nbsp;</td>
                                                        </tr>
                                                        </table>
                                    <br />
                                    CARGAR ACTIVIDADES<br />
                                    <br />
                                    <div class="panel_login" style="width: 631px">
                                        <br />
                                        <asp:Button ID="btnVerActividades" runat="server" 
                                            Text="Ver Actividades a Cargar (F12)" BorderColor="#009933" Height="22px" 
                                            Width="201px" />
                                        <asp:Button ID="btnSeleccionartodos" runat="server" 
                                            Text="Seleccionar todos" Width="139px" Height="22px" 
                                            BorderColor="#009933" />
                                        <asp:Button ID="btn_eliminar" runat="server" Height="22px" Text="Eliminar Seleccionado(s)" 
                                            Width="168px" BorderColor="#009933" />
                                        <br />
                                        <br class="barra" />
                                    </div>
                                    <div class="barra">
                                    </div>
                                    <div style="height: 49px; width: 631px">
                                        <table border="1" style="width:100%; height: 32px;">
                                            <tr>
                                                <td class="style6">
                                                    CODIGO</td>
                                                <td class="style3">
                                                    DESCRIPCION</td>
                                                <td class="style4">
                                                    MONTO DECL.</td>
                                                <td class="style5">
                                                    ALICUOTA</td>
                                                <td>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="style6">
                                                    <asp:TextBox ID="txtcodigo" runat="server" Enabled="False" Width="50px"></asp:TextBox>
                                                </td>
                                                <td class="style3">
                                                    <asp:TextBox ID="txtdescripcion" runat="server" Enabled="False" Width="280px"></asp:TextBox>
                                                </td>
                                                <td class="style4">
                                                    <asp:TextBox ID="txtimporte" runat="server" Width="58px"></asp:TextBox>
                                                </td>
                                                <td class="style5">
                                                    <asp:TextBox ID="txtalicuota" runat="server" Enabled="False" Height="16px" 
                                                        Width="56px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btncargar_categoria" runat="server" Height="17px" 
                                                        Text="Confirmar" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <asp:GridView ID="gvDDJJ" runat="server" AutoGenerateColumns="False" 
                                        BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" 
                                        CellPadding="3" ForeColor="Black" GridLines="Vertical" Height="25px" 
                                        PageSize="3" Width="635px">
                                        <FooterStyle BackColor="#CCCCCC" />
                                        <Columns>
                                            <asp:BoundField DataField="CODIGO" HeaderText="CODIGO">
                                                <ItemStyle Width="30px" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DESCRIPCION" HeaderText="DESCRIPCION">
                                                <ItemStyle HorizontalAlign="Left" Width="200px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="IMPORTE" HeaderText="IMPORTE">
                                                <ItemStyle HorizontalAlign="Right" Width="40px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ALICUOTA" HeaderText="ALICUOTA">
                                                <ItemStyle HorizontalAlign="Center" Width="30px" />
                                            </asp:BoundField>
                                            <asp:templatefield HeaderText="SELECCIONAR" ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chk0" runat="server" />
                                                </ItemTemplate>
                                                <ItemStyle CssClass="colSeleccion" HorizontalAlign="Center" Width="15px" />
                                            </asp:templatefield>
                                        </Columns>
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="#CCCCCC" />
                                    </asp:GridView>
                                    <div class="barra">
                                    </div>
                                    <div style="height: 24px">
                                        <asp:Label ID="lblMensajeItemsRepetido" runat="server" Font-Bold="True" 
                                            ForeColor="#CC0000" 
                                            Text="LA ACTIVIDAD SELECCIONADA YA EXISTE EN ESTA DECLARACION." Visible="False"></asp:Label>
                                    </div>
                                    <div align="center" style="height: 17px">
                                        &nbsp;&nbsp; Monto Total Declarado: $&nbsp;
                                        <asp:Label ID="lblimporte_declarar" runat="server" Font-Bold="True" Text="0.00"></asp:Label>
                                    </div>
                                    
                                    <div align="center">
                                        &nbsp;&nbsp;
                                        </div>
                                    
                                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                                        <Services>
                                            <asp:ServiceReference Path ="http://localhost:2201/servicio_tish.asmx" />
                                        </Services> 
                                    </asp:ScriptManager>
                                    
                                    
                                    <br />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                                                                        <br />
                                                                        <br />
                                                                        <br />
                                                                    </div>
                                                                </div>
                                                            </div>
											</div>
                                            </div>
											</div>
        <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
        <br />
    <br />
</div>
    </form>
<div id="footer"><div class="footer"><div class="cp1">Template Name � 2006 &quot;&gt; Design 
    by 
    <a href="http://www.grupomdq.com/">Grupo MDQ.-</a></div></div></div>
        </body>
</html>
