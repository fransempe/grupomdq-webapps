﻿Option Explicit On
Option Strict On

Imports WEB_TISH.WS_TISH.Service1

Partial Public Class webfrmlistadoperiodos
    Inherits System.Web.UI.Page

    Private mWS As WEB_TISH.WS_TISH.Service1
    Private mLog As Clslog

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mDataSet As DataSet
        Dim mXML As String

        Try


            If Not (IsPostBack) Then

                If (Session("mnro_comercio") Is Nothing) Then
                    Response.Redirect("webfrmlogin.aspx", False)
                Else

                    mWS = New web_tish.ws_tish.Service1
                    mXML = mWS.ObtenerDatosContribuyenteYCTACTE(CInt(Session("mnro_comercio")))
                    mWS = Nothing


                    mDataSet = New DataSet
                    mDataSet.ReadXml(New IO.StringReader(mXML))



                    lblNroComercio.Text = mDataSet.Tables("DatosContribuyente").Rows(0).Item("NRO_COMERCIO").ToString
                    lblNombreComercio.Text = mDataSet.Tables("DatosContribuyente").Rows(0).Item("NOMBRE_COMERCIO").ToString
                    lblContribuyente.Text = mDataSet.Tables("DatosContribuyente").Rows(0).Item("CONTRIBUYENTE").ToString
                    lbldireccion.Text = mDataSet.Tables("DatosContribuyente").Rows(0).Item("Domicilio").ToString
                    lblcuit.Text = mDataSet.Tables("DatosContribuyente").Rows(0).Item("CUIT").ToString
                    lblsituacion_iva.Text = mDataSet.Tables("DatosContribuyente").Rows(0).Item("CODIGO_SITUACION_IVA").ToString



                    gvPeriodos.DataSource = mDataSet.Tables(1)
                    gvPeriodos.DataBind()


                    Call CalcularTotalDeuda(mDataSet)
                    Call RefrescarLinkEstado()


                    Session("cuit") = lblcuit.Text.ToString.Replace("-", Nothing)
                    If (mDataSet.Tables.Count = 2) Then
                        Session("dsgrilla") = mDataSet.Tables(1)
                    End If
                End If

            End If


            link_cerrar_session.Attributes.Add("onclick", "javascript:return cerrar();")
        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)
        End Try
    End Sub

    Private Sub gvDDJJ_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvPeriodos.PageIndexChanging
        Dim ds As New DataSet
        gvPeriodos.PageIndex = e.NewPageIndex
        gvPeriodos.DataSource = Session("dsgrilla")
        gvPeriodos.DataBind()

        Call RefrescarLinkEstado()
    End Sub

    Protected Sub Link_cerrar_session_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_cerrar_session.Click
        Session.RemoveAll()
        Session.Clear()
        Session.Abandon()
        Response.Redirect("webfrmlogin.aspx", False)
    End Sub

    Private Sub gvDDJJ_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvPeriodos.RowCommand
        Dim mMensaje As String
        Dim mFila As Integer


        '    mFila = Convert.ToInt32(e.CommandArgument)
        '    mMensaje = "Usted esta por realizar el Pago correspondiente al Período " & gvPeriodos.Rows(mFila).Cells(1).Text & _
        '               " del Año " & gvPeriodos.Rows(mFila).Cells(0).Text & " por un Monto de " & gvPeriodos.Rows(mFila).Cells(7).Text & vbCr & _
        '               "¿Desea Continuar?"



        '    If (MsgBox(mMensaje.ToString, MsgBoxStyle.YesNo, "SISTEMA TISH") = MsgBoxResult.Yes) Then
        If (e.CommandName.ToString = "Select") Then
            Response.Redirect("comprobantes/webfrmcomprobante_pago.aspx?manio=" & gvPeriodos.Rows(mFila).Cells(0).Text & "&mperiodo=" & gvPeriodos.Rows(mFila).Cells(1).Text & "&mfechavencimiento=" & gvPeriodos.Rows(mFila).Cells(2).Text, False)
        End If
        'End If

    End Sub


    Private Sub CalcularTotalDeuda(ByVal dsGRilla As DataSet)
        Dim i As Integer
        Dim mImporteDeudaAux As Double
        Dim mImporteDeuda As Double


        mImporteDeuda = 0
        For i = 0 To dsGRilla.Tables(1).Rows.Count - 1
            mImporteDeudaAux = CDbl(dsGRilla.Tables(1).Rows(i).Item("IMPORTE_TOTAL").ToString.Replace("$", Nothing).Trim)
            mImporteDeuda = mImporteDeuda + mImporteDeudaAux
        Next


        ' Importe DEUDA
        If (mImporteDeuda <> 0) Then
            lbltotal_deuda.ForeColor = Drawing.Color.Red
        Else
            lbltotal_deuda.ForeColor = Drawing.Color.Black
        End If
        lbltotal_deuda.Text = Format(mImporteDeuda, "N2")
    End Sub

    Private Sub RefrescarLinkEstado()
        Dim i As Integer

        For i = 0 To gvPeriodos.Rows.Count - 1
            If (gvPeriodos.Rows(i).Cells(8).Text = "PAGA") Then
                gvPeriodos.Rows(i).Cells(9).Text = ""
            End If
        Next
    End Sub


#Region "LOG"

    Private Function ObtenerDetalleWEB() As String
        Dim mInfoLog As String = ""
        Try

            For Each mNombreVariable In Request.ServerVariables
                mInfoLog = mInfoLog & mNombreVariable.ToString & ": " & Request.ServerVariables(mNombreVariable.ToString)
                mInfoLog = mInfoLog & vbCrLf
            Next

            Return mInfoLog.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function
#End Region

    Protected Sub link_informe_cta_cte_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_informe_cta_cte.Click
        Session("datos_contribuyente") = lblContribuyente.Text & "|" & _
                                         lbldireccion.Text & "|" & _
                                         "Mar del Plata |" & _
                                         lblcuit.Text & "|" & _
                                         lblsituacion_iva.Text & "|" & _
                                         lblNroComercio.Text & "|" & _
                                         lblNombreComercio.Text


        Response.Redirect("comprobantes/webfrminformectacte.aspx", False)
    End Sub

    
    Protected Sub link_alta_ddjj_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_alta_ddjj.Click
        Response.Redirect("webfrmaltaddjj.aspx", False)
    End Sub

    Private Sub gvPeriodos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPeriodos.RowDataBound
        Dim mMensaje As String


        'Genero el Mensaje
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            mMensaje = "Usted esta por realizar el Pago correspondiente al Período " & e.Row.Cells(1).Text & _
                       " del Año " & e.Row.Cells(0).Text & " por un Monto de " & e.Row.Cells(7).Text & vbCr & _
                       "¿Desea Continuar?"


            e.Row.Cells(9).Attributes.Add("onclick", "javascript:return confirm('" & mMensaje.ToString & "');")
        End If

    End Sub

End Class