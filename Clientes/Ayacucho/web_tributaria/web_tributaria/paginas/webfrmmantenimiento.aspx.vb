﻿Option Explicit On
Option Strict On

Partial Public Class webfrmmantenimiento
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mNombreMunicipalidad As String
        mNombreMunicipalidad = ""
        mNombreMunicipalidad = System.Configuration.ConfigurationManager.AppSettings("Nombre_Municipalidad").ToString.Trim()

        lblNombreMunicipalidad.Text = "Municipalidad de " & mNombreMunicipalidad.ToString.Trim
    End Sub

End Class