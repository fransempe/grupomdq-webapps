<%@ Page Language="vb" AutoEventWireup="true" CodeBehind="webfrmlistado.aspx.vb" Inherits="web_tributaria.webfrmlistado" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >



<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

<link  href="../css/generica.css" rel="stylesheet" type="text/css" />
<link  href="../css/botoneslistado.css" rel="stylesheet" type="text/css" />

    <script src="../tooltip/boxover.js" type="text/javascript"></script>     
    <script src="../js/funciones.js" type="text/javascript"></script>
    
      


    <link href="cssUpdateProgress.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';         
    </script>
        
    <script type="text/javascript">




        /*Esta Funcion Asigna Efectos a las Grillas */
        function efecto_grilla(id_grilla) {
            var grilla = document.getElementById(id_grilla);
            if (grilla != null) {
                for (i = 0; fila = grilla.getElementsByTagName('td')[i]; i++) {
                    fila.onmouseover = function() { iluminar(this, true) }
                    fila.onmouseout = function() { iluminar(this, false) }
                }
            }
        }


        /*Esta Funcion Asigna el EFECTO de CAMBIAR DE COLOR */
        function iluminar(obj, valor) {
            var fila = obj.parentNode;
            for (i = 0; filas = fila.getElementsByTagName('td')[i]; i++)
                filas.style.background = (valor) ? 'gray' : '';
        }


        /*Esta Funcion MARCA O DESMARCA todos los registros de una GRILLA */
        function estado_seleccion_check(id_grilla, estado) {
            var respuesta = true;
            var nombre = ""

            if (estado == false) {
                respuesta = confirm('Esta acci�n eliminara cualquier registro que Usted tenga seleccionado.\n�Desea continuar? ');
            }

            if (respuesta == true) {
                for (i = 0; i < document.forms[0].elements.length; i++) {
                    nombre = document.forms[0].elements[i].id;


                    if (nombre.indexOf(id_grilla) != -1) {
                        document.forms[0].elements[i].checked = estado;
                    }

                }
            }

            calcular_importe(id_grilla);
        }





        /* Esta Funcion Valida los Datos antes de Enviarlos al SERVIDOR */
        function validar(id_grilla) {
            var ok;

            if (!importe_total_seleccionado(id_grilla)) {
                return false
            }



	        if (document.getElementById('lblmunicipalidad_name').innerHTML == 'Azul') {              
                if (!movimientos_validos()) {
                    return false;
                }
            }




            /* Validacion Solo Utilizada con Conexion a RAFAM */
            if (id_grilla == 'gvperiodos_vencidos') {
                if (document.getElementById('lbltipo_conexion').innerHTML == 'RAFAM') {
                    ok = seleccion_recursos(id_grilla);

                    if (!ok) {
                        alert('Las cuotas seleccionadas corresponden a distintos recursos.\n' +
                              'Para poder emitir un comprobante de pago se debe seleccionar cuotas de un �nico recurso.');
                        return false;
                    }
                }
            }


            if (registros_seleccionados(id_grilla)) {
                return confirm("Usted est� a punto de emitir un comprobante.\n"
                               + "Para el correcto funcionamiento se recomienda descargar la �ltima versi�n de Adobe Reader.\n"
                               + "�Desea continuar?");

            } else {
                alert("Debe seleccionar al menos un comprobante a imprimir.");
                return false;
            }




        }


        /* Esta Funcion Valida que no se SELECCIONE registros de mas de un RECURSO */
        /* Esta Funcion SOLO es UTILIZADA cuando se CONECTA A RAFAM */
        function seleccion_recursos(id_grilla) {
            var numero;
            var j;
            var recurso;
            var recurso_aux;
            var primer_recurso_asignado;


            primer_recurso_asignado = false;
            for (var i = 1; i < document.getElementById(id_grilla).rows.length; i++) {

                j = i + 1;
                if (j > 9) {
                    numero = j;
                } else {
                    numero = j;
                }


                var control_check = document.getElementById(id_grilla + '_ctl' + numero + '_CheckBox1');
                if (control_check != null) {
                    if (control_check.checked) {
                        if (!primer_recurso_asignado) {
                            recurso = document.getElementById(id_grilla).rows[i].cells[1].innerHTML;
                            primer_recurso_asignado = true;
                        } else {
                            recurso_aux = document.getElementById(id_grilla).rows[i].cells[1].innerHTML;
                            if (recurso != recurso_aux) {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }






        /* Esta Funcion Valida que el IMPORTE TOTAL SELECCIONADO sea MAYOR A CERO */
        function importe_total_seleccionado(id_grilla) {
            var nombre_label;
            var dato;


            switch (id_grilla) {
                case 'gvperiodos_vencidos': nombre_label = 'lbltotal_vencidos'; break;
                case 'gvperiodos_no_vencidos': nombre_label = 'lbltotal_no_vencidos'; break;
                case 'gvperiodos_otros_comprobantes': nombre_label = 'lbltotal_otros_comprobantes'; break;
            }


            if (document.getElementById(nombre_label).innerHTML.indexOf('-') != -1) {
                alert('Emision de Comprobantes:\nPara poder emitir un comprobante de pago el Importe Total Seleccionado debe ser mayor a cero(0).');
                return false;
            }

            return true;
        }


        /* Esta Funcion Cambia el NOMBRE de PANEL de Comprobantes VENCIDOS */
        function estado_grilla_vencidos() {
            var control = document.getElementById('div_contenedor_movimientos_vencidos');

            if (document.getElementById('id_barra_documentos_vencidos').innerHTML == " 1) PERIODOS ADEUDADOS VENCIDOS (OCULTAR)") {
                control.style.display = 'none';
                document.getElementById('id_barra_documentos_vencidos').innerHTML = " 1) PERIODOS ADEUDADOS VENCIDOS (VER)";
            } else {
                control.style.display = 'block';
                document.getElementById('id_barra_documentos_vencidos').innerHTML = " 1) PERIODOS ADEUDADOS VENCIDOS (OCULTAR)";
            }
        }


        /* Esta Funcion Cambia el NOMBRE de PANEL de Comprobantes NO VENCIDOS */
        function estado_grilla_no_vencidos() {
            var control = document.getElementById('div_grilla_no_vencidos');

            if (document.getElementById('id_barra_documentos_no_vencidos').innerHTML == " 2) COMPROBANTES A VENCER EN LOS PROXIMOS 60 DIAS (OCULTAR)") {
                control.style.display = 'none';
                document.getElementById('id_barra_documentos_no_vencidos').innerHTML = " 2) COMPROBANTES A VENCER EN LOS PROXIMOS 60 DIAS (VER)";
            } else {
                control.style.display = 'block';
                document.getElementById('id_barra_documentos_no_vencidos').innerHTML = " 2) COMPROBANTES A VENCER EN LOS PROXIMOS 60 DIAS (OCULTAR)";
            }
        }


        /* Esta Funcion Cambia el NOMBRE de PANEL de OTROS Comprobantes */
        function estado_grilla_otros_comprobantes() {
            var control = document.getElementById('div_grilla_otros_comprobantes');

            if (document.getElementById('id_barra_documentos_otros_comprobantes').innerHTML == " 3) OTROS COMPROBANTES A VENCER (OCULTAR)") {
                control.style.display = 'none';
                document.getElementById('id_barra_documentos_otros_comprobantes').innerHTML = " 3) OTROS COMPROBANTES A VENCER (VER)";
            } else {
                control.style.display = 'block';
                document.getElementById('id_barra_documentos_otros_comprobantes').innerHTML = " 3) OTROS COMPROBANTES A VENCER (OCULTAR)";
            }
        }


        /* Esta Funcion Cambia el TITULO del TOOLS TIP */
        function obtener_mensaje_tooltips() {
            var control = document.getElementById('div_contenedor_movimientos_vencidos');

            if (document.getElementById('id_barra_documentos_vencidos').innerHTML == " 1) PERIODOS ADEUDADOS VENCIDOS (OCULTAR)") {
                return "PARA VER";
            } else {
                return "PARA OCULTAR";
            }
        }


        /* Esta Funcion SETEA TODAS los CONTROLES al CARGAR la PAGINA  */
        function iniciar_pagina() {
            var control;
            var label;

        
            //Bandera Tipo Tipo Conexion
            control = document.getElementById('lbltipo_conexion');
            control.style.display = 'none';
            
            
            //Panel de registros no Vencidos
            control = document.getElementById('div_contenedor_movimientos_vencidos');
            control.style.display = 'none';
            document.getElementById('id_barra_documentos_no_vencidos').innerHTML = " 1) PERIODOS ADEUDADOS VENCIDOS (VER)";

            //Panel de registros no Vencidos
            control = document.getElementById('div_grilla_no_vencidos');
            control.style.display = 'none';
            document.getElementById('id_barra_documentos_no_vencidos').innerHTML = " 2) COMPROBANTES A VENCER EN LOS PROXIMOS 60 DIAS (VER)";


            //Panel de Otros registros 
            control.style.display = 'none';
            document.getElementById('div_grilla_otros_comprobantes').style.display = 'none';
            document.getElementById('id_barra_documentos_otros_comprobantes').innerHTML = " 3) OTROS COMPROBANTES A VENCER (VER)";



            //Efecto de las grillas 
            efecto_grilla('gvperiodos_vencidos');
            efecto_grilla('gvperiodos_no_vencidos');
            efecto_grilla('gvperiodos_otros_comprobantes');
            estado_seleccion_check_inicial('gvperiodos_vencidos');
            estado_seleccion_check_inicial('gvperiodos_no_vencidos');
            estado_seleccion_check_inicial('gvperiodos_otros_comprobantes');



            //Menejo de Paneles
            var panel = document.getElementById('boton_Imprimir_comprob_vencidos');
            if (panel == null) {
                document.getElementById('div_mensaje_vencidos').style.display = 'none';
                document.getElementById('div_datos_vencidos').style.display = 'none';
                document.getElementById('grilla').style.display = 'none';
                document.getElementById('div_tablas_info').style.display = 'none';
                document.getElementById('div_datos_vencidos').style.display = 'none';

            } else {
                label = document.getElementById('lblcantidad_no_seleccionados_vencidos');
                label.innerHTML = (document.getElementById('gvperiodos_vencidos').rows.length - 1)
                document.getElementById('div_sin_registros_vencidos').style.display = 'none';
            }


            panel = document.getElementById('boton_Imprimir_comprob_no_vencidos');
            if (panel == null) {
                document.getElementById('div_datos_no_vencidos').style.display = 'none';
                document.getElementById('div_grilla_no_vencidos2').style.display = 'none';
                document.getElementById('titulo_grilla_no_vencidos').style.display = 'none';
            } else {
                label = document.getElementById('lblcantidad_no_seleccionados_no_vencidos');
                label.innerHTML = (document.getElementById('gvperiodos_no_vencidos').rows.length - 1);
                document.getElementById('div_sin_registros_no_vencidos').style.display = 'none';

                //Calculo los totales para la grilla
                calcular_totales_por_grilla('gvperiodos_no_vencidos')
            }



            panel = document.getElementById('boton_Imprimir_comprob_otros_comprobantes');
            if (panel == null) {
                document.getElementById('div_datos_otros_comprobantes').style.display = 'none';
                document.getElementById('div_grilla_otros_comprobantes').style.display = 'none';
                document.getElementById('titulo_grilla_otros_comprobantes').style.display = 'none';
            } else {
                label = document.getElementById('lblcantidad_no_seleccionados_otros_comprobantes');
                label.innerHTML = (document.getElementById('gvperiodos_otros_comprobantes').rows.length - 1);
                document.getElementById('div_sin_registros_otros_comprobantes').style.display = 'none';

                //Calculo los totales para la grilla
                calcular_totales_por_grilla('gvperiodos_otros_comprobantes')
            }



            //Pinto los movimientos que estan en juicio y Obtengo los Totales de la Grilla Movimientos Vencidos
            movimientos_juicio('gvperiodos_vencidos');


            //Obtengo la Cantidad de registros por grilla
            document.getElementById('lblcantidad_de_registros').innerHTML = cantidad_registros('gvperiodos_vencidos');
            document.getElementById('lblcantidad_de_registros_no_vencidos').innerHTML = cantidad_registros('gvperiodos_no_vencidos');
            document.getElementById('lblcantidad_de_registros_otros_comprobantes').innerHTML = cantidad_registros('gvperiodos_otros_comprobantes');

        }



        function evento_click_check(id_grilla, tipo_y_nrocomprobante, fila) {

            /* Funcion Solo Utilizada con Conexion a RAFAM */
//            if (document.getElementById('lbltipo_conexion').innerHTML == 'RAFAM') {
                marcar_plan(id_grilla, tipo_y_nrocomprobante, fila);
//            }



            marcar_compDeMigracion(id_grilla, tipo_y_nrocomprobante, fila);

            /* Calculo el Importe Total del los Comprobantes Seleccionados */
            calcular_importe(id_grilla);
        }




        /* Esta Funcion CALCULA el IMPORTE de los COMPROBANTES SELECCIONADOS */
        function calcular_importe(id_grilla) {
            var total = 0;
            var total_aux = 0;
            var numero;
            var j;
            var celda_importe = 0;
            var cant_seleccionados = 0;


            // Obtengo las Columnas de las cuales sacare los Importes
            if (document.getElementById('lbltipo_conexion').innerHTML == 'RAFAM') {
                if (id_grilla == 'gvperiodos_vencidos') { celda_importe = 10 } else { celda_importe = 7 }
            } else {
                if (id_grilla == 'gvperiodos_vencidos') { celda_importe = 9 } else { celda_importe = 7 }
            }


            for (var i = 1; i < document.getElementById(id_grilla).rows.length; i++) {

                j = i + 1;
                if (j > 9) {
                    numero = j;
                } else {
                    numero = '0' + j;
                }

                var control_check = document.getElementById(id_grilla + '_ctl' + numero + '_CheckBox1');
                if (control_check != null) {
                    if (control_check.checked) {
                        total_aux = document.getElementById(id_grilla).rows[i].cells[celda_importe].innerHTML;
                        total = parseFloat(total) + parseFloat(total_aux);
                        cant_seleccionados = cant_seleccionados + 1;
                    }
                }



                /*
                alert(id_grilla + '$ctl' + numero + '$CheckBox1');
                var control_check = document.getElementsByName(id_grilla + '$ctl' + numero + '$CheckBox1'); 
                */



            }


            if (id_grilla == 'gvperiodos_vencidos') {
                document.getElementById('lbltotal_vencidos').innerHTML = formato_importe(total);
                document.getElementById('lblcantidad_seleccionados_vencidos').innerHTML = cant_seleccionados;
                document.getElementById('lblcantidad_no_seleccionados_vencidos').innerHTML = ((document.getElementById(id_grilla).rows.length - 1) - cant_seleccionados);
            } else {
                if (id_grilla == 'gvperiodos_no_vencidos') {
                    document.getElementById('lbltotal_no_vencidos').innerHTML = formato_importe(total);
                    document.getElementById('lblcantidad_seleccionados_no_vencidos').innerHTML = cant_seleccionados;
                    document.getElementById('lblcantidad_no_seleccionados_no_vencidos').innerHTML = ((document.getElementById(id_grilla).rows.length - 1) - cant_seleccionados);
                } else {
                    document.getElementById('lbltotal_otros_comprobantes').innerHTML = formato_importe(total);
                    document.getElementById('lblcantidad_seleccionados_otros_comprobantes').innerHTML = cant_seleccionados;
                    document.getElementById('lblcantidad_no_seleccionados_otros_comprobantes').innerHTML = ((document.getElementById(id_grilla).rows.length - 1) - cant_seleccionados);
                }
            }
        }


        /* Esta Funcion MARCA TODOS los COMPROBANTES que contengan el MISMO (TIPO Y NUMERO) */
        /* Esta Funcion SOLO es UTILIZADA cuando se CONECTA A RAFAM */
        function marcar_plan(id_grilla, tipo_y_nrocomprobante, fila) {
            var numero;
            var j;
            var celda_dato = 0;
            var valor_fila;
            var estado_check;


            // Obtengo el ESTADO del CHECK SELECCIONADO            
            var control_check = document.getElementById(id_grilla + '_ctl' + fila + '_CheckBox1');
            estado_check = control_check.checked;
    

            // Recorro la GRILLA BUSCANDO Comprobantes con el MISMO (TIPO y NRO)
            for (var i = 1; i < document.getElementById(id_grilla).rows.length; i++) {

                j = i + 1;
                if (j > 9) {
                    numero = j;
                } else {
                    numero = '0' + j;
                }

                // Obtengo el SubString para compara el TIPO y NRO de COMPROBANTE 
                valor_fila = document.getElementById(id_grilla).rows[i].cells[celda_dato].innerHTML;
                valor_fila = valor_fila.substring(0, valor_fila.length - 2)

                
                //SI el STRING es igual LO MARCO
                if (valor_fila == tipo_y_nrocomprobante) {
                    var control_check = document.getElementById(id_grilla + '_ctl' + numero + '_CheckBox1');
                    if (control_check != null) {
                        control_check.checked = estado_check;
                    }
                }
            }
        }



        /* Esta funcion solo esta para que refresque el evento click de los check en las grillas*/
        function refresh(id_grilla, tipo_y_nrocomprobante, fila) { }




        /* Esta Funcion MARCA TODOS los COMPROBANTES que contengan el MISMO RECURSO, ANIO Y CUOTA) */
        /* Esta Funcion SOLO es UTILIZADA cuando se CONECTA A RAFAM */
        function marcar_compDeMigracion(id_grilla, tipo_y_nrocomprobante, fila) {
            var numero;
            var j;
            var recurso;
	    var anio;
	    var cuota;

            var recursoComprobante;
	    var anioComprobante;
	    var cuotaComprobante;
            var estado_check;



	    // Tengo que restarle uno por como esta armado desde .NET
	    var filaComprobante = (parseInt(fila) -1);



	    // Obtengo el recurso, anio y cuota del movimiento seleccionado
	    recursoComprobante = document.getElementById(id_grilla).rows[filaComprobante].cells[1].innerHTML;
	    anioComprobante = document.getElementById(id_grilla).rows[filaComprobante].cells[2].innerHTML;
	    cuotaComprobante = document.getElementById(id_grilla).rows[filaComprobante].cells[3].innerHTML;


            // Obtengo el ESTADO del CHECK SELECCIONADO            
            var control_check = document.getElementById(id_grilla + '_ctl' + fila + '_CheckBox1');
            estado_check = control_check.checked;


            // Recorro la GRILLA BUSCANDO Comprobantes con el MISMO (TIPO y NRO)
            for (var i = 1; i < document.getElementById(id_grilla).rows.length; i++) {

                j = i + 1;
                if (j > 9) {
                    numero = j;
                } else {
                    numero = '0' + j;
                }


		//Obtengo el recurso, anio y cuota del movimiento
		recurso = document.getElementById(id_grilla).rows[i].cells[1].innerHTML;
		anio = document.getElementById(id_grilla).rows[i].cells[2].innerHTML;
		cuota = document.getElementById(id_grilla).rows[i].cells[3].innerHTML;



                //SI el movimiento tiene el mismo recurso, anio y cuota lo marco
                if (recursoComprobante == recurso && anioComprobante == anio && cuotaComprobante == cuota) {
                    var control_check = document.getElementById(id_grilla + '_ctl' + numero + '_CheckBox1');
                    if (control_check != null) {
                        control_check.checked = estado_check;
                    }
                }
            }
        }
        /* Esta funcion solo esta para que refresque el evento click de los check en las grillas*/
        function refresh(id_grilla, tipo_y_nrocomprobante, fila) { }








        /* Esta Funcion ASIGNA FORMATO a un IMPORTE */
        function formato_importe(importe) {
            importe = importe.toString().replace(/$|,/g, '');
            if (isNaN(importe))
                importe = "0";

            sign = (importe == (importe = Math.abs(importe)));
            importe = Math.floor(importe * 100 + 0.50000000001);
            cents = importe % 100;
            importe = Math.floor(importe / 100).toString();

            if (cents < 10)
                cents = "0" + cents;

            for (var i = 0; i < Math.floor((importe.length - (1 + i)) / 3); i++)
                importe = importe.substring(0, importe.length - (4 * i + 3)) + ',' +

            importe.substring(importe.length - (4 * i + 3));
            return (((sign) ? '' : '-') + '$ ' + importe + '.' + cents);
        }






        /* Esta Funcion CHEKEA si hay algun COMPROBANTE SELECCIONADO */
        function registros_seleccionados(id_grilla) {
            var numero;
            var j;

            //for (var i=1; i<document.getElementById('gvperiodos_vencidos').rows.length; i++) {
            for (var i = 1; i < document.getElementById(id_grilla).rows.length; i++) {

                j = i + 1;
                if (j > 9) {
                    numero = j;
                } else {
                    numero = '0' + j;
                }


                //var control_check = document.getElementById('gvperiodos_vencidos_ctl' + numero + '_CheckBox1');                         
                var control_check = document.getElementById(id_grilla + '_ctl' + numero + '_CheckBox1');
                if (control_check != null) {
                    if (control_check.checked) {
                        return true;
                    }
                }
            }

            return false;
        }


        /* Esta Funcion CONFIRMA y AVISA sobre la EMISION de un COMPROBANTE */
        function mensaje() {
            var panel = document.getElementById('boton_Imprimir_comprob_vencidos');
            if (panel != null) {
                return confirm("Para el correcto funcionamiento se recomienda descargar la �ltima versi�n de Adobe Reader.\n"
                               + "�Desea continuar?");
            } else {
                alert("Su Cuenta Corriente se encuentra vacia.")
                return false;
            }

        }


        /* Esta Funcion PIDE una CONFIRMACION de CIERRE DE SESSION */
        function cerrar_session() {
            return confirm("Usted esta a punto de cerrar su sesion.\n�Desea continuar?")
        }


        /* Esta Funcion ASIGNA un ESTILO a una BARRA */
        function efecto_barra_over(id_barra) {
            var barra = document.getElementById(id_barra);
            barra.className = "barra_mouseover";
        }


        /* Esta Funcion ASIGNA un ESTILO a una BARRA */
        function efecto_barra_out(id_barra) {
            var barra = document.getElementById(id_barra);
            barra.className = "barra_mouseout";
            barra.title = "";

        }



        /* Esta Funcion SETEA TODOS LOS COMPROBANTES como NO SELECCIONADOS por GRILLA */
        function estado_seleccion_check_inicial(id_grilla) {
            var nombre = '';

            for (i = 0; i < document.forms[0].elements.length; i++) {
                nombre = document.forms[0].elements[i].id;

                if (nombre.indexOf(id_grilla) != -1) {
                    document.forms[0].elements[i].checked = false;
                }
            }

        }




        /* Esta Funcion ASIGNA COLOR y SACA el CHECK a todos los COMPROBANTES en JUICIO */
        function movimientos_juicio(id_grilla) {
            var control_check;
            var padre;
            var numero;
            var i;
            var j;
            var control_fila;
            var cant_registros;
            var importe_origen;
            var importe_recargos;
            var importe_total;
            var importe_movimientos_juicio;
            var columna_rafam;


            cant_registros = 0;
            importe_origen = 0;
            importe_recargos = 0;
            importe_total = 0;
            importe_movimientos_juicio = 0;


            if (document.getElementById('lbltipo_conexion').innerHTML == 'RAFAM') { columna_rafam = 1 } else { columna_rafam = 0 }
            for (i = 1; i < document.getElementById(id_grilla).rows.length; i++) {

                j = i + 1;
                if (j > 9) {
                    numero = j;
                } else {
                    numero = '0' + j;
                }



                if (document.getElementById(id_grilla).rows[i].cells[(6 + columna_rafam)].innerHTML == 'En juicio') {
                    control_fila = document.getElementById(id_grilla).rows[i];

                    // Formato de los renglones en JUICIO
                    document.getElementById(id_grilla).rows[i].style.color = "#AA0000";
                    document.getElementById(id_grilla).rows[i].style.fontweight = 'bold';
                    document.getElementById(id_grilla).rows[i].style.fontfamily = 'Tahoma';


                    // Juan - 19102012
                    // Esta porcion de codigo la comente porque directamente el check no es asignado cuando existe una leyenda
                    // en la columna "condicion especial"               
                    // Obtenemos el elemento
                    //control_check = document.getElementById(id_grilla + '_ctl' + numero + '_CheckBox1');                         
                    // padre = control_check.parentNode;
                    //padre.removeChild(control_check);                                   


                    //Total Movimientos en Juicio
                    importe_movimientos_juicio = parseFloat(importe_movimientos_juicio) + parseFloat(document.getElementById(id_grilla).rows[i].cells[(9 + columna_rafam)].innerHTML);
                    cant_registros = cant_registros + 1;
                }


                //Totales
                importe_origen = parseFloat(importe_origen) + parseFloat(document.getElementById(id_grilla).rows[i].cells[(7 + columna_rafam)].innerHTML);
                importe_recargos = parseFloat(importe_recargos) + parseFloat(document.getElementById(id_grilla).rows[i].cells[(8 + columna_rafam)].innerHTML);
                importe_total = parseFloat(importe_total) + parseFloat(document.getElementById(id_grilla).rows[i].cells[(9 + columna_rafam)].innerHTML);


            }

            document.getElementById('lbltotal_origen').innerHTML = formato_importe(importe_origen);
            document.getElementById('lbltotal_recargo').innerHTML = formato_importe(importe_recargos);
            document.getElementById('lbltotal_importe').innerHTML = formato_importe(importe_total);

            document.getElementById('lblimporte_movimientos_juicio').innerHTML = formato_importe(importe_movimientos_juicio);
            document.getElementById('lblcantidad_registros_en_juicio').innerHTML = '(' + cant_registros + ')';
        }




        /* Esta Funcion CALCULA los TOTALES por GRILLA (NO_VENCIDOS Y OTROS COMPROBANTES)*/
        function calcular_totales_por_grilla(id_grilla) {
            var numero;
            var i;
            var j;
            var importe_origen;
            var importe_recargos;
            var importe_total;



            importe_origen = 0;
            importe_recargos = 0;
            importe_total = 0;
            for (i = 1; i < document.getElementById(id_grilla).rows.length; i++) {

                j = i + 1;
                if (j > 9) {
                    numero = j;
                } else {
                    numero = '0' + j;
                }

                //Totales
                importe_origen = parseFloat(importe_origen) + parseFloat(document.getElementById(id_grilla).rows[i].cells[4].innerHTML);
                importe_recargos = parseFloat(importe_recargos) + parseFloat(document.getElementById(id_grilla).rows[i].cells[5].innerHTML);
                importe_total = parseFloat(importe_total) + parseFloat(document.getElementById(id_grilla).rows[i].cells[6].innerHTML);
            }


            //Asigno Totales            
            if (id_grilla == 'gvperiodos_no_vencidos') {
                document.getElementById('lbltotal_origen_no_vencidos').innerHTML = formato_importe(importe_origen);
                document.getElementById('lbltotal_recargo_no_vencidos').innerHTML = formato_importe(importe_recargos);
                document.getElementById('lbltotal_importe_no_vencidos').innerHTML = formato_importe(importe_total);
            } else {
                document.getElementById('lbltotal_origen_otros_comprobantes').innerHTML = formato_importe(importe_origen);
                document.getElementById('lbltotal_recargo_otros_comprobantes').innerHTML = formato_importe(importe_recargos);
                document.getElementById('lbltotal_importe_otros_comprobantes').innerHTML = formato_importe(importe_total);
            }
        }



        /* Esta Funcion CUENTA la CANTIDAD de COMPROBANTES por GRILLA */
        function cantidad_registros(id_grilla) {
            var grilla = document.getElementById(id_grilla);
            if (grilla != null) {
                return (document.getElementById(id_grilla).rows.length - 1);
            } else {
                return 0;
            }
        }


        /* Esta Funcion ASIGNA ESTILO a un BOTON */
        function efecto_over(boton) {
            boton.className = "boton_gris";
        }


        /* Esta Funcion ASIGNA ESTILO a un BOTON */
        function efecto_out(boton) {
            boton.className = "boton_verde";
        }
        
        
        

   	/* Esta funci�n es s�lo para el caso de AZUL */        
        function movimientos_validos(){
            var control_check;            
            var numero; 
            var i;
            var j;            
            var per_ant_sin_sel;            
            var fecha = new Date();
            var anioActual = fecha.getFullYear();
                        
            per_ant_sin_sel = false;
            for (var i=1; i<document.getElementById('gvperiodos_vencidos').rows.length; i++) {
                  
                j = i + 1;
                if (j > 9){
                    numero = j;
                }else{               
                    numero = '0' + j;
                }
                
                if (document.getElementById('gvperiodos_vencidos').rows[i].cells[2].innerHTML != anioActual){
                
                    var control_check = document.getElementById('gvperiodos_vencidos' + '_ctl' + numero + '_CheckBox1');				
                    if (control_check != null){
                        if (control_check.checked) {               
                       
                            if (per_ant_sin_sel) {                                
                                alert('Debe seleccionar todos los periodos anteriores');
                                return false;
                            }
                        } else {                    
                            if (!per_ant_sin_sel) {                                
                                per_ant_sin_sel = true;
                            }
                        }
                    }                    
                }    
			}
        
        return true;
        }
        
        

        
    </script>



    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Sistema tributario :: Comprobantes </title>
    <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />

        
        
<link  href="../css/botones.css" rel="stylesheet" type="text/css" />


    <link href="../css/listado.css" rel="stylesheet" type="text/css" />
            <style type="text/css">
                #footer
                {
                    height: 0px;
                }
                            
                .columna_titulo{
            	    width: 40%;
            	    text-align:center;
                }
                .columna_titulo2{
            	    width: 40%;
            	    text-align:center;
                }
                
                .columna_dato{
            	    width: 60%;
            	    text-align:center;
            	    font-weight: bold;
                }
                .columna_dato2{
            	    width: 60%;
            	    text-align:center;
            	    font-weight: bold;
                }
                
                
                
                .columna_0 {
            	    width: 70%;
            	    text-align:left;
            	    font-weight: bold;            	
                }
                
                .columna_1 {
            	    width: 30%;
            	    text-align:right;
            	    font-weight: bold;
                }
                
                .columna_2 {
            	    width: 70%;
            	    text-align:left;
            	    font-weight: bold;
                }
                
                .columna_3 {
            	    width: 30%;
            	    text-align:center;
            	    font-weight: bold;
                }
                
                
                            
                #div_datos_vencidos
                {
                    height: 57px;
                }
                
                
                            
                </style>
    </head>

    <body onload="iniciar_pagina()">

        <form id="formulario" class="form" runat="server">
            <div id ="pagina"  class="main">        
            
                <div id="header">   
                    
                    
                    <div class="div_font" style="padding:0px 5px 0px 5px;">
                        <a href="#" title="Usar fuente menor" class="link_font" onclick="assign_font('-');">A-</a> 
                    </div>
                
                    <div class="div_font">
		                <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 
                    </div>		    
                    
                    
                     <span id="logo-text_listado">Consulta tributaria :: Comprobantes </span>  
                    <h2 id="slogan_listado">Municipalidad de 
                        <asp:Label ID="lblmunicipalidad_name" runat="server" Text="Label"></asp:Label></h2>		
                    
                                
                    <div id="links" style="height: 10px; width: 50%; float:right; text-align: right; padding: 5px 5px 0px 0px;"> 
                        &nbsp;&nbsp;&nbsp; <asp:LinkButton ID="link_cerrar_session" runat="server"  
                            CssClass="link" BackColor="#00CC00" ForeColor="Black">[Cerrar Sesi�n]</asp:LinkButton>
                    </div>
                </div>
        
                    
                <asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
                
                    
                <div class="content">
		            <div class="barra"><strong>&nbsp;CONSULTA DE DEUDA Y EMISION DE COMPROBANTES
                        </strong></div>
    		    
                    <div id="tabla" style="padding:10px 0px 10px 0px;" align="center">       
                    
                        <asp:Label id="lbltipo_conexion" runat="server" Text="ninguna.-"  ForeColor="White"></asp:Label>
                                                 
                            <table border="1" cellspacing="10" style="border-style: double; width: 45%; height: 89px; color: #000000;" align="center">
                                <tr>
                                    <td class="columna_titulo">Tipo de Cuenta</td>
                                    <td class="columna_dato"><asp:Label ID="lbltipo_de_cuenta" runat="server" Text="TipoDeCuenta"></asp:Label></td>
                                </tr>
                                            
                                <tr>
                                    <td class="columna_titulo">N�mero de Cuenta</td>
                                    <td class="columna_dato"><asp:Label ID="lblnro_de_cuenta" runat="server" Text="NroDeCuenta"></asp:Label></td>
                                </tr>
                                                                                                                               
                                <tr id="columna_titulo2" runat="server">
                                    <td class="columna_titulo" runat="server">Titular</td>
                                    <td class="columna_dato"><asp:Label ID="lbltitular" runat="server" Text="Titular"></asp:Label></td>
                                </tr>                                                                                                                       
                            </table>
                        </div>
                                                               
                    <div class="barra"><strong>&nbsp;DESCRIPCI�N DE LOS RECURSOS INVOLUCRADOS</strong></div>
                                           
                    <div id="div_informe" style="padding:10px 0px 10px 0px;" align="center">                                    
                            <asp:GridView ID="gvdescripcion_recursos" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" 
                                BorderStyle="Solid" BorderWidth="1px" CellPadding="1" ForeColor="Black" GridLines="Vertical" PageSize="5" Width="567px" AllowSorting="True" EnableTheming="True" >
                                
                                    <FooterStyle BackColor="#CCCCCC" />
                                        <RowStyle ForeColor="Black" />
                                            <Columns>
                                                <asp:BoundField HeaderText="C�digo" DataField="Codigo" HeaderStyle-Width="55px">
                                                    <HeaderStyle Width="55px" />
                                                    <ItemStyle Width="55px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:BoundField>
                                                
                                                <asp:BoundField DataField="Descripcion" HeaderText="Descripci�n" >                                     
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                </asp:BoundField>                                     
                                            </Columns>
                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                                    <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>  
                            
                                                                                           
                        </div>                                                       
                                      
                    <div id="id_barra_documentos_vencidos" runat="server" class="barra" onclick="estado_grilla_vencidos()" onmouseover="efecto_barra_over('id_barra_documentos_vencidos')" onmouseout="efecto_barra_out('id_barra_documentos_vencidos')"   
                            style="cursor: pointer" 
                            title="header=[Comprobantes Vencidos:] body=[Presione CLICK sobre esta barra para VER / OCULTAR los periodos adeudados.]"> 
                        1) PERIODOS ADEUDADOS VENCIDOS (OCULTAR)
                    </div>
                    
                    <a name="ancla_comprobantes_vencidos" id="id_ancla_comprobantes_vencidos"></a>
                    
                                
                    <div id="ajax" style="color: #000000;  width:100%;">
                            <script type="text/javascript" src="jsUpdateProgress.js"></script>		
                        
                        
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate >
                                        <div id="div_contenedor_movimientos_vencidos" style="text-align: center;" align="center">                                               
                                            <div id="div_mensaje_vencidos" style="height: 25px; color: #000000; padding-bottom:10px;">
                                                Los importes por recargos de los per�odos adeudados que aqu� se detallan se 
                                                encuentran calculados a la fecha: 
                                                <asp:Label ID="lblfecha_recargos" runat="server" Text=""></asp:Label>
                                                <br />
                                                Seleccione los per�odos que desee abonar
                                            </div>
                                            
                                            
                                            <a href="#id_ancla_comprobantes_no_vencidos"  class="link">[Ir a Comprobantes No 
                                            Vencidos]</a>
                                            <a href="#id_ancla_otros_comprobantes"  class="link">[Ir a Otros Comprobantes]</a>
                                            
											
											 <div id="cartel_azul" style="width:100%; padding:10px;" align="center" runat="server" visible="false">
                                                <div id="Div2" style="border:solid red; width:43%;" align="center">
                                                    <table border="0" align="center">                                                    
                                                        <tr>
                                                            <td style="width:32px;"><img src="../imagenes/information_user.png" alt="" /></td>
                                                            <td>
                                                                <strong>Importante:</strong><br />
                                                                <label id="mensaje_azul" runat="server"></label>
                                                            </td>
                                                         </tr>                                                                                                     
                                                    </table>
                                                </div>
                                            </div>
        
											
											
                                                                                                   
                                            <div id="grilla" style="text-align: center; padding:5px 5px 0px 5px; width:100%;" align="center"; >
                                                <asp:GridView ID="gvperiodos_vencidos" Name="gvperiodos_vencidos"  runat="server" 
                                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" 
                                                    BorderStyle="Solid" BorderWidth="1px" 
                                                   CellPadding="1" ForeColor="Black" GridLines="Vertical" PageSize="5" 
                                                    Width="99%" AllowSorting="True" EnableTheming="True">
                                                    <FooterStyle BackColor="#CCCCCC" />
                                                    <RowStyle ForeColor="Black" />
                                                        <Columns>
                                                                <asp:BoundField DataField="Nro_Movimiento" HeaderText="Comprobante" />
                                                                <asp:BoundField HeaderText="Recurso" DataField="Recurso"  HeaderStyle-Width="55px">
                                                                    <HeaderStyle Width="55px" />
                                                                    <ItemStyle Width="55px" />
                                                                </asp:BoundField>
                                                                <asp:BoundField HeaderText="A�o" DataField="Anio" />
                                                                <asp:BoundField HeaderText="Cuota" DataField="Cuota" />
                                                                <asp:BoundField HeaderText="Concepto" DataField="Concepto" />
                                                                <asp:BoundField HeaderText="Plan" DataField="Plan" />
                                                                <asp:BoundField HeaderText="Vencimiento" DataField="Fecha_Vencimiento" />
                                                                <asp:BoundField HeaderText="Cond. Especial" DataField="Condicion_Especial" />
                                                                <asp:BoundField HeaderText="Imp. Origen" DataField="Importe_Origen" 
                                                                    DataFormatString="{0:c}"/>
                                                                <asp:BoundField HeaderText="Imp. Recargos" DataField="Importe_Recargos" />
                                                                <asp:BoundField HeaderText="Imp. Total" DataField="Importe_Total" />
                                                                
              
                                                                <asp:templatefield HeaderText="" ShowHeader="False"  ControlStyle-Font-Bold="true" ItemStyle-Width="10px" HeaderStyle-BackColor="White">
                                                                    <HeaderTemplate>
                                                                        <img alt="" src="../imagenes/check_on.jpg"  height="18" width="25" onclick="estado_seleccion_check('gvperiodos_vencidos', true);"   style="cursor:pointer;"  />                                                            
                                                                        <img alt="" src="../imagenes/check_off.jpg"  height="18" width="25" onclick="estado_seleccion_check('gvperiodos_vencidos', false);"  style="cursor:pointer" />
                                                                    </HeaderTemplate>
                                                                    
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="CheckBox1" runat="server" EnableViewState="False" />
                                                                    </ItemTemplate>
                                                                     
                                                                <ControlStyle Font-Bold="True" Width="100px" />
                                                                <HeaderStyle BackColor="White" VerticalAlign="NotSet" Wrap="False" />
                                                                <ItemStyle Wrap="True" Width="20px"  />                                                             
                                                                </asp:templatefield>                                                  
                                                            </Columns>
                                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                                                    <AlternatingRowStyle BackColor="#CCCCCC" />
                                                </asp:GridView>
                                            </div>                                                
                                                    
                                            <div id="div_tablas_info" style="padding:10px 0px 0px 0px;  width:100%;">
                                                <table align="center"; style="width:90%;" >                                           
                                                    <tr>
                                                        <td style="width:45%">
                                                            <div="div_tabla_totales" >
                                                                <table border="1" cellspacing="0" style="width: 100%; height: 60px;  color: #000000;" align="left";>
                                                                    <tr>
                                                                        <td colspan="2"  class="titulo_tabla">DETALLE DE TOTALES:</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="columna_0">Cantidad de Registros:</td> 
                                                                        <td class="columna_1">
                                                                            <asp:Label ID="lblcantidad_de_registros" runat="server" Text="0"></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="columna_0">Total Importe Origen:</td> 
                                                                        <td class="columna_1"><asp:Label ID="lbltotal_origen" runat="server" Text="0.00"></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="columna_0">Total Importe Recargo:</td> 
                                                                        <td class="columna_1"><asp:Label ID="lbltotal_recargo" runat="server" Text="0.00"></asp:Label></td>                                                       
                                                                    </tr>        
                                                                    <tr>
                                                                        <td class="columna_0">Total Importe:</td>
                                                                        <td class="columna_1"><asp:Label ID="lbltotal_importe" runat="server" Text="0.00"></asp:Label></td>
                                                                    </tr>                                                   
                                                                </table>
                                                            </div>
                                                        </td>                                                   
                                                        <td style="width:10%"></td>
                                                        <td style="width:45%">
                                                            <div="div_tabla_totales" >
                                                                <table border="1" cellspacing="0" style="width: 100%; height: 60px;  color: #000000;" align="left";>
                                                                    <tr>
                                                                        <td colspan="4"  class="titulo_tabla">DETALLE DE 
                                                                            MOVIMIENTOS:</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="columna_2" style="  color:#AA0000">Registros en 
                                                                            Juicio:</td> 
                                                                        <td class="columna_3">
                                                                            <asp:Label ID="lblimporte_movimientos_juicio" runat="server" Font-Bold="True" ForeColor="#AA0000" Text="$ 0.00"></asp:Label>
                                                                            <asp:Label ID="lblcantidad_registros_en_juicio" runat="server" Font-Bold="True" ForeColor="#AA0000" Text="(0)"></asp:Label>                                                                                                                                                        
                                                                        </td>
                                                                    </tr>                                                           
                                                                    <tr>
                                                                        <td class="columna_2">Registros No Seleccionados:</td> 
                                                                        <td class="columna_3">
                                                                            <asp:Label ID="lblcantidad_no_seleccionados_vencidos" runat="server" 
                                                                                Font-Bold="True" ForeColor="#AA0000" Text="0"></asp:Label></td>
                                                                    </tr>        
                                                                    <tr>
                                                                        <td class="columna_2">Registros Seleccionados:</td> 
                                                                        <td class="columna_3">
                                                                            <asp:Label ID="lblcantidad_seleccionados_vencidos" runat="server" Font-Bold="True" ForeColor="#009933" Text="0"></asp:Label></td>
                                                                    </tr>                                                   
                                                                    <tr>
                                                                        <td class="columna_2">Total Importe Seleccionados:</td> 
                                                                        <td class="columna_3">&#160;<asp:Label ID="lbltotal_vencidos" runat="server" 
                                                                                Font-Bold="True" ForeColor="#AA0000" Text="$ 0.00"></asp:Label></td>
                                                                    </tr>                                                                                                                                                                                                                                              
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>                                                
                                            </div>
                                                    
                                            <div id="div_datos_vencidos"  align="center">  
                                                
                                                <div id="div_contenedor_botones">
                                                                                               
                                                    <div id="div_boton_informe_deuda" style="width:49%;  float:left;" align="right">
                                                       <asp:Button ID="boton_informe_deuda" runat="server"                                                          
                                                            Text="Imprimir Informe de Deuda" BorderStyle="None" 
                                                            CssClass="boton_verde" Height="35px" Width="284px" 
                                                            onmouseover="efecto_over(this)"
                                                            onmouseout="efecto_out(this)"
                                                            ToolTip="Con esta opcion usted Imprimira el Listado de los Comprobantes Vencidos."                                                           
                                                        />
                                                    </div>
                                                
                                                
                                                    <div id ="div_boton_vencidos" style="width:49%; float:right;" align="left"  >
                                                        <asp:Button ID="boton_Imprimir_comprob_vencidos" runat="server" 
                                                            Text="Imprimir Comprobante de Pago de Deuda Vencida" BorderStyle="None" 
                                                            CssClass="boton_verde" Height="35px" Width="284px" 
                                                            onmouseover="efecto_over(this)"
                                                            onmouseout="efecto_out(this)"
                                                            ToolTip="Con esta opcion usted Imprimira los comprobantes Vencidos Seleccionados."
                                                        />
                                                    </div>
                                                </div>
                                                
                                                
                                            </div>
                                                    
                                            <div id ="div_sin_registros_vencidos" style="height: 20px; text-align: center; color: #000000;" runat="server" >
                                                <asp:Label ID="lblmensaje_periodos_vencidos" runat="server" Font-Bold="True" Font-Size="Small" Text="NO SE ENCONTRARON PERIODOS VENCIDOS" Visible="False"></asp:Label>
                                            </div>                                            
                                        </div>
                                                                                          
                                        <div id ="id_barra_documentos_no_vencidos" class="barra" onclick="estado_grilla_no_vencidos()"  onmouseover="efecto_barra_over('id_barra_documentos_no_vencidos')"  onmouseout="efecto_barra_out('id_barra_documentos_no_vencidos')" 
                                            style="cursor: pointer"
                                            title="header=[Comprobantes Proximos a Vencer:] body=[Presione CLICK sobre esta barra para VER / OCULTAR los comprobantes a vencer en los proximos 60 dias.]"> 
                                            2) COMPROBANTES A VENCER EN LOS PROXIMOS 60 DIAS (VER)
                                        </div>                                           
                                            
                                            
                                        <a name="ancla_comprobantes_no_vencidos" id="id_ancla_comprobantes_no_vencidos"></a>
                                            
                                                    
                                        <div id="div_grilla_no_vencidos" align="center">
                                            
                                            <div id="titulo_grilla_no_vencidos" style="color: #000000; height: 17px; padding-bottom:10px;" align="center">
                                                Seleccione los comprobantes que desea imprimir
                                            </div>
                                            
                                            
                                            <a href="#id_ancla_comprobantes_vencidos"  class="link">[Ir a Comprobantes 
                                            Vencidos]</a>
                                            <a href="#id_ancla_otros_comprobantes"  class="link">[Ir a Otros Comprobantes]</a>
                                            
                                                    
                                            <div id ="div_grilla_no_vencidos2" style="text-align: center; padding:5px 0px 0px 0px;">
                                                <asp:GridView ID="gvperiodos_no_vencidos" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical" PageSize="1" Width="99%" HorizontalAlign="Center">
                                                    <FooterStyle BackColor="#CCCCCC" />
                                                    <RowStyle Height="1px" Wrap="False" />
                                                        <Columns>
                                                            <asp:BoundField HeaderText="Comprobante" DataField="Comprobante" />
                                                            <%--<asp:BoundField HeaderText="Detalle" DataField="Detalle" />--%>
                                                            <asp:BoundField HeaderText="Tipo Comp." DataField="Tipo_Comp" />
                                                            <asp:BoundField HeaderText="Vencimiento" DataField="Vencimiento" />
                                                            <asp:BoundField HeaderText="Fecha Vencimiento" DataField="Fecha_Vencimiento" />
                                                            <asp:BoundField HeaderText="Cond. Especial" DataField="Condicion_Especial" />
                                                            <asp:BoundField HeaderText="Imp. Origen" DataField="Importe_Origen" />
                                                            <asp:BoundField HeaderText="Imp. Recargo" DataField="Importe_Recargo" />
                                                            <asp:BoundField HeaderText="Imp. Total" DataField="Importe_Total" />                                                                                                                  
                                                            <asp:templatefield HeaderText="" ShowHeader="False"  ControlStyle-Font-Bold="true" ItemStyle-Width="10px" HeaderStyle-BackColor="White">
                                                                <HeaderTemplate>
                                                                    <img alt="" src="../imagenes/check_on.jpg"  height="18" width="25" onclick="estado_seleccion_check('gvperiodos_no_vencidos', true);"  style="cursor:pointer;"  />                                                            
                                                                    <img alt="" src="../imagenes/check_off.jpg"  height="18" width="25" onclick="estado_seleccion_check('gvperiodos_no_vencidos', false);"  style="cursor:pointer" />
                                                                </HeaderTemplate>                                                            
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="CheckBox1" runat="server" EnableViewState="false" Checked="false" BorderColor="Black" Width="10px" TextAlign="Left"/>
                                                                </ItemTemplate>                                                             
                                                                <ControlStyle Font-Bold="True" Width="90px" />
                                                                <HeaderStyle BackColor="White" HorizontalAlign="Center" VerticalAlign="NotSet" Wrap="False" />
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />                                                             
                                                            </asp:templatefield>
                                                        </Columns>
                                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" Height="1px" Wrap="False" />
                                                    <AlternatingRowStyle BackColor="#CCCCCC" />
                                                </asp:GridView>
                                            </div>

                                        


                                                   
                                            <div id="div_datos_no_vencidos" >
                                                
                                                    <div id="div_tablas_info_no_vencidos" style="padding:10px 0px 0px 0px;  width:100%;">
                                                <table align="center"; style="width:90%;" >                                           
                                                    <tr>
                                                        <td style="width:45%">
                                                            <div="div_tabla_totales" >
                                                                <table border="1" cellspacing="0" style="width: 100%; height: 60px;  color: #000000;" align="left";>
                                                                    <tr>
                                                                        <td colspan="2"  class="titulo_tabla">DETALLE DE TOTALES:</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="columna_0">Cantidad de Registros:</td> 
                                                                        <td class="columna_1">
                                                                            <asp:Label ID="lblcantidad_de_registros_no_vencidos" runat="server" Text="0"></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="columna_0">Total Importe Origen:</td> 
                                                                        <td class="columna_1">
                                                                            <asp:Label ID="lbltotal_origen_no_vencidos" runat="server" Text="0.00"></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="columna_0">Total Importe Recargo:</td> 
                                                                        <td class="columna_1">
                                                                            <asp:Label ID="lbltotal_recargo_no_vencidos" runat="server" Text="0.00"></asp:Label></td>                                                       
                                                                    </tr>        
                                                                    <tr>
                                                                        <td class="columna_0">Total Importe:</td>
                                                                        <td class="columna_1">
                                                                            <asp:Label ID="lbltotal_importe_no_vencidos" runat="server" Text="0.00"></asp:Label></td>
                                                                    </tr>                                                   
                                                                </table>
                                                            </div>
                                                        </td>                                                   
                                                        <td style="width:10%"></td>
                                                        <td style="width:45%; vertical-align: top;">
                                                            <div="div_tabla_totales" >
                                                                <table border="1" cellspacing="0" style="width: 100%; height: 60px;  color: #000000;" align="left";>
                                                                    <tr>
                                                                        <td colspan="4"  class="titulo_tabla">DETALLE DE MOVIMIENTOS:</td>
                                                                    </tr>
                                                                                                                 
                                                                    <tr>
                                                                        <td class="columna_2">Registros No Seleccionados:</td> 
                                                                        <td class="columna_3">
                                                                            <asp:Label ID="lblcantidad_no_seleccionados_no_vencidos" runat="server" Font-Bold="True" ForeColor="#AA0000" Text="0"></asp:Label>                                                                            
                                                                        </td>
                                                                            
                                                                    </tr>        
                                                                    <tr>
                                                                        <td class="columna_2">Registros Seleccionados:</td> 
                                                                        <td class="columna_3">
                                                                            <asp:Label ID="lblcantidad_seleccionados_no_vencidos" runat="server" 
                                                                                Font-Bold="True" ForeColor="#009933" Text="0"></asp:Label></td>
                                                                    </tr>                                                   
                                                                    <tr>
                                                                        <td class="columna_2">Total Importe Seleccionados:</td> 
                                                                        <td class="columna_3">&#160;<asp:Label ID="lbltotal_no_vencidos" runat="server" 
                                                                                Font-Bold="True" ForeColor="#AA0000" Text="$ 0.00"></asp:Label></td>
                                                                    </tr>                                                                                                                                                                                                                                              
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>                                                
                                            </div>
                                               
                                            
                                                                                           
                                                <div id ="div_link_no_vencidos" style="height: 57px; text-align: center;">
                                                     <div id ="div_boton_no_vencidos" align="center">
                                                        <asp:Button ID="boton_Imprimir_comprob_no_vencidos" runat="server" 
                                                            Text="Imprimir Comprobante de Pago no Vencidos" BorderStyle="None" 
                                                            CssClass="boton_verde" Height="35px" Width="284px" 
                                                            onmouseover="efecto_over(this)"
                                                            onmouseout="efecto_out(this)"
                                                            ToolTip="Con esta opcion usted Imprimira los comprobantes Seleccionados Proximos a Vencer dentro de los 60 dias."
                                                        />
                                                    </div>
                                                                                                    
                                                </div>
                                            </div> 
                                                
                                            <div id ="div_sin_registros_no_vencidos" style="height: 20px; text-align: center; color: #000000;" runat="server" >
                                                <asp:Label ID="lblmensaje_periodos_no_vencidos" runat="server" Font-Bold="True" Font-Size="Small" Text="NO SE ENCONTRARON PERIODOS PROXIMOS A VENCER" Visible="False"></asp:Label>
                                            </div>
                                        </div>
                                        
                                        
                                        <div id ="id_barra_documentos_otros_comprobantes" class="barra" onclick="estado_grilla_otros_comprobantes()"  onmouseover="efecto_barra_over('id_barra_documentos_otros_comprobantes')"  onmouseout="efecto_barra_out('id_barra_documentos_otros_comprobantes')" 
                                            style="cursor: pointer"
                                            title="header=[Otros Comprobantes a Vencer:] body=[Presione CLICK sobre esta barra para VER / OCULTAR los periodos de otros comprobantes.]"> 
                                             3) OTROS COMPROBANTES A VENCER (VER)
                                        </div>                                           


                                        <a name="ancla_otros_comprobantes" id="id_ancla_otros_comprobantes"></a>                                                    
                                        
                                        
                                        <div id="div_grilla_otros_comprobantes" align="center">
                                            <div id="titulo_grilla_otros_comprobantes" 
                                                style="color: #000000; height: 17px; padding-bottom:10px;" align="center">
                                                Seleccione los comprobantes que desea imprimir
                                            </div>
                                            
                                            
                                            <a href="#id_ancla_comprobantes_vencidos"  class="link">[Ir a Comprobantes 
                                            Vencidos]</a>
                                            <a href="#id_ancla_comprobantes_no_vencidos"  class="link">[Ir a Comprobantes No 
                                            Vencidos]</a>
                                                    
                                            <div id ="div_grilla_otros_comprobantes2" style="text-align: center; padding:5px 0px 0px 0px;">
                                                <asp:GridView ID="gvperiodos_otros_comprobantes" runat="server" 
                                                    AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" 
                                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" 
                                                    GridLines="Vertical" PageSize="1" Width="99%" HorizontalAlign="Center">
                                                    <FooterStyle BackColor="#CCCCCC" />
                                                    <RowStyle Height="1px" Wrap="False" />
                                                        <Columns>
                                                            <asp:BoundField HeaderText="Comprobante" DataField="Comprobante" />
                                                            <%--<asp:BoundField HeaderText="Detalle" DataField="Detalle" />--%>
                                                            <asp:BoundField HeaderText="Tipo Comp." DataField="Tipo_Comp" />
                                                            <asp:BoundField HeaderText="Vencimiento" DataField="Vencimiento" />
                                                            <asp:BoundField HeaderText="Fecha Vencimiento" DataField="Fecha_Vencimiento" />
                                                            <asp:BoundField HeaderText="Cond. Especial" DataField="Condicion_Especial" />
                                                            <asp:BoundField HeaderText="Imp. Origen" DataField="Importe_Origen" />
                                                            <asp:BoundField HeaderText="Imp. Recargo" DataField="Importe_Recargo" />
                                                            <asp:BoundField HeaderText="Imp. Total" DataField="Importe_Total" />                                                                                                                  
                                                            <asp:templatefield HeaderText="" ShowHeader="False"  ControlStyle-Font-Bold="true" ItemStyle-Width="10px" HeaderStyle-BackColor="White">
                                                                <HeaderTemplate>
                                                                    <img alt="" src="../imagenes/check_on.jpg"  height="18" width="25" onclick="estado_seleccion_check('gvperiodos_otros_comprobantes', true);"  style="cursor:pointer;"  />                                                            
                                                                    <img alt="" src="../imagenes/check_off.jpg"  height="18" width="25" onclick="estado_seleccion_check('gvperiodos_otros_comprobantes', false);"  style="cursor:pointer" />
                                                                </HeaderTemplate>                                                            
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="CheckBox1" runat="server" EnableViewState="false" Checked="false" BorderColor="Black" Width="10px" TextAlign="Left"/>
                                                                </ItemTemplate>                                                             
                                                                <ControlStyle Font-Bold="True" Width="90px" />
                                                                <HeaderStyle BackColor="White" HorizontalAlign="Center" VerticalAlign="NotSet" Wrap="False" />
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />                                                             
                                                            </asp:templatefield>
                                                        </Columns>
                                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" Height="1px" Wrap="False" />
                                                    <AlternatingRowStyle BackColor="#CCCCCC" />
                                                </asp:GridView>
                                            </div>
                                                   
                                            <div id="div_datos_otros_comprobantes">
                                            
                                                <div id="div1" style="padding:10px 0px 0px 0px;  width:100%;">
                                                <table align="center"; style="width:90%;" >                                           
                                                    <tr>
                                                        <td style="width:45%">
                                                            <div="div_tabla_totales" >
                                                                <table border="1" cellspacing="0" style="width: 100%; height: 60px;  color: #000000;" align="left";>
                                                                    <tr>
                                                                        <td colspan="2"  class="titulo_tabla">DETALLE DE TOTALES:</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="columna_0">Cantidad de Registros:</td> 
                                                                        <td class="columna_1">
                                                                            <asp:Label ID="lblcantidad_de_registros_otros_comprobantes" runat="server" 
                                                                                Text="0"></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="columna_0">Total Importe Origen:</td> 
                                                                        <td class="columna_1">
                                                                            <asp:Label ID="lbltotal_origen_otros_comprobantes" runat="server" 
                                                                                Text="0.00"></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="columna_0">Total Importe Recargo:</td> 
                                                                        <td class="columna_1">
                                                                            <asp:Label ID="lbltotal_recargo_otros_comprobantes" runat="server" 
                                                                                Text="0.00"></asp:Label></td>                                                       
                                                                    </tr>        
                                                                    <tr>
                                                                        <td class="columna_0">Total Importe:</td>
                                                                        <td class="columna_1">
                                                                            <asp:Label ID="lbltotal_importe_otros_comprobantes" runat="server" 
                                                                                Text="0.00"></asp:Label></td>
                                                                    </tr>                                                   
                                                                </table>
                                                            </div>
                                                        </td>                                                   
                                                        <td style="width:10%"></td>
                                                        <td style="width:45%; vertical-align: top;">
                                                            <div="div_tabla_totales" >
                                                                <table border="1" cellspacing="0" style="width: 100%; height: 60px;  color: #000000;" align="left";>
                                                                    <tr>
                                                                        <td colspan="4"  class="titulo_tabla">DETALLE DE MOVIMIENTOS:</td>
                                                                    </tr>
                                                                                                                 
                                                                    <tr>
                                                                        <td class="columna_2">Registros No Seleccionados:</td> 
                                                                        <td class="columna_3">
                                                                            <asp:Label ID="lblcantidad_no_seleccionados_otros_comprobantes" runat="server" Font-Bold="True" ForeColor="#AA0000" Text="0"></asp:Label>                                                                            
                                                                        </td>
                                                                            
                                                                    </tr>        
                                                                    <tr>
                                                                        <td class="columna_2">Registros Seleccionados:</td> 
                                                                        <td class="columna_3">
                                                                            <asp:Label ID="lblcantidad_seleccionados_otros_comprobantes" runat="server" 
                                                                                Font-Bold="True" ForeColor="#009933" Text="0"></asp:Label></td>
                                                                    </tr>                                                   
                                                                    <tr>
                                                                        <td class="columna_2">Total Importe Seleccionados:</td> 
                                                                        <td class="columna_3">&#160;<asp:Label ID="lbltotal_otros_comprobantes" runat="server" 
                                                                                Font-Bold="True" ForeColor="#AA0000" Text="$ 0.00"></asp:Label></td>
                                                                    </tr>                                                                                                                                                                                                                                              
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>                                                
                                            </div>
                                            
                                            
                                                
                                               
                                                
                                                                                           
                                                <div id ="div_link_otros_comprobantes" 
                                            style="height: 60px; text-align: center;">
                                                    <div id ="div_boton_otros_comprobantes" align="center">
                                                        <asp:Button ID="boton_Imprimir_comprob_otros_comprobantes" runat="server" 
                                                            Text="Imprimir Otros Comprobante de Pago" BorderStyle="None" 
                                                            CssClass="boton_verde" Height="35px" Width="284px" 
                                                            onmouseover="efecto_over(this)"
                                                            onmouseout="efecto_out(this)"
                                                            ToolTip="Con esta opcion usted Imprimira Otros comprobantes Seleccionados."
                                                        />
                                                    </div>
                                                </div>
                                            </div> 
                                                
                                            <div id ="div_sin_registros_otros_comprobantes" 
                                                style="height: 20px; text-align: center; color: #000000;" runat="server" >
                                                <asp:Label ID="lblmensaje_periodos_otros_comprobantes" runat="server" 
                                                    Font-Bold="True" Font-Size="Small" 
                                                    Text="NO SE ENCONTRARON OTROS PERIODOS A VENCER" Visible="False"></asp:Label>
                                            </div>
                                        </div>
                                        
                                    </ContentTemplate>   
                                </asp:UpdatePanel>
                                    
                                    
                            <div style="height: 15px; font-weight: bold; font-size: small; text-align: center; font-family: 'Courier New', Courier, 'espacio sencillo';" class="barra">
                                NOTA: </div>
                             <div align="center">&nbsp; - Los per�odos de deuda aqu� listados pueden no reflejar los 
                                 pagos realizados recientemente. Ante cualquier duda relacionada con su estado de 
                                 deuda&nbsp; p�ngase en contacto con la Municipalidad a trav�s del tel�fono 
                                    <strong><asp:Label ID="lbltelefono" runat="server" Text="TELEFONO"></asp:Label></strong> 
                                 &nbsp;o la direcci�n de correo electr�nico 
                                    <strong><asp:Label ID="lblmail" runat="server" Text="MAIL"></asp:Label></strong>
                                    .</div>                           
                                 
    	
	                        <div align="center">
                                    &nbsp; - Para un correcto funcionamiento en la Emisi�n de Comprobantes se le aconseja 
                                    tener actualizado el Acrobat Reader. Para actualizar a la �ltima versi�n    
                                    <a href="http://www.adobe.com/es/products/acrobat/readstep2.html" target="_blank"; class="link">
                                    [clic aqu�]</a>.
                                </div>
                                        
                            <div style="padding:10px 0px 5px 0px; text-align:right";><a href="http://www.grupomdq.com" target="_blank" class="link">
                                [Desarrollado por Grupo MDQ]</a>
                                <br />
                                <asp:Label ID="lblversion" runat="server"></asp:Label>
                            </div>                                                                    
	                    </div>
                                    
                </div>
            </div>                                     
                                                                         
            <asp:Panel ID="panelUpdateProgress" runat="server" CssClass="updateProgress">
			    <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server">
				    <ProgressTemplate>
					    <div style="position: relative; top: 30%; text-align: center;">
						    <img src="loading.gif" style="vertical-align: middle" alt="Processing" />
						    Generando Comprobante ...
					    </div>
				    </ProgressTemplate>
			    </asp:UpdateProgress>
		    </asp:Panel>
		    
		    <ajaxToolkit:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="panelUpdateProgress" BackgroundCssClass="modalBackground" PopupControlID="panelUpdateProgress" />
            
        </form>
    </body>
</html>
