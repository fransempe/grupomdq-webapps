﻿Option Explicit On
Option Strict On

Imports sico

Public Class ClsConexionFOX

    Private mDLL As sico.recurweb
    Private mXML As String
    Private ObjLog As Clslog
    Private mRutaFisica As String

    Private Sub Limpiar()
        Me.mDLL = Nothing
        System.GC.Collect()
        System.GC.WaitForPendingFinalizers()
    End Sub


    Private Function LimpiarXML_UTF(ByVal pXML As String) As String

        pXML = pXML.Replace("Ã¡", "á")
        pXML = pXML.Replace("Ã€", "À")
        pXML = pXML.Replace("Ã©", "é")
        pXML = pXML.Replace("Ã‰", "É")
        pXML = pXML.Replace("Ã*", "í")
        pXML = pXML.Replace("Ã³", "ó")
        'pXML = pXML.Replace("Ã“",  "Ó")
        pXML = pXML.Replace("Ãº", "ú")
        pXML = pXML.Replace("Ã±", "ñ")
        pXML = pXML.Replace("Ã‘", "Ñ")


        Return pXML.Trim
    End Function

    Public Function ProbarConexion() As String
        Call Me.Limpiar()
        Me.mDLL = New sico.recurweb
        Me.mXML = Me.mDLL.ConsultarDeuda("I", 368)
        Me.mDLL = Nothing
        Return Me.mXML.Trim
    End Function


    Public Function VerRuta_CadenaConexion() As String
        Call Me.Limpiar()
        Me.mDLL = New sico.recurweb
        Me.mXML = Me.mDLL.VerRuta
        Me.mDLL = Nothing
        Return Me.mXML.Trim
    End Function


    Public Function ConsultarDeuda(ByVal mTipoCuenta As String, ByVal mNroCuenta As Long, ByVal mNroUsuario As Integer) As String

        Try
            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            Me.mXML = Me.mDLL.ConsultarDeuda(mTipoCuenta.ToString, mNroCuenta)
            Me.mXML = Me.LimpiarXML_UTF(mXML.Trim)

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
            Me.mXML = "Error"
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function


    Public Function EmitirComprobantes(ByVal mXMLComprobantes As String, ByVal mComprobantesVencidos As Boolean) As String
        Dim mComprobante As String

        mComprobante = ""
        If (mComprobantesVencidos) Then
            mComprobante = Me.EmitirComprobanteVencido(mXMLComprobantes.Trim)
        Else
            mComprobante = Me.EmitirComprobanteNoVencido(mXMLComprobantes.Trim)
        End If

        mComprobante = Me.LimpiarXML_UTF(mComprobante.Trim)
        Return mComprobante.Trim
    End Function



    Public Function EmitirComprobanteVencido(ByVal mXMLComprobantesVencidos As String) As String
        Dim mComprobante_XML As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            mComprobante_XML = Me.mDLL.EmitircompPagoDeudaVenc(mXMLComprobantesVencidos.Trim)
            mComprobante_XML = Me.LimpiarXML_UTF(mComprobante_XML.Trim)

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.ToString)
            Me.ObjLog = Nothing
            mComprobante_XML = ex.ToString.Trim
        Finally
            Me.mDLL = Nothing
        End Try

        Return mComprobante_XML.Trim
    End Function


    Public Function EmitirComprobanteNoVencido(ByVal mXMLComprobantesNoVencidos As String) As String
        Dim mComprobante_XML As String

        Try

            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            mComprobante_XML = Me.mDLL.EmitirCompPagoNoVenc(mXMLComprobantesNoVencidos.Trim)
            mComprobante_XML = Me.LimpiarXML_UTF(mComprobante_XML.Trim)

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
            mComprobante_XML = ex.ToString.Trim
        Finally
            Me.mDLL = Nothing
        End Try

        Return mComprobante_XML.Trim
    End Function


    'Public Function ObtenerDatosMunicipalidad() As String

    '    Try

    '        Call Limpiar()
    '        mDLL = New sico.recurweb
    '        mXML = mDLL.ObtenerTelefonoYMail()
    '        mDLL = Nothing
    '        Return mXML.ToString


    '    Catch ex As Exception
    '        ObjLog = New Clslog
    '        ObjLog.GenerarLog(ex, mRutaFisica.Trim)
    '        ObjLog = Nothing
    '        Return "Error"
    '    End Try


    'End Function


    'Esta Funcion me Devuelve el Número de Cuenta en Base a un Dominio
    Public Function ObtenerNroCuentaPorPatente(ByVal mDominio As String) As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            Me.mXML = Me.mDLL.ObtenerNroCuentaPorPatente(mDominio.ToString.Trim)

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
            Me.mXML = "Error"
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function


    'Esta Funcion me Devuelve los datos de la municipalidad
    Public Function ObtenerDatosMunicipalidad() As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            Me.mXML = Me.mDLL.ObtenerDatosMunicipalidad()
            Me.mXML = Me.LimpiarXML_UTF(Me.mXML.Trim)

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
            Me.mXML = "Error"
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function



    'Esta Funcion devuelve el codigo de la municipalidad para generar el barcode
    Public Function ObtenerCodigoMunicipalidad() As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            'mXML = mDLL.ObtenerCodigoMunicipalidad()            

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
            Me.mXML = "Error"
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function


    'Esta Funcion devuelve el codigo de la municipalidad para generar el barcode
    Public Function ObtenerDatosContribuyente(ByVal pTipoImponible As String, ByVal pNroImponible As String) As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            Me.mXML = Me.mDLL.ObtenerDatosContribuyente(pTipoImponible.Trim, pNroImponible.Trim)
            Me.mXML = Me.LimpiarXML_UTF(Me.mXML.Trim)

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
            Me.mXML = "Error"
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function



    'Esta Funcion valida que esten todos los parametros creados y definidos
    Public Function VerificarParametros(ByVal pVerParametros As Boolean) As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            Me.mXML = Me.mDLL.VerificarParametros(pVerParametros)


        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
            Me.mXML = "Error"
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function



    'Esta Funcion Devuelve la fecha a la cual se calcularan los intereses
    Public Function ObtenerFechaActuaWeb() As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            Me.mXML = Me.mDLL.ObtenerFechaActuaWeb()

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
            Me.mXML = "Error"
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function


    Public Function ObtenerFechaVTOWebCalculada() As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            Me.mXML = Me.mDLL.ObtenerFechaVTOWebCalculada()

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
            Me.mXML = "Error"
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function



    Public Function CreateLog(ByVal p_sPath As String, ByVal p_sMensaje As String) As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            Me.mXML = Me.mDLL.createLog(p_sPath.Trim, p_sMensaje.Trim)

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function


    Public Function agregarVisita(ByVal p_sFecha As String, ByVal p_sHora As String, ByVal p_sTipoImponible As String, ByVal p_lNroImponible As Long) As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            Me.mXML = Me.mDLL.agregarVisita(p_sFecha.Trim, p_sHora.Trim, p_sTipoImponible.Trim, p_lNroImponible)

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function

    Public Function actualizarVisita(ByVal p_sFecha As String, ByVal p_sHora As String, ByVal p_sTipoImponible As String, ByVal p_lNroImponible As Long) As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            Me.mXML = Me.mDLL.actualizarVisita(p_sFecha.Trim, p_sHora.Trim, p_sTipoImponible.Trim, p_lNroImponible)

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function

    Public Function getVisitasCantidad() As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            Me.mXML = Me.mDLL.getVisitasCantidad()

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function

    Public Function getVisitasCantidadDetalle(ByVal p_sFechaDesde As String, ByVal p_sFechaHasta As String) As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            Me.mXML = Me.mDLL.getVisitasCantidadDetalle(p_sFechaDesde.Trim, p_sFechaHasta.Trim)

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function


    Public Function getParametroExcluirRecurso() As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            Me.mXML = Me.mDLL.getParametroExcluirRecurso()

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function

    Public Function getObtenerTiposImponible() As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            Me.mXML = Me.mDLL.getObtenerTiposImponible()

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function


    Public Function usaLoginAdmin() As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            Me.mXML = Me.mDLL.getUsaLoginAdmin()

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function


    Public Sub New(ByVal mRuta As String)
        Me.mRutaFisica = mRuta.Trim
    End Sub


    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub


    'Esta Funcion Devuelve si un imponible esta o no Bloqueado en la tabla IMPOBLOQ
    Public Function ObtenerImponibleBloqueado(ByVal pTipoImponible As String, ByVal pNroImponible As Long) As String

        Try
            Me.mXML = ""
            Call Me.Limpiar()
            Me.mDLL = New sico.recurweb
            Me.mXML = Me.mDLL.ObtenerImponibleBloqueado(pTipoImponible.Trim, pNroImponible)

        Catch ex As Exception
            Me.ObjLog = New Clslog
            Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
            Me.ObjLog = Nothing
            Me.mXML = "Error"
        Finally
            Me.mDLL = Nothing
        End Try

        Return Me.mXML.Trim
    End Function

    'Esta Funcion Devuelve el parámetro para imprimir o no el codigo de barras de zárate para COMERCIOS
    Public Function ObtenerParametroImprimeCodBar() As String

        'Try
        '    Me.mXML = ""
        '    Call Me.Limpiar()
        '    Me.mDLL = New sico.recurweb
        '    Me.mXML = Me.mDLL.getParametroImprimeCodBar()
        'Catch ex As Exception
        '    Me.ObjLog = New Clslog
        '    Me.ObjLog.GenerarLog(ex, mRutaFisica.Trim)
        '    Me.ObjLog = Nothing
        '    Me.mXML = "Error"
        'Finally
        '    Me.mDLL = Nothing
        'End Try

        'Return Me.mXML.Trim
        Return "hola"
    End Function
End Class
