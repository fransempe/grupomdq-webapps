﻿Imports iTextSharp.text
Imports iTextSharp.text.pdf
Public Class ClsCrearListadoVacioPDF

#Region "Variables y Estructuras"

    Private Structure SColumnas
        Dim Recurso As Integer
        Dim Anio As Integer
        Dim Cuota As Integer
        Dim Concepto As Integer
        Dim Plan As Integer
        Dim FechaVencimiento As Integer
        Dim CondicionEspecial As Integer
        Dim ImporteOrigen As Integer
        Dim ImporteRecargo As Integer
        Dim ImporteTotal As Integer
    End Structure

    Private mRutaFisica As String
    Private mDatosContribuyente As String()
    Private mTipoConexion As String
    Private mOrganismo As String
    Private mNombreOrganismo As String


    Private dsDatosMunicipalidad As DataSet

#End Region

#Region "Propertys"

    Public WriteOnly Property RutaFisica() As String
        Set(ByVal value As String)
            mRutaFisica = value.Trim
        End Set
    End Property

    Public WriteOnly Property DatosContribuyente() As String()
        Set(ByVal value() As String)
            mDatosContribuyente = value
        End Set
    End Property

    Public WriteOnly Property TipoConexion() As String
        Set(ByVal value As String)
            mTipoConexion = value
        End Set
    End Property

    Public WriteOnly Property Organismo() As String
        Set(ByVal value As String)
            mOrganismo = value
        End Set
    End Property


    Public WriteOnly Property NombreOrganismo() As String
        Set(ByVal value As String)
            mNombreOrganismo = value
        End Set
    End Property


    Public WriteOnly Property DatosMunicipalidad() As DataSet
        Set(ByVal value As DataSet)
            Me.dsDatosMunicipalidad = value
        End Set
    End Property

#End Region

#Region "Contructor"

    ' Contructor de la CLASE
    Public Sub New()        
        mRutaFisica = ""

        mTipoConexion = ""
        mOrganismo = ""
        mNombreOrganismo = ""
    End Sub

#End Region

#Region "Procedimientos"

    'Procedimiento donde se CREA el PDF y se SETEA las propiedades del DOCUMENTO y CONTROLA cuando se CREA una NUEVA PAGINA
    Public Sub CrearPDF()
        Dim mDocumentoPDF As Document = New Document(iTextSharp.text.PageSize.A4, 15, 15, 50, 50)
        Dim writer As PdfWriter = PdfWriter.GetInstance(mDocumentoPDF, New System.IO.FileStream(mRutaFisica.ToString, System.IO.FileMode.Create))

        mDocumentoPDF.SetPageSize(PageSize.A4.Rotate())
        writer.ViewerPreferences = PdfWriter.PageLayoutOneColumn
        mDocumentoPDF.Open()

        Call Me.CrearPaginas(mDocumentoPDF, writer)
        mDocumentoPDF.Close()
    End Sub

    'Este Procedimiento LLAMA a las procedimientos que CREAN las distintas partes del DOCUMENTO
    Private Sub CrearPaginas(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Call CrearEncabezadoLogo(mDocumentoPDF, writer)
        Call CrearEncabezado(mDocumentoPDF, writer)
        Call TablaMovimientos(mDocumentoPDF, writer)
    End Sub

    'Este Procedimiento DIBUJA el ENCABEZADO del DOCUMENTO INCLUYENDO el LOGO
    Private Sub CrearEncabezadoLogo(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mLogo As Image
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mNombreLogo As String
        Dim mLogoHeight As Single
        Dim mLogoWidth As Single
        Dim mLogoCoordenadaX As Single
        Dim mLogoCoordenadaY As Single
        Dim mLogo_AUX As Byte()
        Dim mNombreMunicipalidad_AUX As String





        'Obtengo el Nombre del Logo segun la CONEXION y propiedades para setearlo
        If (mTipoConexion.ToString = "RAFAM") Then
            mNombreLogo = "logo_rafam.jpg"
            mLogoHeight = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoHeight").ToString.Trim)
            mLogoWidth = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoWidth").ToString.Trim)
            mLogoCoordenadaX = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_LogoCoordenadaX").ToString.Trim)
            mLogoCoordenadaY = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_LogoCoordenadaY").ToString.Trim)
        Else



            'LOGO
            mLogo_AUX = ClsTools.ObtenerLogo()
            mLogoHeight = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoHeight").ToString.Trim)
            mLogoWidth = CSng(System.Configuration.ConfigurationManager.AppSettings("LogoWidth").ToString.Trim)
            mLogoCoordenadaX = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_LogoCoordenadaX").ToString.Trim)
            mLogoCoordenadaY = CSng(System.Configuration.ConfigurationManager.AppSettings("Listado_LogoCoordenadaY").ToString.Trim)

        End If


        'Agrego el Logo a PDF
        Try
            If (mTipoConexion.ToString = "RAFAM") Then
                mLogo = iTextSharp.text.Image.GetInstance(mNombreLogo.ToString)
            Else
                mLogo = iTextSharp.text.Image.GetInstance(mLogo_AUX)
            End If

            mLogo.SetAbsolutePosition(mLogoCoordenadaX, mLogoCoordenadaY)
            mLogo.ScaleAbsolute(mLogoWidth, mLogoHeight)
            mDocumentoPDF.Add(mLogo)
        Catch ex As Exception
        End Try


        ''Nombre de La Municipalidad
        'cb = writer.DirectContent
        'cb.BeginText()
        'mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        'cb.SetFontAndSize(mFuente, 10)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mOrganismo.ToString, 105, 555, 0)
        'cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mNombreOrganismo.ToString, 105, 545, 0)
        'cb.EndText()








        'TITULO y HORA de la IMPRESION
        cb = writer.DirectContent
        cb.BeginText()





        mFuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 20)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "LISTADO DE DEUDA", 300, 505, 0)



        'Datos de la municipalidad
        If (dsDatosMunicipalidad IsNot Nothing) Then


            'Pie del logo
            mFuente = FontFactory.GetFont(FontFactory.TIMES, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
            cb.SetFontAndSize(mFuente, 8)


            mNombreMunicipalidad_AUX = ""
            mNombreMunicipalidad_AUX = ClsTools.ObtenerNombreMunicipalidad()

            If (CBool(System.Configuration.ConfigurationManager.AppSettings("NombreMunicipalidad").ToString.Trim)) Then
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Municipalidad de " & mNombreMunicipalidad_AUX.ToString.Trim, 75, 515, 0)
            End If

            If (CBool(System.Configuration.ConfigurationManager.AppSettings("ProvinciaBuenosAires").ToString.Trim)) Then
                cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Pcia. de Buenos Aires", 75, 505, 0)
            End If

            mFuente = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
            cb.SetFontAndSize(mFuente, 12)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Municipalidad de " & mNombreMunicipalidad_AUX.ToString.Trim, 820, 541, 0)


            mFuente = FontFactory.GetFont(FontFactory.TIMES, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
            cb.SetFontAndSize(mFuente, 8)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_DIRECCION").ToString.Trim, 820, 529, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_TELEFONO").ToString.Trim, 820, 517, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, dsDatosMunicipalidad.Tables("MUNICIPALIDAD").Rows(0).Item("MUNICIPALIDAD_MAIL").ToString.Trim, 820, 505, 0)
        End If


        cb.EndText()
    End Sub

    'Este Porcedimiento DIBUJA los DATOS del ENCABEZADO del DOCUMENTO
    Private Sub CrearEncabezado(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim mFuente As iTextSharp.text.pdf.BaseFont
        Dim mFuenteNegrita As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        Dim mValorY As Integer

        cb = writer.DirectContent

        'Rectangulo Contenedora 
        cb.SetLineWidth(1)
        cb.Rectangle(20, 440, 800, 60)

        cb.Stroke()


        mFuente = FontFactory.GetFont(FontFactory.TIMES_ITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        mFuenteNegrita = FontFactory.GetFont(FontFactory.TIMES_BOLDITALIC, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(mFuente, 10)




        cb.BeginText()
        mValorY = 480
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Nro. de Cuenta: ", 25, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mDatosContribuyente(1).ToString, 94, mValorY, 0)
        

        mValorY = mValorY - 15
        cb.SetFontAndSize(mFuente, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Tipo de Cuenta: ", 25, mValorY, 0)
        cb.SetFontAndSize(mFuenteNegrita, 10)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, mDatosContribuyente(0).ToString, 94, mValorY, 0)
        
        
        cb.EndText()

    End Sub

    'Este Procedimiento DIBUJA los REGISTROS SELECCIONADOS, como tambien DIBUJA los TOTALES del PIE
    Private Sub TablaMovimientos(ByVal mDocumentoPDF As Document, ByVal writer As PdfWriter)
        Dim cb As PdfContentByte
        Dim mColumnas As SColumnas
        Dim mRenglon As Integer
        Dim mRenglonTotales As Integer


        cb = writer.DirectContent


        'Rectangulo Contenedora 
        cb.Rectangle(20, 50, 800, 380)


        'Linea superior
        cb.MoveTo(20, 410)
        cb.LineTo(820, 410)
        cb.Stroke()




        mColumnas.Recurso = 45
        mColumnas.Anio = 90
        mColumnas.Cuota = 130
        mColumnas.Concepto = 160
        mColumnas.Plan = 310
        mColumnas.FechaVencimiento = 390
        mColumnas.CondicionEspecial = 480
        mColumnas.ImporteOrigen = 600
        mColumnas.ImporteRecargo = 700
        mColumnas.ImporteTotal = 800


        cb.BeginText()
        Dim fuente As iTextSharp.text.pdf.BaseFont


        'mRenglon = 454
        mRenglon = 434
        mRenglon = mRenglon - 17
        fuente = FontFactory.GetFont(FontFactory.TIMES_BOLD, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 8)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Recurso", mColumnas.Recurso, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Año", mColumnas.Anio, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Cuota", mColumnas.Cuota, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Concepto", mColumnas.Concepto, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Plan", mColumnas.Plan, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Fecha Vencimiento", mColumnas.FechaVencimiento, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Condición Especial", mColumnas.CondicionEspecial, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Importe Origen", mColumnas.ImporteOrigen, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Importe Recargo", mColumnas.ImporteRecargo, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Importe Total", mColumnas.ImporteTotal, mRenglon, 0)




        Dim i As Integer
        Dim mBandera As Integer
        mRenglon = mRenglon - 20
        fuente = FontFactory.GetFont(FontFactory.TIMES_ROMAN, iTextSharp.text.Font.NORMAL, iTextSharp.text.Font.NORMAL).BaseFont
        cb.SetFontAndSize(fuente, 8)
        mBandera = -1





        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Página 1 de 1", 795, 25, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Fecha / Hora de Impresión: " & Format(Now, "dd/MM/yyy").ToString & "  -  " & Format(Now, "HH:mm:ss"), 102, 25, 0)

        mRenglon = mRenglon - 15
        fuente = FontFactory.GetFont(FontFactory.TIMES_BOLD, iTextSharp.text.Font.BOLD, iTextSharp.text.Font.BOLD).BaseFont
        cb.SetFontAndSize(fuente, 9)
        cb.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "TOTALES:", mColumnas.CondicionEspecial, mRenglon, 0)


        'Obtengo el Renglon donde estan los TOTALES
        mRenglonTotales = 0
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, Format(0, "N2"), mColumnas.ImporteOrigen, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, Format(0, "N2"), mColumnas.ImporteRecargo, mRenglon, 0)
        cb.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, Format(0, "N2"), mColumnas.ImporteTotal, mRenglon, 0)

        cb.EndText()

    End Sub

#End Region

End Class
