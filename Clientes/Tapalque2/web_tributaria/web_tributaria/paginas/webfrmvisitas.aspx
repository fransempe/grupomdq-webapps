<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmvisitas.aspx.vb" Inherits="web_tributaria.webfrmvisitas" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link  href="../css/generica.css" rel="stylesheet" type="text/css" />
<link  href="../css/botones.css" rel="stylesheet" type="text/css" />
<script src="../js/funciones.js" type="text/javascript"></script>
<script src="../js/graphs.js" type="text/javascript"></script>
<script src="../js/graphs_seteo.js" type="text/javascript"></script>



<script type="text/javascript">

    function createGraph(p_sVisitas) {
        var arrVisitas = new Array();
        var arrVisitasInmuebles = new Array();
        var arrVisitasComercios = new Array();
        var arrVisitasVehiculos = new Array();
        var arrVisitasCementerios = new Array();
        var arrVisitasContribuyentes = new Array();      

        arrVisitas = p_sVisitas.split(';');       
        arrVisitasInmuebles = arrVisitas[0].split('|');
        arrVisitasComercios = arrVisitas[1].split('|');
        arrVisitasVehiculos = arrVisitas[2].split('|');
        arrVisitasCementerios = arrVisitas[3].split('|');
        arrVisitasContribuyentes = arrVisitas[4].split('|');


        /* valores de los subindices: */
        /* 0: Tipo imponible */
        /* 1: Tota consultas */
        /* 2: Total consultas e impresiones */
        /* 3: Total por imponible */
             
        var graph = new BAR_GRAPH("vBar");
        graph.values = arrVisitasInmuebles[3] + ',' + arrVisitasComercios[3] + ',' + arrVisitasVehiculos[3] + ',' + arrVisitasCementerios[3] + ',' + arrVisitasContribuyentes[3];        
        graph.labels = "Inmueble, Comercio, Veh�culo, Cementerio, Contribuyente";
        graph.showValues = 1;
        graph.barWidth = 30;
        graph.barLength = 1;
        graph.labelSize = 12;
        graph.absValuesSize = 12;
        graph.percValuesSize = 12;
        graph.graphPadding = 10;
        graph.graphBGColor = getGraphBGColor();
        graph.graphBorder = getGraphBorder();
        graph.barColors = getBarColors();
        graph.barBGColor = getBarBGColor();
        graph.barBorder = "2px outset white";
        graph.labelColor = "#000000";
        graph.labelBGColor = getLabelBGColor();
        graph.labelBorder = "2px groove white";
        graph.absValuesColor = "#000000";
        graph.absValuesBGColor = "#FFFFFF";
        graph.absValuesBorder = "2px groove white";        
        document.getElementById('divGraph').innerHTML = graph.create();
    }



    function createTablaDatos(p_sVisitas) {
        var arrVisitas = new Array();
        var arrVisitasInmuebles = new Array();
        var arrVisitasComercios = new Array();
        var arrVisitasVehiculos = new Array();
        var arrVisitasCementerios = new Array();
        var arrVisitasContribuyentes = new Array();
        var totalGeneral
        var shtml_tabla;

        arrVisitas = p_sVisitas.split(';');
        arrVisitasInmuebles = arrVisitas[0].split('|');
        arrVisitasComercios = arrVisitas[1].split('|');
        arrVisitasVehiculos = arrVisitas[2].split('|');
        arrVisitasCementerios = arrVisitas[3].split('|');
        arrVisitasContribuyentes = arrVisitas[4].split('|');


        /* valores de los subindices: */
        /* 0: Tipo imponible */
        /* 1: Tota consultas */
        /* 2: Total consultas e impresiones */
        /* 3: Total por imponible */


        totalGeneral = parseInt(arrVisitasInmuebles[3]) + parseInt(arrVisitasComercios[3]) + 
                       parseInt(arrVisitasVehiculos[3]) + parseInt(arrVisitasCementerios[3]) + parseInt(arrVisitasContribuyentes[3]);
        shtml_tabla = '<div align="center" style="padding-top:15px; padding-bottom:10px;">' +
                        '<table border="1" style="border-collapse: collapse;">' +
                        '<caption style="background-color:#999999; color:#FFFFFF;"><strong>Cantidad de visitas</strong></caption>' +
	                        '<tr>' +
		                        '<td style="width:120px;"></td>' +
		                        '<td style="width:100px;" align="center">Inmueble</td>' +
		                        '<td style="width:100px;" align="center">Comercio</td>' +
		                        '<td style="width:100px;" align="center">Veh�culo</td>' +
		                        '<td style="width:100px;" align="center">Cementerio</td>' +
		                        '<td style="width:100px;" align="center">Contribuyente</td>' +
	                        '</tr>' +
	                        '<tr>' +
		                        '<td  align="left" style="padding-left:3px;">Solo consultas</td>' +
		                        '<td align="center">' + arrVisitasInmuebles[1] + '</td>' +
		                        '<td align="center">' + arrVisitasComercios[1] + '</td>' +
		                        '<td align="center">' + arrVisitasVehiculos[1] + '</td>' +
		                        '<td align="center">' + arrVisitasCementerios[1] + '</td>' +
		                        '<td align="center">' + arrVisitasContribuyentes[1] + '</td>' +
	                        '</tr>' +
	                        '<tr>' +
		                        '<td align="left" style="padding-left:3px;">Consultas e impresiones</td>' +
		                        '<td align="center">' + arrVisitasInmuebles[2] + '</td>' +
		                        '<td align="center">' + arrVisitasComercios[2] + '</td>' +
		                        '<td align="center">' + arrVisitasVehiculos[2] + '</td>' +
		                        '<td align="center">' + arrVisitasCementerios[2] + '</td>' +
		                        '<td align="center">' + arrVisitasContribuyentes[2] + '</td>' +
	                        '</tr>' +
	                        '<tr>' +
		                        '<td align="left" style="padding-left:3px;">Totales por cuenta</td>' +
		                        '<td align="center">' + arrVisitasInmuebles[3] + '</td>' +
		                        '<td align="center">' + arrVisitasComercios[3] + '</td>' +
		                        '<td align="center">' + arrVisitasVehiculos[3] + '</td>' +
		                        '<td align="center">' + arrVisitasCementerios[3] + '</td>' +
		                        '<td align="center">' + arrVisitasContribuyentes[3] + '</td>' +
	                        '</tr>' +
                            '<tr>' +
		                        '<td align="center" colspan="6"><strong>Total general: ' + totalGeneral + '</strong></td>' +
	                        '</tr>' +

                        '</table>' +
                    '</div>';
                   

        window.document.getElementById('div_tabla_datos').innerHTML = shtml_tabla;        
    }
    
</script>






      <!-- ajax -->       
        <script type="text/javascript" language="javascript" src="../js/jquery1-2-6-min.js"></script>
        <script type="text/javascript" language="javascript">


            function ajax_load_visitas() {
                var i_index;
                var s_periodo;
            
                _html = '<div id="loading" align="center">' +
            	            '<table border= "0">' +
	                            '<tr>' +
	                                '<td align="right" style="width:50%;"><img id="img_load" alt="" src="../imagenes/loading.gif" /></td>' +
	                                '<td align="left" style="width:50%;">Cargando datos ......</td>' +
	                            '</tr>' +
	                        '</table>' +
	                    '</div>';
                window.document.getElementById('div_tabla_datos').innerHTML = '';
                window.document.getElementById('divGraph').innerHTML = _html;


                /* Obtengo el periodo seleccionado */
                i_index = window.document.forms['formulario'].cbperiodo.selectedIndex;
                s_periodo = window.document.forms['formulario'].cbperiodo.options[i_index].value;                                           
                             
                                 
                jQuery.ajax({
                    type:"POST",
                    url: "../controladores/handler_visitas.ashx",
                    data: "periodo=" + s_periodo,                                                      
                    success: see_response
                });
            }            
            
            function see_response(html){    
                var html2;                                           
                if (html != '') {
                    createGraph(html);
                    createTablaDatos(html);
                              
                    return false;
                }
                
            return true;    
            }          
     </script>




<script type="text/javascript">

   
    function carga_inicial() {ajax_load_visitas();}
    
    //Asigna estilo al boton que obtiene la cuenta por dominio
    function boton_consulta_onmouseover(boton) {boton.className = 'boton_dominio_mouseover'; }

    //Asigna estilo al boton que obtiene la cuenta por dominio
    function boton_consulta_onmouseout(boton) { boton.className = 'boton_dominio_mouseout';}
    

</script>



<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Sistema tributario :: Contador de visitas</title>
    <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />



<link href="../css/webfrmconsulta_b.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            #footer
            {
                height: 0px;
            }
                        
            .columna_titulo{
            	width: 40%;
            	text-align:center;
            }
            
            .columna_dato{
            	width: 60%;
            	text-align:center;                  	          
            }
            
            .textbox {
                 text-align:center;
                 margin:2px;
            }
            
            
            .textbox_captcha {
                 text-align:center;
                  margin-bottom:2px;
            }
            
                        
            .titulo_tabla{
	            background-image: url('../imagenes/barra_cabecera.png');
	            color:#FFFFFF;
	            font-size:12px;
	            text-align:center;
	            border-left:1px solid #FFFFFF;
	            height:40px;
            }
                             
            
        </style>
        
               
        
        <style type="text/css">        
            table_visitas {                
                border: 1px solid #000;
                border-collapse: collapse;
                caption-side: top;
            }
            
            table_visitas_th, table_visitas_td {
                width: 80px;
                text-align:center;
                vertical-align: top;
                border: 1px solid #000;
                padding: 0.3em;
            }
            
            table_visitas_caption {
                padding: 0.3em;
                color: #fff;
                background: #000;
            }
       
        </style>
        
        
    	        
</head>

<body onload="carga_inicial()">

    <form id="formulario" class="form"  runat="server">
        <div id="div_main" class="main">
        
            <div id="header">  
                <div class="div_font" style="padding:0px 5px 0px 5px;">
                    <a href="#" title="Usar fuente menor" class="link_font" onclick="assign_font('-');">A-</a> 
                </div>
                
                <div class="div_font">
		            <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 
                </div>		            
                                                 
                <span id="logo-text">Consulta tributaria :: Visitas</span>  
                <h2 id="slogan">Municipalidad de <asp:Label ID="lblmunicipalidad_name" runat="server" Text=""></asp:Label></h2>			                    
            </div>
        
        
            <div id="div_contenedor" class="content" runat="server">
	    	    <div class="barra">
                    <strong>&nbsp;SISTEMA TRIBUTARIO :: CONTADOR DE VISITAS</strong>
                </div>                                          
                            
                <div id ="tabla" style="width: 872px; padding: 10px 0px 0px 0px;" align="center"; >                    
                    <div>
	                    <table>
		                    <tr>
			                    <td>Seleccione un per�odo:</td>
			                    <td> 
				                    <select name="cbperiodo" style="width:200px;">
					                    <option value="hoy">Hoy</option>
					                    <option value="semana">Semana (�ltimos siete d�as)</option>					
					                    <option value="mes">Mes (�ltimos treinta d�as)</option>					
					                    <option value="total" selected="selected">Ver todo (Desde el inicio)</option>
				                    </select>
			                    </td>
			                    <td>
			                        <input name="btnconsultar" type="button" value="Consultar"  onclick="javascript:ajax_load_visitas();"
			                         class="boton_dominio_mouseout"			                           
			                         onmouseover="javascript:boton_consulta_onmouseover(this);"
                                     onmouseout="javascript:boton_consulta_onmouseout(this);"
			                         title="Consultar per�odo." 
                                     style="cursor:pointer;" />
                                </td>
		                    </tr>
	                    </table>
                    </div>
                
                    <div id="divGraph"></div>                                        
                    <div id="div_tabla_datos"></div>                    
                    
                <div>
            </div>                                                       
        </div>    
                                
                                
                                
        <div style="height: 15px; font-weight: bold; font-size: small; text-align: center; font-family: 'Courier New', Courier, 'espacio sencillo';" class="barra">
             
        </div>
         
                                    
	    </div>
        <div style="padding: 15px 5px 0px 0px; text-align:right;">
            <a href="webfrmindex.aspx" class="link">[Volver]</a>
            <br />
            <a href="http://www.grupomdq.com" target="_blank" class="link">
                [Desarrollado por Grupo MDQ]
            </a>
            <br />
            <asp:Label ID="lblversion" runat="server"></asp:Label>
        </div>
                    
            </div>
            
        </form>
    </body>
</html>



