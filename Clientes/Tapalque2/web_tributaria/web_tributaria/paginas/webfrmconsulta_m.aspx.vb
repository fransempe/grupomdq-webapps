﻿Option Explicit On
Option Strict On


Imports web_tributaria.ws_consulta_tributaria.Service1

Partial Public Class webfrmconsulta_m
    Inherits System.Web.UI.Page

#Region "Variables"

    Private mWS As web_tributaria.ws_consulta_tributaria.Service1
    Private mLog As Clslog
    Private mXml As String

#End Region

#Region "Eventos Formulario"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not (IsPostBack) Then

            Call CargarDatosMunicipalidad()

            link_consultar_deuda.Attributes.Add("onclick", "javascript:return validar_datos();")
            txtnro_comprobante1.Attributes.Add("onblur", "javascript:completar_ceros_derecha(this, 9, '0');")
            txtnro_comprobante2.Attributes.Add("onblur", "javascript:completar_ceros_derecha(this, 9, '0');")
            txtcaptcha.Attributes.Add("onkeydown", "javascript:consultar_deuda(this, event);")

            txtnro_cuenta.Focus()
        Else
            Call LimpiarControles()
        End If
        Call CargarListbox()

    End Sub

    Protected Sub link_consultar_deuda_Click(ByVal sender As Object, ByVal e As EventArgs) Handles link_consultar_deuda.Click
        Dim mLogin As Integer
        Dim dsDatos As DataSet


        Try
            If (Validar()) Then


                mWS = New ws_consulta_tributaria.Service1
                mXml = mWS.ConsultarDeuda(cmbtipo_cuenta.Text, CInt(txtnro_cuenta.Text.Trim), -1)
                mWS = Nothing

                dsDatos = New DataSet
                dsDatos = ObtenerDatosXML()

                If (dsDatos.Tables("REGISTRO_NOTAS") IsNot Nothing) Then
                    If (dsDatos.Tables("REGISTRO_NOTAS").Rows(0).Item("MENSAJE").ToString = "Cuenta Inexistente") Then

                        lblmensaje.Text = "Cuenta Inexistente."
                        txtnro_cuenta.BackColor = Drawing.Color.Red
                        txtnro_cuenta.ForeColor = Drawing.Color.White
                        txtcaptcha.Text = ""
                        txtnro_cuenta.Focus()
                        Exit Sub
                    End If
                End If




                mLogin = 0
                mWS = New ws_consulta_tributaria.Service1
                mLogin = mWS.Login(cmbtipo_cuenta.Text, CInt(txtnro_cuenta.Text.Trim), _
                                   CInt(txtgrupo_comprobante1.Text.Trim), CInt(txtnro_comprobante1.Text.Trim), CInt(txtnro_verificador1.Text.Trim), _
                                   CInt(txtgrupo_comprobante2.Text.Trim), CInt(txtnro_comprobante2.Text.Trim), CInt(txtnro_verificador2.Text.Trim))
                mWS = Nothing



                If (mLogin = 1) Then

                    'Asigno Variables de Session
                    Session("tipo_cuenta") = cmbtipo_cuenta.Text
                    Session("nro_cuenta") = txtnro_cuenta.Text.Trim

                    Call CargarDatosMunicipalidad()
                    Response.Redirect("webfrmlistado.aspx", False)

                Else

                    Call CrearAuditoria()

                    'txtnro_cuenta.Text = ""
                    'cmbtipo_cuenta.SelectedIndex = 0
                    'txtgrupo_comprobante1.Text = ""
                    'txtnro_comprobante1.Text = ""
                    'txtnro_verificador1.Text = ""
                    'txtgrupo_comprobante2.Text = ""
                    'txtnro_comprobante2.Text = ""
                    'txtnro_verificador2.Text = ""
                    'txtcaptcha.Text = ""


                    lblmensaje.Text = "Número de Comprobante Incorrecto."
                    txtcaptcha.Text = ""
                    If (mLogin = -1) Then
                        txtgrupo_comprobante1.BackColor = Drawing.Color.Red
                        txtgrupo_comprobante1.ForeColor = Drawing.Color.White
                        txtnro_comprobante1.BackColor = Drawing.Color.Red
                        txtnro_comprobante1.ForeColor = Drawing.Color.White
                        txtnro_verificador1.BackColor = Drawing.Color.Red
                        txtnro_verificador1.ForeColor = Drawing.Color.White
                        txtgrupo_comprobante1.Focus()
                    Else
                        txtgrupo_comprobante2.BackColor = Drawing.Color.Red
                        txtgrupo_comprobante2.ForeColor = Drawing.Color.White
                        txtnro_comprobante2.BackColor = Drawing.Color.Red
                        txtnro_comprobante2.ForeColor = Drawing.Color.White
                        txtnro_verificador2.BackColor = Drawing.Color.Red
                        txtnro_verificador2.ForeColor = Drawing.Color.White
                        txtgrupo_comprobante2.Focus()
                    End If


                    Call LimpiarSession()
                End If
            End If

        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)
        End Try
    End Sub

#End Region

#Region "Procedimientos y Funciones"

    Private Function Validar() As Boolean

        'Nro. Cuenta
        If (txtnro_cuenta.Text <> "") Then
            If Not (IsNumeric(txtnro_cuenta.Text)) Then
                Return False
            End If
        Else
            Return False
        End If


        'Nro. Grupo1
        If (txtgrupo_comprobante1.Text <> "") Then
            If Not (IsNumeric(txtgrupo_comprobante1.Text)) Then
                Return False
            End If
        Else
            Return False
        End If


        'Nro. Comprobante 1
        If (txtnro_comprobante1.Text <> "") Then
            If Not (IsNumeric(txtnro_comprobante1.Text)) Then
                Return False
            End If
        Else
            Return False
        End If


        'Nro. Verificador 1
        If (txtnro_verificador1.Text <> "") Then
            If Not (IsNumeric(txtnro_verificador1.Text)) Then
                Return False
            End If
        Else
            Return False
        End If




        'Nro. Grupo2
        If (txtgrupo_comprobante2.Text <> "") Then
            If Not (IsNumeric(txtgrupo_comprobante2.Text)) Then
                Return False
            End If
        Else
            Return False
        End If


        'Nro. Comprobante 2
        If (txtnro_comprobante2.Text <> "") Then
            If Not (IsNumeric(txtnro_comprobante2.Text)) Then
                Return False
            End If
        Else
            Return False
        End If


        'Nro. Verificador 1
        If (txtnro_verificador2.Text <> "") Then
            If Not (IsNumeric(txtnro_verificador2.Text)) Then
                Return False
            End If
        Else
            Return False
        End If


        'Validacion Captcha
        verificador_captcha.ValidateCaptcha(txtcaptcha.Text.Trim())
        If Not (verificador_captcha.UserValidated) Then
            lblmensaje.Text = "Código de Verificación Incorrecto."
            txtcaptcha.BackColor = Drawing.Color.Red
            txtcaptcha.ForeColor = Drawing.Color.White
            txtcaptcha.Text = ""
            txtcaptcha.Focus()

            Return False
        End If


        Return True
    End Function

    Private Sub LimpiarSession()
        Session.RemoveAll()
        Session.Clear()
        Session.Abandon()
    End Sub

#End Region

#Region "LOG"

    Private Function ObtenerDetalleWEB() As String
        Dim mInfoLog As String = ""
        Try

            For Each mNombreVariable In Request.ServerVariables
                mInfoLog = mInfoLog & mNombreVariable.ToString & ": " & Request.ServerVariables(mNombreVariable.ToString)
                mInfoLog = mInfoLog & vbCrLf
            Next

            Return mInfoLog.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function
#End Region

#Region "Auditoria"

    Private Sub CrearAuditoria()
        Dim mRutaFisica As String
        mRutaFisica = Request.ServerVariables("APPL_PHYSICAL_PATH").ToString & "auditoria"

        'Controlo la Cantidad de Archivos
        Call CantidadLog(1000, mRutaFisica.ToString)

        'Genero el TXT
        Call GenerarLogAuditoria(mRutaFisica.ToString)
    End Sub

    Private Sub CantidadLog(ByVal mCantidadLog As Integer, ByVal mRutaFisica As String)

        If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString)) Then
            My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString)
        End If


        Dim mArchivos As System.Collections.ObjectModel.ReadOnlyCollection(Of String)
        Dim mNombreCarpeta As String = ""
        mArchivos = My.Computer.FileSystem.GetFiles(mRutaFisica.ToString)



        Dim c As Integer
        Dim arreglo(mArchivos.Count() - 1) As String
        For c = 0 To UBound(arreglo, 1)
            arreglo(c) = mArchivos(c)
        Next


        If mCantidadLog = 0 Then
            Dim k As Integer
            For k = 0 To (UBound(arreglo, 1)) - 1
                mNombreCarpeta = My.Computer.FileSystem.GetName(Trim(arreglo(k).ToString))
                My.Computer.FileSystem.DeleteFile(Trim(arreglo(k).ToString))
            Next


        Else
            If mArchivos.Count > mCantidadLog Then
                Dim i As Integer
                For i = UBound(arreglo, 1) - (mCantidadLog) To 0 Step -1
                    mNombreCarpeta = My.Computer.FileSystem.GetName(arreglo(i))
                    My.Computer.FileSystem.DeleteFile(Trim(arreglo(i).ToString))
                Next
            End If
        End If


    End Sub

    Private Sub GenerarLogAuditoria(ByVal mRutaFisica As String)
        Dim mCadena As String


        If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString)) Then
            My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString)
        End If


        'Armo el Log
        Dim mLog As New IO.StreamWriter(mRutaFisica.ToString & "\auditorialog" & Format(Now, "ddMMyyyyHHmmss") & ".txt")
        mCadena = "FECHA: " & Format(Now, "dd/MM/yy hh:mm:ss") & "|"
        mCadena = mCadena & "IP: " & Request.ServerVariables("REMOTE_ADDR").ToString() & "|"
        mCadena = mCadena & "HOST: " & Request.ServerVariables("REMOTE_HOST").ToString() & "|"
        mCadena = mCadena & "PUERTO: " & Request.ServerVariables("REMOTE_PORT").ToString()

        mLog.WriteLine(mCadena.ToString)


        mLog.WriteLine("")
        mLog.WriteLine("-- DATOS INGRESADOS --")


        mLog.WriteLine("NRO. CUENTA:        " & txtnro_cuenta.Text)
        mLog.WriteLine("TIPO CUENTA:        " & cmbtipo_cuenta.Text)
        mLog.WriteLine("GRUPO COMPROBANTE1: " & txtgrupo_comprobante1.Text)
        mLog.WriteLine("NRO. COMPROBANTE1:  " & txtnro_comprobante1.Text)
        mLog.WriteLine("NRO. VERIFICADOR1:  " & txtnro_verificador1.Text)
        mLog.WriteLine("GRUPO COMPROBANTE2: " & txtgrupo_comprobante2.Text)
        mLog.WriteLine("NRO. COMPROBANTE2:  " & txtnro_comprobante2.Text)
        mLog.WriteLine("NRO. VERIFICADOR2:  " & txtnro_verificador2.Text)

        mLog.Close()

    End Sub

#End Region


    Private Function ObtenerDatosXML() As DataSet
        Dim mRutaFisica As String
        Dim mArchivo As String
        Dim dsDatos As DataSet

        Try

            mRutaFisica = Request.ServerVariables("APPL_PHYSICAL_PATH").ToString

            If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString & "datos_temporales")) Then
                My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString & "datos_temporales")
            End If

            mArchivo = mRutaFisica.ToString & "datos_temporales\" & Format(Now, "ddMMyyyyHHmmss") & ".xml"


            Dim mCrearXML As New IO.StreamWriter(mArchivo.ToString)
            mCrearXML.WriteLine(mXml.ToString)
            mCrearXML.Close()

            dsDatos = New DataSet
            dsDatos.ReadXml(mArchivo.ToString)

            My.Computer.FileSystem.DeleteFile(mArchivo.ToString)


            Return dsDatos
        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
            Response.Redirect("webfrmerror.aspx", False)
            Return Nothing
        End Try
    End Function

    Private Sub LimpiarControles()
        txtnro_cuenta.BackColor = Drawing.Color.White
        txtnro_cuenta.ForeColor = Drawing.Color.Black
        txtgrupo_comprobante1.BackColor = Drawing.Color.White
        txtgrupo_comprobante1.ForeColor = Drawing.Color.Black
        txtnro_comprobante1.BackColor = Drawing.Color.White
        txtnro_comprobante1.ForeColor = Drawing.Color.Black
        txtnro_verificador1.BackColor = Drawing.Color.White
        txtnro_verificador1.ForeColor = Drawing.Color.Black
        txtgrupo_comprobante2.BackColor = Drawing.Color.White
        txtgrupo_comprobante2.ForeColor = Drawing.Color.Black
        txtnro_comprobante2.BackColor = Drawing.Color.White
        txtnro_comprobante2.ForeColor = Drawing.Color.Black
        txtnro_verificador2.BackColor = Drawing.Color.White
        txtnro_verificador2.ForeColor = Drawing.Color.Black
        txtcaptcha.BackColor = Drawing.Color.White
        txtcaptcha.ForeColor = Drawing.Color.Black
    End Sub


    Private Sub CargarDatosMunicipalidad()
        Dim dsdatos As DataSet

        Try

            mWS = New ws_consulta_tributaria.Service1
            mXml = mWS.ObtenerDatosMunicipalidad()
            mWS = Nothing

            dsdatos = New DataSet
            dsdatos = ObtenerDatosXML()
            If (dsdatos.Tables("DATOSCONTACTOCONMUNICIPALIDAD").Rows(0).Item("NROTELRECLAMOSWEB").ToString = "") Then
                lbltelefono.Text = "00-000000"
            Else
                lbltelefono.Text = dsdatos.Tables("DATOSCONTACTOCONMUNICIPALIDAD").Rows(0).Item("NROTELRECLAMOSWEB").ToString
            End If

            If (dsdatos.Tables("DATOSCONTACTOCONMUNICIPALIDAD").Rows(0).Item("DIRMAILRECLAMOSWEB").ToString = "") Then
                lblmail.Text = "mail@mail.com.ar"
            Else
                lblmail.Text = dsdatos.Tables("DATOSCONTACTOCONMUNICIPALIDAD").Rows(0).Item("DIRMAILRECLAMOSWEB").ToString
            End If
            dsdatos = Nothing


            Session("municipalidad_telefono") = lbltelefono.Text
            Session("municipalidad_mail") = lblmail.Text


        Catch ex As Exception
            dsdatos = Nothing
        End Try
    End Sub

    Private Sub CargarListbox()
        Dim items As String
        Try
            items = ""

            'Este fragmento de codigo pregunta que conexion tiene y en base a eso guarda el metodo corresp.
            If (Application("tipo_conexion").ToString = "FOX") Then
                Me.mWS = New ws_consulta_tributaria.Service1
                items = Me.mWS.getObtenerTiposImponible()
            ElseIf (Application("tipo_conexion").ToString = "RAFAM") Then
                Me.mWS = New ws_consulta_tributaria.Service1
                items = Me.mWS.ObtenerTiposImponible()
            End If


            If cmbtipo_cuenta.Items.Count = 0 Then 'Lee si el list esta vacio.
                If items.ToCharArray = "FALSE" Or items = "" Or items = "0" Then
                    cmbtipo_cuenta.Items.Add("Inmueble")
                    cmbtipo_cuenta.Items.Add("Comercio")
                    'cmbtipo_cuentas.Items.Add("Contribuyente")
                    cmbtipo_cuenta.Items.Add("Cementerio")
                    If (Application("tipo_conexion").ToString = "FOX") Then
                        cmbtipo_cuenta.Items.Add("Vehículo")
                    ElseIf (Application("tipo_conexion").ToString = "RAFAM") Then
                        cmbtipo_cuenta.Items.Add("Rodado")
                    End If
                Else
                    For n = 0 To Len(items) 'Carga listbox.
                        Select Case items.Chars(n)
                            Case CChar("I")
                                cmbtipo_cuenta.Items.Add("Inmueble")
                            Case CChar("C")
                                cmbtipo_cuenta.Items.Add("Comercio")
                                'Case "N"
                                'cmbtipo_cuenta.Items.Add("Contribuyente")
                            Case CChar("E")
                                cmbtipo_cuenta.Items.Add("Cementerio")
                            Case CChar("V")
                                If (Application("tipo_conexion").ToString = "FOX") Then
                                    cmbtipo_cuenta.Items.Add("Vehículo")
                                End If
                            Case CChar("R")
                                If (Application("tipo_conexion").ToString = "RAFAM") Then
                                    cmbtipo_cuenta.Items.Add("Rodado")
                                End If
                        End Select
                    Next n
                End If
            End If

        Catch ex As Exception
            items = "0"
        Finally
            Me.mWS = Nothing
        End Try

    End Sub
End Class