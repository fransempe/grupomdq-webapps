﻿Option Explicit On
Option Strict On

Partial Public Class webfrminformectactevencida_sindeuda
    Inherits System.Web.UI.Page

#Region "Variables"
    Private mWS As web_tributaria.ws_consulta_tributaria.Service1
    Private mPDF As ClsCrearListadoVacioPDF
    Private mLog As Clslog
#End Region



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mRutaFisica As String
        Dim mNombreArchivoComprobante As String
        Dim mArchivo As String



        Try

            If Not (IsPostBack) Then
                If (Session("tipo_cuenta") IsNot Nothing) And (Session("nro_cuenta") IsNot Nothing) Then


                    'Creo Directorio y Archivo en Disco
                    mRutaFisica = Request.ServerVariables("APPL_PHYSICAL_PATH").ToString & "comprobantespdf"
                    If Not (My.Computer.FileSystem.DirectoryExists(mRutaFisica.ToString)) Then
                        My.Computer.FileSystem.CreateDirectory(mRutaFisica.ToString)
                    End If
                    mNombreArchivoComprobante = Session("tipo_cuenta").ToString & Session("nro_cuenta").ToString & Format(Now, "HHmmss")
                    mArchivo = mRutaFisica & "\" & mNombreArchivoComprobante & ".pdf"





                    'Genero el PDF
                    mPDF = New ClsCrearListadoVacioPDF
                    mPDF.TipoConexion = Application("tipo_conexion").ToString
                    Session("datos_contribuyente") = Session("tipo_cuenta").ToString.Trim & "|" & Session("nro_cuenta").ToString.Trim & "|" & ""

                    mPDF.DatosContribuyente = Session("datos_contribuyente").ToString.Split(CChar("|"))
                    mPDF.RutaFisica = mArchivo.ToString

                    mPDF.DatosMunicipalidad = Me.ObtenerDatosMunicipalidad()
                    mPDF.CrearPDF()



                    ' Si esta configurado para que el pdf salga con el cuadro de díalogo de impresión, redirecciono a la pagina "webfrmpdf.aspx"
                    If (CBool(System.Configuration.ConfigurationManager.AppSettings("CuadroDialogoImpresion").ToString.Trim)) Then
                        Session("pdf") = mNombreArchivoComprobante.Trim()
                        Response.Redirect("webfrmpdf.aspx", False)

                    Else

                        'Muestro el Comprobante por pantalla
                        Response.Expires = 0
                        Response.Buffer = True
                        Response.Clear()
                        Response.ContentType = "application/pdf"
                        Response.TransmitFile(mArchivo.ToString)
                        Response.Flush()
                    End If
                Else
                    Call LimpiarSession()
                End If
                End If


        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
            Response.Redirect("../webfrmerror.aspx", False)
        End Try

    End Sub


#Region "LOG"

    Private Function ObtenerDetalleWEB() As String
        Dim mInfoLog As String = ""
        Try

            For Each mNombreVariable In Request.ServerVariables
                mInfoLog = mInfoLog & mNombreVariable.ToString & ": " & Request.ServerVariables(mNombreVariable.ToString)
                mInfoLog = mInfoLog & vbCrLf
            Next

            Return mInfoLog.ToString
        Catch ex As Exception
            Return ""
        End Try
    End Function
#End Region

    Private Sub LimpiarSession()
        Session.RemoveAll()
        Session.Clear()
        Session.Abandon()
        Response.Redirect("../webfrmindex.aspx", False)
    End Sub


    Private Function ObtenerDatosMunicipalidad() As DataSet
        Dim dsDatos As DataSet
        Dim mXML As String

        Try

            'Obtengo los datos de la municipalidad
            mWS = New ws_consulta_tributaria.Service1
            mXML = mWS.ObtenerDatosMunicipalidad()
            mWS = Nothing


            dsDatos = New DataSet
            dsDatos = ClsTools.ObtainDataXML(mXML.ToString.Trim)

            Return dsDatos
        Catch ex As Exception
            mLog = New Clslog
            mLog.GenerarLog(ex, ObtenerDetalleWEB(), Request.ServerVariables("APPL_PHYSICAL_PATH").ToString)
            mLog = Nothing
            Response.Redirect("../webfrmerror.aspx", False)
            Return Nothing
        End Try

    End Function



End Class