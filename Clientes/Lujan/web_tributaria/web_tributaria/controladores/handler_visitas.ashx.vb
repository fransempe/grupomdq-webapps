﻿Option Explicit On
Option Strict On

Imports System.Web
Imports System.Web.Services

Public Class handler_visitas
    Implements System.Web.IHttpHandler

    Private WS As web_tributaria.ws_consulta_tributaria.Service1


    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim sPeriodoSeleccionado As String
        Dim dFechaDesde As Date
        Dim dFechaHasta As Date
        Dim sHTML As String
        Dim sResponse As String



        Try

            context.Response.ContentType = "text/plain"

            'I obtain the variables
            sPeriodoSeleccionado = context.Request.Form("periodo")
            Select Case sPeriodoSeleccionado.Trim
                Case "hoy"
                    dFechaDesde = Now
                    dFechaHasta = Now
                Case "semana"
                    dFechaDesde = Now.AddDays(-7)
                    dFechaHasta = Now
                Case "mes"
                    dFechaDesde = Now.AddDays(-30)
                    dFechaHasta = Now
                Case "total"
                    dFechaDesde = CDate("01/01/2011")
                    dFechaHasta = Now
            End Select




            sHTML = ""
            sHTML = Me.getVisitasDetalle(Format(dFechaDesde, "dd/MM/yyyy"), Format(dFechaHasta, "dd/MM/yyyy"))

            sResponse = sHTML.Trim
        Catch ex As Exception
            sResponse = "<span id='lblmessenger' style='color: Red; font-weight: bold;'>" & _
                            "Error al intentar recuperar los datos." & _
                        "</span>"
        End Try

        context.Response.Write(sResponse.ToString.Trim)
    End Sub

    Private Function getVisitasDetalle(ByVal p_sFechaDesde As String, ByVal p_sFechaHasta As String) As String
        Dim sVisitas As String

        Try
            Me.WS = New web_tributaria.ws_consulta_tributaria.Service1
            sVisitas = Me.WS.getVisitasCantidadDetalle(p_sFechaDesde.Trim, p_sFechaHasta.Trim)
        Catch ex As Exception
            sVisitas = ""
        Finally
            Me.WS = Nothing
        End Try

        Return sVisitas.Trim
    End Function



    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class