﻿Option Explicit On
Option Strict On

Imports WEB_TISH.WS_TISH.Service1

Partial Public Class webfrmcargaractividades
    Inherits System.Web.UI.Page

    Private mWS As WEB_TISH.WS_TISH.Service1
    Private ExData As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btncargar.Attributes.Add("onclick", "javascript:regresar();")

        Call CargarActividades(CInt(Session("mnro_comercio")))
    End Sub

    Private Sub gvactividades_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvactividades.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("OnClick") = Page.ClientScript.GetPostBackClientHyperlink(Me.gvactividades, "Select$" + e.Row.RowIndex.ToString)
        End If
    End Sub

    Private Sub CargarActividades(ByVal mNroComercio As Integer)
        Dim mDataSet As DataSet
        Dim mXML As String

        Try

            mWS = New web_tish.ws_tish.Service1
            mXML = mWS.ObtengoActividades(mNroComercio)


            mDataSet = New DataSet
            mDataSet.ReadXml(New IO.StringReader(mXML))

            gvactividades.DataSource = mDataSet.Tables("Actividades")
            gvactividades.DataBind()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub gvactividades_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gvactividades.SelectedIndexChanged
        lblcodigo.Text = gvactividades.SelectedRow.Cells(0).Text
        lbldescripcion.Text = gvactividades.SelectedRow.Cells(1).Text
        lblalicuota.Text = gvactividades.SelectedRow.Cells(2).Text
    End Sub

End Class