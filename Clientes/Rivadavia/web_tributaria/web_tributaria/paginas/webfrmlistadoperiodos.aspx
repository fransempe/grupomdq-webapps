<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmlistadoperiodos.aspx.vb" Inherits="web_tish.webfrmlistadoperiodos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sistema TISH.-</title>


	<!-- Archivos para los mensajes -->
    <script src="../alertas_js/jquery.js" type="text/javascript"></script>
    <script src="../alertas_js/jquery.ui.draggable.js" type="text/javascript"></script>	
	<script src="../alertas_js/jquery.alerts.js" type="text/javascript"></script>
    <link href="../alertas_js/jquery.alerts.css" rel="stylesheet" type="text/css" media="screen" />

 


<script src="../js/funciones.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">


    //Pregunto al Usuario si confirma el cierre de session   
    function cerrar() {
        return cerrar_sesion_usuario();
    }
    
 
    
</script>


<link href="../css/style.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            .style1
            {
                width: 121px;
            }
            #footer
            {
                height: 23px;
            }
            .style2
            {
                width: 79px;
            }
            .style3
            {
                width: 109px;
            }
        </style>
</head>

<body>
   <form id="form1" runat="server">
<div class="main"><div id="header"><div class="top"><div class="title"><h1>Sistema 
    de Liquidaci�n de Seguridad e Higiene.</h1>
                </div></div>
																					<div class="menu"><ul class="navbar">
		 <li><a href="#" class="nav1">LISTADO DE CTA. CTE. DE SEGURIDAD E HIGIENE</a></li>
</ul></div>
            &nbsp;&nbsp;&nbsp;
            <div align="right">
                <asp:LinkButton ID="link_informe_cta_cte" runat="server" >[Informe CTA. CTE.]</asp:LinkButton>
                &nbsp;&nbsp;<asp:LinkButton ID="link_alta_ddjj" runat="server" >[Alta de DDJJ]</asp:LinkButton>
                &nbsp; <asp:LinkButton ID="link_cerrar_session" runat="server" >[Cerrar Sesion]</asp:LinkButton>
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
</div>
<div class="content">
											<div style="float:left; width:723px; margin:10px 0; padding:0 10px; text-align:justify;" 
                                                align="center">
                                                <div align="center">
                                                    Datos del Comecio:<div style="height: 82px; width: 738px;">
                                                        &nbsp;
                                                    <table align="center" border="1" style="width: 96%; height: 78px;">
                                                        <tr>
                                                            <td class="style1">
                                                                Nro. de Comercio:</td>
                                                            <td class="style2">
                                                                <asp:Label ID="lblNroComercio" runat="server" Text="0.-"></asp:Label>
                                                            </td>
                                                            <td class="style3">
                                                                Nombre del Comercio:</td>
                                                            <td>
                                                                <asp:Label ID="lblNombreComercio" runat="server" Text="Nombre"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="style1">
                                                                CUIT</td>
                                                            <td class="style2">
                                                                <asp:Label ID="lblcuit" runat="server" Text="20-30506785-8"></asp:Label>
                                                            </td>
                                                            <td class="style3">
                                                                Contribuyente:</td>
                                                            <td>
                                                                <asp:Label ID="lblContribuyente" runat="server" Text="Contribuyente"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="style1">
                                                                Situacion IVA:</td>
                                                            <td class="style2">
                                                                <asp:Label ID="lblsituacion_iva" runat="server" Text="CF.-"></asp:Label>
                                                            </td>
                                                            <td class="style3">
                                                                Direccion:</td>
                                                            <td>
                                                                <asp:Label ID="lbldireccion" runat="server" Text="direccion"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    
                                                    
                                                </div>
											</div>
											<div class="fix" id="gvddjj" align="center">
                                                <br />
                                                Periodos<div style="height: 202px; width: 742px; margin-right: 0px;">
                                                    <br />
                                                    <asp:GridView ID="gvPeriodos" runat="server" BackColor="White" 
                                BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" 
                                ForeColor="Black" GridLines="Vertical" Width="673px" 
                                AutoGenerateColumns="False" AllowPaging="True" PageSize="5" Height="162px">
                                <FooterStyle BackColor="Yellow" />
                                                        <RowStyle Height="10px" />
                                <Columns>
                                    <asp:BoundField DataField="ANIO" HeaderText="A�O" >
                                        <ItemStyle Width="2%" HorizontalAlign ="Center" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField DataField="CUOTA" HeaderText="PERIO." >
                                        <ItemStyle HorizontalAlign="Center" Width="2%" />
                                    </asp:BoundField>
                                    
                                    <asp:BoundField DataField="FECHA_VENCIMIENTO" HeaderText="FECHA VENC." 
                                        DataFormatString="{0:M-dd-yyyy}" />
                                        
                                    <asp:BoundField DataField="CANT_EMPLEADOS" HeaderText="EMPL." >
                                        <ItemStyle HorizontalAlign="Center" Width="2%" />
                                    </asp:BoundField >
                                    
                                    
                                    <asp:BoundField DataField="MONTO_TOTAL_DECLARADO" HeaderText="MTO.  DECL." >
                                        <ItemStyle HorizontalAlign="Right"  Width="20%"/>
                                    </asp:BoundField>
                                    
                                    
                                    <asp:BoundField DataField="IMPORTE_ORIGEN" HeaderText="IMP.  ORIGEN" >
                                        <ItemStyle HorizontalAlign="Right"  Width="20%"/>
                                    </asp:BoundField>
                                    
                                                                        
                                    
                                    <asp:BoundField DataField="ACTIVO_INTERESES" HeaderText="ACT.  E INT." >
                                        <ItemStyle HorizontalAlign="Right"  Width="20%"/>
                                    </asp:BoundField>
                                    
                                    
                                    <asp:BoundField DataField="IMPORTE_TOTAL" HeaderText="IMP. TOTAL" >
                                        <ItemStyle HorizontalAlign="Right"  Width="20%"/>
                                    </asp:BoundField>
                                    
                                    
                                    <asp:BoundField DataField="ESTADO" HeaderText="ESTADO" >
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    
                                    
                                    <asp:ButtonField CommandName="Select" 
                                        ShowHeader="True" Text="Pagar" >
                                        <ItemStyle ForeColor="#CC0000" HorizontalAlign="Center" />
                                         
                                    </asp:ButtonField>
                                    
                                    
                                    
                                </Columns>
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#807C7C" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" Height="12px" 
                                                            HorizontalAlign="Center" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                                                    <div style="height: 33px; width: 665px;" align="center">
                                                        <br />
                                                        Total de su DEUDA: $&nbsp;                                                         <asp:Label ID="lbltotal_deuda" runat="server" Font-Bold="True" Text="0,00"></asp:Label>
                                                        <br />
                                                        <br />
                                                        <br />
                                                        <a href="#"><div class="cp2">
                                                             
    <a href="http://www.grupomdq.com/">Design by Grupo MDQ.-</div></a>
                                                        <br />
                                                        <br />
                                                       
                                                </div>
                                            </div>
											</div>
</div>
    </form>
</body>
</html>