<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmlogin_admin.aspx.vb" Inherits="web_tributaria.webfrmlogin_admin" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc1" %>


<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link  href="../css/generica.css" rel="stylesheet" type="text/css" />
        <link  href="../css/botones.css" rel="stylesheet" type="text/css" />
        <link  href="../css/mensajes.css" rel="stylesheet" type="text/css" />
        <script src="../js/funciones.js" type="text/javascript"></script>
        
        
        <script type="text/javascript" language="javascript">
            function validarDatos() {
                                               
                if (control_vacio(window.document.getElementById('txtusuario'), 'Debe Ingresar un usuario')) {
                    return false;
                }

                if (control_vacio(window.document.getElementById('txtpassword'), 'Debe Ingresar un password')) {
                    return false;
                }

                if (control_vacio(window.document.getElementById('txtcaptcha'), 'Debe Ingresar el c�digo de confirmaci�n')) {
                    return false;
                }

                if (!validar_cadena(window.document.getElementById('txtcaptcha'), 'C�digo de verificaci�n')) {
                    return false;
                }

                return true;
            }

        </script>


 


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Sistema tributario :: Login administrador</title>
    <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
    <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />



<link href="../css/webfrmconsulta_b.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            #footer
            {
                height: 0px;
            }
                        
            .columna_titulo{
            	width: 40%;
            	text-align:center;
            }
            
            .columna_dato{
            	width: 60%;
            	text-align:center;                  	          
            }
            
            .textbox {
                 text-align:center;
                 margin:2px;
            }
            
            
            .textbox_captcha {
                 text-align:center;
                  margin-bottom:2px;
            }
            
                        
            .titulo_tabla{
	            background-image: url('../imagenes/barra_cabecera.png');
	            color:#FFFFFF;
	            font-size:12px;
	            text-align:center;
	            border-left:1px solid #FFFFFF;
	            height:40px;
            }
                             
            
        </style>
        
               
        
        <style type="text/css">        
            table_visitas {                
                border: 1px solid #000;
                border-collapse: collapse;
                caption-side: top;
            }
            
            table_visitas_th, table_visitas_td {
                width: 80px;
                text-align:center;
                vertical-align: top;
                border: 1px solid #000;
                padding: 0.3em;
            }
            
            table_visitas_caption {
                padding: 0.3em;
                color: #fff;
                background: #000;
            }
       
        </style>
        
        
    	        
</head>

<body>

    <form id="formulario" class="form"  runat="server">
        <div id="div_main" class="main">
        
            <div id="header">  
                <div class="div_font" style="padding:0px 5px 0px 5px;">
                    <a href="#" title="Usar fuente menor" class="link_font" onclick="assign_font('-');">A-</a> 
                </div>
                
                <div class="div_font">
		            <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 
                </div>		            
                                                 
                <span id="logo-text">Consulta tributaria :: Login administrador</span>  
                <h2 id="slogan">Municipalidad de <asp:Label ID="lblmunicipalidad_name" runat="server" Text=""></asp:Label></h2>			                    
            </div>
        
        
            <div id="div_contenedor" class="content" runat="server">
	    	    <div class="barra">
                    <strong>&nbsp;SISTEMA TRIBUTARIO :: LOGIN ADMINISTRADOR</strong>
                </div>                                          

                    
                <div id="mensaje" style="padding:20px 0px 0px 0px;" align="center">
	                <div id="div_mensaje" class="Globo GlbRed" style="width:45%;" runat="server" visible="FALSE">
	                    <asp:Label ID="lblmensaje" runat="server" Text="" Visible="FALSE"></asp:Label>
	                </div>
                </div>
                    
                            
                <div id ="tabla" style="width: 872px; padding: 20px 0px 60px 0px;" align="center"; >                    
                    
	                
	                <div id="div_login" style="border:solid; width:300px; ">                                                       
                        <table id="table_login"  style="width: 300px; height: 136px;" align="center" border="0">
                            <tr style="height:30px;">
                                <td colspan="2" style="text-align:center; font-weight:bold; background-color:Black; color:White;" >Acceso usuario administrador</td>                        
                            </tr>
                            <tr>
                                <td style="text-align:right; font-weight:bold;">Usuario:</td>
                                <td><input id="txtusuario" type="text" maxlength="10" style="width:150px; text-align:center;" runat="server" /></td>
                            </tr>
                            <tr>                        
                                <td style="text-align:right; font-weight:bold;">Password:</td>                                                    
                                <td><input id="txtpassword" type="password" maxlength="10" style="width:150px; text-align:center;" runat="server" /></td>
                            </tr>                                    
                            <tr>
                                <td colspan="2" style="text-align:center; border:none; width:50px;" align="center">
                                    <cc1:captchacontrol ID="verificador_captcha" runat="server" CaptchaLength="5" CaptchaMinTimeout="5"
                                         CaptchaMaxTimeout="240" FontColor = "#FF7920"  Font-Size="XX-Large" Height="50px" 
                                                            
                                        CaptchaChars="ACDEFGHJKLNPQRTUVXYZ2346789" CaptchaBackgroundNoise="None" 
                                        NoiseColor="Black" />
                                </td>
                            </tr>
                            <tr>                                             
                                <td colspan="2" style="text-align:center;"><input id="txtcaptcha" type="text" maxlength="5" style="width:250px; text-align:center;" runat="server" /></td>                                
                            </tr>
                            
                            <tr style="text-align:center; background-color:Black; height:30px;">                                                                     
                                <td colspan="2">
                                    <asp:Button ID="btnIngresar" runat="server" Text="Ingresar" />
                                </td>
                            </tr>                    
                        </table>
	                    
                    </div>
                <div>
            </div>                                                       
        </div>    
                                
                                
                                
        <div style="height: 15px; font-weight: bold; font-size: small; text-align: center; font-family: 'Courier New', Courier, 'espacio sencillo';" class="barra">             
        </div>
         
                                    
	    </div>
        <div style="padding: 15px 5px 0px 0px; text-align:right;">
            <a href="webfrmindex.aspx" class="link">[Volver]</a>
            <br />
            <a href="http://www.grupomdq.com" target="_blank" class="link">
                [Desarrollado por Grupo MDQ]
            </a>
            <br />
            <asp:Label ID="lblversion" runat="server"></asp:Label>
        </div>
                    
            </div>
            
        </form>
    </body>
</html>



