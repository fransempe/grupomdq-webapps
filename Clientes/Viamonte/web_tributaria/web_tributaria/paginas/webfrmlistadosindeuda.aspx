<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="webfrmlistadosindeuda.aspx.vb" Inherits="web_tributaria.webfrmlistadosindeuda" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <link  href="../css/generica.css" rel="stylesheet" type="text/css" />
        <link  href="../css/botoneslistado.css" rel="stylesheet" type="text/css" />
        <script src="../tooltip/boxover.js" type="text/javascript"></script>     
        <script src="../js/funciones.js" type="text/javascript"></script>
        

            
        <script type="text/javascript">

            /* Esta Funcion PIDE una CONFIRMACION de CIERRE DE SESSION */        
            function cerrar_session(){
                return confirm("Usted esta a punto de cerrar su sesion.\n�Desea continuar?")
            }




            function aguarde() {
                //window.document.getElementById('tabla_contenedora').style.borderStyle = = '0px';                
                window.document.getElementById('tabla_sin_deuda').style.display = 'none';
                window.document.getElementById('tabla_mensaje').style.display = 'block';
                return true;
            }
            
        </script>



        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>Sistema tributario :: Comprobantes </title>
        <link rel="icon" href="../favicon.ico"  type="image/x-icon" />
        <link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />

            
            
        <link  href="../css/botones.css" rel="stylesheet" type="text/css" />
        <link href="../css/webfrmconsulta_b.css" rel="stylesheet" type="text/css" />
            
    </head>


    <body>

        <form id="formulario" class="form" runat="server">
            <div id ="pagina"  class="main">        
            
                <div id="header">   
                    
                    <div class="div_font" style="padding:0px 5px 0px 5px;">
                        <a href="#" title="Usar fuente menor" class="link_font" onclick="assign_font('-');">A-</a> 
                    </div>
                
                    <div class="div_font">
		                <a href="#" title="Usar fuente por defecto" class="link_font" onclick="assign_font('=');">A</a> 
                    </div>		    
                    
                    
                    <span id="logo-text_listado">Consulta tributaria :: Comprobantes </span>  
                    <h2 id="slogan_listado">Municipalidad de <asp:Label ID="lblmunicipalidad_name" runat="server" Text="Label"></asp:Label></h2>	
                    
                                
                    <div id="links" style="height: 10px; width: 50%; float:right; text-align: right; padding: 5px 5px 0px 0px;"> 
                        &nbsp;&nbsp;&nbsp; <asp:LinkButton ID="link_cerrar_session" runat="server"  CssClass="link">[Cerrar 
                        Sesi�n]</asp:LinkButton>
                    </div>
                </div>
        
                
                    
                <div class="content">
		            <div class="barra"><strong>&nbsp;CONSULTA DE DEUDA Y EMISION DE COMPROBANTES</strong></div>
    		    
    		    
                    <div id="tabla" style="padding:50px 0px 50px 0px; height: 186px;" align="center">       
                    
                        <asp:Label id="lbltipo_conexion" runat="server" Text="ninguna.-"  ForeColor="White"></asp:Label>
                                              
                        <table id="tabla_contenedora" align="center" style="width:500px;" border="5" >
                            <tr>
                                <td>                                    
                                    <table id="tabla_sin_deuda" align="center" style="display:block;">
                                        <tr align="center" style="font-size:medium">
                                            <td colspan="2" class="style1">
                                                <div id="mensaje">
                                                    <asp:Label ID="lblmensaje" runat="server" Text=""></asp:Label>
                                                </div>
                                            </td>
                                        </tr>                
                                        <tr> 
                                            <td colspan="2">
                                                <p align="center">Desde ac� usted podr� descargar su informe de deuda</p>
                                            </td>
                                        </tr>                                                        
                                        <tr>
                                            <td align="right" style="width:55%;">
                                                <p><strong>Imprimir informe de deuda -></strong></p>
                                            </td>
                                            <td align="left" style="width:45%;">                                                
                                                <asp:LinkButton ID="link_imprimir" runat="server" CssClass="link"><img alt="Constancia" src="../imagenes/pdf.jpg" width="42"  height="65" /></asp:LinkButton>                                                                                                      
                                            </td>
                                        </tr>                                            
                                    </table>
                                    
                                     <table id="tabla_mensaje" border= "0" style="display:none; width:60%;" align="center">
                                        <tr>
                                            <td align="center" style="width:20%;"><img id="img_load" alt="" src="../imagenes/loading.gif" /></td>
                                            <td align="left" style="width:100%; height:auto;">Aguarde, obteniendo informe de deuda .... </td>
                                        </tr>
                                    </table> 
                                </td>
                            </tr>                                                                      
                        </table>
                    </div>
                                                               
                    
                                
                    <div id="ajax" style="color: #000000;  width:100%;">
                             
                             
                             <div style="height: 15px; font-weight: bold; font-size: small; text-align: center; font-family: 'Courier New', Courier, 'espacio sencillo';" class="barra">
                                NOTA: </div>
                             <div align="center">&nbsp; - Los per�odos de deuda aqu� listados pueden no reflejar los pagos realizados recientemente. Ante cualquier duda relacionada con su estado de deuda&nbsp; p�ngase en contacto con la Municipalidad a trav�s del tel�fono 
                                    <strong><asp:Label ID="lbltelefono" runat="server" Text="TELEFONO"></asp:Label></strong>&nbsp;o la direcci�n de correo electr�nico 
                                    <strong><asp:Label ID="lblmail" runat="server" Text="MAIL"></asp:Label></strong>.</div><div align="center">
                                    &nbsp; - Para un correcto funcionamiento en la Emisi�n de Comprobantes se le aconseja tener actualizado el Acrobat Reader. Para actualizar a la �ltima versi�n    
                                    <a href="http://www.adobe.com/es/products/acrobat/readstep2.html" target="_blank"; class="link">
                                    [clic aqu�]</a>.
                                </div>
                                        
                            <div style="padding:10px 0px 5px 0px; text-align:right";><a href="http://www.grupomdq.com" target="_blank" class="link">
                                [Desarrollado por Grupo MDQ]</a>
                                <br />
                                <asp:Label ID="lblversion" runat="server"></asp:Label></div></div></div></div>
                                
		    
		    
        </form>
    </body>
</html>