<%@ Page Language="vb"  enableEventValidation="false"   AutoEventWireup="false" CodeBehind="webfrmcargaractividades.aspx.vb" Inherits="web_tish.webfrmcargaractividades" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


<script language="javascript"  type="text/javascript">

    var keyCode
    function tecla(e){
        var valor_ascii = e?e.keyCode:event.keyCode;
        
        if (valor_ascii == 13) {
            regresar()
        }
    }



    function regresar(){
          
        if (window.document.getElementById('lblcodigo').innerHTML != "Codigo.")
            {   
                /*Identifico la Ventana Padre*/
                var mVentanaPadre = window.opener;
              
                
                /*Obtengo los datos a devolver*/
                var mDato_codigo = document.getElementById('lblcodigo');
                var mDato_descripcion = document.getElementById('lbldescripcion');
                var mDato_alicuota = document.getElementById('lblalicuota');
                
                
                /*Asino los campos de la ventana Padre*/
                var mCampoPadre_codigo = mVentanaPadre.document.getElementById('txtcodigo');      
                var mCampoPadre_descripcion = mVentanaPadre.document.getElementById('txtdescripcion');      
                var mCampoPadre_alicuota = mVentanaPadre.document.getElementById('txtalicuota');      
                
                                
              
                /* Asigno los datos */
                mCampoPadre_codigo.value = mDato_codigo.innerHTML;       
                mCampoPadre_descripcion.value = mDato_descripcion.innerHTML;       
                mCampoPadre_alicuota.value = mDato_alicuota.innerHTML;       
                
                mVentanaPadre.document.formulario.txtimporte.focus();                
                
                
                window.close()
             } else {
                alert("Debe Seleccionar una Actividad.") ;
             }
                
       }
        
        
        
        
        
        
        function Resaltar_On(GridView)
        {
            if(GridView != null)
            {
               GridView.originalBgColor = GridView.style.backgroundColor;
               GridView.style.backgroundColor="##FFFF66";
              
            }
        }


        function Resaltar_Off(GridView)
        {
            if(GridView != null)
            {
                GridView.style.backgroundColor = GridView.originalBgColor;
            }
        }
        
</script> 


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sistema TISH.-</title>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
        <style type="text/css">

            .style2
            {
                width: 67px;
            }
            .style3
            {
                width: 67px;
                height: 17px;
            }
            .style4
            {
                height: 17px;
            }
            .style8
            {
                height: 278px;
                width: 381px;
            }
            .style9
            {
                width: 381px;
                height: 99px;
            }
        </style>
</head>

<body  onkeypress = "tecla(event)" onResize="window.resizeTo(400,480)">

    <form id="form1" runat="server">
    <table align="left" border="1" style="style3" dir="ltr">
        <tr>
            <td align="center" class="style8" dir="rtl">
                                                    <asp:GridView ID="gvactividades" runat="server" BackColor="White" 
                                BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" 
                                ForeColor="Black" GridLines="Vertical" Width="317px" 
                                AutoGenerateColumns="False" Height="117px">
                                <FooterStyle BackColor="#CCCCCC" />
                                                        <RowStyle Height="10px" />
                                <Columns>
                                    <asp:BoundField DataField="CODIGO" HeaderText="CODIGO" >
                                        <ItemStyle HorizontalAlign="Left" Width="30px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DESCRIPCION" HeaderText="DESCRIPCION" >
                                        <ItemStyle HorizontalAlign="Center" Width="200px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ALICUOTA" HeaderText="ALICUOTA"   
                                        DataFormatString = "{0:F2}" >
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                </Columns>
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" Height="10px" />
                                <SelectedRowStyle BackColor="#99FF66" Font-Bold="True" ForeColor="Black" />
                                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" Height="12px" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                                                    </td>
        </tr>
        <tr>
            <td class="style9">
                                                    <div>
                                                        Datos Selecionados:</div>
                                                    <div>
                                                        <table border="1" style="width:100%;">
                                                            <tr>
                                                                <td align="left" class="style2">
                                                                    CODIGO:</td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblcodigo" runat="server" Text="Codigo."></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="style3">
                                                                    DESCRIPCION:</td>
                                                                <td align="left" class="style4">
                                                                    <asp:Label ID="lbldescripcion" runat="server" Text="Descripcion."></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="style2">
                                                                    ALICUOTA:</td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblalicuota" runat="server" Text="Alicuota."></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div style="height: 38px; width: 381px;" align="center">
                                                        <br />
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:Button ID="btncargar" runat="server" Height="22px" Text="Cargar" />
                                                        <br />
                                                        <br />
                                                        <br />
                                                    </div>
                                                    </td>
        </tr>
    </table>
</form>

</body>
</html>